## CSV Metadata Extraction Desktop Application

### Step 1: Generate Executable JAR File 

 * Prerequisites: 

	**JAVA 17** and **Apache Ant** should ve installed in your machine


### Step 2: Run Executable JAR File 

#### (a) Run Locally

 * Option 1: **GUI**
	
	Double click to the generated executable JAR file
	
 * Option 2: **Command Line**
	
	Run command: 
	
	```
	TESTING-Extract-Metadata-using-App.cmd
	```
	
#### (b) Containirize and Run 

 * Build the Docker Image using the following command: 
 
	```
	docker build -t csv-metadata-extraction-image .
	```
	
 * Verifying the Docker Image
 
	```
	docker images
	```
	
 * Run the Docker Container 
 
	```
	docker run -it csv-metadata-extraction-image
	```
	
 * The you can get the generated file with metatada using commands:
 
	```
	docker ps -a
	```
	
	and
	
	```
	docker cp <containerId>:/file/path/within/container /host/path/target
	```
	
	Replace <containerId> with the particular container id
	
	```
	docker cp <containerId>:/usr/src/app/Test-Metadata-Excel-File.xlsx ./Test-Metadata-Excel-File.xlsx
	```