package org.ntua.iccs.harmonicss.metadata;

import java.util.Date;

import javax.swing.JFrame;

import org.timchros.core.log.messages.MemoryMessage;

public class MetadataExtractionMain {

	public static void main(String[] args) {
		
		System.out.println("MetadataExtractionMain: RUN ON DATE: " + new Date());
		System.out.println();
		System.out.println("JVM memory: " + new MemoryMessage().toParsableString());
		System.out.println();
		
		final ExtractMetadataFrame frame = new ExtractMetadataFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		frame.setVisible(true);
		
		System.out.println("MetadataExtractionMain: Window for Metadata Extraction Successfully Created !");
	}

}
