package org.ntua.iccs.harmonicss.metadata.model;

public class FileInfo {

	private final String name;
	private final int sheetsNumber;
	private final long bytes;
	private final int patientsNumber;
	
	public FileInfo(String name, int sheetsNumber, long bytes, int patientsNumber) {
		super();
		this.name = name;
		this.sheetsNumber= sheetsNumber;
		this.bytes = bytes;
		this.patientsNumber = patientsNumber;
	}

	public String getName() {
		return name;
	}
	
	public int getSheetsNumber() {
		return sheetsNumber;
	}

	public long getBytes() {
		return bytes;
	}

	public int getPatientsNumber() {
		return patientsNumber;
	}

	@Override
	public String toString() {
		return "FileInfo [name=" + name + ", sheetsNumber= " + sheetsNumber + ", bytes=" + bytes + ", patientsNumber=" + patientsNumber + "]";
	}
	
}
