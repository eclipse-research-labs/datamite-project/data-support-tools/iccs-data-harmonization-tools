package org.ntua.iccs.harmonicss.metadata.model;

public abstract class Field {

	final int sheetNum;
	
	final int rowNum;
	final int colNum;
	
	public Field(int sheetNum, int rowNum, int colNum) {
		this.sheetNum = sheetNum;
		this.rowNum = rowNum;
		this.colNum = colNum;
	}

	public int getSheetNum() {
		return sheetNum;
	}
	
	public int getRowNum() {
		return rowNum;
	}

	public int getColNum() {
		return colNum;
	}

	@Override
	public String toString() {
		return sheetNum + "[" + rowNum + "," + colNum + "]";
	}
	
	
	
}
