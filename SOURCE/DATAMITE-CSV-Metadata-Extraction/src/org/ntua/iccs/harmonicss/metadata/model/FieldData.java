package org.ntua.iccs.harmonicss.metadata.model;

public class FieldData extends Field {

	final String data;
	
	public FieldData(int sheetNum, int rowNum, int colNum, String data) {
		super(sheetNum, rowNum, colNum);
		this.data = data;
	}

	public String getData() {
		return data;
	}

	@Override
	public String toString() {
		return super.toString() + "=" + this.data;
	}
	
	
	
}
