package org.ntua.iccs.harmonicss.metadata;

import javax.swing.JTextArea;

/**
 * This class can be used to <i>control</i> the messages printed to either Standard output or {@link JTextArea}.
 * <p>
 * In case a {@link JTextArea} has been specified, the messages will be printed in this area, 
 * otherwise they will be printed in the standard output.
 * 
 * @author Efthymios Chondrogiannis
 */
public final class LogsArea {
	
	/** Determines whether the messages provided to the method {@link LogsArea#println(String)} will be written to the standard output or not */
	private static boolean isEnable = true;
	
	/** The {@link JTextArea} in which the messages will be written. */
	private static JTextArea logsTextArea;
	
	/** Ensures that we will not create an instance of this class */
	private LogsArea() {
		
	}
	
	/**
	 * Specifies the {@link JTextArea} in which the messages will be written.
	 * <p>
	 * If NO {@link JTextArea} specified, the messages will be printed in the standard output.
	 * 
	 * @param textArea
	 * 		The JTextArea in which the messages will be placed.
	 */
	public static void setTextArea(JTextArea textArea) {
		logsTextArea = textArea;
	}
	
	/**
	 * Enables Logging mechanism.
	 */
	public static void enable() {
		isEnable = true;
	}
	
	/**
	 * Disables Logging mechanism.
	 */
	public static void disable() {
		isEnable = false;
	}
	
	/**
	 * Writes the given message in the area specified, changing line.
	 * 
	 * @param msg 
	 * 		The given message
	 */
	public static void println(String msg) {
		if (isEnable) {
			System.out.println(msg);
			if (logsTextArea != null) {
				logsTextArea.append(msg + "\n");
			}
		}
	}
	
	/**
	 * Changes line.
	 */
	public static void println() {
		println("");
	}

	/**
	 * Provides a description of the LogsArea class.
	 * 
	 * @return
	 * 		A description of this class
	 */
	public static String classToString() {
		final String area = (logsTextArea != null) ? "JTextArea provide" : "standard output" ;
		return 
			LogsArea.class.getSimpleName() + " [ The messages written using this class are placed in the " + area + " ]";
	}

}
