package org.ntua.iccs.harmonicss.metadata.model;

public class ColumnData {

	private final int columnNumber;
	private final String data;
	
	public ColumnData(int columnNumber, String data) {
		this.columnNumber = columnNumber;
		this.data = data;
	}

	public int getColumnNumber() {
		return columnNumber;
	}

	public String getData() {
		return data;
	}

	@Override
	public String toString() {
		return columnNumber + ":" + data;
	}
	
}
