package org.ntua.iccs.harmonicss.metadata;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

public class ExtractMetadataFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private static final String TITLE = "DATAMITE - CSV Metadata Extraction";
	private static final int WIDTH = 800;
	private static final int HEIGHT = 550;
	
	private final GridBagLayout layout = new GridBagLayout();
	private final GridBagConstraints constraints = new GridBagConstraints();
	private final JPanel panel = new JPanel();
	
	// FONTS
	private static final Font verdana10 = new Font("Verdana", Font.PLAIN, 10);
	private static final Font verdana12 = new Font("Verdana", Font.PLAIN, 12);
	private static final Font verdana14bold = new Font("Verdana", Font.BOLD, 14);
	private static final Font sans10 = new Font(Font.SANS_SERIF, Font.PLAIN, 10);
	private static final Font sans13bold = new Font(Font.SANS_SERIF, Font.BOLD, 13);
	private static final Font sans16bold = new Font(Font.SANS_SERIF, Font.BOLD, 16);
	private static final Font sans16 = new Font(Font.SANS_SERIF, Font.PLAIN, 16);
	
	// COLORS
	private static final Color blueColor = new Color(0, 62, 129);
	private static final Color greenColor = new Color(0, 102, 0);
	private static final Color redColor = new Color(128, 0, 0);
	
	// BORDER
	protected static final Border redBorder = BorderFactory.createCompoundBorder(
			BorderFactory.createLineBorder(redColor), 
			BorderFactory.createEmptyBorder(0, 5, 0, 0)
	);
	
	protected static final Border greenBorder = BorderFactory.createCompoundBorder(
			BorderFactory.createLineBorder(greenColor), 
			BorderFactory.createEmptyBorder(0, 5, 0, 0)
	);
	
	
	public ExtractMetadataFrame() {
		super(TITLE);
	    final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	    final int x = (screen.width - WIDTH) / 2;
	    final int y = (screen.height - HEIGHT) / 2;
	    setBounds(x, y, WIDTH, HEIGHT);
	    
	    this.panel.setLayout(layout);
		
	    /* COMPONENTS */
	    
	    final JLabel notes = new JLabel("Retrieves the names of the fields and the different values recorded for each one ...");
	    notes.setFont(verdana12);

	    JLabel logo = null;
	    try {
	    	BufferedImage image = ImageIO.read( this.getClass().getClassLoader().getResourceAsStream("img/ICCS-NTUA-Logo.png") );
	    	logo= new JLabel(new ImageIcon(image), JLabel.RIGHT);
	    } catch (Throwable t) {
	    	logo = new JLabel("ICCS/NTUA");
	    	logo.setFont(sans16);
	    }
	    
	    final JLabel title1 = new JLabel("Select the Excel File with your data:"); 			
	    title1.setFont(sans16bold);
	    title1.setForeground (blueColor);
	    
	    final JLabel cohortXlsFilePathLabel = new JLabel("     Cohort Excel File: ", JLabel.LEFT);
	    cohortXlsFilePathLabel.setFont(sans13bold); 
	    final JTextField cohortXlsFilePath = new JTextField(40);
	    cohortXlsFilePath.setFont(verdana10); 
	    cohortXlsFilePath.setEditable(false);
	    cohortXlsFilePath.setBackground(Color.WHITE);
	    cohortXlsFilePath.setBorder(redBorder);
	    
	    final JButton xlsFileJButton = new JButton("Select File");
	    xlsFileJButton.setFont(sans13bold);
	    
	    final JLabel title2 = new JLabel("Specify the folder where the Generated Excel File will be placed:"); 			
	    title2.setFont(sans16bold);
	    title2.setForeground (blueColor);
	    
	    final JLabel outFolderLabel = new JLabel("     Output Folder: ", JLabel.LEFT);
	    outFolderLabel.setFont(sans13bold); 
	    final JTextField outFolderPath = new JTextField(40);
	    outFolderPath.setFont(verdana10);
	    outFolderPath.setEditable(false);
	    outFolderPath.setBackground(Color.WHITE);
	    outFolderPath.setBorder(redBorder);
	    final JButton folderJButton = new JButton("Select Folder");
	    folderJButton.setFont(sans13bold);
	    
	    // TODO: ADD CHECKBOX ABOUT OWL ONTOLOGY 
	    
	    final JButton createButton = new JButton("Extract Metadata");
	    createButton.setFont(verdana14bold); 
	    createButton.setForeground(redColor);
	    
	    final JLabel logsLabel = new JLabel("Logs Area", JLabel.CENTER); 
	    logsLabel.setFont(sans13bold); 
	    final JTextArea logsArea = new JTextArea(5,20);
	    logsArea.setEditable(false);
	    logsArea.setBorder(BorderFactory.createCompoundBorder(
	    	logsArea.getBorder(),BorderFactory.createEmptyBorder(10, 5, 0, 0)
		));
	    JScrollPane logsAreaScroller = new JScrollPane(logsArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	    
	    final JLabel footer = new JLabel("Application developed by ICCS/NTUA within European Project HarmonicSS."); 			
	    footer.setFont(sans10);
	    
	    /* LISTENERS */
	    
	    xlsFileJButton.addActionListener(
	    	new SelectFileListener(cohortXlsFilePath, outFolderPath));
	    
	    folderJButton.addActionListener(
		    	new SelectFolderListener(outFolderPath));
	    
	    createButton.addActionListener(
	    	new ExtractMetadataActionListener(cohortXlsFilePath, outFolderPath, logsArea));
	    
	    /* Adding components to JFrame */
	    
	    constraints.insets = new Insets( 1, 1, 1, 1 );  
	    constraints.fill=GridBagConstraints.BOTH;
		constraints.anchor=GridBagConstraints.CENTER;
		
		addComponent(new JLabel(),			0,0,1,1,10,5);
		addComponent(notes,					0,1,3,1,80,5);
		addComponent(logo,					0,4,1,1,80,5);
		addComponent(new JLabel(),			0,5,1,1,10,5);
		
		addComponent(new JLabel(),			1,0,1,2,10,10);
		addComponent(title1,				1,1,4,2,80,10);
		addComponent(new JLabel(),			1,5,1,2,10,10);
		
		addComponent(new JLabel(),			3,0,1,1,10,5);
		addComponent(cohortXlsFilePathLabel,3,1,1,1,30,5);
		addComponent(cohortXlsFilePath,		3,2,1,1,40,5);
		addComponent(xlsFileJButton,		3,4,1,1,10,5);
		addComponent(new JLabel(),			3,5,1,1,10,5);
		
		addComponent(new JLabel(),			4,0,1,2,10,10);
		addComponent(title2,				4,1,4,2,80,10);
		addComponent(new JLabel(),			4,5,1,2,10,10);
		
		addComponent(new JLabel(),			6,0,1,1,10,5);
		addComponent(outFolderLabel,		6,1,1,1,30,5);
		addComponent(outFolderPath,			6,2,1,1,40,5);
		addComponent(folderJButton,			6,4,1,1,10,5);
		addComponent(new JLabel(),			6,5,1,1,10,5);
		
		addComponent(new JLabel(),			7,0,6,1,10,5);
		
		constraints.fill=GridBagConstraints.BOTH;
		
		addComponent(new JLabel(),			8,0,1,1,10,5);
		addComponent(createButton,			8,1,4,1,80,5);
		addComponent(new JLabel(),			8,5,1,1,10,5);
		
		addComponent(new JLabel(),			9,0,1,2,10,5);
		addComponent(logsLabel,				9,1,4,2,90,5);
		addComponent(new JLabel(),			9,5,1,2,10,5);
		
		addComponent(new JLabel(),			11,0,1,10,10,40);
		addComponent(logsAreaScroller,		11,1,4,10,80,40);
		addComponent(new JLabel(),			11,5,1,10,10,40);
	    
		addComponent(new JLabel(),			21,0,1,2,10,5);
		addComponent(footer,				21,1,5,2,90,5);
		
	    this.setContentPane(panel);
	}
	
	private void addComponent(Component com, int row, int column, int width, int height, int wx, int wy) {
		constraints.gridx=column;
		constraints.gridy=row;
		constraints.gridwidth=width;
		constraints.gridheight=height;
		constraints.weightx=wx;
		constraints.weighty=wy;
		
		layout.setConstraints(com,constraints);
		panel.add(com);
	}
	
	private static final class SelectFileListener implements ActionListener {

		private final JTextField cohortXlsFilePath;
		private final JTextField outFolderPath;
		
		public SelectFileListener(JTextField cohortXlsFilePath, JTextField outFolderPath) {
			super();
			this.cohortXlsFilePath = cohortXlsFilePath;
			this.outFolderPath = outFolderPath;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			final JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
			jfc.setDialogTitle("Select Cohort Excel File");
			jfc.setAcceptAllFileFilterUsed(false);
			final FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel Files", "xls", "xlsx");
			jfc.addChoosableFileFilter(filter);
			
			int returnValue = jfc.showDialog(null, "Select File");
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				File selectedFile = jfc.getSelectedFile();
				this.cohortXlsFilePath.setText(selectedFile.getAbsolutePath());
				this.cohortXlsFilePath.setBorder(greenBorder);
				
				this.outFolderPath.setText(selectedFile.getParentFile().getAbsolutePath());
				this.outFolderPath.setBorder(greenBorder);
			}
		}
		
	} // END OF SelectFileListener CLASS
	
	private static final class SelectFolderListener implements ActionListener {

		private final JTextField outFolderPath;
		
		public SelectFolderListener(JTextField outFolderPath) {
			super();
			this.outFolderPath = outFolderPath;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			final JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
			jfc.setDialogTitle("Select Output Folder");
			jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			
			int returnValue = jfc.showDialog(null, "Select Folder");
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				File selectedFile = jfc.getSelectedFile();
				this.outFolderPath.setText(selectedFile.getAbsolutePath());
				this.outFolderPath.setBorder(greenBorder);
			}
		}
		
	} // END OF SelectFolderListener CLASS
	
	private static final class ExtractMetadataActionListener implements ActionListener {

	    private final JTextField cohortXlsFilePath;
	    private final JTextField outFolder;
	    private final JTextArea logsArea;
	    
	    private ExtractMetadataActionListener(JTextField cohortXlsFilePath, JTextField outFolder, JTextArea logsArea) {
			super();
			this.cohortXlsFilePath = cohortXlsFilePath;
			this.outFolder = outFolder;
			this.logsArea = logsArea;
			
			LogsArea.setTextArea(this.logsArea);
		}
	    
		@Override
		public void actionPerformed(ActionEvent e) {
			final String cohortXlsFilePath = this.cohortXlsFilePath.getText().trim();
			final String outFolder = this.outFolder.getText().trim();
			
			// Check ... Data
			if (cohortXlsFilePath.equals("")) {
				JOptionPane.showMessageDialog(null, "Cohort Excel File - Not specified !", "Missing Data", JOptionPane.WARNING_MESSAGE);
				return;
			} else if (!new File(cohortXlsFilePath).exists()) {
				JOptionPane.showMessageDialog(null, "Cohort Excel File - Not exist !", "Missing Data", JOptionPane.WARNING_MESSAGE);
				return;
			}
			
			if (outFolder.equals("")) {
				JOptionPane.showMessageDialog(null, "Output Folder - Not specified !", "Missing Data", JOptionPane.WARNING_MESSAGE);
				return;
			} else if (!new File(outFolder).exists()) {
				JOptionPane.showMessageDialog(null, "Output Folder - Not exist !", "Missing Data", JOptionPane.WARNING_MESSAGE);
				return;
			}
				
			// Existence of Generated File(s)
			String xlsFileName = new File(cohortXlsFilePath).getName();
			if (xlsFileName.toLowerCase().endsWith(".xlsx")) xlsFileName = xlsFileName.substring(0, xlsFileName.length()-5);
			if (xlsFileName.toLowerCase().endsWith(".xls")) xlsFileName = xlsFileName.substring(0, xlsFileName.length()-4);
			
			final String metadataXlsFilePath = outFolder + File.separator + xlsFileName + "-Metadata.xlsx";
			File genFile = new File(metadataXlsFilePath);
			if (genFile.exists()) {
				int response = JOptionPane.showConfirmDialog(null, 
					"Excel File with name \"" + genFile.getName() + "\" already exists ! Do you want to replace it with the new one ?", 
					" Please choose one of yes or no.", 
					JOptionPane.YES_NO_OPTION,
					JOptionPane.WARNING_MESSAGE
				);
				if (response == JOptionPane.NO_OPTION) return;
			}
			
			// Generate XML Files
			try {
				LogsArea.println("Cohort XLS File PATH: " + new File(cohortXlsFilePath).getAbsolutePath());
				LogsArea.println();
				
				MetadataExtractionService.process(cohortXlsFilePath, metadataXlsFilePath);
				
				LogsArea.println();
				LogsArea.println("Extracting Metadata - SUCCESSFULLY COMPLETED !");
				JOptionPane.showMessageDialog(null, "Cohort Metadata - Successfully Extracted !", "Success Message",JOptionPane.INFORMATION_MESSAGE);
			} catch (Throwable t) {
				LogsArea.println("Extracting Metadata - ERROR ! \n\n");
				t.printStackTrace();
				JOptionPane.showMessageDialog(null, "An error occured while Extracting Metadate from Excel File. \n\nError message: " + t.getMessage(), "Extracting Metadata - Problem",JOptionPane.ERROR_MESSAGE);
			}
			
			// Generate OWL Ontology
			// TODO: Generate OWL on condition that XLSX generated
			// USE code from Mapping Tool
		}
		
	} // END OF ExtractMetadataActionListener CLASS
	
}
