package org.ntua.iccs.harmonicss.metadata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.ntua.iccs.harmonicss.metadata.model.ColumnData;
import org.ntua.iccs.harmonicss.metadata.model.FieldDesc;
import org.ntua.iccs.harmonicss.metadata.model.FileInfo;
import org.timchros.core.transformers.I_StringTransformer;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Checks;
import org.timchros.core.util.Util;

public class MetadataExtractionService {

	// TODO: Field Value Name (String)
	// TODO: Field Value Specific Number
	
	/**
	 * For testing purposes ...
	 */
	public static void main(String[] args) throws Exception {
		
		
		System.out.println(" >> MetadataExtractionService: Process STARTED at " + new Date() );
		System.out.println();
		
		// System.out.println(new MemoryMessage().toParsableString());

		// Read Input Data from Command LineS
		final String xlsFilePath, newXlsFilePath;
		if (args == null || args.length != 2) {
			System.out.println(" * Warning: No Data Provided - Default Arguments will be used *");
			System.out.println();
			xlsFilePath = "./TEST-DATA/Test-Data-Excel-File.xlsx";
			newXlsFilePath = "./TEST-DATA/Test-Data-Excel-File-Metadata.xlsx";	
		} else {
			xlsFilePath = args[0];
			newXlsFilePath = args[1];
		}
		
		System.out.println(" - Cohort Data XLS File Path:     " + xlsFilePath);
		System.out.println(" - Cohort Metadata XLS File Path: " + newXlsFilePath);
		System.out.println();
		
		// Check Given Data
		if (xlsFilePath.trim().equals("") || newXlsFilePath.trim().equals("")) {
			System.out.println("\n * At least one out of two input arguments is empty ! Please check.. "); return;
		}
		
		process(xlsFilePath, newXlsFilePath);
		
		System.out.println();
		System.out.println(" >> MetadataExtractionService: Process FINISHED at " + new Date() );			

	}
	
	public static void process(String inPath, String outPath) throws Exception {
		Checks.checkNotNullArg(inPath, "inPath", outPath, "outPath");
		
		// READ XLS FILE
		LogsArea.println("Reading Data from XLS File: " + inPath + " ...");
		final Map<Integer, Map<Integer, List<ColumnData>>> sheetMap;
		try {
			sheetMap = getDataFromXLS(inPath);
		} catch (Throwable t) {
			t.printStackTrace();
			LogsArea.println("Problem while reading XLS File: " + inPath);
			LogsArea.println("Error: " + t.getMessage());
			throw new RuntimeException("Cannot Read Document: " + new File(inPath).getName());
		}
		if (sheetMap.isEmpty()) throw new RuntimeException("None sheet exist in the Excel document !");
		
		final List<Entry<Integer, Map<Integer, List<ColumnData>>>> sheetEntryList = new ArrayList<Map.Entry<Integer,Map<Integer,List<ColumnData>>>>();
		for (Entry<Integer, Map<Integer, List<ColumnData>>> entry : sheetMap.entrySet()) {
			if (entry.getValue().size() == 0) continue;
			sheetEntryList.add(entry);
		}
		Collections.sort(sheetEntryList, new Comparator<Map.Entry<Integer, ?>>() {
			@Override public int compare(Entry<Integer, ?> e1, Entry<Integer, ?> e2) {
				return e1.getKey() - e2.getKey();
			}
		});
		
		// Find the Number of Rows and Columns per Sheet
		for (Entry<Integer, Map<Integer, List<ColumnData>>> sheetEntry : sheetEntryList) {
			int maxColumns=0;
			for (Entry<Integer, List<ColumnData>> entry : sheetEntry.getValue().entrySet()) {
				final List<ColumnData> columnDataList = entry.getValue();
				if (!columnDataList.isEmpty()) {
					final int columns = columnDataList.get(columnDataList.size()-1).getColumnNumber() + 1;
					if (columns > maxColumns) {
						maxColumns = columns;
					}
				}
			}
			LogsArea.println("Sheet: " + sheetEntry.getKey() + " ( " + ordinal(sheetEntry.getKey()) + " sheet ) --> Rows: " + sheetEntry.getValue().size() + ", Columns: " + maxColumns);
		}

		// EXAMINE OVERALL DATA STRUCTURE FOR EACH SHEET
		for (Entry<Integer, Map<Integer, List<ColumnData>>> sheetEntry : sheetEntryList) {
			final int sheetNumber = sheetEntry.getKey();
			final String sheetMsg = (sheetEntryList.size() > 1) ? ("Sheet " + sheetNumber + " ( " + ordinal(sheetNumber) + " sheet )" + " : ") : "";
			
			final Map<Integer, List<ColumnData>> sheetData = sheetEntry.getValue();
			if (sheetData.containsKey(0) && sheetData.get(0).size() > 0) {
				final List<ColumnData> fields = sheetData.get(0);
				final int emptyFieldsNumber = fields.get(fields.size()-1).getColumnNumber() + 1 - fields.size();
				if (emptyFieldsNumber > 0) {
					final String errorMsg = sheetMsg + "There are " + emptyFieldsNumber + " empty column(s) either at the beginning or among the data !";
					LogsArea.println();
					LogsArea.println("*** " + errorMsg + " ***"); 
					LogsArea.println();
					final List<Integer> indexList = new ArrayList<Integer>();
					int expColumnIndex = 0;
					for (ColumnData columnData : sheetData.get(0)) {
						if (columnData.getColumnNumber() == expColumnIndex) {
							expColumnIndex++;
						} else {
							for (int i = expColumnIndex; i < columnData.getColumnNumber(); i++) {
								indexList.add(i);
							}
							expColumnIndex = columnData.getColumnNumber() + 1;
						}
					}
					
					;
					
					LogsArea.println(sheetMsg + "Empty Column(s): " + Util.listToOneLineString(indexList, new I_StringTransformer<Integer>() {
						@Override public String asString(Integer n) {
							return n + "(" + numberToABC(n) + ")";
						}
					}));
					throw new RuntimeException(errorMsg + " See Logs Area for more information ...");
				}
			} else {
				final String errorMsg = sheetMsg + "The FIRST row is empty !";
				LogsArea.println();
				LogsArea.println("*** " + errorMsg + " *** "); 
				throw new RuntimeException(errorMsg);
			}
			
			if (sheetData.size() > 1) {
				final List<Integer> mapKeyList = new ArrayList<Integer>(sheetData.keySet());
				Collections.sort(mapKeyList);
				final int emptyRowsNumber = mapKeyList.get(mapKeyList.size()-1) + 1 - mapKeyList.size();
				if (emptyRowsNumber > 0) {	
					final String errorMsg = sheetMsg + "There are " + emptyRowsNumber + " empty row(s) among entity data or below fields !";
					LogsArea.println();
					LogsArea.println("*** " + errorMsg + " ***"); 
					LogsArea.println();	
					final List<Integer> indexList = new ArrayList<Integer>();
					final List<Integer> rowIndexList = new ArrayList<Integer>(sheetData.keySet());
					Collections.sort(rowIndexList);
					int expRowIndex = 0;
					for (Integer rowIndex : rowIndexList) {
						if (rowIndex == expRowIndex) {
							expRowIndex++;
						} else {
							for (int i = expRowIndex; i < rowIndex; i++) {
								indexList.add(i);
							}
							expRowIndex = rowIndex + 1;
						}
					}
					LogsArea.println(sheetMsg + "Empty Row(s): " + indexList);
					throw new RuntimeException(errorMsg + " See Logs Area for more information ...");	
				}
			} else {
				final String errorMsg = sheetMsg + "No entity data provided !";
				LogsArea.println();
				LogsArea.println("*** " + errorMsg + " *** "); 
				throw new RuntimeException(errorMsg);
			}
		}
		
		final  Map<Integer, List<ColumnData>> sheet1data = sheetEntryList.iterator().next().getValue();
		
		// IN CASE THE DOCUMENT HAS MORE THAN ONE SHEETS
		// THE NAME OF THE FIRST FIELD SHOULD BE THE SAME AND
		// ALL SHEETS SHOULD HAVE THE SAME NUMBER OF ROWS/PATIENTS
		if (sheetEntryList.size() > 1) {
			final int sheets = sheetEntryList.size();
			final String codeFieldName = sheet1data.get(0).get(0).getData();
			final int rowsNumber = sheet1data.size();
			for (int i = 1; i < sheetEntryList.size(); i++) {
				final Entry<Integer, Map<Integer, List<ColumnData>>> sheetEntry = sheetEntryList.get(i);
				final Map<Integer, List<ColumnData>> sheetDataMap = sheetEntry.getValue();
				if (!codeFieldName.equalsIgnoreCase( sheetDataMap.get(0).get(0).getData()) ) {
					throw new RuntimeException("The first field should be the main Entity UID and "
							+ "it should have exactly the same name in the " + sheets + " sheets (e.g., " + codeFieldName+ ").");
				}
				if (rowsNumber != sheetDataMap.size()) {
					throw new RuntimeException("All the sheets should contain the same number of rows !");
				}
			}
		}
		
		final File cohortFile = new File(inPath);
		final int patientNum = sheet1data.size() - 1;
		final Tuple2<List<FieldDesc>, Map<String, Set<String>>> tuple = processFieldsValues(sheetEntryList);
		saveInXLS(new FileInfo(cohortFile.getName(), sheetEntryList.size(), cohortFile.length(), patientNum), outPath, tuple._1(), tuple._2());
		LogsArea.println("File created: " + outPath);
	}
	
	private static final String ordinal(final int n) {
		if (n < 0) throw new RuntimeException("n = " + n + " < 0 !");
		final int number = n + 1;
		if (number == 1) return "1st";
		if (number == 1) return "2nd";
		if (number == 1) return "3rd";
		return number + "th";
	}
	
	/** * @return A {@link Map} with the Non-empty Fields per Row */
	@SuppressWarnings("resource")
	public static Map<Integer, Map<Integer, List<ColumnData>>> getDataFromXLS(final String xlsFilePath) throws Exception {
		if (xlsFilePath == null || xlsFilePath.trim().equals("")) 
			throw new RuntimeException("Parameter: xlsFilePath NOT specified !");
		if ( (!xlsFilePath.toLowerCase().endsWith(".xls")) && (!xlsFilePath.toLowerCase().endsWith(".xlsx"))) 
			throw new RuntimeException("File \"" + xlsFilePath + "\" is NOT an Excel Document !");
		
		final InputStream is = new FileInputStream( new File(xlsFilePath));
		if (xlsFilePath.toLowerCase().endsWith(".xls")) {
			return getDataFromXLS(is, "xls");
		} else if (xlsFilePath.toLowerCase().endsWith(".xlsx")) {
			return getDataFromXLS(is, "xlsx");
		} 
		return null;
	}
	
	public static Map<Integer, Map<Integer, List<ColumnData>>> getDataFromXLS(final InputStream is, final String xlsType) throws Exception {
		final Workbook workbook = 
			(xlsType.toLowerCase().equals("xls")) ? 
				new HSSFWorkbook(is) :
			(xlsType.toLowerCase().equals("xlsx")) ? 
				new XSSFWorkbook( is ) : null;

		final Map<Integer, Map<Integer, List<ColumnData>>> sheetMap = new HashMap<Integer, Map<Integer, List<ColumnData>>>();
		final Iterator<Sheet> sheetIt = workbook.sheetIterator();		
		while (sheetIt.hasNext()) {
			final Sheet sheet = ( Sheet) sheetIt.next();
			final String name = sheet.getSheetName();
			final int index = workbook.getSheetIndex(sheet);
			try {
				final Map<Integer, List<ColumnData>> rowFieldsMap = new HashMap<Integer, List<ColumnData>>();
				final Iterator<Row> rowIt = sheet.iterator();
				while (rowIt.hasNext()) {
				   	final Row row = rowIt.next();
				    final int rowNum = row.getRowNum();
				    final Iterator<Cell> cellIt = row.cellIterator();
				    while (cellIt.hasNext()) {
				      	final Cell cell = (Cell) cellIt.next();
						final int colNum = cell.getColumnIndex();
						final String cellData = getCellData(cell);
						if (cellData == null) continue;
						if (!rowFieldsMap.containsKey(rowNum)) {
							rowFieldsMap.put(rowNum, new ArrayList<ColumnData>());
						}
						rowFieldsMap.get(rowNum).add(new ColumnData(colNum, cellData));
				    } // END OF CELLs LOOP
				} // END OF ROWs LOOP
				sheetMap.put(index, rowFieldsMap);
			} catch (Throwable t) {
				throw new RuntimeException("Unexpected error while reading data from sheet: " + index + " :: " + name, t);
			}
			
		} // END OF SHEETs LOOP
		
	    workbook.close();
		return sheetMap;
	}
	
	private static final DateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
	private static final NumberFormat numFormater = new DecimalFormat("#.####");
	
	/** @return Data existing in a specific Cell of an Excel File in the form of a String */
	private static String getCellData(Cell cell) {
		if (cell == null || cell.toString().trim().equals("")) return null;
		if (cell.getCellTypeEnum() == CellType.STRING) {
			return cell.getStringCellValue().replaceAll("\\s+", " ").trim();
		} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				return dateFormater.format(cell.getDateCellValue());
		    } else {
		    	return numFormater.format(cell.getNumericCellValue());
		    }
		} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
			return ("" + cell.getBooleanCellValue()).toLowerCase();
		} else if (cell.getCellTypeEnum() == CellType.FORMULA) {
			return "FORMULA: " + cell;
		} else if (cell.getCellTypeEnum() == CellType.BLANK) {
			return null;
		} else if (cell.getCellTypeEnum() == CellType.ERROR) {
			return null;
		} else if (cell.getCellTypeEnum() == CellType._NONE) {
			System.out.println("NONE " + cell);
			return "NONE: " + cell;
		} else {
			System.out.println(" ****** " + cell);
			return cell.toString().trim();
		}
		
		
	}
	
	private static final char PLUS = '+';
	private static final char MINUS = '-';
	private static final char DOT = '.';
	private static final char COMMA = ',';
	private static final char SLASH = '/';
	
	
	private static boolean isStrFormula(final String str) {
		if (str == null || str.trim().equals("")) return false;
		return str.startsWith("FORMULA:");
	}
	
//	private static boolean isStrDate(final String str) {
//		return isStrDate(str, '/') || isStrDate(str, '-');
//	}
	
	private static boolean isStrDate1(final String str) {
		return isStrDate(str, '/');
	}
	
	private static boolean isStrDate2(final String str) {
		return isStrDate(str, '-');
	}
	
	private static boolean isStrDate(final String str, final char c) {
		if (str == null || str.trim().equals("")) return false;
		final int strLastIndex = str.lastIndexOf(c);
		if (strLastIndex <=0) {
			return strCanBeIntYear(str);
		} else {
			final String part1 = str.substring(0, strLastIndex);
			final String part2 = str.substring(strLastIndex+1, str.length());
			final int index= part1.indexOf(c);
			if (index <=0) {
				return strCanBeIntMonth(part1) && strCanBeIntYear(part2);
			} else {
				final String part1a = part1.substring(0, index);
				final String part1b = part1.substring(index+1, part1.length());
				return ((strCanBeIntMonth(part1a) && strCanBeIntDay(part1b))|| (strCanBeIntDay(part1a) && strCanBeIntMonth(part1b))) && strCanBeIntYear(part2);
			}
		}
	}

	private static boolean isStrDate_DayMonthYear1(final String str) {
		return isStrDate_DayMonthYear(str, '/');
	}
	private static boolean isStrDate_DayMonthYear2(final String str) {
		return isStrDate_DayMonthYear(str, '-');
	}
	
	private static boolean isStrDate_DayMonthYear(final String str, final char c) {
		if (str == null || str.trim().equals("")) return false;
		final int index1 = str.lastIndexOf(c);
		if (index1 <=0) return false;
		final String part1 = str.substring(0, index1);
		final String part2 = str.substring(index1+1, str.length());
		final int index2 = part1.lastIndexOf(c);
		if (index2 <=0) return false;
		final String part1a = part1.substring(0, index2);
		final String part1b = part1.substring(index2+1, part1.length());
		return strCanBeIntDay(part1a) && strCanBeIntMonth(part1b) && strCanBeIntYear(part2);
	}

	private static boolean isStrDate_MonthDayYear1(final String str) {
		return isStrDate_MonthDayYear(str, '/');
	}
	private static boolean isStrDate_MonthDayYear2(final String str) {
		return isStrDate_MonthDayYear(str, '-');
	}
	
	private static boolean isStrDate_MonthDayYear(final String str, final char c) {
		if (str == null || str.trim().equals("")) return false;
		final int index1 = str.lastIndexOf(c);
		if (index1 <=0) return false;
		final String part1 = str.substring(0, index1);
		final String part2 = str.substring(index1+1, str.length());
		final int index2 = part1.lastIndexOf(c);
		if (index2 <=0) return false;
		final String part1a = part1.substring(0, index2);
		final String part1b = part1.substring(index2+1, part1.length());
		return strCanBeIntMonth(part1a) && strCanBeIntDay(part1b) && strCanBeIntYear(part2);
	}
	
	private static boolean isStrDate_MonthYear1 (final String str) {
		return isStrDate_MonthYear(str, '/');
	}
	
	private static boolean isStrDate_MonthYear2 (final String str) {
		return isStrDate_MonthYear(str, '-');
	}
	
	private static boolean isStrDate_MonthYear(final String str, final char c) {
		if (str == null || str.trim().equals("")) return false;
		final int index = str.indexOf(c);
		if (index <=0) return false;
		final String part1 = str.substring(0, index);
		final String part2 = str.substring(index+1, str.length());
		return strCanBeIntMonth(part1) && strCanBeIntYear(part2);
	}
	
	private static boolean isStrDate_Year(final String str) {
		return strCanBeIntYear(str);
	}
	
	/** * @return <code>true</code> if the given String is a Date between 1900 and 2025 */
	private static boolean strCanBeIntYear(final String str) {
		if (str == null || str.trim().equals("")) return false;
		if (!str.matches("^[0-9]{4}$")) return false;
		try {
			final int num = Integer.parseInt(str);
			return ((num > 1900) && (num < 2025));
		} catch (Throwable t) {
			return false;
		}
	}
	
	public static boolean strCanBeIntMonth(final String str) {
		if (str == null || str.trim().equals("")) return false;
		if (str.length() > 2 || !str.matches("\\d+")) return false;
		try {
			int n = Integer.parseInt(str);
			return (n > 0 && n <= 12);
		} catch (Throwable t) {
			return false;
		}
	}
	
	public static boolean strCanBeIntDay(final String str) {
		if (str == null || str.trim().equals("")) return false;
		if (str.length() > 2 || !str.matches("\\d+")) return false;
		try {
			int n = Integer.parseInt(str);
			return (n > 0 && n <= 31);
		} catch (Throwable t) {
			return false;
		}
	}
	
	
	/** * @return <code>true</code> if the given String is an Integer */
	private static boolean isStrInteger(final String str) {
		if (str == null || str.trim().equals("")) return false;
		if (str.charAt(0) == PLUS || str.charAt(0) == MINUS) return str.substring(1, str.length()).matches("\\d+");
		return str.matches("\\d+");
	}
	
	/** * @return <code>true</code> if the given String is a Fraction such as 5/4 */
	private static boolean isStrFraction(final String str) {
		if (str == null || str.trim().equals("")) return false;
		final int index = str.indexOf(SLASH);
		if (index <= 0 || index == str.length()-1) return false;
		final String str1 = str.substring(0, index);
		final String str2 = str.substring(index+1, str.length());
		if (!isStrUnsignedInteger(str1)) return false;
		if (!isStrUnsignedInteger(str2)) return false;
		return true;
	}
	
	/** * @return <code>true</code> if the given String is an Unsigned Integer */
	private static boolean isStrUnsignedInteger(final String str) {
		if (str == null || str.trim().equals("")) return false;
		return str.matches("\\d+");
	}
	
	/** * @return <code>true</code> if the given String is a Numeric Value such as +5.4 or -3,000.45 */
	private static boolean isStrNumericValue(final String str) {
		if (str == null || str.trim().equals("")) return false;
		if (str.trim().indexOf(' ') > 0) return false;
		final String newStr = str.replace(COMMA, ' ').replace(DOT, ' ').replace(PLUS, ' ').replace(MINUS, ' ').replaceAll("\\s+", "");
		if (newStr == null || newStr.equals("")) return false;
		return isStrUnsignedInteger(newStr);
	}
	
	@SuppressWarnings("unused")
	private static final int countCharacter(String str, char c) {
		int count = 0;
		if (str == null) return count;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == c) count++;
		}
		return count;
	}
	
	
	/*** @return A Float number on condition that the string represents a numeric value */
	private static float getFloatValue(final String str) {
		try {
			if (!isStrNumericValue(str)) throw new RuntimeException("Str: " + str + " is not a numeric value.");
			final int puncIndex = Math.max(str.lastIndexOf('.'), str.lastIndexOf(','));
			if (puncIndex < 0) return Float.parseFloat(str);
			String intStr = str.substring(0, puncIndex);
			intStr = intStr.replace(DOT, ' ').replace(COMMA, ' ').replaceAll("\\s+", "");
			final String decStr = str.substring(puncIndex + 1, str.length());
			final String newStr = intStr + "." + decStr;
			return Float.parseFloat(newStr);
		} catch (Throwable t) {
			throw new RuntimeException("Error while processing number: " + str, t);
		}
	}

	public static Tuple2<List<FieldDesc>, Map<String, Set<String>>> processFieldsValues(List<Entry<Integer, Map<Integer, List<ColumnData>>>> sheetEntryList) {
		Checks.checkNotNullArg(sheetEntryList, "sheetEntryList");


		// sheetEntryList
		final Map<Integer, List<ColumnData>> sheet1data = sheetEntryList.get(0).getValue();
		
		final int MaxIntN = 10;
		
		int N=0;
		for (Entry<Integer, List<ColumnData>> entry : sheet1data.entrySet()) {
			final List<ColumnData> columnDataList = entry.getValue();
			if (!columnDataList.isEmpty()) {
				final int rowN = columnDataList.get(columnDataList.size()-1).getColumnNumber() + 1;
				if (rowN > N) {
					N = rowN;
				}
			}
		}
		
		// Search for Empty Fields per Sheet
		final Set<String> emptyColumnSet = new HashSet<String>();
		for (Entry<Integer, Map<Integer, List<ColumnData>>> sheetEntry : sheetEntryList) {
			final int sheetID = sheetEntry.getKey();
			final Map<Integer, List<ColumnData>> sheetData = sheetEntry.getValue();
			for (Entry<Integer, List<ColumnData>> entry : sheetData.entrySet()) {
				if (entry.getKey() == 0) continue;
				final Set<Integer> columnSet = new HashSet<Integer>();
				for (ColumnData columnData : entry.getValue()) {
					columnSet.add(columnData.getColumnNumber());
				}
				for (int i = 0; i < N; i++) {
					if (!columnSet.contains(i)) emptyColumnSet.add( combine(sheetID, i) );
				}
			}
		}
		
		// Organize Terms/String per Column/Field
		final Map<String, List<String>> colStrListMap = new HashMap<String, List<String>>();
		for (Entry<Integer, Map<Integer, List<ColumnData>>> sheetEntry : sheetEntryList) {
			final int sheetID = sheetEntry.getKey();
			final Map<Integer, List<ColumnData>> sheetData = sheetEntry.getValue();
			for (Entry<Integer, List<ColumnData>> entry : sheetData.entrySet()) {
				if (entry.getKey() == 0) continue;
				for (ColumnData columnData : entry.getValue()) {
					final int columnNumber = columnData.getColumnNumber();
					final String uid = combine(sheetID, columnNumber);
					if (!colStrListMap.containsKey(uid)) {
						colStrListMap.put(uid, new ArrayList<String>());
					}
					colStrListMap.get(uid).add(columnData.getData());
				}
			}
		}
		
		// Detect Distinct Examples
		final Set<String> multiValueFieldsSet = new HashSet<String>();
		final Map<String, Set<String>> mapSet = new HashMap<String, Set<String>>();
        int maxSetSize = 0;
        for (Entry<String, List<String>> entry : colStrListMap.entrySet()) {
        	//if (entry.getKey()  == 0) continue; 
        	if (entry.getKey().endsWith("-0")) continue; 
        	
        	final Set<String> set = new HashSet<String>();
        	for (String str : entry.getValue()) {
        		if (str.startsWith("FORMULA:"))  {
        			set.add(str);
        		} else  if (isStrNumericValue(str)) {
        			set.add(str);
        		} else {
        			if (str.indexOf(',') > 0) {
	        			multiValueFieldsSet.add(entry.getKey());
	        			for (String token : str.split(",")) {
	        				final String tokenTrim = token.trim();
	        				if (tokenTrim.equals("")) continue;
	        				set.add(tokenTrim);
						}
	        		} else {
	        			set.add(str);
	        		}
        		}
			}
			mapSet.put(entry.getKey(), set);
			
			if (set.size() > maxSetSize) maxSetSize = set.size();
		}
		
        final Set<String> ignFieldsSet = new HashSet<String>();
        
        // TODO: Set with numeric value fields ID
        
		// Search for Datatype
        final Map<String, String> datatypeMap = new HashMap<String, String>();
        datatypeMap.put(combine(0, 0), "STRING");
        final Map<String, String> rangeMap = new HashMap<String, String>();
        for (Entry<String, Set<String>> entry : mapSet.entrySet()) {
        	boolean formulaFlag = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrFormula(str)) {
					formulaFlag = false; break;
				}
			}
			if (formulaFlag) {
				ignFieldsSet.add(entry.getKey());
				datatypeMap.put(entry.getKey(), "FORMULA"); 
				final List<String> exampleList = getExamplesFromList(entry.getValue());
				if (entry.getValue().size() == 1) {
					rangeMap.put(entry.getKey(), "Value: " + exampleList.get(0));
				} else if (entry.getValue().size() > 1) {
					final String prefix = 
						(entry.getValue().size() > MAX_EXAMPLES_NUMBER) ? 
							"Examples: " : 
							("One " + ((multiValueFieldsSet.contains(entry.getKey()) ? "or more " : "")) + "of: ");
					rangeMap.put(entry.getKey(), prefix + exampleList);
				}
				continue;
			}
        	
        	
        	// Check DATE - YEAR
        	boolean dateYearValuesFlag = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrDate_Year(str)) {
					dateYearValuesFlag = false; break;
				}
			}
			if (dateYearValuesFlag) {
				ignFieldsSet.add(entry.getKey());
				
				datatypeMap.put(entry.getKey(), "DATE"); 
				int min = Integer.MAX_VALUE;
				int max = Integer.MIN_VALUE;
				for (String str : entry.getValue()) {
					int n = Integer.parseInt(str);
					if (n < min) min = n;
					if (n > max) max = n;
				}
				if (max > min) {
					rangeMap.put(entry.getKey(), "Format: YEAR, In Range: [ " + min + " , " + max + " ]");
				} else if (max == min) {
					rangeMap.put(entry.getKey(), "Format: YEAR, Value: " + min);
				} 
				
				continue;
			}
			

			boolean dateMonthYearFlag1 = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrDate_MonthYear1(str)) {
					dateMonthYearFlag1 = false; break;
				}
			}
			if (dateMonthYearFlag1) {
				ignFieldsSet.add(entry.getKey());
				datatypeMap.put(entry.getKey(), "DATE"); 
				rangeMap.put(entry.getKey(), "Format: MONTH/YEAR");
				continue;
			}
			boolean dateMonthYearFlag2 = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrDate_MonthYear2(str)) {
					dateMonthYearFlag2 = false; break;
				}
			}
			if (dateMonthYearFlag2) {
				ignFieldsSet.add(entry.getKey());
				datatypeMap.put(entry.getKey(), "DATE"); 
				rangeMap.put(entry.getKey(), "Format: MONTH-YEAR");
				continue;
			}
			
			boolean dateDayMonthYearFlag1 = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrDate_DayMonthYear1(str)) {
					dateDayMonthYearFlag1 = false; break;
				}
			}
			if (dateDayMonthYearFlag1) {
				ignFieldsSet.add(entry.getKey());
				datatypeMap.put(entry.getKey(), "DATE"); 
				rangeMap.put(entry.getKey(), "Format: DAY/MONTH/YEAR");
				continue;
			}
			boolean dateDayMonthYearFlag2 = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrDate_DayMonthYear2(str)) {
					dateDayMonthYearFlag2 = false; break;
				}
			}
			if (dateDayMonthYearFlag2) {
				ignFieldsSet.add(entry.getKey());
				datatypeMap.put(entry.getKey(), "DATE"); 
				rangeMap.put(entry.getKey(), "Format: DAY-MONTH-YEAR");
				continue;
			}
			
			boolean dateMonthDayYearFlag1 = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrDate_MonthDayYear1(str)) {
					dateMonthDayYearFlag1 = false; break;
				}
			}
			if (dateMonthDayYearFlag1) {
				ignFieldsSet.add(entry.getKey());
				datatypeMap.put(entry.getKey(), "DATE"); 
				rangeMap.put(entry.getKey(), "Format: MONTH/DAY/YEAR");
				continue;
			}
			boolean dateMonthDayYearFlag2 = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrDate_MonthDayYear2(str)) {
					dateMonthDayYearFlag2 = false; break;
				}
			}
			if (dateMonthDayYearFlag2) {
				ignFieldsSet.add(entry.getKey());
				datatypeMap.put(entry.getKey(), "DATE"); 
				rangeMap.put(entry.getKey(), "Format: MONTH-DAY-YEAR");
				continue;
			}
			
			boolean dateFlag1 = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrDate1(str)) {
					dateFlag1 = false; break;
				}
			}
			if (dateFlag1) {
				ignFieldsSet.add(entry.getKey());
				datatypeMap.put(entry.getKey(), "DATE"); 
				rangeMap.put(entry.getKey(), "Date Precision varies ! day (if any), month (if any), year seprated by SLASH.");
				continue;
			}
			boolean dateFlag2 = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrDate2(str)) {
					dateFlag2 = false; break;
				}
			}
			if (dateFlag2) {
				ignFieldsSet.add(entry.getKey());
				datatypeMap.put(entry.getKey(), "DATE"); 
				rangeMap.put(entry.getKey(), "Date Precision varies ! day (if any), month (if any), year seprated by DASH.");
				continue;
			}
			
			
			// Check INTEGER
			boolean intFlag = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrInteger(str)) {
					intFlag = false; break;
				}
			}
			if (intFlag) {
				// Ignore only when there are many
				if (entry.getValue().size() >= MaxIntN)	{
					ignFieldsSet.add(entry.getKey());
					// TODO: add value to set
				}
				
				datatypeMap.put(entry.getKey(), "INTEGER"); 
				int min = Integer.MAX_VALUE;
				int max = Integer.MIN_VALUE;
				for (String str : entry.getValue()) {
					int n = Integer.parseInt(str);
					if (n < min) min = n;
					if (n > max) max = n;
				}
				if (max > min) {
					rangeMap.put(entry.getKey(), "In Range: [" + min + "," + max + "]: " + entry.getValue().size() + " different terms.");
				} else if (max == min) {
					rangeMap.put(entry.getKey(), "Value: " + min);
				} 
				continue;
			}
			
			// Check Real Number
			boolean realNumberFlag = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrNumericValue(str)) {
					realNumberFlag = false; break;
				}
			}
			if (realNumberFlag) {
				ignFieldsSet.add(entry.getKey());
				
				datatypeMap.put(entry.getKey(), "REAL NUMBER"); 
				rangeMap.put(entry.getKey(), "There are " + entry.getValue().size() + " different Numeric Values.");
				
				//if (entry.getKey() == 0) System.out.println("000000 " + entry.getValue().size());
				
				try {	
					Float min = Float.MAX_VALUE;
					Float max = Float.MIN_VALUE;
					for (String str : entry.getValue()) {
						Float n = getFloatValue(str);
						if (n.floatValue() < min.floatValue()) min = n;
						if (n.floatValue() > max.floatValue()) max = n;
					}
					if (max.floatValue() > min.floatValue()) {
						rangeMap.put(entry.getKey(), "In Range: [ " + min + " , " + max + " ]: " + entry.getValue().size() + " different terms.");
					} else if (max == min) {
						rangeMap.put(entry.getKey(), "Value: " + min);
					} 
				} catch (Throwable t) { 
//					System.out.println("Column: " + entry.getKey() + " --> " + numberToABC(entry.getKey()));
//					for (ColumnData col : dataMap.get(0)) {
//						if (col.getColumnNumber() == entry.getKey()) {
//							System.out.println("Label: " + col.getData());
//							break;
//						}
//					}
//					t.printStackTrace();
				}
				continue;
			}
			
			// Check Fraction
			boolean fractionFlag = (entry.getValue().size() > 0);
			for (String str : entry.getValue()) {
				if (!isStrFraction(str)) {
					fractionFlag = false; break;
				}
			}
			if (fractionFlag) {
				datatypeMap.put(entry.getKey(), "FRACTION"); 
				
				ignFieldsSet.add(entry.getKey());
				
				final List<String> exampleList = getExamplesFromList(entry.getValue());
				if (entry.getValue().size() == 1) {
					rangeMap.put(entry.getKey(), "Value: " + exampleList.get(0));
				} else if (entry.getValue().size() > 1) {
					final String prefix = 
						(entry.getValue().size() > MAX_EXAMPLES_NUMBER) ? 
							"Examples: " : 
							("One " + ((multiValueFieldsSet.contains(entry.getKey()) ? "or more " : "")) + "of: ");
					rangeMap.put(entry.getKey(), prefix + exampleList);
				}
				continue;
			}
			
			// TODO: +++
			
			datatypeMap.put(entry.getKey(), "STRING"); 
			
			final List<String> exampleList = getExamplesFromList(entry.getValue());
			if (entry.getValue().size() == 1) {
				rangeMap.put(entry.getKey(), "Value: " + exampleList.get(0));
			} else if (entry.getValue().size() > 1) {
				final String prefix = 
					(entry.getValue().size() > MAX_EXAMPLES_NUMBER) ? 
						"Examples: " : 
						("One " + ((multiValueFieldsSet.contains(entry.getKey()) ? "or more " : "")) + "of: ");
				rangeMap.put(entry.getKey(), prefix + exampleList);
			} else {
				rangeMap.put(entry.getKey(), "No value(s) in this field for all entities.");
			}
			
		}
        
        final Set<String> noteWarnSet = new HashSet<String>();
        for (String index : emptyColumnSet) {
			if (!mapSet.containsKey(index)) {
				datatypeMap.put(index, "-"); 
				rangeMap.put(index, "Always Empty !");
				noteWarnSet.add(index);
			}
		}
        for (Entry<String, String> entry : rangeMap.entrySet()) {
			if (entry.getValue().startsWith("Value: ")) noteWarnSet.add(entry.getKey());
		}
        
        // Undetected Empty Fields in the Sheets
        for (Entry<Integer, Map<Integer, List<ColumnData>>> sheetEntry : sheetEntryList) {
        	final int sheetID = sheetEntry.getKey();
			final Map<Integer, List<ColumnData>> sheetData = sheetEntry.getValue();
			for (ColumnData columnData : sheetData.get(0)) {
				if (sheetID > 0 && columnData.getColumnNumber() == 0) continue;
				final Integer column = columnData.getColumnNumber();
				final String uid = combine(sheetID, column);
				if (!colStrListMap.containsKey(uid)) {
					datatypeMap.put(uid, "-");
					emptyColumnSet.add(uid);
					rangeMap.put(uid, "ALWAYS EMPTY !");
					noteWarnSet.add(uid);
				}
			}
        }
        
        // Fields
        final List<FieldDesc> fieldsDescList = new ArrayList<FieldDesc>();
        for (Entry<Integer, Map<Integer, List<ColumnData>>> sheetEntry : sheetEntryList) {
			final int sheetID = sheetEntry.getKey();
			final Map<Integer, List<ColumnData>> sheetData = sheetEntry.getValue();
			for (ColumnData columnData : sheetData.get(0)) {
				if (sheetID > 0 && columnData.getColumnNumber() == 0) continue;
				
				final Integer column = columnData.getColumnNumber();
				final String uid = combine(sheetID, column);
				final String data = (columnData.getData() != null) ? columnData.getData().trim() : null;
				final FieldDesc fieldDesc = new FieldDesc(sheetID, 0, column, data);
				fieldDesc.setDatatype(datatypeMap.get(uid));
				fieldDesc.setNullable(emptyColumnSet.contains(uid));
				fieldDesc.setHasMultipleValues(multiValueFieldsSet.contains(uid));
				if (rangeMap.containsKey(uid)) fieldDesc.setNotes(rangeMap.get(uid));
				fieldDesc.setNoteWarning(noteWarnSet.contains(uid));
				fieldsDescList.add(fieldDesc);
			}
        }
	
		// Distinct Terms
		final Map<String, Set<String>> colValuesMap = new HashMap<String, Set<String>>();
		for (Entry<String, Set<String>> entry : mapSet.entrySet()) {
			if (ignFieldsSet.contains(entry.getKey())) continue;
			colValuesMap.put(entry.getKey(), entry.getValue());
		}
		
		// TODO: Return also the set
		
		return new Tuple2<List<FieldDesc>, Map<String,Set<String>>>(fieldsDescList, colValuesMap);
        
	} // END OF 
	
	private static final char SEP = '-';
	
	private static String combine(final Object sheetNumber, final Object columnIndex) {
		return "" + sheetNumber + SEP + columnIndex; 
	}
	
	private static Tuple2<Integer, Integer> splitStrInt(final String str) {
		int index = str.indexOf(SEP);
		if (index < 0) throw new RuntimeException("Cannot find character \"" + SEP + "\" in string: " + str);
		if (index == 0 || index == str.length() -1) throw new RuntimeException("Character \"" + SEP + "\" NOT in inside the string: " + str);
		final String str1 = str.substring(0,  index);
		final String str2 = str.substring(index + 1,  str.length());
		return new Tuple2<Integer, Integer>(Integer.parseInt(str1), Integer.parseInt(str2));
	}
	
	private static final int MAX_EXAMPLES_NUMBER = 6;
	
	private static List<String> getExamplesFromList(Set<String> set) {
		final List<String> setList = new ArrayList<String>(set);
		final List<String> exampleTermList = new ArrayList<String>();
		for (int i = 0; i < Math.min(MAX_EXAMPLES_NUMBER, setList.size()); i++) {
			String str = setList.get(i);
			exampleTermList.add(str);
		}
		return exampleTermList;
	}
	
	private static final String  dateFormatterPattern = "yyyy-MM-dd";
	private static final SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormatterPattern);
	
	/** Create an Excel Document with the data provided */
	@SuppressWarnings("deprecation")
	public static void saveInXLS(final FileInfo cohortFile, final String xlsFilePath, final List<FieldDesc> fieldDescList, final Map<String, Set<String>> colValuesMap) throws Exception {
		Checks.checkNotNullArg(xlsFilePath, "xlsFilePath", fieldDescList, "fieldDescList", colValuesMap, "colValuesMap");
		
		final Workbook workbook = new XSSFWorkbook();
		        
        // FONT
        final Font boldFont = workbook.createFont();
        boldFont.setBold(true);
        final Font grayBoldFont = workbook.createFont();
        grayBoldFont.setColor(HSSFColor.GREY_40_PERCENT.index);
        grayBoldFont.setBold(true);
        final Font redFont = workbook.createFont();
        redFont.setColor(HSSFColor.DARK_RED.index);
        final Font blueFont = workbook.createFont();
        blueFont.setColor(HSSFColor.DARK_BLUE.index);
        final Font greenFont = workbook.createFont();
        greenFont.setColor(HSSFColor.DARK_GREEN.index);
        
        // CELL FORMAT
        final CellStyle headerStyleA = workbook.createCellStyle();
        headerStyleA.setFillBackgroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        headerStyleA.setFillPattern(FillPatternType.SPARSE_DOTS);
        headerStyleA.setFont(boldFont);
        headerStyleA.setWrapText(true);
        headerStyleA.setBorderBottom(BorderStyle.THICK);
        headerStyleA.setVerticalAlignment(VerticalAlignment.CENTER);
        
        final CellStyle cellIndexStyle = workbook.createCellStyle();
        cellIndexStyle.setAlignment(HorizontalAlignment.LEFT);
        cellIndexStyle.setBorderRight(BorderStyle.THIN);
        
        final CellStyle leftBoarderStyle = workbook.createCellStyle();
        leftBoarderStyle.setBorderLeft(BorderStyle.THIN);

        final CellStyle boardersStyle = workbook.createCellStyle();
        boardersStyle.setAlignment(HorizontalAlignment.CENTER);
        boardersStyle.setBorderLeft(BorderStyle.THIN);
        boardersStyle.setBorderRight(BorderStyle.THIN);
        
        final CellStyle redColorStyle = workbook.createCellStyle();
        redColorStyle.setFont(redFont);
        final CellStyle blueColorStyle = workbook.createCellStyle();
        blueColorStyle.setFont(blueFont);
        final CellStyle greenColorStyle = workbook.createCellStyle();
        greenColorStyle.setFont(greenFont);
        
        final CellStyle valueHeaderStyle = workbook.createCellStyle();
        valueHeaderStyle.setFillBackgroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        valueHeaderStyle.setFillPattern(FillPatternType.SPARSE_DOTS);

        final CellStyle boldStyle = workbook.createCellStyle();
        boldStyle.setAlignment(HorizontalAlignment.LEFT);
        boldStyle.setFont(boldFont);
        
        final CellStyle grayBoldStyle = workbook.createCellStyle();
        grayBoldStyle.setAlignment(HorizontalAlignment.LEFT);
        grayBoldStyle.setFont(grayBoldFont);
        
        valueHeaderStyle.setFont(boldFont);
        valueHeaderStyle.setWrapText(true);
        valueHeaderStyle.setBorderBottom(BorderStyle.THICK);
        valueHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        
        final CellStyle headerStyleLeft = workbook.createCellStyle();
        headerStyleLeft.cloneStyleFrom(valueHeaderStyle);
        headerStyleLeft.setBorderLeft(BorderStyle.THIN);
        
        final CellStyle headerStyleRight = workbook.createCellStyle();
        headerStyleRight.cloneStyleFrom(valueHeaderStyle);
        headerStyleRight.setBorderRight(BorderStyle.THIN);
        
        final CellStyle valueStyle = workbook.createCellStyle();
        valueStyle.setAlignment(HorizontalAlignment.LEFT);
        valueStyle.setBorderLeft(BorderStyle.THIN);
        valueStyle.setFont(blueFont);
        
        final CellStyle descStyle = workbook.createCellStyle();
        descStyle.setAlignment(HorizontalAlignment.LEFT);
        descStyle.setBorderRight(BorderStyle.THIN);
        
        final CellStyle upBoarderStyle = workbook.createCellStyle();
        upBoarderStyle.setBorderTop(BorderStyle.THIN);
        
        /*
		 *  PART 0: Overview
		 */
        final Sheet introSheet = workbook.createSheet("Overview");
        
        final Row headerRow = introSheet.createRow(0);
        headerRow.setHeight((short)600);
        final Cell headerCell1 = headerRow.createCell(0);
        headerCell1.setCellStyle(headerStyleA);
        headerCell1.setCellValue("Cohort Overview");
        final Cell headerCell2 = headerRow.createCell(1);
        headerCell2.setCellStyle(headerStyleA);

        final Row infoRow = introSheet.createRow(2);
        final Cell infoCell = infoRow.createCell(0);
        infoCell.setCellValue("This document contain the Name of Fields and the different Values detected for each one (Controlled Set of Terms).");
        
        final Row fileNameRow = introSheet.createRow(4);
        addRowNameValue(fileNameRow, boldStyle, "FileName:", "" + cohortFile.getName());

        final Row sheetsNumRow = introSheet.createRow(5);
        addRowNameValue(sheetsNumRow, boldStyle, "Number of Sheets:", "" + cohortFile.getSheetsNumber());
        
        final Row fileSizeRow = introSheet.createRow(6);
        addRowNameValue(fileSizeRow, boldStyle, "Size (bytes):", "" + cohortFile.getBytes());
        
        final Row patientNumberRow = introSheet.createRow(7);
        addRowNameValue(patientNumberRow, boldStyle, "Rows Number:", "" + cohortFile.getPatientsNumber());
        
        final Row numFieldRow = introSheet.createRow(8);
        addRowNameValue(numFieldRow, boldStyle, "Number of Fields:", "" + fieldDescList.size());
        
        final Row numSetTermsRow = introSheet.createRow(9);
        addRowNameValue(numSetTermsRow, boldStyle, "Sets (of Terms):", "" + colValuesMap.size());
        
        
        final Row dateRow = introSheet.createRow(11);
        addRowNameValue(dateRow, boldStyle, "Date Metadata File Created:", dateFormatter.format(new Date()));
        
        final Row devInfoRow1 = introSheet.createRow(13);
        final Cell devInfoCell1 = devInfoRow1.createCell(0);
        devInfoCell1.setCellValue("File automatically generated by the Cohort Metadate Extraction tool developed by");
        final Row devInfoRow2 = introSheet.createRow(14);
        final Cell devInfoCell2 = devInfoRow2.createCell(0);
        devInfoCell2.setCellValue("ICCS/NTUA within European Project HarmonicSS.");
        
        final Row versionRow1 = introSheet.createRow(16);
        addRowNameValue(versionRow1, grayBoldStyle, "Version:", "1.01");
        final Row versionRow2 = introSheet.createRow(17);
        addRowNameValue(versionRow2, grayBoldStyle, "Release Date:", "2024-04-02");
        
        
        final Row headerRowB1 = introSheet.createRow(19);
        headerRowB1.setHeight((short)600);
        final Cell headerCellB1 = headerRowB1.createCell(0);
        headerCellB1.setCellStyle(headerStyleA);
        headerCellB1.setCellValue("Additional Notes");
        final Cell headerCellB2 = headerRowB1.createCell(1);
        headerCellB2.setCellStyle(headerStyleA);
        
        final Row infoRowB = introSheet.createRow(21);
        final Cell infoCellB = infoRowB.createCell(0);
        infoCellB.setCellValue("In the following rows/cells you can provide additional notes, if being necessary.");
        
        introSheet.setColumnWidth(0, 10000);
        introSheet.setColumnWidth(1, 18000);
        
        /*
		 *  PART 1: Creation of an Excel Sheet with the Name of the Fields (parameters)
		 */
        final Sheet paramsSheet = workbook.createSheet("Fields-Name");
        
        final Row paramHeaderRow = paramsSheet.createRow(0);
        addRowFields(paramHeaderRow, "ID", "CATEGORY", "SUB-CATEGORY", "FIELD NAME", "FIELD DESCRIPTION", "DATA TYPE", "VALUES (NOTES/CONSTRAINTS)", "CAN BE EMPTY", "HAS MANY VALUES");
        paramHeaderRow.setHeight((short)600);
        for (Cell cell : paramHeaderRow) {
        	final int index = cell.getColumnIndex();
        	if (index == 0) {
        		final CellStyle style = workbook.createCellStyle();
				style.cloneStyleFrom(headerStyleA);
				style.setBorderRight(BorderStyle.THIN);
        		cell.setCellStyle(style);
        	} else if (index == 3) {
        		final CellStyle style = workbook.createCellStyle();
				style.cloneStyleFrom(headerStyleA);
				style.setBorderLeft(BorderStyle.THIN);
        		cell.setCellStyle(style);
        	} else if (index == 7 || index == 8) { 
        		final CellStyle style = workbook.createCellStyle();
				style.cloneStyleFrom(headerStyleA);
				style.setAlignment(HorizontalAlignment.CENTER);
				style.setBorderLeft(BorderStyle.THIN);
				style.setBorderRight(BorderStyle.THIN);
        		cell.setCellStyle(style);
        	} else {
        		cell.setCellStyle(headerStyleA);
        	}
		}
        
        int nameMaxCharsNum = 0;
        int paramRowIndex = 1;
        for (FieldDesc data : fieldDescList) {
			final Row row = paramsSheet.createRow(paramRowIndex++);
			final Cell indexCell = row.createCell(0);
			final String columnIndex = 
				(cohortFile.getSheetsNumber() == 1) 
    				? numberToABC(data.getColNum()) 
    				: combine(data.getSheetNum() , numberToABC(data.getColNum()) );
			indexCell.setCellValue(columnIndex);
			indexCell.setCellStyle(cellIndexStyle);
			
			final boolean isFieldDate = data.isDate();
			final boolean isFieldRealNumber = data.isRealNumber();
			
			final Cell nameCell = row.createCell(3);
			nameCell.setCellValue(data.getName());
			nameCell.setCellStyle(leftBoarderStyle);
			if (isFieldDate) {
				final CellStyle style = workbook.createCellStyle();
				style.cloneStyleFrom(greenColorStyle);
				style.setBorderLeft(BorderStyle.THIN);
				nameCell.setCellStyle(style);
			}
			if (isFieldRealNumber) {
				final CellStyle style = workbook.createCellStyle();
				style.cloneStyleFrom(blueColorStyle);
				style.setBorderLeft(BorderStyle.THIN);
				nameCell.setCellStyle(style);
			}
			
			if (!"STRING".equals(data.getDatatype())) {
				final Cell datatypeCell = row.createCell(5);
				datatypeCell.setCellValue(data.getDatatype());
				if (data.isNoteWarning()) datatypeCell.setCellStyle(redColorStyle);
				if (isFieldDate) datatypeCell.setCellStyle(greenColorStyle);
				if (isFieldRealNumber) datatypeCell.setCellStyle(blueColorStyle);
			}
			
			if (data.getNotes() != null) {
				final Cell commentsCell = row.createCell(6);
				commentsCell.setCellValue(data.getNotes());
				if (data.isNoteWarning()) commentsCell.setCellStyle(redColorStyle);
				if (isFieldDate) commentsCell.setCellStyle(greenColorStyle);
				if (isFieldRealNumber) commentsCell.setCellStyle(blueColorStyle);
			}

			final Cell emptynessCell = row.createCell(7);
			if (data.isNullable()) emptynessCell.setCellValue("YES");
			emptynessCell.setCellStyle(boardersStyle);
			if (isFieldDate) {
				final CellStyle style = workbook.createCellStyle();
				style.cloneStyleFrom(greenColorStyle);
				style.setAlignment(HorizontalAlignment.CENTER);
				style.setBorderLeft(BorderStyle.THIN);
				style.setBorderRight(BorderStyle.THIN);
				emptynessCell.setCellStyle(style);
			}
			if (isFieldRealNumber) {
				final CellStyle style = workbook.createCellStyle();
				style.cloneStyleFrom(blueColorStyle);
				style.setAlignment(HorizontalAlignment.CENTER);
				style.setBorderLeft(BorderStyle.THIN);
				style.setBorderRight(BorderStyle.THIN);
				emptynessCell.setCellStyle(style);
			}

			
			final Cell multipleValueCell = row.createCell(8);
			if (data.isHasMultipleValues()) multipleValueCell.setCellValue("YES");
			multipleValueCell.setCellStyle(boardersStyle);
			if (isFieldRealNumber) {
				final CellStyle style = workbook.createCellStyle();
				style.cloneStyleFrom(redColorStyle);
				style.setAlignment(HorizontalAlignment.CENTER);
				style.setBorderLeft(BorderStyle.THIN);
				style.setBorderRight(BorderStyle.THIN);
				multipleValueCell.setCellStyle(style);
			}
			
			if (data.getName().length() > nameMaxCharsNum) nameMaxCharsNum = data.getName().length();
		}
        
        if (nameMaxCharsNum > 200) nameMaxCharsNum = 200;
        
        final Row row = paramsSheet.createRow(paramRowIndex);
        final CellStyle style = workbook.createCellStyle();
        style.setBorderTop(BorderStyle.THICK);
        for (int i = 0; i <= 8; i++) {
        	final Cell cell = row.createCell(i);
			cell.setCellStyle(style);
		}
        
        paramsSheet.setColumnWidth(0, 1400);
        paramsSheet.setColumnWidth(1, 5500);
        paramsSheet.setColumnWidth(2, 5500);
        paramsSheet.setColumnWidth(3, 200 * nameMaxCharsNum);
        paramsSheet.setColumnWidth(4, 18000);
        paramsSheet.setColumnWidth(5, 5000);
        paramsSheet.setColumnWidth(6, 16000);
        paramsSheet.setColumnWidth(7, 2500);
        paramsSheet.setColumnWidth(8, 2500);
        
		/*
		 *  PART 2: Creation of an Excel Sheet with the Distinct Terms found for each Field
		 */
        
        final Sheet valuesSheet = workbook.createSheet("Fields-Values");
        
        final List<Entry<String, Set<String>>> entryList = new ArrayList<Map.Entry<String,Set<String>>>(colValuesMap.entrySet());
        Collections.sort(entryList, new Comparator<Entry<String, Set<String>>>() {
			@Override public int compare(Entry<String, Set<String>> e1, Entry<String, Set<String>> e2) {
				final Tuple2<Integer, Integer> t1 = splitStrInt(e1.getKey());
				final Tuple2<Integer, Integer> t2 = splitStrInt(e2.getKey());
				final int n1 = t1.get_1() * 1000 + t1.get_2();
				final int n2 = t2.get_1() * 1000 + t2.get_2();
				return n1 - n2;
			}
		});
        
        int maxRows = 0;
        for (Entry<String, Set<String>> entry : entryList) {
			final int rows = entry.getValue().size();
			if (rows > maxRows) maxRows = rows;
		}
        
        for (int i = 0; i <= maxRows + 2; i++) {
        	valuesSheet.createRow(i);
		}

    	final Row valueHeaderRow = valuesSheet.getRow(0);
    	short rowHeight = 500;
    	valueHeaderRow.setHeight(rowHeight);
    	
        int colIndex = 0;
        for (Entry<String, Set<String>> entry : entryList) {
        	final String colID = entry.getKey();
        	
        	FieldDesc fieldDesc = null;
        	for (FieldDesc obj : fieldDescList) {
				if (colID.equals( combine(obj.getSheetNum(), obj.getColNum()))) {
					fieldDesc = obj; break;
				}
			}
        	
        	final Row headerRow1 = valuesSheet.getRow(0);
        	final Cell headerRow1Cell = headerRow1.createCell(colIndex, CellType.STRING);
        	final String newColumnID = 
        			(cohortFile.getSheetsNumber() == 1) 
        				? numberToABC(fieldDesc.getColNum()) 
        				: combine(fieldDesc.getSheetNum(), numberToABC(fieldDesc.getColNum()));
        	headerRow1Cell.setCellValue( newColumnID + " : " + fieldDesc.getName());
        	headerRow1Cell.setCellStyle(headerStyleLeft);
        	final Cell headerRow1OtherCell = headerRow1.createCell(colIndex + 1, CellType.STRING);
        	headerRow1OtherCell.setCellStyle(headerStyleRight);
        	
        	final Row columnReferenceRow = valuesSheet.getRow(1);
        	final Cell columnReferenceCell = columnReferenceRow.createCell(colIndex, CellType.STRING);
        	columnReferenceCell.setCellValue("Value");
        	columnReferenceCell.setCellStyle(headerStyleLeft);
        	final Cell headerDescCell = columnReferenceRow.createCell(colIndex + 1, CellType.STRING);
        	headerDescCell.setCellValue("Description");
        	headerDescCell.setCellStyle(headerStyleRight);
        	
        	final List<String> list = new ArrayList<String>(entry.getValue());
        	Collections.sort(list);
        	int rowIndex = 2;
        	for (String str : list) {
        		final Row strRow = valuesSheet.getRow(rowIndex++);
        		final Cell valueCell = strRow.createCell(colIndex, CellType.STRING);
        		valueCell.setCellValue(str);
        		valueCell.setCellStyle(valueStyle);
        		final Cell descCell = strRow.createCell(colIndex+1, CellType.STRING);
        		descCell.setCellStyle(descStyle);
			}
        	final Row lastRow = valuesSheet.getRow(rowIndex++);
        	final Cell lastCell1 = lastRow.createCell(colIndex, CellType.STRING);
        	lastCell1.setCellStyle(upBoarderStyle);
        	final Cell lastCell2 = lastRow.createCell(colIndex+1, CellType.STRING);
        	lastCell2.setCellStyle(upBoarderStyle);
        	
        	colIndex += 2;
        }
        
        int columnIndex = 0;
        for (Entry<String, Set<String>> entry : entryList) {
        	int maxCharNum = 0;
			for (String str : entry.getValue()) {
				if (str.length() > maxCharNum) maxCharNum = str.length();
			}
			if (maxCharNum < 8) maxCharNum = 8;
			if (maxCharNum > 100) maxCharNum = 100;
			valuesSheet.setColumnWidth(columnIndex++, 400 * maxCharNum);
			valuesSheet.setColumnWidth(columnIndex++, 8000);
		}
        
        columnIndex = 0;
        for (@SuppressWarnings("unused") Entry<String, Set<String>> entry : entryList) {
        	 valuesSheet.addMergedRegion(new CellRangeAddress(0, 0, columnIndex, columnIndex + 1));
        	 columnIndex += 2;
        }
        
        // Save Object to File
        final FileOutputStream fileOut = new FileOutputStream(xlsFilePath);
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
		
	}

	/** Create in the in the given Excel Row one or more Cells/Fields with the given names (and a predefined format) */
	private static void addRowFields(final Row row, final String... strs) {
		if (strs == null || strs.length == 0) return;
		int index = 0;
		for (String str : strs) {
			row.createCell(index++).setCellValue(str);
		}
		for (int i = 0; i < index; i++) {
			row.getCell(i).getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
		}
	}

	private static final int BASE = 26;
	
	private static String numberToABC(final int n) {
		if (n < 0) 
			throw new RuntimeException("Negative number.. n= " + n);
		if (n == 0) 
			return "A";
		List<Integer> digitList = new ArrayList<Integer>();
		int tmp = n;
		while (tmp != 0) {
			int rem = tmp % BASE;
			digitList.add(rem);
			tmp = tmp / BASE;
		}
		Collections.reverse(digitList);
		// First ... 
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < digitList.size() - 1; i++) {
			final Integer digit = digitList.get(i);
			final char c = (char) ('A' + (digit - 1));
			sb.append(c);
		}
		// Last Digit
		final char lastChar = (char) ('A' + (digitList.get(digitList.size()-1)));
		sb.append(lastChar);
		return sb.toString();
	}
	
	private static void addRowNameValue(final Row row, final CellStyle cellStyle, String name, String value) {
        final Cell fileNameLabelCell = row.createCell(0);
        fileNameLabelCell.setCellStyle(cellStyle);
        fileNameLabelCell.setCellValue(name);
        final Cell numFieldValueCell = row.createCell(1);
        numFieldValueCell.setCellValue(value);
        numFieldValueCell.setCellStyle(cellStyle);
	}
	
	private static int AbcStrToNumber(final String str) {
		if (str == null || str.trim().equals(""))
			throw new RuntimeException("String is empty.. str= " + str);
		
		Integer n = 0;
		
		for (int i = 0; i < str.length() - 1; i++) {
			final char c = str.charAt(i);
			final int digit = (int) c - 'A' + 1;
			final int pow = str.length() - i - 1;
			final int number = digit * (int) Math.pow(BASE, pow);
			n += number;
		}
		final char lastChar = str.charAt(str.length()-1);
		final int lastNumber = (int) lastChar - 'A';
		n += lastNumber;
		
		return n;
			
	}
	
//	for (int i = 0; i < 500; i++) {
//		final String abcStr = numberToABC(i);
//		final int number = AbcStrToNumber(abcStr);
//		if ( i != number) System.out.println(i + " :: " + abcStr + " :: " + number);
//	}
	
}
