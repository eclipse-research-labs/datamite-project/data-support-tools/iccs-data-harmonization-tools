package org.ntua.iccs.harmonicss.metadata.model;

public class FieldDesc extends Field {

	final String name;
	
	String datatype;
	boolean isNullable;
	boolean hasMultipleValues;
	String notes;
	boolean isNoteWarning;
	
	public FieldDesc(int sheetNum, int rowNum, int colNum, String name) {
		super(sheetNum, rowNum, colNum);
		this.name = name;
	}
	
	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public void setNullable(boolean isNullable) {
		this.isNullable = isNullable;
	}

	public void setHasMultipleValues(boolean hasMultipleValues) {
		this.hasMultipleValues = hasMultipleValues;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public void setNoteWarning(boolean isNoteWarning) {
		this.isNoteWarning = isNoteWarning;
	}
	
	public String getName() {
		return name;
	}

	public String getDatatype() {
		return datatype;
	}

	public boolean isNullable() {
		return isNullable;
	}

	public boolean isHasMultipleValues() {
		return hasMultipleValues;
	}
	
	public boolean isNoteWarning() {
		return isNoteWarning;
	}

	public String getNotes() {
		return notes;
	}
	
	public boolean isDate() {
		//TODO: remove
		if (this.datatype == null) System.out.println(this);
		return (this.datatype != null) ? this.datatype.equals("DATE") : false;
	}
	
	public boolean isRealNumber() {
		return (this.datatype != null) ? this.datatype.equals("REAL NUMBER") : false;
	}

	@Override
	public String toString() {
		return super.toString() + "=" + this.name + " " +
			"(" 
				+ datatype + ((notes !=null) ? " " + notes : "") 
				+ ((isNullable) ? ", NULLABLE" : "") 
				+ ((hasMultipleValues) ? ", MULTIPLE-VALUES" : "") + 
			")";
	}

}
