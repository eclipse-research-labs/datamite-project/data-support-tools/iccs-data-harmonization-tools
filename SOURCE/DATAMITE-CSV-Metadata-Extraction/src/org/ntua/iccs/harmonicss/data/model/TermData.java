package org.ntua.iccs.harmonicss.data.model;

public class TermData {

	private final String label;
	private final String description;
	
	public TermData(String label) {
		this.label = label;
		this.description = null;
	}
	
	public TermData(String label, String description) {
		this.label = label;
		this.description = description;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return description;
	}
	
	public boolean hasDescription() {
		return (description != null && !description.trim().equals(""));
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermData other = (TermData) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TermData [label=" + label + ", description=" + description + "]";
	}
	
}
