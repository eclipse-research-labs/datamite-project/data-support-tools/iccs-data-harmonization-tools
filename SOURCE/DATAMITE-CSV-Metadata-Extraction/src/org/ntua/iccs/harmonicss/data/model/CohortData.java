package org.ntua.iccs.harmonicss.data.model;

public class CohortData {

	private final String xlsFileName;
	private final String xlsFileSheets;
	private final String xlsPatientNumber;
	private final String xlsPatientField;
	private final String xlsVocabularies;
	private final String xlsMetadataDate;
	private final String xlsNotes;
	
	public CohortData(String xlsFileName, String xlsFileSheets, String xlsPatientNumber, String xlsPatientField, String xlsVocabularies, String xlsMetadataDate, String xlsNotes) {
		this.xlsFileName = xlsFileName;
		this.xlsFileSheets = xlsFileSheets;
		this.xlsPatientNumber = xlsPatientNumber;
		this.xlsPatientField = xlsPatientField;
		this.xlsVocabularies = xlsVocabularies;
		this.xlsMetadataDate = xlsMetadataDate;
		this.xlsNotes = xlsNotes;
	}

	public String getXlsFileName() {
		return xlsFileName;
	}

	public String getXlsFileSheets() {
		return xlsFileSheets;
	}

	public String getXlsPatientNumber() {
		return xlsPatientNumber;
	}

	public String getXlsPatientField() {
		return xlsPatientField;
	}
	
	public String getXlsVocabularies() {
		return xlsVocabularies;
	}
	
	public String getXlsMetadataDate() {
		return xlsMetadataDate;
	}

	public String getXlsNotes() {
		return xlsNotes;
	}
	
	@Override
	public String toString() {
		return "CohortData ["
				+ "xlsFileName=" + xlsFileName + ", "
				+ "xlsFileSheets=" + xlsFileSheets + ", "
				+ "xlsPatientNumber=" + xlsPatientNumber + ", "
				+ "xlsPatientField=" + xlsPatientField + ", "
				+ "xlsVocabularies=" + xlsVocabularies + ", "
				+ "xlsMetadataDate=" + xlsMetadataDate + ", "
				+ "xlsNotes=" + xlsNotes + 
			"]";
	}
	
}
