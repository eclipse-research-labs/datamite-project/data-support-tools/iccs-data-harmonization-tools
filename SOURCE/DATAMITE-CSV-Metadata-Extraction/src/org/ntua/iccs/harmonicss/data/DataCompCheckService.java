package org.ntua.iccs.harmonicss.data;

import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.ntua.iccs.harmonicss.data.model.AttrData;
import org.ntua.iccs.harmonicss.data.model.CohortData;
import org.ntua.iccs.harmonicss.data.model.TermData;
import org.ntua.iccs.harmonicss.data.model.VocabData;
import org.ntua.iccs.harmonicss.metadata.MetadataExtractionService;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Format;

public class DataCompCheckService {

	//TODO: Revise and Export as a Stand alone Desktop Application
	
	/**
	 * For testing purposes ...
	 */
	public static void main(String[] args) throws Exception {
		
		System.out.println(" >> DataCompCheckService: BEGIN");
		System.out.println();
		
//		// UNIRO
//		final String dataFilePath = "C:/Users/Efthymios/Downloads/UNIRO/UNIRO-2019-04-15-Cohort-FAKE-Data.xls";
//		final String metadataFilePath = "C:/Users/Efthymios/Downloads/UNIRO/UNIRO-2019-04-15-Cohort-Metadata-FINAL-v.0.4.2.xlsx";

		// UiB
		final String dataFilePath = "C:/Users/Efthymios/Desktop/UiB/UiB-2019-07-13-Cohort-FAKE-Data.xlsx";
		final String metadataFilePath = "C:/Users/Efthymios/Desktop/UiB/UiB-2019-07-13-Cohort-Metadata-v.0.3.2.xlsx";
		
//		// HUA
//		final String dataFilePath = "C:/Users/Efthymios/Downloads/HUA/HUA-2019-05-02-Cohort-FAKE-Data.xlsx";
//		final String metadataFilePath = "C:/Users/Efthymios/Downloads/HUA/HUA-2019-05-02-Cohort-Metadata-FINAL-v.0.4.xlsx";
		
		check(dataFilePath, metadataFilePath);
		
		System.out.println();
		System.out.println(" >> DataCompCheckService: END");
	}
	
	public static void check(String dataExcelPath, String metadataExcelPath) throws Exception {
		
		final String genMetadataFilePath = "./Metadata.xlsx";
		
		// Generate Metadata based on the given MS Excel File with the Patient Data
		try {
			MetadataExtractionService.process(dataExcelPath, genMetadataFilePath);
		} catch (Throwable t) {
			throw new RuntimeException("Cannot GENERATE Metadata based on the given MS Excel File: " + dataExcelPath);
		}
		
		// Read Metadata Extracted
		final Tuple3<CohortData, List<AttrData>, List<VocabData>> genTuple;
		try {
			genTuple = getDataFromXLS(genMetadataFilePath);
		} catch (Throwable t) {
			throw new RuntimeException("Cannot READ Metadata from the GENERATED MS Excel File: " + genMetadataFilePath);
		}
		
		// Read Metadata Provided
		final Tuple3<CohortData, List<AttrData>, List<VocabData>> givenTuple;
		try {
			givenTuple = getDataFromXLS(metadataExcelPath);
		} catch (Throwable t) {
			throw new RuntimeException("Cannot READ Metadata from the GIVEN MS Excel File: " + metadataExcelPath);
		}
		
		// Compare ...
		
		// Check Number of Fields
		final String genMetaFieldsNum = genTuple.get_1().getXlsPatientField().trim();
		final String givenMetaFieldsNum = givenTuple.get_1().getXlsPatientField().trim();
		if (!genMetaFieldsNum.equals(givenMetaFieldsNum))
			throw new RuntimeException("The number of Cohort Data Fields (" + genMetaFieldsNum + ") is different than expected ! (Given Metadata Cohort Fields: " + givenMetaFieldsNum +")");
		
		// Check Fields Data Types
		int dtWarnCount = 0;
		for (int i = 0; i < genTuple._2().size(); i++) {
			final AttrData genAttrData = genTuple._2().get(i);
			final AttrData givenAttrData = givenTuple._2().get(i);
			
			final String genDT = genAttrData.getDataType();
			final String givenDT = givenAttrData.getDataType();
			if (!genDT.equals(givenDT)) {
				// Probably Fields are empty
				if (genDT.equals("-")) continue;
				// Seems ok
				if (genDT.equals("INTEGER") && givenDT.equals("REAL NUMBER")) continue;
				System.out.println("WARN: Column: " + Format.formatStringLeftByMaxLength(genAttrData.getID(), 2) + " :: " + 
						"Datatype: \"" + genDT + "\"" + " DIFFERENT THAN expected: \"" + givenDT + "\"");
				dtWarnCount++;
			}
		} // END OF FIELDS LOOP
		if (dtWarnCount > 0) System.out.println("\n * Number of Warnings: " + dtWarnCount);
		
		// Check Fields Values
		int columnWarnCount = 0;
		for (VocabData  genVocabData : genTuple._3()) {
			final String columnID = genVocabData.getAttrIDsSet().iterator().next();
			VocabData columnGivenVocabData = null;
			for (VocabData givenVocabData : givenTuple._3()) {
				if (givenVocabData.getAttrIDsSet().contains(columnID)) {
					columnGivenVocabData = givenVocabData;
					break;
				}
			}
			// Check if the value of this Field/Column comes from a Controlled set of terms/values
			if (columnGivenVocabData == null) continue;
			// Compare existing values with expected ones
			final Set<String> acceptedValuesSet = new HashSet<String>();
			for (TermData termData : columnGivenVocabData.getTermsSet()) {
				acceptedValuesSet.add(termData.getLabel());
			}
			for (TermData termData : genVocabData.getTermsSet()) {
				final List<String> unexpectedValuesList = new ArrayList<String>();
				if (!acceptedValuesSet.contains(termData.getLabel())) {
					unexpectedValuesList.add(termData.getLabel());
				}
				if (unexpectedValuesList.size() > 0) {
					final List<String> acceptedValuesList = new ArrayList<String>(acceptedValuesSet);
					Collections.sort(acceptedValuesList);
					System.out.println("WARN: Column: " + columnID + " Inappropriate values: " + unexpectedValuesList + " :: Expected one of: " + acceptedValuesList);
					columnWarnCount++;
				}
			}
		} // END OF VOCABS/FIELDS Loop
		if (columnWarnCount > 0) System.out.println("\n * Number of Columns with Unexpected values: " + columnWarnCount);

	}
	
 	public static Tuple3<CohortData, List<AttrData>, List<VocabData>> getDataFromXLS(final String xlsFilePath) throws Exception {
		if (xlsFilePath == null || xlsFilePath.trim().equals("")) 
			throw new RuntimeException("Parameter: xlsFilePath NOT specified !");
		if ( (!xlsFilePath.toLowerCase().endsWith(".xls")) && (!xlsFilePath.toLowerCase().endsWith(".xlsx"))) 
			throw new RuntimeException("File \"" + xlsFilePath + "\" is NOT an Excel Document !");
		
		final Workbook workbook = 
			(xlsFilePath.toLowerCase().endsWith(".xls")) ? 
				new HSSFWorkbook(new FileInputStream( new File(xlsFilePath) ) ) :
			(xlsFilePath.toLowerCase().endsWith(".xlsx")) ? 
				new XSSFWorkbook( new FileInputStream( new File(xlsFilePath) ) ) : null;

		// Overview
		final Sheet sheet0 = workbook.getSheetAt(0);	
		
		final String xlsFileName = getCellData(sheet0.getRow(4).getCell(1));
		final String xlsFileSheets = getCellData(sheet0.getRow(5).getCell(1));
		final String xlsPatientNumber = getCellData(sheet0.getRow(7).getCell(1));
		final String xlsPatientField = getCellData(sheet0.getRow(8).getCell(1));
		final String xlsVocabularies = getCellData(sheet0.getRow(9).getCell(1));
		final String xlsMetadataDate = getCellData(sheet0.getRow(11).getCell(1));
		
		final StringBuilder notesSb = new StringBuilder();
		final Iterator<Row> rowIt = sheet0.iterator();
		while (rowIt.hasNext()) {
			final StringBuilder rowSb = new StringBuilder();
		   	final Row row = rowIt.next();
		    final int rowNum = row.getRowNum();
		    if (rowNum < 22) continue;
		    final Iterator<Cell> cellIt = row.cellIterator();
		    while (cellIt.hasNext()) {
		      	final Cell cell = (Cell) cellIt.next();
				final String cellData = getCellData(cell);
				if (cellData == null) continue;
				if (rowSb.length() > 0) rowSb.append(" ");
				rowSb.append(cellData);
		    } // END OF CELLs LOOP
		    if (rowSb.length() > 0) {
		    	if (notesSb.length() > 0) notesSb.append("\n\n");
				notesSb.append(rowSb);
		    }
		} // END OF ROWs LOOP
		
		final String xlsNotes = (notesSb.length() > 0) ? notesSb.toString() : null;
		final CohortData cohortData = new CohortData(xlsFileName, xlsFileSheets, xlsPatientNumber, xlsPatientField, xlsVocabularies, xlsMetadataDate, xlsNotes);
		
		// FIELDS
		final List<AttrData> attrDataList = new ArrayList<AttrData>();
		final Sheet sheet1 = workbook.getSheetAt(1);		
		final Iterator<Row> rowIt1 = sheet1.iterator();
		while (rowIt1.hasNext()) {
		   	final Row row = rowIt1.next();
		   	final int rowNum = row.getRowNum();
		   	if (rowNum == 0) continue;
			final String ID = getCellData(row.getCell(0));
			final String category = getCellData(row.getCell(1));
			final String subCategory = getCellData(row.getCell(2));
			final String name = getCellData(row.getCell(3));
			final String description = getCellData(row.getCell(4));
			final String dataType = getCellData(row.getCell(5));
			final String dataNotes = getCellData(row.getCell(6));
			final String canBeEmpty = getCellData(row.getCell(7));
			final String hasManyValues = getCellData(row.getCell(8));
		   	if (areStrsEmpty(ID, category, subCategory, name, description, dataType, dataNotes, canBeEmpty, hasManyValues)) continue;
		   	final AttrData attrData = new AttrData(ID, category, subCategory, name, description, dataType, dataNotes, canBeEmpty, hasManyValues);
		   	// System.out.println(rowNum + " :: " + attrData);
		   	attrDataList.add(attrData);
		} // END OF ROWs LOOP		
		
		// VALUES
		final Sheet sheet2 = workbook.getSheetAt(2);
		final Map<Integer, VocabData> vocMap = new HashMap<Integer, VocabData>();
		final Row row0 = sheet2.getRow(0);
		final Iterator<Cell> row0cellIt = row0.cellIterator();
		while (row0cellIt.hasNext()) {
			final Cell cell = (Cell) row0cellIt.next();
			final int colNum = cell.getColumnIndex();
			final String cellData = getCellData(cell);
			if (cellData == null) continue;
			final int index = cellData.indexOf(':');
			if (index < 0) throw new RuntimeException("Unable to find SEMICOLON character in column " + colNum + " of the third sheet !");
			final String str1 = cellData.substring(0, index);
			final Set<String> attrIDsSet = new HashSet<String>();
			for (String id : str1.split(",")) { attrIDsSet.add(id.trim()); }
			final String str2 = cellData.substring(index+1, cellData.length());
			final String vocName = str2.trim();
			vocMap.put(colNum, new VocabData(vocName, attrIDsSet));
			//System.out.println(colNum + " :: " + vocName + " :: " + attrIDsSet);
		}
		
		for (Entry<Integer, VocabData> entry : vocMap.entrySet()) {
			final int colNum = entry.getKey().intValue();
			final Iterator<Row> sheet2RowIt = sheet2.iterator();
			while (sheet2RowIt.hasNext()) {
				final Row row = sheet2RowIt.next();
				final int rowNum = row.getRowNum();
				if (rowNum <= 1) continue;
				final String label = getCellData(row.getCell(colNum));
				if (label == null) continue;
				final String description = getCellData(row.getCell(colNum+1));
				entry.getValue().getTermsSet().add(new TermData(label, description));
			} // END OF ROWs LOOP	
		}
		final List<Entry<Integer, VocabData>> entryList = new ArrayList<Entry<Integer, VocabData>>(vocMap.entrySet());
		Collections.sort(entryList, new Comparator<Entry<Integer, VocabData>>() {
			@Override public int compare(Entry<Integer, VocabData> e1, Entry<Integer, VocabData> e2) {
				return e1.getKey() - e2.getKey();
			}
		});
		
		final List<VocabData> vocabDataList = new ArrayList<VocabData>();
		for (Entry<Integer, VocabData> entry : entryList) {
			vocabDataList.add(entry.getValue());
		}
		
	    workbook.close();
	    
		return new Tuple3<CohortData, List<AttrData>, List<VocabData>>(cohortData, attrDataList, vocabDataList);
	}

	private static final DateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
	private static final NumberFormat numFormater = new DecimalFormat("#.####");
	
	/** @return Data existing in a specific Cell of an Excel File in the form of a String */
	private static String getCellData(Cell cell) {
		if (cell == null || cell.toString().trim().equals("")) return null;
		if (cell.getCellTypeEnum() == CellType.STRING) {
			return cell.getStringCellValue().replaceAll("\\s+", " ").trim();
		} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				return dateFormater.format(cell.getDateCellValue());
		    } else {
		    	return numFormater.format(cell.getNumericCellValue());
		    }
		} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
			return ("" + cell.getBooleanCellValue()).toLowerCase();
		} else if (cell.getCellTypeEnum() == CellType.FORMULA) {
			return "FORMULA: " + cell;
		} else if (cell.getCellTypeEnum() == CellType.BLANK) {
			return null;
		} else if (cell.getCellTypeEnum() == CellType.ERROR) {
			return null;
		} else if (cell.getCellTypeEnum() == CellType._NONE) {
			System.out.println("NONE " + cell);
			return "NONE: " + cell;
		} else {
			System.out.println(" ****** " + cell);
			return cell.toString().trim();
		}
			
	}

	private static boolean areStrsEmpty(String... strs) {
 		boolean flag = true;
 		for (String str : strs) {
			if (str != null && !str.trim().equals("")) {
				flag = false; break;
			}
		}
 		return flag;
 	}
	
}
