@ECHO OFF
SET JAVA_HOME=C:\Program Files\Java\jdk-17.0.1
SET PATH=%JAVA_HOME%\bin;%PATH%
ECHO JAVA_HOME=%JAVA_HOME%
ECHO.
CALL java -version
ECHO.
CALL ant -f dektop-app-build.xml
TIMEOUT /t 3