@ECHO OFF

SET JAR_FILE="./DATAMITE-CSV-Metadata-Extraction-v.0.5.2.jar"

SET MAIN_CLASS=org.ntua.iccs.harmonicss.metadata.MetadataExtractionService

SET COHORT_XLS_FILE_PATH=./other-files/TEST-DATA/Test-Data-Excel-File.xlsx
SET COHORT_METADATA_XLS_FILE_PATH=./other-files/TEST-DATA/Test-Metadata-Excel-File.xlsx

CALL java -cp %JAR_FILE% %MAIN_CLASS% %COHORT_XLS_FILE_PATH% %COHORT_METADATA_XLS_FILE_PATH%
