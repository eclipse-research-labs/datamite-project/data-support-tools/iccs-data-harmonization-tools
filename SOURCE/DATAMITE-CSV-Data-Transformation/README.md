## CSV Data Harmonization Desktop Application

### Step 1: Generate Executable JAR File 

 * Prerequisites: 

	**JAVA 17** and **Apache Ant** should ve installed in your machine


### Step 2: Run Executable JAR File 

#### (a) Run Locally

 * Option 1: **GUI**
	
	Double click to the generated executable JAR file
	
 * Option 2: **Command Line**
	
	Run command: 
	
	```
	TESTING-Data-Harmonization-App.cmd
	```
	
#### (b) Containirize and Run 

 * Build the Docker Image using the following command: 
 
	```
	docker build -t csv-data-harmonization-image .
	```
	
 * Verifying the Docker Image
 
	```
	docker images
	```
	
 * Run the Docker Container 
 
	```
	docker run -it csv-data-harmonization-image
	```
	
 * The you can get the generated file with metatada using commands:
 
	```
	docker ps -a
	```
	
	and
	
	```
	docker cp <containerId>:/file/path/within/container /host/path/target
	```
