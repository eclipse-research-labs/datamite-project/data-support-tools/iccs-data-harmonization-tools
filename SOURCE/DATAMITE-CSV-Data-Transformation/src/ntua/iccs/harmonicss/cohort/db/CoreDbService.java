package ntua.iccs.harmonicss.cohort.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.timchros.core.util.Throable;

import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.LogFactory;

/**
 * Provides some core services for inserting pieces of data in the appropriate tables 
 * and returning the ID of the data inserted 
 */
class CoreDbService {

	public static final Logger log = LogFactory.getLoggerForClass(CoreDbService.class);
	
	/** Ensure that we wont create an instance of this class */
	private CoreDbService() { }
	
	/* ********************* User-defined Data Types ********************* */
	
	/** Insert Integer Interval and return the ID */
    public static int insertIntegerInterval(JSONObject jo, Connection conn) throws Exception {
    	// Down Limit
    	String intervDownLimit = null;
        try { intervDownLimit = jo.get("interval-Integer-Down-Limit").toString(); } catch (Exception e) { }
    	// Up Limit
    	String intervUpLimit = null;
        try { intervUpLimit = jo.get("interval-Integer-Up-Limit").toString(); } catch (Exception e) { }
    	// Insert Data
        if(intervUpLimit != null && intervDownLimit != null) {
        	final String insertSqlQuery1 = 
        		"insert into dt_int_range(dt_int_range.INT1, dt_int_range.INT2) values " + 
        		"(" + intervDownLimit + ", " + intervUpLimit + ")";
            return CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery1);
        } else {
        	// Half-bounded Interval
            if (intervUpLimit != null) {
            	final String insertSqlQuery2 = 
            		"insert into dt_int_range(dt_int_range.INT1, dt_int_range.INT2) values " + 
            		"(null, " + intervUpLimit + ")";
                return CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery2);
            } else { // (intervDownLimit != null)
            	final String insertSqlQuery3 = 
            		"insert into dt_int_range(dt_int_range.INT1, dt_int_range.INT2) values " + 
            		"(" + intervDownLimit + ", null)";
                return CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery3);
            }
        }
    }
    
    /** Insert Interval and return the ID */
    public static int insertAmountInterval(JSONObject jo, Vocab vocab, Connection conn) throws Exception {
    	// Down Limit
    	String intervDownLimit = null;
        try { intervDownLimit = jo.get("interval-Amount-Down-Limit").toString(); } catch (Exception e) { }
    	// Up Limit
    	String intervUpLimit = null;
        try { intervUpLimit = jo.get("interval-Amount-Up-Limit").toString(); } catch (Exception e) { }
    	// Unit of Measurement
    	JSONObject amountUnitCV = null;
        try { amountUnitCV = (JSONObject) jo.get("amount-Unit-CV"); } catch (Exception e) { }
        Integer amountUnitID = null;
        if(amountUnitCV != null) {
            String amountUnitCode = amountUnitCV.get("code-Value").toString();
            amountUnitID = vocab.getCodeID(amountUnitCode);
        }
        // Insert Data
        if(intervUpLimit != null && intervDownLimit != null) {
        	final String insertSqlQuery1 = 
        		"insert into dt_amount_range(VALUE1, VALUE2, UNIT_ID) values " + 
        		"(" + intervDownLimit + ", " + intervUpLimit + ", " + amountUnitID + ")";
            return CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery1);
        } else {
        	// Half-bounded Interval
            if (intervUpLimit != null) {
            	final String insertSqlQuery2 = 
            		"insert into dt_amount_range(VALUE1, VALUE2, UNIT_ID) values " + 
            		"(null, " + intervUpLimit + ", " + amountUnitID + ")";
                return CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery2);
            } else { // (intervDownLimit != null)
            	final String insertSqlQuery3 = 
            		"insert into dt_amount_range(VALUE1, VALUE2, UNIT_ID) values " + 
            		"(" + intervDownLimit + ", null, " + amountUnitID + ")";
                return CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery3);
            }
        }
    }
    
    /** Insert Amount or Interval and return the ID */
    public static int insertAmountOrInterval(JSONObject jo, Vocab vocab, Connection conn) throws Exception {
    	// Amount Value or Limits
    	String amountValue = null;
    	try { amountValue = jo.get("amount-Value").toString(); } catch (Exception e) { }
    	String intervUpLimit = null;
        try { intervUpLimit = jo.get("interval-Amount-Up-Limit").toString(); } catch (Exception e) { }
        String intervDownLimit = null;
        try { intervDownLimit = jo.get("interval-Amount-Down-Limit").toString(); } catch (Exception e) { }
        // Unit of Measurement
        JSONObject amountUnitCV = null;
        try { amountUnitCV = (JSONObject) jo.get("amount-Unit-CV"); } catch (Exception e) { }
        Integer amountUnitID = null;
        if(amountUnitCV != null) {
            String amountUnitCode = amountUnitCV.get("code-Value").toString();
            amountUnitID = vocab.getCodeID(amountUnitCode);
        }
        // The appropriate Insert SQL query based on the given data
        if(amountValue != null) { // Exact Value
        	final String insertAmountSQL = 
            	"insert into dt_amount(VALUE, UNIT_ID, OP_ID, VALUE2) values " + 
            	"(" + amountValue + ", " + amountUnitID + ", 2, null)";
            return CoreDbService.executeInsertSQLandReturnID(conn, insertAmountSQL);
        } else { // Amount Interval
            if(intervUpLimit != null && intervDownLimit != null) {
            	final String insertSqlQuery1 = 
            		"insert into dt_amount(VALUE, UNIT_ID, OP_ID, VALUE2) values " + 
            		"(" + intervDownLimit + ", " + amountUnitID + ", 2, " + intervUpLimit + ")";
                return CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery1);
            } else {
            	// Half-bounded Interval
                if (intervUpLimit != null) {
                    final String insertSqlQuery2 = 
                    	"insert into dt_amount(VALUE, UNIT_ID, OP_ID, VALUE2) values " + 
                    	"(" + intervUpLimit + ", " + amountUnitID + ", 1, null)";
                    return CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery2);
                } else { // (intervDownLimit != null)
                	final String insertSqlQuery3 = 
                		"insert into dt_amount(VALUE, UNIT_ID, OP_ID, VALUE2) values " + 
                		"(" + intervDownLimit + ", " + amountUnitID + ", 3, null)";
                	return CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery3);
                }
            }
        }
    }
    
    /** Insert Period and return the ID */
    public static Integer insertPeriod(JSONObject jo, Connection conn) throws Exception {
    	try {
	    	// Start Date
	    	String intervStartDate = null;
	    	try { intervStartDate = jo.get("date-Interval-Start-Date").toString(); } catch (Exception e) { }
	    	final Integer intervStartDateID = (intervStartDate != null) ? CoreDbService.insertDate(intervStartDate, conn) : null;
	    	// End Date
	    	String intervEndDate = null;
	        try { intervEndDate = jo.get("date-Interval-End-Date").toString(); } catch (Exception e) { }
	        final Integer intervEndDateID = (intervEndDate != null) ? CoreDbService.insertDate(intervEndDate, conn) : null;
	        // Insert SQL Query
	        final String insertSqlQuery = 
	        	"insert into dt_period_of_time(START_DATE_ID, END_DATE_ID, EXACT_ID, BEFORE_PERIOD_ID) values " +
	            "(" + intervStartDateID + ", " + intervEndDateID + ", 1, null)";
	        return executeInsertSQLandReturnID(conn, insertSqlQuery);
    	} catch(Throwable t) {
    		// Note: A Year should be in range of 1901 to 2155
    		log.info("insertPeriod(...): " + Throable.throwableAsSimpleOneLineString(t));
    		return null;
    	}
    }
    
    /** Insert Date or Period and return the ID */
    public static Integer insertDateOrPeriod(Object dtObj, Connection conn) throws Exception {
        JSONObject intervDateJO = null;
        String dateStr = null;
        if (dtObj instanceof JSONObject) {
        	intervDateJO = (JSONObject) dtObj;
        } else {
        	dateStr = "" + dtObj;
        }
        Integer dateID = null;
        if(intervDateJO == null) {
        	// Exact Date
        	dateID = (dateStr != null) ? CoreDbService.insertDate(dateStr, conn) : null;
        } else { 
        	// Date Interval 
            String intervalStartDate = null;
            try { intervalStartDate = intervDateJO.get("date-Interval-Start-Date").toString(); } catch (Exception e) { }
            String intervalEndDate = null;
            try { intervalEndDate = intervDateJO.get("date-Interval-End-Date").toString(); } catch (Exception e) { }
            // Insert Data
            if((intervalStartDate != null) && (intervalEndDate != null)) {
            	dateID = CoreDbService.insertDate(intervalStartDate, intervalEndDate, 2, conn);
            } else {
                if (intervalStartDate != null) {
                	final int opID = 3; // AFTER
                	dateID = CoreDbService.insertDatePeriod(intervalStartDate, opID, conn);
                } else { // (intervalEndDate != null)
                	final int opID = 1; // BEFORE
                	dateID = CoreDbService.insertDatePeriod(intervalEndDate, opID, conn);
                }
            }
        }
        return dateID;
    }
    
    private static final String nullDate = "----/--/--";
    
    /** Insert a Simple Date and return the ID */
    public static Integer insertDate(String date, Connection conn) {
    	final int opID = 2; // DURING / EQUALS
    	return insertDate(date, nullDate, opID, conn);
    }
    
    /** Insert a Simple Date Period and return the ID */
    private static Integer insertDatePeriod(final String date, final int opID, final Connection conn) {
    	return insertDate(date, nullDate, opID, conn);
    }
    
    private static Integer insertDate(String date1, String date2, int opID, Connection conn) {
    	try {
	    	// Date 1 - List with Year Month Day
	    	final List<Integer> dtList1 = getDateList(date1);
	        // Date 2 - List with Year Month Day
	        final List<Integer> dtList2 = getDateList(date2);
	        // Insert Data
	        final String insertDateSQL = 
	        	"insert into dt_date(YEAR, MONTH, DAY, OP_ID, YEAR2, MONTH2, DAY2) values " + 
	        	"(" + dtList1.get(0) + ", " + dtList1.get(1) + ", " + dtList1.get(2) + ", " + opID + ", " + 
	        		dtList2.get(0) + ", " + dtList2.get(1) + ", " + dtList2.get(2) + ")";
	        final int dateID = executeInsertSQLandReturnID(conn, insertDateSQL);
	        return dateID;
    	} catch (Throwable t) {
    		// Note: A Year should be in range of 1901 to 2155
    		log.info("insertDate(...): " + Throable.throwableAsSimpleOneLineString(t));
    		return null;
    	}
    }
    
    private static List<Integer> getDateList(final String dateStr) {
    	final List<Integer> dateList = new ArrayList<Integer>();
        final String[] dateToken = dateStr.split("/");
        for(int i=0; i < dateToken.length; i++) {
        	final String token = dateToken[i];
            if(token.equals("--") || token.equals("----")){
            	dateList.add(null);
            } else {
            	dateList.add(Integer.parseInt(token));
            }
        }
        return dateList;
    }
    
    /* ********************* Execute SQL Queries ********************* */
    
    /** Execute the given SQL query */
    public static void executeSQL(final Connection conn, final String sql) throws Exception {
    	Statement stmt = null;
    	try {
    		// Insert Data
	    	stmt = conn.createStatement();
	    	final int affectedRows = stmt.executeUpdate(sql);
	    	if (affectedRows == 0) { 
	    		throw new SQLException("Executing SQL query \"" + sql + "\" failed, no rows affected !");
	    	}
    	} catch (Throwable t) {
    		throw new SQLException("SQL query: " + sql, t);
    	} finally {
    		if (stmt != null) stmt.close();
    	}
    }
    
    /** Execute the given insert SQL query and Return the ID */
    public static int executeInsertSQLandReturnID(final Connection conn, final String insertSQL) throws Exception {
    	PreparedStatement stmt = null;
    	try {
    		// Insert Data (highlight the fact that we need the generated ID)
	    	stmt = conn.prepareStatement(insertSQL, Statement.RETURN_GENERATED_KEYS);
	    	final int affectedRows = stmt.executeUpdate();
	    	if (affectedRows == 0) { 
	    		throw new SQLException("Executing SQL query \"" + insertSQL + "\" failed, no rows affected !");
	    	}
	    	// Retrieve ID
	    	final int genID;
	    	try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
	    		if (generatedKeys.next()) {
	    			genID = generatedKeys.getInt(1);
	    			generatedKeys.close();
	    		} else {
	    			throw new SQLException("Inserting Data failed, no ID obtained !");
	    		}
	    	}
	    	return genID;
    	} catch (Throwable t) {
    		throw new SQLException("SQL query: " + insertSQL, t);
    	} finally {
    		if (stmt != null) stmt.close();
    	}
    }
    
}
