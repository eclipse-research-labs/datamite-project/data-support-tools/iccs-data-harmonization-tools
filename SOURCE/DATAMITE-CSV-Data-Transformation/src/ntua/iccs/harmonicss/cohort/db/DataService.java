package ntua.iccs.harmonicss.cohort.db;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.LogFactory;

/** 
 * Provides the functionality for storing the JSON data that belong to a particular Data Type (e.g., Lab Test)
 * in the appropriate DB tables 
 */
class DataService {

	/** The functionality that every JSON Object Processor should provide */
	private interface JsonDataInsertionI {
		
		/** @return A positive integer if data successfully inserted in the database, otherwise a negative integer. */
		public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception;
	
	}
	
	public static final Logger log = LogFactory.getLoggerForClass(DataService.class);
	
	/** Ensure that we wont create an instance of this class */
	private DataService() { }
	
	/** Distinct Data Types inserted in the DB */
	private static final Set<String> dataTypeInsertSet = new HashSet<>();
	
	public static void emptyDataTypesInsertSet() {
		dataTypeInsertSet.clear();
	}
	
	public static List<String> getDataTypesInsertList() {
		final List<String> dataTypeInsertList = new ArrayList<String>(dataTypeInsertSet);
		Collections.sort(dataTypeInsertList, new Comparator<String>() {
			@Override public int compare(String s1, String s2) {
				return s1.compareTo(s2);
			}
		});
		return dataTypeInsertList;
	}
	
	/** Distinct Data Types ignored in the DB */
	private static final Set<String> dataTypeIgnSet = new HashSet<>();
	
	public static void emptyDataTypesIgnSet() {
		dataTypeIgnSet.clear();
	}
	
	public static List<String> getDataTypesIgnList() {
		final List<String> dataTypeIgnList = new ArrayList<String>(dataTypeIgnSet);
		Collections.sort(dataTypeIgnList, new Comparator<String>() {
			@Override public int compare(String s1, String s2) {
				return s1.compareTo(s2);
			}
		});
		return dataTypeIgnList;
	}
	
	/** A Map with the Reference Model Data Types and the corresponding Java Services */
	@SuppressWarnings("unchecked")
	private static final Map<String, JsonDataInsertionI> typeServiceMap = Util.newHashMapFromTuples(
		// Demographics
		Tuple2.newTuple2("Gender", new GenderJsonDataInsertion()),
		Tuple2.newTuple2("Ethnicity", new EthnicityJsonDataInsertion()),
		Tuple2.newTuple2("Education", new EducationJsonDataInsertion()),
		Tuple2.newTuple2("Occupation", new OccupationJsonDataInsertion()),
		Tuple2.newTuple2("Body-Weight", new BodyWeightJsonDataInsertion()),
		// Pregnancy & Smoking Status
		Tuple2.newTuple2("Pregnancy", new PregnancyJsonDataInsertion()),
		Tuple2.newTuple2("Tobacco-Consumption", new TobaccoConsumptionJsonDataInsertion()),
		// Medical Conditions
		Tuple2.newTuple2("Diagnosis", new DiagnosisJsonDataInsertion()),
		Tuple2.newTuple2("Symptom-or-Sign", new SymptomJsonDataInsertion()),
		// Interventions
		Tuple2.newTuple2("Medication", new MedicationJsonDataInsertion()),
		Tuple2.newTuple2("Chemotherapy", new ChemotherapyJsonDataInsertion()),
		Tuple2.newTuple2("Surgery", new SurgeryJsonDataInsertion()),
		// Lab/Imaging Test & Biopsies
		Tuple2.newTuple2("Laboratory-Test", new LaboratoryTestJsonDataInsertion()),
		Tuple2.newTuple2("Medical-Imaging-Test", new MedicalImagingTestJsonDataInsertion()),
		Tuple2.newTuple2("Biopsy", new BiopsyJsonDataInsertion()),
		// Questionnaire Scores and Data
		Tuple2.newTuple2("Questionnaire-Score", new QuestionnaireScoreJsonDataInsertion()),
		Tuple2.newTuple2("ESSDAI-Domain-AL", new ESSDAIDomainALJsonDataInsertion()),
		Tuple2.newTuple2("CACI-Comorbidity", new CACIComorbidityJsonDataInsertion()),
		// Other Data
		Tuple2.newTuple2("Clinical-Trial", new ClinicalTrialJsonDataInsertion()),
		Tuple2.newTuple2("Family-History", new FamilyHistoryJsonDataInsertion()),
		Tuple2.newTuple2("Other-Healthcare-Entity-Visit", new OtherHealthcareEntityVisitJsonDataInsertion())
	);
	
	/** Insert the given Patient JSON data in the DB  */
	public static void insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
		final String dataType = jo.get("type").toString();
		if (!typeServiceMap.containsKey(dataType)) {
			throw new RuntimeException("No Registered Service for Data Type: " + dataType);
		} else {
			final JsonDataInsertionI service = typeServiceMap.get(dataType);
			final int rsp = service.insertJsonData(vocab, jo, patientID, visitID, stmtID, conn);
			if (rsp > 0) {
				dataTypeInsertSet.add(dataType);
			} else {
				final String stmtMsg = (stmtID == 1) ? "HAS" : "HAS-NOT";
				log.warn("IGNORE: " + stmtMsg + " :: " + jo);
				dataTypeIgnSet.add(dataType);
			}
		}
	}

	/* ======================================== Data Types and Services ======================================= */

	/* ********************************************* Demographics ********************************************* */
	
	private static final class GenderJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// Gender 
			JSONObject genderCV = null;
	        try { genderCV  = (JSONObject) jo.get("gender-CV"); } catch (Exception e) { }
	        final Integer genderID = (genderCV != null) ? vocab.getCodeID(genderCV.get("code-Value").toString()) : null;
	        // Insert Data
	        final String insertSqlQuery = 
	        	"insert into demo_sex_data(PATIENT_ID, SEX_ID, VISIT_ID) values " + 
	            "(" + patientID + ", " + genderID + ", " + visitID + ")";
	        CoreDbService.executeSQL(conn, insertSqlQuery);
	        return 1;
		}
	} // END OF GenderJsonDataInsertion
	
	private static final class EthnicityJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// Ethnicity
			JSONObject ethnicityCV = null;
			try { ethnicityCV = (JSONObject) jo.get("ethnicity-CV"); } catch (Exception e) { }
            final Integer ethnicityID = ((ethnicityCV != null)) ? vocab.getCodeID(ethnicityCV.get("code-Value").toString()) : null;
            // Insert Data
            final String insertSqlQuery = 
                "insert into demo_ethnicity_data(PATIENT_ID, ETHNICITY_ID, VISIT_ID) values " + 
                "(" + patientID + ", " + ethnicityID + ", " + visitID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF EthnicityJsonDataInsertion	
	
	private static final class EducationJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;	
			// Education Level
			JSONObject educLevCV = null;
            try { educLevCV = (JSONObject) jo.get("education-Level-CV"); } catch (Exception e) { }
            final Integer educLevID = ((educLevCV != null)) ? vocab.getCodeID(educLevCV.get("code-Value").toString()) : null;
            // Insert Data
            final String insertSqlQuery = 
            	"insert into demo_education_level_data(PATIENT_ID, EDUCATION_LEVEL_ID, VISIT_ID) values " +
                "(" + patientID + ", " + educLevID + ", " + visitID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;			
		}
	} // END OF EducationJsonDataInsertion
	
	private static final class OccupationJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// Occupation Loss
			Integer workLossID = null;
			final Object worklossObj = jo.get("work-Loss-due-to-PSS");
			if (worklossObj != null) {
				if (worklossObj instanceof JSONObject) {
					final JSONObject worklossJO = (JSONObject) worklossObj; 
					workLossID = vocab.getCodeID(worklossJO.get("code-Value").toString());
				} else { // Boolean
					final Boolean worklossBool = (Boolean) worklossObj;
					if (worklossBool != null)
						workLossID = (worklossBool == true) ? vocab.getCodeID("CONFIRM-01") : vocab.getCodeID("CONFIRM-02");
				}
			}
			// Insert Data
            final String insertSqlQuery = 
            	"insert into demo_occupation_data(PATIENT_ID, LOSS_OF_WORK_DUE_TO_PSS_ID, VISIT_ID) values " +
                "(" + patientID + ", " + workLossID + ", " + visitID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF OccupationJsonDataInsertion
	
	private static final class BodyWeightJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// BMI
			JSONObject bmiCV = null;
            try { bmiCV = (JSONObject) jo.get("bmi-CV"); } catch (Exception e) { }
            final Integer bmiID = (bmiCV != null) ? vocab.getCodeID(bmiCV.get("code-Value").toString()) : null;
            // Insert Data
            final String insertSqlQuery = 
                "insert into demo_weight_data(PATIENT_ID, BMI_CLASS_ID, VISIT_ID) values " +
                "(" + patientID + ", " + bmiID + ", " + visitID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
	        return 1;
		}
	} // END OF BodyWeightJsonDataInsertion
	
	/* ********************************************* Pregnancy & Smoking ********************************************* */
	
	private static final class PregnancyJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
            // Conception Date
            final Object conceptDateObj  = jo.get("pregnancy-Conception-Date");
            final Integer conceptDateID = (conceptDateObj != null) ? CoreDbService.insertDateOrPeriod(conceptDateObj, conn) : null;
            // Outcome
            JSONObject pregOutCV = null;
            try { pregOutCV = (JSONObject) jo.get("pregnancy-Outcome-CV"); } catch (Exception e) { }
            final Integer pregOutID = (pregOutCV != null) ? vocab.getCodeID(pregOutCV.get("code-Value").toString()) : null;
            // Outcome Date
            final Object outcomeDateObj  = jo.get("pregnancy-Outcome-Date");
            final Integer outcomeDateID = (outcomeDateObj != null) ? CoreDbService.insertDateOrPeriod(outcomeDateObj, conn) : null;
            // Insert Data
            final String insertSqlQuery = 
            	"insert into demo_pregnancy_data(PATIENT_ID, CONCEPTION_DATE_ID, OUTCOME_DATE_ID, OUTCOME_ID, SS_CONCORDANT_ID, VISIT_ID, STMT_ID) values " +
            	"(" + patientID + ", " + conceptDateID + ", " + outcomeDateID + ", " + pregOutID + ", null, " + visitID + ", " + stmtID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF PregnancyJsonDataInsertion
	
	private static final class TobaccoConsumptionJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
            // Smoking Status
			JSONObject smokStatCV = null;
            try { smokStatCV = (JSONObject) jo.get("tobacco-Consumption-Status-CV"); } catch (Exception e) { }
            final Integer smokStatID = (smokStatCV != null) ? vocab.getCodeID(smokStatCV.get("code-Value").toString()) : null;
			// Amount of Cigars
            JSONObject amount = null;
            try { amount = (JSONObject) jo.get("tobacco-Amount"); } catch (Exception e) { }
            final Integer amountID = (amount != null) ? CoreDbService.insertAmountOrInterval(amount, vocab, conn) : null;
            // Insert Data
            final String insertSqlQuery = 
            	"insert into lifestyle_smoking(PATIENT_ID, STATUS_ID, AMOUNT_ID, PERIOD_ID, VISIT_ID, STMT_ID) values " + 
            	"(" + patientID + ", " + smokStatID + ", " + amountID + ", null, " + visitID + ", " + stmtID + ")";
			CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF TobaccoConsumptionJsonDataInsertion
	
	/* ********************************************* Medical Conditions ********************************************* */
	
	private static final class SymptomJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
            // Symptom Code(s)
            JSONObject symptomCV = null;
            try { symptomCV = (JSONObject) jo.get("symptom-sign-CV"); } catch (Exception e) { }
            final List<String> codeList = new ArrayList<>();
            if (symptomCV.has("code-Value")) { 
            	codeList.add(symptomCV.get("code-Value").toString());
            } else {
            	final String op = symptomCV.has("boolean-Operator") ? symptomCV.getString("boolean-Operator") : null;
            	if (!op.equals("AND")) return -1;
            	final JSONArray ja = symptomCV.getJSONArray("expression-Code-CV-JA");
            	for (Object obj : ja) {
					final JSONObject termJO = (JSONObject) obj;
					codeList.add(termJO.get("code-Value").toString());
				}
            }
            if (codeList.isEmpty()) return -1;
            for (String symptomCode : codeList) {			
	            final Integer symptomID = (symptomCV != null) ? vocab.getCodeID(symptomCode) : null;
	            // Symptom Date or Period of Time
	            final Object dtObj  = jo.get("symptom-sign-Date");
	            final Integer dateID = (dtObj != null) ? CoreDbService.insertDateOrPeriod(dtObj, conn) : null;
	            // Insert Data
	            final String insertSqlQuery = 
	            	"insert into cond_symptom(PATIENT_ID, CONDITION_ID, OBSERVE_DATE_ID, VISIT_ID, STMT_ID) values " +
	                 "(" + patientID + ", " + symptomID + ", " + dateID + ", " + visitID + ", " + stmtID + ")";
	            CoreDbService.executeSQL(conn, insertSqlQuery);      
            }
            return 1;
		}
	} // END OF SymptomJsonDataInsertion

	private static final class DiagnosisJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
            // Diagnosis Code
			JSONObject diagnosisCV = null;
			try { diagnosisCV = (JSONObject) jo.get("diagnosis-CV"); } catch (Exception e) { }
            final int diagnosisID = vocab.getCodeID(diagnosisCV.get("code-Value").toString());
            // Diagnosis Date
            final Object diagDateObj = jo.get("diagnosis-Date");
            final Integer diagDateID = (diagDateObj != null) ? CoreDbService.insertDateOrPeriod(diagDateObj, conn) : null;
            // Organ(s)
            JSONArray diagOrganJA = null;
            try { diagOrganJA = (JSONArray) jo.get("diagnosis-Organ-CV-JA"); } catch (Exception e) { }
            // Stage
            JSONObject diagStageJO = null;
            try { diagStageJO = (JSONObject) jo.get("diagnosis-Stage-CV"); } catch (Exception e) { }
            final Integer diagStageID = 
                (diagStageJO != null) ? vocab.getCodeID(diagStageJO.get("code-Value").toString()) : null;    
            // Performance Status
            JSONObject performStatJO = null;
            try { performStatJO = (JSONObject) jo.get("diagnosis-Perform-Status-CV"); } catch (Exception e) { }
            final Integer performStatID = 
            	(performStatJO != null) ? vocab.getCodeID(performStatJO.get("code-Value").toString()) : null;
            // Insert Data
            final String insertSqlQuery =
            	"insert into cond_diagnosis(PATIENT_ID, CONDITION_ID, STAGE_ID, PERFORMANCE_STATUS_ID, DIAGNOSIS_DATE_ID, VISIT_ID, STMT_ID) values " +
                "(" + patientID + ", " + diagnosisID + ", " + diagStageID + ", " + performStatID + ", " + diagDateID + ", " + visitID + ", " + stmtID + ")";
            final int entryID = CoreDbService.executeInsertSQLandReturnID(conn, insertSqlQuery);

            // TODO: review..  FIXED Organ may not be a simple term but a combination of terms
            
            // Organ(s)
            if (diagOrganJA != null) {
            	for (Object object : diagOrganJA) {
					// Organ(s)
            		final JSONObject diagOrganJO = (JSONObject) object;
            		final List<String> diagOrganCodeList = new ArrayList<>();
                    if (diagOrganJO.has("code-Value")) { 
                    	diagOrganCodeList.add(diagOrganJO.get("code-Value").toString());
                    } else {
                    	final String op = diagOrganJO.has("boolean-Operator") ? diagOrganJO.getString("boolean-Operator") : null;
                    	if (!op.equals("AND")) {
                    		log.warn("Diagnosis (Row-ID: " + entryID + ") Organ being ignorred: " + diagOrganJO);
                    		continue;
                    	}
                    	final JSONArray ja = diagOrganJO.getJSONArray("expression-Code-CV-JA");
                    	for (Object obj : ja) {
        					final JSONObject termJO = (JSONObject) obj;
        					diagOrganCodeList.add(termJO.get("code-Value").toString());
        				}
                    } 
                    if (diagOrganCodeList.isEmpty()) continue;
                    for (String diagOrganCode : diagOrganCodeList) {
    	            	final Integer organID = vocab.getCodeID(diagOrganCode);
    	                // Insert Data
    	                final String insertOrganSQL = 
    	                	"insert into cond_diagnosis_organs(DIAGNOSIS_ID, ORGAN_ID) values " +
    	                    "(" + entryID + ", " + organID + ")";
    	                CoreDbService.executeSQL(conn, insertOrganSQL);
					}
            	}
            }
			
            return 1;
		}
	} // END OF DiagnosisJsonDataInsertion
	
	/* ********************************************* Interventions ********************************************* */

	private static final class MedicationJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
            // Pharmaceutical Drug
			JSONObject drugCV = null;
            try { drugCV = (JSONObject) jo.get("medication-CV"); } catch (Exception e) { }
            Integer drugID = (drugCV != null) ? vocab.getCodeID(drugCV.get("code-Value").toString()) : null;
            // Amount or Interval
            JSONObject dosage = null;
            try { dosage = (JSONObject) jo.get("medication-Dosage"); } catch (Exception e) { }
            final Integer dosageID = (dosage != null) ? CoreDbService.insertAmountOrInterval(dosage, vocab, conn) : null;
            // Period of Time
            JSONObject period = null;
            try { period = (JSONObject) jo.get("medication-Date-Interval"); } catch (Exception e) { }
            final Integer periodID = (period != null) ? CoreDbService.insertPeriod(period, conn) : null;
            // Insert Data
            final String insertSqlQuery = 
            	"insert into interv_medication(PATIENT_ID, MEDICATION_ID, DOSAGE_ID, PERIOD_ID, VISIT_ID, STMT_ID) values " +
            	"(" + patientID + ", " + drugID + ", " + dosageID + ", " + periodID + ", " + visitID + ", " + stmtID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF MedicationJsonDataInsertion
	
	private static final class ChemotherapyJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
            // Period of Time
            JSONObject period = null;
            try { period = (JSONObject) jo.get("chemotherapy-Date-Interval"); } catch (Exception e) { }
            final Integer periodID = (period != null) ? CoreDbService.insertPeriod(period, conn) : null;
            // Insert Data
            final String insertSqlQuery =
            	"insert into interv_chemotherapy(PATIENT_ID, DUE_TO_PSS_ID, PERIOD_ID, VISIT_ID, STMT_ID) values " + 
            	"(" + patientID + ", null, " + periodID + ", " + visitID + ", " + stmtID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF ChemotherapyJsonDataInsertion
	
	private static final class SurgeryJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			// Date
			final Object surgDateObj = jo.get("procedure-Date");
			final Integer surgDateID = (!isNullOrEmpty(surgDateObj)) ? CoreDbService.insertDateOrPeriod(surgDateObj, conn) : null;
            // Insert Data
            final String insertSqlQuery = 
            	"insert into interv_surgery(PATIENT_ID, DUE_TO_PSS_ID, SURGERY_DATE_ID , VISIT_ID, STMT_ID) values " + 
            	"(" + patientID + ", null, " + surgDateID + ", " + visitID + ", " + stmtID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF SurgeryJsonDataInsertion
	
	/* ********************************************* Lab Tests ********************************************* */
	
	private static final class LaboratoryTestJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// Test
			JSONObject testCV = null;
            try { testCV = (JSONObject) jo.get("test-CV"); } catch (Exception e) { }
            final Integer testID = (testCV != null) ? vocab.getCodeID(testCV.get("code-Value").toString()) : null;
            // Date
            final Object testDateObj = jo.get("test-Date");
            final Integer testDateID = (!isNullOrEmpty(testDateObj)) ? CoreDbService.insertDateOrPeriod(testDateObj, conn) : null;
            // Outcome
            final Object testOutcomeObj = jo.get("test-Outcome");
            final Integer termID;
            final Integer amountID;
            if (!isNullOrEmpty(testOutcomeObj)) {
                if (testOutcomeObj instanceof Boolean) { // Outcome is a Boolean
	            	final Boolean testOutcomeBool = (Boolean) testOutcomeObj;
	            	termID = (testOutcomeBool) ? vocab.getCodeID("CONFIRM-01") : vocab.getCodeID("CONFIRM-02");
	            	amountID = null;
	            } else {
	            	final JSONObject testOutcomeJO = (JSONObject) testOutcomeObj;
	            	if (testOutcomeJO.has("code-Value")) { // Outcome is another Term
	            		termID = vocab.getCodeID(testOutcomeJO.get("code-Value").toString());
	            		amountID = null;
	            	} else { // Outcome is an Amount
	            		termID = null;
	            		amountID = CoreDbService.insertAmountOrInterval(testOutcomeJO, vocab, conn);
	            	}
	            }
			} else {
				termID = null;
				amountID = null;
			}
            // Assessment 
            JSONObject testOutAssessCV = null;
            try { testOutAssessCV = (JSONObject) jo.get("test-Outcome-Assessment-Code"); } catch (Exception e) { }
            final Integer testOutAssessID = (testOutAssessCV != null) ? vocab.getCodeID(testOutAssessCV.get("code-Value").toString()) : null;
            // Normal Range 
            JSONObject testNormRange = null;
            try { testNormRange = (JSONObject) jo.get("test-Normal-Range"); } catch (Exception e) { }
            final Integer normRangeID = (testNormRange != null) ? CoreDbService.insertAmountInterval(testNormRange, vocab, conn) : null;
            // Insert Data
            final String insertSqlQuery = 
            	"insert into exam_lab_test(PATIENT_ID, TEST_ID, OUTCOME_AMOUNT_ID, OUTCOME_ASSESSMENT_ID, NORMAL_RANGE_ID, OUTCOME_TERM_ID, SAMPLE_DATE_ID, VISIT_ID) values " +
            	"(" + patientID + ", " + testID + ", " + amountID + ", " + testOutAssessID + ", " + normRangeID + ", " + termID + ", " + testDateID + ", " + visitID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF LaboratoryTestJsonDataInsertion
	
	private static final class MedicalImagingTestJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			// Test
			JSONObject testCV = null;
			try { testCV = (JSONObject) jo.get("test-CV"); } catch (Exception e) { }
			final Integer testID = (testCV != null) ? vocab.getCodeID(testCV.get("code-Value").toString()) : null;
			// Date
			final Object testDateObj = jo.get("test-Date");
			final Integer testDateID = (!isNullOrEmpty(testDateObj)) ? CoreDbService.insertDateOrPeriod(testDateObj, conn) : null;
			// Assessment
			JSONObject testOutAssessCV = null;
			try { testOutAssessCV = (JSONObject) jo.get("test-Outcome-Assessment-Code"); } catch (Exception e) { }
			final Integer testOutAssessID = (testOutAssessCV != null) ? vocab.getCodeID(testOutAssessCV.get("code-Value").toString()) : null;
			// Insert Data
			final String insertSqlQuery = 
	            "insert into exam_medical_imaging_test(PATIENT_ID, TEST_ID, ASSESSMENT_ID, TEST_DATE_ID, VISIT_ID) values " +
	            "(" + patientID + ", " + testID + ", " + testOutAssessID + ", " + testDateID + ", " + visitID + ")";
	        CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF MedicalImagingTestJsonDataInsertion
	
	private static final class BiopsyJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// Biopsy Type
			JSONObject typeCV = null;
            try { typeCV = (JSONObject) jo.get("biopsy-Type-CV"); } catch (Exception e) { }
            final Integer typeID = (typeCV != null) ? vocab.getCodeID(typeCV.get("code-Value").toString()) : null;
			// Biopsy Test
			JSONObject testCV = null;
            try { testCV = (JSONObject) jo.get("test-CV"); } catch (Exception e) { }
            final Integer testID = (testCV != null) ? vocab.getCodeID(testCV.get("code-Value").toString()) : null;
            // Date
            final Object testDateObj = jo.get("test-Date");
            final Integer testDateID = (!isNullOrEmpty(testDateObj)) ? CoreDbService.insertDateOrPeriod(testDateObj, conn) : null;
            // Outcome
            final Object testOutcomeObj = jo.get("test-Outcome");
            final Integer checkID;
            final Integer amountID;
            if (!isNullOrEmpty(testOutcomeObj)) {
                if (testOutcomeObj instanceof Boolean) { // Outcome is a Boolean
	            	final Boolean testOutcomeBool = (Boolean) testOutcomeObj;
	            	checkID = (testOutcomeBool) ? vocab.getCodeID("CONFIRM-01") : vocab.getCodeID("CONFIRM-02");
	            	amountID = null;
	            } else { // Outcome is an Amount
	            	final JSONObject testOutcomeJO = (JSONObject) testOutcomeObj;
	            	checkID = null;
	            	amountID = CoreDbService.insertAmountOrInterval(testOutcomeJO, vocab, conn);
	            }
			} else {
				checkID = null;
				amountID = null;
			}
            // Assessment 
            JSONObject testOutAssessCV = null;
            try { testOutAssessCV = (JSONObject) jo.get("test-Outcome-Assessment-Code"); } catch (Exception e) { }
            final Integer testOutAssessID = (testOutAssessCV != null) ? vocab.getCodeID(testOutAssessCV.get("code-Value").toString()) : null;
            // Normal Range 
            JSONObject testNormRange = null;
            try { testNormRange = (JSONObject) jo.get("test-Normal-Range"); } catch (Exception e) { }
            final Integer normRangeID = (testNormRange != null) ? CoreDbService.insertAmountInterval(testNormRange, vocab, conn) : null;
            // Insert Data
            final String insertSqlQuery = 
            	"insert into exam_biopsy(PATIENT_ID, BIOPSY_ID, TEST_ID, OUTCOME_AMOUNT_ID, NORMAL_RANGE_ID, ASSESSMENT_ID, OUTCOME_CHECK_ID, BIOPSY_DATE_ID, VISIT_ID) values " +
                "(" + patientID + ", " + typeID + ", " + testID + ", " + amountID + ", " + normRangeID + ", " + testOutAssessID + ", " + checkID + ", " + testDateID + ", " + visitID + ")";
			CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF BiopsyJsonDataInsertion	
	
	/* ********************************************* Questionnaires ********************************************* */
	
	private static final class QuestionnaireScoreJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// Questionnaire
			JSONObject questCV = null;
			try { questCV = (JSONObject) jo.get("score-CV"); } catch (Exception e) { }
			final Integer questID = (questCV != null) ? vocab.getCodeID(questCV.get("code-Value").toString()) : null;
			// Score
			String scoreValue = null;
			try { scoreValue = jo.get("questionnaire-Score").toString(); } catch (Exception e) { }
			// Assessment
			JSONObject assessCV = null;
			try { assessCV = (JSONObject) jo.get("score-Assessment-Code"); } catch (Exception e) { }
			final Integer assessID = (assessCV != null) ? vocab.getCodeID(assessCV.get("code-Value").toString()) : null;
			// Normal Range
			JSONObject normRange = null;
			try { normRange = (JSONObject) jo.get("score-Normal-Range"); } catch (Exception e) { }
			final Integer normRangeID = (!isNullOrEmpty(normRange)) ? CoreDbService.insertIntegerInterval(normRange, conn) : null;
			// Test Date
			final Object questDateObj = jo.get("completion-Date");
			final Integer questDateID = (!isNullOrEmpty(questDateObj)) ? CoreDbService.insertDateOrPeriod(questDateObj, conn) : null;
			// Insert Data
			final String insertSqlQuery = 
				"insert into exam_questionnaire_score(PATIENT_ID, SCORE_ID, VALUE, ASSESSMENT_ID , NORMAL_RANGE_ID, OTHER_TERM_ID, QUESTIONNAIRE_DATE_ID, VISIT_ID) values " +
				"(" + patientID + ", " + questID + ", " + scoreValue + ", " + assessID + ", " + normRangeID + ", null, " + questDateID +" , " + visitID + ")";
            
			CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF QuestionnaireScoreJsonDataInsertion
	
	private static final class ESSDAIDomainALJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// Domain
			JSONObject essdaiDomainCV = null;
            try { essdaiDomainCV = (JSONObject) jo.get("quest-ESSDAI-Domain-CV"); } catch (Exception e) { }
            final Integer essdaiDomainID = (essdaiDomainCV != null) ? vocab.getCodeID(essdaiDomainCV.get("code-Value").toString()) : null;
            // Activity Level
            JSONObject actLevCV = null;
            try { actLevCV = (JSONObject) jo.get("activity-Level-CV"); } catch (Exception e) { }
            final Integer actLevID = (actLevCV != null) ? vocab.getCodeID(actLevCV.get("code-Value").toString()) : null;
            // Date
         	final Object dateObj = jo.get("completion-Date");
         	final Integer dateID = (!isNullOrEmpty(dateObj)) ? CoreDbService.insertDateOrPeriod(dateObj, conn) : null;
			// Insert Data
         	final String insertSqlQuery = 
         		"insert into exam_essdai_domain(PATIENT_ID, DOMAIN_ID, ACTIVITY_LEVEL_ID, QUESTIONNAIRE_DATE_ID, VISIT_ID) values " +
         		"(" + patientID + ", " + essdaiDomainID + ", " + actLevID + ", " + dateID + ", " + visitID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF ESSDAIDomainALJsonDataInsertion
	
	private static final class CACIComorbidityJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// Comorbidity
            JSONObject comorCV = null;
            try { comorCV = (JSONObject) jo.get("quest-CACI-Comorbidity-CV"); } catch (Exception e) { }
            final Integer comorID = (comorCV != null) ? vocab.getCodeID(comorCV.get("code-Value").toString()) : null;
            // Check
            Boolean check = null;
			try { check = (Boolean) jo.get("condition-Presence-CV"); } catch (Exception e) { }
			final Integer checkID = ((check != null)) ? ((check == true) ? vocab.getCodeID("CONFIRM-01") : vocab.getCodeID("CONFIRM-02")) : null;
            // Date
			final Object dateObj = jo.get("completion-Date");
			final Integer dateID = (!isNullOrEmpty(dateObj)) ? CoreDbService.insertDateOrPeriod(dateObj, conn) : null;
			// Insert Data
            final String insertSqlQuery = 
            	"insert into exam_caci_condition(PATIENT_ID, CACI_ID, VALUE_ID, QUESTIONNAIRE_DATE_ID, VISIT_ID) values " +
            	"(" + patientID + ", " + comorID + ", " + checkID + ", " + dateID + ", " + visitID + ")";
			CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF CACIComorbidityJsonDataInsertion
	
	
	/* ********************************************* Other Data ********************************************* */
	
	private static final class ClinicalTrialJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			// Study Drug
            // JSONObject drugCV = null;
			// try { drugCV = (JSONObject) jo.get("intervention-CV"); } catch (Exception e) { }
			// final Integer drugID = (drugCV != null) ? vocab.getCodeID(drugCV.get("code-Value").toString()) : null;
            // Study Condtion
			// JSONObject medCondCV = null;
			// try { medCondCV = (JSONObject) jo.get("medical-Condition-CV"); } catch (Exception e) { }
			// final Integer medCondID = (medCondCV != null) ? vocab.getCodeID(medCondCV.get("code-Value").toString()) : null;
            // Study Period of time
            JSONObject period = null;
            try { period = (JSONObject) jo.get("study-Participation-Date-Interval"); } catch (Exception e) { }
            final Integer periodID = (period != null) ? CoreDbService.insertPeriod(period, conn) : null;
            // Insert Data
            final String insertSqlQuery = 
            	"insert into other_clinical_trials(PATIENT_ID, PERIOD_ID, VISIT_ID, STMT_ID) values " +
            	"(" + patientID + ", " + periodID + ", " + visitID + ", " + stmtID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF ClinicalTrialJsonDataInsertion
	
	private static final class FamilyHistoryJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			// Medical Condition
			JSONObject medCondCV = null;
            try { medCondCV = (JSONObject) jo.get("medical-Condition-CV"); } catch (Exception e) { }
            final Integer medCondID = 
                (medCondCV != null) ? vocab.getCodeID(medCondCV.get("code-Value").toString()) : null;
            // Relative Degree
            JSONObject relativDegCV = null;
            try { relativDegCV = (JSONObject) jo.get("relative-Degree-CV"); } catch (Exception e) { }
            final Integer relativDegID = 
                (relativDegCV != null) ? vocab.getCodeID(relativDegCV.get("code-Value").toString()) : null;
            // Insert Data
            final String insertSqlQuery = 
              	"insert into other_family_history(PATIENT_ID, MEDICAL_CONDITION_ID, RELATIVE_DEGREE_ID, VISIT_ID, STMT_ID) values " +
              	"(" + patientID + ", " + medCondID + ", " + relativDegID + ", " + visitID + ", " + stmtID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF FamilyHistoryJsonDataInsertion
	
	private static final class OtherHealthcareEntityVisitJsonDataInsertion implements JsonDataInsertionI {
		@Override public int insertJsonData(Vocab vocab, JSONObject jo, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
			if (stmtID == vocab.getCodeID("CONFIRM-02")) return -1;
			// HE specialist
			JSONObject specCV = null;
            try { specCV = (JSONObject) jo.get("other-HE-Speciality-CV"); } catch (Exception e) { }
            final Integer specID = 
            	(specCV != null) ? vocab.getCodeID(specCV.get("code-Value").toString()) : null;
            // HE Visit Date
            final Object dtObj = jo.get("other-he-Visit-Date");
            final Integer dtID = (!isNullOrEmpty(dtObj)) ? CoreDbService.insertDateOrPeriod(dtObj, conn) : null;
            // Insert data
            final String insertSqlQuery =
            	"insert into other_healthcare_visit(PATIENT_ID, SPECIALIST_ID, DATE_ID, VISIT_ID) values " +
            	"(" + patientID + ", " + specID + ", " + dtID + ", " + visitID + ")";
            CoreDbService.executeSQL(conn, insertSqlQuery);
            return 1;
		}
	} // END OF OtherHealthcareEntityVisitJsonDataInsertion
	
	// Family-History

	private static final boolean isNullOrEmpty(final Object obj) {
		if (obj == null) return true;
		final String str = "" + obj;
		return (str.trim().equals("") || str.trim().equals("null"));
	}
	
}
