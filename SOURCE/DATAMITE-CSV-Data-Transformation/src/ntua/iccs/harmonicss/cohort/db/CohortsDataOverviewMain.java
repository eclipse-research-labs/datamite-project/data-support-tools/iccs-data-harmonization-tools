package ntua.iccs.harmonicss.cohort.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.Map;

import org.timchros.core.util.Util;

public class CohortsDataOverviewMain {

	private static final String dbUsername = "cohort";
	private static final String dbPassword = "1exarchos2!";	
	
	private static final Map<String, String> dbCohortMap = Util.newHashMap(
		"chdb002", "IDIBPAS", 
		"chdb003", "UNPIG",
		"chdb005", "UPSUD-P", 
		"chdb006", "UoB",
		"chdb007", "UNIVAQ", 
		"chdb008", "ULB", 
		"chdb009", "HUA", 
		"chdb010", "UMCG",
		"chdb011", "UiB",
		"chdb013", "UOI",
		"chdb014", "UU", 
		"chdb015", "UNIRO", 
		"chdb016", "QMUL",
		"chdb018", "UMCU ???",
		"chdb020", "MHH",
		"chdb021", "UNIPI",
		"chdb022", "CUMB ???", // TODO: FIX
		"chdb023", "UBO ???",
		"chdb025", "UoA", 
		"chdb028", "AOUD", 
		"chdb029", "TEST-huser", 
		"chdb030", "TEST-hrexper",
		"chdb031", "UNEW",
		"chdb033", "TEST-harmonicss.user", // TODO: Maybe UPSUD-A
		"chdb034", "TEST-test1",
		"chdb035", "TEST-test2",
		"chdb036", "TEST-test3",
		"chdb037", "TEST-test4",
		"chdb038", "TEST-test5"
	);
	
	public static void main(String[] args) {

		System.out.println(" >> CohortsDataOverviewMain: START " + new Date());
		System.out.println();
		
		final StringBuilder sb =  new StringBuilder();
		
		int cohortCount = 0;
		int patientCount = 0;
		for (int i = 1; i < 38; i++) {
			final String dbName = getDbName(i);
			final String dbUrl = "jdbc:mysql://192.168.50.6:3306/" + dbName;
			sb.append(" - " + dbName);
			if (dbCohortMap.containsKey(dbName)) sb.append(" [" + dbCohortMap.get(dbName) + "]");
			final boolean status = JsonDataInsertionToDB.dataInsertionCheck(dbUrl, dbUsername, dbPassword);
			sb.append(" --> " + ((status) ? "YES" : ""));
			if (status) {
				cohortCount++;
				final int count = getPatientsCount(dbUrl, dbUsername, dbPassword);
				patientCount += count;
				sb.append(" :: " + count);
			}
			sb.append("\n");
		}
		sb.append("\n * Cohorts: " + cohortCount);
		sb.append("\n * Total number of patients: " + patientCount);
		
		System.out.println(sb.toString());
		
		System.out.println();
		System.out.println(" >> CohortsDataOverviewMain: END " + new Date());
		
	}
	
    public static int getPatientsCount(String dbUrl, String dbUsername, String dbPassword) {
    	Connection conn = null;
    	try {
	    	conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
	    	final Map<String, Integer> patientMap = JsonDataInsertionToDB.getPatientIDs(conn);
	        return patientMap.size();
    	} catch(Throwable t) {
    		final String ermsg = "getPatientsCount(" + dbUrl + ", " + dbUsername + ", ... ) problem";
    		throw new RuntimeException(ermsg, t);
    	} finally {
    		if (conn != null) { try { conn.close(); } catch(Throwable t3) {} }
    	}
    }
    
	private static String getDbName(final int index) {
		if (index < 10) return "chdb00" + index;
		if (index < 100) return "chdb0" + index;
		return "chdb" + index;
	}

}
