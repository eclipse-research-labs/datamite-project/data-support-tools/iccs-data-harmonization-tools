package ntua.iccs.harmonicss.cohort.db;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.timchros.core.log.messages.MemoryMessage;
import org.timchros.core.util.Clock;
import org.timchros.core.util.Throable;
import org.timchros.core.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.LogFactory;

/**
 * Provides the functionality for inserting the patient data stored in a JSON File in the given DB
 */
public class JsonDataInsertionToDB {
	
	public static final Logger log = LogFactory.getLoggerForClass(JsonDataInsertionToDB.class);
	
	static {
		final String driverClass = "com.mysql.jdbc.Driver";
		try {
			Class.forName(driverClass).getDeclaredConstructor().newInstance();
		} catch (Throwable t) {
			final String ermsg = "Cannot Find Driver Java Class: " + driverClass;
			log.error(ermsg, t);
			throw new RuntimeException("", t);
		}
	}
	
	/** Current JSON to DB Process Status */
	public static final class ProcessStatus {

//		private static String msg = null;
//		
//		public static synchronized void setDatabaseInsertionStatusEmpty() { msg = null; }
//		
//		public static synchronized void setDatabaseInsertionStatus(final String str) { msg = str; }
//		
//		public static String getDatabaseInsertionStatus() { return msg; }
//		
		private static boolean runFlag = false;
		private static String latestMsg = null;
		
		public static void setRunFlagTRUE() { runFlag = true; }
		public static void setRunFlagFALSE() { runFlag = false; }
		public static boolean getRunStatus() { return runFlag; }
		
		public static synchronized void setMsg(final String msg) { latestMsg = msg; }
		public static String getMsg() { return latestMsg; }
		
	}
	
	/** A synopsis of the JSON data inserted in the the DB  */
	public static final class DataInsertionResponse { 
		
		private static final String NL = "\n";
		
		final int patientNumber;
		final List<String> insertDataType;
		final List<String> ignoreDataType;
		
		public DataInsertionResponse(int patientNumber, List<String> insertDataType, List<String> ignoreDataType) {
			super();
			this.patientNumber = patientNumber;
			this.insertDataType = insertDataType;
			this.ignoreDataType = ignoreDataType;
		}

		public int getPatientNumber() { return patientNumber; }

		public List<String> getInsertDataType() { return insertDataType; }

		public List<String> getIgnoreDataType() { return ignoreDataType; }

		public JSONObject asJson() {
			final JSONObject jo = new JSONObject();
			jo.put("patientNumber", patientNumber);
			final JSONArray recDataTypeJA = new JSONArray();
			for (String dt : insertDataType) {
				recDataTypeJA.add(dt);
			}
			jo.put("recDataTypeJA", recDataTypeJA);
			final JSONArray ignDataTypeJA = new JSONArray();
			for (String dt : ignoreDataType) {
				ignDataTypeJA.add(dt);
			}
			jo.put("ignDataTypeJA", ignDataTypeJA);
			return jo;
		}
		
		public static DataInsertionResponse fromJsonObject(final JSONObject jo) {
			final int patientNumber = jo.getInt("patientNumber");
			final JSONArray recDataTypeJA = jo.getJSONArray("recDataTypeJA");
			final List<String> insertDataTypeList = new ArrayList<>();
			for (Object recDataType : recDataTypeJA) {
				insertDataTypeList.add("" + recDataType);
			}
			final JSONArray ignDataTypeJA = jo.getJSONArray("ignDataTypeJA");
			final List<String> ignoreDataTypeList = new ArrayList<>();
			for (Object ignDataType : ignDataTypeJA) {
				ignoreDataTypeList.add("" + ignDataType);
			}
			return new DataInsertionResponse(patientNumber, insertDataTypeList, ignoreDataTypeList);
		}
		
		public String asString() {
			return 
				"PatientNumber: " + patientNumber + NL +
				" - Data Types Inserted: " + Util.listToOneLineString(insertDataType) + NL +
				" - Data Types Ignored: " + Util.listToOneLineString(ignoreDataType);
		}
		
		@Override
		public String toString() {
			return "DataInsertionResponse [patientNumber=" + patientNumber + ", insertDataType=" + insertDataType + ", ignoreDataType=" + ignoreDataType + "]";
		}
		
	}
	
    /** Ensure that we wont create an instance of this class */
    private JsonDataInsertionToDB() { }
    
	/* ********************* Check DB ********************* */
	
    private static final String harmVisitCountSQL = 
    	"SELECT count(*) as NUM FROM patient_visit, voc_visit_type WHERE visit_type_id = voc_visit_type.id and voc_visit_type.CODE = 'VISIT-00'";
    
    /** @return <code>true</code> if there is at least one Harmonization Visit, otherwise <code>false</code> */
    public static boolean dataInsertionCheck(String dbUrl, String dbUsername, String dbPassword) {
    	log.info("dataInsertionCheck(" + dbUrl + ", " + dbUsername + ", ... ) ... ");
    	Connection conn = null;
    	Statement stmt = null;
    	ResultSet rs = null;
    	try {
	    	conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
	    	stmt = conn.createStatement();
	        rs = stmt.executeQuery(harmVisitCountSQL);
	        final int count = (rs.next()) ? rs.getInt("NUM") : -1;
	        final boolean resp = (count > 0);
	        log.info("dataInsertionCheck(..) :: Data already Inserted ? " + resp);
	        return resp;
    	} catch(Throwable t) {
    		final String ermsg = "dataInsertionCheck(" + dbUrl + ", " + dbUsername + ", ... ) problem";
    		log.error(ermsg, t);
    		throw new RuntimeException(ermsg, t);
    	} finally {
    		if (rs != null) { try { rs.close(); } catch(Throwable t1) {} }
    		if (stmt != null) { try { stmt.close(); } catch(Throwable t2) {} }
    		if (conn != null) { try { conn.close(); } catch(Throwable t3) {} }
    	}
    }

    /* ********************* Empty DB ********************* */
    
    /** SQL Queries for Empty Tables with the appropriate order */
    private static final List<String> emptySqlList = Util.newArrayList(
    	"truncate cohort",
    	/* Demographics */
    	"delete from demo_sex_data",
    	"delete from demo_ethnicity_data",
    	"delete from demo_education_level_data",
    	"delete from demo_occupation_data",
    	"delete from demo_weight_data",
    	"delete from demo_pregnancy_data",
    	"delete from lifestyle_smoking",
    	/* Medical Conditions */
    	"delete from cond_diagnosis_organs", //"truncate table cond_diagnosis_organs",
    	"delete from cond_diagnosis",
    	"delete from cond_symptom",
    	/* Interventions */
    	"delete from interv_medication",
    	"delete from interv_chemotherapy",
    	"delete from interv_surgery",
    	/* Lab Tests */
    	"delete from exam_lab_test",
    	"delete from exam_biopsy",
    	"delete from exam_medical_imaging_test",
    	/* Questionnaires */
    	"delete from exam_questionnaire_score",
    	"delete from exam_essdai_domain",
    	"delete from exam_caci_condition",
    	/* Other Data */
    	"delete from other_clinical_trials",
    	"delete from other_family_history",
    	"delete from other_healthcare_visit",
    	/* **************** Rest Data **************** */
    	"delete from dt_amount_range",
    	"delete from dt_amount",
    	"delete from dt_int_range",
    	"delete from dt_period_of_time",
    	"delete from patient_visit",
    	"delete from patient",
    	"delete from dt_date"
    );
    
    /** Empty DB and return the numbers of rows affected */
    public static int emptyDB(String dbUrl, String dbUsername, String dbPassword) {
    	Connection conn = null;
    	int count = 0;
    	try {
    		log.info("emptyDB(" + dbUrl + ", " + dbUsername + ", " + dbPassword + ") ...");
    		// DB connection
	    	conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
	    	// Execute Queries one by one
	    	for (String emptySql : emptySqlList) {
	    		Statement stmt = null;
	    		try {
		    		stmt = conn.createStatement();
		    		count += stmt.executeUpdate(emptySql);
		    	} catch (Throwable t) {
					throw new RuntimeException("SQL Query Execution: " + emptySql, t);
				} finally {
					if (stmt != null) try { stmt.close(); } catch (Exception t2) { }
				}
			} // END OF Loop
	    	log.info("emptyDB() :: Successfully Completed ! Rows affected: " + count);
    	} catch(Throwable t) {
    		final String ermsg = "emptyDB(" + dbUrl + ", " + dbUsername + ", " + dbPassword + ") problem";
    		log.error(ermsg, t);
    		throw new RuntimeException(ermsg, t);
    	} finally {
    		if (conn != null) try { conn.close(); } catch (Exception t2) { }
		}
    	return count;
    }
	
    /* ********************* Insert Data ********************* */
    
	/** Loads the JSON data from File and accordingly stores in the appropriate tables of the given DB */
	public static DataInsertionResponse insertJsonDataToDB(String jsonFilePath, String dbUrl, String dbUsername, String dbPassword) {
		try {
			log.info("insertJsonDataToDB(" + jsonFilePath + ", " + dbUrl + ", " + dbUsername + ", ... ) ...");
			
			// Indicate that JSON to DB started
			ProcessStatus.setMsg("Inserting Cohort Data");
			
			log.debug("BEFORE GC " + new MemoryMessage().toParsableString());
			System.gc();
			log.debug("AFTER  GC " + new MemoryMessage().toParsableString());
			
			// Load JSON Data from File
			ProcessStatus.setMsg("Reading JSON File with Patient Data");
			final String theJOstr = new String(Files.readAllBytes(Paths.get(jsonFilePath)));
			log.debug("JSON File Successfully Loaded.");
			final Clock clock = new Clock(); clock.start();
			final String dateStr = getDateHarmProcessRun(theJOstr);
			final String cohortStr = getCohortStr(theJOstr);
			final List<String> personStrList = getPatientStrList(theJOstr);
			clock.stop();
			log.debug("JSON File Successfully \"Parsed\" in " + clock.getDurationInMilliSeconds() + "  ms !");
			
			log.debug("BEFORE GC " + new MemoryMessage().toParsableString());
			System.gc();
			log.debug("AFTER  GC " + new MemoryMessage().toParsableString());
			
			// Open Connection to DB
			final Connection conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
			
			// Load Vocabularies from DB
			final Vocab vocab = new Vocab(conn);
			
			// Load Patient IDs
			final Map<String, Integer> patUidKeyMap = getPatientIDs(conn);
			
			// Process JSON data and insert them in the DB
			final String harmDate = dateStr;
					
			// Cohort Metadata
		    final JSONObject cohort = JSONObject.fromObject(cohortStr);
		    cohortInsertion(cohort, conn);
		    
		    // Cohort Patient Data
		    int patientCount = 0;
		    for (String personStr : personStrList) {
		    	patientCount++;
		    	final String msg = df.format(new Date()) + " :: Patient no. " + patientCount + " / " + personStrList.size();
		    	ProcessStatus.setMsg(msg);
		    	log.debug(" - " + msg);
		    	// Patient Data
		    	final JSONObject personJO = JSONObject.fromObject(personStr);
		    	String uid = "";
	            try { uid = personJO.get("person-Unique-ID").toString(); } catch (Exception e) { }
	            String birthDate = null;
	            try { birthDate = personJO.get("person-Date-Birth").toString(); } catch (Exception e) { }
	            String diagonsisDate = null;
	            try { diagonsisDate = personJO.get("person-SS-Diagnosis").toString(); } catch (Exception e) { }
	            String symptomsDate = null;
	            try { symptomsDate =  personJO.get("person-SS-Symptoms-Onset").toString(); } catch (Exception e) { }
	            String cohortInclDate = null;
	            try { cohortInclDate = personJO.get("person-Cohort-Inclusion").toString(); } catch (Exception e) { }
	            
	            // Insert Patient Data and Return ID
	            final int patientID;
	            if (patUidKeyMap.containsKey(uid)) {
	            	patientID = patUidKeyMap.get(uid);
	            	log.info("Patient with the given UID already exists !");
	            } else {
	            	patientID = insertPatientData(vocab, uid, birthDate, diagonsisDate, symptomsDate, cohortInclDate, conn);
	            }
	            
	            // Insert Visit Data and Return ID
	            final int visitID = insertVisitData(vocab, patientID, harmDate, conn);
	            
	            /* **************** Insert Patient Data ***************** */
	            
	            DataService.emptyDataTypesInsertSet();
	            DataService.emptyDataTypesIgnSet();
	            
	            // Positive Statements
	            JSONObject positiveStatementsJO = null;
	            try { positiveStatementsJO = (JSONObject) personJO.get("POSITIVE-STMT"); } catch (Exception e) { }
	            if(positiveStatementsJO != null) {
	                final int stmtID = vocab.getCodeID("CONFIRM-01"); // TRUE
	                insertDataBunch(vocab, positiveStatementsJO, patientID, visitID, stmtID, conn);
	            }
	            // Negative Statements
	            JSONObject negativeStatementsJO = null;
	            try { negativeStatementsJO = (JSONObject) personJO.get("NEGATIVE-STMT"); }  catch (Exception e) { }
	            if(negativeStatementsJO != null) {
	            	final int stmtID = vocab.getCodeID("CONFIRM-02"); // FALSE
	            	insertDataBunch(vocab, negativeStatementsJO, patientID, visitID, stmtID, conn);
	            }
	            
		    } // END OF Persons Loop 
		    
		    final DataInsertionResponse response = new DataInsertionResponse(personStrList.size(), DataService.getDataTypesInsertList(), DataService.getDataTypesIgnList());
		    
		    // Indicate that the JSON to DB process completed
		 	ProcessStatus.setMsg("Successfully ! " + response.asString());
		    
		 	log.info("insertJsonDataToDB(...) :: Successfully Completed !\n" + response.asString());
		 	
	        return response;
		} catch (Throwable t) {
			// Indicate that the JSON to DB process completed
			ProcessStatus.setMsg("Error: " + t.getClass().getSimpleName() + " :: " + t.getMessage());
			final String ermsg = "insertJsonDataToDB(" + jsonFilePath + ", " + dbUrl + ", " + dbUsername + ", ... ) problem";
			log.error(ermsg, t);
			throw new RuntimeException(ermsg, t);
		}
	}
	
	private static final String GetPatientsSQL = "SELECT ID, UID FROM patient";
	
	public static Map<String, Integer> getPatientIDs(Connection conn) throws SQLException {
		final Map<String, Integer> idsKeysMap = new HashMap<String, Integer>();
		final Statement stmt = conn.createStatement();
        final ResultSet rs = stmt.executeQuery(GetPatientsSQL);
        while(rs.next()) {
        	final int id = rs.getInt("ID");
            final String code = rs.getString("UID");
            idsKeysMap.put(code, id);
        }
        rs.close();
        stmt.close();
		
		return idsKeysMap;
	}
	
	private static int insertPatientData(Vocab vocab, String uid, String birthDate, String diagonsisDate, String symptomsDate, String cohortInclDate, Connection conn) throws Exception {
		// Insert Dates
		final Integer birthDateID = 
			(birthDate != null) ? CoreDbService.insertDate(birthDate, conn) : null;
		final Integer symptomsDateID = 
			(symptomsDate != null) ? CoreDbService.insertDate(symptomsDate, conn) : null;
		final Integer diagonsisDateID = 
			(diagonsisDate != null) ? CoreDbService.insertDate(diagonsisDate, conn) : null;
		final Integer cohortInclDateID = 
			(cohortInclDate != null) ? CoreDbService.insertDate(cohortInclDate, conn) : null;
		final Integer lastFollowUpDateID = null;
		// Insert Data
		final String insertPatientSQL = 
			"insert into patient(UID, DATE_OF_BIRTH_ID, PSS_SYMPTOMS_ONSET_DATE_ID, PSS_DIAGNOSIS_DATE_ID, COHORT_INCLUSION_DATE_ID, LAST_FOLLOW_UP_DATE_ID) values " + 
			"('" + uid + "', " + birthDateID + ", " + symptomsDateID + ", " + diagonsisDateID + ", " + cohortInclDateID + ", " + lastFollowUpDateID + ")";
		int patientID = CoreDbService.executeInsertSQLandReturnID(conn, insertPatientSQL);
		return patientID;
	}
	
	private static int insertVisitData(Vocab vocab, int patientID, String harmDate, Connection conn) throws Exception {
		// Date
		final Integer harmDateID = CoreDbService.insertDate(harmDate, conn);
		// Visit Status and Type
        int visitStatusID = vocab.getCodeID("VIS-STAT-02");
        int visitTypeID = vocab.getCodeID("VISIT-00");
        // Insert Data
        final String insertVisitSQL = 
        	"insert into patient_visit(PATIENT_ID, VISIT_DATE_ID, VISIT_TYPE_ID, VISIT_STATUS_ID) values " + 
        	"('" + patientID + "', '" + harmDateID + "', '" + visitTypeID + "', '" + visitStatusID + "')";
        int visitID = CoreDbService.executeInsertSQLandReturnID(conn, insertVisitSQL);
		return visitID;
	}
	
	private static void insertDataBunch(Vocab vocab, JSONObject statementsJO, int patientID, int visitID, int stmtID, Connection conn) throws Exception {
		// For each category
		for (Object key : statementsJO.keySet()) {
			final JSONArray dataObjJA = statementsJO.getJSONArray("" + key);
			// For each data - object
			for (Object dataObj : dataObjJA) {
				final JSONObject dataJO = (JSONObject) dataObj;
				// Insert Data
				DataService.insertJsonData(vocab, dataJO, patientID, visitID, stmtID, conn);
			}
			
		} // END OF categories Loop
	}
	
    private static void cohortInsertion(JSONObject cohort, Connection connection) throws SQLException {
    	final PreparedStatement  cohortst = connection.prepareStatement("insert into cohort(PARAMETER, VALUE) values (?, ?)");
    	JSONArray terms = null;
    	try { terms = (JSONArray) cohort.get("term-JA");  } catch (Exception e) { }
        
    	try {
	    	if(terms != null) {
	    		for(int k=0; k<terms.size(); k++) {
	    			final JSONObject term = (JSONObject)terms.get(k);
	    			String code = "";
	    			try { code = term.get("code").toString(); } catch (Exception e) { }
	    			String label = "";
	    			try { label = term.get("label").toString(); } catch (Exception e) { }
	    			cohortst.setString(1, code);
	    			cohortst.setString(2, label);
	    			cohortst.addBatch();
	    		}
	    		cohortst.executeBatch();
	    	}
    	} catch (Throwable t) {
    		log.error("Cohort Metadata Insertion problem - Error being ignorred: " + Throable.throwableAsSimpleOneLineString(t));
    	}
    }
    
	/* ******************************* COARSE JSON PARSER ******************************* */
	
	private static final String getDateHarmProcessRun(final String theJOstr) {
		final String DATE_PROP = "\"date-Harmonization-Process-Run\"";
		int datePropStartIndex = theJOstr.indexOf(DATE_PROP);
		if (datePropStartIndex < 0) throw new RuntimeException("JSON parse problem... datePropStartIndex: " + datePropStartIndex);
		int dateStartIndex = theJOstr.indexOf("\"", datePropStartIndex + DATE_PROP.length() + 1);
		if (dateStartIndex < 0) throw new RuntimeException("JSON parse problem... dateStartIndex: " + dateStartIndex);
		int dateEndIndex = theJOstr.indexOf("\"", dateStartIndex + 1);
		if (dateEndIndex < 0) throw new RuntimeException("JSON parse problem... dateEndIndex: " + dateEndIndex);
		return theJOstr.substring(dateStartIndex + 1, dateEndIndex).trim();
	}
	
	private static final String getCohortStr(final String theJOstr) {
		final String COHORT = "\"cohort\"";
		int cohortStartIndex = theJOstr.indexOf(COHORT);
		if (cohortStartIndex > 0) cohortStartIndex = theJOstr.indexOf('{', cohortStartIndex); 
		if (cohortStartIndex < 0) throw new RuntimeException("JSON parse problem... cohortStartIndex: " + cohortStartIndex);
		int cohortEndIndex = theJOstr.indexOf("number-Patients", cohortStartIndex);
		if (cohortEndIndex > 0) cohortEndIndex = theJOstr.indexOf('}', cohortEndIndex);
		if (cohortEndIndex < 0) throw new RuntimeException("JSON parse problem... cohortEndIndex: " + cohortEndIndex);
		return theJOstr.substring(cohortStartIndex, cohortEndIndex + 1);
	}
	
	private static final List<String> getPatientStrList(final String theJOstr) {
		// Find person-JA String
		final String PERSON_JA = "\"person-JA\"";
		int jaStartIndex = theJOstr.indexOf(PERSON_JA);
		if (jaStartIndex > 0) jaStartIndex = theJOstr.indexOf('[', jaStartIndex); 
		if (jaStartIndex > 0) jaStartIndex++;
		int jaEndIndex = theJOstr.lastIndexOf(']');
		if (jaStartIndex < 0 || jaEndIndex < 0 || jaStartIndex > jaEndIndex)
			throw new RuntimeException("JSON parse problem... jaStartIndex: " + jaStartIndex + " , jaEndIndex: " + jaEndIndex);
		final String personJAstr = theJOstr.substring(jaStartIndex, jaEndIndex).trim();
		// Split String by Person
		final List<String> strList = new ArrayList<>();
		final String[] strArray = (" " + personJAstr).split(",?\\s*\\{\\s*\"type\":\\s?\"Person\",");
		for (String str : strArray) {
			if (str.trim().equals("")) continue;
			final String joStr = "{" + str;
			strList.add(joStr);
		}
		return strList;
	}    
    
	/* ******************************* TESTING ******************************* */
	
	private static final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss.SSS");
	
	// JSON FILES
	private static final List<String> jsonFilePathList = Util.newArrayList(
		"./@ICCS-Cohorts-Metadata/UoA/UoA-HARMONIZED-Cohort-FAKE-Data.json"//,
//		"./@ICCS-Cohorts-Metadata/UOI/UOI-HARMONIZED-Cohort-FAKE-Data.json",
//		"./@ICCS-Cohorts-Metadata/HUA/HUA-HARMONIZED-Cohort-FAKE-Data.json",
//		"./@ICCS-Cohorts-Metadata/QMUL/QMUL-HARMONIZED-Cohort-FAKE-Data.json",
//		"./@ICCS-Cohorts-Metadata/UU/UU-HARMONIZED-Cohort-FAKE-Data.json",
//		"./@ICCS-Cohorts-Metadata/UiB/UiB-HARMONIZED-Cohort-FAKE-Data.json",
//		"./@ICCS-Cohorts-Metadata/UNIRO/UNIRO-HARMONIZED-Cohort-FAKE-Data.json",
//		"./@ICCS-Cohorts-Metadata/UNIPI/UNIPI-HARMONIZED-Cohort-FAKE-Data.json"//,
//		"./@ICCS-Cohorts-Metadata/LARGE/UNIPI-HARMONIZED-Cohort-FAKE-Data-200.json",
//		"./@ICCS-Cohorts-Metadata/LARGE/UNIPI-HARMONIZED-Cohort-FAKE-Data-1000.json"
//		"./@ICCS-Cohorts-Metadata/OTHER-JSON/UU-HARMONIZED-Cohort-FAKE-Data-NEW.json"
//		"./@ICCS-Cohorts-Metadata/OTHER-JSON/UoB-Cohort-Harmonized-Data_v15.json"
//		"./@ICCS-Cohorts-Metadata/OTHER-JSON/UNIRO-HARMONIZED-Cohort-FAKE-Data-NEW.json"
//		"./@ICCS-Cohorts-Metadata/OTHER-JSON/UOI-Real-Local-Cohort-Harmonized-Data.json"
//		"./@ICCS-Cohorts-Metadata/OTHER-JSON/UNIVAQ-Real-Local-Cohort-Harmonized-Data.json"
//		"./@ICCS-Cohorts-Metadata/OTHER-JSON/NEW/QMUL-Cohort-Harmonized-Data.json"
//		"./@ICCS-Cohorts-Metadata/OTHER-JSON/NEW/CUMB-Cohort-Harmonized-Data.json"
	);
	
	
	// HarmonicSS DB - ponte.grid.ece.ntua.gr
	private static final String dbUrl = "jdbc:mysql://147.102.19.66:3306/TEST-HARMM"; // E // F --> Fake UNIPI
	private static final String dbUsername = "emps";
	private static final String dbPassword = "emps";
	
//	// HarmonicSS DB - ponte.grid.ece.ntua.gr
//	private static final String dbUrl = "jdbc:mysql://147.102.19.66:3306/Harm-DB-10"; // E // F --> Fake UNIPI
//	private static final String dbUsername = "emps";
//	private static final String dbPassword = "emps";
	
//	private static final String dbUrl = "jdbc:mysql://192.168.50.6:3306/chdb036";
//	private static final String dbUsername = "cohort";
//	private static final String dbPassword = "1exarchos2!";	
	
//	// Cloud DB
//	private static final String dbUrl = "jdbc:mysql://192.168.50.6:3306/chdb030";
//	private static final String dbUsername = "hrexpert";
//	private static final String dbPassword = "1hrexpert2!";	
	
//	// HarmonicSS huser DB
//	private static final String dbUrl = "jdbc:mysql://192.168.50.6:3306/chdb029";
//	private static final String dbUsername = "cohort";
//	private static final String dbPassword = "1exarchos2!";	
	
//	private static final String dbUrl = "jdbc:mysql://192.168.50.6:3306/chdb022";
//	private static final String dbUsername = "cohort";
//	private static final String dbPassword = "1exarchos2!";	
	
//	// BioIRC Test DB
//	private static final String dbUrl = "jdbc:mysql://192.168.50.6:3306/chdb036";
//	private static final String dbUsername = "cohort";
//	private static final String dbPassword = "1exarchos2!";	
	
//	// CERTH Test DB
//	private static final String dbUrl = "jdbc:mysql://192.168.50.6:3306/chdb035";
//	private static final String dbUsername = "cohort";
//	private static final String dbPassword = "1exarchos2!";	
	
	private static boolean justClean = true; // false; // true;
	
	/** For testing purposes... */
	public static void main(String[] args) {

		System.out.println(" >> DataInsertionMain: Process STARTED at " + new Date());
		System.out.println();
		
		System.out.println("BEFORE GC " + new MemoryMessage().toParsableString());
		System.gc();
		System.out.println("AFTER  GC " + new MemoryMessage().toParsableString());
		System.out.println();
		
		System.out.println("Number of JSON Files: " + jsonFilePathList.size());
		
		//for (int i = 0; i < 100; i++)
		for (String jsonFilePath : jsonFilePathList) {
			System.out.println();
			System.out.println(" ** About File: " + jsonFilePath);
			
			System.out.println();
			System.out.println(df.format(new Date()) + " :: Harmonization Data Insertion Check: " + dbUrl + " , " + dbUsername + " , " + dbPassword + " ... ");
			final boolean dataInsertionCheck = dataInsertionCheck(dbUrl, dbUsername, dbPassword);
			System.out.println(df.format(new Date()) + " :: Harmonization Data Insertion Check : " + dataInsertionCheck);
			
			System.out.println();
			final Clock emtClock = new Clock(); 
			System.out.println(df.format(new Date()) + " :: Empty DB ... " + dbUrl + " , " + dbUsername + " , " + dbPassword);
			emtClock.start();
			final int emptyResponse  = emptyDB(dbUrl, dbUsername, dbPassword);
			emtClock.stop();
			System.out.println(df.format(new Date()) + " :: Empty DB ... Successuflly Completed in " + emtClock.getDurationInMilliSeconds() + " ms ! Rows Affected: " + emptyResponse);
			
			if (justClean) return;
			
			System.out.println();
			final Clock insClock = new Clock(); 
			System.out.println(df.format(new Date()) + " :: Insert JSON data to DB ... " + dbUrl + " , " + dbUsername + " , " + dbPassword);
			insClock.start();
			final DataInsertionResponse insertResponse = insertJsonDataToDB(jsonFilePath, dbUrl, dbUsername, dbPassword);
			insClock.stop();
			System.out.println(df.format(new Date()) + " :: " + insertResponse.asString());
			System.out.println("Time Elapse: " + insClock.getDurationInMilliSeconds() + " ms");
			
			System.out.println();
			System.out.println("BEFORE GC " + new MemoryMessage().toParsableString());
			System.gc();
			System.out.println("AFTER  GC " + new MemoryMessage().toParsableString());
			
		} // END OF JSON FILE LIST
	
		System.out.println();
		System.out.println("BEFORE GC " + new MemoryMessage().toParsableString());
		System.gc();
		System.out.println("AFTER  GC " + new MemoryMessage().toParsableString());	 
			 
		System.out.println();
		System.out.println(" >> DataInsertionMain: Process COMPLETED at " + new Date());
	}
	
//	/** For testing purposes... */
//	public static void main(String[] args) throws Exception {
//
//		System.out.println(" >> JsonFileParseMain: Process STARTED at " + new Date());
//		System.out.println();
//		
//		for (String jsonFilePath : jsonFilePathList) {
//			System.out.println(" ** About File: " + jsonFilePath + "\n");
//			
//			final String theJOstr = new String(Files.readAllBytes(Paths.get(jsonFilePath)));
//			
//			final Clock newclock = new Clock(); newclock.start();
//
//			final String dateStr = getDateHarmProcessRun(theJOstr);
//			// System.out.println(dateStr);
//			
//			final String cohortStr = getCohortStr(theJOstr);
//			final JSONObject cjo = JSONObject.fromObject(cohortStr);
//			// System.out.println(cjo.toString());
//			
//			final List<String> personStrList = getPatientStrList(theJOstr);
//			System.out.println("Patients.. " + personStrList.size() + "\n");
//			int patientCount = 1;
//			for (String personStr : personStrList) {
//				System.out.println(df.format(new Date()) + " :: Patient no. " + patientCount++);
//				final JSONObject pjo = JSONObject.fromObject(personStr);
//				// System.out.println(pjo.toString());
//			}
//			
//			newclock.stop();
//			System.out.println("\nJSON File Successfully Parsed in " + newclock.getDurationInMilliSeconds() + "  ms !");
//			
//		} // END OF JSON FILE LIST
//	
//		System.out.println();
//		System.out.println(" >> JsonFileParseMain: Process COMPLETED at " + new Date());
//	}
	
}