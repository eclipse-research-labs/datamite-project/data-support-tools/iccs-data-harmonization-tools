package ntua.iccs.harmonicss.cohort.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.timchros.core.log.messages.MemoryMessage;
import org.timchros.core.util.Util;

/** 
 * Loads the Vocabularies (i.e., controlled set of terms) from the appropriate Tables 
 * and accordingly enables user to retrieve the ID of each Code
 */
class Vocab {

	/** SQL Queries for retrieving the controlled set of terms from the corresponding DB tables */
	private static final List<String> codesSQLqueryList = Util.newArrayList(
		"select * from voc_activity_level",
		"select * from voc_ana_pattern",
		"select * from voc_assessment",
		"select * from voc_biopsy",
		"select * from voc_bmi_class",
		"select * from voc_caci_condition",
		"select * from voc_confirmation",
		"select * from voc_cryo_type",
		"select * from voc_direction",
		"select * from voc_education_level",
		"select * from voc_essdai_domain",
		"select * from voc_ethnicity",
		"select * from voc_exam_outcome_type",
		"select * from voc_ipi_risk",
		"select * from voc_lab_test",
		"select * from voc_lab_test_type",
		"select * from voc_lymphoma_organ",
		"select * from voc_lymphoma_stage",
		"select * from voc_medical_condition",
		"select * from voc_medical_imaging_test",
		"select * from voc_performance_status",
		"select * from voc_pharm_drug",
		"select * from voc_pregnancy_outcome",
		"select * from voc_questionnaire",
		"select * from voc_relative_degree",
		"select * from voc_sex",
		"select * from voc_smoking_status",
		"select * from voc_specialist",
		"select * from voc_symptom_sign",
		"select * from voc_unit",
		"select * from voc_visit_status",
		"select * from voc_visit_type"
	);
	
	/** A Map with the Codes and the corresponding IDs */
	private final Map<String, Integer> codeMap = new HashMap<String, Integer>();
	
	/** Loads Controlled Set of terms from the corresponding DB tables */
	public Vocab(Connection conn) {
		try {
			// For each Controlled Set of Terms (i.e., Table) ...
			for (String codesSqlQuery : codesSQLqueryList) {
				// Execute the SQL query and add Codes in the List
		        final Statement stmt = conn.createStatement();
		        final ResultSet rs = stmt.executeQuery(codesSqlQuery);
		        while(rs.next()) {
		        	final int id = rs.getInt("ID");
		            final String code = rs.getString("CODE");
		            codeMap.put(code, id);
		        }
		        rs.close();
		        stmt.close();	
			}
		} catch (Throwable t) {
			throw new RuntimeException("Cannot Load Vocabularies from DB", t);
		}
	}
	
	public Map<String, Integer> getCodeMap() {
		return codeMap;
	}
	
	public Integer getCodeID(final String code) {
		if (isStrNullOrEmpty(code)) return null;
		final Integer id = codeMap.get(code);
		if (id == null) throw new RuntimeException("Code: " + code + " --> ID: null !");
		return id;
	}

	private boolean isStrNullOrEmpty(final String str) {
		if (str == null) return true;
		return (str.trim().equals("") || str.trim().equals("null"));
	}
	
	@Override
	public String toString() {
		return "Vocab [ codeMap.size()=" + codeMap.size() + " ]";
	}
	
	/* ******************************* TESTING ******************************* */
	
	private static final String dbUrl = "jdbc:mysql://ponte.grid.ece.ntua.gr:3306/HarmonicSS-F";
	private static final String dbUsername = "emps";
	private static final String dbPassword = "emps";	
	
	/** For Testing purposes */
	public static void main(String[] args) throws Exception {
	
		System.out.println(" >> Vocab: START - " + new Date());
		System.out.println();
		
		System.out.println("BEFORE GC " + new MemoryMessage().toParsableString());
		System.gc();
		System.out.println("AFTER GC " + new MemoryMessage().toParsableString());
		System.out.println();
		
		// Get DB Connection
        final Connection conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
        
		// Load Codes
		final Vocab voc = new Vocab(conn);
		
		System.out.println("\n" + voc + "\n");
		
		// Print Codes
		final List<Entry<String, Integer>> entryList = new ArrayList<Map.Entry<String,Integer>>(voc.getCodeMap().entrySet());
		Collections.sort(entryList, new Comparator<Entry<String, Integer>>() {
			@Override public int compare(Entry<String, Integer> e1, Entry<String, Integer> e2) {
				return e1.getKey().compareTo(e2.getKey());
			}
		});
		System.out.println(Util.listToMultiLineString(entryList));
		
		// Get ID for Code
		final String testCode = "COND-091400";
		final Integer testCodeID = voc.getCodeID(testCode);
		System.out.println("\n * " + testCode + " -->" + testCodeID);
		
		System.out.println();
		System.out.println("BEFORE GC " + new MemoryMessage().toParsableString());
		System.gc();
		System.out.println("AFTER GC " + new MemoryMessage().toParsableString());
		
		System.out.println(); 
		System.out.println(" >> Vocab: END - " + new Date());
	}
	
}
