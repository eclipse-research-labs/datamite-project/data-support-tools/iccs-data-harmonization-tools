package ntua.iccs.harmonicss.cohort.data.owl.complex;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.NS;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.BooleanUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.booleanValueUri;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.timchros.core.tuple.Tuple2;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.owl.OntoDataRepo;

public class BooleanValue implements PropertyValue {

	private final boolean booleanValue;
	
	public BooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}
	
	public BooleanValue(final Tuple2<TermExpr, Relation> termExprT) {
		if (termExprT == null) 
			throw new NullPointerException("Object \"termExprT\" is NULL !");
		if (termExprT._1() instanceof Term) {
			final Term term = (Term) termExprT._1();
			if (!term.getLocalName().startsWith("CONFIRM-"))
				throw new RuntimeException("The given Term \"" + term.asString() + "\" is NOT a Confirmation Term !");
			this.booleanValue = term.getLocalName().equals("CONFIRM-01");
		} else {
			throw new RuntimeException("The given object " + termExprT._1().asString() + " is NOT a simple term !");
		}
	}
	
	public BooleanValue(Term term) {
		if (term == null)
			throw new RuntimeException("Given term is NULL !");
		if (!term.getLocalName().startsWith("CONFIRM-"))
			throw new RuntimeException("The given Term \"" + term.asString() + "\" is NOT a Confirmation Term !");
		this.booleanValue = term.getLocalName().equals("CONFIRM-01");
	}
	
	public boolean getBooleanValue() {
		return booleanValue;
	}

	@Override
	public String asString() {
		return "" + booleanValue;
	}
	
	@Override
	public String asAnonymString() {
		return "BOOLEAN";
	}
	
	@Override
	public String toString() {
		return "BooleanValue [booleanValue=" + booleanValue + "]";
	}

	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		if (propRangeOntResource != null && "boolean".equals(propRangeOntResource.getLocalName())) {
			return ResourceFactory.createTypedLiteral("" + this.booleanValue, XSDDatatype.XSDboolean);
		} else {
			final OntClass booleanOntClass = ontoModel.getOntClass(BooleanUri);
			final Individual booleanIndiv = booleanOntClass.createIndividual(NS + booleanOntClass.getLocalName() + "-" + OntoDataRepo.getInstance().nextIndexAsStr(booleanOntClass));
			final OntProperty booleanValue = ontoModel.getOntProperty(booleanValueUri);
			booleanIndiv.setPropertyValue(booleanValue, ResourceFactory.createTypedLiteral("" + this.getBooleanValue(), XSDDatatype.XSDboolean));
			return booleanIndiv;
		}
	}

	@Override
	public Object toJsonObj() {
		return booleanValue;
	}
	
}
