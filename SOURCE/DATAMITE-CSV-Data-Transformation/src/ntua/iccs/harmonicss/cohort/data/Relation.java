package ntua.iccs.harmonicss.cohort.data;

public enum Relation {
	
	Equivalent("ET"), Broader_Meaning("BT"), Narrower_Meaning("NT"), Partially_Overlapping("OT"), Linked_With("OT");
	
	private final String abbr;
	
	Relation(final String abbr) {
		this.abbr = abbr;
	}
	
	public String getAbbr() {
		return this.abbr;
	}
	
    @Override
    public String toString() {
        return this.name();
    }
    
}
