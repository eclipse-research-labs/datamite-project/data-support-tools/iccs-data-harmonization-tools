package ntua.iccs.harmonicss.cohort.data.json;

import ntua.iccs.harmonicss.cohort.data.Relation;

public class TermsMappingRule {

	final TermExpr srcTermExpr;
	final TermExpr trgTermExpr;
	final Relation relation;
	
	public TermsMappingRule(TermExpr srcTermExpr, TermExpr trgTermExpr, Relation relation) {
		this.srcTermExpr = srcTermExpr;
		this.trgTermExpr = trgTermExpr;
		this.relation = relation;
	}

	public TermExpr getSrcTermExpr() {
		return srcTermExpr;
	}

	public TermExpr getTrgTermExpr() {
		return trgTermExpr;
	}

	public Relation getRelation() {
		return relation;
	}

	@Override
	public String toString() {
		return 
			"TermsMappingRule ["
				+ "srcTermExpr=" + srcTermExpr.asString() + ", "
				+ "trgTermExpr=" + trgTermExpr.asString() + ", "
				+ "relation=" + relation + 
			"]";
	}
	
}
