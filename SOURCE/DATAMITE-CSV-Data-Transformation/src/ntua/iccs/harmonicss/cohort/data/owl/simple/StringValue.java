package ntua.iccs.harmonicss.cohort.data.owl.simple;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;

/** Representation of a String Value. <p> Data will be transformed to an RDF Literal of type <code>xsd:string</code>. */
public class StringValue implements PropertyValue {

	private final String str;

	public StringValue(String str) {
		this.str = str;
	}

	public String getStr() {
		return str;
	}
	
	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		if (this.str == null) throw new RuntimeException("StringValue - str is NULL !");
		return ResourceFactory.createTypedLiteral(this.getStr(), XSDDatatype.XSDstring);
	}

	@Override
	public Object toJsonObj() {
		return str;
	}
	
	@Override
	public String asString() {
		return str;
	}

	@Override
	public String asAnonymString() {
		return "STRING";
	}
	
	@Override
	public String toString() {
		return "StringValue [str=" + str + "]";
	}
}
