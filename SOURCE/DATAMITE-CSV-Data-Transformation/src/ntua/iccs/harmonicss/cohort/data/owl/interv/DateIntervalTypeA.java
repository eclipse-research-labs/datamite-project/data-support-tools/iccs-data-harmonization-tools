package ntua.iccs.harmonicss.cohort.data.owl.interv;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.DateIntervalAUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.NS;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.dateIntervalAEndDateUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.dateIntervalAStartDateUri;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.owl.OntoDataRepo;

/** The Start and End Date of an Interval */
public class DateIntervalTypeA implements PropertyValue {

	private final DateValue startDate;
	private final DateValue endDate;
	
	public DateIntervalTypeA(DateValue startDate, DateValue endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public DateIntervalTypeA(Tuple3<Integer, Integer, Integer> startDateTuple, Tuple3<Integer, Integer, Integer> endDateTuple) {
		this.startDate = (startDateTuple != null) ? new DateValue(startDateTuple._1(), startDateTuple._2(), startDateTuple._3()) : null;
		this.endDate = (endDateTuple != null) ? new DateValue(endDateTuple._1(), endDateTuple._2(), endDateTuple._3()) : null;
	}
	
	public DateValue getStartDate() {
		return startDate;
	}

	public DateValue getEndDate() {
		return endDate;
	}
	
	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		final OntClass dateIntervalAOntClass = ontoModel.getOntClass(DateIntervalAUri);
		final Individual dateInterval = dateIntervalAOntClass.createIndividual(NS + dateIntervalAOntClass.getLocalName() + "-" +  OntoDataRepo.getInstance().nextIndexAsStr(dateIntervalAOntClass));
		if (this.getStartDate() != null) {
			final OntProperty startDateIntervA = ontoModel.getOntProperty(dateIntervalAStartDateUri);
			dateInterval.setPropertyValue(startDateIntervA, ResourceFactory.createTypedLiteral(this.getStartDate().asFormatedString(), XSDDatatype.XSDstring));
		}
		if (this.getEndDate() != null) {
			final OntProperty endDateIntervA = ontoModel.getOntProperty(dateIntervalAEndDateUri);
			dateInterval.setPropertyValue(endDateIntervA, ResourceFactory.createTypedLiteral(this.getEndDate().asFormatedString(), XSDDatatype.XSDstring));
		}
		return dateInterval;	
	}

	@Override
	public Object toJsonObj() {
		final JSONObject json = new JSONObject();
		if (this.getStartDate() != null) json.put("date-Interval-Start-Date", this.getStartDate().toJsonObj());
		if (this.getEndDate() != null) json.put("date-Interval-End-Date", this.getEndDate().toJsonObj());
		return json;
	}
	
	@Override
	public String asString() {
		return 
			"[" + 
			((startDate != null) ? startDate.asString() : " ") + 
			"-" +  
			((endDate != null) ? endDate.asString() : " ") + 
			"]";
	}
	
	@Override
	public String asAnonymString() {
		return 
			"[" + 
			((startDate != null) ? "START-DATE" : " ") + 
			"-" +  
			((endDate != null) ? "END-DATE" : " ") + 
			"]";
	}

	@Override
	public String toString() {
		return "DateIntervalA [startDate=" + startDate + ", endDate=" + endDate + "]";
	}
	
}
