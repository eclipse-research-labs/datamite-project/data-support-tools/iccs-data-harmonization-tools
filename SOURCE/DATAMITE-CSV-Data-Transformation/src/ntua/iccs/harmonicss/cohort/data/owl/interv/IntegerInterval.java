package ntua.iccs.harmonicss.cohort.data.owl.interv;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.IntIntervalUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.NS;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.intIntervDNLUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.intIntervUNLUri;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.timchros.core.tuple.Tuple2;

import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.owl.OntoDataRepo;

/** The up (inclusive) and down (inclusive) limits of an interval */
public class IntegerInterval implements PropertyValue {

	private final Integer downValue;
	private final Integer upValue;
	
	public IntegerInterval(Tuple2<Integer, Integer> tuple) {
		this(tuple._1(), tuple._2());
	}
	
	public IntegerInterval(Integer downValue, Integer upValue) {
		this.downValue = downValue;
		this.upValue = upValue;
	}

	public Integer getDownValue() {
		return downValue;
	}

	public Integer getUpValue() {
		return upValue;
	}

	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		final OntClass intIntervalOntClass = ontoModel.getOntClass(IntIntervalUri);
		final Individual intInterval = intIntervalOntClass.createIndividual(NS + intIntervalOntClass.getLocalName() + "-" + OntoDataRepo.getInstance().nextIndexAsStr(intIntervalOntClass));
		if (this.getDownValue() != null) {
			final OntProperty intIntervDNL = ontoModel.getOntProperty(intIntervDNLUri);
			intInterval.setPropertyValue(intIntervDNL, ResourceFactory.createTypedLiteral("" + this.getDownValue(), XSDDatatype.XSDint));
		}
		if (this.getUpValue() != null) {
			final OntProperty intIntervUNL = ontoModel.getOntProperty(intIntervUNLUri);
			intInterval.setPropertyValue(intIntervUNL, ResourceFactory.createTypedLiteral("" + this.getUpValue(), XSDDatatype.XSDint));
		}
		return intInterval;	
	}
	
	@Override
	public Object toJsonObj() {
		final JSONObject json = new JSONObject();
		if (this.getDownValue() != null) json.put("interval-Integer-Down-Limit", this.getDownValue());
		if (this.getUpValue() != null) json.put("interval-Integer-Up-Limit", this.getUpValue());
		return json;
	}

	@Override
	public String asString() {
		if ((downValue != null) && (upValue != null) && (downValue == upValue)) return "[ " + downValue + " ]";
		return
			"[" + 
			((downValue != null) ? downValue : " ") + 
			"-" +  
			((upValue != null) ? upValue : " ") + 
			"]";
	}
	
	@Override
	public String asAnonymString() {
		if ((downValue != null) && (upValue != null) && (downValue == upValue)) return "[ VALUE ]";
		return
			"[" + 
			((downValue != null) ? "DOWN-LIMIT" : " ") + 
			"-" +  
			((upValue != null) ? "UP-LIMIT" : " ") + 
			"]";
	}

	@Override
	public String toString() {
		return "IntegerInterval [downValue=" + downValue + ", upValue=" + upValue + "]";
	}
	
}
