package ntua.iccs.harmonicss.cohort.data.owl;

import java.util.List;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.timchros.core.transformers.I_StringTransformer;
import org.timchros.core.util.Util;

public class PropertyValueList implements PropertyValue {

	private final List<PropertyValue> propValueList;
	
	public PropertyValueList(List<PropertyValue> propValueList) {
		this.propValueList = propValueList;
	}

	public List<PropertyValue> getPropValueList() {
		return propValueList;
	}

	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		throw new RuntimeException("Method toRDFNode() deliberately not implemented !");
	}

	@Override
	public Object toJsonObj() {
		throw new RuntimeException("Method toJsonObj() deliberately not implemented !");
	}
	
	@Override
	public String asString() {
		return Util.listToOneLineString(this.propValueList, new I_StringTransformer<PropertyValue>() {
			@Override public String asString(PropertyValue pv) { return pv.asString(); }
		});
	}

	@Override
	public String asAnonymString() {
		return Util.listToOneLineString(this.propValueList, new I_StringTransformer<PropertyValue>() {
			@Override public String asString(PropertyValue pv) { return pv.asAnonymString(); }
		});
	}
	
	@Override
	public String toString() {
		return "PropertyValueList [propValueList=" + propValueList + "]";
	}



}
