package ntua.iccs.harmonicss.cohort.data.owl.simple;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;

/** Representation of an Integer Value. <p> Data will be transformed to an RDF Literal of type <code>xsd:integer</code>. */
public class IntegerValue implements PropertyValue {

	private final int num;

	public IntegerValue(int num) {
		this.num = num;
	}

	public int getNum() {
		return num;
	}

	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		return ResourceFactory.createTypedLiteral("" + this.getNum(), XSDDatatype.XSDint);
	}

	@Override
	public Object toJsonObj() {
		return num;
	}

	
	@Override
	public String asString() {
		return "" + num;
	}

	@Override
	public String asAnonymString() {
		if (num == 0) return "ZERO";
		if (num > 0) return "POSITIVE-INTEGER";
		if (num < 0) return "NEGATIVE-INTEGER";
		return "INTEGER";
	}
	
	@Override
	public String toString() {
		return "IntegerValue [num=" + num + "]";
	}
}
