package ntua.iccs.harmonicss.cohort.data.json;

import org.timchros.core.interfaces.I_Check;

public abstract class TermExpr {

	/** @return A human readable String with the given Term or Expression */
	public abstract String asString();
	
	/** @return <code>true</code> if this is a (simple) Term, otherwise <code>false</code>. */
	public boolean isTerm() {
		return (this instanceof Term);
	}
	
	public Term asTerm() {
		return (Term) this;
	}
	
	/** @return <code>true</code> if this is a (complex) Term, otherwise <code>false</code>. */
	public boolean isComplexTerm() {
		return (this instanceof TermExpression);
	}
	
	public TermExpression asTermExpression() {
		return (TermExpression) this;
	}
	
	/** @return <code>true</code> if this is a (simple) Confirmation Term, otherwise <code>false</code>. */
	public boolean isSimpleConfirmationTerm() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isConfirmationTerm();
		}
		return false;
	}
	
	public boolean isConfirmationTermNO() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isConfirmationTermNO();
		}
		return false;
	}
	public boolean isConfirmationTermYES() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isConfirmationTermYES();
		}
		return false;
	}
	
	/** @return <code>true</code> if this is a (simple) Assessment Term, otherwise <code>false</code>. */
	public boolean isSimpleAssessmentTerm() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isAssessmentTerm();
		}
		return false;
	}
	
	public boolean isAssessmentTermNORMAL() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isAssessmentTermNORMAL();
		}
		return false;
	}
	
	public boolean isAssessmentTermABNORMAL() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isAssessmentTermABNORMAL();
		}
		return false;
	}
	
	/** @return <code>true</code> if this is a (simple) Activity Level Term, otherwise <code>false</code>. */
	public boolean isSimpleActivityLevelTerm() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isActivityLevelTerm();
		}
		return false;
	}
	
	/** @return <code>true</code> if this is a (simple) Smoking Status Term, otherwise <code>false</code>. */
	public boolean isSimpleSmokingStatusTerm() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isSmokingStatusTerm();
		}
		return false;
	}
	
	/** @return <code>true</code> if this is a (simple) Sex Term, otherwise <code>false</code>. */
	public boolean isSimpleSexTerm() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isSexTerm();
		}
		return false;
	}
	
	public boolean isSimpleSexTermMALE() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isSexTermMALE();
		}
		return false;
	}
	
	public boolean isSimpleSexTermFEMALE() {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return term.isSexTermFEMALE();
		}
		return false;
	}
	
	
	/* *************** Simple Term or Expression *************** */
	
	private static final I_Check<Term> bloodTestCheck = new I_Check<Term>() {
		@Override public boolean isTrue(Term term) { return term.isBloodTest(); }};


	public boolean isBloodTest() {
		return check(bloodTestCheck);
	}
	
	private static final I_Check<Term> bloodTestOutcomeTermCheck = new I_Check<Term>() {
		@Override public boolean isTrue(Term term) { return term.isAnaPatternTerm() || term.isCryoTypeTerm(); }};
	
	public boolean isBloodTestOutcomeTerm() {
		return check(bloodTestOutcomeTermCheck);
	}
	
	private static final I_Check<Term> symptomCheck = new I_Check<Term>() {
		@Override public boolean isTrue(Term term) { return term.isSymptomSign(); }};

	public boolean isSymptomSign() {
		return check(symptomCheck);
	}
	
	private static final I_Check<Term> medCondCheck = new I_Check<Term>() {
		@Override public boolean isTrue(Term term) { return term.isMedicalCondition(); }};		
		
	public boolean isMedicalCondition() {
		return check(medCondCheck);
	}
	
	private static final I_Check<Term> drugCheck = new I_Check<Term>() {
		@Override public boolean isTrue(Term term) { return term.isDrug(); }};		
	
	public boolean isDrug() {
		return check(drugCheck);
	}
		
	private static final I_Check<Term> organCheck = new I_Check<Term>() {
		@Override public boolean isTrue(Term term) { return term.isOrgan(); }};		
		
	public boolean isOrgan() {
		return check(organCheck);
	}
	
	private static final I_Check<Term> salivGlandCheck = new I_Check<Term>() {
		@Override public boolean isTrue(Term term) { return term.isSalivaryGlandBiopsy(); }};		
		
	public boolean isSalivaryGlandBiopsy() {
		return check(salivGlandCheck);
	}
	
	/** @return <code>true</code> if this object is a (simple) Term or Expression of the given Type (i.e., terms satisfying the given condition), otherwise <code>false</code>. */
	private boolean check(I_Check<Term> termCheck) {
		if (this instanceof Term) {
			final Term term = (Term) this;
			return termCheck.isTrue(term);
		} else {
			final TermExpression termExpression = (TermExpression) this;
			for (TermExpr termExpr : termExpression.getTermExprList()) {
				if (!termExpr.check(termCheck)) return false;
			}
			return true;
		}
	}

}
