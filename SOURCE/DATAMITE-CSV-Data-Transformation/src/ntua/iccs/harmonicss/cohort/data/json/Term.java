package ntua.iccs.harmonicss.cohort.data.json;

/** A term from either Cohort or Reference Model OWL ontologies */
public class Term extends TermExpr {

	final String uri;
	final String label;
	// Use for Cohort Fields
	final String rangeuri;

	public Term(String uri, String label) {
		super();
		this.uri = uri;
		this.label = label;
		this.rangeuri = null;
	}
	
	public Term(String uri, String label, String rangeuri) {
		super();
		this.uri = uri;
		this.label = label;
		this.rangeuri = rangeuri;
	}

	public String getUri() {
		return uri;
	}

	public String getLabel() {
		return label;
	}
	
	public String getRangeUri() {
		return rangeuri;
	}

	public String getLocalName() {
		final int index = uri.lastIndexOf('#');
		return (index > 0) ? uri.substring(index + 1) : uri;
	}

	/* About Confirmation */
	
	public boolean isConfirmationTerm() {
		return this.getLocalName().startsWith("CONFIRM-");
	}
	
	public boolean isConfirmationTermYES() {
		return this.getLocalName().equals("CONFIRM-01");
	}
	
	public boolean isConfirmationTermNO() {
		return this.getLocalName().equals("CONFIRM-02");
	}
	
	/* About Assessment */
	
	public boolean isAssessmentTerm() {
		return this.getLocalName().startsWith("ASSESS-");
	}
	
	public boolean isAssessmentTermNORMAL() {
		return this.getLocalName().equals("ASSESS-10");
	}
	
	public boolean isAssessmentTermABNORMAL() {
		return this.getLocalName().startsWith("ASSESS-2");
	}
	
	public boolean isActivityLevelTerm() {
		return this.getLocalName().startsWith("ACTLVL-");
	}
	
	public boolean isSmokingStatusTerm() {
		return this.getLocalName().startsWith("SMOK-STAT-");
	}
	
	public boolean isSexTerm() {
		return this.getLocalName().startsWith("SEX-");
	}
	
	public boolean isSexTermMALE() {
		return this.getLocalName().equals("SEX-01");
	}
	
	public boolean isSexTermFEMALE() {
		return this.getLocalName().equals("SEX-02");
	}
	
	/* About Other Classes */
	
	public boolean isBloodTest() {
		return this.getLocalName().startsWith("BLOOD-");
	}
	
	public boolean isAnaPatternTerm() {
		return this.getLocalName().startsWith("ANA-PAT-");
	}
	
	public boolean isCryoTypeTerm() {
		return this.getLocalName().startsWith("CRYO-");
	}
	
	public boolean isSymptomSign() {
		return this.getLocalName().startsWith("SYMPT-");
	}
	
	public boolean isMedicalCondition() {
		return this.getLocalName().startsWith("COND-");
	}
	
	public boolean isDrug() {
		return this.getLocalName().startsWith("CHEM-");
	}
	
	public boolean isOrgan() {
		return this.getLocalName().startsWith("ORGAN-");
	}
	
	public boolean isSalivaryGlandBiopsy() {
		return this.getLocalName().startsWith("SAL-BIO-");
	}
	
	@Override
	public String asString() {
		return this.getLocalName() + " (" + label + ")";
	}
	
	@Override
	public String toString() {
		return "Term [uri=" + uri + ", label=" + label + ", rangeuri=" + rangeuri + "]";
	}

	public static String getCategoryByCode(final String code) {
		if (code == null || code.trim().equals("")) return null;
		if (code.startsWith("CONFIRM-")) return "CONFIRMATION";
		if (code.startsWith("ASSESS-")) return "ASSESSMENT";
		if (code.startsWith("UNITEXP-")) return "UNIT";
		
		if (code.startsWith("SEX-")) return "SEX";
		if (code.startsWith("ETHN-")) return "ETHNICITY";
		if (code.startsWith("EDU-LEV-")) return "EDUCATION-LEVEL";
		if (code.startsWith("BMI-")) return "BMI";
		if (code.startsWith("SMOK-STAT-")) return "SMOKING-STATUS";
		if (code.startsWith("PREG-OUT-")) return "PREGNANCY-OUTCOME";
		
		if (code.startsWith("COND-")) return "MEDICAL-CONDITION";
		if (code.startsWith("SYMPT-")) return "SYMPTOM";
		if (code.startsWith("ORGAN-")) return "ORGAN";
		if (code.startsWith("PERFORM-")) return "ECOG-STATUS";
		if (code.startsWith("STAGE-")) return "STAGE";
		if (code.startsWith("REL-DEG-")) return "RELATIVE-DEGREE";
		if (code.startsWith("SPEC-")) return "SPEC-DEPARTMENT";
		
		if (code.startsWith("CHEM-")) return "DRUG";
		if (code.startsWith("BLOOD-")) return "BLOOD-TEST";
		if (code.startsWith("URINE-")) return "URINE-TEST";
		if (code.startsWith("ORAL-")) return "ORAL-TEST";
		if (code.startsWith("OCULAR-")) return "OCULAR-TEST";
		if (code.startsWith("SAL-BIO-")) return "BIOPSY-SAL-GLAND";
		if (code.startsWith("BIOPSY-")) return "BIOPSY-TEST";
		if (code.startsWith("IMG-")) return "IMGING-TEST";
		if (code.startsWith("ANA-PAT-")) return "ANA-PATTERN";
		if (code.startsWith("CRYO-")) return "CRYO-TYPE";
		
		if (code.startsWith("QUEST-")) return "QUEST-SCORE";
		if (code.startsWith("IPI-RISK-")) return "IPI-RISK";
		if (code.startsWith("CACI-")) return "CACI-CONDITION";
		if (code.startsWith("ESSDAI-")) return "ESSDAI-DOMAIN";
		if (code.startsWith("ACTLVL-")) return "ACTIVITY-LEVEL";

		return "OTHER";
	}
	
}
