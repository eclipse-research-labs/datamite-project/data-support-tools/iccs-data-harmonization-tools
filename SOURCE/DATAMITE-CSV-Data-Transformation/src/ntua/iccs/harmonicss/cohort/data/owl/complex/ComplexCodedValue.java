package ntua.iccs.harmonicss.cohort.data.owl.complex;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.NS;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.CodeExprUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.codeExprCodeUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.codeExprHierarRelationUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.codeExprBooleanOperUri;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.timchros.core.tuple.Tuple2;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.data.BooleanOperator;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.json.TermExpression;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.owl.OntoDataRepo;

public class ComplexCodedValue implements PropertyValue {

	private final Relation hierarRelation;
	private final BooleanOperator booleanOperator;
	private final List<SimpleCodedValue> simpleCodesList;
	
	public ComplexCodedValue(Tuple2<TermExpr, Relation> termExprT) {
		if (termExprT == null) 
			throw new NullPointerException("Object \"termExprT\" is NULL !");
		if (termExprT._1() instanceof TermExpression) {
			final TermExpression termExpression = (TermExpression) termExprT._1();
			this.hierarRelation = termExprT._2();
			this.booleanOperator = termExpression.getBooleanOp();
			this.simpleCodesList = new ArrayList<SimpleCodedValue>();
			for (TermExpr termExpr : termExpression.getTermExprList()) {
				if (termExpr instanceof Term) {
					final Term term = (Term) termExpr;
					this.simpleCodesList.add(new SimpleCodedValue(term.getLocalName(), term.getLabel()));
				} else {
					throw new RuntimeException("Internal Element of an Expression " + termExprT._1().asString() + " is NOT a Term !");
				}
			}
		} else {
			throw new RuntimeException("The given object " + termExprT._1().asString() + " is NOT a Term Expression !");
		}
		
	}
	
	public ComplexCodedValue(Relation hierarRelation, BooleanOperator booleanOperator, List<SimpleCodedValue> simpleCodesList) {
		this.hierarRelation = hierarRelation;
		this.booleanOperator = booleanOperator;
		this.simpleCodesList = simpleCodesList;
	}

	public Relation getHierarRelation() {
		return hierarRelation;
	}

	public BooleanOperator getBooleanOperator() {
		return booleanOperator;
	}

	public List<SimpleCodedValue> getSimpleCodesList() {
		return simpleCodesList;
	}


	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		// Get Coded Value individual in the Map (if already exists)
//		if (OntoDataRepo.getInstance().getCodedValueIndivMap().containsKey(this)) 
//			return OntoDataRepo.getInstance().getCodedValueIndivMap().get(this);
		// Create Coded Value individual
		final OntClass codeExprOntClass = ontoModel.getOntClass(CodeExprUri);
		final Individual codeExpr = codeExprOntClass.createIndividual(NS + codeExprOntClass.getLocalName() + "-" + OntoDataRepo.getInstance().nextIndexAsStr(codeExprOntClass) );
		final OntProperty codeExprHierarRelation = ontoModel.getOntProperty(codeExprHierarRelationUri);
		codeExpr.setPropertyValue(codeExprHierarRelation, ResourceFactory.createStringLiteral(this.hierarRelation.getAbbr()));
		final OntProperty codeExprBooleanOperator = ontoModel.getOntProperty(codeExprBooleanOperUri);
		codeExpr.setPropertyValue(codeExprBooleanOperator, ResourceFactory.createStringLiteral(this.booleanOperator.name()));
		final OntProperty codeExprCode = ontoModel.getOntProperty(codeExprCodeUri);
		for (SimpleCodedValue simpleCodedValue : simpleCodesList) {
			codeExpr.addProperty(codeExprCode, simpleCodedValue.toRDFNode(ontoModel, propRangeOntResource));
		}
		// Save Coded Value individual in the Map
//		OntoDataRepo.getInstance().getCodedValueIndivMap().put(this, code);
		return codeExpr;	
	}
	
	@Override
	public Object toJsonObj() {
		final JSONObject json = new JSONObject();
		json.put("hierarchical-Relationship", this.hierarRelation.getAbbr());
		json.put("boolean-Operator", this.booleanOperator.name());
		final JSONArray ja = new JSONArray();
		for (SimpleCodedValue simpleCodedValue : simpleCodesList) {
			ja.add(simpleCodedValue.toJsonObj());
		}
		json.put("expression-Code-CV-JA", ja);
		return json;
	}

	@Override
	public String asString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		sb.append(hierarRelation);
		sb.append(" ");
		int i = 0;
		for (SimpleCodedValue scv : simpleCodesList) {
			if (i > 0) sb.append(" " + this.booleanOperator.name() + " ");
			sb.append(scv.asString());
			i++;
		}
		sb.append(" }");
		return sb.toString();
	}

	@Override
	public String asAnonymString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		sb.append(hierarRelation);
		sb.append(" ");
		sb.append(booleanOperator);
		sb.append(" ");
		sb.append("TERM-EXPR-LIST");
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return "ComplexCodedValue [hierarRelation=" + hierarRelation + ", booleanOperator=" + booleanOperator + ", simpleCodesList=" + simpleCodesList + "]";
	}


}
