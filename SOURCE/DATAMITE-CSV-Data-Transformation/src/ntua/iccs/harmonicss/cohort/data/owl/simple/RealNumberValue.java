package ntua.iccs.harmonicss.cohort.data.owl.simple;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;

public class RealNumberValue implements PropertyValue {

	private final float value;
	
	public RealNumberValue(float value){
		this.value = value;
	}
	
	public float getValue() {
		return value;
	}

	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		return ResourceFactory.createTypedLiteral("" + this.getValue(), XSDDatatype.XSDfloat);
	}

	@Override
	public Object toJsonObj() {
		return value;
	}

	@Override
	public String asString() {
		return "" + value;
	}

	@Override
	public String asAnonymString() {
		if (value == 0) return "ZERO";
		if (value > 0) return "POSITIVE-REAL-NUMBER";
		if (value < 0) return "NEGATIVE-REAL-NUMBER";
		return "REAL-NUMBER";
	}

	@Override
	public String toString() {
		return "RealNumberValue [value=" + value + "]";
	}

}
