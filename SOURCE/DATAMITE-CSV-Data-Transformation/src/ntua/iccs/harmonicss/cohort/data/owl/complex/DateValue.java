package ntua.iccs.harmonicss.cohort.data.owl.complex;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.NS;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.DateUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.dateDayUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.dateMonthUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.dateYearUri;

import java.util.Calendar;
import java.util.Date;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.timchros.core.tuple.Tuple3;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.owl.OntoDataRepo;

/** The Year, Month (optional) and Day (optional) an event took place. */
public class DateValue implements PropertyValue {
	
	/* Mandatory */
	private final int year;
	/* Optional */
	private final int month;
	/* Optional */
	private final int day;
	
	public DateValue(int year) {
		this.year = year;
		this.month = -1;
		this.day = -1;
	}
	
	public DateValue(int year, int month) {
		this.year = year;
		this.month = (month >= 1 && month <=12) ? month : -1;
		this.day = -1;
	}
	
	public DateValue(int year, int month, int day) {
		this.year = year;
		this.month = (month >= 1 && month <=12) ? month : -1;
		this.day = (this.month > 0 && day >= 1 && day <=31) ? day : -1;
	}
	
	public DateValue(final Tuple3<Integer, Integer, Integer> dateTuple) {
		this(dateTuple._1(), dateTuple._2(), dateTuple._3());
	}
	
	public DateValue(Date date) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		this.year = calendar.get(Calendar.YEAR);
		this.month = calendar.get(Calendar.MONTH) + 1;
		this.day = calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	// A String of type xsd:date
	public String asFormatedString() {
		StringBuilder sb = new StringBuilder();
		sb.append(year);
		sb.append("/");
		if (month >= 1 && month <= 12) {
			sb.append(numberTo2DigitString(month));
			sb.append("/");
			if (day >= 1 && day <= 31) {
				sb.append(numberTo2DigitString(day));
			} else {
				sb.append("--");
			}
		} else {
			sb.append("--/--");
		}
		return sb.toString();
	}
	
	private String numberTo2DigitString(int n) {
		if (n < 10) return "0" + n;
		return "" + n;
	}

	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		if (propRangeOntResource != null && "string".equals(propRangeOntResource.getLocalName())) {
			return ResourceFactory.createTypedLiteral(this.asFormatedString(), XSDDatatype.XSDstring);
		} else {
			final OntClass dateOntClass = ontoModel.getOntClass(DateUri);
			final Individual date = dateOntClass.createIndividual(NS + dateOntClass.getLocalName() + "-" + OntoDataRepo.getInstance().nextIndexAsStr(dateOntClass));
			final OntProperty dateYear = ontoModel.getOntProperty(dateYearUri);
			date.setPropertyValue(dateYear, ResourceFactory.createTypedLiteral("" + this.getYear(), XSDDatatype.XSDunsignedShort));
			if (this.getMonth() >= 1 && this.getMonth() <= 12) {
				final OntProperty dateMonth = ontoModel.getOntProperty(dateMonthUri);
				date.setPropertyValue(dateMonth, ResourceFactory.createTypedLiteral("" + this.getMonth(), XSDDatatype.XSDunsignedShort));
				if (this.getDay() >= 1 && this.getDay() <= 31) {
					final OntProperty dateDay = ontoModel.getOntProperty(dateDayUri);
					date.setPropertyValue(dateDay, ResourceFactory.createTypedLiteral("" + this.getDay(), XSDDatatype.XSDunsignedShort));
				}
			}
			return date;
		}
	}
	
	@Override
	public Object toJsonObj() {
		return this.asFormatedString();
	}
	
	@Override
	public String asString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(year);
		if (month > 0) sb.append("/" + month);
		if (day > 0) sb.append("/" + day);
		return sb.toString();
	}
	
	@Override
	public String asAnonymString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("YYYY");
		if (month > 0) sb.append("/" + "MM");
		if (day > 0) sb.append("/" + "DD");
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return "DateValue [year=" + year + ", month=" + month + ", day=" + day + "]";
	}

}
