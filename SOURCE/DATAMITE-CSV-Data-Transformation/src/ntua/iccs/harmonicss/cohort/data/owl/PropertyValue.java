package ntua.iccs.harmonicss.cohort.data.owl;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;

/** Indicates that the data will be transformed to OWL resources */
public interface PropertyValue {

	/** @return An RDF node with the given data */
	public RDFNode toRDFNode(OntModel ontoModel, final OntResource propRangeOntResource);
	
	/** @return a JSON object */
	public Object toJsonObj();
	
	/** @return A human readable String with the Object values, for testing/debugging purposes */
	public String asString();
	
	/** @return A human readable String with the Object Anonym values, for testing/debugging purposes */
	public String asAnonymString();
}
