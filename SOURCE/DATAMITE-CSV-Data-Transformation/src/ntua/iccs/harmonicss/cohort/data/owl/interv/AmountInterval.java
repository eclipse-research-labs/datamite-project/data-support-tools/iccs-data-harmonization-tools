package ntua.iccs.harmonicss.cohort.data.owl.interv;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.AmountIntervalUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.NS;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.amountIntervDNLUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.amountIntervUNLUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.amountIntervUnitCodeUri;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.owl.OntoDataRepo;

/** The up (inclusive) and down (inclusive) limits of an interval along with the unit of measurment */
public class AmountInterval implements PropertyValue {

	private final Float downValue;
	private final Float upValue;
	private final SimpleCodedValue unit;
	
	public AmountInterval(Tuple3<Float, Float, Tuple2<String, String>> tuple) {
		this(tuple._1(), tuple._2(), new SimpleCodedValue(tuple._3()._1(), tuple._3()._2()));
	}
	
	public AmountInterval(Float downValue, Float upValue, SimpleCodedValue unit) {
		this.downValue = downValue;
		this.upValue = upValue;
		this.unit = unit;
	}

	public Float getDownValue() {
		return downValue;
	}
	
	public String getDownValueAsString() {
		// TODO: Update
		return "" + downValue;
	}

	public Float getUpValue() {
		return upValue;
	}
	
	public String getUpValueAsString() {
		// TODO: Update
		return "" + upValue;
	}

	public SimpleCodedValue getUnit() {
		return unit;
	}

	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		final OntClass amountIntervalOntClass = ontoModel.getOntClass(AmountIntervalUri);
		final Individual amountInterval = amountIntervalOntClass.createIndividual(NS + amountIntervalOntClass.getLocalName() + "-" + OntoDataRepo.getInstance().nextIndexAsStr(amountIntervalOntClass));
		if (this.getDownValue() != null) {
			final OntProperty amountIntervDNL = ontoModel.getOntProperty(amountIntervDNLUri);
			amountInterval.setPropertyValue(amountIntervDNL, ResourceFactory.createTypedLiteral(this.getDownValueAsString(), XSDDatatype.XSDfloat));
		}
		if (this.getUpValue() != null) {
			final OntProperty amountIntervUNL = ontoModel.getOntProperty(amountIntervUNLUri);
			amountInterval.setPropertyValue(amountIntervUNL, ResourceFactory.createTypedLiteral(this.getUpValueAsString(), XSDDatatype.XSDfloat));
		}
		final OntProperty amountIntervalUnit = ontoModel.getOntProperty(amountIntervUnitCodeUri);
		amountInterval.setPropertyValue(amountIntervalUnit, this.getUnit().toRDFNode(ontoModel, propRangeOntResource));
		return amountInterval;	
	}
	
	@Override
	public Object toJsonObj() {
		final JSONObject json = new JSONObject();
		if (this.getDownValue() != null) json.put("interval-Amount-Down-Limit", this.getDownValueAsString());
		if (this.getUpValue() != null) json.put("interval-Amount-Up-Limit", this.getUpValueAsString());
		json.put("amount-Unit-CV", this.getUnit().toJsonObj());
		return json;
	}

	@Override
	public String asString() {
		if ((downValue != null) && (upValue != null) && (downValue == upValue)) return "[ " + downValue + " ] " + unit.getName();
		return "[" + 
			((downValue != null) ? getDownValueAsString() : " ") + 
			"-" + 
			((upValue != null) ? getUpValueAsString() : " ") + 
			"] " + 
			unit.getName();
	}
	
	@Override
	public String asAnonymString() {
		if ((downValue != null) && (upValue != null) && (downValue == upValue)) return "[ VALUE ] " + unit.getName();
		return "[" + 
				((downValue != null) ? "DOWN-LIMIT" : " ") + 
				"-" + 
				((upValue != null) ? "UP-LIMIT" : " ") + 
				"] " + 
				unit.getName();
	}

	@Override
	public String toString() {
		return "AmountInterval [downValue=" + downValue + ", upValue=" + upValue + ", unit=" + unit + "]";
	}

}
