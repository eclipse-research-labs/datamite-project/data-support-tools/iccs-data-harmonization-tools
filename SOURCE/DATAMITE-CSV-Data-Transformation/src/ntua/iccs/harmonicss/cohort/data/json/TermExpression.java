package ntua.iccs.harmonicss.cohort.data.json;

import java.util.List;

import ntua.iccs.harmonicss.cohort.data.BooleanOperator;

public class TermExpression extends TermExpr {
	
	private final BooleanOperator booleanOp;
	private final List<TermExpr> termExprList;
	
	public TermExpression(BooleanOperator booleanOp, List<TermExpr> termExprList) {
		this.booleanOp = booleanOp;
		this.termExprList = termExprList;
	}

	public BooleanOperator getBooleanOp() {
		return booleanOp;
	}

	public List<TermExpr> getTermExprList() {
		return termExprList;
	}

	@Override
	public String asString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("{ ");
		int i = 0;
		for (TermExpr termExpr : termExprList) {
			if (i > 0) sb.append(" " + this.booleanOp.name() + " ");
			sb.append(termExpr.asString());
			i++;
		}
		sb.append(" }");
		return sb.toString();
	}
	
	@Override
	public String toString() {
		return "TermExpression [booleanOp=" + booleanOp + ", termExprList=" + termExprList + "]";
	}
	
}
