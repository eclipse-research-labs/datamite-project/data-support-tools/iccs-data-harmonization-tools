package ntua.iccs.harmonicss.cohort.data.owl.complex;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.NS;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.AmountUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.amountUnitUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.amountValueUri;

import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.timchros.core.tuple.Tuple2;

import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.owl.OntoDataRepo;

/** A numeric value followed by a unit of measurement */
public class AmountValue implements PropertyValue {

	private final float value;
	private final SimpleCodedValue unit;
	
	public AmountValue(Tuple2<Float, Tuple2<String, String>> tuple) {
		this(tuple._1(), new SimpleCodedValue(tuple._2()._1(), tuple._2()._2()));
	}
	
	public AmountValue(float value, SimpleCodedValue unit) {
		super();
		this.value = value;
		this.unit = unit;
	}

	public float getValue() {
		return value;
	}
	
	public String getValueAsString() {
		return "" + value;
	}
	
	public SimpleCodedValue getUnit() {
		return unit;
	}


	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		final OntClass amountOntClass = ontoModel.getOntClass(AmountUri);
		final Individual amount = amountOntClass.createIndividual(NS + amountOntClass.getLocalName() + "-" +  OntoDataRepo.getInstance().nextIndexAsStr(amountOntClass));
		final OntProperty amountValue = ontoModel.getOntProperty(amountValueUri);
		amount.setPropertyValue(amountValue, ResourceFactory.createTypedLiteral(this.getValueAsString(), XSDDatatype.XSDfloat));
		final OntProperty amountUnit = ontoModel.getOntProperty(amountUnitUri);
		amount.setPropertyValue(amountUnit, this.getUnit().toRDFNode(ontoModel, propRangeOntResource));
		return amount;	
	}

	@Override
	public Object toJsonObj() {
		final JSONObject json = new JSONObject();
		json.put("amount-Value", this.getValueAsString());
		json.put("amount-Unit-CV", this.getUnit().toJsonObj());
		return json;
	}
	
	@Override
	public String asString() {
		return value + " " + unit.getName();
	}
	
	@Override
	public String asAnonymString() {
		return "VALUE" + " " + unit.getName() + "(" + unit.getCode() + ")";
	}

	@Override
	public String toString() {
		return "AmountValue [value=" + value + ", unit=" + unit + "]";
	}
	
}
