package ntua.iccs.harmonicss.cohort.data.json;

import static ntua.iccs.harmonicss.cohort.CohortData.numberToABC;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

public class FieldsMappingRule {
	
	final Map<Integer, Term> srcPropMap;
	final Term trgEntityClass;
	final Map<Integer, Term> trgPropMap;
	final DataTransformation dtService;
	
	public FieldsMappingRule(Map<Integer, Term> srcPropMap, Term trgEntityClass, Map<Integer, Term> trgPropMap, DataTransformation dtService) {
		super();
		this.srcPropMap = srcPropMap;
		this.trgEntityClass = trgEntityClass;
		this.trgPropMap = trgPropMap;
		this.dtService = dtService;
	}

	public Map<Integer, Term> getSrcPropMap() {
		return srcPropMap;
	}

	public Term getTrgEntityClass() {
		return trgEntityClass;
	}

	public Map<Integer, Term> getTrgPropMap() {
		return trgPropMap;
	}

	public DataTransformation getDtService() {
		return dtService;
	}
	
	public List<String> getSrcFieldsABC() {
		final List<String> idsList = new ArrayList<>();
		for (Entry<Integer, Term> entry : srcPropMap.entrySet()) {
			idsList.add(paramUriToABC(entry.getValue().getUri()));
		}
		Collections.sort(idsList);
		return idsList;
	}
	
	private static final String PARAMETER = "Parameter-";
	
	private static final char DASH = '-';
	
	public static String paramUriToABC(final String uri) {
		if (uri == null) return null;
		int index = uri.lastIndexOf(PARAMETER);
		if (index < 0) throw new RuntimeException("Cannot find string \"" + PARAMETER + "\" in URI: " + uri);
		final String str = uri.substring(index + PARAMETER.length(), uri.length());
		try {
			final int dashIndex = str.indexOf(DASH);
			if (dashIndex > 0) {
				final String sheetNumStr = str.substring(0, dashIndex);
				int sheetNum = Integer.parseInt(sheetNumStr);
				final String columnNumStr = str.substring(dashIndex + 1);
				int columnNum = Integer.parseInt(columnNumStr);
				return sheetNum + "" + DASH + numberToABC(columnNum);
			} else {
				int columnNum = Integer.parseInt(str);
				return numberToABC(columnNum);
			}
		} catch(Throwable t) {
			throw new RuntimeException("What follows the String \"" + PARAMETER + "\" is not valid: \"" + str + "\".", t);
		}
	}

	@Override
	public String toString() {
		return 
			"MappingRule [" + 
				"srcPropMap=" + srcPropMap + ", " + 
				"trgEntityClass=" + trgEntityClass + ", " + 
				"trgPropMap=" + trgPropMap + ", " + 
				"dtService=" + dtService + 
			"]";
	}

}
