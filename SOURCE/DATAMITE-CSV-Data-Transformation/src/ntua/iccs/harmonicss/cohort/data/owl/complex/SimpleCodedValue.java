package ntua.iccs.harmonicss.cohort.data.owl.complex;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.NS;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.CodeUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.AssessmentCodeUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.UnitCodeUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.codeDisplayNameUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.codeExprHierarRelationUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.codeValueUri;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.timchros.core.tuple.Tuple2;

import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.owl.OntoDataRepo;

/** A specific Term with a unique Code and Name. */
public class SimpleCodedValue implements PropertyValue {

	private final Relation relation;
	private final String code;
	private final String name;
	
	public SimpleCodedValue(final Tuple2<TermExpr, Relation> termExprT) {
		if (termExprT == null) 
			throw new NullPointerException("Object \"termExprT\" is NULL !");
		if (termExprT._1() instanceof Term) {
			this.relation = termExprT._2();
			final Term term = (Term) termExprT._1();
			this.code = term.getLocalName();
			this.name = term.getLabel().trim();
		} else {
			throw new RuntimeException("The given object " + termExprT._1().asString() + " is NOT a simple term !");
		}
	}
	
	public SimpleCodedValue(final OntClass ontClass) {
		this.relation = null;
		this.code = ontClass.getLocalName();
		this.name = ontClass.getLabel(null).trim();
	}
	
	public SimpleCodedValue(Relation relation, String code, String name) {
		this.relation = relation;
		this.code = code;
		this.name = name;
	}

	public SimpleCodedValue(Term term) {
		this(term.getLocalName(), term.getLabel().trim());
	}
	
	public SimpleCodedValue(String code, String name) {
		this.relation = null;
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleCodedValue other = (SimpleCodedValue) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public RDFNode toRDFNode(OntModel ontoModel, OntResource propRangeOntResource) {
		// Get Coded Value individual in the Map (if already exists)
		if (OntoDataRepo.getInstance().getCodedValueIndivMap().containsKey(this)) 
			return OntoDataRepo.getInstance().getCodedValueIndivMap().get(this);
		// Create Coded Value individual
		final OntClass codeOntClass;
		if (Term.getCategoryByCode(this.getCode()).equals("ASSESSMENT")) {
			codeOntClass = ontoModel.getOntClass(AssessmentCodeUri);
		} else if (Term.getCategoryByCode(this.getCode()).equals("UNIT")) {
			codeOntClass = ontoModel.getOntClass(UnitCodeUri);
		} else {
			codeOntClass = ontoModel.getOntClass(CodeUri);
		}
		final Individual code = codeOntClass.createIndividual(NS + codeOntClass.getLocalName() + "-" + OntoDataRepo.getInstance().nextIndexAsStr(codeOntClass) );
		final OntProperty codeValue = ontoModel.getOntProperty(codeValueUri);
		code.setPropertyValue(codeValue, ResourceFactory.createStringLiteral(this.getCode()));
		final OntProperty codeDisplayName = ontoModel.getOntProperty(codeDisplayNameUri);
		code.setPropertyValue(codeDisplayName, ResourceFactory.createStringLiteral(this.getName()));
		if (relation != null) {
			final OntProperty codeExprHierarRelation = ontoModel.getOntProperty(codeExprHierarRelationUri);
			code.setPropertyValue(codeExprHierarRelation, ResourceFactory.createStringLiteral(this.relation.getAbbr()));
		}
		
		// Save Coded Value individual in the Map
		OntoDataRepo.getInstance().getCodedValueIndivMap().put(this, code);
		return code;	
	}
	
	@Override
	public Object toJsonObj() {
		final JSONObject json = new JSONObject();
		json.put("code-Value", this.getCode());
		json.put("code-Display-Name", this.getName());
		if (relation != null) json.put("hierarchical-Relationship", this.relation.getAbbr());
		return json;
	}
	
	@Override
	public String asString() {
		return code + " (" + name + ")";
	}
	
	@Override
	public String asAnonymString() {
		return ntua.iccs.harmonicss.cohort.data.json.Term.getCategoryByCode(code) + "-CODE";
	}

	@Override
	public String toString() {
		return "SimpleCodedValue [code=" + code + ", name=" + name + "]";
	}
}
