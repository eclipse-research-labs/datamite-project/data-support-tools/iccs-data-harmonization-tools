package ntua.iccs.harmonicss.cohort.data.xls;

/** The value of a Patient in the Field/Column specified */
public class ColumnData {

	private final int columnNumber;
	private final String data;
	
	public ColumnData(int columnNumber, String data) {
		this.columnNumber = columnNumber;
		this.data = data;
	}

	public int getColumnNumber() {
		return columnNumber;
	}

	public String getData() {
		return data;
	}

	public boolean isEmpty() {
		if (data == null) return true;
		if (data.trim().equals("")) return true;
		return false;
	}
	
	@Override
	public String toString() {
		return columnNumber + ":" + data;
	}
	
}
