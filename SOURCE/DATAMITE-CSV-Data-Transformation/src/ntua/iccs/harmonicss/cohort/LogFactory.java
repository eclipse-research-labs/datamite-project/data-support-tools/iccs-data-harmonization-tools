package ntua.iccs.harmonicss.cohort;

import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.timchros.core.log.messages.MemoryMessage;
import org.timchros.core.util.Checks;
import org.timchros.core.util.Format;

public class LogFactory {

	private static final String Log4jPropFilePath = "other-files/HELPER/TEXT/log4j.properties";
	
	static {
		try {
			/* Logging System Configuration */
			InputStream is = LogFactory.class.getClassLoader().getResourceAsStream(Log4jPropFilePath);
			if (is == null) is = new FileInputStream(Log4jPropFilePath);
		    final Properties prop  = new Properties();
			prop.load(is);
			PropertyConfigurator.configure(prop);
			/* Default Encoding UTF-8 */
			System.setProperty("file.encoding","UTF-8");
			Field charset = Charset.class.getDeclaredField("defaultCharset");
			charset.setAccessible(true);
			charset.set(null,null);
			getLoggerForClass(LogFactory.class).debug("================================================ INITIALIZATION ============================================================");
			Logger log = getLoggerForClass(LogFactory.class);
			log.info("System Version: " + DataTransformationService.version);
			log.info("System Property \"file.encoding\" = \"" + System.getProperty("file.encoding") + "\"");
			log.info("System " + new MemoryMessage().toParsableString());
			//log.info(AmountsValueTransformer.unitTransListToString());
		} catch(Throwable t) {
			throw new RuntimeException("Cannot initialize Logger...", t);
		}
	}
	
	/** Ensure that we will not create an instance of this class */
	private LogFactory() {
		
	}
	
	/** @return A {@link Logger} with the <b>SimpleName</b> of class provided ( augmented with white spaces so its length being 25 characters ) */
	public static Logger getLoggerForClass(Class<?> cls) {
		Checks.checkNotNullArg(cls, "cls");
		return Logger.getLogger(Format.formatStringLeftByMaxLength(cls.getSimpleName(), 25));
	}

}