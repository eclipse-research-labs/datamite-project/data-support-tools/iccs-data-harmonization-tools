package ntua.iccs.harmonicss.cohort;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Throable;

public class TestingPlaceMain {

	public static final Logger log = LogFactory.getLoggerForClass(TestingPlaceMain.class);
	
	public static void main(String[] args) throws Exception {
		
		System.out.println(" >> START");
		
		final DecimalFormat df = new DecimalFormat ("#.000000");

		final Float value1 = getFloat(",5");
		final Float value2 = getFloat("1,8");
		
		final String formula = "FIELD1 * FIELD2 * 1000";
//		
		final String expr1 = formula.replaceAll("FIELD1", "" + value1).replaceAll("FIELD2", "" + value2);
		final double calc1 = eval(expr1);
		
		System.out.println("FIELD1: " + value1);
		System.out.println("FIELD2: " + value2);
		System.out.println("calc= " + calc1);
		
		System.out.println();
		
		final String expr2 = formula.replaceAll("FIELD1", df.format(value1)).replaceAll("FIELD2", df.format(value2));
		final double calc2 = eval(expr2);
		System.out.println("FIELD1: " + df.format(value1));
		System.out.println("FIELD2: " + df.format(value2));
		System.out.println("calc= " + df.format(calc2));
		
//		final String jsonFilePath = "./@ICCS-Cohorts-Metadata/HUA/Mapping.json";
//		final Tuple2<List<FieldsMappingRule>, List<TermsMappingRule>> tuple = MappingRules.loadMappingRulesFromJsonFile(jsonFilePath);
////		for (FieldsMappingRule mr : tuple._1()) {
////			System.out.println(mr.toString());
////		}
//		
//		final String harmOwlFilePath = "OWL/HarmonicSS-Reference-Model+Vocabularies-v.0.9.3.owl";
//		final OntModel ontModel = OntoDataRepo.getOntModel(harmOwlFilePath);
//		
//		final String clsUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#BLOOD-730";
//		final Tuple2<OntClass, String> uniTuple = new DemoFieldValueReplacement().getTestUnit(ontModel, ontModel.getOntClass(clsUri));
//		log.info(uniTuple);
		
		System.out.println(" >> END");
		
	}
	
	/** @return A Float based on the given String */
	protected static Float getFloat(final String str) {
		if (isEmpty(str)) return null;
		try {
			return Float.parseFloat(str.replaceAll(",","\\."));
		} catch (Throwable t) {
			log.debug("getFloat(): Data \"" + str + "\" being ignored due to: " + Throable.throwableAsSimpleOneLineString(t));
			return null;
		}
	}
	
	/** @return <code>true</code> if the given String is NULL or EMPTY (e.g., contains one or more spaces), otherwise <code>false</code>*/
	protected static boolean isEmpty(final String str) {
		if (str == null) return true;
		if (str.trim().equals("")) return true;
		return false;
	}
	
//	public static void main(String[] args) {
//
//		//final String date = "2015/01/22";
//		final String date = "23/01/201";
//		final String dateFormat = "(dd)/(MM)/yyyy";
//		
//		System.out.println(getDateTuple(date, dateFormat));
//		
//	}
	
	private static final String dfYearOpMonthOpDay = "yyyy/(MM)/(dd)";
	private static final String dfOpDayOpMonthYear = "(dd)/(MM)/yyyy";

//	private static final List<String> userDateFormatList = Util.newArrayList(dfYearOpMonthOpDay, dfOpDayOpMonthYear);
	
	/** @return A Tuple with Year, Month, Day provided that the given value is not empty and follows the given Simple Date Format */
	protected static Tuple3<Integer, Integer, Integer> getDateTuple(final String dateStr, final String dateFormatGiven) {
		if (isEmpty(dateStr)) return null;
		
		final String dateFormat;
		if (dfYearOpMonthOpDay.equals(dateFormatGiven)) {
			final int slashCount = getSlashCount(dateStr);
			if (slashCount == 0) {
				dateFormat = "yyyy";
			} else if (slashCount == 1) {
				dateFormat = "yyyy/MM";
			} else {
				dateFormat = "yyyy/MM/dd";
			}
		} else if (dfOpDayOpMonthYear.equals(dateFormatGiven)) {
			final int slashCount = getSlashCount(dateStr);
			if (slashCount == 0) {
				dateFormat = "yyyy";
			} else if (slashCount == 1) {
				dateFormat = "MM/yyyy";
			} else {
				dateFormat = "dd/MM/yyyy";
			}
		} else {
			dateFormat = dateFormatGiven;
		}
		
		try {
			final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			final Date date = sdf.parse(dateStr);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			final int year = calendar.get(Calendar.YEAR);
			if (("" + year).length() != 4) System.out.println("The year detected (i.e., " + year + ") is NOT a 4-digit number. Given-Date: " + dateStr + " , Given-Date-Format: " + dateFormatGiven);
			final int month = 
				(dateFormat.indexOf('m') > 0 || dateFormat.indexOf('M') > 0) 
					? calendar.get(Calendar.MONTH) + 1 /* JANUARY --> 0 */ : -1;
			final int day = 
				(dateFormat.indexOf('d') >= 0 || dateFormat.indexOf('D') >= 0)  
					? calendar.get(Calendar.DAY_OF_MONTH) : -1;
			return Tuple3.tuple(year, month, day);
		} catch (Throwable t) {
			return null;
		}
	}
	
	/** @return Number of slash (/) characters */
	private static int getSlashCount(final String dateStr) {
		if (dateStr == null || isEmpty(dateStr)) return -1;
		int slashCount = 0;
		for (int i = 0; i < dateStr.length(); i++) {
		    if (dateStr.charAt(i) == '/') {
		    	slashCount++;
		    }
		}
		return slashCount;
	}
	
	public static double eval(final String str) {
	    return new Object() {
	        int pos = -1, ch;

	        void nextChar() {
	            ch = (++pos < str.length()) ? str.charAt(pos) : -1;
	        }

	        boolean eat(int charToEat) {
	            while (ch == ' ') nextChar();
	            if (ch == charToEat) {
	                nextChar();
	                return true;
	            }
	            return false;
	        }

	        double parse() {
	            nextChar();
	            double x = parseExpression();
	            if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
	            return x;
	        }

	        // Grammar:
	        // expression = term | expression `+` term | expression `-` term
	        // term = factor | term `*` factor | term `/` factor
	        // factor = `+` factor | `-` factor | `(` expression `)`
	        //        | number | functionName factor | factor `^` factor

	        double parseExpression() {
	            double x = parseTerm();
	            for (;;) {
	                if      (eat('+')) x += parseTerm(); // addition
	                else if (eat('-')) x -= parseTerm(); // subtraction
	                else return x;
	            }
	        }

	        double parseTerm() {
	            double x = parseFactor();
	            for (;;) {
	                if      (eat('*')) x *= parseFactor(); // multiplication
	                else if (eat('/')) x /= parseFactor(); // division
	                else if (eat('^')) x = Math.pow(x, parseFactor()); //exponentiation -> Moved in to here. So the problem is fixed
	                else return x;
	            }
	        }

	        double parseFactor() {
	            if (eat('+')) return parseFactor(); // unary plus
	            if (eat('-')) return -parseFactor(); // unary minus

	            double x;
	            int startPos = this.pos;
	            if (eat('(')) { // parentheses
	                x = parseExpression();
	                eat(')');
	            } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
	                while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
	                x = Double.parseDouble(str.substring(startPos, this.pos));
	            } else if (ch >= 'a' && ch <= 'z') { // functions
	                while (ch >= 'a' && ch <= 'z') nextChar();
	                String func = str.substring(startPos, this.pos);
	                x = parseFactor();
	                if (func.equals("sqrt")) x = Math.sqrt(x);
	                else if (func.equals("sin")) x = Math.sin(Math.toRadians(x));
	                else if (func.equals("cos")) x = Math.cos(Math.toRadians(x));
	                else if (func.equals("tan")) x = Math.tan(Math.toRadians(x));
	                else throw new RuntimeException("Unknown function: " + func);
	            } else {
	                throw new RuntimeException("Unexpected: " + (char)ch);
	            }

	            //if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation -> This is causing a bit of problem

	            return x;
	        }
	    }.parse();
	}
	
}
