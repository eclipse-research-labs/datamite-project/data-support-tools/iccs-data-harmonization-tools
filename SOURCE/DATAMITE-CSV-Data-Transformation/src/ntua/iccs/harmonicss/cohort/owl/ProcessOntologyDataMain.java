package ntua.iccs.harmonicss.cohort.owl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;

public class ProcessOntologyDataMain {

	/** Read the data existing in the given OWL file */
	public static void main(String[] args) throws Exception {

		final String owlFilePath = "./@ICCS-Cohorts-Metadata/HUA/HUA-Harmonized-Dummy-Data.owl";
		
		final OntModel ontoModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);
		InputStream is = new FileInputStream(new File(owlFilePath));
		ontoModel.read(is, null);
		is.close();

		// TODO: Cohort Data
		
		// TODO: Later get the Visit (only one Visit)
		
		final String personClassURI = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Person";
		final OntClass personOntClass = ontoModel.getOntClass(personClassURI);
		System.out.println(personOntClass);
		final ExtendedIterator<? extends OntResource> exIt = personOntClass.listInstances();
		while (exIt.hasNext()) {
			final OntResource ontResource = (OntResource) exIt.next();
			if (ontResource.isIndividual()) {
				Individual indiv = ontResource.asIndividual();
				System.out.println(indiv);
				
				final String personUIDpropertyURI = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Unique-ID";
				final RDFNode idnode = indiv.getPropertyValue(ontoModel.getDatatypeProperty(personUIDpropertyURI));
				System.out.println(idnode);
				
				final String personDateOfBirthpropertyURI = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Date-Birth";
				final RDFNode dobnode = indiv.getPropertyValue(ontoModel.getDatatypeProperty(personDateOfBirthpropertyURI));
				System.out.println(dobnode);
				
				final String associatedDataPropertyURI = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Associated-with-Data";
				
				final NodeIterator associatedNodesIterator = indiv.listPropertyValues(ontoModel.getObjectProperty(associatedDataPropertyURI));
				for (RDFNode node : associatedNodesIterator.toList()) {
					Resource resource = node.asResource();
					System.out.println(resource.getURI());
					
					// TODO: Depending on the URI ... get Data and record in the appropriate table
					
				} //  End of LOOP with data/nodes associated with the Person
				
			}
			
		} // END OF person individuals iteration
		
		
	} // END OF main()

}
