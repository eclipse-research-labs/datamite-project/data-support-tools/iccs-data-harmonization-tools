package ntua.iccs.harmonicss.cohort.owl;

public class ReferenceMoUri {

	public static final String NS = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/individual#";
	
	public static final String REFNS = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#";
	public static final String VOCNS = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#";
	
	public static final String CohortUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Cohort";
	public static final String cohortAttributeUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#cohort-Attribute";
	
	public static final String PersonUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Person";
	
	public static final String personHasDataUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Associated-with-Data";
	public static final String dataBelongToVisitUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#data-Belong-to-Visit";
	
	public static final String CodeUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Code";
	public static final String codeValueUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#code-Value";
	public static final String codeDisplayNameUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#code-Display-Name";
	
	public static final String AssessmentCodeUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Assessment-Code";
	public static final String UnitCodeUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Unit-Code";
	
	public static final String CodeExprUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Code-Expression";
	public static final String codeExprCodeUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#expression-Code-CV";
	public static final String codeExprHierarRelationUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#hierarchical-Relationship";
	public static final String codeExprBooleanOperUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#boolean-Operator";
	
	public static final String IntIntervalUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Integer-Interval";
	public static final String intIntervDNLUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#interval-Integer-Down-Limit";
	public static final String intIntervUNLUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#interval-Integer-Up-Limit";
	
	public static final String BooleanUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Boolean";
	public static final String booleanValueUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#boolean-DT-Value";
	
	public static final String AmountUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Amount";
	public static final String amountValueUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#amount-Value";
	public static final String amountUnitUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#amount-Unit-CV";
	
	public static final String AmountIntervalUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Amount-Interval";
	public static final String amountIntervDNLUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#interval-Amount-Down-Limit";
	public static final String amountIntervUNLUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#interval-Amount-Up-Limit";
	public static final String amountIntervUnitCodeUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#amount-Unit-CV";
	
	public static final String DateUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Date";
	public static final String dateYearUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#date-Year";
	public static final String dateMonthUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#date-Month";
	public static final String dateDayUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#date-Day";

	public static final String DateIntervalAUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Date-Interval-by-Start-End-Date";
	public static final String dateIntervalAStartDateUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#date-Interval-Start-Date";
	public static final String dateIntervalAEndDateUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#date-Interval-End-Date";
	
	public static final String HeathcareEntityVisitUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Heathcare-Entity-Visit";
	public static final String visitDateUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#he-Visit-Date";
	public static final String visitNumberUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#he-Visit-Number";
	public static final String visitStatusUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#he-Visit-Status-CV";
	public static final String visitTypeUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#he-Visit-Type-CV";

	public static final String ClosedVisitUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#VIS-STAT-02";
	public static final String HarmVisitUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#VISIT-00";
	
	public static final String testOutcomeUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology#med-Test-Outcome";
	public static final String testOutcomeUnitUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology#med-Test-Outcome-Unit";
	public static final String UnitUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Units-Expression";
	public static final String unitExpressionUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology#unit-Expression";
	
}
