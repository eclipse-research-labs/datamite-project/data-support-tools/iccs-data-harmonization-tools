package ntua.iccs.harmonicss.cohort.owl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;

import ntua.iccs.harmonicss.cohort.DataTransformationService;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;

public class OntoDataRepo {
	
	private static OntoDataRepo instance = null;
	
	public static void reInit() {
		instance = null;
	}
	
	public static OntoDataRepo getInstance() {
		synchronized (OntoDataRepo.class) {
			if (instance == null) {
				instance  = new OntoDataRepo();
			}
			return instance;
		}
	}
	
	/** A Map with the OWL entities (i.e., individuals) created for the corresponding terms (i.e., simple Coded Values) */
	final Map<SimpleCodedValue, Individual> cvIndivMap;
	
	/** A Map with the Entity Index per Class */
	final Map<OntClass, Integer> ontClsIndexMap;
	
	private OntoDataRepo() {
		cvIndivMap = new HashMap<>();
		ontClsIndexMap = new HashMap<>();
	}
	
	public Map<SimpleCodedValue, Individual> getCodedValueIndivMap() {
		return this.cvIndivMap;
	}
	
	public synchronized Integer getNextIndexFor(OntClass ontClass) {
		if (!ontClsIndexMap.containsKey(ontClass)) {
			ontClsIndexMap.put(ontClass, 0);
		}
		final Integer index = ontClsIndexMap.get(ontClass) + 1;
		ontClsIndexMap.put(ontClass, index);
		return index;
	}
	
	public synchronized String nextIndexAsStr(OntClass ontClass) {
		if (ontClass == null) throw new RuntimeException("Argument \"ontCls\" is null !");
		final Integer index = OntoDataRepo.getInstance().getNextIndexFor(ontClass);
		return indexTo5charSting(index);
	}

	private static final int charMax = 5;
	
	private String indexTo5charSting(int index) {
		final StringBuilder sb = new StringBuilder();
		final int indexLength = ("" + index).length();
		for (int i = 0; i < charMax - indexLength; i++) {
			sb.append("0");
		}
		sb.append(index);
		return sb.toString();
	}
	
	public static OntModel getOntModel(final String owlFilePath) throws IOException {
		//BasicConfigurator.configure();
		//Logger.getRootLogger().setLevel(Level.WARN);
		
		final OntModel ontoModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM, null);
		
		if (owlFilePath != null) {
			InputStream is = DataTransformationService.class.getClassLoader().getResourceAsStream(owlFilePath);
			if (is == null) is = new FileInputStream(new File(owlFilePath));
			ontoModel.read(is, null);
			is.close();
		}
//		if (owlFilePath != null) {
//			InputStream is = DataTransformationService.class.getClassLoader().getResourceAsStream(owlFilePath);
//			if (is != null) {
//				InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
//
//			}
//			if (is == null) is = new FileInputStream(new File(owlFilePath));
//			ontoModel.read(is, null);
//			is.close();
//		}
		return ontoModel;
	}
	
	public static void saveOntoToFile(OntModel ontModel, String path) throws IOException {
		final FileOutputStream fos = new FileOutputStream( new File(path) );
		fos.write("<?xml version=\"1.0\"?>\n".getBytes());
		
		ontModel.setNsPrefix("ref", "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#");
		ontModel.setNsPrefix("voc", "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#");
		ontModel.setNsPrefix("ind", ReferenceMoUri.NS);
		
		ontModel.write(fos, "RDF/XML");
		fos.flush();
		fos.close();
	}
	
	@Override
	public String toString() {
		return OntoDataRepo.class.getSimpleName() + " [ontClsIndexMap=" + ontClsIndexMap + "]";
	}
	
}
