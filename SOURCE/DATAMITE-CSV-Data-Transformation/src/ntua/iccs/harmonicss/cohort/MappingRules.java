package ntua.iccs.harmonicss.cohort;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.data.json.FieldsMappingRule;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.json.TermExpression;
import ntua.iccs.harmonicss.cohort.data.json.TermsMappingRule;
import ntua.iccs.harmonicss.cohort.data.BooleanOperator;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;
import ntua.iccs.harmonicss.cohort.trans.WebServiceForDT;

/** Provides the functionality for reading the Mapping Rules residing in the JSON file */
public class MappingRules {

	/** Ensures that we will not create an instance of this class */
	private MappingRules() {
		
	}
	
	// In case the name of the Java Class MR services is different from the real ones..
	private static final Map<String, String> clsMap = Util.newHashMap( );
	
	/** @return A List of Mapping Rules based on the given JSON file */
	public static Tuple2<List<FieldsMappingRule>, List<TermsMappingRule>> loadMappingRulesFromJsonFile(final String jsonFilePath) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		final String content = new String(Files.readAllBytes(Paths.get(jsonFilePath)), StandardCharsets.UTF_8);
		//final String content = new String(Files.readAllBytes(Paths.get(jsonFilePath)));
		final JSONObject json = JSONObject.fromObject(content);
		final JSONArray jarray = json.getJSONObject("correspondences").getJSONArray("jsonarray");
		
		// Check Mapping Rules (JSON data)
		for (int i = 0; i < jarray.size(); i++) {
			final JSONObject mrJson = jarray.getJSONObject(i);
			if (mrJson.isEmpty() || !mrJson.containsKey("entity1") || !mrJson.containsKey("entity2"))
				throw new RuntimeException("Invalid Mapping Rule: " + mrJson);
		}
		
		// Fields Mapping Rules
		final List<FieldsMappingRule> mapFielsList = new ArrayList<>();
		for (int i = 0; i < jarray.size(); i++) {
			final JSONObject mrJson = jarray.getJSONObject(i);
			final Map<Integer, Term> srcPropMap = getSourceProperties(mrJson.getJSONObject("entity1"));
			final Tuple2<Term, Map<Integer, Term>> trgTuple = getTargetEntityProperties(mrJson.getJSONObject("entity2"));
			
			// Practically ignore No-Fields Mapping Rules
			if (srcPropMap == null || trgTuple == null) continue;
				
			DataTransformation dtService = null;
			if (mrJson.containsKey("directTransformation")) {
				try {
					final Tuple2<String, Map<String, String>> dtTuple = getDataTranformationData(mrJson.getJSONObject("directTransformation"));
					// GET service (either a Java Class or a Web Service)
					final Tuple2<String, String> serviceTuple = getService(dtTuple._1());
					if (serviceTuple == null)
						throw new RuntimeException("Cannot find DT service based on data: \"" + dtTuple._1() + "\"");
					if (serviceTuple._1().equalsIgnoreCase("CLASS")) {
						final String cls = (clsMap.containsKey(serviceTuple._2())) ? clsMap.get(serviceTuple._2()) : serviceTuple._2();
						//dtService = (DataTransformation) Class.forName(cls).newInstance();
						dtService = (DataTransformation) Class.forName(cls).getDeclaredConstructor().newInstance();
						dtService.setArgs(dtTuple._2());
					} else if (serviceTuple._1().equalsIgnoreCase("WEB-SERVICE")) {
						dtService = new WebServiceForDT(serviceTuple._2());
						dtService.setArgs(dtTuple._2());
					} else {
						throw new RuntimeException("Cannot specify the DT service that should be used based on the given data: \"" + dtTuple._1() + "\".");
					}
				} catch(Throwable t) {
					throw new RuntimeException("Mapping Rule - Loading Data Tranformation .. Problem !", t);
				}
			}
			
			mapFielsList.add(new FieldsMappingRule(srcPropMap, trgTuple._1(), trgTuple._2(), dtService));
			
		} // END OF jarray Loop
	
		// Terms Mapping Rules
		final List<TermsMappingRule> mapTermList = new ArrayList<>();
		for (int i = 0; i < jarray.size(); i++) {
			final JSONObject mrJson = jarray.getJSONObject(i);
			final TermExpr srcEntity = getClassExpr(mrJson.getJSONObject("entity1"));
			final TermExpr trgEntity = getClassExpr(mrJson.getJSONObject("entity2"));
			final Relation relation = Relation.valueOf(mrJson.getString("relation").replaceAll("\\s+", "_"));
			
			// Practically ignore No-Terms Mapping Rules
			if (srcEntity == null || trgEntity == null) continue;
			
			mapTermList.add(new TermsMappingRule(srcEntity, trgEntity, relation));
		} // END OF jarray Loop
		
		return Tuple2.newTuple2(mapFielsList, mapTermList);
	}
	
	/* ************** About Fields Mapping **************  */
	
	/** @return A Map with the Source Properties (along with the order given) */
	private static Map<Integer, Term> getSourceProperties(JSONObject jo) {
		if (jo == null)
			throw new RuntimeException("JSONObject is NULL !");
		if (jo.isEmpty() || !jo.containsKey("pid"))
			throw new RuntimeException("Invalid JSONObject (no pid): " + jo);
		
		final String pid = jo.getString("pid");
		
		if (pid.equals("SimplePropertyPattern")) { if (jo.has("valuerange")) System.out.println(" ** TEST: " + jo.getString("valuerange"));
			final Map<Integer, Term> propMap = new HashMap<>();
			propMap.put(0, new Term(jo.getString("propertyuri"), jo.getString("propertyname"), ((jo.has("valuerange")) ? jo.getString("valuerange") : null)));
			return propMap;
		}
		if (pid.equals("PropertiesCollectionPattern")) {
			final Map<Integer, Term> propMap = new HashMap<>();
			final JSONArray ja = jo.getJSONArray("proparray");
			for (int i = 0; i < ja.size(); i++) { 
				final JSONObject propjo = ja.getJSONObject(i); // if (propjo.has("valuerange")) System.out.println(" ** TEST: " + propjo.getString("valuerange"));
				final int propIndex = (propjo.has("index")) ? propjo.getInt("index"): -1;
				if (propIndex < 0) throw new RuntimeException("Missing 'index'.");
				// ObjectProperty: SimpleRelationPattern
				if (propjo.has("relationuri"))
					propMap.put(propIndex, new Term(propjo.getString("relationuri"), propjo.getString("relationname")));
				// DatatypeProperty: SimplePropertyPattern
				if (propjo.has("propertyuri"))
					propMap.put(propIndex, new Term(propjo.getString("propertyuri"), propjo.getString("propertyname"), ((propjo.has("valuerange")) ? propjo.getString("valuerange") : null)));
			}
			return propMap;
		}
		
		return null;
	}
	
	/** @return A Tuple with the Target Class and its Properties (along with the order given) */
	private static Tuple2<Term, Map<Integer, Term>> getTargetEntityProperties(JSONObject jo) {
		if (jo == null)
			throw new RuntimeException("JSONObject is NULL !");
		if (jo.isEmpty() || !jo.containsKey("pid"))
			throw new RuntimeException("Invalid JSONObject (no pid): " + jo);
		
		final String pid = jo.getString("pid");
		if (pid.equals("ClassWithPropertiesRestrictionPattern")) {
			final JSONObject clsjo = jo.getJSONObject("cls");
			final Term clsTerm =  new Term(clsjo.getString("classuri"), clsjo.getString("classname"));
			final Map<Integer, Term> propMap = new HashMap<>();
			final JSONArray ja = jo.getJSONArray("proparray");
			for (int i = 0; i < ja.size(); i++) {
				final JSONObject propjo = ja.getJSONObject(i);
				final int propIndex = (propjo.has("index")) ? propjo.getInt("index"): -1;
				if (propIndex < 0)
					throw new RuntimeException("Missing 'index'.");
				// ObjectProperty
				if (propjo.has("relationuri"))
					propMap.put(propIndex, new Term(propjo.getString("relationuri"), propjo.getString("relationname")));
				// DatatypeProperty
				if (propjo.has("propertyuri"))
					propMap.put(propIndex, new Term(propjo.getString("propertyuri"), propjo.getString("propertyname")));
			}
			return new Tuple2<Term, Map<Integer, Term>>(clsTerm, propMap);
		}
		
		return null;
	}
	
	/** @return A Tuple with the Service URI along with the given Arguments */
	private static Tuple2<String, Map<String, String>> getDataTranformationData(JSONObject jo) {
		if (jo == null)
			throw new RuntimeException("JSONObject is NULL !");
		if (jo.isEmpty() || !jo.containsKey("uri"))
			throw new RuntimeException("Invalid JSONObject (no pid): " + jo);
		
		final String serviceUri = jo.getString("uri").trim();
		if (serviceUri.equals(""))
			throw new RuntimeException("Data Transformation Service URI is emtpy !");
		final Map<String, String> argsMap = new HashMap<>();
		if (jo.containsKey("arguments")) {
			final JSONArray ja = jo.getJSONArray("arguments");
			for (int i = 0; i < ja.size(); i++) {
				final JSONObject argjo = ja.getJSONObject(i);
				argsMap.put(
					argNameClean(argjo.getString("argname")),
					argjo.getString("argvalue")
				);
			}
		}
		
		return new Tuple2<String, Map<String,String>>(serviceUri, argsMap);
	}
	
	public static String argNameClean(final String str) {
		if (str == null) return null;
		return str.replaceAll("\\s+", "_").replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("/", "");
	}
	
	/** @return A Tuple with the service Type and the Service Name/URL */
	private static Tuple2<String, String> getService(final String str) {
		if (str == null) return null;
		final String strTrim = str.trim();
		if (strTrim.equals("")) return null;
		// FIND the first occurrence of semicolon
		final int index = Math.max(strTrim.indexOf(":"), Math.max(strTrim.lastIndexOf("/"), strTrim.lastIndexOf("#")));
		if (index <= 0) return null;
		final String type = str.substring(0, index).trim();
		final String name = str.substring(index + 1).trim();
		return new Tuple2<String, String>(type, name);
	}
	
	/* ************** About Terminology Alignment **************  */
	
	/** @return Either a Term or an AND/OR Terms Expression */
	private static TermExpr getClassExpr(JSONObject jo) {
		if (jo == null)
			throw new RuntimeException("JSONObject is NULL !");
		if (jo.isEmpty() || !jo.containsKey("pid"))
			throw new RuntimeException("Invalid JSONObject (no pid): " + jo);
		
		final String pid = jo.getString("pid");
		
		if (pid.equals("SimpleClassPattern")) {
			return new Term(jo.getString("classuri"), jo.getString("classname")) ;
		}
		
		if (pid.equals("ClassIntersectionPattern") || pid.equals("ClassUnionPattern")) {
			final JSONArray ja = jo.getJSONArray("classarray");
			final List<TermExpr> termExprList = new ArrayList<>();
			for (Object ob : ja) {
				final JSONObject obJo = (JSONObject) ob;
				termExprList.add(getClassExpr(obJo));
			}
			final BooleanOperator booleanOp = 
				(pid.equals("ClassIntersectionPattern")) 
					? BooleanOperator.AND 
					: BooleanOperator.OR;
			
			return new TermExpression(booleanOp, termExprList);
		}

		return null;
	}
	
}
