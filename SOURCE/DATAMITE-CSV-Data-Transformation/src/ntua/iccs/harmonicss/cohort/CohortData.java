package ntua.iccs.harmonicss.cohort;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ntua.iccs.harmonicss.cohort.data.xls.ColumnData;

/** Provides the functionality for reading the content of a MS Excel File. */
public class CohortData {

	private static final Logger log = LogFactory.getLoggerForClass(CohortData.class);
	
	private static final Comparator<Entry<Integer, ?>> entryKeyIntComparator = new Comparator<Entry<Integer, ?>>() {
		@Override public int compare(Entry<Integer, ?> e1, Entry<Integer, ?> e2) {
			return e1.getKey() - e2.getKey();
		}
	};
	
	/** Ensures that we will not create an instance of this class */
	private CohortData() {
		
	}
	
	/** @return A List of Patients. For each patient there is a Map. The <b>key</b> is the name of the XLS Field. The <b>value</b> contain the patient data in this field. */
	public static List<Map<String, String>> loadExcelData(final String xlsFilePath) throws Exception {
		final Map<Integer, Map<Integer, List<ColumnData>>> sheetDataMap = CohortData.getDataFromXLS(xlsFilePath);
		int nonEmptySheetsNum = 0;
		for (Entry<Integer, Map<Integer, List<ColumnData>>> entry : sheetDataMap.entrySet()) {
			if(entry.getValue().size() > 0) {
				nonEmptySheetsNum++;
			}
		}
		if (sheetDataMap.size() != nonEmptySheetsNum) {
			final int emptySheetsNum = sheetDataMap.size() - nonEmptySheetsNum;
			log.warn("There are " + emptySheetsNum + " EMPTY SHEETS in the MS Excel (XLS) with the patient data.");
		}
		final List<Map<String, String>> patientDataMapList = new ArrayList<>();
		if (sheetDataMap.size() == 1) {
			final Map<Integer, List<ColumnData>> sheetData = sheetDataMap.get(0);
			final List<Entry<Integer, List<ColumnData>>> entryList = new ArrayList<>(sheetData.entrySet());
			Collections.sort(entryList, entryKeyIntComparator);
			for (Entry<Integer, List<ColumnData>> entry : entryList) {
				// Ignore the First Row (contains the name of the fields)
				if (entry.getKey() == 0) continue;
				// Place the Fields Values in a Map (some fields with empty values may not exist in the map)
				final Map<String, String> patientDataMap = new HashMap<>();
				for (ColumnData data : entry.getValue()) {
					patientDataMap.put(numberToABC(data.getColumnNumber()), data.getData());
				}
				patientDataMapList.add(patientDataMap);
			}
		} else {
			final List<Entry<Integer, Map<Integer, List<ColumnData>>>> sheetEntryList = new ArrayList<>(sheetDataMap.entrySet());
			Collections.sort(sheetEntryList, entryKeyIntComparator);
			final Map<Integer, Map<String, String>> rowMap = new HashMap<>();
			for (Entry<Integer, Map<Integer, List<ColumnData>>> sheetEntry : sheetEntryList) {
				final List<Entry<Integer, List<ColumnData>>> rowEntryList = new ArrayList<>(sheetEntry.getValue().entrySet());
				Collections.sort(rowEntryList, entryKeyIntComparator);
				for (Entry<Integer, List<ColumnData>> rowEntry : rowEntryList) {
					// Ignore the First Row (contains the name of the fields)
					if (rowEntry.getKey() == 0) continue;
					// Prepare Row/Patient Map 
					if (!rowMap.containsKey(rowEntry.getKey())) rowMap.put(rowEntry.getKey() , new HashMap<>());
					// Place the Fields Values in a Map (some fields with empty values may not exist in the map)
					for (ColumnData data : rowEntry.getValue()) {
						rowMap.get(rowEntry.getKey()).put(combine(sheetEntry.getKey(), numberToABC(data.getColumnNumber())), data.getData());
					} // END OF columns iteration
				} // END OF rows iteration
			} // END OF sheets iteration
			for (Entry<Integer, Map<String, String>> entry : rowMap.entrySet()) {
				patientDataMapList.add(entry.getValue());
			}
		}
		return patientDataMapList;
	}
	
	private static final char SEP = '-';
	
	private static String combine(final Object sheetNumber, final Object columnIndex) {
		return "" + sheetNumber + SEP + columnIndex; 
	}
	
	/** @return A {@link Map} with the Non-empty Fields per Row */
	@SuppressWarnings("resource")
	private static Map<Integer, Map<Integer, List<ColumnData>>> getDataFromXLS(final String xlsFilePath) throws Exception {
		if (xlsFilePath == null || xlsFilePath.trim().equals("")) 
			throw new RuntimeException("Parameter: xlsFilePath NOT specified !");
		if ( (!xlsFilePath.toLowerCase().endsWith(".xls")) && (!xlsFilePath.toLowerCase().endsWith(".xlsx"))) 
			throw new RuntimeException("File \"" + xlsFilePath + "\" is NOT an Excel Document !");
		
		final InputStream is = new FileInputStream( new File(xlsFilePath));
		if (xlsFilePath.toLowerCase().endsWith(".xls")) {
			return getDataFromXLS(is, "xls");
		} else if (xlsFilePath.toLowerCase().endsWith(".xlsx")) {
			return getDataFromXLS(is, "xlsx");
		} 
		return null;
	}
	
	public static Map<Integer, Map<Integer, List<ColumnData>>> getDataFromXLS(final InputStream is, final String xlsType) throws Exception {
		final Workbook workbook = 
			(xlsType.toLowerCase().equals("xls")) ? 
				new HSSFWorkbook(is) :
			(xlsType.toLowerCase().equals("xlsx")) ? 
				new XSSFWorkbook( is ) : null;

		final Map<Integer, Map<Integer, List<ColumnData>>> sheetMap = new HashMap<Integer, Map<Integer, List<ColumnData>>>();
		final Iterator<Sheet> sheetIt = workbook.sheetIterator();		
		while (sheetIt.hasNext()) {
			final Sheet sheet = ( Sheet) sheetIt.next();
			final String name = sheet.getSheetName();
			final int index = workbook.getSheetIndex(sheet);
			try {
				final Map<Integer, List<ColumnData>> rowFieldsMap = new HashMap<Integer, List<ColumnData>>();
				final Iterator<Row> rowIt = sheet.iterator();
				while (rowIt.hasNext()) {
				   	final Row row = rowIt.next();
				    final int rowNum = row.getRowNum();
				    final Iterator<Cell> cellIt = row.cellIterator();
				    while (cellIt.hasNext()) {
				      	final Cell cell = (Cell) cellIt.next();
						final int colNum = cell.getColumnIndex();
						final String cellData = getCellData(cell);
						if (cellData == null) continue;
						if (!rowFieldsMap.containsKey(rowNum)) {
							rowFieldsMap.put(rowNum, new ArrayList<ColumnData>());
						}
						rowFieldsMap.get(rowNum).add(new ColumnData(colNum, cellData));
				    } // END OF CELLs LOOP
				} // END OF ROWs LOOP
				sheetMap.put(index, rowFieldsMap);
			} catch (Throwable t) {
				throw new RuntimeException("Unexpected error while reading data from sheet: " + index + " :: " + name, t);
			}
			
		} // END OF SHEETs LOOP
		
	    workbook.close();
		return sheetMap;
	}
	
	private static final DateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
	private static final NumberFormat numFormater = new DecimalFormat("#.####");
	
	/** @return Data existing in a specific Cell of an Excel File in the form of a String */
	private static String getCellData(Cell cell) {
		if (cell == null || cell.toString().trim().equals("")) return null;
		if (cell.getCellTypeEnum() == CellType.STRING) {
			return cell.getStringCellValue().replaceAll("\\s+", " ").trim();
		} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				return dateFormater.format(cell.getDateCellValue());
		    } else {
		    	return numFormater.format(cell.getNumericCellValue());
		    }
		} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
			return ("" + cell.getBooleanCellValue()).toLowerCase();
		} else if (cell.getCellTypeEnum() == CellType.FORMULA) {
			return "FORMULA: " + cell;
		} else if (cell.getCellTypeEnum() == CellType.BLANK) {
			return null;
		} else if (cell.getCellTypeEnum() == CellType.ERROR) {
			return null;
		} else if (cell.getCellTypeEnum() == CellType._NONE) {
			System.out.println("NONE " + cell);
			return "NONE: " + cell;
		} else {
			System.out.println(" ****** " + cell);
			return cell.toString().trim();
		}
		
	}

	private static final int BASE = 26;
	
	public static String numberToABC(final int n) {
		if (n < 0) 
			throw new RuntimeException("Negative number.. n= " + n);
		if (n == 0) 
			return "A";
		List<Integer> digitList = new ArrayList<Integer>();
		int tmp = n;
		while (tmp != 0) {
			int rem = tmp % BASE;
			digitList.add(rem);
			tmp = tmp / BASE;
		}
		Collections.reverse(digitList);
		// First ... 
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < digitList.size() - 1; i++) {
			final Integer digit = digitList.get(i);
			final char c = (char) ('A' + (digit - 1));
			sb.append(c);
		}
		// Last Digit
		final char lastChar = (char) ('A' + (digitList.get(digitList.size()-1)));
		sb.append(lastChar);
		return sb.toString();
	}
	
	private static final char DASH = '-';
	
	public static int keyToNumber(final String str) {
		if (str == null || str.trim().equals(""))
			throw new RuntimeException("String is empty.. str= " + str);
		
		final int dashIndex = str.indexOf(DASH);
		if (dashIndex > 0) {
			final String sheetNumStr = str.substring(0, dashIndex);
			final String fieldAbcStr = str.substring(dashIndex + 1);
			return 1000 * Integer.parseInt(sheetNumStr) + abcStrToNumber(fieldAbcStr);
		} else {
			return abcStrToNumber(str);
		}
	}
	
	public static int abcStrToNumber(final String str) {
		if (str == null || str.trim().equals(""))
			throw new RuntimeException("String is empty.. str= " + str);
		
		Integer n = 0;
		
		for (int i = 0; i < str.length() - 1; i++) {
			final char c = str.charAt(i);
			final int digit = (int) c - 'A' + 1;
			final int pow = str.length() - i - 1;
			final int number = digit * (int) Math.pow(BASE, pow);
			n += number;
		}
		final char lastChar = str.charAt(str.length()-1);
		final int lastNumber = (int) lastChar - 'A';
		n += lastNumber;
		
		return n;
			
	}
	
}
