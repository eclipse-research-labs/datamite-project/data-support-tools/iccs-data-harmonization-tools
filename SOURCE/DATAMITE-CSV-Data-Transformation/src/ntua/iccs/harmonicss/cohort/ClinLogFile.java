package ntua.iccs.harmonicss.cohort;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class ClinLogFile {

	public static String clinLogFilePath = "./Clinician-CohortDataHarmonization-LogFile.log";
	
	private static ClinLogFile instance = null;
	
	public static ClinLogFile getInstance() {
		synchronized (ClinLogFile.class) {
			if (instance == null) {
				instance  = new ClinLogFile();
			}
			return instance;
		}
	}
	
	private Writer writer = null;
	
	private ClinLogFile() {
		
	}
	
	public void openFile() {
		if (writer == null) {
			try {
				writer = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(clinLogFilePath, true), "UTF8"));
			} catch (Throwable t) {
				throw new RuntimeException("Cannot OPEN File: " + clinLogFilePath);
			}
		}
	}
	
	public void writeToFile(final String msg) {
		try {
			if (writer != null) writer.write(msg);
		} catch (IOException e) {
			throw new RuntimeException("Cannot WRITE message to File: " + clinLogFilePath);
		}	
	}
	
	public void closeFile() {
		if (writer != null) {
			try {
				writer.flush();
				writer.close();
				writer = null;
			} catch (Throwable t) {
				throw new RuntimeException("Cannot CLOSE File: " + clinLogFilePath);
			}
		}
	}
		
}
