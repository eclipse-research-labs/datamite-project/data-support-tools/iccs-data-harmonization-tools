package ntua.iccs.harmonicss.cohort.trans.quest;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Fields with the ESSDAI Domain Activity Level. The corresponding Reference Model term should be specified. The values of this field should be independently ** aligned with ** the Activity Level terms (i.e., NO, LOW, MODERATE, HIGH) specified in the Reference Model. */
public class ESSDAIDomainActivityLevel extends DataTransformation {

	private enum ServiceArg { ESSDAI_Domain }
	
	/** Provides the values of the parameters specified in the right side taking into account the patient data in the given fields as well as additional parameters provided. The value of the 1st Field should be an integer. For specifying whether the score was normal or not (Assessment), at least one of the two optional properties about UNL and/or DNL should be given.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#ESSDAI-Domain-AL that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String actLevStr = data.get(0); // ESSDAI Domain Activity Level

		// Additional Data / Arguments (if any)
		final String essdaiDomainUri = getArgValue(ServiceArg.ESSDAI_Domain);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass essdaiDomainTerm = getReferenceModelTermWithURI(essdaiDomainUri); 
		final Tuple2<TermExpr, Relation> actLevTermExprT = getReferenceModelTermForCohortValue(actLevStr, 0);
		
		// IF the value is empty or not aligned with Reference Model terms, no data will be recorded
		if (actLevTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is an Activity Level Term
		if (!actLevTermExprT._1().isSimpleActivityLevelTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + actLevTermExprT._1().asString() + "\" is NOT an Activity Level Term !");
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(essdaiDomainTerm);
		final PropertyValue propValue1 = new SimpleCodedValue(actLevTermExprT);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#quest-ESSDAI-Domain-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#activity-Level-CV
		
		return newPosTuple(map);
	}

}