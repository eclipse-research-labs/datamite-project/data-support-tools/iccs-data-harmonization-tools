package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.BooleanValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a cohort Field about a Lab Test with a Boolean Value (i.e., YES/NO). However the specific numeric outcome was recorded. The corresponding Reference Model term should be given. Also, the format of the number existing in the field along with the range of YES values (i.e., when the value of the field belongs in this range, it indicates the outcome of the given test was true) should be specified. */
public class LabTestOutcomeBooleanBasedOnNumber extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Field_Number_Format, Down_Limit, Upper_Limit }
	
	/** Provides the appropriate Terms for the specific Lab Test along with its Outcome (i.e., Boolean Value) based on the Value of the given Field and the additional parameters provided (i.e., corresponding Reference Model term and Range of YES values). In case the Field is empty, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testNumericValueStr = data.get(0); // Lab Test Outcome: Numeric Value

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String valueFormatIndex = getArgValue(ServiceArg.Field_Number_Format);
		final String downLimitStr = getArgValue(ServiceArg.Down_Limit);
		final String upLimitStr = getArgValue(ServiceArg.Upper_Limit);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);

		// Ensure that the value of the given Test is a Boolean 
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.BOOLEAN))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not a Boolean ! Possible Test Outcome(s): " + expOutSet);

		final Tuple2<Float, Tuple2<String, String>> valueTuple = getAmountValueTuple(testNumericValueStr, valueFormatIndex, "-", testTerm);
		
		// IF value is empty or not a Real Number, no data will be recorded
		if (valueTuple == null) return null;
		
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downLimitStr, upLimitStr, "-", testTerm);
		
		// Check if value is within the given Range of Values (in that case, the term Normal is being returned)
		final Term assessTerm = (normRangeTuple != null) ? getAssessmentForValue(valueTuple._1(), normRangeTuple._1(), normRangeTuple._2()) : null;
		
		// IF we cannot Find the assessment based on the given data (i.e., neither DNL and UNL specified), no data will be recorded
		if (assessTerm == null) return null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = 
			(assessTerm.isAssessmentTermNORMAL()) ? new BooleanValue(true) : new BooleanValue(false);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		
		return newPosTuple(map);
	}

}
