package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 2 fields: A) A Field that indicates whether the person has performed a Surgery or not and B) Another Field that indicate the period of time (after date) when the Surgery took place. The values of the 1st Field should be independently ** aligned with ** the Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Additionally the format of the Date (2nd field) should be given. */
public class SurgeryConfirmPlusPeriodAfterDate extends DataTransformation {

	private enum ServiceArg { Date_Format }
	
	/** The system provides the appropriate data taking into account the values of the given Fields (i.e., surgery value and start date), additional data provided (i.e., date format) and mapping rules specified. Depending on the value of the 1st field, it indicates whether the person has performed a surgery or not. Provided that at least one of the two other fields is not empty, the period of time when the surgery took place is also provided.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Surgery that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String surgeryConfirmValue = data.get(0); // Surgery: YES/NO
		final String startDateStr = data.get(1); // Surgery in Period Start Date

		// Additional Data / Arguments (if any)
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(surgeryConfirmValue, 0);
		
		// IF the value is empty, it will be ignored.
		if (confirmValue == null) return null;

		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");

		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(startDateStr, dateFormat);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = 
			(dateTuple != null) ? new DateIntervalTypeA(new DateValue(dateTuple), null): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#procedure-Date
		
		// Depending on the value of the Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
		
	}

}
