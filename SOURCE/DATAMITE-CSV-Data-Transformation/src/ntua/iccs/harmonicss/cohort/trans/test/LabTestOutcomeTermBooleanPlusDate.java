package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 2 Cohort Fields, A) A Field that indicates whether a specific Lab Test Outcome Term was observed (e.g., specific ANA pattern or Cryo Type) or not, and B) Another Field with the Date the examination took place. The corresponding Reference Model term should be given. Also, the values of the 1st field (with the test outcome) should be independently ** aligned with ** Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Additionally the format of the Date (2nd field) should be given. */
public class LabTestOutcomeTermBooleanPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Date_Format }
	
	public static final String anaPatternTestUri = 
		"http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#BLOOD-523";
	public static final String cryoTypeTestUri = 
		"http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#BLOOD-312";
	
	/** Provides the appropriate Terms for the specific Lab Test along with its Outcome and the Date the test took place, based on the Values of the given Fields, the parameters provided (i.e., corresponding Reference Model term, Date Format) and the Mapping Rules specified, provided that the value of the 1st field is not empty. More precisely, IF the value is yes (i.e., aligned with the YES confirmation term) the service indicates that the specific outcome was observed, otherwise not.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String outcomeTermConfirmValue = data.get(0); // Lab Test Outcome Term: Boolean Value
		final String testDateStr = data.get(1); // Lab Test Date

		// Additional Data / Arguments (if any)
		final String outcomeTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String testDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass outcomeTerm = getReferenceModelTermWithURI(outcomeTermUri);
		
		final OntClass testTerm;
		if (outcomeTerm.getSuperClass().getLocalName().equals("Antinuclear-Antibody-Pattern")) {
			testTerm = getReferenceModelTermWithURI(anaPatternTestUri);
		} else if (outcomeTerm.getSuperClass().getLocalName().equals("Cryoglubin-Type")) {
			testTerm = getReferenceModelTermWithURI(cryoTypeTestUri);
		} else {
			throw new RuntimeException("There is no Reference Model Test with outcome the given Term: \"" + outcomeTerm.getLocalName() + "\"");
		}
		
		final Tuple2<TermExpr, Relation> outcomeConfirmValueTermExprT = getReferenceModelTermForCohortValue(outcomeTermConfirmValue, 0);
		
		// IF the value is empty, no data will be recorded
		if (outcomeConfirmValueTermExprT == null) return null;

		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!outcomeConfirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + outcomeConfirmValueTermExprT._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(testDateStr, testDateFormat);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm); 
		final PropertyValue propValue1 = new SimpleCodedValue(outcomeTerm); 
		final PropertyValue propValue2 = (dateTuple != null) ? new DateValue(dateTuple): null; 

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		
		// Depending on the value of the Test Outcome Field, return a positive/negative "statement"
		if (outcomeConfirmValueTermExprT._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
	}

}
