package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping three Cohort Fields: a) A Field that indicates whether the person has been diagnosed with a particular condition or not, and b) Two other Fields about the Period of Time (i.e., Start and End Date) of Diagnosis. The corresponding Reference Model term (Medical Condition) should be specified. Also, the values of the 1st Field should be independently ** aligned with ** the Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Additionally the Format of the Dates (2nd and 3rd Fields) should be given. */
public class MedConditionDiagnosisPlusPeriod extends DataTransformation {

	private static final Logger log = LogFactory.getLoggerForClass(MedConditionDiagnosisPlusPeriod.class);
	
	private enum ServiceArg { Reference_Model_Condition, Dates_Format }
	
	/** Provides the corresponding Reference Model term (i.e., Medical Condition) along with the Period of time the condition diagnosed based on the given Data (i.e., Fields values, Additional Data and Mapping Rules) provided that the value existing in the 1st field indicates that the person has been diagnosed with the specific Medical Condition.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Diagnosis that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String condConfirmValue = data.get(0); // Medical Condition
		final String diagPeriodStartDate = data.get(1); // Date of Diagnosis Period Start
		final String diagPeriodEndDate = data.get(2); // Date of Diagnosis Period End

		// Additional Data / Arguments (if any)
		final String condTermUri = getArgValue(ServiceArg.Reference_Model_Condition);
		final String dateFormat = getArgValue(ServiceArg.Dates_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass condTerm = getReferenceModelTermWithURI(condTermUri);
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(condConfirmValue, 0);

		// IF the value is empty, it will be ignored.
		if (confirmValue == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Integer, Integer, Integer> startDateTuple = getDateTuple(diagPeriodStartDate, dateFormat);
		final Tuple3<Integer, Integer, Integer> endDateTuple = getDateTuple(diagPeriodEndDate, dateFormat);

		// Ensure that the Date Interval (Start Date - End Date) is valid
		final boolean isDateIntervalValid = isDateIntervalValid(startDateTuple, endDateTuple);
		if (!isDateIntervalValid) {
			log.warn("Invalid Date Interval: [" + startDateTuple + " , " + endDateTuple + "] Period of Time is being ignored !");
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(condTerm);
		final PropertyValue propValue1 = 
			((startDateTuple != null || endDateTuple != null) && isDateIntervalValid) 
				? new DateIntervalTypeA(startDateTuple, endDateTuple): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-Date
		
		// Depending on the value of the Condition Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
	}

}
