package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 2 Cohort Fields, A) Field with the amount measured (i.e., both value and unit of measurement) for a specific Lab Test, and B) Another Field with the Date it took place. The corresponding Reference Model term should be specified. The Format of the number (i.e.., the 1st token of the String) existing in the Field should be given. Additionally, the unit of measurement recorded (i.e., the 2nd token of the String) should be linked (i.e., either being the same or can be converted) with the unit of measurement specified in the Reference Model about the given Lab Test. Also we would like to know the Normal Range of values (if applicable). Additionally the format of the Date (2nd field) should be given. */
public class LabTestOutcomeAmountPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Blood_Test, Field_1st_Token_Number_Format, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, UNL_and_DNL_Unit_of_Measurement, Date_Format }
	
	/** Provides the appropriate Term for the specific Lab Test, the Date it took place and the Outcome measured based on the Values of the given Fields (i.e., the value/1st-token and unit-of-measurement/2nd-token of the 1st Field and the Date specified in the 2nd Field) and the additional parameters provided (i.e., corresponding Reference Model term and number/1st-token Format, date format). Also, provided that the UNL and DNL specified (including Unit of Measurement), the Normal Range of Values will be also recorded, as well as whether the outcome was normal or not. In case the value of the 1st Field is Empty or the value does not follow the predefined format (number unit-of-measurement), no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String amountStr = data.get(0); // Lab Test Outcome: Amount (Number Unit-of-Measurement)
		final String testDateStr = data.get(1); // Lab Test Date

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Blood_Test);
		final String tokenFormatIndex = getArgValue(ServiceArg.Field_1st_Token_Number_Format);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String limitsUnit = getArgValue(ServiceArg.UNL_and_DNL_Unit_of_Measurement);
		final String testDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test can be an Amount
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.AMOUNT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Amount ! Possible Test Outcome(s): " + expOutSet);


		final Tuple2<Float, Tuple2<String, String>> valueTuple = getAmountValueTuple(amountStr, tokenFormatIndex, testTerm);
				
		// IF the value is empty or not a Number, no data will be recorded
		if (valueTuple == null) return null;
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(testDateStr, testDateFormat);
		
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, limitsUnit, testTerm);
		final Term assessTerm = (normRangeTuple != null) ? getAssessmentForValue(valueTuple._1(), normRangeTuple._1(), normRangeTuple._2()) : null;
		
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue2 = new AmountValue(valueTuple);
		final PropertyValue propValue3 = (assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;
		final PropertyValue propValue4 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;	
		
		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(4, propValue4); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}
