package ntua.iccs.harmonicss.cohort.trans;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;

/** 
 * The functionality that should be provided by every Mapping Rule Data transformation service 
 */
public interface DataTransformationI {

	/** 
	 * Specify Data Transformation Attributes specified in the Mapping Rule 
	 */
	public void setArgs(Map<String, String> args);
	
	/** 
	 * Specify the Range of Input Field Values 
	 */
	public void setValueRange(Map<Integer, String> paramValueRangeMap);
	
	/**
	 * Produces the values of the Parameters existing in the Right side of the Mapping Rule
	 * based on the values of the parameters mentioned in the Left side (patient data) 
	 * and the Data Transformation Attributes (should be given at a previous step)
	 * 
	 * The 1st element indicates whether the patient is Linked with the data provided or not (Negative Assertion)
	 * 
	 * The 2nd element indicates the amount of Entities we should create (often, only one) along with the parameters of such Entities.
	 * 
	 * @return A Tuple with a Boolean value and a List with the Data (Map) of the OWL entities we should create
	 */
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data);
	
}





