package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping three Fields, A) a Field that indicates whether a Woman was pregnant or not, and B) Two other fields about the period of conception (start, end dates). The values of the 1st field should be independently ** aligned with ** the Confirmation YES/NO terms specified in the Reference Model. Additionally the Format of the Date (2nd and 3rd Fields) should be given. */
public class PregnancyConceptionPeriodBetweenDates extends DataTransformation {

	private enum ServiceArg { Date_Format }
	
	/** Provides an Entity that indicates whether the woman was pregnant or not based on the values of the given fields (i.e., pregnancy confirmation and period of conception), additional data provided (i.e., date format) and mapping rules specified. Provided that at least one of the two date fields is not empty, the period of conception is also recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Pregnancy that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String pregConfirmValue = data.get(0); // Pregnancy: YES/NO
		final String startDateStr = data.get(1); // Conception in Period Start Date
		final String endDateStr = data.get(2); // Conception in Period End Date

		// Additional Data / Arguments (if any)
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));

		// Data Processing
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(pregConfirmValue, 0);
		
		// IF the value is empty, it will be ignored.
		if (confirmValue == null) return null;

		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");

		final Tuple3<Integer, Integer, Integer> startDateTuple = getDateTuple(startDateStr, dateFormat);
		final Tuple3<Integer, Integer, Integer> endDateTuple = getDateTuple(endDateStr, dateFormat);
		
		// Ensure that the Date Interval (Start Date - End Date) is valid
		final boolean isDateIntervalValid = isDateIntervalValid(startDateTuple, endDateTuple);
		if (!isDateIntervalValid) {
			log.warn("Invalid Date Interval: [" + startDateTuple + " , " + endDateTuple + "] Period of Time is being ignored !");
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = 
			((startDateTuple != null || endDateTuple != null) && isDateIntervalValid) ? 
				new DateIntervalTypeA(startDateTuple, endDateTuple): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#pregnancy-Conception-Date
						
		// Depending on the value of the Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
		
	}

}
