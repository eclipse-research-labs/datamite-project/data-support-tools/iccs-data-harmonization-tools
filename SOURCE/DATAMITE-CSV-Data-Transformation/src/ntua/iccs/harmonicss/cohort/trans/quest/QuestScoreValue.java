package ntua.iccs.harmonicss.cohort.trans.quest;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.IntegerInterval;
import ntua.iccs.harmonicss.cohort.data.owl.simple.IntegerValue;
import ntua.iccs.harmonicss.cohort.data.owl.simple.RealNumberValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping a Cohort Fields with the value of a Questionnaire Score (integer). 
 * <p>
 * The corresponding Reference Model term should be specified. 
 * <p>
 * The Normal Range of Values for this Questionnaire can be also specified (optional). 
 */
public class QuestScoreValue extends DataTransformation {

	private enum ServiceArg { Reference_Model_Questionnaire_Score, Questionnaire_Date_Format, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL }
	
	/** Provides the values of the parameters specified in the right side taking into account the patient data in the given fields as well as additional parameters provided. The value of the 1st Field should be an integer. For specifying whether the score was normal or not (Assessment), at least one of the two optional properties about UNL and/or DNL should be given.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Questionnaire-Score that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// TODO: TEST
		
		// Input Fields
		final String questScore = data.get(0); // Questionnaire Score

		// Additional Data / Arguments (if any)
		final String scoreTermUri = getArgValue(ServiceArg.Reference_Model_Questionnaire_Score);
		final String downNormLimit = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimit = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass scoreTerm = getReferenceModelTermWithURI(scoreTermUri); 

		final Object score = getNumber(questScore);
		
		// IF score is empty or not an Integer, no data will be recorded
		if (score == null) return null;
		
		final Tuple2<Integer, Integer> normRangeTuple = getIntegerNormalRangeTuple(downNormLimit, upNormLimit);
		final Term assessTerm;
		if (score instanceof Integer) {
			assessTerm = (normRangeTuple != null) ? getAssessmentForValue((Integer)score, normRangeTuple._1(), normRangeTuple._2()) :  null;
		} else {
			assessTerm = (normRangeTuple != null) ? getAssessmentForValue((Float)score, new Float(normRangeTuple._1()), new Float (normRangeTuple._2())) :  null;
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();

		final PropertyValue propValue0 = new SimpleCodedValue(scoreTerm);
		final PropertyValue propValue1 = 
			(score instanceof Integer) ? new IntegerValue((Integer)score) : new RealNumberValue((Float)score);
		final PropertyValue propValue2 = 
			(assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;
		final PropertyValue propValue3 = 
			(normRangeTuple != null) ? new IntegerInterval(normRangeTuple): null;		

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#questionnaire-Score
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-Assessment-Code
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-Normal-Range
		
		return newPosTuple(map);
	}

}
