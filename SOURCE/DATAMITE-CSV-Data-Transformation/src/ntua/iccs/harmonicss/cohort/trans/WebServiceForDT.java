package ntua.iccs.harmonicss.cohort.trans;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;

/** Harmonization of Data using an External Web Service */
public class WebServiceForDT extends DataTransformation {
	
	private final String webServiceWsdlUrl;
	
	public WebServiceForDT(String webServiceWsdlUrl) {
		super();
		this.webServiceWsdlUrl = webServiceWsdlUrl;
	}
	
	public String getWebServiceWsdlUrl() {
		return webServiceWsdlUrl;
	}

	/** The Web Service is internally being used for producing the values of the properties based on the given Data */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// TODO: INVOKE WEB SERVICE
		// SEND A) INPUT DATA, B) ARGUMENTS and C) RELEVANT MAPPING RULES
		// RETRIEVE OUTPUT DATA AND PREPARE RESPONSE
		
		return null;
	}

}
