package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping 2 fields: A) A Field about whether the outcome of a Lab Test was normal or not 
 * and B) Another Field that indicates the date the test took place.
 * <p>
 * The corresponding Reference Model Lab Test term should be specified along with the corresponding Assessment term. 
 * <p>
 * The values of the 1st Field should be independently ** aligned with ** the Confirmation Terms (i.e., Yes/No) 
 * specified in the Reference Model. 
 * <p>
 * Also we would like to know the Normal Range of values (if applicable).
 * <p>
 * Additionally the format of the Date (2nd field) should be given.
 */
public class LabTestOutcomeAssessmentConfirmationPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Assessment, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, UNL_and_DNL_Unit_of_Measurement, Date_Format }
	
	/** Provides the appropriate Term for the specific Lab Test, the Outcome Assessment and the Date it took place, based on the Values of the given Fields, the parameters provided (i.e., corresponding Reference Model Lab Test and Assessment terms, Date Format) and Mapping Rules specified. Also, provided that the UNL and DNL specified (including Unit of Measurement), the Normal Range of Values will be also recorded. In case the value of the 1st field is empty or not aligned with the Confirmation terms, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutcomeAssessConfirmValue = data.get(0); // Lab Test Outcome Assessment: YES/NO
		final String testDateStr = data.get(1); // Lab Test Date

		// Additional Data / Arguments (if any)
		final String labTestTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String assessmentTermUri = getArgValue(ServiceArg.Assessment);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String unit = getArgValue(ServiceArg.UNL_and_DNL_Unit_of_Measurement);
		final String testDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass labTestTerm = getReferenceModelTermWithURI(labTestTermUri);
		
		// Ensure that the value of the given Test can be an Assessment
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(labTestTerm);
		if (!expOutSet.contains(TestOutcome.ASSESSMENT))
			throw new RuntimeException("The expected outcome for the given test (" + labTestTerm.getLocalName() +") is not an Assessment ! Possible Test Outcome(s): " + expOutSet);
		
		final OntClass assessmentTerm = getReferenceModelTermWithURI(assessmentTermUri);
		final Tuple2<TermExpr, Relation> confirmValueTermExprT = getReferenceModelTermForCohortValue(testOutcomeAssessConfirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, no Data will be recorded
		if (confirmValueTermExprT == null) return null;		
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValueTermExprT._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(testDateStr, testDateFormat);
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, unit, labTestTerm);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = 
			new SimpleCodedValue(labTestTerm.getLocalName(), labTestTerm.getLabel(null));
		final PropertyValue propValue1 = 
			(dateTuple != null) ? new DateValue(dateTuple._1(), dateTuple._2(), dateTuple._3()): null;
		final PropertyValue propValue2 =
			( confirmValueTermExprT._1().isConfirmationTermYES() )
				? new SimpleCodedValue(assessmentTerm.getLocalName(), assessmentTerm.getLabel(null))
				: getOppositeAssessmentTerm(assessmentTerm) ;
		final PropertyValue propValue3 = 
			(normRangeTuple != null) ? new AmountInterval(normRangeTuple._1(), normRangeTuple._2(), 
				new SimpleCodedValue(normRangeTuple._3()._1(), normRangeTuple._3()._2())) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}
