package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Field about the Amount of cigar packs an Active Smoker consumes per year with the corresponding entity from the Reference Model. */
public class SmokingCigarPacksAmount extends DataTransformation {

	private static final Logger log = LogFactory.getLoggerForClass(SmokingCigarPacksAmount.class);
	
	private enum ServiceArg {  }
	
	/** Provides the appropriate Term (i.e., Active Smoker) along with the Amount of cigar-packs consumed per year based on Value of the given Field.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Tobacco-Consumption that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String cigarPacksNumStr = data.get(0); // Packs of Cigarettes Field (Integer)

		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final Integer cigarPacksNum = getInteger(cigarPacksNumStr);
		
		// IF Field is empty or not an Integer, no data will be recorded
		if (cigarPacksNum == null) return null;
		
		if (cigarPacksNum == 0) {
			log.warn("Cigar-Packs is ZERO - No Data Recorded");
		} else if (cigarPacksNum < 0) {
			log.error("Cigar-Packs is a Negative Number !");
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = 
			new SimpleCodedValue("SMOK-STAT-01", "Active Smoker");
		final PropertyValue propValue1 = 
			new AmountValue(cigarPacksNum, new SimpleCodedValue("UNITEXP-18", "Packets of cigarettes / Year"));

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#tobacco-Consumption-Status-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#tobacco-Amount
		
		return newPosTuple(map);
	}

}
