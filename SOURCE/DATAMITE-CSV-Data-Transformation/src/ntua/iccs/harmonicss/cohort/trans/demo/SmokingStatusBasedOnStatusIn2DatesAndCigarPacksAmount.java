package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.VOCNS;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping three Cohort Fields, a) A Field that indicates whether a person was smoking or not is a specific Date, b) Another Field that indicates whether a person was smoking or not in another Date (after the previous one), and c) Another Field with the Amount of cigarettes consumed in a specific period of time, on average. The values of both fields should be independently ** aligned with ** the Confirmation YES/NO terms specified in the Reference Model. Also, the units-expression (e.g., Cigar-Packs per Year) about the amount of cigars consumed in a specific period of time should be also provided. */
public class SmokingStatusBasedOnStatusIn2DatesAndCigarPacksAmount extends DataTransformation {

	private enum ServiceArg { Value_Format, Units_Expression }
	
	/** Provides the appropriate Term (i.e., Smoking Status) based on values of the given Fields (smoking or not in two different time points, and amount of cigars or cigar-packs on average in a specific period of time) and the Mapping Rules already specified.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Tobacco-Consumption that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String smokConfirmValueA = data.get(0); // Smoking (yes/no) in Date A
		final String smokConfirmValueB = data.get(1); // Smoking (yes/no) in Date B (after Date A)
		final String smokValueStr = data.get(2); // Amount of Cigarettes & Frequency of use (number)

		// Additional Data / Arguments (if any)
		final String valueFormatIndex = getArgValue(ServiceArg.Value_Format);
		final String unitsExpression = getArgValue(ServiceArg.Units_Expression);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final Tuple2<TermExpr, Relation> termExprTupleA = getReferenceModelTermForCohortValue(smokConfirmValueA, 0);
		final Tuple2<TermExpr, Relation> termExprTupleB = getReferenceModelTermForCohortValue(smokConfirmValueB, 1);
		
		// Ensure that the Corresponding Reference Model terms are Confirmation Terms
		if (termExprTupleA != null && !termExprTupleA._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + termExprTupleA._1().asString() + "\" is NOT a Confirmation Term !");
		if (termExprTupleB != null && !termExprTupleB._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + termExprTupleB._1().asString() + "\" is NOT a Confirmation Term !");
		
		// IF both smoking status Fields are empty, no data will be recorded
		if (termExprTupleA == null && termExprTupleB == null) return null; 
		
		final Float smokAmount = getValueInTargetUnit(smokValueStr, valueFormatIndex, unitsExpression, "Cigar-Packs/Year");
		
		final Term smokStatTerm;
		if (termExprTupleA != null && termExprTupleB == null) {
			if (termExprTupleA._1().isConfirmationTermYES()) {
				// Active Smoker
				smokStatTerm = new Term(VOCNS + "SMOK-STAT-01", "Active");
			} else {
				// Never Smoker
				smokStatTerm = new Term(VOCNS + "SMOK-STAT-03", "Never Smoker");
			}
		} else if (termExprTupleA == null && termExprTupleB != null) {
			if (termExprTupleB._1().isConfirmationTermYES()) {
				// Active Smoker
				smokStatTerm = new Term(VOCNS + "SMOK-STAT-01", "Active");
			} else {
				// Never Smoker
				smokStatTerm = new Term(VOCNS + "SMOK-STAT-03", "Never Smoker");
			}
		} else {
			if (termExprTupleA._1().isConfirmationTermYES()) {
				if (termExprTupleB._1().isConfirmationTermYES()) {
					// Active Smoker
					smokStatTerm = new Term(VOCNS + "SMOK-STAT-01", "Active");
				} else {
					// Ex-Smoker
					smokStatTerm = new Term(VOCNS + "SMOK-STAT-02", "Ex-Smoker");
				}
			} else {
				if (termExprTupleB._1().isConfirmationTermYES()) {
					// Active Smoker
					smokStatTerm = new Term(VOCNS + "SMOK-STAT-01", "Active");
				} else {
					// Never Smoker
					smokStatTerm = new Term(VOCNS + "SMOK-STAT-03", "Never Smoker");
				}
			}
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(smokStatTerm); 
		final PropertyValue propValue1 = 
			(smokAmount != null && smokAmount > 0) ? new AmountValue(smokAmount, new SimpleCodedValue("UNITEXP-18", "Packets of cigarettes / Year")) : null;
		
		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#tobacco-Consumption-Status-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#tobacco-Amount
		
		return newPosTuple(map);
	}

}
