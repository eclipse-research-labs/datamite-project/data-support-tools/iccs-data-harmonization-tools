package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping two Cohort Fields: a) A Field with one or more symptoms/signs observed, separated by comma, 
 * and b) Another Field about the Date ALL the Symptoms/Signs observed (for the very first time). 
 * <p>
 * The different symptoms (cohort terms) existing in the 1st Field should be independently ** aligned with ** 
 * the Symptoms/Signs terms specified in the Reference Model. 
 * <p>
 * Additionally the Format of the Date (2nd Field) should be given.
 */
public class SymptomOnsetListPlusDate extends DataTransformation {

	private enum ServiceArg { Date_Format }
	
	/** Provides a List of Entities with the corresponding Reference Model term (i.e., Symptom/Sign) along with the date the symptom/sign observed based on the value of the given Fields, additional data given (i.e., date format) and the Mapping Rules specified (i.e., mapping of cohort Symptom Terms/Values with the corresponding Reference Model terms about Symptoms). If the value of this Field is empty or none of the comma separated terms are aligned with the Reference Model Symptom/Sign terms, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Symptom-or-Sign that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String termsStr = data.get(0); // Symptom(s)/Sign(s)
		final String symptDate = data.get(1); // Date of Onset

		// Additional Data / Arguments (if any)
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final List<String> strList = getTermsStrList(termsStr);
		
		// IF field is empty
		if (strList == null) return null;
		
		final List<Tuple2<TermExpr, Relation>> termExprTupleList = new ArrayList<>();
		for (String str : strList) {
			final Tuple2<TermExpr, Relation> termExprTuple = getReferenceModelTermForCohortValue(str, 0);
			if (termExprTuple == null) continue;
			// Keep only Symptoms/Signs
			if (!termExprTuple._1().isSymptomSign()) continue;
			termExprTupleList.add(termExprTuple);
		}
		
		// IF none term aligned with symptoms terms
		if (termExprTupleList.isEmpty()) return null;
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(symptDate, dateFormat);
		
		final List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
		for (Tuple2<TermExpr, Relation> termExprTuple : termExprTupleList) {
			final Map<Integer, PropertyValue> map = new HashMap<>();
			final PropertyValue propValue0 = 
				(termExprTuple._1().isTerm()) ? new SimpleCodedValue(termExprTuple) : new ComplexCodedValue(termExprTuple);
			final PropertyValue propValue1 = 
				(dateTuple != null) ? new DateValue(dateTuple): null;
			map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#symptom-sign-CV
			map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#symptom-sign-Date
			
			mapList.add(map);
		}
		
		return Tuple2.newTuple2(Boolean.TRUE, mapList);	
	}

}
