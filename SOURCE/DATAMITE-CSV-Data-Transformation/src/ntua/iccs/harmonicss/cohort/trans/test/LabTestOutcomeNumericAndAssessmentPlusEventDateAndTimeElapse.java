package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 4 Cohort Fields A) A Field with the Lab Test Outcome measured (i.e., the value of this Field is a Real Number), B) Another Field that indicates whether this outcome was Normal or not and C) Two other Fields that indicate the Date when the Lab Test took place. The corresponding Reference Model term should be given as well as the format of the real number existing in the 1st Field and the Unit of Measurement. Also, the values of the 2nd Field should be independently ** aligned with ** Assessment term (i.e., Normal/Abnormal values). Additionally the format of the Date (3rd field) along with the Unit of Time Elapse (4th field) should be given. Moreover, the Normal Range of Values for this Lab Test can be also specified (optional). However, they should be also expressed in the same Unit of Measurement with the one specified for the 1st Field. */
public class LabTestOutcomeNumericAndAssessmentPlusEventDateAndTimeElapse extends DataTransformation {

	private enum ServiceArg { Reference_Model_Blood_Test, Outcome_Format, Outcome_Unit_of_Measurement, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, Event_Date_Format, Elapse_Time_Unit }
	
	/** Provides the values of the parameters existing in the right side, provided that at least one of the two first fields is not Empty. The outcome of the Lab Test depends on the value existing in the 1st Field as well as the Unit of Measurement specified. The 2nd Field along with the mapping rules already specified are being used for detecting whether the outcome was normal or not. The Normal Range of values is also recorded, provided that the corresponding attributes (optional) are given. The values existing in the 3rd and 4th Fields along with the Date format and Elapse Time Unit are used for specifying the date the test took place.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutValue = data.get(0); // Lab Test Outcome: Numeric Value
		final String testOutAssess = data.get(1); // Lab Test Outcome: Assessment
		final String eventDateStr = data.get(2); // Event Date
		final String elapseTimeStr = data.get(3); // Elapse Time

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Blood_Test);
		final String testValueFormatIndex = getArgValue(ServiceArg.Outcome_Format);
		final String testValueUnit = getArgValue(ServiceArg.Outcome_Unit_of_Measurement);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String eventDateFormat = getArgValue(ServiceArg.Event_Date_Format);
		final String elapseTimeUnit = getArgValue(ServiceArg.Elapse_Time_Unit);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test can be an Amount
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.AMOUNT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Amount ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple2<Float, Tuple2<String, String>> valueTuple = getAmountValueTuple(testOutValue, testValueFormatIndex, testValueUnit, testTerm);
		final Tuple2<TermExpr, Relation> testOutAssessTermExprT = getReferenceModelTermForCohortValue(testOutAssess, 1);
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, testValueUnit, testTerm);
		
		// IF neither value (e.g., being empty or not a Number) nor assessment provided, no data will be recorded
		if (valueTuple == null && testOutAssessTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is an Assessment Term
		if (testOutAssessTermExprT != null && !testOutAssessTermExprT._1().isSimpleAssessmentTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testOutAssessTermExprT._1().asString() + "\" is NOT an Assessment Term !");
		
		final Tuple3<Integer, Integer, Integer> eventDateTuple = getDateTuple(eventDateStr, eventDateFormat);
		final Integer elapseTimeInt = getInteger(elapseTimeStr);
		final Tuple2<Integer, String> timeElapseTuple = (elapseTimeInt != null) ? Tuple2.newTuple2(elapseTimeInt, elapseTimeUnit) : null;
		// Provides the Date the test took place, provided that both the Event Date and the Time Elapse specified, with the necessary level of Details
		final Tuple3<Integer, Integer, Integer> testDateTuple = getDateTupleAfterTime(eventDateTuple, timeElapseTuple);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = (testDateTuple != null) ? new DateValue(testDateTuple): null;
		final PropertyValue propValue2 = (valueTuple != null) ? new AmountValue(valueTuple): null;
		final PropertyValue propValue3 = (testOutAssessTermExprT != null) ? new SimpleCodedValue(testOutAssessTermExprT) : null;
		final PropertyValue propValue4 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(4, propValue4); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}
