package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping three Fields, A) A Field with one or more Drugs administered, separated by comma and B) two other Fields that indicates the period of time (Start - End dates) when the person received ALL the Drugs administered. The different drugs (1st Field, comma-separated cohort terms) should be independently ** aligned with ** the Drug terms specified in the Reference Model. Additionally the Format of the Dates (2nd and 3rd Field) should be given. */
public class DrugAdminListPlusStartEndDate extends DataTransformation {

	private enum ServiceArg { Date_Format }
	
	/** Provides a List of Entities with the corresponding Reference Model term (i.e., Drug) along with the period of time the drug administered based on the value of the given Fields (i.e., cohort drug terms and start-end date) and the Mapping Rules specified (i.e., mapping of cohort Drug Terms/Values with the corresponding Reference Model terms about Drugs). If the value of the 1st Field is empty or none of the comma separated terms are aligned with the Reference Model Drug terms, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Medication that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String termsStr = data.get(0); // Drug(s): Comma-separated Terms
		final String drugStartDate = data.get(1); // Drug(s) Start Date
		final String drugEndDate = data.get(2); // Drug(s) End Date

		// Additional Data / Arguments (if any)
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final List<String> strList = getTermsStrList(termsStr);
		
		// IF field is empty
		if (strList == null) return null;
		
		final List<Tuple2<TermExpr, Relation>> termExprTupleList = new ArrayList<>();
		for (String str : strList) {
			final Tuple2<TermExpr, Relation> termExprTuple = getReferenceModelTermForCohortValue(str, 0);
			if (termExprTuple == null) continue;
			// Keep only Drugs
			if (!termExprTuple._1().isDrug()) continue;
			termExprTupleList.add(termExprTuple);
		}
		
		// IF none term aligned with drug terms
		if (termExprTupleList.isEmpty()) return null;
		
		final Tuple3<Integer, Integer, Integer> startDateTuple = getDateTuple(drugStartDate, dateFormat);
		final Tuple3<Integer, Integer, Integer> endDateTuple = getDateTuple(drugEndDate, dateFormat);
		
		// Ensure that the Date Interval (Start Date - End Date) is valid
		final boolean isDateIntervalValid = isDateIntervalValid(startDateTuple, endDateTuple);
		if (!isDateIntervalValid) {
			log.warn("Invalid Date Interval: [" + startDateTuple + " , " + endDateTuple + "] Period of Time is being ignored !");
		}
		
		List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
		for (Tuple2<TermExpr, Relation> termExprTuple : termExprTupleList) {
			final Map<Integer, PropertyValue> map = new HashMap<>();
			final PropertyValue propValue0 = 
				(termExprTuple._1().isTerm()) ? new SimpleCodedValue(termExprTuple) : new ComplexCodedValue(termExprTuple);
			final PropertyValue propValue1 = 
				((startDateTuple != null || endDateTuple != null) && isDateIntervalValid) ? 
					new DateIntervalTypeA(startDateTuple, endDateTuple): null;
			map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-CV
			map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Date-Interval
			mapList.add(map);
		}
		
		return Tuple2.newTuple2(Boolean.TRUE, mapList);
	}

}

