package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.BooleanValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping three Cohort Fields: a) A Field about a Lab Test with a Boolean Value (i.e., YES/NO) and 
 * b) Two other Fields about the Period of Time (i.e., Start and End Date) the Lab Test took place.
 * <p>
 * The corresponding Reference Model term should be given. 
 * <p>
 * Also, the values of the 1st field should be independently ** aligned with ** Confirmation terms 
 * (i.e., YES/NO values) specified in the Reference Model.
 * <p>
 * Additionally the format of the Dates (2nd and 3rd Field) should be given.
 */
public class LabTestOutcomeBooleanPlusPeriod extends DataTransformation {

	private static final Logger log = LogFactory.getLoggerForClass(LabTestOutcomeBooleanPlusPeriod.class);
	
	private enum ServiceArg { Reference_Model_Lab_Test, Dates_Format }
	
	/** Provides the appropriate Terms for the specific Lab Test along with its Outcome and the Period of Time it took place, based on the Value of the given Fields, the parameters provided (i.e., corresponding Reference Model term and date format) and the Mapping Rules already specified. In case the 1st Field is empty or not aligned with the Confirmation terms, no data will be recorded. Also, in case the 2nd & 3rd Fields are empty, no information about the Test Date Period of Time will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testValue = data.get(0); // Lab Test Outcome: Boolean Value
		final String testPeriodStartDate = data.get(1); // Lab Test Date Period Start
		final String testPeriodEndDate = data.get(2); // Lab Test Date Period End

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String dateFormat = getArgValue(ServiceArg.Dates_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test is a Boolean 
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.BOOLEAN))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not a Boolean ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple2<TermExpr, Relation> testConfirmValueTermExprT = getReferenceModelTermForCohortValue(testValue, 0);

		// IF the value is empty or not aligned with Reference Model confirmation terms, no data will be recorded
		if (testConfirmValueTermExprT == null) return null;

		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!testConfirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testConfirmValueTermExprT._1().asString() + "\" is NOT a Confirmation Term !");
			
		final Tuple3<Integer, Integer, Integer> startDateTuple = getDateTuple(testPeriodStartDate, dateFormat);
		final Tuple3<Integer, Integer, Integer> endDateTuple = getDateTuple(testPeriodEndDate, dateFormat);
		
		// Ensure that the Date Interval (Start Date - End Date) is valid
		final boolean isDateIntervalValid = isDateIntervalValid(startDateTuple, endDateTuple);
		if (!isDateIntervalValid) {
			log.warn("Invalid Date Interval: [" + startDateTuple + " , " + endDateTuple + "] Period of Time is being ignored !");
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = new BooleanValue(testConfirmValueTermExprT);
		final PropertyValue propValue2 = 
			((startDateTuple != null || endDateTuple != null) && isDateIntervalValid) ? 
				new DateIntervalTypeA(startDateTuple, endDateTuple): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		
		return newPosTuple(map);
	}

}
