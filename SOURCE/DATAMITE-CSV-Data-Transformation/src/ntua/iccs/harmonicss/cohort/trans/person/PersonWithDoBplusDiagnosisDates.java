package ntua.iccs.harmonicss.cohort.trans.person;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.simple.StringValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping ... */
public class PersonWithDoBplusDiagnosisDates extends DataTransformation {

	private enum ServiceArg { Date_of_Birth_Format, Diagnosis_Dates_Format }
	
	/** Provides the values ...
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Person that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String uid = data.get(0); // Patient Unique ID
		final String dob = data.get(1); // Date of Birth
		final String diagnDate = data.get(2); // Date of pSS Diagnosis

		// Additional Data / Arguments (if any)
		final String dobFormat = getArgValue(ServiceArg.Date_of_Birth_Format);
		final String keyDatesFormat = getArgValue(ServiceArg.Diagnosis_Dates_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		if (isEmpty(uid)) throw new RuntimeException("Patient ID cannot be empty !");
		
		final Tuple3<Integer, Integer, Integer> dobDateTuple = getDateTuple(dob, dobFormat);
		final Tuple3<Integer, Integer, Integer> diagnDateTuple = getDateTuple(diagnDate, keyDatesFormat);
	
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 =
			new StringValue(uid);
		final PropertyValue propValue1 =
			(dobDateTuple != null) ? new DateValue(dobDateTuple._1(), dobDateTuple._2(), dobDateTuple._3()): null;
		final PropertyValue propValue2 =
			(diagnDateTuple != null) ? new DateValue(diagnDateTuple._1(), diagnDateTuple._2(), diagnDateTuple._3()): null;
		
		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Unique-ID
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Date-Birth
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-SS-Diagnosis
		
		// The List should often contain only one Map (so that only one instance will be created)
		return newPosTuple(map);
	}

}