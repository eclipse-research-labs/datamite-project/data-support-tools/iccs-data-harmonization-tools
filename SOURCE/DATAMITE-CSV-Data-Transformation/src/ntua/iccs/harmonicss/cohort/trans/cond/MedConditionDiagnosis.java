package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/**
 * Mapping a Cohort Field that indicates whether the person has been diagnosed with a particular condition or not.
 * <p>
 * The corresponding Reference Model term (Medical Condition) should be specified. 
 * <p>
 * Also, the values of this Field should be independently ** aligned with ** the Confirmation terms 
 * (i.e., YES/NO values) specified in the Reference Model.
 */
public class MedConditionDiagnosis extends DataTransformation {

	private enum ServiceArg { Reference_Model_Condition }
	
	/** 
	 * Depending of the value of the given field (the cohort values should be aligned with yes/no terms) 
	 * it introduces an OWL entity with the specific Reference Model condition provided.
	 * 
	 * @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Diagnosis that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String condConfirmValue = data.get(0); // Specific Medical Condition Field

		// Additional Data / Arguments (if any)
		final String condTermUri = getArgValue(ServiceArg.Reference_Model_Condition);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass condTerm = getReferenceModelTermWithURI(condTermUri);
		final Tuple2<TermExpr, Relation> confirmValueT = getReferenceModelTermForCohortValue(condConfirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, it will be ignored.
		if (confirmValueT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValueT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValueT._1().asString() + "\" is NOT a Confirmation Term !");
		
		// Properties Value
		final PropertyValue diagnosisCV = new SimpleCodedValue(condTerm);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		map.put(0, diagnosisCV); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-CV
		
		// Depending on the value of the Condition Field, return a positive/negative "statement"
		if (confirmValueT._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
	}

}

