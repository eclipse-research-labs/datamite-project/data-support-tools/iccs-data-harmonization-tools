package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.VOCNS;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Cohort Fields, a) A Field that indicates whether a person was smoking or not is a specific Date and, b) Another Field that indicates whether a person was smoking or not in another Date (after the previous one). The values of both fields should be independently ** aligned with ** the Confirmation YES/NO terms specified in the Reference Model. */
public class SmokingStatusBasedOnStatusIn2Dates extends DataTransformation {

	private enum ServiceArg {  }
	
	/** Provides the appropriate Term (i.e., Smoking Status) based on values of the given Fields (smoking or not in two different time points) and the Mapping Rules already specified.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Tobacco-Consumption that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String smokConfirmValueA = data.get(0); // Smoking (yes/no) in Date A
		final String smokConfirmValueB = data.get(1); // Smoking (yes/no) in Date B (after Date A)

		// Additional Data / Arguments (if any)

		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final Tuple2<TermExpr, Relation> termExprTupleA = getReferenceModelTermForCohortValue(smokConfirmValueA, 0);
		final Tuple2<TermExpr, Relation> termExprTupleB = getReferenceModelTermForCohortValue(smokConfirmValueB, 1);

		// Ensure that the Corresponding Reference Model terms are Confirmation Terms
		if (termExprTupleA != null && !termExprTupleA._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + termExprTupleA._1().asString() + "\" is NOT a Confirmation Term !");
		if (termExprTupleB != null && !termExprTupleB._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + termExprTupleB._1().asString() + "\" is NOT a Confirmation Term !");
		
		if (termExprTupleA == null && termExprTupleB == null) return null; 
		
		final Term smokStatTerm;
		if (termExprTupleA != null && termExprTupleB == null) {
			if (termExprTupleA._1().isConfirmationTermYES()) {
				// Active Smoker
				smokStatTerm = new Term(VOCNS + "SMOK-STAT-01", "Active");
			} else {
				// Never Smoker
				smokStatTerm = new Term(VOCNS + "SMOK-STAT-03", "Never Smoker");
			}
		} else if (termExprTupleA == null && termExprTupleB != null) {
			if (termExprTupleB._1().isConfirmationTermYES()) {
				// Active Smoker
				smokStatTerm = new Term(VOCNS + "SMOK-STAT-01", "Active");
			} else {
				// Never Smoker
				smokStatTerm = new Term(VOCNS + "SMOK-STAT-03", "Never Smoker");
			}
		} else {
			if (termExprTupleA._1().isConfirmationTermYES()) {
				if (termExprTupleB._1().isConfirmationTermYES()) {
					// Active Smoker
					smokStatTerm = new Term(VOCNS + "SMOK-STAT-01", "Active");
				} else {
					// Ex-Smoker
					smokStatTerm = new Term(VOCNS + "SMOK-STAT-02", "Ex-Smoker");
				}
			} else {
				if (termExprTupleB._1().isConfirmationTermYES()) {
					// Active Smoker
					smokStatTerm = new Term(VOCNS + "SMOK-STAT-01", "Active");
				} else {
					// Never Smoker
					smokStatTerm = new Term(VOCNS + "SMOK-STAT-03", "Never Smoker");
				}
			}
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(smokStatTerm); 

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#tobacco-Consumption-Status-CV
		
		return newPosTuple(map);
	}

}
