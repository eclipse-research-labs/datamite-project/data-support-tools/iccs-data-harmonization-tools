package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import net.sf.json.JSONObject;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.AmountsValueTransformer;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 5 cohort Fields, A) A Field that indicates whether the person has received a specific Drug or not and B) four fields about the dosage amount (value and unit of measurement) and frequency of use (value and unit of time). The corresponding Reference Model term (Drug) should be specified. Also, the values of this Field should be independently ** aligned with ** the Confirmation terms (i.e., YES/NO values) specified in the Reference Model. The Mapping of Units of Measurement and Units of Time with the corresponding Reference Model Units terms should be also provided in the form of a JSON. */
public class DrugDosageAdmin extends DataTransformation {

	private enum ServiceArg { Reference_Model_DrugSubstance, Unit_of_Measurement_JO, Unit_of_Time_JO }
	
	/** Provides the corresponding Reference Model term (i.e., Drug) along with the Dosage administered in the given Period of Time (i.e., start - end date) based on the given Data and Mapping Rules specified, provided that the value existing in the 1st field is not empty. More precisely, IF the value is yes (i.e., aligned with the YES confirmation term) the service indicates that the person has received that drug, otherwise not.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Medication that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String drugConfirmValue = data.get(0); // Drug Field: YES/NO
		final String dosageValueStr = data.get(1); // Dosage Value
		final String dosageUnitCode = data.get(2); // Dosage Unit of Measurement Encoded
		final String freqValueStr = data.get(3); // Frequency Value
		final String freqUnitCode = data.get(4); // Frequency Unit of Time Encoded

		// Additional Data / Arguments (if any)
		final String drugTermUri = getArgValue(ServiceArg.Reference_Model_DrugSubstance);
		final String unitMeasurementJOstr = getArgValue(ServiceArg.Unit_of_Measurement_JO);
		final String unitTimeJOstr = getArgValue(ServiceArg.Unit_of_Time_JO);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));

		// Data Processing
		final OntClass drugTerm = getReferenceModelTermWithURI(drugTermUri);
		
		final JSONObject unitMeasurementJO = JSONObject.fromObject(unitMeasurementJOstr);
		final JSONObject unitTimeJO = JSONObject.fromObject(unitTimeJOstr);
		
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(drugConfirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, it will be ignored.
		if (confirmValue == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");
		
		// Dosage
		final Float dosageValue = getFloat(dosageValueStr);
		final String dosageUnit = unitMeasurementJO.has(dosageUnitCode) ? unitMeasurementJO.getString(dosageUnitCode) : null;
		final Float freqValue = getFloat(freqValueStr);
		final String freqUnit = unitTimeJO.has(freqUnitCode) ? unitTimeJO.getString(freqUnitCode) : null;
		
		final Tuple2<OntClass, String> dosageUnitTuple = getDrugDosageUnit(drugTerm);
		final Float newDosage;
		if (dosageUnitTuple == null) {
			newDosage = null;
			log.warn("No Unit specified for Drug: " + drugTerm.getLocalName() + " .. Dosage NOT recorded !");
		} else {
			if (dosageValue != null && dosageUnit != null && freqValue != null && freqUnit != null) {
				final Float value = dosageValue * freqValue;
				final String valueUnitExpr = dosageUnit + "/" + freqUnit;
				newDosage = AmountsValueTransformer.expressValueInTargetUnit(value, valueUnitExpr, dosageUnitTuple._2());
			} else {
				newDosage = null;
			}
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(drugTerm);
		final PropertyValue propValue1 = 
			(newDosage != null) ? new AmountValue(newDosage, 
				new SimpleCodedValue(dosageUnitTuple.get_1().getLocalName(), dosageUnitTuple.get_2())) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Dosage
		
		// Depending on the value of the Drug Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}

	}

}
