package ntua.iccs.harmonicss.cohort.trans.quest;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.data.owl.interv.IntegerInterval;
import ntua.iccs.harmonicss.cohort.data.owl.simple.IntegerValue;
import ntua.iccs.harmonicss.cohort.data.owl.simple.RealNumberValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping three Fields, A) A Field with the value of a Questionnaire Score (integer or real number) and B) Two other Fields about the Period of Time (Start & End dates) when the score was recorded. The corresponding Reference Model term should be specified. Also, the format of the Date (2nd and 3rd fields) should be given. Moreover, the Normal Range of Values for this Questionnaire can be also specified (optional). */
public class QuestScoreValuePlusPeriodBetweenDates extends DataTransformation {

	private enum ServiceArg { Reference_Model_Questionnaire_Score, Date_Format, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL }
	
	/** Provides an Entity with the values of the parameters specified in the right side taking into account the patient data in the given fields (i.e., score value and start, end dates) as well as additional parameters provided (i.e., score, data format and normal ranges of values). The value of the 1st Field should be an integer or a real number. The Period of Time will be recorded provided that the value of at least one of the two date field is not empty and the data follow the given format. For specifying whether the score was normal or not (Assessment), at least one of the two optional properties about UNL and/or DNL should be given.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Questionnaire-Score that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String questScore = data.get(0); // Questionnaire Score
		final String startDateStr = data.get(1); // Period Start Date
		final String endDateStr = data.get(2); // Period End Date

		// Additional Data / Arguments (if any)
		final String scoreTermUri = getArgValue(ServiceArg.Reference_Model_Questionnaire_Score);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		final String downNormLimit = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimit = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass scoreTerm = getReferenceModelTermWithURI(scoreTermUri);
		
		final Object score = getNumber(questScore);
		
		// IF score is empty or not an Integer/Real-Number, no data will be recorded
		if (score == null) return null;
		
		final Tuple3<Integer, Integer, Integer> startDateTuple = getDateTuple(startDateStr, dateFormat);
		final Tuple3<Integer, Integer, Integer> endDateTuple = getDateTuple(endDateStr, dateFormat);
		
		// Ensure that the Date Interval (Start Date - End Date) is valid
		final boolean isDateIntervalValid = isDateIntervalValid(startDateTuple, endDateTuple);
		if (!isDateIntervalValid) {
			log.warn("Invalid Date Interval: [" + startDateTuple + " , " + endDateTuple + "] Period of Time is being ignored !");
		}
		
		final Tuple2<Integer, Integer> normRangeTuple = getIntegerNormalRangeTuple(downNormLimit, upNormLimit);
		final Term assessTerm;
		if (score instanceof Integer) {
			assessTerm = (normRangeTuple != null) ? getAssessmentForValue((Integer)score, normRangeTuple._1(), normRangeTuple._2()) :  null;
		} else {
			assessTerm = (normRangeTuple != null) ? getAssessmentForValue((Float)score, new Float(normRangeTuple._1()), new Float (normRangeTuple._2())) :  null;
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(scoreTerm);
		final PropertyValue propValue1 = 
			((startDateTuple != null || endDateTuple != null) && isDateIntervalValid) ? 
				new DateIntervalTypeA(startDateTuple, endDateTuple): null;
		final PropertyValue propValue2 = 
			(score instanceof Integer) ? new IntegerValue((Integer)score) : new RealNumberValue((Float)score);
		final PropertyValue propValue3 = 
			(assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;
		final PropertyValue propValue4 = 
			(normRangeTuple != null) ? new IntegerInterval(normRangeTuple): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#completion-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#questionnaire-Score
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-Assessment-Code
		map.put(4, propValue4); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-Normal-Range
		
		return newPosTuple(map);
	}

}
