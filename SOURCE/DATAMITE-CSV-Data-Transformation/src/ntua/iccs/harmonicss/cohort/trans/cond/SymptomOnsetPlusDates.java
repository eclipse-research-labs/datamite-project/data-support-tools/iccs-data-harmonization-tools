package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Cohort Fields: a) A Field that indicates whether the person has experienced a specific Symptom/Sign or not, and b) Another Field about the Dates (separated by comma) the Symptom/Sign observed. The corresponding Reference Model term (Symptom/Sign) should be specified. Also, the values of the 1st Field should be independently ** aligned with ** the Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Additionally the Format of the comma separated Dates (2nd Field) should be given. */
public class SymptomOnsetPlusDates extends DataTransformation {

	private enum ServiceArg { Reference_Model_SymptomSign, Date_Format }
	
	/** Provides a List of Entities with the given Symptom/Sing and the date it was observed, based on the values of the given Fields, additional data given (i.e., date format) and the Mapping Rules specified (i.e., mapping of 1st Field values with the corresponding Reference Model terms), provided that the value of the 1st Field is YES. In case the value of this Field is no, the system indicates that symptom/sign was not observed the given dates, if any.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Symptom-or-Sign that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String symptConfirmValue = data.get(0); // Symptom/Sign
		final String symptDateList = data.get(1); // Dates of Onset (separated by comma)

		// Additional Data / Arguments (if any)
		final String symptomTermUri = getArgValue(ServiceArg.Reference_Model_SymptomSign);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass symptTerm = getReferenceModelTermWithURI(symptomTermUri);
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(symptConfirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, it will be ignored.
		if (confirmValue == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");
		
		final List<String> dateStrList = getTermsStrList(symptDateList);
		
		// Parse comma separate Date Strings
		final List<Tuple3<Integer, Integer, Integer>> dateTupleList = new ArrayList<>();
		if (dateStrList != null) {
			for (String dateStr : dateStrList) {
				final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(dateStr, dateFormat);
				if (dateTuple != null) dateTupleList.add(dateTuple);
			}
		}
		
		final List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
		
		if (dateTupleList.isEmpty()) {
			final Map<Integer, PropertyValue> map = new HashMap<>();
			final PropertyValue propValue0 = new SimpleCodedValue(symptTerm);
			final PropertyValue propValue1 = null; 
			map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#symptom-sign-CV
			map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#symptom-sign-Date
			mapList.add(map);
		} else {
			// Create so many entities as the number of dates
			for (Tuple3<Integer, Integer, Integer> dateTuple : dateTupleList) {
				final Map<Integer, PropertyValue> map = new HashMap<>();
				final PropertyValue propValue0 = new SimpleCodedValue(symptTerm);
				final PropertyValue propValue1 = new DateValue(dateTuple);
				map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#symptom-sign-CV
				map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#symptom-sign-Date
				mapList.add(map);
			}
		}
		
		
		// Depending on the value of the Symptom Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return Tuple2.newTuple2(Boolean.TRUE, mapList);
		} else {
			return Tuple2.newTuple2(Boolean.FALSE, mapList);
		}
	}

}
