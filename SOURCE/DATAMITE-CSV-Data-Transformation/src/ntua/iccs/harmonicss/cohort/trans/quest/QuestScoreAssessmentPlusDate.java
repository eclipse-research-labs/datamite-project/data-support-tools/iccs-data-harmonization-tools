package ntua.iccs.harmonicss.cohort.trans.quest;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.IntegerInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Fields, A) A Field that indicates whether the outcome of a specific Lab Test was Normal or not and B) Another Field with the Date the questionnaire filled in. The corresponding Reference Model term should be specified. Also, the values of the 1st Field should be independently ** aligned with ** the Assessment Terms specified in the Reference Model. Additionally, the format of the Date (2nd field) should be given. Moreover, the Normal Range of Values for this Questionnaire can be also specified (optional). */
public class QuestScoreAssessmentPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Questionnaire_Score, Questionnaire_Date_Format, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL }
	
	/** Provides an Entity with the values of the parameters specified in the right side (i.e., Score, Assessment, Date and optionally Normal Range of Values) taking into account the patient data in the given fields (i.e., Score Assessment and Date), additional parameters provided (i.e., Score, Date Format, Normal range of values) and Mapping Rules specified (i.e., Mapping of 1st Field values with Assessment Terms).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Questionnaire-Score that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String scoreAssessStr = data.get(0); // Questionnaire Score: Normal/Abnormal
		final String questDate = data.get(1); // Questionnaire Date

		// Additional Data / Arguments (if any)
		final String scoreTermUri = getArgValue(ServiceArg.Reference_Model_Questionnaire_Score);
		final String questDateFormat = getArgValue(ServiceArg.Questionnaire_Date_Format);
		final String downNormLimit = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimit = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));

		// Data Processing
		final OntClass scoreTerm = getReferenceModelTermWithURI(scoreTermUri);
		
		final Tuple2<TermExpr, Relation> scoreAssessTermExprT = getReferenceModelTermForCohortValue(scoreAssessStr, 0);
		
		// IF the value is empty or not-aligned with Assessment Terms, no Data will be recorded
		if (scoreAssessTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is an Assessment Term
		if (!scoreAssessTermExprT._1().isSimpleAssessmentTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + scoreAssessTermExprT._1().asString() + "\" is NOT an Assessment Term !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(questDate, questDateFormat);
		
		final Tuple2<Integer, Integer> normRangeTuple = getIntegerNormalRangeTuple(downNormLimit, upNormLimit);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(scoreTerm);
		final PropertyValue propValue1 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue2 = new SimpleCodedValue(scoreAssessTermExprT);
		final PropertyValue propValue3 = (normRangeTuple != null) ? new IntegerInterval(normRangeTuple): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#completion-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-Assessment-Code
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-Normal-Range
		
		return newPosTuple(map);
	}

}
