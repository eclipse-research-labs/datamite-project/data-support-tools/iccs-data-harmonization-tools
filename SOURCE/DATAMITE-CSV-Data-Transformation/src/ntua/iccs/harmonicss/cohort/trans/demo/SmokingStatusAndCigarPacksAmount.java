package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Cohort Fields, a) A Field with the smoking status and B) Another Field with the Amount of cigarettes consumed in a specific period of time, on average. The values of the 1st Field should be independently ** aligned with ** the Smoking Status terms (e.g., Active Smoker) specified in the Reference Model. The units-expression (e.g., Cigar-Packs per Year) about the amount of cigars consumed in a specific period of time should be also provided. */
public class SmokingStatusAndCigarPacksAmount extends DataTransformation {

	private enum ServiceArg { Value_Format, Units_Expression }
	
	/** Provides the appropriate Term (i.e., Smoking Status) along with the Amount of cigar-packs consumed per year based on Values of the given Fields, additional data provided (i.e., Units-Expression) and Mapping Rules specified.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Tobacco-Consumption that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String smokStatValue = data.get(0); // Smoking Status
		final String smokValueStr = data.get(1); // Amount of Cigarettes & Frequency of use (number)

		// Additional Data / Arguments (if any)]
		final String valueFormatIndex = getArgValue(ServiceArg.Value_Format);
		final String unitsExpression = getArgValue(ServiceArg.Units_Expression);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final Tuple2<TermExpr, Relation> termExprTuple = getReferenceModelTermForCohortValue(smokStatValue, 0);
		
		// IF the value is empty or not-aligned with Reference Model Terms, it will be ignored.
		if (termExprTuple == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Smoking Status Terms
		if (!termExprTuple._1().isSimpleSmokingStatusTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + termExprTuple._1().asString() + "\" is NOT a Smoking Status Term !");
		
		final Float smokAmount = getValueInTargetUnit(smokValueStr, valueFormatIndex, unitsExpression, "Cigar-Packs/Year");
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = 
			(termExprTuple._1().isTerm()) ? new SimpleCodedValue(termExprTuple) : new ComplexCodedValue(termExprTuple);
		final PropertyValue propValue1 = 
			(smokAmount != null && smokAmount > 0) ? new AmountValue(smokAmount, new SimpleCodedValue("UNITEXP-18", "Packets of cigarettes / Year")) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#tobacco-Consumption-Status-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#tobacco-Amount
		
		return newPosTuple(map);
	}

}
