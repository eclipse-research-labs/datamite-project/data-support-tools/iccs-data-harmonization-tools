package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping 2 Cohort Fields, A) A Field with the value measured for a specific Medical Test 
 * and B) Another Field with the Date it took place.
 * <p>
 * The corresponding Reference Model term should be given. 
 * <p>
 * The Format of the number existing in the 1st Field and the Unit of Measurement should be given. 
 * <p>
 * Also we would like to know the Normal Range of values (if applicable).
 * <p> 
 * Additionally the format of the Date (2nd field) should be given. 
 */
public class LabTestOutcomeNumericPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Blood_Test, Field_Number_Format, Field_Unit_of_Measurement, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, Date_Format }
	
	/**
	 * Depending of the value of the first field (the cohort values should be aligned with yes/no terms) it introduces an OWL entity with the specific Lab Test and the appropriate outcome taking into acount the specific outcome value recorded, the cohort unit of measurement and the one specified for the given Blood Test. Also introduces the appropriate Date taking into account the Date Format given. Provided that the UNL and DNL specified, the Normal Range of Values will be also recorded, as well as whether the outcome was normal or not.
	 * 
	 * @return The amount of instances (i.e., List size() ) of OWL class 
	 * http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test 
	 * that should be created along with the parameters of each one of them (i.e., Map), 
	 * otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutValue = data.get(0); // Medical Test Outcome: Numeric Value
		final String testDateStr = data.get(1); // Medical Test Date

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Blood_Test);
		final String valueFormatIndex = getArgValue(ServiceArg.Field_Number_Format);
		final String testValueUnit = getArgValue(ServiceArg.Field_Unit_of_Measurement);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String testDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test can be an Amount
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.AMOUNT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Amount ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple2<Float, Tuple2<String, String>> valueTuple = getAmountValueTuple(testOutValue, valueFormatIndex, testValueUnit, testTerm);
		
		// IF the value is empty or not a Number, no data will be recorded
		if (valueTuple == null) return null;
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(testDateStr, testDateFormat);
		
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, testValueUnit, testTerm);
		final Term assessTerm = (normRangeTuple != null) ? getAssessmentForValue(valueTuple._1(), normRangeTuple._1(), normRangeTuple._2()) : null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue2 = (valueTuple != null) ? new AmountValue(valueTuple): null;
		final PropertyValue propValue3 = (assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;
		final PropertyValue propValue4 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(4, propValue4); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}
