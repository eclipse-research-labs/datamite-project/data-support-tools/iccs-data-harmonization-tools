package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

//TODO: Test

/** Mapping 2 fields: A) A Field the value of which indicates whether the outcome of a specific Biopsy Test was Normal or not, and B) Another Field that indicates the date when the Biopsy Test took place. The corresponding Reference Model term should be given. Also, the values of the 1st Field should be independently ** aligned with ** the Assessment Terms specified in the Reference Model. Additionally the format of the Date (2nd field) should be given. The Normal Range of Values can be optionally specified. */
public class BiopsyTestOutcomeAssessmentPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Biopsy_Test, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, UNL_and_DNL_Unit_of_Measurement, Date_Format, Reference_Model_Biopsy_Type }
	
	/** Provides the appropriate Terms for the specific biopsy Test (including type - Salivary Gland) along with its Outcome Assessment and the Date it took place, based on the Value of the given Fields, additional data provided (i.e., corresponding Reference Model terms, Date Format and Normal Range of Values) and Mapping Rules specified. In case the 1st Field is empty, no data will be recorded. Also, in case the 2nd Field is empty, no information about the Test Date will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Biopsy that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutAssess = data.get(0); // Biopsy Test Outcome: Normal/Abnormal
		final String biopsyDate = data.get(1); // Biopsy Date

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Biopsy_Test);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String valueUnit = getArgValue(ServiceArg.UNL_and_DNL_Unit_of_Measurement);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		final String salglandTermUri = getArgValue(ServiceArg.Reference_Model_Biopsy_Type);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test can be an Assessment
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.ASSESSMENT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Assessment ! Possible Test Outcome(s): " + expOutSet);
		
		final OntClass salGlandTerm = getReferenceModelTermWithURI(salglandTermUri);
		
		final Tuple2<TermExpr, Relation> testOutAssessTermExprT = getReferenceModelTermForCohortValue(testOutAssess, 0);
		
		// IF the value is empty no Data will be recorded
		if (testOutAssessTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is an Assessment Term
		if (!testOutAssessTermExprT._1().isSimpleAssessmentTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testOutAssessTermExprT._1().asString() + "\" is NOT an Assessment Term !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(biopsyDate, dateFormat);	
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, valueUnit, testTerm);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();

		final PropertyValue propValue0 = new SimpleCodedValue(salGlandTerm);
		final PropertyValue propValue1 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue2 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue3 = new SimpleCodedValue(testOutAssessTermExprT);
		final PropertyValue propValue4 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#biopsy-Type-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(4, propValue4); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}
