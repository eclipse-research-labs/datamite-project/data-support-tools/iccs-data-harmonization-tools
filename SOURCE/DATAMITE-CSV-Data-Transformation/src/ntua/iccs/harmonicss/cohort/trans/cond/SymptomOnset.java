package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping a Cohort Field that indicates whether the person has experienced a specific Symptom/Sign or not. 
 * <p>
 * The corresponding Reference Model term (Symptom) should be specified. 
 * <p>
 * Also, the values of this Field should be independently ** aligned with ** the Confirmation terms (i.e., YES/NO values) specified in the Reference Model. 
 */
public class SymptomOnset extends DataTransformation {

	private enum ServiceArg { Reference_Model_SymptomSign }
	
	/** Provides the corresponding Reference Model term (i.e., Symptom/Sign) or not, taking into account the value of the given Field and the Mapping Rules specified (i.e., align Field Values with Confirmation Terms).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Symptom-or-Sign that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String symptConfirmValue = data.get(0); // Symptom/Sign

		// Additional Data / Arguments (if any)
		final String symptomTermUri = getArgValue(ServiceArg.Reference_Model_SymptomSign);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass symptTerm = getReferenceModelTermWithURI(symptomTermUri);
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(symptConfirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, it will be ignored.
		if (confirmValue == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");

		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(symptTerm);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#symptom-sign-CV
		
		// Depending on the value of the Symptom Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
	}

}
