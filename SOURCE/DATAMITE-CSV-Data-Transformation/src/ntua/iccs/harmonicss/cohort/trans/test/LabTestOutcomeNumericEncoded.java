package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONObject;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a cohort Field with the value measured for a specific Lab Test. However, the value of this Field is not the actual value measured, but a string that points to the value measured. The corresponding Reference Model term should be specified. Also, the mapping (i.e., a JSON object/string) of the values of this Field (strings) with the real values measured (integers/real-numbers) along with the Number Format and Unit of Measurement of such values should be given. Additionally, we would like to know the Normal Range of values (if applicable). These values should be expressed in the same unit of measurement. */
public class LabTestOutcomeNumericEncoded extends DataTransformation {

	private enum ServiceArg { Reference_Model_Blood_Test, Values_Mapping_JSON, Number_Format, Unit_of_Measurement, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL }
	
	/** Provides the appropriate Term for the specific Lab Test and the Outcome measured based on the Value of the given Field and the additional parameters provided (i.e., corresponding Reference Model term, JSON object with the corresponding value measured and unit of measurement). Also, provided that the UNL and DNL specified, the Normal Range of Values will be also recorded, as well as whether the outcome was normal or not. In case the value of this Field is empty or no Number provided for the value of this field, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutValueCode = data.get(0); // Lab Test Outcome: Numeric Value (encdoded)

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Blood_Test);
		final String valueMapJsonStr = getArgValue(ServiceArg.Values_Mapping_JSON);
		final String valueFormatIndex = getArgValue(ServiceArg.Number_Format);
		final String testValueUnit = getArgValue(ServiceArg.Unit_of_Measurement);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		final JSONObject valueMapJson = JSONObject.fromObject(valueMapJsonStr);
		
		// Get Value based on the Code
		if (isEmpty(testOutValueCode)) return null;
		final String testOutValue = (valueMapJson.has(testOutValueCode)) ? valueMapJson.getString(testOutValueCode) : null;
		if (testOutValue == null) return null;
		
		final Tuple2<Float, Tuple2<String, String>> valueTuple = getAmountValueTuple(testOutValue, valueFormatIndex, testValueUnit, testTerm);
		
		// IF the value is empty or not a Number, no data will be recorded
		if (valueTuple == null) return null;
				
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, testValueUnit, testTerm);
		final Term assessTerm = (normRangeTuple != null) ? getAssessmentForValue(valueTuple._1(), normRangeTuple._1(), normRangeTuple._2()) : null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = new AmountValue(valueTuple);
		final PropertyValue propValue2 = (assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;
		final PropertyValue propValue3 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}
