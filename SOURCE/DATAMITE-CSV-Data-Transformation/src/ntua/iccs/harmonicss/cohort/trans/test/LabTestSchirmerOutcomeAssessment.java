package ntua.iccs.harmonicss.cohort.trans.test;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.VOCNS;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 2 Cohort Fields A) A Field with the Schirmer's Test Left Eye value, and B) Another Field with the Schirmer's Test Right Eye value. The Format of the numbers existing in the 1st and 2nd Fields should be given. Also, the Normal Range of values should be specified, so that it can be accordingly used for deciding whether the worst eye value was normal or not. */
public class LabTestSchirmerOutcomeAssessment extends DataTransformation {

	private enum ServiceArg { Field_Number_Format, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL }
	
	/** Provides the appropriate Term for the specific Lab Test (i.e., Schirmer's Test Worst Eye Value) and whether its outcome was Normal or not, based on the Values of the given Fields (i.e., values recorded for both Left and Right eyes) and the additional parameters provided (i.e., numbers format and normal range of values). In case both 1st and 2nd Fields are empty or their values are not valid numbers, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String schirmLeftEyeValue = data.get(0); // Schirmer Test - Left Eye Value : Number
		final String schirmRightEyeValue = data.get(1); // Schirmer Test - Right Eye Value : Number

		// Additional Data / Arguments (if any)
		final String valueFormatIndex = getArgValue(ServiceArg.Field_Number_Format);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
	
		// Data Processing
		final OntClass schirmTestOntClass = refModel.getOntClass(VOCNS + "OCULAR-01");
		
		final Float value1 = getRealNumber(schirmLeftEyeValue, valueFormatIndex);
		final Float value2 = getRealNumber(schirmRightEyeValue, valueFormatIndex);

		final Float downNormLimit = getRealNumber(downNormLimitStr, "DOT");
		final Float upNormLimit = getRealNumber(upNormLimitStr, "DOT");
		
		// Ensure that the Normal Range of Values specified
		if (downNormLimit == null && upNormLimit == null)
			throw new RuntimeException("Normal Range of Schirmer Test Values not specified !");
		
		// IF both fields/values are empty or not Real Numbers, no data will be recorded
		if (value1 == null && value2 == null) return null;
		
		final Float minValue;
		if (value1 == null) {
			minValue = value2;
		} else if (value2 == null) {
			minValue = value1;
		} else {
			minValue = Math.min(value1, value2);
		}
		
		final Term assessTerm = (downNormLimit != null || upNormLimit != null) ? getAssessmentForValue(minValue, downNormLimit, upNormLimit) : null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(schirmTestOntClass);
		final PropertyValue propValue1 = new SimpleCodedValue(assessTerm);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		
		return newPosTuple(map);
	}

}
