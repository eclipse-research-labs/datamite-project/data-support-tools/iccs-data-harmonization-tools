package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.VOCNS;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Field about the Body Mass Index value calculated. The format of the number along with the unit of measurement should be given. */
public class BodyMassIndexBasedOnValue extends DataTransformation {

	private enum ServiceArg { Number_Format, Unit_of_Measurement }
	
	/** Provides the appropriate Term (i.e., BMI category) based on Value of the given Field and additional data provide (i.e., number format and unit of measurement).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Body-Weight that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String bmiStr = data.get(0); // Body Mass Index Value

		// Additional Data / Arguments (if any)
		final String bmiFormatIndex = getArgValue(ServiceArg.Number_Format);
		final String bmiUnit = getArgValue(ServiceArg.Unit_of_Measurement);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final Float bmi = getValueInTargetUnit(bmiStr, bmiFormatIndex, bmiUnit, "kg/m^2");
	
		// IF Field is empty or not an Real Number, no data will be recorded
		if (bmi == null) return null;
		
		// Category
		final Term bmiTerm;
		if (bmi < 18.5) {
			bmiTerm = new Term(VOCNS + "BMI-01", "Underweight (BMI < 18.5)"); 
		} else if (bmi < 24.9) {
			bmiTerm = new Term(VOCNS + "BMI-02", "Normal Weight (BMI 18.5 - 24.9)"); 	
		} else if (bmi < 29.9) {
			bmiTerm = new Term(VOCNS + "BMI-03", "Overweight (BMI 25 - 29.9)"); 
		} else {
			bmiTerm = new Term(VOCNS + "BMI-04", "Obesity (BMI >= 30)"); 
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(bmiTerm);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#bmi-CV
		
		return newPosTuple(map);
	}

}
