package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Field about the Number of times a specific pregnancy outcome (e.g., number of abortions) was mentioned. The corresponding Reference Model term should be given. */
public class PregnancyOutcomeNumber extends DataTransformation {

	private enum ServiceArg { Reference_Model_Pregnancy_Outcome }
	
	/** Provides a List of Entities with the given Pregnancy Outcome, provided that the Field is not Empty and greater than zero. In the second case (number of times is 0) the system returns that none of the given pregnancy outcome was mentioned.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Pregnancy that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String pregOutNumStr = data.get(0); // Pregnancy Outcome (Integer)

		// Additional Data / Arguments (if any)
		final String outTermUri = getArgValue(ServiceArg.Reference_Model_Pregnancy_Outcome);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass outTerm = getReferenceModelTermWithURI(outTermUri);
		
		final Integer pregOutNum = getInteger(pregOutNumStr);
		
		// IF Field is empty or not an Integer, no data will be recorded
		if (pregOutNum == null || pregOutNum < 0) return null;
		
		if (pregOutNum == 0) {
			final Map<Integer, PropertyValue> map = new HashMap<>();
			final PropertyValue propValue0 = new SimpleCodedValue(outTerm);
			map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#pregnancy-Outcome-CV
			return newNegTuple(map);
		} else {
			List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
			for (int i = 0; i < pregOutNum; i++) {
				final Map<Integer, PropertyValue> map = new HashMap<>();
				final PropertyValue propValue0 = new SimpleCodedValue(outTerm);
				map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#pregnancy-Outcome-CV
				mapList.add(map);
			}
			return Tuple2.newTuple2(Boolean.TRUE, mapList);
		}
	}

}
