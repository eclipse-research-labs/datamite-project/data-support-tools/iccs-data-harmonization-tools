package ntua.iccs.harmonicss.cohort.trans;

import java.util.List;

import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Util;

/** Provides the functionality for expressing given data in different Units of Measurement */
public class AmountsValueTransformer {
	
	@SuppressWarnings("unchecked")
	private static final List<Tuple3<String, String, Double>> unitTransList = Util.newArrayList(
		// Mass Weight Units
		Tuple3.tuple("g", "mg", Math.pow(10, 3)),	// Gram (g) to Milligram (mg)
		Tuple3.tuple("g", "μg", Math.pow(10, 6)),	// Gram (g) to Microgram (μg)
		Tuple3.tuple("g", "ng", Math.pow(10, 9)),	// Gram (g) to Nanogram (ng)
		Tuple3.tuple("g", "pg", Math.pow(10, 12)),	// Gram (g) to Picogram (pg)
		Tuple3.tuple("mg", "μg", Math.pow(10, 3)),	// Milligram (mg) to Microgram (μg)
		Tuple3.tuple("mg", "ng", Math.pow(10, 6)),	// Milligram (mg) to Nanogram (ng)
		Tuple3.tuple("mg", "pg", Math.pow(10, 9)),	// Milligram (mg) to Picogram (pg)
		Tuple3.tuple("μg", "ng", Math.pow(10, 3)),	// Microgram (μg) to Nanogram (ng)
		Tuple3.tuple("μg", "pg", Math.pow(10, 6)),	// Microgram (μg) to Picogram (pg)
		Tuple3.tuple("ng", "pg", Math.pow(10, 6)),	// Nanogram (ng) to Picogram (pg)
		
		// Mass Weight Units
		Tuple3.tuple("kg", "g", Math.pow(10, 3)),	// KiloGram (kg) to Gram (g)
		// Distance Units		
		Tuple3.tuple("m", "cm", Math.pow(10, 2)),	// Meter (m) to Centimeter (cm)
		Tuple3.tuple("m", "mm", Math.pow(10, 3)),	// Meter (m) to Millimeter (mm)
		
		// Volumes Units
		Tuple3.tuple("L", "dL", Math.pow(10, 1)),	// Liter (L) to Deciliter (dL)
		Tuple3.tuple("L", "mL", Math.pow(10, 3)),	// Liter (L) to Milliliter (mL)
		Tuple3.tuple("dL", "mL", Math.pow(10, 2))	// Deciliter (dL) to Milliliter (mL)
		
		
	);

	/** @return The appropriate number based on the data specified in the above Map */
	public static Double getUnitOperator(final String srcUnit, final String trgUnit) {
		if (srcUnit.equalsIgnoreCase(trgUnit)) return 1.0; //new Double(1);
		// Search for Operator
		for (Tuple3<String, String, Double> tuple : unitTransList) {
			if (tuple._1().equalsIgnoreCase(srcUnit) && tuple._2().equalsIgnoreCase(trgUnit))
				return tuple._3();
			if (tuple._1().equalsIgnoreCase(trgUnit) && tuple._2().equalsIgnoreCase(srcUnit))
				return (1/tuple._3());
		}
		return null;
	}
	
	private static final char SLASH = '/';
	
	/** @return The number with which we should multiply the given value in order to be expressed in the requested unit of measurement */
	public static Double getUnitExprOperator(final String srcUnit, final String trgUnit) {
		if (srcUnit.equalsIgnoreCase(trgUnit)) return 1.0; // new Double(1);
		// For units
		final Double op = getUnitOperator(srcUnit, trgUnit);
		if (op != null) return op;
		// For units expressions
		final int srcIndex = srcUnit.indexOf(SLASH);
		final int trgIndex = trgUnit.indexOf(SLASH);
		if (srcIndex > 0 && trgIndex > 0) {
			final String srcUnit1 = srcUnit.substring(0, srcIndex).trim();
			final String srcUnit2 = srcUnit.substring(srcIndex + 1).trim();
			final String trgUnit1 = trgUnit.substring(0, trgIndex).trim();
			final String trgUnit2 = trgUnit.substring(trgIndex + 1).trim();
			final Double op1 = getUnitOperator(srcUnit1, trgUnit1);
			final Double op2 = getUnitOperator(srcUnit2, trgUnit2);
			if (op1 != null && op2 != null) 
				return (op1 / op2);
		}
		
		// TODO: Update IF different Units Expression Format is needed
		
		// No operator found
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private static final List<Tuple3<String, String, Double>> unitExprTransList = Util.newArrayList(
		// TODO: Maybe we should remove the 4 units conversions 
		// since the unit conversion is NOT global but depends on the specific Lab Test
		
//		// Units Conversion
//		Tuple3.tuple("mmol/L", "mg/dL", 18.018), 	// 18.01801801801802
//		Tuple3.tuple("mmol/L", "g/dL", 0.018018), 	
//		Tuple3.tuple("umol/L", "mg/dL", 0.0113),	// umol = μmol
//		Tuple3.tuple("μmol/L", "mg/dL", 0.0113),
		
		Tuple3.tuple("mg/hour", "mg/day", 24d),	
		Tuple3.tuple("mg/hour", "mg/week", 168d),
		
		// Smoking Status Amount Unit-expression
		// TODO: Check
		Tuple3.tuple("Cigars/Year", "Cigar-Packs/Year", (double)1/30),
		Tuple3.tuple("Cigar-Packs/Month", "Cigar-Packs/Year", 12d),
		Tuple3.tuple("Cigars/Month", "Cigar-Packs/Year", (double)12/30),
		Tuple3.tuple("Cigar-Packs/Day", "Cigar-Packs/Year", 365d),
		Tuple3.tuple("Cigars/Day", "Cigar-Packs/Year", (double)365/30),
		// Same Units
		Tuple3.tuple("mmol/L", "mEq/L", 1d),
		Tuple3.tuple("IE/ml", "IU/mL", 1d)
	);

	/** @return The given value expressed in the requested unit (target) taking into account the current unit (source) */
	public static Float expressValueInTargetUnit(Float value, String srcUnit, String trgUnit) {
		if (value == null) return null;
		if (srcUnit == null || trgUnit == null) return null;
		
		// Source & Target units being the same
		if (srcUnit.equalsIgnoreCase(trgUnit)) return value;
		
		// Predefined Unit Conversion
		for (Tuple3<String, String, Double> unitExprT : unitExprTransList) {
			if (unitExprT._1().equalsIgnoreCase(srcUnit) && unitExprT._2().equalsIgnoreCase(trgUnit)) {
				return new Double(value * unitExprT._3()).floatValue();
			}
		}
		
		// Find operator
		final Double op = getUnitExprOperator(srcUnit, trgUnit);
		if (op != null) return new Double(value * op).floatValue();
		
		throw new RuntimeException("No registered service for Value Transformation from \"" + srcUnit +"\" to \"" + trgUnit + "\".");
	}
	
	/** For testing purposes ... */
	public static void main(String[] args) {
		System.out.println(" >> AmountsValueTransformer: BEGIN");
		System.out.println();
		
		System.out.println(getUnitExprOperator("g/L", "g/dL"));
		
		System.out.println("\nValues Converstion ... \n");
		
		exprValueToUnit(5f, "L", "mL");
		exprValueToUnit(10f, "g", "kg");
		
		exprValueToUnit(10f, "mg/L", "μg/mL");

		exprValueToUnit(10f, "IE/ml", "IU/mL");
		exprValueToUnit(10f, "mmol/L", "mEq/L");
		
//		exprValueToUnit(10f, "umol/l", "mg/dL");
		
		exprValueToUnit(15f, "g/m^2", "kg/m^2");
//		exprValueToUnit(15f, "kg/cm^2", "kg/m^2");
		
		exprValueToUnit(10f, "mg/hour", "mg/week");
		
		System.out.println();
		System.out.println(" >> AmountsValueTransformer: END");
	}
	
	private static void exprValueToUnit(final Float value, final String srcUnit, final String trgUnit) {
		System.out.println( value + " " + srcUnit + " --> " + expressValueInTargetUnit(value, srcUnit, trgUnit) + " " + trgUnit);
	}
	
	
	public static String unitTransListToString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("unitTransList-size() = " + unitTransList.size() + "\n");
		for (Tuple3<String, String, Double> tuple : unitTransList) {
			sb.append(" - " + tuple._1() + " --> x " + tuple._3() + " --> " + tuple._2() + "\n");
		}
		return sb.toString();
	}

}
