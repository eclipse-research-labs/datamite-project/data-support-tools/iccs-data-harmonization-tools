package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping two Fields, A) A Field that indicates whether the person has received a specific Drug or not 
 * and B) Another Field that indicates that the person received this drug before the date specified. 
 * <p>
 * The corresponding Reference Model term (Drug) should be specified. 
 * <p>
 * Also, the values of this Field should be independently ** aligned with ** the Confirmation terms 
 * (i.e., YES/NO values) specified in the Reference Model. 
 * <p>
 * Additionally the Format of the Date (2nd Field) should be given. */
public class DrugAdminPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_DrugSubstance, Date_Format }
	
	/** Provides the corresponding Reference Model term (i.e., Medical Condition) along with the Period of Time (i.e., the one before a specific date) based on the given Data (i.e., Fields values, Additional Data and Mapping Rules), provided that the value existing in the 1st field indicates that the person has received the specific Drug.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Medication that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String drugConfirmValue = data.get(0); // Drug Field: YES/NO
		final String drugDate = data.get(1); // Drug Date

		// Additional Data / Arguments (if any)
		final String drugTermUri = getArgValue(ServiceArg.Reference_Model_DrugSubstance);
		final String drugDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass drugTerm = getReferenceModelTermWithURI(drugTermUri);
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(drugConfirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, it will be ignored.
		if (confirmValue == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(drugDate, drugDateFormat);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 =  new SimpleCodedValue(drugTerm);
		final PropertyValue propValue1 = 
			(dateTuple != null) ? new DateIntervalTypeA(null, new DateValue(dateTuple)): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Date-Interval
		
		// Depending on the value of the Drug Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
	}

}
