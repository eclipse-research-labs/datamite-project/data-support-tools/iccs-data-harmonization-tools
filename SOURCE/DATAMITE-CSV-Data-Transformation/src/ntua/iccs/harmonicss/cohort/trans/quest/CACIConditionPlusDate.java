package ntua.iccs.harmonicss.cohort.trans.quest;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.BooleanValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Fields. A) A Field about a CACI condition and B) Another Field with the Date the Questionnaire filled in. The corresponding Reference Model term should be specified. The values of the 1st Field should be independently ** aligned with ** the Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Also, the format of the Date (2nd field) should be given. */
public class CACIConditionPlusDate extends DataTransformation {

	private enum ServiceArg { CACI_Condition, Date_Format }
	
	/** Provides the values of the parameters specified in the right side taking into account the patient data in the given fields and the Mapping Rules specified (i.e., mapping of values with Confirmation Terms) as well as additional parameters provided (i.e., CACI condition, Date Format).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#CACI-Comorbidity that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String condValue = data.get(0); // CACI Condition
		final String dateStr = data.get(1); // CACI Questionnaire Date
		
		// Additional Data / Arguments (if any)
		final String caciConditionUri = getArgValue(ServiceArg.CACI_Condition);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass caciConditionTerm = getReferenceModelTermWithURI(caciConditionUri); 
		
		final Tuple2<TermExpr, Relation> condConfirmValueTermExprT = getReferenceModelTermForCohortValue(condValue, 0);
		
		// IF the value is empty or not aligned with Reference Model confirmation terms, no data will be recorded
		if (condConfirmValueTermExprT == null) return null;
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(dateStr, dateFormat);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(caciConditionTerm);
		final PropertyValue propValue1 = new BooleanValue(condConfirmValueTermExprT);
		final PropertyValue propValue2 = 
			(dateTuple != null) ? new DateValue(dateTuple): null;
		
		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#quest-CACI-Comorbidity-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#condition-Presence-CV
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#completion-Date
			
		return newPosTuple(map);
	}

}
