package ntua.iccs.harmonicss.cohort.trans.person;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.simple.StringValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

//TODO: check

/** Mapping Cohort Fields about Patient Unique ID, Date of Birth, and two other Fields about the Age of a person at pSS Diagnosis and Cohort inclusion, with the corresponding entity from the Reference Model. The Date/Year of Birth should follow a predefined format. */
public class PersonWithDoBplusDiagnosisCohortAges extends DataTransformation {

	private enum ServiceArg { Date_of_Birth_Format }
	
	/** Provides the values of the properties mentioned based on the data given (UID and DoB plus Ages) and the parameters specified (DoB Format).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Person that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String uid = data.get(0); // Patient Unique ID Field
		final String dob = data.get(1); // Date of Birth Field
		final String diagnAge = data.get(2); // Age at pSS diagnosis
		final String cohorAge = data.get(3); // Age at Cohort inclusion

		// Additional Data / Arguments (if any)
		final String dateFormat = getArgValue(ServiceArg.Date_of_Birth_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		if (isEmpty(uid)) throw new RuntimeException("Patient ID cannot be empty !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(dob, dateFormat);
		final Integer diagnAgeInt = getInteger(diagnAge);
		final Integer cohorAgeInt = getInteger(cohorAge);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new StringValue(uid);
		final PropertyValue propValue1 = 
			(dateTuple != null) ? new DateValue(dateTuple._1(), dateTuple._2(), dateTuple._3()): null;
		final PropertyValue propValue2 = 
			(dateTuple != null && diagnAgeInt != null) ? new DateValue(dateTuple._1() + diagnAgeInt, dateTuple._2(), dateTuple._3()): null;
		final PropertyValue propValue3 = 
			(dateTuple != null && cohorAgeInt != null) ? new DateValue(dateTuple._1() + cohorAgeInt, dateTuple._2(), dateTuple._3()): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Unique-ID
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Date-Birth
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-SS-Diagnosis
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Cohort-Inclusion
		
		return newPosTuple(map);
	}

}
