package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 3 Cohort Fields A. A Field with the Lab Test Outcome measured (i.e., the value of this Field is a Real Number), B. Another Field that indicates whether this outcome was Normal or not and C. A third Field with the Date the Lab Test took place. For this purpose, the corresponding Reference Model term should be given as well as the format of the real number existing in the 1st Field and the Unit of Measurment. Also, the values of the 2nd Field should be independently aligned with Assessment term (i.e., Normal/Abnormal values). Additionally the format of the Date existign in the 3rd Field should be given. Moreover, the Normal Range of Values for this Lab Test can be also specified (optinal). However, they should be also expressed in the same Unit of Measurement with the one specified for the 1st Field. */
public class LabTestOutcomeNumericAndAssessmentPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Blood_Test, Outcome_Format, Outcome_Unit_of_Measurement, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, Date_Format }
	
	/** This service provides the values of the parameters existing in the rigth side, provided that at least on of the two first fields is not Empty. The outcome of the Lab Test depends on the value existing in the 1st Field as well as the Unit of Measurment specified. The 2nd Field along with the mapping rules already specified are being used for detecting whether the outcome was normal or not. The Normal Range of values are also recorded, provided that the corresponding attributes (optinal) are given. The value existing in the 3rd Field along with the Data format are used for specifying the date the test took place.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {

		// Input Fields
		final String testOutValue = data.get(0); // Medical Test Outcome: Numeric Value
		final String testOutAssess = data.get(1); // Medical Test Outcome: Assessment
		final String testDateStr = data.get(2); // Medical Test Date

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Blood_Test);
		final String testValueFormatIndex = getArgValue(ServiceArg.Outcome_Format);
		final String testValueUnit = getArgValue(ServiceArg.Outcome_Unit_of_Measurement);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String testDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test can be an Amount
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.AMOUNT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Amount ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(testDateStr, testDateFormat);
		final Tuple2<Float, Tuple2<String, String>> valueTuple = getAmountValueTuple(testOutValue, testValueFormatIndex, testValueUnit, testTerm);
		final Tuple2<TermExpr, Relation> testOutAssessTermExprT = getReferenceModelTermForCohortValue(testOutAssess, 1);
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, testValueUnit, testTerm);
		
		// IF neither value (e.g., being empty or not a Number) nor assessment provided, no data will be recorded
		if (valueTuple == null && testOutAssessTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is an Assessment Term
		if (testOutAssessTermExprT != null && !testOutAssessTermExprT._1().isSimpleAssessmentTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testOutAssessTermExprT._1().asString() + "\" is NOT an Assessment Term !");
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue2 = (valueTuple != null) ? new AmountValue(valueTuple): null;
		final PropertyValue propValue3 = (testOutAssessTermExprT != null) ? new SimpleCodedValue(testOutAssessTermExprT) : null;
		final PropertyValue propValue4 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(4, propValue4); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}
	
}