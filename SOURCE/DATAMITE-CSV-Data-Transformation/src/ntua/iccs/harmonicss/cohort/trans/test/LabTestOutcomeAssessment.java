package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping a Cohort Field the value of which indicates whether the outcome of a specific Lab Test was Normal or not.
 * <p>
 * The corresponding Reference Model term should be specified.
 * <p>
 * Also, the values of this Field should be independently ** aligned with ** the Assessment Terms specified in the Reference Model.
 * <p>
 * Additionally, we would like to know the Normal Range of values (if applicable).
 */
public class LabTestOutcomeAssessment extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, UNL_and_DNL_Unit_of_Measurement }
	
	/** Provides the appropriate Term for the specific Lab Test and the Outcome Assessment based on the Value of the given Field, the parameters provided (i.e., corresponding Reference Model) and Mapping Rules specified. Also, provided that the UNL and DNL specified (including Unit of Measurement), the Normal Range of Values will be also recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutAssess = data.get(0); // Lab Test Outcome: Normal/Abnormal

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String unit = getArgValue(ServiceArg.UNL_and_DNL_Unit_of_Measurement);
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri); 
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Ensure that the value of the given Test can be an Assessment
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.ASSESSMENT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Assessment ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple2<TermExpr, Relation> testOutAssessTermExprT = getReferenceModelTermForCohortValue(testOutAssess, 0);
		
		// IF the value is empty or not-aligned with Assessment Terms, no Data will be recorded
		if (testOutAssessTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is an Assessment Term
		if (!testOutAssessTermExprT._1().isSimpleAssessmentTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testOutAssessTermExprT._1().asString() + "\" is NOT an Assessment Term !");
		
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, unit, testTerm);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = new SimpleCodedValue(testOutAssessTermExprT);
		final PropertyValue propValue2 = 
			(normRangeTuple != null) ? new AmountInterval(normRangeTuple._1(), normRangeTuple._2(), 
				new SimpleCodedValue(normRangeTuple._3()._1(), normRangeTuple._3()._2())) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}