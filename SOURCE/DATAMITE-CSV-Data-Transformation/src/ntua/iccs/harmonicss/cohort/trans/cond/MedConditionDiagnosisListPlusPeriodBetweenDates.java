package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping three Cohort Fields a) A Field with one or more Medical Conditions diagnosed, separated by comma, and b) Two other Fields about the period of time ALL the Medical Conditions diagnosed. The different medical conditions (cohort terms) existing in the 1st Field should be independently ** aligned with ** the Medical Condition terms specified in the Reference Model. Additionally the Format of the Date (2nd and 3rd Fields) should be given. */
public class MedConditionDiagnosisListPlusPeriodBetweenDates extends DataTransformation {

	private enum ServiceArg { Date_Format }
	
	/** Provides a List of Entities with the corresponding Reference Model term (i.e., Medical Condition) along with the period of time the medical condition diagnosed based on the value of the given Fields, additional data given (i.e., date format) and the Mapping Rules specified (i.e., mapping of cohort Medical Condition Terms/Values with the corresponding Reference Model terms about Medical Conditions). If the value of the 1st Field is empty or none of the comma separated terms are aligned with the Reference Model Medical Condition terms, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Diagnosis that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String termsStr = data.get(0); // Medical Condition(s): Comma-separated Terms
		final String startDateStr = data.get(1); // Diagnosis in Period Start Date
		final String endDateStr = data.get(2); // Diagnosis in Period End Date

		// Additional Data / Arguments (if any)
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
final List<String> strList = getTermsStrList(termsStr);
		
		// IF field is empty
		if (strList == null) return null;
		
		final List<Tuple2<TermExpr, Relation>> termExprTupleList = new ArrayList<>();
		for (String str : strList) {
			final Tuple2<TermExpr, Relation> termExprTuple = getReferenceModelTermForCohortValue(str, 0);
			if (termExprTuple == null) continue;
			// Keep only Medical Conditions
			if (!termExprTuple._1().isMedicalCondition()) continue;
			termExprTupleList.add(termExprTuple);
		}
		
		// IF none term aligned with symptoms terms
		if (termExprTupleList.isEmpty()) return null;
		
		final Tuple3<Integer, Integer, Integer> startDateTuple = getDateTuple(startDateStr, dateFormat);
		final Tuple3<Integer, Integer, Integer> endDateTuple = getDateTuple(endDateStr, dateFormat);
		
		// Ensure that the Date Interval (Start Date - End Date) is valid
		final boolean isDateIntervalValid = isDateIntervalValid(startDateTuple, endDateTuple);
		if (!isDateIntervalValid) {
			log.warn("Invalid Date Interval: [" + startDateTuple + " , " + endDateTuple + "] Period of Time is being ignored !");
		}
		
		List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
		for (Tuple2<TermExpr, Relation> termExprTuple : termExprTupleList) {
			final Map<Integer, PropertyValue> map = new HashMap<>();
			final PropertyValue propValue0 = 
				(termExprTuple._1().isTerm()) ? new SimpleCodedValue(termExprTuple) : new ComplexCodedValue(termExprTuple);
			final PropertyValue propValue1 = 
				((startDateTuple != null || endDateTuple != null) && isDateIntervalValid) ? 
					new DateIntervalTypeA(startDateTuple, endDateTuple): null;
			map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-CV
			map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-Date
			mapList.add(map);
		}
		
		return Tuple2.newTuple2(Boolean.TRUE, mapList);

	}

}
