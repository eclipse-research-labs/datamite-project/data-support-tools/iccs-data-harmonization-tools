package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.BooleanValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping a Cohort Field about a Lab Test with a Boolean Value (i.e., YES/NO). 
 * <p>
 * The corresponding Reference Model term should be given. 
 * <p>
 * Also, the values of this field should be independently ** aligned with ** Confirmation terms 
 * (i.e., YES/NO values) specified in the Reference Model.
 */
public class LabTestOutcomeBoolean extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test }
	
	/** Provides the appropriate Terms for the specific Lab Test along with its Outcome, based on the Value of the given Field, the parameters provided (i.e., corresponding Reference Model term) and the Mapping Rules already specified.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testValue = data.get(0); // Lab Test Outcome: Boolean Value

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test is a Boolean 
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.BOOLEAN))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not a Boolean ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple2<TermExpr, Relation> testConfirmValueTermExprT = getReferenceModelTermForCohortValue(testValue, 0);
		
		// IF the value is empty or not aligned with Reference Model confirmation terms, no data will be recorded
		if (testConfirmValueTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!testConfirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testConfirmValueTermExprT._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = new BooleanValue(testConfirmValueTermExprT);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		
		return newPosTuple(map);
	}

}