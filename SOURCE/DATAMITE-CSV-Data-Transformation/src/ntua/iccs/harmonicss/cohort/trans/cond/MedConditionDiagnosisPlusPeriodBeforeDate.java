package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Cohort Fields: a) A Field that indicates whether the person has been diagnosed with a particular condition or not, and b) Another Fields with the Date BEFORE which the condition was examined. The corresponding Reference Model term (Medical Condition) should be specified. Also, the values of the 1st Field should be independently ** aligned with ** the Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Additionally the Format of the Date (2nd Field) should be given. */
public class MedConditionDiagnosisPlusPeriodBeforeDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Condition, Date_Format }
	
	/** Provides the corresponding Reference Model term (i.e., Medical Condition) along with the relevant Period of Time when the condition was examined (i.e., the one before the date of the second field) based on the given Data (i.e., given Disorder and Date Format) and Mapping Rules specified (for yes/no values), provided that the value existing in the 1st field is not empty. More precisely, IF the value is yes (i.e., aligned with the YES confirmation term) the service indicates that the person has been diagnosed with the specific condition, otherwise not.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Diagnosis that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String condConfirmValue = data.get(0); // Medical Condition
		final String diagPeriodEndDate = data.get(1); // Date

		// Additional Data / Arguments (if any)
		final String condTermUri = getArgValue(ServiceArg.Reference_Model_Condition);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass condTerm = getReferenceModelTermWithURI(condTermUri);
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(condConfirmValue, 0);

		// IF the value is empty, it will be ignored.
		if (confirmValue == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Integer, Integer, Integer> endDateTuple = getDateTuple(diagPeriodEndDate, dateFormat);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(condTerm);
		final PropertyValue propValue1 = 
			(endDateTuple != null) ? new DateIntervalTypeA(null, new DateValue(endDateTuple)): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-Date
		
		// Depending on the value of the Condition Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
		
	}

}
