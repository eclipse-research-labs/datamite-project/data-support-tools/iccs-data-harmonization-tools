package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValueList;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping three Cohort Fields a) A Field with one or more Medical Condition(s) diagnosed, separated by comma, b) A Field with one or more Organ(s) affected, separated by comma, and c) Another Field about the Date the Medical Condition diagnosed. The terms/values of the 1st and 2nd Fields should be independently ** aligned with ** the corresponding Reference Model terms (Condition and Organs respectively). Additionally the Format of the Date (3rd Field) should be given. */
public class MedConditionDiagnosisOrgansListPlusDate extends DataTransformation {

	private enum ServiceArg { Date_Format }
	
	/** Provides a List of Entities with the corresponding Medical Condition along with the Organ(s) affected and the Date the condition diagnosed, based on the given Data (i.e., Fields values, Additional Data, IF needed, and Mapping Rules) provided that the value of the 1st field is not empty and value(s) are aligned with Medical Condition terms.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Diagnosis that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String condTermsStr = data.get(0); // Medical Condition(s): Comma-separated Terms
		final String organTermsStr = data.get(1); // Organ(s): Comma-separated Terms
		final String condDate = data.get(2); // Date of Diagnosis

		// Additional Data / Arguments (if any)
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		
		// Conditions Diagnosed
		final List<String> condTermList = getTermsStrList(condTermsStr);
		
		// IF field is empty
		if (condTermList == null) return null;
		
		final List<Tuple2<TermExpr, Relation>> condTermTupleList = new ArrayList<>();
		for (String str : condTermList) {
			final Tuple2<TermExpr, Relation> termExprTuple = getReferenceModelTermForCohortValue(str, 0);
			if (termExprTuple == null) continue;
			// Keep only Medical Conditions
			if (!termExprTuple._1().isMedicalCondition()) continue;
			condTermTupleList.add(termExprTuple);
		}
		
		// IF none term aligned with condition terms
		if (condTermTupleList.isEmpty()) return null;
		
		// Organs affected
		final List<String> organTermList = getTermsStrList(organTermsStr);
		final List<Tuple2<TermExpr, Relation>> organTermExprTupleList = new ArrayList<>();
		if (organTermList != null) { 
			for (String organTerm : organTermList) {
				final Tuple2<TermExpr, Relation> organTermExprTuple = getReferenceModelTermForCohortValue(organTerm, 1);
				if (organTermExprTuple == null) continue;
				// Keep only Organ
				if (!organTermExprTuple._1().isOrgan()) continue;
				organTermExprTupleList.add(organTermExprTuple);
			}
		}	
		
		final PropertyValue organsValue;
		if (!organTermExprTupleList.isEmpty()) {
			final List<PropertyValue> propValueList = new ArrayList<>();
			for (Tuple2<TermExpr, Relation> organTermExprTuple : organTermExprTupleList) {
				if (organTermExprTuple._1().isTerm()) {
					propValueList.add(new SimpleCodedValue(organTermExprTuple));
				} else {
					propValueList.add(new ComplexCodedValue(organTermExprTuple));
				}
			}
			organsValue = new PropertyValueList(propValueList);
		} else {
			organsValue = null;
		}
		
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(condDate, dateFormat);
		
		List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
		for (Tuple2<TermExpr, Relation> termExprTuple : condTermTupleList) {
			final Map<Integer, PropertyValue> map = new HashMap<>();
			final PropertyValue propValue0 = 
				(termExprTuple._1().isTerm()) ? new SimpleCodedValue(termExprTuple) : new ComplexCodedValue(termExprTuple);
			final PropertyValue propValue1 = organsValue;	
			final PropertyValue propValue2 = 
				(dateTuple != null) ? new DateValue(dateTuple): null;
				map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-CV
				map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-Organ-CV
				map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-Date
				mapList.add(map);
		}
		
		return Tuple2.newTuple2(Boolean.TRUE, mapList);
	}

}
