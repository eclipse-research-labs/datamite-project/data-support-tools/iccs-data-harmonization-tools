package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

//TODO: check

/** Mapping ... */
public class MedImgTestOutcomeAssessmentPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Date_Format }
	
	/** Provides ...
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Medical-Imaging-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutAssess = data.get(0); // Medical Imaging Test Outcome: Normal/Abnormal
		final String testDateStr = data.get(1); // Medical Imaging Test Date

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String testDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri); 
		final Tuple2<TermExpr, Relation> testOutAssessTermExprT = getReferenceModelTermForCohortValue(testOutAssess, 0);
		
		// IF the value is empty or not-aligned with Assessment Terms, no Data will be recorded
		if (testOutAssessTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is an Assessment Term
		if (!testOutAssessTermExprT._1().isSimpleAssessmentTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testOutAssessTermExprT._1().asString() + "\" is NOT an Assessment Term !");
				
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(testDateStr, testDateFormat);
				
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue2 = new SimpleCodedValue(testOutAssessTermExprT);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		
		// The List should often contain only one Map (so that only one instance will be created)
		return newPosTuple(map);
	}

}
