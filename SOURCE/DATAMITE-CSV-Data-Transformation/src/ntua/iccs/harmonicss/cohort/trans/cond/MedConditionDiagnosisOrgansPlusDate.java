package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValueList;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping three Cohort Fields a) A Field that indicates whether the person has been diagnosed with 
 * a particular condition or not, b) A Field about the organ(s) affected, and c) Another Field about 
 * the Date the Medical Condition diagnosed. 
 * <p>
 * The values of the 1st Field should be independently ** aligned with ** either Confirmation terms 
 * (i.e., YES/NO values) or the specific Medical Condition type specified in the Reference Model. 
 * <p>
 * In the first case, the corresponding Reference Model term (Medical Condition) should be given. 
 * <p>
 * Also, the different values existing in the 2nd Field should be independently ** aligned with ** the 
 * Organs specified in the Reference Model. 
 * <p>
 * Additionally the Format of the Date (3rd Field) should be given. 
 */
public class MedConditionDiagnosisOrgansPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Condition, Date_Format }
	
	/** Provides the corresponding Reference Model term (i.e., Medical Condition) along with the Organ(s) affected and the Date the condition diagnosed based on the given Data (i.e., Fields values, Additional Data, IF needed, and Mapping Rules) provided that the value existing in the 1st field indicates that the person has been diagnosed with the specific Medical Condition (i.e., being a Confirmation Term) or is aligned with the corresponding diagnosis term (i.e., being a Medical Condition).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Diagnosis that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String condValue = data.get(0); // Medical Condition
		final String organsStr = data.get(1); // Organ(s)
		final String condDate = data.get(2); // Date of Diagnosis

		// Additional Data / Arguments (if any)
		final String condTermUri = getArgValue(ServiceArg.Reference_Model_Condition);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		// Condition diagnosed
		if (isEmpty(condValue)) return null;
		final Tuple2<TermExpr, Relation> condTermExprTuple = getReferenceModelTermForCohortValue(condValue, 0);
		
		if (!condTermExprTuple._1().isSimpleConfirmationTerm() && !condTermExprTuple._1().isMedicalCondition()) return null;
		
		// Organs affected
		final List<String> orgStrList = getTermsStrList(organsStr);
		final List<Tuple2<TermExpr, Relation>> organTermExprTupleList = new ArrayList<>();
		if (orgStrList != null) { 
			for (String orgStr : orgStrList) {
				final Tuple2<TermExpr, Relation> organTermExprTuple = getReferenceModelTermForCohortValue(orgStr, 1);
				if (organTermExprTuple == null) continue;
				// Keep only Organ
				if (!organTermExprTuple._1().isOrgan()) continue;
				organTermExprTupleList.add(organTermExprTuple);
			}
		}
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(condDate, dateFormat);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final boolean stmtBoolean;
		final PropertyValue propValue0;
		if (condTermExprTuple._1().isSimpleConfirmationTerm()) {
			stmtBoolean = (condTermExprTuple._1().isConfirmationTermYES());
			final OntClass condTerm = getReferenceModelTermWithURI(condTermUri);
			propValue0 = new SimpleCodedValue(condTerm.getLocalName(), condTerm.getLabel(null));
		} else { // Medical Condition
			stmtBoolean = true;
			propValue0 = (condTermExprTuple._1().isTerm()) 
				? new SimpleCodedValue(condTermExprTuple) : new ComplexCodedValue(condTermExprTuple);
		}
		
		final PropertyValue propValue1;
		if (!organTermExprTupleList.isEmpty()) {
			final List<PropertyValue> propValueList = new ArrayList<>();
			for (Tuple2<TermExpr, Relation> organTermExprTuple : organTermExprTupleList) {
				if (organTermExprTuple._1().isTerm()) {
					propValueList.add(new SimpleCodedValue(organTermExprTuple));
				} else {
					propValueList.add(new ComplexCodedValue(organTermExprTuple));
				}
			}
			propValue1 = new PropertyValueList(propValueList);
		} else {
			propValue1 = null;
		}
		
		final PropertyValue propValue2 = 
			(dateTuple != null) ? new DateValue(dateTuple): null;;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-Organ-CV
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-Date
		
		if (stmtBoolean) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
	}

}
