package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONArray;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Fields, A) A Field that indicates i., whether a person is currently receiving a drug (with a given dosage amount range), ii., or whether he/she has received that drug in the past, iii., or that he person has never received that drug and B) Another Field that indicates the date the person was questioned. The corresponding Reference Model term (Drug) should be specified along with the Dosage Amount Range (e.g., less than 20 mg/day) administered. The values of the 1st Field about Current, Past and Never should be specified (in the form of a JSON array). Additionally the Format of the Date (2nd Field) should be given. */
public class DrugDosageRangeAdminCurrentPastNever extends DataTransformation {

	private enum ServiceArg { Reference_Model_DrugSubstance, Dosage_Value_Range_Down_Limit, Dosage_Value_Range_Up_Limit, Current_JA, Past_JA, Never_JA, Date_Format }
	
	/** Provides the corresponding Reference Model term (i.e., Drug), the Dosage administered (i.e., Amount Interval) along with the relevant Period of Time (i.e., the period before or after the given date) based on the value of the 1st field and additional parameters provided (i.e., drug and dosage, meaning of values and date format), provided that the value existing in the 1st field is not empty. More precisely, IF the value is current or past (i.e., aligned with the YES confirmation term) the service indicates that the person is linked with that drug (either now or in the past) and dosage, otherwise not (never).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Medication that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String drugStatusValue = data.get(0); // Drug & Dosage Field: Current/Past/Never value
		final String drugDate = data.get(1); // Date

		// Additional Data / Arguments (if any)
		final String drugTermUri = getArgValue(ServiceArg.Reference_Model_DrugSubstance);
		final String dosageRangeDownLimit = getArgValue(ServiceArg.Dosage_Value_Range_Down_Limit);
		final String dosageRangeUpLimit = getArgValue(ServiceArg.Dosage_Value_Range_Up_Limit);
		final String currentJAStr = getArgValue(ServiceArg.Current_JA);
		final String pastJAStr = getArgValue(ServiceArg.Past_JA);
		final String neverJAStr = getArgValue(ServiceArg.Never_JA);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass drugTerm = getReferenceModelTermWithURI(drugTermUri);
		
		final Tuple2<OntClass, String> dosageUnitT = getDrugDosageUnit(drugTerm);
		if (dosageUnitT == null)
			throw new RuntimeException("About the Drug with code " + drugTerm.getLocalName() + " we do not know the Dosage Amount Unit and Frequency of Use.");
		
		final Float downValue = getRealNumber(dosageRangeDownLimit, "DOT");
		final Float upValue = getRealNumber(dosageRangeUpLimit, "DOT");
		
		if (downValue == null && upValue == null)
			throw new RuntimeException("The range of Dosage Values has not specified yet.");
		
		final JSONArray currentJA = JSONArray.fromObject(currentJAStr);
		final JSONArray pastJA = JSONArray.fromObject(pastJAStr);
		final JSONArray neverJA = JSONArray.fromObject(neverJAStr);
		
		// IF the value is empty, it will be ignored.
		if (isEmpty(drugStatusValue)) return null;
		
		// Ensure that the values is one of the current/past/never terms/values
		if (!currentJA.contains(drugStatusValue) && !pastJA.contains(drugStatusValue) && !neverJA.contains(drugStatusValue)) {
			log.warn("The value of the Field is none of the given terms about Current/Past/Never.. Data being ignored !");
			return null;
		}
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(drugDate, dateFormat);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(drugTerm);
		final PropertyValue propValue1 = new AmountInterval(downValue, upValue, new SimpleCodedValue(dosageUnitT._1().getLocalName(), dosageUnitT._2()));
		final PropertyValue propValue2;
		if (dateTuple != null && (currentJA.contains(drugStatusValue) || pastJA.contains(drugStatusValue)) ) {
			if (currentJA.contains(drugStatusValue)) {
				// Current ... Drug administered in the period of time AFTER the given Date
				propValue2 = new DateIntervalTypeA(new DateValue(dateTuple), null);
			} else {
				// Past ... Drug administered in the period of time BEFORE the given Date
				propValue2 = new DateIntervalTypeA(null, new DateValue(dateTuple));
			}
		} else {
			propValue2 = null;
		}

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Dosage
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Date-Interval
		
		// Depending on the value of the Field, return a positive/negative "statement"
		if (currentJA.contains(drugStatusValue) || pastJA.contains(drugStatusValue)) {
			// Current/Past ... Drug administered
			return newPosTuple(map);
		} else {
			// Never ... Drug NOT administered
			return newNegTuple(map);
		}
	
	}

}
