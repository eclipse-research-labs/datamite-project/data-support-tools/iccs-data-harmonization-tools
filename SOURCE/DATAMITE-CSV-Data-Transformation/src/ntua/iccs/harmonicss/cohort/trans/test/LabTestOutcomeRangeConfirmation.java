package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping a Cohort Field the value of which indicates whether the outcome of a specific Lab Test 
 * was within a specific Range of Values. 
 * <p>
 * The corresponding Reference Model Lab Test term should be specified along with the specific Range of Values. 
 * <p>
 * Also, the values of this Field that indicate whether the patient agrees with the given Lab Test Outcome Range statement 
 * should be independently ** aligned with ** the Confirmation Terms (i.e., Yes/No) specified in the Reference Model. 
 * <p>
 * Additionally, we would like to know the Normal Range of values (if applicable).
 */
public class LabTestOutcomeRangeConfirmation extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Value_Range_Down_Limit, Value_Range_Upper_Limit, Unit_of_Measurement, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL }
	
	/** Provides the appropriate Term for the specific Lab Test and the Outcome Range, based on the Value of the given Field, the parameters provided (i.e., corresponding Reference Model Lab Test and Range of Values) and Mapping Rules specified. Also, provided that the UNL and DNL specified (including Unit of Measurement), the Normal Range of Values will be also recorded. In case the value of the field is empty or not aligned with the Confirmation terms or the Range of outcome Values not specified, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String labTestOutcomeRangeConfirmValue = data.get(0); // Lab Test Outcome Range: YES/NO

		// Additional Data / Arguments (if any)
		final String labTestTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String downLimitStr = getArgValue(ServiceArg.Value_Range_Down_Limit);
		final String upLimitStr = getArgValue(ServiceArg.Value_Range_Upper_Limit);
		final String unit = getArgValue(ServiceArg.Unit_of_Measurement);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass labTestTerm = getReferenceModelTermWithURI(labTestTermUri);
		
		// Ensure that the value of the given Test can be an Amount
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(labTestTerm);
		if (!expOutSet.contains(TestOutcome.AMOUNT))
			throw new RuntimeException("The expected outcome for the given test (" + labTestTerm.getLocalName() +") is not an Amount ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple3<Float, Float, Tuple2<String, String>> rangeTuple = getAmountNormalRangeTuple(downLimitStr, upLimitStr, unit, labTestTerm);
		
		if (rangeTuple == null) throw new RuntimeException("The Range of Values has NOT specified !");
		
		final Tuple2<TermExpr, Relation> confirmValueTermExprT = getReferenceModelTermForCohortValue(labTestOutcomeRangeConfirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, no Data will be recorded
		if (confirmValueTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValueTermExprT._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, unit, labTestTerm);
		
		final AmountInterval amountInterval = 
			( confirmValueTermExprT._1().isConfirmationTermYES() )
				? new AmountInterval(rangeTuple) : getOppositeRangeAmountInterval(rangeTuple);
		
		final Term assessTerm = 
			(normRangeTuple != null) ? 
				getAssessmentForRange(amountInterval.getDownValue(), amountInterval.getUpValue(), normRangeTuple._1(), normRangeTuple._2()) : null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(labTestTerm);
		final PropertyValue propValue1 = amountInterval;
		final PropertyValue propValue2 = (assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;			
		final PropertyValue propValue3 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}
