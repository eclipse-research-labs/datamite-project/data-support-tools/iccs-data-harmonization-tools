package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 3 Cohort Fields, A) A Field about whether the outcome of an Imaging Test was normal or not and B) Two other Fields that indicate the Date when the Imaging Test took place. From these two Fields, one indicates the date an Event took place and the other one the Time (i.e., years/months/days) elapse since that date, when the test took place. The corresponding Reference Model term should be specified. The values of the 1st Field should be independently ** aligned with ** the Assessment Terms (i.e., Normal/Abnormal) specified in the Reference Model. Additionally the format of the Date (2nd field) along with the Unit of Time Elapse (3rd field) should be given. */
public class MedImgTestOutcomeAssessmentPlusEventDateAndTimeElapse extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Event_Date_Format, Elapse_Time_Unit }
	
	/** Provides the values of the parameters specified in the right side (i.e., test code, assessment and date) taking into account the patient data (i.e., the values in the 3 Fields) as well as additional parameters provided (i.e., Reference Model term, Date Format, Unit of Time) and Mapping Rules already specified (about mapping of values with Confirmation Terms).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Medical-Imaging-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutAssess = data.get(0); // Medical Imaging Test Outcome: Normal/Abnormal
		final String eventDateStr = data.get(1); // Event Date
		final String elapseTimeStr = data.get(2); // Elapse Time

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String eventDateFormat = getArgValue(ServiceArg.Event_Date_Format);
		final String elapseTimeUnit = getArgValue(ServiceArg.Elapse_Time_Unit);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri); 
		final Tuple2<TermExpr, Relation> testOutAssessTermExprT = getReferenceModelTermForCohortValue(testOutAssess, 0);
		
		// IF the value is empty or not-aligned with Assessment Terms, no Data will be recorded
		if (testOutAssessTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is an Assessment Term
		if (!testOutAssessTermExprT._1().isSimpleAssessmentTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testOutAssessTermExprT._1().asString() + "\" is NOT an Assessment Term !");
		
		final Tuple3<Integer, Integer, Integer> eventDateTuple = getDateTuple(eventDateStr, eventDateFormat);
		final Integer elapseTimeInt = getInteger(elapseTimeStr);
		final Tuple2<Integer, String> timeElapseTuple = (elapseTimeInt != null) ? Tuple2.newTuple2(elapseTimeInt, elapseTimeUnit) : null;
		// Provides the Date the test took place, provided that both the Event Date and the Time Elapse specified, with the necessary level of Details
		final Tuple3<Integer, Integer, Integer> testDateTuple = getDateTupleAfterTime(eventDateTuple, timeElapseTuple);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = (testDateTuple != null) ? new DateValue(testDateTuple): null;
		final PropertyValue propValue2 = new SimpleCodedValue(testOutAssessTermExprT);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		
		// The List should often contain only one Map (so that only one instance will be created)
		return newPosTuple(map);
	}

}
