package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Field that indicates whether a specific Lab Test Outcome Term was observed (e.g., specific ANA pattern or Cryo Type) or not. The corresponding Reference Model term should be given. Also, the values of the field should be independently ** aligned with ** Confirmation terms (i.e., YES/NO values) specified in the Reference Model. */
public class LabTestOutcomeTermBoolean extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test }
	
	/** Provides the appropriate Terms for the specific Lab Test along with its Outcome, based on the Value of the given Field, the parameters provided (i.e., corresponding Reference Model term) and the Mapping Rules specified, provided that the value of the field is not empty. More precisely, IF the value is yes (i.e., aligned with the YES confirmation term) the service indicates that the specific outcome was observed, otherwise not.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String outcomeTermConfirmValue = data.get(0); // Lab Test Outcome Term: Boolean Value

		// Additional Data / Arguments (if any)
		final String outcomeTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass outcomeTerm = getReferenceModelTermWithURI(outcomeTermUri);
		
		final OntClass testTerm;
		if (outcomeTerm.getSuperClass().getLocalName().equals("Antinuclear-Antibody-Pattern")) {
			testTerm = getReferenceModelTermWithURI(LabTestOutcomeTermBooleanPlusDate.anaPatternTestUri);
		} else if (outcomeTerm.getSuperClass().getLocalName().equals("Cryoglubin-Type")) {
			testTerm = getReferenceModelTermWithURI(LabTestOutcomeTermBooleanPlusDate.cryoTypeTestUri);
		} else {
			throw new RuntimeException("There is no Reference Model Test with outcome the given Term: \"" + outcomeTerm.getLocalName() + "\"");
		}
		
		final Tuple2<TermExpr, Relation> outcomeConfirmValueTermExprT = getReferenceModelTermForCohortValue(outcomeTermConfirmValue, 0);
		
		// IF the value is empty, no data will be recorded
		if (outcomeConfirmValueTermExprT == null) return null;

		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!outcomeConfirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + outcomeConfirmValueTermExprT._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm); 
		final PropertyValue propValue1 = new SimpleCodedValue(outcomeTerm); 

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		
		// Depending on the value of the Test Outcome Field, return a positive/negative "statement"
		if (outcomeConfirmValueTermExprT._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
	
	}

}
