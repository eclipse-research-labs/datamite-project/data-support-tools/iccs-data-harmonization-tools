package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Field with the value measured for a specific Lab Test. However, about this Lab Test we are only interested whether the outcome was Normal or not. The corresponding Reference Model Lab Test term should be specified. Also, the Format of the number existing in the Field should be given, along with the Normal Range of Values. */
public class LabTestOutcomeAssessmentBasedOnNumber extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Field_Number_Format, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL }
	
	/** Provides the appropriate Term for the specific Lab Test and the Outcome Assessment based on the Value of the given Field and the parameters provided (i.e., corresponding Reference Model Lab Test, Number Format and Normal Range of Values).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testNumericValueStr = data.get(0); // Lab Test Outcome: Numeric Value

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String valueFormatIndex = getArgValue(ServiceArg.Field_Number_Format);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);

		// Ensure that the value of the given Test can be an Assessment
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.ASSESSMENT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Assessment ! Possible Test Outcome(s): " + expOutSet);
		
		final Float num = getRealNumber(testNumericValueStr, valueFormatIndex);
		
		// IF value is empty or not a Real Number, no data will be recorded
		if (num == null) return null;
		
		final Float downNormLimit = getRealNumber(downNormLimitStr, "DOT");
		final Float upNormLimit = getRealNumber(upNormLimitStr, "DOT");
		
		final Term assessTerm = (downNormLimit != null || upNormLimit != null) ? getAssessmentForValue(num, downNormLimit, upNormLimit) : null;
		
		// IF we cannot Find the assessment based on the given data (i.e., neither DNL and UNL specified), no data will be recorded
		if (assessTerm == null) return null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = new SimpleCodedValue(assessTerm);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		
		return newPosTuple(map);
	}

}