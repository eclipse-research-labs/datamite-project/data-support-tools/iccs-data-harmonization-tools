package ntua.iccs.harmonicss.cohort.trans.quest;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 3 fields: A) A Field with the ESSDAI Domain Activity Level and B) Two other Fields that indicate the Date when the Lab Test took place. From these two Fields, one indicates the date an Event took place and the other one the Time (i.e., years/months/days) elapse since that date, when the test took place. The corresponding Reference Model term should be specified. The values of this field should be independently ** aligned with ** the Activity Level terms (i.e., NO, LOW, MODERATE, HIGH) specified in the Reference Model. Additionally the format of the Date (2nd field) along with the Unit of Time Elapse (3rd field) should be given. */
public class ESSDAIDomainActivityLevelPlusEventDateAndTimeElapse extends DataTransformation {

	private enum ServiceArg { ESSDAI_Domain, Event_Date_Format, Elapse_Time_Unit }
	
	/** Provides the values of the parameters specified in the right side taking into account the patient data in the given fields as well as additional parameters provided.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#ESSDAI-Domain-AL that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String actLevStr = data.get(0); // ESSDAI Domain Activity Level
		final String eventDateStr = data.get(1); // Event Date
		final String elapseTimeStr = data.get(2); // Elapse Time

		// Additional Data / Arguments (if any)
		final String essdaiDomainUri = getArgValue(ServiceArg.ESSDAI_Domain);
		final String eventDateFormat = getArgValue(ServiceArg.Event_Date_Format);
		final String elapseTimeUnit = getArgValue(ServiceArg.Elapse_Time_Unit);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass essdaiDomainTerm = getReferenceModelTermWithURI(essdaiDomainUri); 
		final Tuple2<TermExpr, Relation> actLevTermExprT = getReferenceModelTermForCohortValue(actLevStr, 0);
		
		// IF the value is empty or not aligned with Reference Model terms, no data will be recorded
		if (actLevTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is an Activity Level Term
		if (!actLevTermExprT._1().isSimpleActivityLevelTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + actLevTermExprT._1().asString() + "\" is NOT an Activity Level Term !");
		
		final Tuple3<Integer, Integer, Integer> eventDateTuple = getDateTuple(eventDateStr, eventDateFormat);
		final Integer elapseTimeInt = getInteger(elapseTimeStr);
		final Tuple2<Integer, String> timeElapseTuple = (elapseTimeInt != null) ? Tuple2.newTuple2(elapseTimeInt, elapseTimeUnit) : null;
		// Provides the Date the test took place, provided that both the Event Date and the Time Elapse specified, with the necessary level of Details
		final Tuple3<Integer, Integer, Integer> testDateTuple = getDateTupleAfterTime(eventDateTuple, timeElapseTuple);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(essdaiDomainTerm);
		final PropertyValue propValue1 = new SimpleCodedValue(actLevTermExprT);
		final PropertyValue propValue2 = 
			(testDateTuple != null) ? new DateValue(testDateTuple): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#quest-ESSDAI-Domain-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#activity-Level-CV
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#completion-Date
		
		return newPosTuple(map);
	}

}
