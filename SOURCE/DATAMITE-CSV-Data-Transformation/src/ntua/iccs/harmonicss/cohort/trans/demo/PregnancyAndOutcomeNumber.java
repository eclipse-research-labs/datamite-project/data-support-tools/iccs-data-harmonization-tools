package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Cohort Fields, a) a Field about the number of pregnancies and b) another Field about the number of times a specific pregnancy outcome (e.g., number of abortions) was mentioned. The corresponding Reference Model term (pregnancy outcome) should be given. */
public class PregnancyAndOutcomeNumber extends DataTransformation {

	private static final Logger log = LogFactory.getLoggerForClass(PregnancyAndOutcomeNumber.class);
	
	private enum ServiceArg { Reference_Model_Pregnancy_Outcome }
	
	/** Provides a List of Entities (based on the value of the 1st Field) about Pregnancies took place, provided that the value of at least one of these two Fields is not empty and their values are valid (i.e., 1st Field Value >/= 2nd Field Value). For some of them (based on the value of the 2nd Field), the system also provides the specific Pregnancy outcome recorded. In case the value of the 1st Field (and hence 2nd Field) is zero, the system returns that the woman was not Pregnant. However, if the 1st Field is empty and only the 2nd Field is zero, the system returns that the specific Pregnancy Outcome was not observed (i.e., the woman was not Pregnant that ended up with the given Pregnancy Outcome).  
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Pregnancy that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String pregNumStr = data.get(0); // Pregnancy (Integer)
		final String pregOutNumStr = data.get(1); // Pregnancy Outcome (Integer)

		// Additional Data / Arguments (if any)
		final String outTermUri = getArgValue(ServiceArg.Reference_Model_Pregnancy_Outcome);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass outTerm = getReferenceModelTermWithURI(outTermUri);
		
		final Integer pregNum = getInteger(pregNumStr);
		final Integer pregOutNum = getInteger(pregOutNumStr);
		
		// IF both Fields are empty or not an Integer, no data will be recorded
		if ((pregNum == null || pregNum < 0) && (pregOutNum == null || pregOutNum < 0)) return null;
		
		// Check that the two values (if both of them provided) are valid, otherwise no data will be recorded
		if (pregNum != null && pregOutNum !=null) {
			if (pregOutNum > pregNum) {
				log.warn("Number of Pregnancies: " + pregNum + " < Number of Pregnancies Outcome: " + pregOutNum + ". Data is being ignored !");
				return null;
			}
		}
		
		if (pregNum != null && pregOutNum ==null) {
			// Only the Pregnancies Number is available
			if (pregNum == 0) {
				// Not Pregnant 
				final Map<Integer, PropertyValue> map = new HashMap<>();
				final PropertyValue propValue0 = null;
				map.put(0, propValue0);
				return newNegTuple(map);
			} else {
				// Pregnant several time but for all of the the outcome was not available
				List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
				for (int i = 0; i < pregNum; i++) {
					final Map<Integer, PropertyValue> map = new HashMap<>();
					final PropertyValue propValue0 = null;
					map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#pregnancy-Outcome-CV
					mapList.add(map);
				}
				return Tuple2.newTuple2(Boolean.TRUE, mapList);
			}
			
		} else if (pregNum == null && pregOutNum !=null) {
			// Only the Pregnancies Outcome Number is available
			if (pregOutNum == 0) {
				// Not Pregnant that ended up with the Given Outcome
				final Map<Integer, PropertyValue> map = new HashMap<>();
				final PropertyValue propValue0 = new SimpleCodedValue(outTerm);
				map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#pregnancy-Outcome-CV
				return newNegTuple(map);
			} else {
				// Pregnant several time but for all of the the outcome was the given one
				List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
				for (int i = 0; i < pregOutNum; i++) {
					final Map<Integer, PropertyValue> map = new HashMap<>();
					final PropertyValue propValue0 = new SimpleCodedValue(outTerm);
					map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#pregnancy-Outcome-CV
					mapList.add(map);
				}
				return Tuple2.newTuple2(Boolean.TRUE, mapList);
			}
		} else {
			// Both Pregnancies and Outcome Number are available
			if (pregNum == 0) {
				// Not Pregnant 
				final Map<Integer, PropertyValue> map = new HashMap<>();
				final PropertyValue propValue0 = null;
				map.put(0, propValue0);
				return newNegTuple(map);
			} else {
				// Pregnant several time but for some of the the outcome was the given one
				List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
				for (int i = 0; i < pregNum; i++) {
					final Map<Integer, PropertyValue> map = new HashMap<>();
					final PropertyValue propValue0 = (i < pregOutNum) ? new SimpleCodedValue(outTerm) : null;
					map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#pregnancy-Outcome-CV
					mapList.add(map);
				}
				return Tuple2.newTuple2(Boolean.TRUE, mapList);
			}
		}
		
	}

}
