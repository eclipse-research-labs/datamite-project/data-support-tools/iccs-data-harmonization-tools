package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.BooleanValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 2 fields: A) A Field about a Biopsy Test with a Boolean Value (i.e., YES/NO) and B) Another Field that indicates the date when the Biopsy Test took place. The corresponding Reference Model term should be given. Also, the values of the 1st field should be independently ** aligned with ** Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Additionally the format of the Date (2nd field) should be given. */
public class BiopsyTestOutcomeBooleanPlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Biopsy_Test, Date_Format, Reference_Model_Biopsy_Type }
	
	/** Provides the appropriate Terms for the specific biopsy Test (including type - Salivary Gland) along with its Outcome and the Date it took place, based on the Value of the given Fields, the parameters provided (i.e., corresponding Reference Model terms and date format) and the Mapping Rules already specified. In case the 1st Field is empty or not aligned with the Confirmation terms, no data will be recorded. Also, in case the 2nd Field is empty, no information about the Test Date will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Biopsy that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testConfirmValue = data.get(0); // Biopsy Test Outcome: Boolean Value
		final String biopsyDate = data.get(1); // Biopsy Test Date

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Biopsy_Test);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		final String salglandTermUri = getArgValue(ServiceArg.Reference_Model_Biopsy_Type);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test is a Boolean 
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.BOOLEAN))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not a Boolean ! Possible Test Outcome(s): " + expOutSet);
		
		final OntClass salGlandTerm = getReferenceModelTermWithURI(salglandTermUri);
		final Tuple2<TermExpr, Relation> testConfirmValueTermExprT = getReferenceModelTermForCohortValue(testConfirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, it will be ignored.
		if (testConfirmValueTermExprT == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!testConfirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testConfirmValueTermExprT._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(biopsyDate, dateFormat);		
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = new BooleanValue(testConfirmValueTermExprT);
		final PropertyValue propValue2 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue3 = new SimpleCodedValue(salGlandTerm);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#biopsy-Type-CV
		
		return newPosTuple(map);
	}

}
