package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 3 fields: A) A Field about a Biopsy Test with a Numeric Value , B) Another Field about the Salivary Gland examined and C) A Field that indicates the date when the Biopsy took place. The corresponding Reference Model term should be given . Also, the Format of the number existing in the 1st Field and the Unit of Measurement should be given. Moreover, the values of the 2nd field should be independently ** aligned with ** Salivary Gland terms (e.g., Minor Salivary Gland) specified in the Reference Model. Additionally the format of the Date (3rd field) should be given. */
public class BiopsyTestOutcomeNumericPlusSiteAndDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Biopsy_Test, Field_Number_Format, Field_Unit_of_Measurement, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, Date_Format }
	
	/** Provides the appropriate Terms for the specific biopsy Test (including type - Salivary Gland) along with its Outcome and the Date it took place, based on the Value of the given Fields, the parameters provided (i.e., corresponding Reference Model terms and date/number format, unit of measurement and optionally the normal range of values) and the Mapping Rules already specified. In case the 1st Field is empty or not aligned with the Confirmation terms, no data will be recorded. Also, in case the 2nd Field is empty or the values are not aligned with Salivary Gland terms, no information about the Biopsy Site will be recorded. Moreover, in case the 3rd Field is empty, no information about the Biopsy Date will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Biopsy that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testNumericValue = data.get(0); // Biopsy Test with Outcome: Numeric Value
		final String biopsySiteValue = data.get(1); // Biopsy Site
		final String biopsyDate = data.get(2); // Biopsy Date

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Biopsy_Test);
		final String valueFormatIndex = getArgValue(ServiceArg.Field_Number_Format);
		final String testValueUnit = getArgValue(ServiceArg.Field_Unit_of_Measurement);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test can be an Amount
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.AMOUNT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Amount ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple2<Float, Tuple2<String, String>> valueTuple = getAmountValueTuple(testNumericValue, valueFormatIndex, testValueUnit, testTerm);
		
		// IF the value is empty or not a Number, it will be ignored.
		if (valueTuple == null) return null;
		
		final Tuple2<TermExpr, Relation> salivGlandTermExprT = getReferenceModelTermForCohortValue(biopsySiteValue, 1);
		
		// Ensure that the Corresponding Biopsy Site term is a Salivary Gland (Biopsy) Term
		if (salivGlandTermExprT != null && !salivGlandTermExprT._1().isSalivaryGlandBiopsy()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + salivGlandTermExprT._1().asString() + "\" is NOT a Salivary Gland (Biopsy) Term !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(biopsyDate, dateFormat);
		
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, testValueUnit, testTerm);
		final Term assessTerm = (normRangeTuple != null) ? getAssessmentForValue(valueTuple._1(), normRangeTuple._1(), normRangeTuple._2()) : null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0;
		if (salivGlandTermExprT != null) {
			propValue0 = (salivGlandTermExprT._1().isTerm()) ? new SimpleCodedValue(salivGlandTermExprT) : new ComplexCodedValue(salivGlandTermExprT);
		} else {
			propValue0 = null;
		}
		
		final PropertyValue propValue1 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue2 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue3 = new AmountValue(valueTuple);
		final PropertyValue propValue4 = (assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;
		final PropertyValue propValue5 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;
			
		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#biopsy-Type-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(4, propValue4); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(5, propValue5); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}