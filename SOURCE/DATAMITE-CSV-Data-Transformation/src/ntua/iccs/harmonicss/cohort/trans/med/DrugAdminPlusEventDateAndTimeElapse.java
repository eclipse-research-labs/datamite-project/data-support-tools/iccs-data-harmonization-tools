package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 3 fields: A) A Field that indicates whether the person has received a specific Drug or not and B) Two other Fields that indicate the Date when the Lab Test took place. From these two Fields, one indicates the date an Event took place and the other one the Time (i.e., years/months/days) elapse since that date, when the test took place. The corresponding Reference Model term should be specified. Also, the values of the 1st Field should be independently ** aligned with ** the Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Additionally the format of the Date (2nd field) along with the Unit of Time Elapse (3rd field) should be given. */
public class DrugAdminPlusEventDateAndTimeElapse extends DataTransformation {

	private enum ServiceArg { Reference_Model_DrugSubstance, Event_Date_Format, Elapse_Time_Unit }
	
	/** Provides the corresponding Reference Model term (i.e., Medical Condition) along with the Period of Time (i.e., the one before a specific date) based on the given Data (i.e., Fields values, Additional Data and Mapping Rules), provided that the value existing in the 1st field indicates that the person has received the specific Drug.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Medication that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String drugConfirmValue = data.get(0); // Drug Field: YES/NO
		final String eventDateStr = data.get(1); // Event Date
		final String elapseTimeStr = data.get(2); // Elapse Time

		// Additional Data / Arguments (if any)
		final String drugTermUri = getArgValue(ServiceArg.Reference_Model_DrugSubstance);
		final String eventDateFormat = getArgValue(ServiceArg.Event_Date_Format);
		final String elapseTimeUnit = getArgValue(ServiceArg.Elapse_Time_Unit);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass drugTerm = getReferenceModelTermWithURI(drugTermUri);
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(drugConfirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, it will be ignored.
		if (confirmValue == null) return null;
		
		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Integer, Integer, Integer> eventDateTuple = getDateTuple(eventDateStr, eventDateFormat);
		final Integer elapseTimeInt = getInteger(elapseTimeStr);
		final Tuple2<Integer, String> timeElapseTuple = (elapseTimeInt != null) ? Tuple2.newTuple2(elapseTimeInt, elapseTimeUnit) : null;
		// Provides the Date the test took place, provided that both the Event Date and the Time Elapse specified, with the necessary level of Details
		final Tuple3<Integer, Integer, Integer> drugadminDateTuple = getDateTupleAfterTime(eventDateTuple, timeElapseTuple);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 =  new SimpleCodedValue(drugTerm);
		final PropertyValue propValue1 = 
			(drugadminDateTuple != null) ? new DateIntervalTypeA(null, new DateValue(drugadminDateTuple)): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Date-Interval
		
		// Depending on the value of the Drug Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
	}

}
