package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import net.sf.json.JSONArray;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Field that indicates whether the person has received Chemotherapy or not. In this Field we may encounter more than one comma separated terms. The values of the given Field that indicate that the person has received a Chemotherapy should be specified (in the form of a JSON array). */
public class ChemotherapyTerm extends DataTransformation {

	private enum ServiceArg { Chemotherapy_Terms_JA }
	
	/** Provides an Empty Map for indicating that we should create an instance but this instance won't contain any property, provided that we may encounter one of the given Chemotherapy Terms.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Chemotherapy that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String termsStr = data.get(0); // Field with one or more Comma-separated Terms

		// Additional Data / Arguments (if any)
		final String chemoJAStr = getArgValue(ServiceArg.Chemotherapy_Terms_JA);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final JSONArray chemoJA = JSONArray.fromObject(chemoJAStr);
		
		// Data Processing
		final List<String> termList = getTermsStrList(termsStr);
				
		// IF field is empty
		if (termList == null) return null;
		
		// Check about Chemotherapy term(s)
		boolean chemoTermExists = false;
		for (String term : termList) {
			if (chemoJA.contains(term)) {
				chemoTermExists = true; break;
			}
		}
		
		// IF no Chemotherapy Term found, return null so that no Chemotherapy instance created
		if (!chemoTermExists) return null;

		// Return an Empty Map for indicating that we should create a Chemotherapy
		final Map<Integer, PropertyValue> map = new HashMap<>();
		return newPosTuple(map);
	}

}
