package ntua.iccs.harmonicss.cohort.trans;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.VOCNS;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.UnitUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.testOutcomeUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.testOutcomeUnitUri;
import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.unitExpressionUri;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Throable;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.json.TermsMappingRule;
import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;

public abstract class DataTransformation implements DataTransformationI {

	public static final Logger log = LogFactory.getLoggerForClass(DataTransformation.class);
	
	protected static final DecimalFormat df = new DecimalFormat ("#.000000");
	
	public static enum TestOutcome { AMOUNT, BOOLEAN, TERM, ASSESSMENT }
	
	/* Common for ALL Objects of this Class */
	
	/** The HarmonicSS Reference Model */
	protected static OntModel refModel;
	
	public static void setRefModel(OntModel ontoModel) {
		refModel = ontoModel;
	}
	
	/** The Mapping Rules specified for the Cohort Set of Terms */
	protected static List<TermsMappingRule> termMapList;
	
	public static void setTermsMR(final List<TermsMappingRule> maprules) {
		termMapList = maprules;
	}
	
	/* Different for each Object of this Class */
	
	/** Additional Arguments provided */
	protected final Map<String, String> args = new HashMap<String, String>();
	
	/** The Range of Values of Cohort Fields (if any) */
	protected final Map<Integer, String> paramValueRangeMap = new HashMap<Integer, String>();
	
	public DataTransformation() {
		
	}
	
	/* ***************** About Arguments ***************** */
	
	@Override
	public void setArgs(Map<String, String> args) {
		this.args.putAll(args);
	}
	
	public Map<String, String> getArgs() {
		return args;
	}

	/** @return The value of the argument with the given name */
	protected String getArgValue(Enum<?> arg) {
		return this.args.get(arg.name());
	}
	
	/** A Set with the names of elements specified in the given enum */
	protected Set<String> getEnumNameSet(Class<? extends Enum<?>> enumClass) {
		final Set<String> set = new HashSet<>();
		for (Enum<?> enumConstant : enumClass.getEnumConstants()) {
			set.add( enumConstant.name() );
		}
		return set;
	}
	
	/** Compare given Arguments with expected */
	protected void compareGivenArgsWithExpected(final Set<String> expArgSet) {
		// Arguments specified in the Mapping Rule but NOT used by the DT service
		final List<String> unusedGivenArgsList = new ArrayList<>();
		final Set<String> givArgSet = this.args.keySet();
		for (String givArg : givArgSet) {
			if (!expArgSet.contains(givArg)) {
				unusedGivenArgsList.add(givArg);
			}
		}
		// Arguments expected by the DT service but not provided
		final List<String> unspecExpectArgsList = new ArrayList<>();
		for (String expArg : expArgSet) {
			if (!givArgSet.contains(expArg)) {
				unspecExpectArgsList.add(expArg);
			}
		}
		
		if (!unusedGivenArgsList.isEmpty())
			throw new RuntimeException("MR: Arguments NOT used: " + unusedGivenArgsList + " DT service: Arguments NOT provided: " + unspecExpectArgsList);
	}
	
	/* ******** About Range of Input Fields / Properties Values ******** */
	
	@Override
	public void setValueRange(Map<Integer, String> paramValueRangeMap) {
		this.paramValueRangeMap.putAll(paramValueRangeMap);
	}

	public Map<Integer, String> getParamValueRangeMap() {
		return paramValueRangeMap;
	}

	/** @return <code>true</code> if the given String is NULL or EMPTY (e.g., contains one or more spaces), otherwise <code>false</code>*/
	protected boolean isEmpty(final String str) {
		if (str == null) return true;
		if (str.trim().equals("")) return true;
		return false;
	}
	
	/** @return A Tuple with a TRUE value and a List with only one element, i.e., the given Map */
	protected Tuple2<Boolean, List<Map<Integer, PropertyValue>>> newPosTuple(final Map<Integer, PropertyValue> map) {
		final List<Map<Integer, PropertyValue>> list = new ArrayList<>();
		list.add(map);
		return Tuple2.newTuple2(Boolean.TRUE, list);
	}
	
	/** @return A Tuple with a FALSE value and a List with only one element, i.e., the given Map */
	protected Tuple2<Boolean, List<Map<Integer, PropertyValue>>> newNegTuple(final Map<Integer, PropertyValue> map) {
		final List<Map<Integer, PropertyValue>> list = new ArrayList<>();
		list.add(map);
		return Tuple2.newTuple2(Boolean.FALSE, list);
	}
	
	/** @return The Reference Model Term (OWL class) with the given URI.
	 *  @throws RuntimeException in case none OWL class found. */
	protected OntClass getReferenceModelTermWithURI(final String termUri) {
		if (isEmpty(termUri)) throw new RuntimeException("The given string \"termUri\" is NULL or empty !");
		final OntClass term = refModel.getOntClass(termUri);
		if (term == null) throw new RuntimeException("Cannot find Reference Model term with URI: " + termUri);
		return term;
	}	
	
	/** @return A Tuple with the corresponding Reference Model term for the given Field Value along with the Hierarchical 
	 *  Relationship with the given term, taking into account the Mapping Rules already specified for the values of this Field. */
	protected Tuple2<TermExpr, Relation> getReferenceModelTermForCohortValue(final String localValue, final int fieldIndex) {
		if (!paramValueRangeMap.containsKey(fieldIndex))
			throw new RuntimeException("The values of the Field \"" + fieldIndex + "\" do not come from a controlled set of terms !");
		final String rangeUri = paramValueRangeMap.get(fieldIndex);
		// Check NULL or Empty
		if (localValue == null || localValue.trim().equals("")) return null;
		// Find the corresponding Reference Model term (if any)
		for (TermsMappingRule mr : termMapList) {
			if (mr.getSrcTermExpr() instanceof Term) {
				final Term term = (Term) mr.getSrcTermExpr();
				if (term.getUri().startsWith(rangeUri) && term.getLabel().equalsIgnoreCase(localValue)) 
					return Tuple2.newTuple2(mr.getTrgTermExpr(), mr.getRelation());
			}
		}
		return null;
	}
	
	/** @return An Integer or a Real Number (Float) */
	protected Object getNumber(final String str) {
		if (isEmpty(str)) return null;
		if (str.indexOf('.') > 0 || str.indexOf(',') > 0) {
			return getFloat(str);
		} else {
			return getInteger(str);
		}
	}
	
	/** @return An Integer based on the given String */
	protected Integer getInteger(final String str) {
		if (isEmpty(str)) return null;
		try {
			return Integer.parseInt(str);
		} catch (Throwable t) {
			log.debug("getInteger(): Data \"" + str + "\" being ignored due to: " + Throable.throwableAsSimpleOneLineString(t));
			return null;
		}
	}
	
	/** @return A Float based on the given String */
	protected Float getFloat(final String str) {
		if (isEmpty(str)) return null;
		try {
			return Float.parseFloat(str.replaceAll(",","\\."));
		} catch (Throwable t) {
			log.debug("getFloat(): Data \"" + str + "\" being ignored due to: " + Throable.throwableAsSimpleOneLineString(t));
			return null;
		}
	}
	
//	/** @return A Tuple with Down and Up Normal Limits */
//	protected Tuple2<Float, Float> getFloatNormalRangeTuple(final String downNormLimitStr, final String upNormLimitStr) {
//		if (isEmpty(downNormLimitStr) && isEmpty(upNormLimitStr)) return null;
//		try {
//			final Float downNormLimit = getFloat(downNormLimitStr);
//			final Float upNormLimit = getFloat(upNormLimitStr);
//			return Tuple2.newTuple2(downNormLimit, upNormLimit);
//		} catch (Throwable t) {
//			log.debug("getIntegerNormalRange(): Data [ \"" + downNormLimitStr + "\" - \"" + upNormLimitStr + "\" ] being ignored due to: " + Throable.throwableAsSimpleOneLineString(t));
//			return null;
//		}
//	}
	
	/** @return A Tuple with Down and Up Normal Limits */
	protected Tuple2<Integer, Integer> getIntegerNormalRangeTuple(final String downNormLimitStr, final String upNormLimitStr) {
		if (isEmpty(downNormLimitStr) && isEmpty(upNormLimitStr)) return null;
		try {
			final Integer downNormLimit = (!isEmpty(downNormLimitStr)) ? Integer.parseInt(downNormLimitStr) : null;
			final Integer upNormLimit = (!isEmpty(upNormLimitStr)) ? Integer.parseInt(upNormLimitStr) : null;
			return Tuple2.newTuple2(downNormLimit, upNormLimit);
		} catch (Throwable t) {
			log.debug("getIntegerNormalRange(): Data [ \"" + downNormLimitStr + "\" - \"" + upNormLimitStr + "\" ] being ignored due to: " + Throable.throwableAsSimpleOneLineString(t));
			return null;
		}	
	}	
	
	// User-defined (i.e., non SimpleDateFormat) Date Formats
	private static final String dfYearOpMonthOpDay = "yyyy/(MM)/(dd)";
	private static final String dfOpDayOpMonthYear = "(dd)/(MM)/yyyy";
	
	/** @return A Tuple with Year, Month, Day provided that the given value is not empty and follows the given Simple Date Format */
	protected Tuple3<Integer, Integer, Integer> getDateTuple(final String dateStr, final String dateFormatGiven) {
		if (isEmpty(dateStr)) return null;
		// Deal with user-defined Date Formats
		final String dateFormat;
		if (dfYearOpMonthOpDay.equals(dateFormatGiven)) {
			final int slashCount = getSlashCount(dateStr);
			if (slashCount == 0) {
				dateFormat = "yyyy";
			} else if (slashCount == 1) {
				dateFormat = "yyyy/MM";
			} else {
				dateFormat = "yyyy/MM/dd";
			}
		} else if (dfOpDayOpMonthYear.equals(dateFormatGiven)) {
			final int slashCount = getSlashCount(dateStr);
			if (slashCount == 0) {
				dateFormat = "yyyy";
			} else if (slashCount == 1) {
				dateFormat = "MM/yyyy";
			} else {
				dateFormat = "dd/MM/yyyy";
			}
		} else {
			dateFormat = dateFormatGiven;
		}
		// Parse string using Java 
		try {
			final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			sdf.setLenient(false) ;
			final Date date = sdf.parse(dateStr);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			final int year = calendar.get(Calendar.YEAR);
			if (("" + year).length() != 4) {
				log.warn("The year detected (i.e., " + year + ") is NOT a 4-digit number. Given-Date: " + dateStr + " , Given-Date-Format: " + dateFormatGiven);
				return null;
			}
			final int month = 
				(dateFormat.indexOf('m') >= 0 || dateFormat.indexOf('M') >= 0) 
					? calendar.get(Calendar.MONTH) + 1 /* JANUARY --> 0 */ : -1;
			final int day = 
				(dateFormat.indexOf('d') >= 0 || dateFormat.indexOf('D') >= 0)  
					? calendar.get(Calendar.DAY_OF_MONTH) : -1;
			return Tuple3.tuple(year, month, day);
		} catch (Throwable t) {
			return null;
		}
	}
	
	/** @return Number of slash (/) characters */
	private int getSlashCount(final String dateStr) {
		if (dateStr == null || isEmpty(dateStr)) return -1;
		int slashCount = 0;
		for (int i = 0; i < dateStr.length(); i++) {
		    if (dateStr.charAt(i) == '/') {
		    	slashCount++;
		    }
		}
		return slashCount;
	}
	
	private static enum TimeUnit { YEAR, MONTH, DAY };
	
	/** @return The Date before/after the given one based on the time elapse, provided that both the Event Date and the Time Elapse specified, with the necessary level of Details */
	protected Tuple3<Integer, Integer, Integer> getDateTupleAfterTime(final Tuple3<Integer, Integer, Integer> dateTuple, final Tuple2<Integer, String> timeElapseTuple) {
		if (dateTuple == null || timeElapseTuple == null) return null;
		// Find Unit of Time
		final TimeUnit timeUnit = getTimeUnit(timeElapseTuple._2());
		if (timeUnit == null) throw new RuntimeException("Unit of Time is none of: Year/Month/Day");
		
		if (timeUnit == TimeUnit.YEAR) {
			return Tuple3.tuple(dateTuple._1() + timeElapseTuple._1(), dateTuple._2(), dateTuple._3());
		} else if (timeUnit == TimeUnit.MONTH) {
			if (dateTuple._2() > 0) {
				// TODO: FIX when exceed 12
				return Tuple3.tuple(dateTuple._1(), dateTuple._2() + timeElapseTuple._1(), dateTuple._3());
			} else {
				throw new RuntimeException("There is no Information about the MONTH the Event Took place.");
			}
		} else if (timeUnit == TimeUnit.DAY) {
			if (dateTuple._2() > 0) {
				// TODO: FIX when exceed 30/31
				return Tuple3.tuple(dateTuple._1(), dateTuple._2(), dateTuple._3() + timeElapseTuple._1());
			} else {
				throw new RuntimeException("There is no Information about the DAY the Event Took place.");
			}
		}
		return null;
	}
	
	private static TimeUnit getTimeUnit(final String str) {
		for (TimeUnit tm : TimeUnit.values()) {
			if (tm.name().equalsIgnoreCase(str)) return tm;
		}
		return null;
	}
	
	/** @return <code>true</code> provided that the start Date is before the end Date, otherwise <code>false</code> */
	protected boolean isDateIntervalValid(final Tuple3<Integer, Integer, Integer> start, final Tuple3<Integer, Integer, Integer> end) {
		if (start == null || end == null) return true;
		return ( 
			(start._1() <= end._1()) && 
			(start._2() <= end._2()) && 
			(start._3() <= end._3())
		);
	}
	
	/** A Tuple with the given Value (i.e., the first token of the given String) expressed in the Unit of Measurements specified for this Lab Test (provided that the UNIT specified) */
	protected Tuple2<Float, Tuple2<String, String>> getAmountValueTuple(String amountStr, String tokenFormatIndex, OntClass testTerm) {
		if (isEmpty(amountStr)) return null;
		final int spaceIndex = amountStr.indexOf(' ');
		if (spaceIndex < 0) {
			log.debug("The Field contains only one token: " + amountStr + " (expected two: value and unit) - Data being ignored !");
			return null;
		}
		final String value = amountStr.substring(0, spaceIndex).trim();
		final String unit = amountStr.substring(spaceIndex + 1).trim();
		return getAmountValueTuple(value, tokenFormatIndex, unit, testTerm);
	}
	
	protected static enum NumFormat {DOT, COMMA, FRACTION }
	
	/** @return A Tuple with the given Value expressed in the Unit of Measurements specified for this Lab Test (provided that the UNIT specified) */
	protected Tuple2<Float, Tuple2<String, String>> getAmountValueTuple(final String value, final String valueFormatIndex, final String unit, final OntClass testTerm) {
		if (isEmpty(value) || isEmpty(valueFormatIndex) || isEmpty(unit)) return null;
		
		// Check Value Format
		if (!valueFormatIndex.equalsIgnoreCase(NumFormat.DOT.name()) 
			&& !valueFormatIndex.equalsIgnoreCase(NumFormat.COMMA.name()) 
			&& !valueFormatIndex.equalsIgnoreCase(NumFormat.FRACTION.name()))
				throw new RuntimeException("Not supported Format: " + valueFormatIndex + " (valueFormatIndex)");
		
		// Find the Unit of measurement specified for this Test
		final Tuple2<OntClass, String> unitTuple = getTestUnit(refModel, testTerm);
		
		// TODO: CHECK 
		// Continue, provide that the Reference Model - Unit of Measurement found for the given term
		// NOTE: "-" this used for those Lab Test with actually a number (e.g., titer)
		if (unitTuple == null && !unit.trim().equals("-")) return null;
		
		// Parse given Data
		final float existingValue;
		try {
			if (valueFormatIndex.equalsIgnoreCase(NumFormat.DOT.name())) {
				existingValue = Float.parseFloat(value.replaceAll(",","\\.")); // Normally, the replace statement should not exist
			} else if (valueFormatIndex.equalsIgnoreCase(NumFormat.COMMA.name())) {
				existingValue = Float.parseFloat(value.replaceAll(",","\\."));
			} else if (valueFormatIndex.equalsIgnoreCase(NumFormat.FRACTION.name())) {
				if (value.equals("0")) {
					existingValue = 0f;
				} else {
					final int slashIndex = value.indexOf('/');
					final int semicolonIndex = value.indexOf(':');
					final String str = (slashIndex < 0 && semicolonIndex > 0) ? value.replaceAll(":","/") : value;
					int index = str.indexOf('/'); 
					if (index <= 0) throw new RuntimeException("Cannot find slash (/) in the given string (fraction): " + str);
					existingValue = (Float) Float.parseFloat(str.substring(0, index)) / Float.parseFloat(str.substring(index + 1));
				}
			} else {
				return null;
			}
		} catch (Throwable t) {
			log.debug("Data \"" + value + "\" being ignored due to: " +  Throable.throwableAsSimpleOneLineString(t));
			return null;
		}
		
		final Float newValue;
		if (valueFormatIndex.equalsIgnoreCase(NumFormat.FRACTION.name())) {
			newValue = existingValue;
		} else {
			// Transform UNL/DNL values taking into account the existing/given Unit of measurement
			newValue = (unitTuple != null) 
				? AmountsValueTransformer.expressValueInTargetUnit(existingValue, unit, unitTuple._2()) : existingValue;
		}
		
		if (unitTuple != null) {
			return Tuple2.newTuple2(newValue, Tuple2.newTuple2(unitTuple._1().getLocalName(), unitTuple._2()));
		} else {
			return Tuple2.newTuple2(newValue, Tuple2.newTuple2("UNITEXP-19", "#"));
		}
	}
	
	/** @return A Tuple with the Down and Upper Normal Limits expressed in the Unit of Measurements specified for this Lab Test (provided that the UNIT specified) */
	protected Tuple3<Float, Float, Tuple2<String, String>> getAmountNormalRangeTuple(final String dnl, final String unl, final String unit, final OntClass testTerm) {
		if (isEmpty(unit)) return null;
		if (isEmpty(dnl) && isEmpty(unl)) return null;
		
		// Find the Unit of measurement specified for this Test
		final Tuple2<OntClass, String> unitTuple = getTestUnit(refModel, testTerm);
		
		// TODO: CHECK
		// Continue, provide that the Reference Model - Unit of Measurement found for the given term
		// NOTE: "-" this used for those Lab Test with actually a number (e.g., titer)
		if (unitTuple == null && !unit.trim().equals("-")) return null;
		
		// Parse given Data
		final Float givenDNL;
		final Float givenUNL;
		try {
			givenDNL = ((!isEmpty(dnl)) ? Float.parseFloat(dnl) : null);
			givenUNL = ((!isEmpty(unl)) ? Float.parseFloat(unl) : null);
		} catch (Throwable t) {
			log.debug("Data [ \"" + dnl + "\" - \"" + unl + "\" ] being ignored due to: " + Throable.throwableAsSimpleOneLineString(t));
			return null;
		}
		
		// Transform UNL/DNL values taking into account the existing/given Unit of measurement
		final Float newDNL = (unitTuple != null) ? 
			AmountsValueTransformer.expressValueInTargetUnit(givenDNL, unit, unitTuple._2()) : givenDNL;
		final Float newUNL = (unitTuple != null) ? 
			AmountsValueTransformer.expressValueInTargetUnit(givenUNL, unit, unitTuple._2()) : givenUNL;
		
		if (unitTuple != null) {
			return Tuple3.tuple(newDNL, newUNL, Tuple2.newTuple2(unitTuple._1().getLocalName(), unitTuple._2()));
		} else {
			return Tuple3.tuple(newDNL, newUNL, Tuple2.newTuple2("UNITEXP-19", "#"));
		}
	}
	
	/** @return A Term that indicates whether the value is normal or not */
	protected Term getAssessmentForValue(final Integer value, final Integer upLimit, final Integer downLimit) {
		if (value == null) return null;
		if (upLimit == null && downLimit == null) return null;
		// Below DNL
		if (downLimit != null && value < downLimit)
			return new Term(VOCNS + "ASSESS-21", "Below Down Normal Limit (DNL)");
		// Above UNL
		if (upLimit != null && value > upLimit) 
			return new Term(VOCNS + "ASSESS-22", "Above Upper Normal Limit (UNL)"); 
		// NORMAL
		return new Term(VOCNS + "ASSESS-10", "Normal"); 
	}
	
	/** @return A Term that indicates whether the value is normal or not */
	protected Term getAssessmentForValue(final Float value, final Float downLimit, final Float upLimit) {
		if (value == null) return null;
		if (upLimit == null && downLimit == null) return null;
		// Below DNL
		if (downLimit != null && value < downLimit)
			return new Term(VOCNS + "ASSESS-21", "Below Down Normal Limit (DNL)");
		// Above UNL
		if (upLimit != null && value > upLimit) 
			return new Term(VOCNS + "ASSESS-22", "Above Upper Normal Limit (UNL)"); 
		// NORMAL
		return new Term(VOCNS + "ASSESS-10", "Normal"); 
	}
	
	
	public Tuple2<OntClass, String> getDrugDosageUnit(OntClass drugOntClass) {
		final AnnotationProperty unitExpressionProp = refModel.getAnnotationProperty(unitExpressionUri);
		if (unitExpressionProp == null) throw new RuntimeException("Cannot find Annotation property: " + unitExpressionUri);
		// Get Term Unit of Measurement specified (if any)
		final RDFNode unitRdfNode = drugOntClass.getPropertyValue(unitExpressionProp);
		if (unitRdfNode == null) return null;
		final String unit = unitRdfNode.toString().trim();
		if (unit.equals("")) return null;
		// Find the Unit of Measurement (Term) specified in the Reference Model
		final OntClass unitsRootClass = refModel.getOntClass(UnitUri);
		if (unitsRootClass == null) throw new RuntimeException("Cannot find OWL class: " + UnitUri);
		final ExtendedIterator<OntClass> extIt = unitsRootClass.listSubClasses();
		while (extIt.hasNext()) {
			final OntClass unitOntClass = (OntClass) extIt.next();
			final RDFNode exprRdfNode = unitOntClass.getPropertyValue(unitExpressionProp);
			if (exprRdfNode == null) continue;
			final String expr = exprRdfNode.toString().trim();
			if (expr.equals("")) continue;
			if (unit.equalsIgnoreCase(expr)) return Tuple2.newTuple2(unitOntClass, expr);
		}
		throw new RuntimeException("Cannot find Unit term (OWL class) with Label: " + unit);
	}
	
	/** @return The Unit of measurement specified in the Reference Model for the given Term */
	public Tuple2<OntClass, String> getTestUnit(OntModel ontModel, OntClass ontClass) {
		final AnnotationProperty testOutcomeUnitProp = ontModel.getAnnotationProperty(testOutcomeUnitUri);
		if (testOutcomeUnitProp == null) throw new RuntimeException("Cannot find Annotation property: " + testOutcomeUnitUri);
		final AnnotationProperty unitExpressionProp = ontModel.getAnnotationProperty(unitExpressionUri);
		if (unitExpressionProp == null) throw new RuntimeException("Cannot find Annotation property: " + unitExpressionUri);
		// Get Term Unit of Measurement specified (if any)
		final RDFNode unitRdfNode = ontClass.getPropertyValue(testOutcomeUnitProp);
		if (unitRdfNode == null) return null;
		final String unit = unitRdfNode.toString().trim();
		if (unit.equals("")) return null;
		// Find the Unit of Measurement (Term) specified in the Reference Model
		final OntClass unitsRootClass = ontModel.getOntClass(UnitUri);
		if (unitsRootClass == null) throw new RuntimeException("Cannot find OWL class: " + UnitUri);
		//ontModel.listClasses();
		final ExtendedIterator<OntClass> extIt = unitsRootClass.listSubClasses();
		while (extIt.hasNext()) {
			final OntClass unitOntClass = (OntClass) extIt.next();
			final RDFNode exprRdfNode = unitOntClass.getPropertyValue(unitExpressionProp);
			if (exprRdfNode == null) continue;
			final String expr = exprRdfNode.toString().trim();
			if (expr.equals("")) continue;
			if (unit.equalsIgnoreCase(expr)) return Tuple2.newTuple2(unitOntClass, expr);
		}
		throw new RuntimeException("Cannot find Unit term (OWL class) with Label: " + unit);
	}
	
	/** @return An assessment term with the opposite meaning */
	protected SimpleCodedValue getOppositeAssessmentTerm(final OntClass assessmentTerm) {
		// About Normal/Abnormal
		if (assessmentTerm.getLocalName().equals("ASSESS-20")) 
			return new SimpleCodedValue("ASSESS-10", "Normal");
		if (assessmentTerm.getLocalName().equals("ASSESS-10")) 
			return new SimpleCodedValue("ASSESS-20", "Abnormal");
		
		// About Above or Below Normal Range of Values
		// In both cases, just return that this value was normal
		// Nevertheless, this is not 100% correct
		if (assessmentTerm.getLocalName().startsWith("ASSESS-2")) 
			return new SimpleCodedValue("ASSESS-10", "Normal");
		
		// Normally we will never be here
		return null;
	}
	
	/** @return An assessment term with the opposite meaning */
	protected SimpleCodedValue getOppositeAssessmentTerm(final Term term) {
		// About Normal/Abnormal
		if (term.getLocalName().equals("ASSESS-20")) 
			return new SimpleCodedValue("ASSESS-10", "Normal");
		if (term.getLocalName().equals("ASSESS-10")) 
			return new SimpleCodedValue("ASSESS-20", "Abnormal");
		
		// About Above or Below Normal Range of Values
		// In both cases, just return that this value was normal
		// Nevertheless, this is not 100% correct
		if (term.getLocalName().startsWith("ASSESS-2")) 
			return new SimpleCodedValue("ASSESS-10", "Normal");
		
		// Normally we will never be here
		return null;
	}
	
	/** @return The opposite/complementary Range of values */
	public AmountInterval getOppositeRangeAmountInterval(final Tuple3<Float, Float, Tuple2<String, String>> rangeTuple) {
		if (rangeTuple == null) return null;
		// IN neither Down nor Up Limits specified
		if (rangeTuple._1() == null && rangeTuple._2() == null) return null;
		
		// NOTE: Due to its complexity when both Limits specified, we return Null
		if (rangeTuple._1() != null && rangeTuple._2() != null) {
			log.warn("getOppositeRangeAmountInterval(): The 'opposite' of the given Range " + rangeTuple + " is too complex and it is being ignored.");
			return null;
		}
		
		// Range [-,B] --> Opposite [B,-s]
		if (rangeTuple._1() == null)
			return new AmountInterval(rangeTuple._2(), null, new SimpleCodedValue(rangeTuple._3()._1(), rangeTuple._3()._2()));
		// Range [A,-] --> Opposite [-,A]
		if (rangeTuple._2() == null)
			return new AmountInterval(null, rangeTuple._1(), new SimpleCodedValue(rangeTuple._3()._1(), rangeTuple._3()._2()));
		
		// Normally, we should not be here
		return null;
	}
	
	/** @return An assessment term (if possible) indicating whether the given range of values (up, down) is within the normal range of values (DNL, UNL) or not */
	protected Term getAssessmentForRange(final Float downLimit, final Float upLimit, final Float downNormLimit, final Float upNormLimit) {
		// IF Normal Range of Values not specified
		if (downNormLimit == null && upNormLimit == null) return null;
		// IF Range of Values not provided
		if (downLimit == null && upLimit == null) return null;
		
		if (upLimit != null && downNormLimit != null && upLimit < downNormLimit)
			return new Term(VOCNS + "ASSESS-20", "Abnormal"); 
		
		if (downLimit != null && upNormLimit != null && downLimit > upNormLimit)
			return new Term(VOCNS + "ASSESS-20", "Abnormal"); 
	
		// TODO: Check & Update
		// Take into account that any of the two ranges can be opened or closed
		
		return null;
	}
	
	/** @return A Set with possible outcome (e.g., NUMBER and ASSESSMENT) of the given Lab Test */
	protected Set<TestOutcome> getTestPossibleOutcome(final OntClass ontClass) {
		if (ontClass == null) return null;
		final AnnotationProperty testOutcomeProp = refModel.getAnnotationProperty(testOutcomeUri);
		if (testOutcomeProp == null) throw new RuntimeException("Cannot find Annotation property: " + testOutcomeUri);
		final RDFNode testOutcomeRdfNode = ontClass.getPropertyValue(testOutcomeProp);
		if (testOutcomeRdfNode == null) return newTestOutcomeTerms();
		final String testOutcome = testOutcomeRdfNode.toString().trim();
		if (testOutcome.equals("")) return newTestOutcomeTerms();
				
		if (testOutcome.equals("Boolean (Yes/No)"))
			return newTestOutcomeTerms(TestOutcome.BOOLEAN);
		
		if (testOutcome.equals("Amount and/or Assessment")) 
			return newTestOutcomeTerms(TestOutcome.AMOUNT, TestOutcome.ASSESSMENT);

		if (testOutcome.startsWith("Number and/or Assessment")) 
			return newTestOutcomeTerms(TestOutcome.AMOUNT, TestOutcome.ASSESSMENT);
		
		if (testOutcome.equals("Assessment Only"))
			return newTestOutcomeTerms(TestOutcome.ASSESSMENT);
		
		if (testOutcome.startsWith("Term"))
			return newTestOutcomeTerms(TestOutcome.TERM);
		
		log.error("Unexpected annotation property value about Test Outcome: " + testOutcome);
		
		return newTestOutcomeTerms();
	}
	
	private Set<TestOutcome> newTestOutcomeTerms(TestOutcome... terms) {
		final Set<TestOutcome> set = new HashSet<>();
		if (terms == null || terms.length == 0) return set;
		for (TestOutcome term : terms) {
			set.add(term);
		}
		return set;
	}
	
	/** @return A List of terms/strings separated by comma */
	protected List<String> getTermsStrList(final String termsStr) {
		if (termsStr == null || isEmpty(termsStr)) return null;
		final List<String> strList = new ArrayList<>();
		for (String str : termsStr.split(",")) {
			final String strTrim = str.trim();
			if (strTrim.equals("")) continue;
			strList.add(strTrim);
		}
		return strList;
	}
	
	/** @return The real number (i.e., the 1st token) of the given String with the value and unit of measurement */
	protected Float getRealNumberFromAmountStr(final String str, final String numFormatIndex) {
		if (isEmpty(str)) return null;
		// Get 1st Token
		final int spaceIndex = str.indexOf(' ');
		final String numStr = (spaceIndex > 0) ? str.substring(0, spaceIndex).trim() : str;
		// Parse Number
		final Float num = getRealNumber(numStr, numFormatIndex);
		return num;
	}
	
	/** @return A real number based on the given one/string and its format */
	protected Float getRealNumber(final String numStr, final String numFormatIndex) {
		if (isEmpty(numStr) || isEmpty(numFormatIndex)) return null;
		
		// Check Value Format
		if (!numFormatIndex.equalsIgnoreCase(NumFormat.DOT.name()) 
			&& !numFormatIndex.equalsIgnoreCase(NumFormat.COMMA.name()) 
			&& !numFormatIndex.equalsIgnoreCase(NumFormat.FRACTION.name()))
				throw new RuntimeException("Not supported Format: " + numFormatIndex + " (valueFormatIndex)");
		
		try {
			if (numFormatIndex.equalsIgnoreCase(NumFormat.DOT.name())) {
				return Float.parseFloat(numStr.replaceAll(",","\\.")); // Normally, the replace statement should not exist
			} else if (numFormatIndex.equalsIgnoreCase(NumFormat.COMMA.name())) {
				return Float.parseFloat(numStr.replaceAll(",","\\."));
			} else if (numFormatIndex.equalsIgnoreCase(NumFormat.FRACTION.name())) {
				if (numStr.equals("0")) return 0f;
				final int slashIndex = numStr.indexOf('/');
				final int semicolonIndex = numStr.indexOf(':');
				final String str = (slashIndex < 0 && semicolonIndex > 0) ? numStr.replaceAll(":","/") : numStr;
				int index = str.indexOf('/'); 
				if (index <= 0) throw new RuntimeException("Cannot find slash (/) in the given string (fraction): " + str + ((semicolonIndex > 0) ? " (character ':' replaced by '/')" : ""));
				return (Float) Float.parseFloat(str.substring(0, index)) / Float.parseFloat(str.substring(index + 1));
			} else {
				return null;
			}
		} catch (Throwable t) {
			log.debug("Data \"" + numStr + "\" being ignored due to: " +  Throable.throwableAsSimpleOneLineString(t));
			return null;
		}
	}
	
	/** @return A Real Number with the outcome of the given expression (e.g, 4 * 3.55 + 2) */
	protected double eval(final String str) {
	    return new Object() {
	        int pos = -1, ch;

	        void nextChar() {
	            ch = (++pos < str.length()) ? str.charAt(pos) : -1;
	        }

	        boolean eat(int charToEat) {
	            while (ch == ' ') nextChar();
	            if (ch == charToEat) {
	                nextChar();
	                return true;
	            }
	            return false;
	        }

	        double parse() {
	            nextChar();
	            double x = parseExpression();
	            if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
	            return x;
	        }

	        // Grammar:
	        // expression = term | expression `+` term | expression `-` term
	        // term = factor | term `*` factor | term `/` factor
	        // factor = `+` factor | `-` factor | `(` expression `)`
	        //        | number | functionName factor | factor `^` factor

	        double parseExpression() {
	            double x = parseTerm();
	            for (;;) {
	                if      (eat('+')) x += parseTerm(); // addition
	                else if (eat('-')) x -= parseTerm(); // subtraction
	                else return x;
	            }
	        }

	        double parseTerm() {
	            double x = parseFactor();
	            for (;;) {
	                if      (eat('*')) x *= parseFactor(); // multiplication
	                else if (eat('/')) x /= parseFactor(); // division
	                else if (eat('^')) x = Math.pow(x, parseFactor()); //exponentiation -> Moved in to here. So the problem is fixed
	                else return x;
	            }
	        }

	        double parseFactor() {
	            if (eat('+')) return parseFactor(); // unary plus
	            if (eat('-')) return -parseFactor(); // unary minus

	            double x;
	            int startPos = this.pos;
	            if (eat('(')) { // parentheses
	                x = parseExpression();
	                eat(')');
	            } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
	                while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
	                x = Double.parseDouble(str.substring(startPos, this.pos));
	            } else if (ch >= 'a' && ch <= 'z') { // functions
	                while (ch >= 'a' && ch <= 'z') nextChar();
	                String func = str.substring(startPos, this.pos);
	                x = parseFactor();
	                if (func.equals("sqrt")) x = Math.sqrt(x);
	                else if (func.equals("sin")) x = Math.sin(Math.toRadians(x));
	                else if (func.equals("cos")) x = Math.cos(Math.toRadians(x));
	                else if (func.equals("tan")) x = Math.tan(Math.toRadians(x));
	                else throw new RuntimeException("Unknown function: " + func);
	            } else {
	                throw new RuntimeException("Unexpected: " + (char)ch);
	            }

	            //if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation -> This is causing a bit of problem

	            return x;
	        }
	    }.parse();
	}
	
	protected Float getValueInTargetUnit(final String value, final String valueFormatIndex, final String srcUnit, final String trgUnit) {
		if (isEmpty(value) || isEmpty(valueFormatIndex) || isEmpty(srcUnit) || isEmpty(trgUnit)) return null;
		
		// Check Value Format
		if (!valueFormatIndex.equalsIgnoreCase(NumFormat.DOT.name()) 
			&& !valueFormatIndex.equalsIgnoreCase(NumFormat.COMMA.name()) 
			&& !valueFormatIndex.equalsIgnoreCase(NumFormat.FRACTION.name()))
				throw new RuntimeException("Not supported Format: " + valueFormatIndex + " (valueFormatIndex)");
		
		// Parse given Data
		final float existingValue;
		try {
			if (valueFormatIndex.equalsIgnoreCase(NumFormat.DOT.name())) {
				existingValue = Float.parseFloat(value.replaceAll(",","\\.")); // Normally, the replace statement should not exist
			} else if (valueFormatIndex.equalsIgnoreCase(NumFormat.COMMA.name())) {
				existingValue = Float.parseFloat(value.replaceAll(",","\\."));
			} else if (valueFormatIndex.equalsIgnoreCase(NumFormat.FRACTION.name())) {
				int index = value.indexOf('/'); 
				if (index <= 0) throw new RuntimeException("Cannot find slash (/) in the given string (fraction): " + value);
				existingValue = (Float) Float.parseFloat(value.substring(0, index)) / Float.parseFloat(value.substring(index + 1));
			} else {
				return null;
			}
		} catch (Throwable t) {
			log.debug("Data \"" + value + "\" being ignored due to: " +  Throable.throwableAsSimpleOneLineString(t));
			return null;
		}
				
		final Float newValue = AmountsValueTransformer.expressValueInTargetUnit(existingValue, srcUnit, trgUnit);
		return newValue;
	}
	
}
