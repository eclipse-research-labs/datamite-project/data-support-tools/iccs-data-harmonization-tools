package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.BooleanValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Field about Occupation Loss with the corresponding entity from the Reference Model. The values of this field should be independently ** aligned with ** the Confirmation YES/NO terms specified in the Reference Model */
public class OccupationLoss extends DataTransformation {

	/** Provides the appropriate Boolean Value based on Value of the given Field and the Mapping Rules already specified.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Occupation that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String confirmValue = data.get(0); // Occupation Loss Field

		// Data Processing
		final Tuple2<TermExpr, Relation> confirmValueTermExprT = getReferenceModelTermForCohortValue(confirmValue, 0);
		
		// IF the value is empty or not-aligned with Confirmation Terms, it will be ignored.
		if (confirmValueTermExprT == null) return null;

		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValueTermExprT._1().asString() + "\" is NOT a Confirmation Term !");

		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new BooleanValue(confirmValueTermExprT);

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#work-Loss-due-to-PSS
		
		return newPosTuple(map);
	}

}

