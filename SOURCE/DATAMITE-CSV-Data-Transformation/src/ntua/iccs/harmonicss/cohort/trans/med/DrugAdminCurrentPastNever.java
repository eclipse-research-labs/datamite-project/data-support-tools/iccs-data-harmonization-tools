package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONArray;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Fields, A) A Field that indicates i., whether a person is currently receiving a drug, ii., or whether he/she has received that drug in the past, iii., or that he person has never received that drug and B) Another Field that indicates the date the person was questioned. The corresponding Reference Model term (Drug) should be specified. The values of the 1st Field about Current, Past and Never should be specified (in the form of a JSON array). Additionally the Format of the Date (2nd Field) should be given. */
public class DrugAdminCurrentPastNever extends DataTransformation {

	private static final Logger log = LogFactory.getLoggerForClass(DrugAdminCurrentPastNever.class);
	
	private enum ServiceArg { Reference_Model_DrugSubstance, Current_JA, Past_JA, Never_JA, Date_Format }
	
	/** Provides the corresponding Reference Model term (i.e., Drug) along with the relevant Period of Time (i.e., the period before or after the given date) based on the value of the 1st field and additional parameters provided (i.e., meaning of values and date format), provided that the value existing in the 1st field is not empty. More precisely, IF the value is current or past (i.e., aligned with the YES confirmation term) the service indicates that the person is linked with that drug (either now or in the past), otherwise not (never).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Medication that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String drugStatusValue = data.get(0); // Drug Field: Current/Past/Never value
		final String drugDate = data.get(1); // Date

		// Additional Data / Arguments (if any)
		final String drugTermUri = getArgValue(ServiceArg.Reference_Model_DrugSubstance);
		final String currentJAStr = getArgValue(ServiceArg.Current_JA);
		final String pastJAStr = getArgValue(ServiceArg.Past_JA);
		final String neverJAStr = getArgValue(ServiceArg.Never_JA);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass drugTerm = getReferenceModelTermWithURI(drugTermUri);
		
		final JSONArray currentJA = JSONArray.fromObject(currentJAStr);
		final JSONArray pastJA = JSONArray.fromObject(pastJAStr);
		final JSONArray neverJA = JSONArray.fromObject(neverJAStr);
		
		// IF the value is empty, it will be ignored.
		if (isEmpty(drugStatusValue)) return null;
		
		// Ensure that the values is one of the current/past/never terms/values
		if (!currentJA.contains(drugStatusValue) && !pastJA.contains(drugStatusValue) && !neverJA.contains(drugStatusValue)) {
			log.warn("The value of the Field is none of the given terms about Current/Past/Never.. Data being ignored !");
			return null;
		}
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(drugDate, dateFormat);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(drugTerm);
		final PropertyValue propValue1;
		if (dateTuple != null && (currentJA.contains(drugStatusValue) || pastJA.contains(drugStatusValue)) ) {
			if (currentJA.contains(drugStatusValue)) {
				// Current ... Drug administered in the period of time AFTER the given Date
				propValue1 = new DateIntervalTypeA(new DateValue(dateTuple), null);
			} else {
				// Past ... Drug administered in the period of time BEFORE the given Date
				propValue1 = new DateIntervalTypeA(null, new DateValue(dateTuple));
			}
		} else {
			propValue1 = null;
		}

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Date-Interval
		
		// Depending on the value of the Field, return a positive/negative "statement"
		if (currentJA.contains(drugStatusValue) || pastJA.contains(drugStatusValue)) {
			// Current/Past ... Drug administered
			return newPosTuple(map);
		} else {
			// Never ... Drug NOT administered
			return newNegTuple(map);
		}
		
	}

}
