package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.VOCNS;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Cohort Fields A) a Field about Weight and B) another Field about Height. The format of the numbers existing in the two fields should be specified. Also the units of measurement should be given. */
public class BodyMassIndexBasedOnWeightHeight extends DataTransformation {

	private enum ServiceArg { Weight_Number_Format, Weight_Unit_of_Measurement, Height_Number_Format, Height_Unit_of_Measurement }
	
	/** Provides the appropriate Term (i.e., BMI category) based on Value of the given Fields (i.e., Weight and Height) taking into account additional data provided (i.e., Format of Number and Units of Measurement).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Body-Weight that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String weightStr = data.get(0); // Weight
		final String heightStr = data.get(1); // Height

		// Additional Data / Arguments (if any)
		final String weightValueFormatIndex = getArgValue(ServiceArg.Weight_Number_Format);
		final String weightValueUnit = getArgValue(ServiceArg.Weight_Unit_of_Measurement);
		final String heightValueFormatIndex = getArgValue(ServiceArg.Height_Number_Format);
		final String heightValueUnit = getArgValue(ServiceArg.Height_Unit_of_Measurement);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final Float weightKgr = getValueInTargetUnit(weightStr, weightValueFormatIndex, weightValueUnit, "kg");
		if (weightKgr == null) return null;
		//System.out.println(weightStr +  "weightKgr: " + weightKgr);
		final Float heightM = getValueInTargetUnit(heightStr, heightValueFormatIndex, heightValueUnit, "m");
		if (heightM == null) return null;
		//System.out.println(heightStr + " heightM: " + heightM);
		
		// BMI = (weight / (height)^2), in kg/m^2
		final double bmi = (float) weightKgr / Math.pow(heightM, 2);
		
		// Category
		final Term bmiTerm;
		if (bmi < 18.5) {
			bmiTerm = new Term(VOCNS + "BMI-01", "Underweight (BMI < 18.5)"); 
		} else if (bmi < 24.9) {
			bmiTerm = new Term(VOCNS + "BMI-02", "Normal Weight (BMI 18.5 - 24.9)"); 	
		} else if (bmi < 29.9) {
			bmiTerm = new Term(VOCNS + "BMI-03", "Overweight (BMI 25 - 29.9)"); 
		} else {
			bmiTerm = new Term(VOCNS + "BMI-04", "Obesity (BMI >= 30)"); 
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(bmiTerm); 

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#bmi-CV
		
		return newPosTuple(map);
	}

}
