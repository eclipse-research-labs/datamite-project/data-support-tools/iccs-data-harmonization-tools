package ntua.iccs.harmonicss.cohort.trans.demo;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/**
 * Mapping a Cohort Field about a Demographic Characteristic (e.g., Sex) with the corresponding entity from the Reference Model. 
 * <p>
 * The values of this field should be independently ** aligned with ** the corresponding terms (e.g., Sex/Gender) specified in the Reference Model
 */
public class DemoFieldValueReplacement extends DataTransformation {

	/** Finds the appropriate Term based on the existing one and the Mapping Rules already specified.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Gender that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String termValue = data.get(0); // Sex/Gender Field

		// Data Processing
		final Tuple2<TermExpr, Relation> termExprTuple = getReferenceModelTermForCohortValue(termValue, 0);
		
		// IF the value is empty or not-aligned with Reference Model Terms, it will be ignored.
		if (termExprTuple == null) return null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = 
			(termExprTuple._1().isTerm()) ? new SimpleCodedValue(termExprTuple) : new ComplexCodedValue(termExprTuple);
		
		map.put(0, propValue0); // Value of Property: http://.../reference-model#...
		
		return newPosTuple(map);
	}

}