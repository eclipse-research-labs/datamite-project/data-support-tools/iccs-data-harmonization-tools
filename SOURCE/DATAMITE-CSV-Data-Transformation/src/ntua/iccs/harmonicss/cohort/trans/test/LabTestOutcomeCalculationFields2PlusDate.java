package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 3 Cohort Fields, A) Two Field being necessary for calculating the outcome of a specific Lab Test and B) Another Field with the Date it took place. The corresponding Reference Model term should be specified. The Format of the numbers existing in the 1st and 2nd Fields should be given along with the Formula that should be used for calculating the outcome based on the values of the given Fields (e.g., FIELD1 * FIELD2 * 1000) and the Unit of Measurement of the calculated outcome. Also we would like to know the Normal Range of values (if applicable). Additionally the format of the Date (3rd field) should be given. */
public class LabTestOutcomeCalculationFields2PlusDate extends DataTransformation {

	private static final Logger log = LogFactory.getLoggerForClass(LabTestOutcomeCalculationFields2PlusDate.class);
	
	private enum ServiceArg { Reference_Model_Blood_Test, Field_1_Number_Format, Field_2_Number_Format, Outcome_Formula_Math_Expression, Outcome_Unit_of_Measurement, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, Date_Format }
	
	/** Provides the appropriate Term for the specific Lab Test, the Date it took place and the Outcome measured based on the Value of the given Fields and the additional parameters provided (i.e., corresponding Reference Model term, Numbers and Date format, Formula and Unit of measurement). Also, provided that the UNL and DNL specified, the Normal Range of Values will be also recorded, as well as whether the outcome was normal or not. In case the 1st and/or 2nd Fields are empty or no Unit of Measurement specified in the Reference Model about the specific Lab Test, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutValue1Str = data.get(0); // Lab Test: Numeric Value 1
		final String testOutValue2Str = data.get(1); // Lab Test: Numeric Value 2
		final String testDateStr = data.get(2); // Lab Test Date

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Blood_Test);
		final String valueFormatIndex1 = getArgValue(ServiceArg.Field_1_Number_Format);
		final String valueFormatIndex2 = getArgValue(ServiceArg.Field_2_Number_Format);
		final String outcomeFormulaStr = getArgValue(ServiceArg.Outcome_Formula_Math_Expression);
		final String testValueUnit = getArgValue(ServiceArg.Outcome_Unit_of_Measurement);
		final String downNormLimitStr = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimitStr = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String testDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);

		// Ensure that the value of the given Test can be an Amount
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.AMOUNT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Amount ! Possible Test Outcome(s): " + expOutSet);
		
		final Float testOutValue1 = getRealNumber(testOutValue1Str, valueFormatIndex1);
		final Float testOutValue2 = getRealNumber(testOutValue2Str, valueFormatIndex2);
		
		// IF any of the given values is Empty, no data will be recorded
		if (testOutValue1 == null || testOutValue2 == null) return null;
		
		// Calculate Outcome
//		final String expr = outcomeFormulaStr.replaceAll("FIELD1", df.format(testOutValue1)).replaceAll("FIELD2", df.format(testOutValue2));
		final String expr = outcomeFormulaStr.replaceAll("FIELD1", "" + testOutValue1).replaceAll("FIELD2", "" + testOutValue2);
		final double outcomeValue;
		try {
			outcomeValue = eval(expr);
		} catch(Throwable t) {
			log.error("Cannot evaluate expression: " + expr + " - " + t.getMessage());
			return null;
		}
		
		final Tuple2<Float, Tuple2<String, String>> valueTuple = getAmountValueTuple(df.format(outcomeValue), "DOT", testValueUnit, testTerm);
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(testDateStr, testDateFormat);
		
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple = getAmountNormalRangeTuple(downNormLimitStr, upNormLimitStr, testValueUnit, testTerm);
		final Term assessTerm = (normRangeTuple != null) ? getAssessmentForValue(valueTuple._1(), normRangeTuple._1(), normRangeTuple._2()) : null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue2 = (valueTuple != null) ? new AmountValue(valueTuple): null;
		final PropertyValue propValue3 = (assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;
		final PropertyValue propValue4 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(4, propValue4); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
	}

}
