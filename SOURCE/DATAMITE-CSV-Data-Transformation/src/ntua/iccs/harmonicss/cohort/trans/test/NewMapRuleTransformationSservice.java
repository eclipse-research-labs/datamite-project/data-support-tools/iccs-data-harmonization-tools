package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 3 fields: Provide Mapping Scenario Description.... */
public class NewMapRuleTransformationSservice extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL, UNL_and_DNL_Unit_of_Measurement, DoB_Date_Format }
	
	/** Provide service description....
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@SuppressWarnings("unused")
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutAssess = data.get(0); // Lab Test Outcome: Assessment
		final String dateValue = data.get(1); // Date of Birth
		final String ageValue = data.get(2); // Age of Person at Test

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String arg1 = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String arg2 = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		final String arg3 = getArgValue(ServiceArg.UNL_and_DNL_Unit_of_Measurement);
		final String dobDateFormat = getArgValue(ServiceArg.DoB_Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		final Tuple2<TermExpr, Relation> termExprT = getReferenceModelTermForCohortValue(testOutAssess, 0);
		
		// IF the value is empty or not-aligned with Assessment Terms, no Data will be recorded
		if (termExprT == null) return null;
		
		final Integer age = getInteger(ageValue);
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(dateValue, dobDateFormat);
		final Tuple3<Integer, Integer, Integer> testDateTuple = (dateTuple != null && age != null) ?
			new Tuple3<Integer, Integer, Integer>(dateTuple._1() + age, dateTuple._2(), dateTuple._3()) : null;
		
		System.out.println(" ** WHEN MR USED - THIS MSG SHOULD APPEAR IN YOUR SCREEN **");

		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 =
			new SimpleCodedValue(testTerm.getLocalName(), testTerm.getLabel(null));
		final PropertyValue propValue1 = 
			(testDateTuple != null) ? new DateValue(testDateTuple._1(), testDateTuple._2(), testDateTuple._3()): null;
		final PropertyValue propValue2;
		if (termExprT._1().isTerm()) {
			final Term term = (Term) termExprT._1();
			propValue2 = new SimpleCodedValue(term.getLocalName(), term.getLabel());
		} else {
			throw new RuntimeException("NOT SUPPORTED .. TODO");
		}
		
		final PropertyValue propValue3 = 
			null;  // TODO: later... 

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		// The List should often contain only one Map (so that only one instance will be created)
		return newPosTuple(map);
	}

}
