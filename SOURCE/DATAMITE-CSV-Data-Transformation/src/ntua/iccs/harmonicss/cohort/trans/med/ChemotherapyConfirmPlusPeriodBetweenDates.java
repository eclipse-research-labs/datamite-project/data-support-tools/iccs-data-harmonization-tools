package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 3 fields: A) A Field that indicates whether the person has received Chemotherapy or not and B) Two other Fields that indicate the period of time (start and end date) when the person received Chemotherapy. The values of the 1st Field should be independently ** aligned with ** the Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Additionally the format of the Date (2nd and 3rd fields) should be given. */
public class ChemotherapyConfirmPlusPeriodBetweenDates extends DataTransformation {

	private enum ServiceArg { Date_Format }
	
	/** The system provides the appropriate data taking into account the values of the given Fields (i.e., chemotherapy value and start & end dates), additional data provided (i.e., dates format) and mapping rules specified. Depending on the value of the 1st field, it indicates whether the person received chemotherapy or not. Provided that at least one of the two other fields is not empty, the period of chemotherapy is also provided.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Chemotherapy that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String chemoConfirmValue = data.get(0); // Chemotherapy: YES/NO
		final String startDateStr = data.get(1); // Chemotherapy Period Start Date
		final String endDateStr = data.get(2); // Chemotherapy Period End Date

		// Additional Data / Arguments (if any)
		final String dateFormat = getArgValue(ServiceArg.Date_Format);

		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final Tuple2<TermExpr, Relation> confirmValue = getReferenceModelTermForCohortValue(chemoConfirmValue, 0);
		
		// IF the value is empty, it will be ignored.
		if (confirmValue == null) return null;

		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!confirmValue._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + confirmValue._1().asString() + "\" is NOT a Confirmation Term !");

		final Tuple3<Integer, Integer, Integer> startDateTuple = getDateTuple(startDateStr, dateFormat);
		final Tuple3<Integer, Integer, Integer> endDateTuple = getDateTuple(endDateStr, dateFormat);
		
		// Ensure that the Date Interval (Start Date - End Date) is valid
		final boolean isDateIntervalValid = isDateIntervalValid(startDateTuple, endDateTuple);
		if (!isDateIntervalValid) {
			log.warn("Invalid Date Interval: [" + startDateTuple + " , " + endDateTuple + "] Period of Time is being ignored !");
		}
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = 
			((startDateTuple != null || endDateTuple != null) && isDateIntervalValid) ? 
				new DateIntervalTypeA(startDateTuple, endDateTuple): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#chemotherapy-Date-Interval
				
		// Depending on the value of the Field, return a positive/negative "statement"
		if (confirmValue._1().isConfirmationTermYES()) {
			return newPosTuple(map);
		} else {
			return newNegTuple(map);
		}
	
	}

}
