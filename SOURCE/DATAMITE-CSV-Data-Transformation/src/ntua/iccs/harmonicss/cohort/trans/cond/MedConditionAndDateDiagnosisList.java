package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Field with one or more elements (sequences of characters) separated by comma. Each element contain the name of a Medical Condition and optionally the Date diagnosed (with this order). The different medical conditions (cohort terms) should be independently ** aligned with ** the Medical Condition terms specified in the Reference Model. Also, the format of the Date (i.e., the Last Token in the String) should be given. */
public class MedConditionAndDateDiagnosisList extends DataTransformation {

	private enum ServiceArg { Date_Token_Format }
	
	/** Provides a List of Entities with the corresponding Reference Model term (i.e., Medical Condition) along with the date the medical condition diagnosed based on the value of the given Fields, additional data given (i.e., date format) and the Mapping Rules specified (i.e., mapping of cohort Medical Condition Terms/Values with the corresponding Reference Model terms about Medical Conditions). If the value of this Field is empty or none of the comma separated elements (after removing the last token about the date) are aligned with the Reference Model Medical Condition terms, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Diagnosis that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String elements = data.get(0); // Medical Condition(s) + (Date): Comma-separated Elements

		// Additional Data / Arguments (if any)
		final String dateTokenFormat = getArgValue(ServiceArg.Date_Token_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final List<String> strList = getTermsStrList(elements);
		
		// IF field is empty
		if (strList == null) return null;
		
		final List<Tuple2<Tuple2<TermExpr, Relation>, Tuple3<Integer, Integer, Integer>>> tupleList = new ArrayList<>();
		for (String str : strList) {
			final Tuple2<String, String> tuple = splitStr(str, dateTokenFormat);
			if (tuple == null) continue;
			final Tuple2<TermExpr, Relation> termExprTuple = getReferenceModelTermForCohortValue(tuple._1(), 0);
			Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(tuple._2(), dateTokenFormat);
			if (termExprTuple == null) continue;
			// Keep only Medical Conditions
			if (!termExprTuple._1().isMedicalCondition()) continue;
			tupleList.add(Tuple2.newTuple2(termExprTuple, dateTuple));
		}
		
		// IF none term aligned with symptoms terms
		if (tupleList.isEmpty()) return null;
		
		final List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
		for (Tuple2<Tuple2<TermExpr, Relation>, Tuple3<Integer, Integer, Integer>> tuple : tupleList) {
			final Tuple2<TermExpr, Relation> termExprTuple = tuple._1();
			final Tuple3<Integer, Integer, Integer> dateTuple = tuple._2();
			final Map<Integer, PropertyValue> map = new HashMap<>();
			final PropertyValue propValue0 = 
				(termExprTuple._1().isTerm()) ? new SimpleCodedValue(termExprTuple) : new ComplexCodedValue(termExprTuple);
			final PropertyValue propValue1 = 
				(dateTuple != null) ? new DateValue(dateTuple): null;
			map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-CV
			map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-Date
			mapList.add(map);
		}
		
		return Tuple2.newTuple2(Boolean.TRUE, mapList);
	}

	private Tuple2<String, String> splitStr(final String str, final String dateFormat) {
		final int spaceIndex = str.lastIndexOf(' ');
		if (spaceIndex <= 0) {
			return Tuple2.newTuple2(str, null);
		} else {
			final String lastToken = str.substring(spaceIndex).trim();
			if (isStrDate(lastToken, dateFormat)) {
				return Tuple2.newTuple2(str.substring(0, spaceIndex).trim(), lastToken);
			} else {
				return Tuple2.newTuple2(str, null);
			}
		}
	}
	
	private boolean isStrDate(String str, String dateFormat) {
		final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false) ;
		try {
			return true;
		} catch (Throwable t) {
			return false;
		}
	}
	
}

