package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.VOCNS;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.data.BooleanOperator;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.json.TermExpression;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.BooleanValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Field with one or more Lab Tests (often, auto-antibody tests) took place, separated by comma. The different Lab Test (cohort terms) should be independently ** aligned with ** the Lab Tests specified in the Reference Model. */
public class LabTestList extends DataTransformation {

	private static final Logger log = LogFactory.getLoggerForClass(LabTestList.class);
	
	/** Provides a List of Entities with the corresponding Reference Model term (i.e., Lab Test) and the appropriate outcome or assessment. In case of a Lab Test with a Boolean value (e.g., an auto-antibody), the outcome recorded will be true. In case of a Lab test with a Numeric Value and/or Assessment, the assessment recorded will be Abnormal. If the value of this Field is empty or none of the comma separated terms is aligned with the Reference Model Lab Tests terms (with a Boolean or Numeric Value), no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String termsStr = data.get(0); // Lab-Test(s): Comma-separated Terms

		// Data Processing
		final List<String> strList = getTermsStrList(termsStr);
		
		// IF field is empty
		if (strList == null) return null;
		
		final List<Tuple2<TermExpr, Relation>> termExprTupleTmpList = new ArrayList<>();
		for (String str : strList) {
			final Tuple2<TermExpr, Relation> termExprTuple = getReferenceModelTermForCohortValue(str, 0);
			if (termExprTuple == null) continue;

			// Keep only Blood Tests and their Outcome (if being a Term)
			if (!termExprTuple._1().isBloodTest() && !termExprTuple._1().isBloodTestOutcomeTerm()) {
				log.warn("The Following Term is being ignored: " + termExprTuple._1().asString() + " (not a Simple Blood Test Term)");
				continue;
			}
			termExprTupleTmpList.add(termExprTuple);
		}
		
		// Pre-processing ...
		// Replace Complex Term linked with AND Operator with a simple ones
		final List<Tuple2<TermExpr, Relation>> termExprTupleList = new ArrayList<>();
		for (Tuple2<TermExpr, Relation> termExprTuple : termExprTupleTmpList) {
			if (termExprTuple._1().isTerm()) {
				termExprTupleList.add(termExprTuple);	
			} else {
				final TermExpression termExpr = termExprTuple._1().asTermExpression();
				if (termExpr.getBooleanOp().equals(BooleanOperator.AND)) {
					for (TermExpr te : termExpr.getTermExprList()) {
						termExprTupleList.add(Tuple2.newTuple2(te, termExprTuple._2()));
					}
				} else {
					termExprTupleList.add(termExprTuple);
				}
			}
		}
		
		// IF none term aligned with Lab Test terms
		if (termExprTupleList.isEmpty()) return null;
		
		final List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
		for (Tuple2<TermExpr, Relation> termExprTuple : termExprTupleList) {
			if (!termExprTuple._1().isTerm()) {
				log.warn("'Complex' Term \"" + termExprTuple._1().asString() + "\" is being ignorred (not a simple/existing Lab Test term or outcome).");
				continue;
			}
			
			final Term term = (Term) termExprTuple._1();
			
			if (term.isBloodTest()) {
				final Set<TestOutcome> expOutSet = getTestPossibleOutcome(refModel.getOntClass(term.getUri()));
				
				final Map<Integer, PropertyValue> map = new HashMap<>();
				
				final PropertyValue propValue0 = new SimpleCodedValue(term);
				final PropertyValue propValue1 = 
					(expOutSet.contains(TestOutcome.BOOLEAN)) ? new BooleanValue(true) : null;
				final PropertyValue propValue2 = 
					(expOutSet.contains(TestOutcome.ASSESSMENT)) ? new SimpleCodedValue(new Term(VOCNS + "ASSESS-20", "Abnormal")) : null;
				
				map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
				map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
				map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
					
				mapList.add(map);
			} else { // Blood Test Outcome			
				final OntClass testTerm;
				if (term.isAnaPatternTerm()) {
					testTerm = getReferenceModelTermWithURI(LabTestOutcomeTermBooleanPlusDate.anaPatternTestUri);
				} else if (term.isCryoTypeTerm()) {
					testTerm = getReferenceModelTermWithURI(LabTestOutcomeTermBooleanPlusDate.cryoTypeTestUri);
				} else {
					throw new RuntimeException("There is no Reference Model Test with outcome the given Term: \"" + term + "\"");
				}
				
				final Map<Integer, PropertyValue> map = new HashMap<>();
				
				final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
				final PropertyValue propValue1 = new SimpleCodedValue(term); 
				final PropertyValue propValue2 = null;
				
				map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
				map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
				map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
					
				mapList.add(map);	
			}
			
		}
		
		return Tuple2.newTuple2(Boolean.TRUE, mapList);
	}
	
}
