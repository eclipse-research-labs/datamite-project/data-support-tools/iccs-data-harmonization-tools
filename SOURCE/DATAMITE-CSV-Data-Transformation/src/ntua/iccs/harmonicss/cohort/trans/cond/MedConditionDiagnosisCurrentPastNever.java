package ntua.iccs.harmonicss.cohort.trans.cond;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONArray;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping two Fields, A) A Field that indicates i., whether a person has a particular condition (current), ii., or whether he/she had the condition in the past, iii., or that he person has never diagnosed with that condition and B) Another Field that indicates that the date the person was questioned or examined. The corresponding Reference Model term (Drug) should be specified. The values of the 1st Field about Current, Past and Never should be specified (in the form of a JSON array). Additionally the Format of the Date (2nd Field) should be given. */
public class MedConditionDiagnosisCurrentPastNever extends DataTransformation {

	private enum ServiceArg { Reference_Model_Condition, Current_JA, Past_JA, Never_JA, Date_Format }
	
	/** Provides the corresponding Reference Model term (i.e., Medical Condition) along with the relevant Period of Time (i.e., the period before given date in case the person has been diagnosed with this condition in the past) or Date (in case the person has been diagnosed with the condition that date) based on the value of the 1st field and additional parameters given (i.e., meaning of values and date format), provided that the value existing in the 1st field is not empty. More precisely, IF the value is current or past (i.e., aligned with the YES confirmation term) the service indicates that the person is linked with that condition (either now or in the past), otherwise not (never).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Diagnosis that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String condStatusValue = data.get(0); // Medical Condition: Current/Past/Never value
		final String condDate = data.get(1); // Date

		// Additional Data / Arguments (if any)
		final String condTermUri = getArgValue(ServiceArg.Reference_Model_Condition);
		final String currentJAStr = getArgValue(ServiceArg.Current_JA);
		final String pastJAStr = getArgValue(ServiceArg.Past_JA);
		final String neverJAStr = getArgValue(ServiceArg.Never_JA);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass condTerm = getReferenceModelTermWithURI(condTermUri);
		
		final JSONArray currentJA = JSONArray.fromObject(currentJAStr);
		final JSONArray pastJA = JSONArray.fromObject(pastJAStr);
		final JSONArray neverJA = JSONArray.fromObject(neverJAStr);
		
		// IF the value is empty, it will be ignored.
		if (isEmpty(condStatusValue)) return null;
		
		// Ensure that the values is one of the current/past/never terms/values
		if (!currentJA.contains(condStatusValue) && !pastJA.contains(condStatusValue) && !neverJA.contains(condStatusValue)) {
			log.warn("The value of the Field is none of the given terms about Current/Past/Never.. Data being ignored !");
			return null;
		}
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(condDate, dateFormat);		
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(condTerm);
		final PropertyValue propValue1;
		if (dateTuple != null && (currentJA.contains(condStatusValue) || pastJA.contains(condStatusValue)) ) {
			if (currentJA.contains(condStatusValue)) {
				// Current ... Condition just diagnosed (i.e., the date specified)
				propValue1 = new DateValue(dateTuple);
			} else {
				// Past ... Condition diagnosed in the period of time BEFORE the given Date
				propValue1 = new DateIntervalTypeA(null, new DateValue(dateTuple));
			}
		} else {
			propValue1 = null;
		}

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#diagnosis-Date
		
		// Depending on the value of the Field, return a positive/negative "statement"
		if (currentJA.contains(condStatusValue) || pastJA.contains(condStatusValue)) {
			// Current/Past ... Condition diagnosed
			return newPosTuple(map);
		} else {
			// Never ... Drug NOT diagnosed
			return newNegTuple(map);
		}
		
	}

}
