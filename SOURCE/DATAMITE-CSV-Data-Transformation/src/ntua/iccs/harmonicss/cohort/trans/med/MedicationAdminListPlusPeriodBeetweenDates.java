package ntua.iccs.harmonicss.cohort.trans.med;

import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.data.owl.interv.DateIntervalTypeA;
import ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping three Fields, A) A Field with one or more codes (e.g., numbers) separated by comma, where each code points to a specific Drug along with the Dosage amount and frequency, and B) two other Fields that indicates that the person received ALL the Drugs with the given dosage amount and frequency BETWEEN the start & end dates specified. The codes we may encounter in the 1st should be included in a JSON object where each one should point to another JSON object with the RM term and the given dosage value (i.e., number) or range (i.e., number1, number2). The unit has already defined for each drug. Additionally the Format of the Dates (2nd and 3rd Field) should be given. */
public class MedicationAdminListPlusPeriodBeetweenDates extends DataTransformation {

	private static final String TERM = "term";
	private static final String DOSAGE = "dosage";
	private static final char COMMA = ',';
	private static final String NOTERMS = "noterms";
	
	private enum ServiceArg { Medication_JO, Date_Format }
	
	/** Provides a List of Entities with the corresponding Reference Model term (i.e., Drug) along with the Dosage prescribed and period of administration based on the values of the given Fields (i.e., medication id and start-end date) and additional data provided (especially the JSON with the Mapping of Medication codes to the specific Drug and Dosage). If the value of the 1st Field is empty or none of the codes included in the JSON object (hence their meaning has not specified), no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Medication that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String codesStr = data.get(0); // Medications(s): Comma-separated Codes
		final String startDateStr = data.get(1); // Medications(s) Start Date
		final String endDateStr = data.get(2); // Medications(s) End Date

		// Additional Data / Arguments (if any)
		final String codesJOStr = getArgValue(ServiceArg.Medication_JO);
		final String dateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final JSONObject codesJO = JSONObject.fromObject(codesJOStr);
		
		// Ensure that the Values of each each Code is a JSON object and it has at least a property about the term
		for (Object key : codesJO.keySet()) {
			try {
				final JSONObject jo = codesJO.getJSONObject("" + key);
				if (jo == null) throw new NullPointerException("Method getJSONObject returns null !");
				if (!jo.has(TERM) && !jo.has(NOTERMS)) throw new RuntimeException("JSON object " + jo + " does NOT have a property with name \"" + TERM + "\" or \"" + NOTERMS + "\".");
			} catch(Throwable t) {
				throw new RuntimeException("The code \"" + key + "\" of the parameter \"" + ServiceArg.Medication_JO + "\" has not properly defined.", t);
			}
		}
		
		final List<String> strList = getTermsStrList(codesStr);
		
		// IF field is empty
		if (strList == null) return null;
		
		final Tuple3<Integer, Integer, Integer> startDateTuple = getDateTuple(startDateStr, dateFormat);
		final Tuple3<Integer, Integer, Integer> endDateTuple = getDateTuple(endDateStr, dateFormat);
		
		// Ensure that the Date Interval (Start Date - End Date) is valid
		final boolean isDateIntervalValid = isDateIntervalValid(startDateTuple, endDateTuple);
		if (!isDateIntervalValid) {
			log.warn("Invalid Date Interval: [" + startDateTuple + " , " + endDateTuple + "] Period of Time is being ignored !");
		}

		// IF it has only one value that indicates that the Person has not received a List of Drugs
		if (strList.size() == 1) {
			final String str = strList.get(0);
			if (codesJO.has(str)) {
				final JSONObject codeDataJO = codesJO.getJSONObject(str);
				// Check IF Code Data point to the List of NOT administered Drugs
				if (codeDataJO != null && codeDataJO.has(NOTERMS)) {
					final JSONArray ja = codeDataJO.getJSONArray(NOTERMS);
					List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
					for (Object code : ja) {
						final Map<Integer, PropertyValue> map = new HashMap<>();
						final PropertyValue propValue0 = new SimpleCodedValue(this.getReferenceModelTermWithURI(ReferenceMoUri.VOCNS + code));
						final PropertyValue propValue1 = null;
						final PropertyValue propValue2 = 
							((startDateTuple != null || endDateTuple != null) && isDateIntervalValid) ? 
								new DateIntervalTypeA(startDateTuple, endDateTuple): null;
								
						map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-CV
						map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Dosage
						map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Date-Interval
								
						mapList.add(map);
					}
					return Tuple2.newTuple2(Boolean.FALSE, mapList);
				}
			}
		}
		
		final List<Tuple2<OntClass, PropertyValue>> medTupleList = new ArrayList<>();
		for (String str : strList) {
			if (!codesJO.containsKey(str)) continue;
			final JSONObject codeDataJO = codesJO.getJSONObject(str);
			
			final String drugCode = codeDataJO.getString(TERM);
			final OntClass drugOntClass = this.getReferenceModelTermWithURI(ReferenceMoUri.VOCNS + drugCode);
			
			final Tuple2<OntClass, String> unitTuple = getDrugDosageUnit(drugOntClass);
			final SimpleCodedValue unit = (unitTuple != null) ? new SimpleCodedValue(unitTuple._1().getLocalName(), unitTuple._2()): null;
			
			PropertyValue dosagePropValue = null;
			if (codeDataJO.has(DOSAGE)) {
				final String dosage = codeDataJO.getString(DOSAGE).trim();
				if (!isEmpty(dosage)) {
					final int commaIndex = dosage.indexOf(COMMA);
					if (commaIndex >= 0) {
						// Dosage Range
						final Float downValue = getRealNumber(dosage.substring(0, commaIndex).trim(), "DOT");
						final Float upValue = getRealNumber(dosage.substring(commaIndex + 1).trim(), "DOT");
						dosagePropValue = ((downValue != null || upValue != null) && unit != null) ? new AmountInterval(downValue, upValue, unit) : null;
					} else {
						// Exact Dosage
						final Float dosageValue = getRealNumber(dosage, "DOT");
						dosagePropValue = (dosageValue != null && unit != null) ? new AmountValue(dosageValue, unit) : null;
					}
				}
			}
			
			medTupleList.add(Tuple2.newTuple2(drugOntClass, dosagePropValue));
		}
		
		// IF none term linked with Reference Model terms
		if (medTupleList.isEmpty()) return null;
		
		List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
		for (Tuple2<OntClass, PropertyValue> medTuple : medTupleList) {
			final Map<Integer, PropertyValue> map = new HashMap<>();
			final PropertyValue propValue0 = new SimpleCodedValue(medTuple._1());
			final PropertyValue propValue1 = medTuple._2();
			final PropertyValue propValue2 = 
				((startDateTuple != null || endDateTuple != null) && isDateIntervalValid) ? 
					new DateIntervalTypeA(startDateTuple, endDateTuple): null;
			
			map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-CV
			map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Dosage
			map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#medication-Date-Interval
			
			mapList.add(map);
		}
		
		return Tuple2.newTuple2(Boolean.TRUE, mapList);
	}

}
