package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;

import java.util.ArrayList;
import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.ComplexCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping a Cohort Field about a Lab Test with one or more comma separated Terms. The corresponding Reference Model term should be given. Also, the values of this field should be independently ** aligned with ** the appropriate terms (i.e., ANA pattern or Cryo Type) specified in the Reference Model. */
public class LabTestOutcomeTerm extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test }
	
	/** Provides the appropriate Terms for the specific Lab Test along with its Outcome, based on the Value of the given Field, the parameters provided (i.e., corresponding Reference Model term) and the Mapping Rules already specified.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String termsStr = data.get(0); // Lab Test Outcome: Term(s)

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test is a Boolean 
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.TERM))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not a Term ! Possible Test Outcome(s): " + expOutSet);
		
		final List<String> strList = getTermsStrList(termsStr);
		
		// IF field is empty
		if (strList == null) return null;
		
		final List<Tuple2<TermExpr, Relation>> termExprTupleList = new ArrayList<>();
		for (String str : strList) {
			final Tuple2<TermExpr, Relation> testTermExprT = getReferenceModelTermForCohortValue(str, 0);
			if (testTermExprT == null) continue;
			// Ensure that the Corresponding Reference Model term neither a Confirmation nor an Assessment Term
			if (testTermExprT._1().isSimpleConfirmationTerm() || testTermExprT._1().isSimpleAssessmentTerm()) continue;
			termExprTupleList.add(testTermExprT);
		}
		
		// IF none term aligned with Reference Model terms
		if (termExprTupleList.isEmpty()) return null;
		
		final List<Map<Integer, PropertyValue>> mapList = new ArrayList<>();
		for (Tuple2<TermExpr, Relation> termExprTupleT : termExprTupleList) {
			final Map<Integer, PropertyValue> map = new HashMap<>();
			final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
			final PropertyValue propValue1 = 
				(termExprTupleT._1().isTerm()) ? new SimpleCodedValue(termExprTupleT) : new ComplexCodedValue(termExprTupleT);
			map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
			map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
			mapList.add(map);
		}
		
		return Tuple2.newTuple2(Boolean.TRUE, mapList);	
	}

}
