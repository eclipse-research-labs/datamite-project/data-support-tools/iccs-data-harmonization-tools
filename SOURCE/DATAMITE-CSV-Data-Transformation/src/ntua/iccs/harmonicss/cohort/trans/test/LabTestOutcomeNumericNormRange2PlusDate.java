package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.AmountValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.AmountInterval;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 2 Cohort Fields, A) A Field with the value measured for a specific Lab Test and B) Another Field with the Date it took place. However, for the evaluation of the data recorded, we also need to know C) the Sex of the person, which should be also given. The corresponding Reference Model Lab Test term should be specified. The Format of the number existing in the 1st Field and the Unit of Measurement should be given. Additionally we should know the Normal Range of values for both males and females. Also the format of the Date (2nd field) should be given. Last but not least, the values of the 3rd Field should be ** independently aligned ** with the Reference Model Sex terms. */
public class LabTestOutcomeNumericNormRange2PlusDate extends DataTransformation {

	private enum ServiceArg { Reference_Model_Blood_Test, Field_Number_Format, Field_Unit_of_Measurement, Male_Down_Normal_Limit_DNL, Male_Upper_Normal_Limit_UNL, Female_Down_Normal_Limit_DNL, Female_Upper_Normal_Limit_UNL, Date_Format }
	
	/** Provides the appropriate Term for the specific Lab Test, the Date it took place and the Outcome measured based on the Values of the given Fields and the additional parameters provided (i.e., corresponding Reference Model term, date format and unit of measurement - both current and desirable). The system also provides whether the value recorded was normal or not taking into account the sex of each person along with the normal range of values for each sex. In case the 1st Field is empty or no Unit of Measurement specified in the Reference Model about the specific Lab Test, no data will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testOutValue = data.get(0); // Lab Test Outcome: Numeric Value
		final String testDateStr = data.get(1); // Lab Test Date
		final String sexCode = data.get(2); // Sex/Gender

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Blood_Test);
		final String valueFormatIndex = getArgValue(ServiceArg.Field_Number_Format);
		final String testValueUnit = getArgValue(ServiceArg.Field_Unit_of_Measurement);
		final String maleDownNormLimitStr = getArgValue(ServiceArg.Male_Down_Normal_Limit_DNL);
		final String maleUpNormLimitStr = getArgValue(ServiceArg.Male_Upper_Normal_Limit_UNL);
		final String femaleDownNormLimitStr = getArgValue(ServiceArg.Female_Down_Normal_Limit_DNL);
		final String femaleUpNormLimitStr = getArgValue(ServiceArg.Female_Upper_Normal_Limit_UNL);
		final String testDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));

		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test can be an Amount
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.AMOUNT))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not an Amount ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple2<Float, Tuple2<String, String>> valueTuple = getAmountValueTuple(testOutValue, valueFormatIndex, testValueUnit, testTerm);
		
		// IF the value is empty or not a Number, no data will be recorded
		if (valueTuple == null) return null;
		
		final Tuple2<TermExpr, Relation> termExprTuple = getReferenceModelTermForCohortValue(sexCode, 2);
		
		// Ensure that the Corresponding Reference Model term is a Smoking Status Terms
		if (termExprTuple != null && !termExprTuple._1().isSimpleSexTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + termExprTuple._1().asString() + "\" is NOT a Sex Term !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(testDateStr, testDateFormat);
		
		// Find Normal Range based on the Sex
		final Tuple3<Float, Float, Tuple2<String, String>> normRangeTuple;
		if (termExprTuple != null) {
			if (termExprTuple._1().isSimpleSexTermMALE()) {
				normRangeTuple = getAmountNormalRangeTuple(maleDownNormLimitStr, maleUpNormLimitStr, testValueUnit, testTerm);
			} else if (termExprTuple._1().isSimpleSexTermFEMALE()) {
				normRangeTuple = getAmountNormalRangeTuple(femaleDownNormLimitStr, femaleUpNormLimitStr, testValueUnit, testTerm);
			} else {
				normRangeTuple = null;
			}
		} else {
			// Sex Unknown and hence Normal Range
			normRangeTuple = null;
		}
		
		final Term assessTerm = (normRangeTuple != null) ? getAssessmentForValue(valueTuple._1(), normRangeTuple._1(), normRangeTuple._2()) : null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = (dateTuple != null) ? new DateValue(dateTuple): null;
		final PropertyValue propValue2 = (valueTuple != null) ? new AmountValue(valueTuple): null;
		final PropertyValue propValue3 = (assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;
		final PropertyValue propValue4 = (normRangeTuple != null) ? new AmountInterval(normRangeTuple) : null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome-Assessment-Code
		map.put(4, propValue4); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Normal-Range
		
		return newPosTuple(map);
		
	}

}
