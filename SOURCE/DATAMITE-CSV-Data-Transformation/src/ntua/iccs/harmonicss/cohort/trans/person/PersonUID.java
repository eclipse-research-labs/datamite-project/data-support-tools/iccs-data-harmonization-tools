package ntua.iccs.harmonicss.cohort.trans.person;
import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.simple.StringValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** 
 * Mapping Cohort Fields about Patient Unique ID with the corresponding entity from the Reference Model.
 */
public class PersonUID extends DataTransformation {

	/** Provides the UID (i.e., the one existing in the given Field).
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Person that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String uid = data.get(0); // Patient Unique ID

		// Data Processing
		if (isEmpty(uid)) throw new RuntimeException("Patient ID cannot be empty !");
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new StringValue(uid); 

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#person-Unique-ID
		
		return newPosTuple(map);
	}

}