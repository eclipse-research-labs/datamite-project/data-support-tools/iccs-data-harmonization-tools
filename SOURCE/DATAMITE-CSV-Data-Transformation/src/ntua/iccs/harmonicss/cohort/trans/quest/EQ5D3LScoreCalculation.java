package ntua.iccs.harmonicss.cohort.trans.quest;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Util;

import java.util.HashMap;
import java.util.HashSet;

import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.interv.IntegerInterval;
import ntua.iccs.harmonicss.cohort.data.owl.simple.IntegerValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping Five Cohort Fields with the Level recorded (i.e., a positive integer in Set {1, 2, 3} ) for each one out of  5 dimensions of the EQ-5D-3L questionnaire. */
public class EQ5D3LScoreCalculation extends DataTransformation {

	private static final Logger log = LogFactory.getLoggerForClass(EQ5D3LScoreCalculation.class);
	
	private enum ServiceArg { Down_Normal_Limit_DNL, Upper_Normal_Limit_UNL }

	private static final Set<Integer> levelSet = new HashSet<Integer>(Util.newArrayList(1, 2, 3));
	
	/** Provides the questionnaire score (i.e., EQ-5D-3L) along with the value measured (based on the values of the given Fields) as well as the Normal Range of Value (if specified). In case any of the given Fields is empty or its value do not belong to {1, 2, 3} no data will be recorded. 
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Questionnaire-Score that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String mobilityLevelStr = data.get(0); // EQ-5D Mobility
		final String selfcareLevelStr = data.get(1); // EQ-5D Self Care
		final String usuActivLevelStr = data.get(2); // EQ-5D Usual Activities
		final String painLevelStr = data.get(3); // EQ-5D Pain
		final String anxietyLevelStr = data.get(4); // EQ-5D Anxiety

		// Additional Data / Arguments (if any)
		final String downNormLimit = getArgValue(ServiceArg.Down_Normal_Limit_DNL);
		final String upNormLimit = getArgValue(ServiceArg.Upper_Normal_Limit_UNL);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final Integer mobilityLevel = getInteger(mobilityLevelStr);
		if (mobilityLevel == null) return null;
		if (!levelSet.contains(mobilityLevel)) { log.warn("EQ-5D Mobility Level is NOT in Set {1, 2, 3} - EQ-5D Data being ignored !"); return null; }
		
		final Integer selfcareLevel = getInteger(selfcareLevelStr);
		if (selfcareLevel == null) return null;
		if (!levelSet.contains(selfcareLevel)) { log.warn("EQ-5D Self Care Level is NOT in Set {1, 2, 3} - EQ-5D Data being ignored !"); return null; }
		
		final Integer usuActivLevel = getInteger(usuActivLevelStr);
		if (usuActivLevel == null) return null;
		if (!levelSet.contains(usuActivLevel)) { log.warn("EQ-5D Usual Activities is NOT in Set {1, 2, 3} - EQ-5D Data being ignored !"); return null; }
		
		final Integer painLevel = getInteger(painLevelStr);
		if (painLevel == null) return null;
		if (!levelSet.contains(painLevel)) { log.warn("EQ-5D Pain Level is NOT in Set {1, 2, 3} - EQ-5D Data being ignored !"); return null; }
		
		final Integer anxietyLevel = getInteger(anxietyLevelStr);
		if (anxietyLevel == null) return null;
		if (!levelSet.contains(anxietyLevel)) { log.warn("EQ-5D Anxiety Level is NOT in Set {1, 2, 3} - EQ-5D Data being ignored !"); return null; }
		
		// Calculate Total Score based on the values/levels in the 5 dimensions
		final String scoreStr = mobilityLevel + "" + selfcareLevel + "" + usuActivLevel + "" + painLevel + "" + anxietyLevel;
		final Integer score = Integer.parseUnsignedInt(scoreStr);

		final Tuple2<Integer, Integer> normRangeTuple = getIntegerNormalRangeTuple(downNormLimit, upNormLimit);
		final Term assessTerm = (normRangeTuple != null) ? getAssessmentForValue(score, normRangeTuple._1(), normRangeTuple._2()) : null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue("QUEST-060", "EuroQol Group - Five Dimensions - Three Level scale (EQ-5D-3L) Health State"); 
		final PropertyValue propValue1 = new IntegerValue(score);
		final PropertyValue propValue2 = 
			(assessTerm != null) ? new SimpleCodedValue(assessTerm) : null;
		final PropertyValue propValue3 = 
			(normRangeTuple != null) ? new IntegerInterval(normRangeTuple): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#questionnaire-Score
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-Assessment-Code
		map.put(3, propValue3); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#score-Normal-Range
		
		return newPosTuple(map);
	}

}
