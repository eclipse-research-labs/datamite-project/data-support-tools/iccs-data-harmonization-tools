package ntua.iccs.harmonicss.cohort.trans.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.BooleanValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** Mapping 3 fields: A) A Field about a Lab Test with a Boolean Value (i.e., YES/NO) and B) Two other Fields that indicate the Date when the Lab Test took place. From these two Fields, one indicates the date an Event took place and the other one the Time (i.e., years/months/days) elapse since that date, when the test took place. The corresponding Reference Model Lab Test term should be given. Also, the values of the 1st field (with the test outcome) should be independently ** aligned with ** Confirmation terms (i.e., YES/NO values) specified in the Reference Model. Additionally the format of the Date (2nd field) along with the Unit of Time Elapse should be given. */
public class LabTestOutcomeBooleanPlusEventDateAndTimeElapse extends DataTransformation {

	private enum ServiceArg { Reference_Model_Lab_Test, Event_Date_Format, Elapse_Time_Unit }
	
	/** Provides the appropriate Terms for the specific Lab Test along with its Outcome and the Date it took place, based on the Value of the given Fields, the parameters provided (i.e., corresponding Reference Model term, Date format and Elapse Time Unit) and the Mapping Rules already specified. In case the 1st Field is empty or not aligned with the Confirmation terms, no data will be recorded. Also, in case the 2nd or 3rd Field is empty, no information about the Test Date will be recorded.
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Laboratory-Test that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testValue = data.get(0); // Lab Test Outcome: Boolean Value
		final String eventDateStr = data.get(1); // Event Date
		final String elapseTimeStr = data.get(2); // Elapse Time

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String eventDateFormat = getArgValue(ServiceArg.Event_Date_Format);
		final String elapseTimeUnit = getArgValue(ServiceArg.Elapse_Time_Unit);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test is a Boolean 
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.BOOLEAN))
			throw new RuntimeException("The expected outcome for the given test (" + testTerm.getLocalName() +") is not a Boolean ! Possible Test Outcome(s): " + expOutSet);
		
		final Tuple2<TermExpr, Relation> testConfirmValueTermExprT = getReferenceModelTermForCohortValue(testValue, 0);
		
		// IF the value is empty or not aligned with Reference Model confirmation terms, no data will be recorded
		if (testConfirmValueTermExprT == null) return null;

		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!testConfirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The corresponding Reference Model term \"" + testConfirmValueTermExprT._1().asString() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Integer, Integer, Integer> eventDateTuple = getDateTuple(eventDateStr, eventDateFormat);
		final Integer elapseTimeInt = getInteger(elapseTimeStr);
		final Tuple2<Integer, String> timeElapseTuple = (elapseTimeInt != null) ? Tuple2.newTuple2(elapseTimeInt, elapseTimeUnit) : null;
		// Provides the Date the test took place, provided that both the Event Date and the Time Elapse specified, with the necessary level of Details
		final Tuple3<Integer, Integer, Integer> testDateTuple = getDateTupleAfterTime(eventDateTuple, timeElapseTuple);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = new BooleanValue(testConfirmValueTermExprT);
		final PropertyValue propValue2 = 
			(testDateTuple != null) ? new DateValue(testDateTuple): null;

		map.put(0, propValue0); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Outcome
		map.put(2, propValue2); // Value of Property: http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#test-Date
		
		return newPosTuple(map);
	}

}
