package ntua.iccs.harmonicss.cohort.metanno;

import java.util.Date;

import javax.swing.JFrame;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.timchros.core.log.messages.MemoryMessage;

/** For Desktop Application - Creates GUI */
public class CohMetaMappingsAnnoMain {

	public static void main(String[] args) {
		
		System.out.println("CohMetaMappingsAnnoMain: RUN ON DATE: " + new Date());
		System.out.println();
		System.out.println("JVM memory: " + new MemoryMessage().toParsableString());
		System.out.println();
		
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.WARN);
		
		final CohMetaMappingsAnnoFrame frame = new CohMetaMappingsAnnoFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		frame.setVisible(true);
		
		System.out.println("CohMetaMappingsAnnoMain: Window for Cohort Metadata Annotation Successfully Created !");
	}
	
}
