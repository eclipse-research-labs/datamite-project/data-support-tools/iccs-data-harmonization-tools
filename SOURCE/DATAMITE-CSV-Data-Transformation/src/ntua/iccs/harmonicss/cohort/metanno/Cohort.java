package ntua.iccs.harmonicss.cohort.metanno;

public class Cohort {

//	// UoA
//	public static final String cohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/UoA/UoA-2019-06-18-Cohort-Metadata-v.0.7.1.xlsx";
//	public static final String mapFilePath = 
//		"./@ICCS-Cohorts-Metadata/UoA/Mapping.json";
//	public static final String annoCohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/UoA/UoA-2019-06-18-Cohort-Metadata-v.0.7.1-ANNO.xlsx";
	
//	// UOI
//	public static final String cohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/UOI/UOI-2019-07-07-Cohort-Metadata-v.0.3.2.xlsx";
//	public static final String mapFilePath = 
//		"./@ICCS-Cohorts-Metadata/UOI/Mapping.json";
//	public static final String annoCohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/UOI/UOI-2019-07-07-Cohort-Metadata-v.0.3.2-ANNO.xlsx";
	
//	// HUA
//	public static final String cohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/HUA/HUA-2019-05-02-Cohort-Metadata-FINAL-v.0.4.2.xlsx";
//	public static final String mapFilePath = 
//		"./@ICCS-Cohorts-Metadata/HUA/Mapping.json";
//	public static final String annoCohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/HUA/HUA-2019-05-02-Cohort-Metadata-FINAL-v.0.4.2-ANNO.xlsx";

//	// UNIRO
//	public static final String cohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/UNIRO/UNIRO-2019-04-15-Cohort-Metadata-FINAL-v.0.4.3.xlsx";
//	public static final String mapFilePath = 
//		"./@ICCS-Cohorts-Metadata/UNIRO/Mapping.json";
//	public static final String annoCohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/UNIRO/UNIRO-2019-04-15-Cohort-Metadata-FINAL-v.0.4.3-ANNO.xlsx";
	
//	// QMUL
//	public static final String cohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/QMUL/QMUL-2019-04-11-Cohort-Metadata-FINAL-v.0.3.xlsx";
//	public static final String mapFilePath = 
//		"./@ICCS-Cohorts-Metadata/QMUL/QMUL-2019-04-11-Mapping-Rules-v.1.0.json";
//	public static final String annoCohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/QMUL/QMUL-2019-04-11-Cohort-Metadata-FINAL-v.0.3-ANNO.xlsx";
			
	// UU
	public static final String cohMetadataFilePath = 
		"./@ICCS-Cohorts-Metadata/UU/UU-2018-10-02-Cohort-Metadata-FINAL-v.0.3.4.xlsx";
	public static final String mapFilePath = 
		"./@ICCS-Cohorts-Metadata/UU/Mapping.json";
	public static final String annoCohMetadataFilePath = 
		"./@ICCS-Cohorts-Metadata/UU/UU-2018-10-02-Cohort-Metadata-FINAL-v.0.3.4-ANNO.xlsx";
	
//	// OTHER
//	public static final String cohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/TEST-ATOS/UNIVAQ-2019-04-17 Cohort-Metadata-FINAL3 (2)_10-07-2019.xlsx";
//	public static final String mapFilePath = 
//		"./@ICCS-Cohorts-Metadata/TEST-ATOS/UNIVAQ-Mapping2.json";
//	public static final String annoCohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/TEST-ATOS/UNIVAQ-2019-04-17 Cohort-Metadata-FINAL3 (2)_10-07-2019-ANNO.xlsx";

//	// UiB
//	public static final String cohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/UiB/UiB-2019-07-13-Cohort-Metadata-v.0.3.2.xlsx";
//	public static final String mapFilePath = 
//		"./@ICCS-Cohorts-Metadata/UiB/Mapping.json";
//	public static final String annoCohMetadataFilePath = 
//		"./@ICCS-Cohorts-Metadata/UiB/UiB-2019-07-13-Cohort-Metadata-v.0.3.2-ANNO.xlsx";
	
	private Cohort() {
		
	}
	
}
