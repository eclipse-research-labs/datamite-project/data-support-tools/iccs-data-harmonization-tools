package ntua.iccs.harmonicss.cohort.metanno;

import static ntua.iccs.harmonicss.cohort.metanno.Cohort.annoCohMetadataFilePath;
import static ntua.iccs.harmonicss.cohort.metanno.Cohort.cohMetadataFilePath;
import static ntua.iccs.harmonicss.cohort.metanno.Cohort.mapFilePath;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.timchros.core.tuple.Tuple2;

import ntua.iccs.harmonicss.cohort.LogsArea;
import ntua.iccs.harmonicss.cohort.MappingRules;
import ntua.iccs.harmonicss.cohort.data.json.FieldsMappingRule;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.json.TermExpression;
import ntua.iccs.harmonicss.cohort.data.json.TermsMappingRule;

/** Highlight Metadata Fields Mapped */
public class CohMetaMappingsAnnoService {
	
	public static void main(String[] args) throws Exception {

		System.out.println(" >> CohMetaMappingsAnnoMain: Process STARTED at " + new Date());
		System.out.println();
		
		// Read Input Data from Command LineS
		final String userCohMetadataFilePath, userMapFilePath, userAnnoCohMetadataFilePath;
		if (args == null || args.length != 3) {
			System.out.println(" * Warning: No Data Provided - Default Arguments will be used *");
			System.out.println();
			userCohMetadataFilePath = cohMetadataFilePath;
			userMapFilePath = mapFilePath;
			userAnnoCohMetadataFilePath = annoCohMetadataFilePath;
		} else {
			userCohMetadataFilePath = args[0];
			userMapFilePath = args[1];
			userAnnoCohMetadataFilePath = args[2];
		}
		
		System.out.println(" - Cohort Metadata XLS File Path:  " + userCohMetadataFilePath);
		System.out.println(" - Mappings Rules JSON File Path:  " + userMapFilePath);
		System.out.println(" - Annotated Cohort Metadata Path: " + userAnnoCohMetadataFilePath);
		System.out.println();
		
		// Check Given Data
		if (userCohMetadataFilePath.trim().equals("") || userMapFilePath.trim().equals("") || userAnnoCohMetadataFilePath.trim().equals("")) {
			System.out.println("\n * At least one out of three input arguments is empty ! Please check.. "); return;
		}		
		
		annoMetadata(userCohMetadataFilePath, userMapFilePath, userAnnoCohMetadataFilePath);
		
		System.out.println();
		System.out.println(" >> CohMetaMappingsAnnoMain: Process COMPLETED at " + new Date());
	}

	private static final NumberFormat numFormater = new DecimalFormat("#.####");
	
	public static void annoMetadata(final String cohMetaFilePath, final String mapFilePath, final String newFilePath) throws Exception {

		// Mapping Rules
		final Tuple2<List<FieldsMappingRule>, List<TermsMappingRule>> mrTuple = MappingRules.loadMappingRulesFromJsonFile(mapFilePath);
		final Set<String> mapColSet = new HashSet<>();
		for (FieldsMappingRule mr : mrTuple._1()) {
			for (String col : mr.getSrcFieldsABC()) {
				mapColSet.add(col);
			}
		}
		final Map<Integer, HashSet<String>> termsMap = new HashMap<>();
		for (TermsMappingRule mr : mrTuple._2()) {
			final TermExpr termExpr = mr.getSrcTermExpr();
			if (termExpr instanceof Term) {
				final Term term = (Term) termExpr;
				final int domainIndex = getTermDomainIndex(term);
				if (!termsMap.containsKey(domainIndex)) termsMap.put(domainIndex, new HashSet<>());	
				termsMap.get(domainIndex).add(term.getLabel());
			} else {
				final TermExpression termExpression = (TermExpression) termExpr;
				for (TermExpr inTermExpr : termExpression.getTermExprList()) {
					if (inTermExpr instanceof Term) {
						final Term interm = (Term) inTermExpr;
						final int domainIndex = getTermDomainIndex(interm);
						if (!termsMap.containsKey(domainIndex)) termsMap.put(domainIndex, new HashSet<>());	
						termsMap.get(domainIndex).add(interm.getLabel());
					}
				}
			}
		}

		// Cohort Metadata
		final InputStream is = new FileInputStream( new File(cohMetaFilePath));
		@SuppressWarnings("resource")
		final Workbook workbook = new XSSFWorkbook( is );
		is.close();
		
		final CellStyle fcs = workbook.createCellStyle();
		fcs.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		fcs.setFillBackgroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		fcs.setFillPattern(FillPatternType.SPARSE_DOTS);
		fcs.setAlignment(HorizontalAlignment.LEFT);
		fcs.setBorderRight(BorderStyle.THIN);
		
		final Sheet sF = workbook.getSheetAt(1);
		final Iterator<Row> sFrowIt = sF.rowIterator();
		int rowCount = 0;
		while (sFrowIt.hasNext()) {
			final Row row = sFrowIt.next();
			final Cell cell = row.getCell(0);
			final String col = ( cell != null) ? cell.getStringCellValue() : null;
			if (mapColSet.contains(col)) {
				rowCount++;
				cell.setCellStyle(fcs);
			}
		}
		LogsArea.println();
		LogsArea.println(" * Number of 2nd sheet Fields/Rows affected: " + rowCount);
		
		final CellStyle tcs = workbook.createCellStyle();
		tcs.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		tcs.setFillBackgroundColor(IndexedColors.LIGHT_GREEN.getIndex());
		tcs.setFillPattern(FillPatternType.SPARSE_DOTS);
		tcs.setWrapText(true);
		tcs.setAlignment(HorizontalAlignment.LEFT);
		tcs.setBorderLeft(BorderStyle.THIN);
		
		final Sheet tF = workbook.getSheetAt(2);
		final Iterator<Row> tFrowIt = tF.rowIterator();
		int termCount = 0;
		int rowIndex = 0;
		while (tFrowIt.hasNext()) {
			final Row row = tFrowIt.next();
			rowIndex++;
			if (rowIndex <= 2) continue;
			final Iterator<Cell> cellIt =  row.cellIterator();
			while (cellIt.hasNext()) {
				final Cell cell = (Cell) cellIt.next();
				if (cell == null) continue;
				final String cellstr;
				try {
					if (cell.getCellTypeEnum() == CellType.STRING) {
						cellstr = cell.getStringCellValue().trim();
					} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
						cellstr = "" + numFormater.format(cell.getNumericCellValue());
					} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
						cellstr = "" + cell.getBooleanCellValue();
					} else {
						cellstr = cell.toString().trim();
					}
				} catch (Throwable t) {
					throw new Exception("Cannot get value of Cell:  [" + (rowIndex - 1) + " , " + cell.getColumnIndex() + "] existing in the 3nd Sheet.", t);
				}

				
				if (cellstr.equals("")) continue;
				final int colIndex = cell.getColumnIndex();
				if (colIndex % 2 != 0) continue;
				final int domainindex = (colIndex) / 2;
				final String term = cellstr;
//				if (domainindex == 0) {
//					System.out.println(colIndex + " :: " + term);
//					System.out.println("\t" + termsMap.get(domainindex + 1));
//				}
				if (termsMap.containsKey(domainindex + 1) && termsMap.get(domainindex + 1).contains(term)) {
					termCount++;
			        cell.setCellStyle(tcs);
				}
			}
		}
		LogsArea.println();
		LogsArea.println(" * Number of 3rd sheet Terms/Cells affected: " + termCount);
		
		// Save to new File
		final FileOutputStream fileOut = new FileOutputStream(newFilePath);
	    workbook.write(fileOut);
	    fileOut.close();
		workbook.close();
	}
	
//	/** @return Data existing in a specific Cell of an Excel File in the form of a String */
//	private static String getCellData(Cell cell) {
//		if (cell == null || cell.toString().trim().equals("")) return null;
//		if (cell.getCellTypeEnum() == CellType.STRING) {
//			return cell.getStringCellValue().replaceAll("\\s+", " ").trim();
//		} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
//			if (HSSFDateUtil.isCellDateFormatted(cell)) {
//				return dateFormater.format(cell.getDateCellValue());
//		    } else {
//		    	return numFormater.format(cell.getNumericCellValue());
//		    }
//		} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
//			return ("" + cell.getBooleanCellValue()).toLowerCase();
//		} else if (cell.getCellTypeEnum() == CellType.FORMULA) {
//			return "FORMULA: " + cell;
//		} else if (cell.getCellTypeEnum() == CellType.BLANK) {
//			return null;
//		} else if (cell.getCellTypeEnum() == CellType.ERROR) {
//			return null;
//		} else if (cell.getCellTypeEnum() == CellType._NONE) {
//			System.out.println("NONE " + cell);
//			return "NONE: " + cell;
//		} else {
//			System.out.println(" ****** " + cell);
//			return cell.toString().trim();
//		}
//		
//	}

	private static int getTermDomainIndex(final Term term) {
		final String str = term.getLocalName();
		return Integer.parseInt(str.substring(7, 10));
	}
	
}
