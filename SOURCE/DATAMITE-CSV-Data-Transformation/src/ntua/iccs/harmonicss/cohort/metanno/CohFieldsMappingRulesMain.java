package ntua.iccs.harmonicss.cohort.metanno;

import static ntua.iccs.harmonicss.cohort.metanno.Cohort.cohMetadataFilePath;
import static ntua.iccs.harmonicss.cohort.metanno.Cohort.mapFilePath;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.timchros.core.tuple.Tuple2;

import ntua.iccs.harmonicss.cohort.MappingRules;
import ntua.iccs.harmonicss.cohort.data.json.FieldsMappingRule;
import ntua.iccs.harmonicss.cohort.data.json.TermsMappingRule;

public class CohFieldsMappingRulesMain {

	public static void main(String[] args) throws Exception {

		System.out.println(" >> CohFieldsMappingRulesMain: Process STARTED at " + new Date());
		System.out.println();
		
		final String mapGraphFilePath = mapFilePath + "-Mapping-Graph.dot";
		
		createGraph(cohMetadataFilePath, mapFilePath, mapGraphFilePath);
		
		System.out.println();
		System.out.println(" >> CohFieldsMappingRulesMain: Process COMPLETED at " + new Date());
	}

	private static String NL = "\n";
	private static String TAB = "\t";
	
	@SuppressWarnings("unused")
	public static void createGraph(final String cohMetaFilePath, final String mapFilePath, final String newFilePath) throws Exception {
		final Tuple2<List<FieldsMappingRule>, List<TermsMappingRule>> mrTuple = MappingRules.loadMappingRulesFromJsonFile(mapFilePath);
		final Set<String> colSet = new HashSet<>();
		for (FieldsMappingRule mr : mrTuple._1()) {
			for (String col : mr.getSrcFieldsABC()) {
				colSet.add(col);
			}
		}
		final List<String> colList = new ArrayList<String>(colSet);
		Collections.sort(colList, new Comparator<String>() {
			@Override public int compare(String s1, String s2) {
				final int l1 = s1.length();
				final int l2 = s2.length();
				return (l1 == l2) ? s1.compareTo(s2) : l1 - l2 ;			}
		});
		System.out.println(colList);
		
		final StringBuilder sb = new StringBuilder();
		sb.append("digraph G {" + NL);
		sb.append(NL);
		sb.append(TAB + "rankdir=LR;" + NL);
		sb.append(NL);
		// Fields
		sb.append(TAB + "subgraph cluster_Fields {" + NL);
		sb.append(NL);
		sb.append(TAB + TAB + "style=filled;" + NL);
		sb.append(TAB + TAB + "color=lightgrey;" + NL);
		sb.append(TAB + TAB + "node [style=filled,color=white];" + NL);
		sb.append(NL);
		for (String column : colList) {
			sb.append(TAB + TAB + column + ";" + NL);
		}
//		sb.append(TAB + TAB + "label = \"Cohort Fields\";" + NL);
//		sb.append(TAB + "}" + NL);
		sb.append(NL);
		// Mapping Rules
//		sb.append(TAB + "subgraph cluster_MappingRules {" + NL);
//		sb.append(TAB + "style=filled;" + NL);
//		sb.append(TAB + "color=lightgrey;" + NL);
//		sb.append(TAB + "node [style=filled,color=white];" + NL);
		int mrIndex = 0;
		for(FieldsMappingRule mr : mrTuple._1()) {
			sb.append(TAB + TAB + "MR" + mrIndex + ";" + NL);
			mrIndex++;
		}
//		sb.append(TAB + TAB + "label = \"Mapping Rules\";" + NL);
//		sb.append(TAB + "}" + NL);
		sb.append(NL);
		// Links
		mrIndex = 0;
		for(FieldsMappingRule mr : mrTuple._1()) {
			for (String col : mr.getSrcFieldsABC()) {
				sb.append(TAB + TAB + col + " -> MR" + mrIndex + ";" + NL);
			}
			mrIndex++;
		}
		
		sb.append(NL);
		for(FieldsMappingRule mr : mrTuple._1()) {
			final StringBuilder tmpSb = new StringBuilder();
			for (String col : mr.getSrcFieldsABC()) {
				tmpSb.append(col + "; ");
			}
			sb.append(TAB + TAB + "{rank = same; MR" + mrIndex + "; " + tmpSb + "}" + NL);
			mrIndex++;
		}
		
		sb.append(NL);
		sb.append(TAB + "}" + NL);
		sb.append(NL);
		sb.append("}" + NL);
		
		// Save to File
		Files.write(Paths.get(newFilePath), sb.toString().getBytes());
	}
	
}
