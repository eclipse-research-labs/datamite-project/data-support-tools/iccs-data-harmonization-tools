package ntua.iccs.harmonicss.cohort.other;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.timchros.core.util.Format;
import org.timchros.core.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MappingScenariosUsedMain {

	private static final String scenariosJsonFileLocalPath = "./@ICCS-Cohorts-Metadata/Mapping-Scenarios.json";
	
	private static final List<String> mapFilePathList = Util.newArrayList(
		"./@ICCS-Cohorts-Metadata/QMUL/Mapping.json",
		"./@ICCS-Cohorts-Metadata/UU/Mapping.json",
		"./@ICCS-Cohorts-Metadata/UNIRO/Mapping.json",
		"./@ICCS-Cohorts-Metadata/UNIPI/Mapping.json",
		"./@ICCS-Cohorts-Metadata/UiB/Mapping.json",
		"./@ICCS-Cohorts-Metadata/UoA/Mapping.json",
		"./@ICCS-Cohorts-Metadata/UOI/Mapping.json",
		"./@ICCS-Cohorts-Metadata/HUA/Mapping.json"
	);
	
	public static void main(String[] args) throws Exception {
		
		System.out.println(" >> MappingScenariosUsedMain: Process STARTED at " + new Date());
		System.out.println();
		
		// Mapping Scenarios
		System.out.println(" * Reading Mapping Scenarios");
		final InputStream msis = new FileInputStream(scenariosJsonFileLocalPath);
		final String msJsonStr = IOUtils.toString(msis, StandardCharsets.UTF_8.name());
		msis.close();
		final JSONObject msJson = JSONObject.fromObject(msJsonStr);
		
		final Map<String, Integer> msUseMap = new HashMap<String, Integer>(); 
		
		// Files with Mapping Rules
		for (String mapFilePath : mapFilePathList) {
			System.out.println(" - Processing File: " + mapFilePath);
			final InputStream mris = new FileInputStream(mapFilePath);
			final String mrJsonStr = IOUtils.toString(mris, StandardCharsets.UTF_8.name());
			mris.close();
			
			final JSONObject mrJson = JSONObject.fromObject(mrJsonStr);
			
			final JSONArray mrJA = mrJson.getJSONObject("correspondences").getJSONArray("jsonarray");
			for (Object mr : mrJA) {
				final JSONObject mrJO = (JSONObject) mr;
				final Object dt = mrJO.get("directTransformation");
				final JSONObject dtJO = (dt instanceof JSONObject) ? (JSONObject) dt : null;
				final String uri = (dtJO != null) ? dtJO.getString("uri") : null;
				if (uri != null && uri.startsWith("CLASS:")) {
					final String cls = uri.substring(6).trim();
					if (msUseMap.containsKey(cls)) {
						msUseMap.put(cls, msUseMap.get(cls) + 1);
					} else {
						msUseMap.put(cls, 1); // new Integer(1)
					}
				}
				
			}
			
		}
		System.out.println("\nProcesssing successfully completed ... \n");
		
		final List<Entry<String, Integer>> entryList = new ArrayList<Map.Entry<String,Integer>>(msUseMap.entrySet());
		Collections.sort(entryList, new Comparator<Entry<String, Integer>>() {
			@Override public int compare(Entry<String, Integer> e1, Entry<String, Integer> e2) {
				return e2.getValue() - e1.getValue();
			}
		});
		
		int mrcount = 0;
		for (Entry<String, Integer> entry : entryList) {
			mrcount += entry.getValue();
		}
		
		System.out.println("Mapping Scenarios used: " + entryList.size() + "\n");
		System.out.println("   " + Format.formatStringLeftByMaxLength("Mapping Scenario Class", 90) + "   Number");
		System.out.println(Format.makeStringWithLength("-", 110));
		float sumpercent = 0;
		for (Entry<String, Integer> entry : entryList) {
			final float percent = 100 * (float) entry.getValue() / mrcount;
			sumpercent += percent;
			System.out.println(" - " + Format.formatStringLeftByMaxLength(entry.getKey(), 90, ".") + " : " + entry.getValue() + " - " + percent + " " + sumpercent);
		}
		System.out.println("\n");
		
		
		int index = 0;
		mrcount = mrcount - 13;
		sumpercent = 0;
		for (Entry<String, Integer> entry : entryList) {
			index++;
			final float percent = 100 * (float) entry.getValue() / mrcount;
			sumpercent += percent;
			System.out.println(getLabel(entry.getKey(), index) + "\t" + entry.getValue() + "\t" + sumpercent);
		}
		
		System.out.println("Mapping Scenarios used: " + entryList.size() + "\n");
		System.out.println("   " + Format.formatStringLeftByMaxLength("Mapping Scenario", 90) + "   Number");
		System.out.println(Format.makeStringWithLength("-", 110));
		for (Entry<String, Integer> entry : entryList) {
			System.out.println(" - " + Format.formatStringLeftByMaxLength(getScenarioNameBasedOnJavaClass(msJson, entry.getKey()), 90, ".") + " : " + entry.getValue());
		}
		
		System.out.println();
		System.out.println(" >> MappingScenariosUsedMain: Process COMPLETED at " + new Date());
		
	}
	
	public static String getScenarioNameBasedOnJavaClass(final JSONObject msJson, final String clsName) {
		final JSONArray categJA = msJson.getJSONArray("categJA");
		JSONObject msJO = null;
		for (int i = 0; i < categJA.size(); i++) {
			final JSONObject categJO = categJA.getJSONObject(i);
			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
			for (int j = 0; j < mapscenJA.size(); j++) {
				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);
				if (!mapscenJO.has("datatrans")) continue;
				final JSONObject datatransJO = mapscenJO.getJSONObject("datatrans");
				final String dturi = datatransJO.getString("uri");
				
				if (dturi.trim().endsWith(clsName)) {
					msJO = mapscenJO; break;
				}
			} // END OF mapscenJA
			if (msJO != null) break;
		} // END OF categJA
		return msJO.getString("name");
	}
	
	public static String getLabel(final String str, final int msIndex) {
		final int dotIndex = str.lastIndexOf('.');
		final String msName = str.substring(dotIndex + 1);
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < msName.length(); i++) {
			final char c = msName.charAt(i);
			if (Character.isUpperCase(c)) sb.append(c);
		}
		return sb + " (" + msIndex + ")";
	}
	
}
