package ntua.iccs.harmonicss.cohort.other;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ViewMappingScenariosMain {

	private static final String scenariosJsonFileLocalPath = "./@ICCS-Cohorts-Metadata/Mapping-Scenarios.json";
	
	public static void main(String[] args) throws Exception {

		final InputStream msis = new FileInputStream(scenariosJsonFileLocalPath);
		final String jsonStr = IOUtils.toString(msis, StandardCharsets.UTF_8.name());
		msis.close();
		
		int msCount = 0;
		
		final JSONObject msJson = JSONObject.fromObject(jsonStr);
		final JSONArray categJA = msJson.getJSONArray("categJA");
		for (int i = 0; i < categJA.size(); i++) {
			final JSONObject categJO = categJA.getJSONObject(i);
			
			// Category Name
			final String categ = (categJO.has("categ")) ? categJO.getString("categ") : null;
			
			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
			
			System.out.println(categ.toUpperCase() + " " + mapscenJA.size());
			System.out.println();
			
			for (int j = 0; j < mapscenJA.size(); j++) {
				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);

				// Mapping Scenario Name & Description
				final String name = (mapscenJO.has("name")) ? mapscenJO.getString("name") : null;
				final String desc = (mapscenJO.has("desc")) ? mapscenJO.getString("desc") : null;
				
				System.out.println(name);
				System.out.println(desc);
				System.out.println();
				
				msCount++;
				
			} // END OF mapscenJA
			
		} // END OF categJA
		
		System.out.println(" ** " + msCount + " Mapping Scenarios **");
		
	}

}
