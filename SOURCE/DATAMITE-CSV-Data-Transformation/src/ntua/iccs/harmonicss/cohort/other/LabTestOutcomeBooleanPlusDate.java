package ntua.iccs.harmonicss.cohort.other;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.Relation;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.BooleanValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

public class LabTestOutcomeBooleanPlusDate extends DataTransformation {
	
	private enum ServiceArg { Reference_Model_Lab_Test, Date_Format }
	
	@Override public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
		final String testValue = data.get(0); // Medical Test Outcome: Boolean Value
		final String testDateStr = data.get(1); // Medical Test Date

		// Additional Data / Arguments (if any)
		final String testTermUri = getArgValue(ServiceArg.Reference_Model_Lab_Test);
		final String testDateFormat = getArgValue(ServiceArg.Date_Format);
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing
		final OntClass testTerm = getReferenceModelTermWithURI(testTermUri);
		
		// Ensure that the value of the given Test is a Boolean 
		final Set<TestOutcome> expOutSet = getTestPossibleOutcome(testTerm);
		if (!expOutSet.contains(TestOutcome.BOOLEAN))
			throw new RuntimeException("The outcome for the given test (" + testTerm +") is NOT a Boolean !");
		
		final Tuple2<TermExpr, Relation> testConfirmValueTermExprT = getReferenceModelTermForCohortValue(testValue, 0);
		
		// IF the value is empty, no data will be recorded
		if (testConfirmValueTermExprT == null) return null;

		// Ensure that the Corresponding Reference Model term is a Confirmation Term
		if (!testConfirmValueTermExprT._1().isSimpleConfirmationTerm()) 
			throw new RuntimeException("The term \"" + testConfirmValueTermExprT._1() + "\" is NOT a Confirmation Term !");
		
		final Tuple3<Integer, Integer, Integer> dateTuple = getDateTuple(testDateStr, testDateFormat);
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
		final PropertyValue propValue0 = new SimpleCodedValue(testTerm);
		final PropertyValue propValue1 = new BooleanValue(testConfirmValueTermExprT);
		final PropertyValue propValue2 = (dateTuple != null) ? new DateValue(dateTuple): null;
				
		map.put(0, propValue0); // Value of Property: http://.../reference-model#test-CV
		map.put(1, propValue1); // Value of Property: http://.../reference-model#test-Outcome
		map.put(2, propValue2); // Value of Property: http://.../reference-model#test-Date
		
		return newPosTuple(map);
	}
	
}

