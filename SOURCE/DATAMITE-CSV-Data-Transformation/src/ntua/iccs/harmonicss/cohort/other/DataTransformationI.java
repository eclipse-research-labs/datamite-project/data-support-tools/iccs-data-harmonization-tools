package ntua.iccs.harmonicss.cohort.other;

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;

/** 
 * A Data Transformation Service should implement this interface 
 */
public interface DataTransformationI {

	/** 
	 * Records a {@link Map} with the Additional Parameters specified for a specific Mapping Rule
	 */
	public void setArgs(Map<String, String> args);
	
	/**
	 * Provides an Entity with the data recorded in the corresponding MS Excel Field(s) 
	 * formally expressed using the terms specified in the Reference Model. The 1st element
	 * indicates whether the patient is linked with the data (affirmation) or not (negation).
	 * The 2nd element indicates the amount of entities we should create along with the value
	 * of their properties. 
	 * 
	 * @param A {@link Map} with the Data recorded for a Patient in the corresponding Fields
	 * 
	 * @return An {@link Tuple2} with the Harmonized Patient Data
	 */
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data);
	
}





