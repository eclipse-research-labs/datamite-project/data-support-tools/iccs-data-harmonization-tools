package ntua.iccs.harmonicss.cohort.dev;

import static ntua.iccs.harmonicss.cohort.MappingRules.argNameClean;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.timchros.core.tuple.Tuple2;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/** Run using the following command:
 * 	CMD: java -cp <JAR-NAME>.jar GenDataTransJavaClassMain <Mapping-Scenarios-JSON-File-Path> <Data-Transformation-Service-Java-Class-Name> */
public class GenDataTransJavaClassMain {

	private static final String dtTemplateFilePath = "TEXT/Data-Transformation-Service-Template.java";
	
	private static final String scenariosJsonFileLocalPath = "./@ICCS-Cohorts-Metadata//Mapping-Scenarios.json";
	private static final String scenarioSearchDataTrans = 
		"ntua.iccs.harmonicss.cohort.trans.med.DrugDosageAdmin";
	
	public static void main(String[] args) throws Exception {

		System.out.println(">> GenDataTransJavaClassMain: Process STARTED at " + new Date());
		System.out.println();

		// Read Input Data from Command LineS
		final String scenariosJsonFilePath;
		final String scenarioDataTransClsName;
		if (args == null || args.length != 2) {
			System.out.println(" * Warning: No Data Provided - Default Arguments will be used *");
			System.out.println();
			scenariosJsonFilePath = scenariosJsonFileLocalPath;
			scenarioDataTransClsName = scenarioSearchDataTrans;
		} else {
			scenariosJsonFilePath = args[0];
			scenarioDataTransClsName = args[1];
		}
		
		System.out.println(" - Mapping-Scenarios-JSON-File-Path: " + scenariosJsonFilePath);
		System.out.println(" - Service-Java-Class-Name: " + scenarioDataTransClsName);
		System.out.println();
		
		// Check Given Data
		if (scenariosJsonFilePath.trim().equals("")) {
			System.out.println("\n * path/to/file/MappingScenarios.json NOT specified !"); return;
		}
		if (scenarioDataTransClsName.trim().equals("")) {
			System.out.println("\n * data.transformation.class.name NOT specified !"); return;
		}
		
		// Read text (template) from File
		InputStream is = GenDataTransJavaClassMain.class.getClassLoader().getResourceAsStream(dtTemplateFilePath);
		if (is == null) is = new FileInputStream(dtTemplateFilePath);
		final String template = IOUtils.toString(is, StandardCharsets.UTF_8.name());
		is.close();
		
		// Search for Mapping Scenario
		final InputStream msis = new FileInputStream(scenariosJsonFilePath);
		final String jsonStr = IOUtils.toString(msis, StandardCharsets.UTF_8.name());
		msis.close();
		final JSONObject msJson = JSONObject.fromObject(jsonStr);
		final JSONArray categJA = msJson.getJSONArray("categJA");
		JSONObject msJO = null;
		for (int i = 0; i < categJA.size(); i++) {
			final JSONObject categJO = categJA.getJSONObject(i);
			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
			for (int j = 0; j < mapscenJA.size(); j++) {
				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);
				if (!mapscenJO.has("datatrans")) continue;
				final JSONObject datatransJO = mapscenJO.getJSONObject("datatrans");
				final String dturi = datatransJO.getString("uri");
				
				if (dturi.trim().endsWith(scenarioDataTransClsName)) {
					msJO = mapscenJO; break;
				}
			} // END OF mapscenJA
			if (msJO != null) break;
		} // END OF categJA
		
		if (msJO == null) { 
			System.out.println("None Mapping Scenario found !");
		} else {
			System.out.println("Mapping Scenario JSON: \n" + msJO.toString(5) + "\n");
			
			// Mapping Scenario Description
			final String clsDesc = (msJO.has("desc")) ? msJO.getString("desc") : null;
			
			// Input Fields
			final JSONObject entity1JO = msJO.getJSONObject("entity1");
			final List<String> fieldsList = new ArrayList<>();
			if (entity1JO.has("ja") && !entity1JO.getJSONArray("ja").isEmpty()) {
				for (Object obj : entity1JO.getJSONArray("ja")) {
					final JSONObject fieldJO = (JSONObject) obj;
					fieldsList.add(fieldJO.getString("name"));
				}
			}
						
			// Data Transformation Description
			final JSONObject datatransJO = msJO.getJSONObject("datatrans");
			final String dtDesc = (datatransJO.has("desc")) ? datatransJO.getString("desc") : null;
			
			// Additional Data / Arguments
			final List<String> argsList = new ArrayList<>();
			if (datatransJO.has("ja") && !datatransJO.getJSONArray("ja").isEmpty()) {
				for (Object obj : datatransJO.getJSONArray("ja")) {
					final JSONObject argJO = (JSONObject) obj;
					argsList.add( argNameClean(argJO.getString("name")));
				}
			}
			
			// Class - Properties
			final JSONObject entity2JO = msJO.getJSONObject("entity2");
			final String destinEntityURI = entity2JO.getString("uri");
			final List<String> propList = new ArrayList<>();
			if (entity2JO.has("ja") && !entity2JO.getJSONArray("ja").isEmpty()) {
				for (Object obj : entity2JO.getJSONArray("ja")) {
					final JSONObject propJO = (JSONObject) obj;
					propList.add(propJO.getString("uri"));
				}
			}
			
			final Tuple2<String, String> clsTuple = getClsTuple(scenarioDataTransClsName);
			final String argsListStr = argsList.toString();
			
			final StringBuilder inFieldsSb = new StringBuilder();
			for (int i = 0; i < fieldsList.size(); i++) {
				String name = fieldsList.get(i);
				inFieldsSb.append("\t\tfinal String value" + i + " = data.get(" + i + "); // " + name);
				if (i < fieldsList.size() - 1) inFieldsSb.append("\n");
			}
			
			final StringBuilder inArgsSb = new StringBuilder();
			for (int i = 0; i < argsList.size(); i++) {
				String arg = argsList.get(i);
				inArgsSb.append("\t\tfinal String arg" + i + " = getArgValue(ServiceArg." + arg + ");");
				if (i < argsList.size() - 1) inArgsSb.append("\n");
			}
			
			final StringBuilder propValueSb = new StringBuilder();
			for (int i = 0; i < propList.size(); i++) {
				propValueSb.append("\t\tfinal PropertyValue propValue" + i + " = null; // TODO: update \n");
			}
			propValueSb.append("\n");
			for (int i = 0; i < propList.size(); i++) {
				String propUri = propList.get(i);
				propValueSb.append("\t\tmap.put(" + i + ", propValue" + i + "); // Value of Property: " + propUri);
				if (i < propList.size() - 1) propValueSb.append("\n");
			}
			
			final String clsFileContent = template
				.replace("${PACKAGE-NAME}", clsTuple._1())
				.replace("${CLASS-DESC}", clsDesc)
				.replace("${CLASS-NAME}", clsTuple._2())
				.replace("${ARGS-NAME}", argsListStr.substring(1, argsListStr.length()-1))
				.replace("${METHOD-DESC}", dtDesc)
				.replace("${METHOD-BODY-IN-FIELDS}", inFieldsSb.toString())
				.replace("${METHOD-BODY-IN-ARGS}", inArgsSb.toString())
				.replace("${ENTITY-CLASS}", destinEntityURI)
				.replace("${METHOD-BODY-OUT}", propValueSb);
			
			final String javaFilePath = clsTuple._2() + ".java"; 
			Files.write(Paths.get(javaFilePath), clsFileContent.getBytes());
			System.out.println("Generated JAVA class successfully stored to File: \"" + javaFilePath + "\"\n");
			
			System.out.println("JAVA CLASS: \n\n" + clsFileContent);
			
			System.out.println();
			System.out.println(">> GenDataTransJavaClassMain: Process FINISHED at " + new Date());
			
		} // END OF main()
		
	}

	private static Tuple2<String, String> getClsTuple(final String cls) {
		if (cls == null || cls.equals("")) return null;
		final int dotIndex = cls.lastIndexOf('.');
		if (dotIndex < 0) return new Tuple2<String, String>("", cls); //throw new RuntimeException("Cannot find doc (.) in the class name: " + cls);
		final String pack = cls.substring(0, dotIndex);
		final String name = cls.substring(dotIndex + 1);
		
		return new Tuple2<String, String>(pack, name);
	}
	
}
