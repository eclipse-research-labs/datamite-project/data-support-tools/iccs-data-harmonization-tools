package ntua.iccs.harmonicss.cohort.ws;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DataTransformationOutput {

	private String status;
	private String url;
	
	public DataTransformationOutput() {
		
	}

	public DataTransformationOutput(String status, String url) {
		super();
		this.status = status;
		this.url = url;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "DataTransformationOutput [status=" + status + ", url=" + url
				+ "]";
	}
	
}
