package ntua.iccs.harmonicss.cohort.ws;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.log4j.Logger;
import org.timchros.core.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.LogFactory;

public class NextCloudServiceClient {
	
	public static final Logger log = LogFactory.getLoggerForClass(NextCloudServiceClient.class);
	
	private static final class Authenticator implements ClientRequestFilter {

	    private final String user;
	    private final String password;

	    public Authenticator(String user, String password) {
	        this.user = user;
	        this.password = password;
	    }

	    public void filter(ClientRequestContext requestContext) throws IOException {
	    	final  MultivaluedMap<String, Object> headers = requestContext.getHeaders();
	        final String basicAuthentication = getBasicAuthentication();
	        headers.add("Authorization", basicAuthentication);

	    }

	    private String getBasicAuthentication() {
	        String token = this.user + ":" + this.password;
	        try {
	            return "Basic " + DatatypeConverter.printBase64Binary(token.getBytes("UTF-8"));
	        } catch (UnsupportedEncodingException ex) {
	            throw new IllegalStateException("Cannot encode with UTF-8", ex);
	        }
	    }
	    
	}
	
	private static final class NoOpTrustManager implements X509TrustManager {
    	
        @Override
        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) { 
        	
        }

        @Override
        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {  
        	
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {  
        	return new X509Certificate[0]; 
        }
        
    }
	
    private static SSLContext getSslContext() throws NoSuchAlgorithmException, KeyManagementException {
		final SSLContext sslContext = SSLContext.getInstance("TLSv1");
		final KeyManager[] keyManagers = null;
		final TrustManager[] trustManager = {new NoOpTrustManager()};
		final SecureRandom secureRandom = new SecureRandom();
		sslContext.init(keyManagers, trustManager, secureRandom);
		return sslContext;
	}
    

    /* ******************************* About User Databases ******************************* */
    
    private static final String userDbURL = 
    	"https://private.harmonicss.eu/index.php/apps/coh/api/1.0/info";
    
    private static JSONArray getUserDbJson(final String username, final String password) throws Exception {
    	final SSLContext sslContext = getSslContext();
    	final HostnameVerifier allHostsValid = new NoopHostnameVerifier();
    	final Client client = ClientBuilder.newBuilder().sslContext(sslContext).hostnameVerifier(allHostsValid).build();
        client.register(new Authenticator(username, password));
        final WebTarget webTarger = client.target(userDbURL);
		final Response responseObj = webTarger.request().get();
		if ( responseObj.getStatus() != 200 ) throw new RuntimeException("getUserLoginJson(..): " + responseObj.getStatus() + " :: " + responseObj.getStatusInfo());
		final String responseStr = responseObj.readEntity(String.class);
    	return JSONArray.fromObject(responseStr);
    }
    
    public static synchronized List<DbConnData> getUserDbList(final String username, final String password) {
    	try {
    		log.info("getUserDbList() user: " + username);
	    	final JSONArray dbJA = getUserDbJson(username, password); 
	    	final List<DbConnData> dbList = new ArrayList<>();
	    	for (Object dbObj : dbJA) {
				final JSONObject jo = (JSONObject) dbObj;
				final String dbName = jo.getString("dbarea");
				final String dbUsername = jo.getString("dbuname");
				final String dbPassword = jo.getString("dbupass");
				final String dbServerIP = jo.getString("dbserver");
				final String dbPort = jo.getString("dbport");
				dbList.add(new DbConnData(dbServerIP, dbPort, dbName, dbUsername, dbPassword));
			}
	    	log.info("getUserDbList() user: " + username + " successfully completed !");
	    	return dbList;
    	} catch(Throwable t) {
    		log.error("getUserDbList() Cannot find user \"" + username + "\" database(s) ...", t);
    		return new ArrayList<>();
    	}
    }

	
    /* ******************************* About User Login ******************************* */
    
    private static final String userLoginURL = 
    	"https://private.harmonicss.eu/ocs/v1.php/cloud/users/USERNAME?format=json";
    
    private static JSONObject getUserLoginJson(final String username, final String password) throws Exception {
    	final SSLContext sslContext = getSslContext();
    	final HostnameVerifier allHostsValid = new NoopHostnameVerifier();
    	final Client client = ClientBuilder.newBuilder().sslContext(sslContext).hostnameVerifier(allHostsValid).build();
        client.register(new Authenticator(username, password));
        final WebTarget webTarger = client.target(userLoginURL.replace("USERNAME", username));
		final Response responseObj = webTarger.request().header("OCS-APIRequest", "true").get();
		if ( responseObj.getStatus() != 200 ) throw new RuntimeException("getUserLoginJson(..): " + responseObj.getStatus() + " :: " + responseObj.getStatusInfo());
		final String responseStr = responseObj.readEntity(String.class);
    	return JSONObject.fromObject(responseStr);
    }
    
    public static boolean isUserValid(final String username, final String password) {
    	try {
    		log.info("isUserValid() user: " + username);
    		final JSONObject jo = getUserLoginJson(username, password);
    		if (!jo.has("ocs")) return false;
    		final JSONObject ocsJO = jo.getJSONObject("ocs");
    		if (!ocsJO.has("meta")) return false;
    		final JSONObject metaJO = ocsJO.getJSONObject("meta");
    		if (!metaJO.has("status")) return false;
    		final String statusStr = metaJO.getString("status");
    		if (statusStr == null || !statusStr.trim().equalsIgnoreCase("ok")) return false;
    		log.info("isUserValid() user: " + username + " successfully completed !");
    		return true;
    	} catch(Throwable t) {
    		log.error("isUserValid() Cannot check IF user \"" + username + "\" is valid ...", t);
    		return false;
    	}
    	
    }
    
//	private static final String USERNAME = "hrexpert";
//	private static final String PASSWORD = "1hrexpert2!";
	
//	private static final String USERNAME = "harmonicss.user";
//	private static final String PASSWORD = "1n20192!";
    
//	private static final String USERNAME = "test2";
//	private static final String PASSWORD = "1test22!";
	
	private static final String USERNAME = "test1";
	private static final String PASSWORD = "1test12!";
	
//    private static final String USERNAME = "francescoferrodoc@gmail.com";
//	private static final String PASSWORD = "timeotimuncio";
	
    /** For Testing purposes ... */
	public static void main(String[] args) throws Exception {
		
		System.out.println(" >> ServiceTestingMain: START - " + new Date());
		System.out.println();

		// User Login
		System.out.println("\nUSER LOGIN:");
		System.out.println("Valid: " + isUserValid(USERNAME, PASSWORD));
		
		// User DBs
		System.out.println("\nUSER DBs: ");
		System.out.println(Util.listToMultiLineString(getUserDbList(USERNAME, PASSWORD)));

		System.out.println();
		System.out.println(" >> ServiceTestingMain: END - " + new Date());
		
	}
	
}
