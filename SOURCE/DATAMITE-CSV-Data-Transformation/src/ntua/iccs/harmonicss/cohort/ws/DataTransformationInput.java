package ntua.iccs.harmonicss.cohort.ws;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DataTransformationInput {

	private String prvCloudDataFileUrl;
	private String prvCloudMappingFileUrl;
	private String prvCloudUsername;
	private String prvCloudPassword;

	public DataTransformationInput() {
		
	}

	public DataTransformationInput(String prvCloudDataFileUrl, String prvCloudMappingFileUrl, String prvCloudUsername, String prvCloudPassword) {
		super();
		this.prvCloudDataFileUrl = prvCloudDataFileUrl;
		this.prvCloudMappingFileUrl = prvCloudMappingFileUrl;
		this.prvCloudUsername = prvCloudUsername;
		this.prvCloudPassword = prvCloudPassword;
	}
	
	public boolean isValid() {
		if (this.prvCloudDataFileUrl == null || this.prvCloudDataFileUrl.trim().equals("")) return false;
		if (this.prvCloudMappingFileUrl == null || this.prvCloudMappingFileUrl.trim().equals("")) return false;
		if (this.prvCloudUsername == null || this.prvCloudUsername.trim().equals("")) return false;
		if (this.prvCloudPassword == null || this.prvCloudPassword.trim().equals("")) return false;
		return true;
	}

	public String getPrvCloudDataFileUrl() {
		return prvCloudDataFileUrl;
	}

	public void setPrvCloudDataFileUrl(String prvCloudDataFileUrl) {
		this.prvCloudDataFileUrl = prvCloudDataFileUrl;
	}

	public String getPrvCloudMappingFileUrl() {
		return prvCloudMappingFileUrl;
	}

	public void setPrvCloudMappingFileUrl(String prvCloudMappingFileUrl) {
		this.prvCloudMappingFileUrl = prvCloudMappingFileUrl;
	}

	public String getPrvCloudUsername() {
		return prvCloudUsername;
	}

	public void setPrvCloudUsername(String prvCloudUsername) {
		this.prvCloudUsername = prvCloudUsername;
	}

	public String getPrvCloudPassword() {
		return prvCloudPassword;
	}

	public void setPrvCloudPassword(String prvCloudPassword) {
		this.prvCloudPassword = prvCloudPassword;
	}

	@Override
	public String toString() {
		return "DataTransformationInput [prvCloudDataFileUrl="
				+ prvCloudDataFileUrl + ", prvCloudMappingFileUrl="
				+ prvCloudMappingFileUrl + ", prvCloudUsername="
				+ prvCloudUsername + ", prvCloudPassword=" + prvCloudPassword
				+ "]";
	}
	
}
