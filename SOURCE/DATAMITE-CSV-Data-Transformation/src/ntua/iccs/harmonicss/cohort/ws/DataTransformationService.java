package ntua.iccs.harmonicss.cohort.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public class DataTransformationService {

	// http://localhost:8080/CohortDataHarmonization/HarmonizeData/run
	
	@POST
	@Path("/run")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response run(DataTransformationInput userInput) {
		System.out.println("** IN > " + userInput);
		if (!userInput.isValid())
			return Response.status(200).entity(new DataTransformationOutput("ERROR, At least one of the four input parameters has not specified !", null)).build();
		try {
			final String url = new DataTransformationImpl().harmonizeData(
				userInput.getPrvCloudDataFileUrl(), userInput.getPrvCloudMappingFileUrl(), 
				userInput.getPrvCloudUsername(), userInput.getPrvCloudPassword()
			);
			System.out.println("** OUT > OK");
			return Response.status(200).entity(new DataTransformationOutput("OK", url)).build();
		} catch (Throwable t) {
			System.out.println("** OUT > PROBLEM ! ");
			return Response.status(200).entity(new DataTransformationOutput(t.getMessage(), null)).build();
		}
		
	}
	
}
