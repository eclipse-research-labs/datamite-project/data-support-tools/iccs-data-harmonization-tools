package ntua.iccs.harmonicss.cohort.ws;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import org.apache.log4j.Logger;
import org.timchros.core.interfaces.I_Check;
import org.timchros.core.tuple.Tuple2;

import com.github.sardine.DavResource;
import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.ClinLogFile;
import ntua.iccs.harmonicss.cohort.DataTransformationService;
import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.db.JsonDataInsertionToDB;
import ntua.iccs.harmonicss.cohort.db.JsonDataInsertionToDB.DataInsertionResponse;
import ntua.iccs.harmonicss.cohort.db.JsonDataInsertionToDB.ProcessStatus;

@WebService(endpointInterface = "ntua.iccs.harmonicss.cohort.ws.DataTransformation")
public class DataTransformationImpl implements DataTransformation {

	private static final SimpleDateFormat namdf = new SimpleDateFormat("yyyyMMdd-HHmmss");
	
	private static final SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public static final Logger log = LogFactory.getLoggerForClass(DataTransformationImpl.class);

	@Override
	public void logSystemTest() throws Exception {
		log.info("logSystemTest()");
	}
	
	/* ***************** For Data Harmonization Purposes ***************** */
	
	// TODO: Make it synchronized so that it can be used by only one user
	
	@Override
	public String harmonizeData(
		String prvCloudDataExcelFileUrl, String prvCloudMappingFileUrl, String prvCloudUsername, String prvCloudPassword
		// , String dbUrl, String dbUsername, String dbPassword
	) throws Exception {
		
		// Folder with Write Access
		final File folder = new File("/tmp");
		if (!folder.exists()) folder.mkdirs();
		
		
		
		// >> STEP 1: DOWNLOAD DATA (EXCEL) AND MAPPINGS (JSON) 
		
		// About Data
		final String dataExcelFileName = getFileNameFromUrl(prvCloudDataExcelFileUrl);
		final String dataExcelCloudURL = prvCloudDataExcelFileUrl;
		final String dataExcelLocalFilePath = folder + "/" + dataExcelFileName;
		
		// About Mappings
		final String mappingJsonFileName = getFileNameFromUrl(prvCloudMappingFileUrl);
		final String mappingJsonCloudURL = prvCloudMappingFileUrl;
		final String mappingJsonLocalFilePath = folder + "/" + mappingJsonFileName;
		
		try {
		
		final Sardine sardine = SardineFactory.begin(prvCloudUsername, prvCloudPassword);
		
		// Download Cohort Data Excel Document
		final InputStream dis = sardine.get(dataExcelCloudURL);
		Files.copy(dis, new File(dataExcelLocalFilePath).toPath());
		dis.close();
		
		// Download Cohort Mapping Rules
		final InputStream mis = sardine.get(mappingJsonCloudURL);
		Files.copy(mis, new File(mappingJsonLocalFilePath).toPath());
		mis.close();
		
		
		// >> STEP 2:  HARMONIZE DATA BASED ON MAPPING FILE
		
		final String dateStr = namdf.format(new Date());
		
		ClinLogFile.clinLogFilePath = folder+ "/" + dateStr + "-Clinician-CohortDataHarmonization-LogFile.log";
		ClinLogFile.getInstance().openFile();
		// TODO: Delete OLD Log Files
		
		// About Harmonized Data
		final String harmDataOwlFileName = dateStr + "-Harmonized-Data.owl";
		final String harmDataJsonFileName = dateStr + "-Harmonized-Data.json";
		final String locHarmDataOwlFilePath = folder + "/" + harmDataOwlFileName;
		final String locHarmDataJsonFilePath = folder + "/" + harmDataJsonFileName;
		
		// Harmonize Cohort Data based on Mapping Rules
		DataTransformationService.process(dataExcelLocalFilePath, mappingJsonLocalFilePath, locHarmDataOwlFilePath);
		
		// >> STEP 3: UPLOAD HARMONIZED DATA (BOTH OWL AND JSON) AND LOG FILES
		
		final String dataExcelFolderCloudURL = getFolderUrlFromFileUrl(prvCloudDataExcelFileUrl);
		final String cloudHarmOwlDataFileURL = dataExcelFolderCloudURL + harmDataOwlFileName;
		final String cloudHarmJsonDataFileURL = dataExcelFolderCloudURL + harmDataJsonFileName;
		
		// Upload Harmonized Data OWL File - IF File already exists, it will be replaced by the new one
		byte[] owlFileBytes = Files.readAllBytes(new File(locHarmDataOwlFilePath).toPath());
		sardine.put(cloudHarmOwlDataFileURL, owlFileBytes);
		
		// Upload Harmonized Data OWL File - IF File already exists, it will be replaced by the new one
		byte[] jsonFileBytes = Files.readAllBytes(new File(locHarmDataJsonFilePath).toPath());
		sardine.put(cloudHarmJsonDataFileURL, jsonFileBytes);
		
		// TODO: Upload Log Files Files & Delete  
		
//		// TODO: Use the other service .. for JSON to DB
	
		cleanFiles(dataExcelLocalFilePath, mappingJsonLocalFilePath, locHarmDataOwlFilePath, locHarmDataJsonFilePath);
		
		final JSONObject jo = new JSONObject();
		jo.put("jsonFileUrl", cloudHarmJsonDataFileURL);
		jo.put("owlFileUrl", cloudHarmOwlDataFileURL);
		return jo.toString();
		
		} catch (Exception e) {
			cleanFiles(dataExcelLocalFilePath, mappingJsonLocalFilePath);
			throw e;
		}
	}
	
	private synchronized void cleanFiles(final String... paths) {
		for (String path : paths) {
			final File inFile = new File(path);
			if (inFile.exists()) inFile.delete();
		}
	}
	
	private String getFolderUrlFromFileUrl(final String url) {
		check(url);
		// Find Folder URL
		int slashIndex = url.lastIndexOf('/');
		if (slashIndex < 0) throw new RuntimeException("Invalid File URL (cannot find SLASH character - begin of File Name)");
		// Return Folder URL
		return url.substring(0, slashIndex + 1);
	}
	
	private String getFileNameFromUrl(final String url) {
		check(url);
		// Ensure that the URL points to a File (search for a dot)
		int dotIndex = url.lastIndexOf('.');
		if (dotIndex < 0) throw new RuntimeException("Invalid File URL (cannot find DOT character - File Extension)");
		// Find the File Name (including File Extension)
		int slashIndex = url.lastIndexOf('/');
		if (slashIndex < 0) throw new RuntimeException("Invalid File URL (cannot find SLASH character - begin of File Name)");
		// Return File Name
		return url.substring(slashIndex + 1);
	}

//	@Override
//	public String searchForFilesWithSuffix(String prvCloudIP, String prvCloudUsername, String prvCloudPassword, String suffix) throws Exception {
//		// Check
//		check(prvCloudIP, prvCloudUsername, prvCloudPassword, suffix);
//		
//		// Private Cloud Space - Credentials
//		final Sardine sardine = SardineFactory.begin(prvCloudUsername, prvCloudPassword);
//		
//		// Search For File with the given SUFFIX
//		final List<Tuple2<String, Date>> tupleList = getFilessWithSuffix(sardine, prvCloudIP, suffix);
//		
//		// Prepare Response (JSON ARRAY)
//		final JSONArray ja = new JSONArray();
//		
//		if (tupleList == null) return ja.toString();
//		
//		for (Tuple2<String, Date> tuple : tupleList) {
//			final JSONObject jo = new JSONObject();
//			jo.put("url", tuple._1());
//			jo.put("dt", tuple._2());
//			ja.add(jo);
//		}
//		return ja.toString();
//	}
	
	private void check(String... strs) {
		for (String str : strs) {
			if (str == null  || str.trim().equals(""))
				throw new RuntimeException("At least one argument is null or empty !");
		}
	}
	
	private static final String HOME_PATH = "/hcloud/remote.php/webdav/";
	
	/** @return A List of Files with the given suffix along with the Date they were modified */
	private synchronized List<Tuple2<String, Date>> getFilesWithSuffix(final Sardine sardine, final String ip, final String suffix) throws IOException {
		// Check
		if (suffix == null) return null;
		final String suffixTrimLC = suffix.trim().toLowerCase();
		if (suffixTrimLC.equals("")) return null;
		
		final List<Tuple2<String, Date>> tupleList = new ArrayList<Tuple2<String, Date>>();
		
		final I_Check<DavResource> check = new I_Check<DavResource>() {
			@Override public boolean isTrue(DavResource davResource) {
				return davResource.getName().toLowerCase().endsWith(suffixTrimLC);
			}
		};
		
		processFiles(sardine, ip, HOME_PATH, tupleList, check);
		
		return tupleList;
	}	
	
	/** Examines the Files existing under the given folder recursively and update the given List provided that the File satisfies the given condition */
	private static void processFiles(final Sardine sardine, final String ip, final String path, final List<Tuple2<String, Date>> tupleList, final I_Check<DavResource> check) throws IOException {
		final String url =  "http://" + ip + path;
		//System.out.println("#URL: " + url);
		final List<DavResource> docList = sardine.list(url);
		for (DavResource davResource : docList) {
			final String davResourcePath = davResource.getPath().replaceAll(" ", "%20");
			if (davResource.isDirectory()) {
				if (!davResourcePath.equals(path)) {
					processFiles(sardine, ip, davResourcePath, tupleList, check);					
				}
			} else {
				if (check.isTrue(davResource)) {
					tupleList.add(Tuple2.newTuple2("http://" + ip + davResourcePath, davResource.getModified()));
				}
			}
		}
	}		
	
	/* ********************************* For JSON to DB ********************************* */

	private static final String JSON_SUFFIX = ".json";
	
	@Override
	public String searchForJsonFiles(String prvCloudIP, String prvCloudUsername, String prvCloudPassword) throws Exception {
		try {
			// Establish a Connection
			final Sardine sardine = SardineFactory.begin(prvCloudUsername, prvCloudPassword);
			// Find Files
			final List<Tuple2<String, Date>> tupleList = getFilesWithSuffix(sardine, prvCloudIP, JSON_SUFFIX);
			if (tupleList == null || tupleList.isEmpty()) return null;
			// Sort based on the Date
			Collections.sort(tupleList, new Comparator<Tuple2<String, Date>>() {
				@Override public int compare(Tuple2<String, Date> t1, Tuple2<String, Date> t2) {
					return t2._2().compareTo(t1._2());
				}
			});
			// Prepare Response
			final JSONArray ja = new JSONArray();
			for (Tuple2<String, Date> tuple : tupleList) {
				final JSONObject jo = new JSONObject();
				jo.put("url", tuple._1());
				jo.put("dt", df.format(tuple._2()));
				ja.add(jo);
			}
			return ja.toString();
		} catch (Throwable t) {
			// Highlight the Problem
			log.error("searchForJsonFiles(..) Problem", t);
			throw new RuntimeException("searchForJsonFiles(" + prvCloudIP + ", " + prvCloudUsername + ", " + prvCloudPassword + ") problem", t);
		}
	}
	
	public String searchForXlsFiles(String prvCloudIP, String prvCloudUsername, String prvCloudPassword) throws Exception {
		try {
			// Establish a Connection
			final Sardine sardine = SardineFactory.begin(prvCloudUsername, prvCloudPassword);
			// Find Files
			final List<Tuple2<String, Date>> tupleList = getFilesWithSuffixes(sardine, prvCloudIP, "xls", "xlsx");
			if (tupleList == null || tupleList.isEmpty()) return null;
			// Sort based on the Date
			Collections.sort(tupleList, new Comparator<Tuple2<String, Date>>() {
				@Override public int compare(Tuple2<String, Date> t1, Tuple2<String, Date> t2) {
					return t2._2().compareTo(t1._2());
				}
			});
			// Prepare Response
			final JSONArray ja = new JSONArray();
			for (Tuple2<String, Date> tuple : tupleList) {
				final JSONObject jo = new JSONObject();
				jo.put("url", tuple._1());
				jo.put("dt", df.format(tuple._2()));
				ja.add(jo);
			}
			return ja.toString();
		} catch (Throwable t) {
			// Highlight the Problem
			log.error("searchForXlsFiles(..) Problem", t);
			throw new RuntimeException("searchForXlsFiles(" + prvCloudIP + ", " + prvCloudUsername + ", " + prvCloudPassword + ") problem", t);
		}
	}
	
	private synchronized List<Tuple2<String, Date>> getFilesWithSuffixes(final Sardine sardine, final String ip, final String... suffixes) throws IOException {
		// Check
		if (suffixes == null) return null;
		final List<String> suffixTrimLcList = new ArrayList<>();
		for (String suffix : suffixes) {
			final String suffixTrimLC = suffix.trim().toLowerCase();
			if (suffixTrimLC.equals("")) continue;
			suffixTrimLcList.add(suffixTrimLC);
		}
		
		if (suffixTrimLcList.isEmpty()) return null;
		
		final List<Tuple2<String, Date>> tupleList = new ArrayList<Tuple2<String, Date>>();
		
		final I_Check<DavResource> check = new I_Check<DavResource>() {
			@Override public boolean isTrue(DavResource davResource) {
				final String davResrouceNameLC = davResource.getName().toLowerCase();
				for (String suffixTrimLC : suffixTrimLcList) {
					if (davResrouceNameLC.endsWith(suffixTrimLC)) return true;
				}
				return false;
			}
		};
		
		processFiles(sardine, ip, HOME_PATH, tupleList, check);
		
		return tupleList;
	}	

//	@Override
//	public String insertHarmData(String prvCloudIP, String prvCloudUsername, String prvCloudPassword, String dbUrl, String dbUsername, String dbPassword) throws Exception {
//
//		// Use Logging System
//		
//		// >> PART A: Find the JSON File with Patient Data
//		
//		// Find the Latest File with Name: "Cohort-Harmonized-Data.json"
//		final String name = "Cohort-Harmonized-Data.json";
//		// Establish a Connection
//		final Sardine sardine = SardineFactory.begin(prvCloudUsername, prvCloudPassword);
//		// Find Files
//		final List<Tuple2<String, Date>> tupleList = getFilessWithSuffix(sardine, prvCloudIP, name);
//		if (tupleList == null || tupleList.isEmpty()) return null;
//		// Sort based on the Date
//		Collections.sort(tupleList, new Comparator<Tuple2<String, Date>>() {
//			@Override public int compare(Tuple2<String, Date> t1, Tuple2<String, Date> t2) {
//				return t2._2().compareTo(t1._2());
//			}
//		});
//		// Download Latest File
//		final String jsonFilePathURL = tupleList.get(0)._1();
//		final String jsonFilePath = "./" + name;
//		final InputStream mis = sardine.get(jsonFilePathURL);
//		Files.copy(mis, new File(jsonFilePath).toPath());
//		mis.close();
//		
//		// >> PART B: Insert the Data to DB
//		
//		// Invoke your method that inserts the patient data residing in the given JSON File Path in the given DB
//		
//		// Return the data gathered during this process (i.e., the ones presented in the UI) in a Java Object
//		
//		final String jsonStr =  new String(Files.readAllBytes(Paths.get(jsonFilePath)));
//		final JSONObject json = JSONObject.fromObject(jsonStr);
//		final int personNum = json.getJSONArray("person-JA").size();
//		
//		final JSONObject jo = new JSONObject();
//		jo.put("json-url", jsonFilePathURL);
//		jo.put("patients", personNum);
//		jo.put("note", "Service Implmenetation is under process !!!");
//		
//		return jo.toString(5,5);
//	}

	@Override
	public String systemJsonDataInsertionProcessStatus() throws Exception {
		final Boolean occupied = ProcessStatus.getRunStatus();
		final String message = ProcessStatus.getMsg();
		final JSONObject jo = new JSONObject();
		jo.put("occupied", occupied);
		if (message != null) jo.put("message", message);
		return jo.toString();
	}
	
	@Override
	public boolean checkHarmDataInsertion(String dbUrl, String dbUsername, String dbPassword) throws Exception {
		boolean check = JsonDataInsertionToDB.dataInsertionCheck(dbUrl, dbUsername, dbPassword);
		return check;
	}
	
	@Override
	public String insertHarmJsonData(String prvCloudJsonFileUrl, String prvCloudUsername, String prvCloudPassword, String dbUrl, String dbUsername, String dbPassword) throws Exception {
		final String envPath = System.getenv("HOME");
		final String folder = (envPath != null && !envPath.equals("")) ? /* Cloud - Linux */ envPath : /* Windows */ "."; 
		final String name = "Local-Cohort-Harmonized-Data.json";
		final String jsonFilePath = folder + File.separatorChar + name;
		
		// System.out.println(" ** jsonFilePath: " + jsonFilePath);
		
		final Path path = new File(jsonFilePath).toPath();
		
		try {
			// >> PART A: Download JSON File with Patient Data
			ProcessStatus.setRunFlagTRUE();
			ProcessStatus.setMsg("Downloading JSON File ...");
			
			log.info("Downloading JSON File \"" + prvCloudJsonFileUrl + "\" ...");
			// Establish a Connection
			final Sardine sardine = SardineFactory.begin(prvCloudUsername, prvCloudPassword);
			// Download File
			final String jsonFilePathURL = prvCloudJsonFileUrl;
			final InputStream mis = sardine.get(jsonFilePathURL);
			Files.copy(mis, path);
			mis.close();
			log.info("Downloading JSON File \"" + prvCloudJsonFileUrl + "\" Successfully Completed ! Local File: " + path);
			
			// >> PART B: Insert the Data to DB
			
			// Load Data from File and Insert in the DB
			final DataInsertionResponse response = JsonDataInsertionToDB.insertJsonDataToDB(jsonFilePath, dbUrl, dbUsername, dbPassword);
			
			return response.asJson().toString();

		} catch (Throwable t) {
			// Highlight the Problem
			log.error("insertHarmJsonData(..) Problem", t);
			throw new RuntimeException("insertHarmJsonData(" + prvCloudJsonFileUrl + ", " + prvCloudUsername + ", " + prvCloudPassword + ", " + dbUrl + ", " + dbUsername + ", " + dbPassword + ") problem", t);
		} finally {
			log.info("Delete JSON File \"" + path + "\" ...");
			// Remove File
			Files.deleteIfExists(path);
			log.info("Delete JSON File \"" + path + "\" Successfully Completed !");
			ProcessStatus.setRunFlagFALSE();
		}
		
	}




	
}
