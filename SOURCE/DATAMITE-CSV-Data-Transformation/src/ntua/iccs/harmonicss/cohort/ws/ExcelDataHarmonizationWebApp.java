package ntua.iccs.harmonicss.cohort.ws;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.LogFactory;

import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Throable;

@WebServlet("/ExcelDataHarmonizationWebApp")
public class ExcelDataHarmonizationWebApp extends HttpServlet {

	public static final Logger log = LogFactory.getLoggerForClass(ExcelDataHarmonizationWebApp.class);
	
	private static final String HOST = "private.harmonicss.eu";
	
	private static final long serialVersionUID = 1L;
	
	/**
     * @see HttpServlet#HttpServlet()
     */
	public ExcelDataHarmonizationWebApp() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	// WEB PAGE URL
	// http://localhost:8080/CohortDataHarmonization/ExcelDataHarmonizationWebApp
	// For testing ...
	// http://localhost:8080/CohortDataHarmonization/ExcelDataHarmonizationWebApp?prvCloudDataFileUrl=URL1&prvCloudMappingFileUrl=URL2
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();
		
		// TODO: GET Username and Password from Request Parameters if specified
		
		final String reqUsername = request.getParameter("username");
		final String reqPassword = request.getParameter("password");

		// Username and Password from Session
		final HttpSession session = request.getSession();
		final String pcsUsername = 
			(!WebAppUtil.isStrNullOrEmpty(reqUsername)) 
				? reqUsername : JsonDataInsertionWebApp.objToStr( session.getAttribute("pcsUsername") );
		final String pcsPassword = 
			(!WebAppUtil.isStrNullOrEmpty(reqPassword)) 
				? reqPassword : JsonDataInsertionWebApp.decrypt( JsonDataInsertionWebApp.objToStr( session.getAttribute("pcsPasswordEncr") ) );
		
		// Cohort Data and Mappings from Request
		final String prvCloudDataFileUrl = request.getParameter("prvCloudDataFileUrl");
		final String prvCloudMappingFileUrl = request.getParameter("prvCloudMappingFileUrl");
		
		//TODO: IF NOT data and mappings specified, present form, 
		// ELSE harmonize data and present output message when finished
		
		if (WebAppUtil.isStrNullOrEmpty(prvCloudDataFileUrl) || WebAppUtil.isStrNullOrEmpty(prvCloudMappingFileUrl)) {
			// Ensure that user is valid
			final boolean isUserValid = NextCloudServiceClient.isUserValid(pcsUsername, pcsPassword);
			if (!isUserValid) {
				out.println(WebAppUtil.prepareLoginPage("Invalid User !")); 
				out.close(); return ;
			}
			
			// Find Excel Files URL in the Private Cloud Space
			final List<Tuple2<String, String>> xlsTupleList = getExcelFiles(HOST, pcsUsername, pcsPassword);
			if (xlsTupleList == null) {
				out.println(WebAppUtil.prepareLoginPage("Cannot detect Excel Files in your Private Cloud Space !")); 
				out.close(); return ;
			}
			
			// Find JSON Files URL in the Private Cloud Space
			final List<Tuple2<String, String>> jsonTupleList = JsonDataInsertionWebApp.getJsonFiles(HOST, pcsUsername, pcsPassword);
			if (jsonTupleList == null) {
				out.println(WebAppUtil.prepareLoginPage("Cannot detect JSON Files in your Private Cloud Space !")); 
				out.close(); return ;
			}
			
			out.println(WebAppUtil.prepareFilesSelectionPage(xlsTupleList, jsonTupleList));
			out.close();
			return;
			
		} else {
			/*
			out.print("<h2>For debugging ...</h2>");
			out.print("<p>pcsUsername: " + pcsUsername + "</p>");
			out.print("<p>pcsPassword: " + pcsPassword + "</p>");
			out.print("<p>prvCloudDataFileUrl: " + prvCloudDataFileUrl + "</p>");
			out.print("<p>prvCloudMappingFileUrl: " + prvCloudMappingFileUrl + "</p>");
			*/
			try {
				final String jsonStr = new DataTransformationImpl().harmonizeData(
					prvCloudDataFileUrl, prvCloudMappingFileUrl, 
					pcsUsername, pcsPassword
				);
				out.print("Success, " + jsonStr);
			} catch (Throwable t) {
				out.print("Failure, " + Throable.throwableAsSimpleOneLineString(t));
			}
			
		}
		
		// // >> Prepare Login Page
		//out.println(prepareLoginPage(null));
		out.close();
		return ;	
	}
	
	public static List<Tuple2<String, String>> getExcelFiles(String host, String username, String password) {
		try {
			final String jaStr = new DataTransformationImpl().searchForXlsFiles(host, username, password);
			final JSONArray ja = JSONArray.fromObject(jaStr);
			final List<Tuple2<String, String>> tupleList = new ArrayList<Tuple2<String,String>>();
			for (Object obj : ja) {
				final JSONObject jo = (JSONObject) obj;
				tupleList.add(Tuple2.newTuple2(jo.getString("url"), jo.getString("dt")));
			}
			return tupleList;
		} catch (Throwable t) {
			return null;
		}
	}

}
