package ntua.iccs.harmonicss.cohort.ws;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple2;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import ntua.iccs.harmonicss.cohort.LogFactory;
import ntua.iccs.harmonicss.cohort.db.JsonDataInsertionToDB;
import ntua.iccs.harmonicss.cohort.db.JsonDataInsertionToDB.DataInsertionResponse;

@WebServlet("/JsonDataInsertionWebApp")
public class JsonDataInsertionWebApp extends HttpServlet {

	public static final Logger log = LogFactory.getLoggerForClass(JsonDataInsertionWebApp.class);
	
	private static final long serialVersionUID = 1L;

	
	private static final String HOST = "private.harmonicss.eu";

	/**
     * @see HttpServlet#HttpServlet()
     */
    public JsonDataInsertionWebApp() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	// WEB PAGE URL
	// http://localhost:8181/CohortDataHarmonization/JsonDataInsertionWebApp
	// For testing ...
	// http://localhost:8181/CohortDataHarmonization/JsonDataInsertionWebApp?username=hrexpert&password=1hrexpert2!
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();
		
		// Session Attributes
		final HttpSession session = request.getSession();
		final String pcsUsername = objToStr( session.getAttribute("pcsUsername") );
		final String pcsPasswordEncr = objToStr( session.getAttribute("pcsPasswordEncr") );
		final String pcsPassword = decrypt(pcsPasswordEncr);
		
		// Request Parameters - Login
		final String username = request.getParameter("username");
		final String password = request.getParameter("password");
	
		// Request Parameters - Data Insertion
		final String jsonFileURL = request.getParameter("jsonfileurl");
		final String dbName = request.getParameter("dbname");
		
		// IF both JSON file and DB specified insert Data..
		if (!WebAppUtil.isStrNullOrEmpty(jsonFileURL) && !WebAppUtil.isStrNullOrEmpty(dbName)) {
			// Find again the DB connection data for the given user
			final List<DbConnData> tmpDbList = NextCloudServiceClient.getUserDbList(pcsUsername, pcsPassword);
			// tmpDbList.add(new DbConnData("147.102.19.66", "3306", "HarmonicSS-E", "emps", "emps"));
			
			// Find DB connection data based on the DB name
			final DbConnData dbConnData = getDbConnData(tmpDbList, dbName);
			final String dbUrl = (dbConnData != null) ? dbConnData.getURL() : null ; 
			final String dbUsername = (dbConnData != null) ? dbConnData.getUsername() : null ; 
			final String dbPassword = (dbConnData != null) ? dbConnData.getPassword() : null ; 
			
			// -----------------------------------------------------------------------
			// >> Insert JSON data to DB
			// -----------------------------------------------------------------------

			// Check ... IF data already inserted 
			try {
				if (harmDataInserted(dbUrl, dbUsername, dbPassword)) {
					// Add Link and Service about Clean DB ... (if being necessary)
					out.print(WebAppUtil.prepareMessagePage("Harmonized JSON Data already inserted !")); 
					out.close(); return;
				}
			} catch (Throwable t) {
				out.print(WebAppUtil.prepareMessagePage("Problem accessing user Relational DB: " + t.getMessage())); 
				out.close(); return;
			}
			
			// Check ... IF another process is already being executed
			if (JsonDataInsertionToDB.ProcessStatus.getRunStatus() == true) {
				out.print(WebAppUtil.prepareMessagePage("Service is busy.. Please try again after 5 minutes !")); 
				out.close(); return;
			}
			
			// Insert Data in a background Thread ...
			final Runnable runObj = new Runnable() {
				@Override public void run() {
					insertData(jsonFileURL, pcsUsername, pcsPassword, dbUrl, dbUsername, dbPassword);
				}
			};
			final Thread thread = new Thread(runObj, "User-" + pcsUsername+"-Thread");
			thread.start();
			log.info("Thread has just started... ID: " + thread.getId() + " , Name: " + thread.getName());
			
			// >> Prepare Web Page about Response
			out.print(WebAppUtil.prepareMessagePage(
				"Data Insertion has started.. View current status: <a href=\"JsonDataInsertionStatus\">View Status</a>"));
			out.close(); return;
			
		} else {
			// IF both Username and Password were given
			if (!WebAppUtil.isStrNullOrEmpty(username) && !WebAppUtil.isStrNullOrEmpty(password)) {

				// Ensure that user is valid
				final boolean isUserValid = NextCloudServiceClient.isUserValid(username, password);
				if (!isUserValid) {
					out.println(WebAppUtil.prepareLoginPage("Invalid User !")); 
					out.close(); return ;
				}
				
				// Find JSON Files URL in the Private Cloud Space
				final List<Tuple2<String, String>> jsonTupleList = getJsonFiles(HOST, username, password);
				if (jsonTupleList == null) {
					out.println(WebAppUtil.prepareLoginPage("Cannot detect JSON Files in your Private Cloud Space !")); 
					out.close(); return ;
				}
				
				// Store in Session Username and Password (encrypted)
				session.setAttribute("pcsUsername", username);
				final String passwordEncr = encrypt(password);
				session.setAttribute("pcsPasswordEncr", passwordEncr);
			
				// Find DBs Connection Data
				final List<DbConnData> dbList = NextCloudServiceClient.getUserDbList(username, password);
				// dbList.add(new DbConnData("147.102.19.66", "3306", "HarmonicSS-E", "emps", "emps"));
				
				log.info("En" + passwordEncr + "cr");
				
				// -----------------------------------------------------------------------
				// >> Prepare Web Page about File and DB selection
				// -----------------------------------------------------------------------
				
				out.println(WebAppUtil.prepareSelectionPage(jsonTupleList, dbList));
				out.close();
				return;
				
			} else {
				// >> Prepare Login Page
				out.println(WebAppUtil.prepareLoginPage(null));
				out.close();
				return ;	
			}
			
		}

	} // END OF doPost(..)
	
	public static List<Tuple2<String, String>> getJsonFiles(String host, String username, String password) {
		try {
			final String jaStr = new DataTransformationImpl().searchForJsonFiles(host, username, password);
			final JSONArray ja = JSONArray.fromObject(jaStr);
			final List<Tuple2<String, String>> tupleList = new ArrayList<Tuple2<String,String>>();
			for (Object obj : ja) {
				final JSONObject jo = (JSONObject) obj;
				tupleList.add(Tuple2.newTuple2(jo.getString("url"), jo.getString("dt")));
			}
			return tupleList;
		} catch (Throwable t) {
			return null;
		}
	}
	
	private static DbConnData getDbConnData(final List<DbConnData> dbList, final String dbName) {
    	for (DbConnData db : dbList) {
			if (db.getName().equals(dbName)) return db;
		}
    	return null;
    }
	
	public static String objToStr(final Object obj) {
		if (obj == null) return null;
		final String objStr = obj.toString();
		if (objStr.trim().equals("")) return null;
		return objStr;
	}
	
	/* *************************** Background Processes *************************** */
	
	private static boolean harmDataInserted(String dbUrl, String dbUsername, String dbPassword) {
		try {
			final boolean check = new DataTransformationImpl().checkHarmDataInsertion(dbUrl, dbUsername, dbPassword);
			return check;
		} catch (Throwable t) {
			throw new RuntimeException("Unexpected Problem with DB check..", t);
		}
	}
	
	private static DataInsertionResponse insertData(String jsonFileURL, String pcsUsername, String pcsPassword, String dbUrl, String dbUsername, String dbPassword) {
		try {
			final String insRespStr = new DataTransformationImpl().insertHarmJsonData(jsonFileURL, pcsUsername, pcsPassword, dbUrl, dbUsername, dbPassword);
			final JSONObject jo = JSONObject.fromObject(insRespStr);
			return DataInsertionResponse.fromJsonObject(jo);
		} catch (Throwable t) {
			throw new RuntimeException("Unexpected Problem with Patient Data Insertion to DB.", t);
		}
	}

	/* *************************** Text Encryption/Decryption *************************** */
	
	private static final String algorithm = "AES";
	private static final String secretKey = "H@rmon!cSs&2#!45";
	
	
	public static String encrypt(final String password) {
		if (password == null || password.trim().equals("")) return null;
		try {
			final SecretKeySpec skeyspec = new SecretKeySpec(secretKey.getBytes(), algorithm);
			final Cipher cipher = Cipher.getInstance(algorithm);
			cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
			final byte[] encrypted = cipher.doFinal(password.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(encrypted);
		} catch (Throwable t) {
			log.error("encrypt(..) Problem", t);
			return null;
		}
	}
	
	public static String decrypt(final String encrPassword) {
		if (encrPassword == null || encrPassword.trim().equals("")) return null;
		try {
			final SecretKeySpec skeyspec = new SecretKeySpec(secretKey.getBytes(),algorithm);
			final Cipher cipher = Cipher.getInstance(algorithm);
			cipher.init(Cipher.DECRYPT_MODE, skeyspec);
			final byte[] decrypted = cipher.doFinal(Base64.getDecoder().decode(encrPassword.getBytes()));
			return new String(decrypted);
		} catch (Throwable t) {
			log.error("decrypt(..): Problem encrPassword=" + encrPassword, t);
			return null;
		}
	}
	
//	 private static final String TEST_USERNAME = "hrexpert";
	private static final String TEST_PASSWORD = "1hrexpert2!";
	
	public static void main(String[] args) throws Exception {
		
		System.out.println("Begin");
		
		final String text = TEST_PASSWORD; //"Thymios";
		System.out.println("Text: " + text);
		final String encrText = encrypt(text);
		System.out.println("Enrcypt: " + encrText);
		final String decrText = decrypt(encrText);
		System.out.println("Decrypt: " + decrText);
		
		System.out.println("...");
		System.out.println(decrypt("2qQOoypMULSKg8bHbwyYNA=="));
		System.out.println(decrypt("2qQOoypMULSKg8bHbwyYNA=="));
		System.out.println("...");
		
//		System.out.println(Util.listToMultiLineString(getJsonFiles(HOST, TEST_USERNAME, TEST_PASSWORD)));
		
		// System.out.println(getJsonFiles(HOST, TEST_USERNAME, TEST_PASSWORD));
		
		System.out.println("End");
		
	}
	
}
