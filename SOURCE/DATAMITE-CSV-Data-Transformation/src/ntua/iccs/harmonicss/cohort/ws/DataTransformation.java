package ntua.iccs.harmonicss.cohort.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(name="DataTransformation", targetNamespace = "")
public interface DataTransformation {

	@WebMethod
	public void logSystemTest() throws Exception;	
	
	/**
	 * Express the given Patient Data (Excel) using the Reference Model terms (OWL + JSON)
	 * based on the Mapping Rules specified. Data is accordingly stored in the Relational Database.
	 */
	@WebMethod
	public String harmonizeData(
		/* Private Cloud Space - Files + Credentials */
		@WebParam(name = "prvCloudDataExcelFileUrl") String prvCloudDataExcelFileUrl, 
		@WebParam(name = "prvCloudMappingFileUrl") String prvCloudMappingFileUrl, 
		@WebParam(name = "prvCloudUsername") String prvCloudUsername, 
		@WebParam(name = "prvCloudPassword") String prvCloudPassword // ,
//		/* DB - Connection Details */
//		@WebParam(name = "dbUrl") String dbUrl, 
//		@WebParam(name = "dbUsername") String dbUsername, 
//		@WebParam(name = "dbPassword") String dbPassword
	) throws Exception;
	
//	/** 
//	 * Primarily, for testing purposes ... 
//	 */
//	@WebMethod
//	public String searchForFilesWithSuffix(
//		/* Private Cloud Space - Connection Details */
//		@WebParam(name = "prvCloudIP") String prvCloudIP, 
//		@WebParam(name = "prvCloudUsername") String prvCloudUsername, 
//		@WebParam(name = "prvCloudPassword") String prvCloudPassword,
//		/* File Name Suffix */
//		@WebParam(name = "suffix") String suffix
//	) throws Exception;
		
//	/** 
//	 * Finds the Latest JSON File with the Harmonized Patient Data and accordingly insert them in the DB 
//	 */
//	@WebMethod
//	public String insertHarmData(
//		/* Private Cloud Space - Connection Details */
//		@WebParam(name = "prvCloudIP") String prvCloudIP, 
//		@WebParam(name = "prvCloudUsername") String prvCloudUsername, 
//		@WebParam(name = "prvCloudPassword") String prvCloudPassword,
//		/* DB - Connection Details */
//		@WebParam(name = "dbUrl") final String dbUrl, 
//		@WebParam(name = "dbUsername") final String dbUsername, 
//		@WebParam(name = "dbPassword") final String dbPassword
//	) throws Exception;
	
	/** 
	 * Provides a JSON Array with JSON Files (URL) along with the Date they were modified.
	 */
	@WebMethod
	public String searchForJsonFiles(
		/* Private Cloud Space - Connection Details */
		@WebParam(name = "prvCloudIP") String prvCloudIP, 
		@WebParam(name = "prvCloudUsername") String prvCloudUsername, 
		@WebParam(name = "prvCloudPassword") String prvCloudPassword
	) throws Exception;
	
	/** 
	 * Check IF the Harmonized Data have been already inserted in the DB
	 */
	@WebMethod
	public boolean checkHarmDataInsertion(
		/* DB - Connection Details */
		@WebParam(name = "dbUrl") final String dbUrl, 
		@WebParam(name = "dbUsername") final String dbUsername, 
		@WebParam(name = "dbPassword") final String dbPassword
	) throws Exception;
	
	/**
	 * Check IF the JSON-to-DB service is occupied (for the insertion of the data located in another file) 
	 */
	@WebMethod
	public String systemJsonDataInsertionProcessStatus() throws Exception;
	
	/** 
	 * Download the JSON File from the Private Cloud Space and accordingly insert Patient JSON data in the DB 
	 */
	@WebMethod
	public String insertHarmJsonData(
		/* Private Cloud Space - Connection Details */
		@WebParam(name = "prvCloudJsonFileUrl") String prvCloudJsonFileUrl, 
		@WebParam(name = "prvCloudUsername") String prvCloudUsername, 
		@WebParam(name = "prvCloudPassword") String prvCloudPassword,
		/* DB - Connection Details */
		@WebParam(name = "dbUrl") final String dbUrl, 
		@WebParam(name = "dbUsername") final String dbUsername, 
		@WebParam(name = "dbPassword") final String dbPassword
	) throws Exception;	
	

	

	
}
