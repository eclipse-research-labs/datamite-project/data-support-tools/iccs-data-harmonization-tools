package ntua.iccs.harmonicss.cohort.ws.test;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;

import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;

public class DownFileMain {

	public static void main(String[] args) throws Exception {

		System.out.println(" >> DownFileMain : BEGIN");
		
		final String prvCloudUsername = "username";
		final String prvCloudPassword = "password";
		
		// Establish a Connection
		final Sardine sardine = SardineFactory.begin(prvCloudUsername, prvCloudPassword);
		// Download File
		final String jsonFilePathURL = "http://private.harmonicss.eu/hcloud/remote.php/webdav/qualified_data/Cohort-Harmonized-Data%20Kopie.json";
		final String locPath = "C:/Users/Efthymios/Desktop/XXYYZZ-Cohort-Harmonized-Data.json";
		final InputStream mis = sardine.get(jsonFilePathURL);
		Files.copy(mis, new File(locPath).toPath());
		mis.close();
		
		System.out.println(" >> DownFileMain : END");
		
	}

}
