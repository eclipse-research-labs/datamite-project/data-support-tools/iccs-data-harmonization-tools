package ntua.iccs.harmonicss.cohort.ws.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.timchros.core.interfaces.I_Check;
import org.timchros.core.tuple.Tuple2;

import com.github.sardine.DavResource;
import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;

public class PrivateCloudSpaceTestingPlaceMain {

	private static final String METACLOUD_IP = "private.harmonicss.eu"; //"83.212.104.6";
	private static final String METACLOUD_USERNAME = "hrexpert";
	private static final String METACLOUD_PASSWORD = "1hrexpert2!";
	private static final String HOME_PATH = "/hcloud/remote.php/webdav/";
	private static final String METACLOUD_HOME_HTTP_URL =  "http://" + METACLOUD_IP + HOME_PATH; //"http://" + METACLOUD_IP + HOME_PATH;
	
	public static void main(String[] args) throws Exception {
		
		// Sardine and HTTPS
		// https://github.com/lookfirst/sardine/wiki/UsageGuide
		
		// https://anto.online/other/resolution-pkix-path-building-failed/
		// http://www.djcxy.com/p/31162.html
		
//		final FileInputStream fis = new FileInputStream(new File(KEYSTORE_PATH));		
//		final KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType()); // JKS
//		keyStore.load(fis, null);
//		fis.close();
//		final SSLSocketFactory socketFactory = new SSLSocketFactory(keyStore);
//
//		Sardine sardine = new SardineImpl() { 
//			@Override protected SSLSocketFactory createDefaultSecureSocketFactory() { return socketFactory; }
//		};
//		sardine.setCredentials(METACLOUD_USERNAME, METACLOUD_PASSWORD);
//		
//		System.out.println("\nPRIVATE CLOUD PATH/FOLDER: \"" + METACLOUD_HOME_URL + "\".");
//		boolean bool1 = sardine.exists(METACLOUD_HOME_URL);
//		System.out.println("Exists: " + bool1);
//		
//		if (true) return;
		
//		SSLSocketFactory.getSocketFactory();
		
		System.out.println(" >> PrivateCloudSpaceTestingPlaceMain: BEGIN");
		System.out.println();
		
		final String prvCloudUsername = METACLOUD_USERNAME;
		final String prvCloudPassword = METACLOUD_PASSWORD;
		
		final Sardine downSardine = SardineFactory.begin(prvCloudUsername, prvCloudPassword);
		System.out.println("\nPRIVATE CLOUD PATH/FOLDER: \"" + METACLOUD_HOME_HTTP_URL + "\".");
		boolean bool = downSardine.exists(METACLOUD_HOME_HTTP_URL);
		System.out.println("Exists: " + bool);
		
		
//		final String METACLOUD_HOME_HTTPS_URL =  "https://" + METACLOUD_IP + HOME_PATH; //"http://" + METACLOUD_IP + HOME_PATH;
//		final Sardine downSardine = SardineFactory.begin(prvCloudUsername, prvCloudPassword);
//		System.out.println("\nPRIVATE CLOUD PATH/FOLDER: \"" + METACLOUD_HOME_HTTPS_URL + "\".");
//		boolean bool = downSardine.exists(METACLOUD_HOME_HTTPS_URL);
//		System.out.println("Exists: " + bool);
		
		
//		final String suffix = ".json";
//		System.out.println("\nFiles with File Name SUFFIX: \"" + suffix + "\".");
//		final List<Tuple2<String, Date>> tupleList = getFilessWithSuffix(downSardine, METACLOUD_IP, suffix);
//		Collections.sort(tupleList, new Comparator<Tuple2<String, Date>>() {
//			@Override public int compare(Tuple2<String, Date> t1, Tuple2<String, Date> t2) {
//				return t2._2().compareTo(t1._2());
//			}
//		});
//		System.out.println(Util.listToMultiLineString(tupleList, new I_StringTransformer<Tuple2<String, Date>>() {
//			@Override public String asString(Tuple2<String, Date> tuple) {
//				return tuple._2() + " :: " + tuple._1();
//			}
//		}));
	
		
		System.out.println();
		System.out.println(" >> PrivateCloudSpaceTestingPlaceMain: END");
		
	}
	
	public static void check(String... strs) {
		for (String str : strs) {
			if (str == null  || str.trim().equals(""))
				throw new RuntimeException("At least one argument is null or empty !");
		}
	}
	
	public static synchronized List<Tuple2<String, Date>> getFilessWithSuffix(final Sardine sardine, final String ip, final String suffix) throws IOException {
		// Check
		if (suffix == null) return null;
		final String suffixTrimLC = suffix.trim().toLowerCase();
		if (suffixTrimLC.equals("")) return null;
		
		final List<Tuple2<String, Date>> tupleList = new ArrayList<Tuple2<String, Date>>();
		
		final I_Check<DavResource> check = new I_Check<DavResource>() {
			@Override public boolean isTrue(DavResource davResource) {
				return davResource.getName().toLowerCase().endsWith(suffixTrimLC);
			}
		};
		
		processFiles(sardine, ip, HOME_PATH, tupleList, check);
		
		return tupleList;
	}
	
	private static void processFiles(final Sardine sardine, final String ip, final String path, final List<Tuple2<String, Date>> tupleList, final I_Check<DavResource> check) throws IOException {
		final String url =  "http://" + ip + path;
		//System.out.println("#URL: " + url);
		final List<DavResource> docList = sardine.list(url);
		for (DavResource davResource : docList) {
			if (davResource.isDirectory()) {
				if (!davResource.getPath().equals(path)) {
					//System.out.println("DIR: " + davResource.getPath());
					processFiles(sardine, ip, davResource.getPath(), tupleList, check);					
				}
			} else {
				if (check.isTrue(davResource)) {
//					davResource.getModified() getCreation()
					//System.out.println("FILE: " + davResource.getPath() + " \t :: " + davResource.getModified());
					tupleList.add(Tuple2.newTuple2("http://" + ip + davResource.getPath(), davResource.getModified()));
				}
			}
		}
	}
	
}
