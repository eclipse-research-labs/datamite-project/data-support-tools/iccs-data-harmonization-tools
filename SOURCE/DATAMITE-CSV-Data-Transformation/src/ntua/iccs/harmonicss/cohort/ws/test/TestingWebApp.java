package ntua.iccs.harmonicss.cohort.ws.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/TestingWebApp")
public class TestingWebApp extends HttpServlet {

	/**
     * @see HttpServlet#HttpServlet()
     */
    public TestingWebApp() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	private static final long serialVersionUID = 1L;

	// WEB PAGE URL
	// http://localhost:8181/CohortDataHarmonization/TestingWebApp
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setIntHeader("Refresh", 2);
		response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();
		
		final StringBuilder sb = new StringBuilder();

		sb.append("This is a test message !");
		
		out.print(sb.toString());
		
		out.close();
	}
	
}

