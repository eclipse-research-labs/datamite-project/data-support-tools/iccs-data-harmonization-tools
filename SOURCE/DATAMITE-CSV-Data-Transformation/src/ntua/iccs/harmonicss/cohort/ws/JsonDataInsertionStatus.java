package ntua.iccs.harmonicss.cohort.ws;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ntua.iccs.harmonicss.cohort.db.JsonDataInsertionToDB;

@WebServlet("/JsonDataInsertionStatus")
public class JsonDataInsertionStatus extends HttpServlet {

	/**
     * @see HttpServlet#HttpServlet()
     */
    public JsonDataInsertionStatus() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	private static final long serialVersionUID = 1L;

	// WEB PAGE URL
	// http://localhost:8181/CohortDataHarmonization/JsonDataInsertionStatus
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setIntHeader("Refresh", 2);
		response.setContentType("text/html; charset=UTF-8");
		final PrintWriter out = response.getWriter();
		out.println(WebAppUtil.prepareStatusPage(getStatus()));
		out.close();
	}

	private String getStatus() {
		final StringBuilder sb = new StringBuilder();
		// Status
		if (JsonDataInsertionToDB.ProcessStatus.getRunStatus()) {
			sb.append("Under process: ");
		} else {
			sb.append("Completed: ");
		}
		// Message
		sb.append(JsonDataInsertionToDB.ProcessStatus.getMsg());
		return sb.toString();
	}
	
}
