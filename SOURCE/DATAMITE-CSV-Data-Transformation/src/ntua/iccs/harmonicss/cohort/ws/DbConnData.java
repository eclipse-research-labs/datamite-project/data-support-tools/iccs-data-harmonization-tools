package ntua.iccs.harmonicss.cohort.ws;

public class DbConnData {
	
	final String ip;
	final String port;
	final String name;
	final String username;
	final String password;
	
	public DbConnData(String ip, String port, String name, String username, String password) {
		super();
		this.ip = ip;
		this.port = port;
		this.name = name;
		this.username = username;
		this.password = password;
	}
	
	public String getURL() { return "jdbc:mysql://" + ip + ":" + port + "/" + name; }
//	
//	public String getIp() { return ip; }
//
//	public String getPort() { return port; }

	public String getName() { return name; }

	public String getUsername() { return username; }
//
	public String getPassword() { return password; }

	@Override
	public String toString() {
		return "DbConnData [ip=" + ip + ", port=" + port + ", name=" + name + ", username=" + username + ", password=" + password + "]";
	}
	
}
