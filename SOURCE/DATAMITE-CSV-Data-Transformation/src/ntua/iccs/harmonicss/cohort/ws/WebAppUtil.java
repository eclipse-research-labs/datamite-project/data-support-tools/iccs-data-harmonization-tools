package ntua.iccs.harmonicss.cohort.ws;

import java.util.List;

import org.timchros.core.tuple.Tuple2;

public class WebAppUtil {

	private static final String URL_PREFIX = "http://private.harmonicss.eu/hcloud/remote.php/webdav/";
	private static final String NL = "\n";	
	
	private WebAppUtil() {
		
	}
	
	public static String prepareLoginPage(final String msg) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html>");
		sb.append("<html><head>");
		sb.append("<meta charset=\"utf-8\">");
		sb.append("<title>Login Page</title>");
		sb.append("<style>");
		sb.append("body { padding: 0px; margin: 0px; font-family: Arial, Helvetica, sans-serif; } ");
		sb.append(".header { background-color: #e6f2ff; text-align: center; padding-top: 10px; padding-bottom: 10px; } ");
		sb.append("#harmonicssLogo { height: 40px;  } ");
		sb.append("#iccsntuaLogo { height: 40px; padding-left: 20px; } ");
		sb.append("h2 { text-align: center;  } ");
		sb.append("p { color: red; text-align: center; } ");
		sb.append("th, td { text-align: left; padding: 5px; } ");
		sb.append("table { padding: 20px 30px; margin: 5px; width:100px; text-align: center; margin-left:auto; margin-right:auto; border: 1px solid gray; border-radius: 15px; } ");
		sb.append("input { padding: 5px 10px; border-radius: 5px; font-size: 1.1em;} ");
		sb.append("form { margin-top: 20px; text-align: center; } ");
		sb.append("</style>");
		sb.append("</head><body>" + NL);
		// Header
		sb.append(headerDiv() + NL);
		// Title
		sb.append("<h2>Login Page</h2>" + NL);
		// Form
		sb.append("<form method=\"post\">" + NL);
		sb.append("<table>" + NL);
		sb.append("<tr><th>Username:</th><td><input type=\"text\" name=\"username\"></td></tr>" + NL);
		sb.append("<tr><th>Password:</th><td><input type=\"password\" name=\"password\"></td></tr>" + NL);
		sb.append("<tr><th><input type=\"reset\" value=\"Reset\"></th><td><input type=\"submit\" value=\"Submit\"  "
				+ "onclick=\"this.value='Wait ...'; this.disabled='disabled'; this.form.submit();\"></td></tr>" + NL);
		sb.append("</table>" + NL);
		sb.append("</form>");
		// Message
		if (!isStrNullOrEmpty(msg)) sb.append("<p>" + msg + "</p>");
		sb.append("</body></html>");
		return sb.toString();
	}
	
	public static String prepareSelectionPage(final List<Tuple2<String, String>> jsonTupleList, final List<DbConnData> dbList) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html>");
		sb.append("<html><head>");
		sb.append("<meta charset=\"utf-8\">");
		sb.append("<title>Select JSON + DB</title>");
		sb.append("<style>");
		sb.append("body { padding: 0px; margin: 0px; font-family: Arial, Helvetica, sans-serif; } ");
		sb.append(".header { background-color: #e6f2ff; text-align: center; padding-top: 10px; padding-bottom: 10px; } ");
		sb.append("#harmonicssLogo { height: 40px;  } ");
		sb.append("#iccsntuaLogo { height: 40px; padding-left: 20px; } ");
		sb.append("h2 { text-align: center; } ");
		sb.append("table { padding: 20px 30px; margin: 5px; width:100px; text-align: center; margin-left:auto; margin-right:auto; } ");
		sb.append("input, select { padding: 5px 10px; border-radius: 5px; font-size: 1.1em;} ");
		sb.append("th, td { text-align: left; padding: 5px; } ");
		sb.append("</style>");
		sb.append("</head><body>" + NL);
		// Header
		sb.append(headerDiv() + NL);
		// Title 
		sb.append("<h2>Select JSON File and DB : <a href=\"Logout\">Logout</a></h2>" + NL);
		sb.append("<form>");
		sb.append("<table>" + NL);
		// JSON FILE
		if (jsonTupleList.size() > 1) sb.append("<tr><td colspan=\"2\">Select the appropriate JSON file:</td></tr>");
		sb.append("<tr><th>JSON:</th><td><select name=\"jsonfileurl\">");
		for (Tuple2<String, String> tuple : jsonTupleList) {
			final String path = tuple._1().substring(URL_PREFIX.length());
			sb.append("<option value=\"" + tuple._1() + "\">" + tuple._2() + " : " + path + "</option>");
		}
		sb.append("</select></td></tr>");
		// DB URL
		if (dbList.size() > 1) sb.append("<tr><td colspan=\"2\">Select the appropriate Database:</td></tr>");
		sb.append("<tr><th>Database:</th><td><select name=\"dbname\">");
		for (DbConnData db : dbList) {
			sb.append("<option value=\"" + db.getName() + "\">" + db.getName() + "</option>");
		}
		sb.append("</select></td></tr>");
		// Submit Button
		sb.append("<tr><th><br></th><td><input type=\"submit\" value=\"Insert Cohort Data\" "
				+ "onclick=\"this.value='Wait ...'; this.disabled='disabled'; this.form.submit();\"></td></tr>");
		
		sb.append("<tr><td colspan=\"2\"><br></td></tr>");
		sb.append("<tr><td colspan=\"2\">For harmonizing your data (if not already done) visit:<br><a href=\"ExcelDataHarmonizationWebApp\">Cohort Data Harmonization Web Application</a></td></tr>");
		
		sb.append("</table>" + NL);
		sb.append("</form>");
		sb.append("</body></html>");
		return sb.toString();
	}
	
	public static String prepareMessagePage(final String msg) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html>");
		sb.append("<html><head>");
		sb.append("<meta charset=\"utf-8\">");
		sb.append("<title>Message</title>");
		sb.append("<style>");
		sb.append("body { padding: 0px; margin: 0px; font-family: Arial, Helvetica, sans-serif; } ");
		sb.append(".header { background-color: #e6f2ff; text-align: center; padding-top: 10px; padding-bottom: 10px; } ");
		sb.append("#harmonicssLogo { height: 40px;  } ");
		sb.append("#iccsntuaLogo { height: 40px; padding-left: 20px; } ");
		sb.append("h2 { text-align: center; } ");
		sb.append("p { color: darkred; text-align: center; font-size: 1.1em; margin-top: 30px; padding: 10px; } ");
		sb.append("</style>");
		sb.append("</head><body>" + NL);
		// Header
		sb.append(headerDiv() + NL);
		// Title
		sb.append("<h2>Important Message - <a href=\"Logout\">Logout</a></h2>" + NL);
		// Message
		if (!isStrNullOrEmpty(msg)) sb.append("<p>" + msg + "</p>");
		sb.append("</body></html>");
		return sb.toString();
	}
	
	public static String prepareStatusPage(final String status) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html>");
		sb.append("<html><head>");
		sb.append("<meta charset=\"utf-8\">");
		sb.append("<title>Process Status</title>");
		sb.append("<style>");
		sb.append("body { padding: 0px; margin: 0px; font-family: Arial, Helvetica, sans-serif; } ");
		sb.append(".header { background-color: #e6f2ff; text-align: center; padding-top: 10px; padding-bottom: 10px; } ");
		sb.append("#harmonicssLogo { height: 40px;  } ");
		sb.append("#iccsntuaLogo { height: 40px; padding-left: 20px; } ");
		sb.append("p { color: darkblue; text-align: center; font-size: 1.1em; margin-top: 30px; padding: 10px; } ");
		sb.append("</style>");
		sb.append("</head><body>");
		// Header
		sb.append(headerDiv() + NL);
		// Process Status
		sb.append("<p>" + status + "</p>");
		sb.append("<p><a href=\"Logout\">Logout</a></p>");
		sb.append("</body></html>");
		return sb.toString();
	}
	
	public static String prepareLogoutPage() {
		final StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html>");
		sb.append("<html><head>");
		sb.append("<meta charset=\"utf-8\">");
		sb.append("<title>Logout</title>");
		sb.append("<style>");
		sb.append("body { padding: 0px; margin: 0px; font-family: Arial, Helvetica, sans-serif; } ");
		sb.append(".header { background-color: #e6f2ff; text-align: center; padding-top: 10px; padding-bottom: 10px; } ");
		sb.append("#harmonicssLogo { height: 40px;  } ");
		sb.append("#iccsntuaLogo { height: 40px; padding-left: 20px; } ");
		sb.append("h2 { text-align: center; } ");
		sb.append("p { color: darkred; text-align: center; font-size: 1.1em; margin-top: 30px; padding: 10px; } ");
		sb.append("</style>");
		sb.append("</head><body>" + NL);
		// Header
		sb.append(headerDiv() + NL);
		// Title
		sb.append("<p>Successfully Logout !</p>");
		sb.append("<p><a href=\"JsonDataInsertionWebApp\">Go back to Login Page</a></p>");
		sb.append("</body></html>");
		return sb.toString();
	}
	
	private static String headerDiv() {
		return 
			"<div class='header'>" + 
			"<img id='harmonicssLogo' src=\"IMG/HarmonicSS-Logo.png\" alt=\"HarmonicSS Logo\">" +
			"<img id='iccsntuaLogo' src=\"IMG/ICCS-NTUA-Logo.png\" alt=\"ICCS/NTUA Logo\">" +
			"</div>";
	}
	
	public static boolean isStrNullOrEmpty(final String str) {
		if (str == null) return true;
		return str.trim().equals("");
	}
	
	
	// NEW
	
	public static String prepareFilesSelectionPage(final List<Tuple2<String, String>> xlsTupleList, final List<Tuple2<String, String>> jsonTupleList) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE html>");
		sb.append("<html><head>");
		sb.append("<meta charset=\"utf-8\">");
		sb.append("<title>Select JSON + DB</title>");
		sb.append("<style>");
		sb.append("body { padding: 0px; margin: 0px; font-family: Arial, Helvetica, sans-serif; } ");
		sb.append(".header { background-color: #e6f2ff; text-align: center; padding-top: 10px; padding-bottom: 10px; } ");
		sb.append("#harmonicssLogo { height: 40px;  } ");
		sb.append("#iccsntuaLogo { height: 40px; padding-left: 20px; } ");
		sb.append("h2 { text-align: center; } ");
		sb.append("table { padding: 20px 30px; margin: 5px; width:100px; text-align: center; margin-left:auto; margin-right:auto; } ");
		sb.append("input, select { padding: 5px 10px; border-radius: 5px; font-size: 1.1em;} ");
		sb.append("th, td { text-align: left; padding: 5px; } ");
		sb.append("</style>");
		sb.append("</head><body>" + NL);
		// Header
		sb.append(headerDiv() + NL);
		// Title 
		sb.append("<h2>Select Cohort Data (Excel) and Mappings (Json) : <a href=\"Logout\">Logout</a></h2>" + NL);
		sb.append("<form action=\"ExcelDataHarmonizationWebApp\" method=\"POST\">");
		sb.append("<table>" + NL);
		// EXCEL FILE
		if (xlsTupleList.size() > 1) sb.append("<tr><td colspan=\"2\">Select the appropriate EXCEL file:</td></tr>");
		sb.append("<tr><th>Cohort Data (Excel):</th><td><select name=\"prvCloudDataFileUrl\">");
		for (Tuple2<String, String> tuple : xlsTupleList) {
			final String path = tuple._1().substring(URL_PREFIX.length());
			sb.append("<option value=\"" + tuple._1() + "\">" + tuple._2() + " : " + path + "</option>");
		}
		sb.append("</select></td></tr>");
		// JSON FILE 
		if (jsonTupleList.size() > 1) sb.append("<tr><td colspan=\"2\">Select the appropriate JSON file:</td></tr>");
		sb.append("<tr><th>Mappings(JSON):</th><td><select name=\"prvCloudMappingFileUrl\">");
		for (Tuple2<String, String> tuple : jsonTupleList) {
			final String path = tuple._1().substring(URL_PREFIX.length());
			sb.append("<option value=\"" + tuple._1() + "\">" + tuple._2() + " : " + path + "</option>");
		}
		sb.append("</select></td></tr>");

		// Submit Button
		sb.append("<tr><th><br></th><td><input type=\"submit\" value=\"Harmonize Cohort Data\" "
				+ "onclick=\"this.value='Wait ...'; this.disabled='disabled'; this.form.submit();\"></td></tr>");
		
		sb.append("</table>" + NL);
		sb.append("</form>");
		sb.append("</body></html>");
		return sb.toString();
	}
	
}
