package ntua.iccs.harmonicss.cohort;

import static ntua.iccs.harmonicss.cohort.owl.ReferenceMoUri.*;
import static ntua.iccs.harmonicss.cohort.CohortData.*;
import static ntua.iccs.harmonicss.cohort.data.json.FieldsMappingRule.paramUriToABC;
import static ntua.iccs.harmonicss.cohort.LogsArea.MsgDestin.CLINICAL;
import static ntua.iccs.harmonicss.cohort.LogsArea.MsgDestin.TECHNICAL;
import static ntua.iccs.harmonicss.cohort.LogsArea.MsgDestin.FILES;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ntua.iccs.harmonicss.cohort.data.json.FieldsMappingRule;
import ntua.iccs.harmonicss.cohort.data.json.Term;
import ntua.iccs.harmonicss.cohort.data.json.TermExpr;
import ntua.iccs.harmonicss.cohort.data.json.TermExpression;
import ntua.iccs.harmonicss.cohort.data.json.TermsMappingRule;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.data.owl.PropertyValueList;
import ntua.iccs.harmonicss.cohort.data.owl.complex.DateValue;
import ntua.iccs.harmonicss.cohort.data.owl.complex.SimpleCodedValue;
import ntua.iccs.harmonicss.cohort.data.owl.simple.IntegerValue;
import ntua.iccs.harmonicss.cohort.owl.OntoDataRepo;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.ontology.Ontology;
import org.apache.jena.ontology.UnionClass;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.log4j.Logger;
import org.timchros.core.transformers.I_StringTransformer;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Checks;
import org.timchros.core.util.Throable;
import org.timchros.core.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class DataTransformationService {
	
	public static final String version = "1.0.3.1";
	
	private static final SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
	
	public static final Logger log = LogFactory.getLoggerForClass(DataTransformationService.class);

	private static final String dataXlsFilePath = 
		"TEST-DATA/UoA-2019-06-18-Cohort-FAKE-Data.xlsx";
	private static final String mappingsJsonFilePath = 
		"TEST-DATA/Mapping.json";
	private static final String genHarmOwlFilePath = 
		"TEST-DATA/UoA-Dummy-Data-Harmonization.owl";		

	private static final String harmOwlFilePath = 
		"other-files/owl/HarmonicSS-Reference-Model+Vocabularies-v.0.9.3.owl";
	
	public static void main(String[] args) throws Exception {
		
		System.out.println(" >> DataTransformationService: Process STARTED at " + new Date() );
		System.out.println();

		// Read Input Data from Command LineS
		final String userDataXlsFilePath, userMappingsJsonFilePath, userGenHarmOwlFilePath;
		if (args == null || args.length != 3) {
			System.out.println(" * Warning: No Data Provided - Default Arguments will be used *");
			System.out.println();
			userDataXlsFilePath = dataXlsFilePath;
			userMappingsJsonFilePath = mappingsJsonFilePath;
			userGenHarmOwlFilePath = genHarmOwlFilePath;
		} else {
			userDataXlsFilePath = args[0];
			userMappingsJsonFilePath = args[1];
			userGenHarmOwlFilePath = args[2];
		}
		
		System.out.println(" - Cohort Data XLS File Path:      " + userDataXlsFilePath);
		System.out.println(" - Mappings Rules JSON File Path:  " + userMappingsJsonFilePath);
		System.out.println(" - Generated Cohort OWL File Path: " + userGenHarmOwlFilePath);
		System.out.println();
		
		// Check Given Data
		if (userDataXlsFilePath.trim().equals("") || userMappingsJsonFilePath.trim().equals("") || userGenHarmOwlFilePath.trim().equals("")) {
			System.out.println("\n * At least one out of three input arguments is empty ! Please check.. "); return;
		}
		
		process(userDataXlsFilePath, userMappingsJsonFilePath, userGenHarmOwlFilePath);
		
		System.out.println(" >> DataTransformationService: Process FINISHED at " + new Date() );	
	}
	
	private static Comparator<Entry<String, String>> patientEntryComparator = new Comparator<Entry<String, String>>() {
		@Override public int compare(Entry<String, String> e1, Entry<String, String> e2) {
			return keyToNumber(e1.getKey()) - keyToNumber(e2.getKey());
		}
	};
	
	public static void process(String dataXlsFilePath, String mappingsJsonFilePath, String genHarmOwlFilePath) throws Exception {

		Checks.checkNotNullArg(
			mappingsJsonFilePath, "mappingsJsonFilePath", 
			mappingsJsonFilePath, "mappingsJsonFilePath", 
			genHarmOwlFilePath, "genHarmOwlFilePath"
		);
		
		OntoDataRepo.reInit();
		
		// Open Clinician Log File
		ClinLogFile.getInstance().openFile();
		
		LogsArea.println(FILES, "================================================ COHORT-DATA-HARMONIZATION =================================================\n");

		LogsArea.println(FILES, "Process started on Date: " + df.format(new Date()) + "\n");
		
		LogsArea.println(FILES, "********************************************* A. Load Cohort Data & Mappings ***********************************************\n");
		
		// Step A.1: Load Patient Data from MS Excel Document
		LogsArea.println("Loading Cohort Data (MS Excel Document) ... path= \"" + dataXlsFilePath + "\"");
		final List<Map<String, String>> patientDataList = CohortData.loadExcelData(dataXlsFilePath);
		LogsArea.println("Loading Cohort Data (MS Excel Document) ... Successfully Completed !\n");
		final Set<String> columnsUidsSet = new HashSet<>();
		int numFields = 0;
		for (Map<String, String> map : patientDataList) {
			numFields += map.size();
			for (Entry<String, String> entry : map.entrySet()) {
				columnsUidsSet.add(entry.getKey());
			}
		}
		// The number of Fields used (more Field may exist, however they are not be used by any patient)
		final int cohortFieldsCount = columnsUidsSet.size();
		LogsArea.println("Number of patients: " + patientDataList.size() + " (Fields: " + cohortFieldsCount + ")\n");
		columnsUidsSet.clear();
		
		final float patientDataFieldsAvg = numFields / patientDataList.size();
		final float patientEmptyFieldsAvg = cohortFieldsCount - patientDataFieldsAvg;
		LogsArea.println("Number of Fields per Patient: " + patientDataFieldsAvg + " (on average) - Empty Fields: " + patientEmptyFieldsAvg + " (on average)\n");
		
		// For debugging ... Print Patient Data (i.e., Field:Value)
		final StringBuilder csb = new StringBuilder();
		for (Map<String, String> map : patientDataList) {
			final List<Entry<String, String>> entryList = new ArrayList<>(map.entrySet());
			Collections.sort(entryList, patientEntryComparator);
			csb.append(" - ");
			for (Entry<String, String> entry : entryList) {
				csb.append(entry.getKey() + ":" + entry.getValue() + " ");
			}
			csb.append("\n");
		}
		LogsArea.println(CLINICAL, "COHORT DATA:\n\n" + csb.toString());
		
		final StringBuilder tsb = new StringBuilder();
		for (Map<String, String> map : patientDataList) {
			final List<Entry<String, String>> entryList = new ArrayList<>(map.entrySet());
			Collections.sort(entryList, patientEntryComparator);
			tsb.append(" - ");
			for (Entry<String, String> entry : entryList) {
				if ((entry.getValue() != null && !entry.getValue().isEmpty())) {
					tsb.append(entry.getKey() + ":" + "STR" + " ");
				}
			}
			tsb.append("\n");
		}
		LogsArea.println(TECHNICAL, "COHORT DATA:\n\n" + tsb.toString());
		
		// Step A.2: Load Mapping Rules specified
		LogsArea.println("Loading Mapping Rules (JSON File) ... path=" + mappingsJsonFilePath);
		final Tuple2<List<FieldsMappingRule>, List<TermsMappingRule>> mrTuple = MappingRules.loadMappingRulesFromJsonFile(mappingsJsonFilePath);
		LogsArea.println("Loading Mapping Rules (JSON File) ... Successfully Completed !\n");
		final List<FieldsMappingRule> mrFieldList = mrTuple._1();
		final List<TermsMappingRule> mrTermList = mrTuple._2();
		LogsArea.println("Mapping Rules:: About-Fields: " + mrFieldList.size() + " , About-Terms: " + mrTermList.size() + "\n");
		
		// For debugging... Sort Mapping Rules
		Collections.sort(mrFieldList, new Comparator<FieldsMappingRule>() {
			/* Sort based on the Fields ID */
			@Override public int compare(FieldsMappingRule mr1, FieldsMappingRule mr2) {
				return mr1.getSrcFieldsABC().get(0).compareTo(mr2.getSrcFieldsABC().get(0));
			}
		});
		Collections.sort(mrTermList, new Comparator<TermsMappingRule>() {
			/* Sort based on the URI (and hence Domain) */
			@Override public int compare(TermsMappingRule mr1, TermsMappingRule mr2) {
				if (mr1.getSrcTermExpr().isTerm() && mr2.getSrcTermExpr().isTerm()) {
					final Term term1 = (Term) mr1.getSrcTermExpr();
					final Term term2 = (Term) mr2.getSrcTermExpr();
					return term1.getUri().compareTo(term2.getUri());
				} else {
					return 1;
				}
				
			}
		});
		
		// For debugging... Print Fields Mapping Rules
		LogsArea.println("FIELDS MAPPING RULES:\n\n" + Util.listToMultiLineString(mrFieldList, new I_StringTransformer<FieldsMappingRule>() {
			@Override public String asString(FieldsMappingRule mr) {return mappingRuleToOneLineString(mr); }
		}) + "\n");
		
		// For debugging... Find Fields used by the Mapping Rules
		final Set<String> mrColumnUidsSet = new HashSet<>();
		for (FieldsMappingRule mappingRule : mrFieldList) {
			for (Entry<Integer, Term> entry : mappingRule.getSrcPropMap().entrySet()) {
				final String uri = entry.getValue().getUri();
				final String uid = paramUriToABC(uri);
				mrColumnUidsSet.add(uid);
			}
		}
		final int columnsMappedCount = mrColumnUidsSet.size();
		LogsArea.println("From the \"" + cohortFieldsCount + "\" Fields, the \"" + columnsMappedCount + "\" used by the given Mapping Rules !\n");
		final List<String> mrColumnUidsList = new ArrayList<>(mrColumnUidsSet);
		Collections.sort(mrColumnUidsList, new Comparator<String>() {
			@Override public int compare(String s1, String s2) {
				if (s1.length() == s2.length()) {
					return s1.compareTo(s2);
				} else {
					return s1.length() - s2.length();
				}
			}
		});
		LogsArea.println(TECHNICAL, "USED COLUMNS: " + Util.listToOneLineString(mrColumnUidsList) + "\n");
		mrColumnUidsList.clear();
		mrColumnUidsSet.clear();
		
		// For debugging... Print Terms Mapping Rules
		LogsArea.println("TERMS MAPPING RULES:\n\n" + Util.listToMultiLineString(mrTermList, new I_StringTransformer<TermsMappingRule>() {
			@Override public String asString(TermsMappingRule mr) {return mappingRuleToString(mr); }
		}) + "\n");
		
		// Find Reference Model Distinct Terms
		final Set<String> uriSet = new HashSet<>();
		for (FieldsMappingRule mappingRule : mrFieldList) {
			uriSet.add(mappingRule.getTrgEntityClass().getUri());
			for (Entry<String, String> entry : mappingRule.getDtService().getArgs().entrySet()) {
				final String paramValue = entry.getValue();
				if (paramValue.startsWith(VOCNS)) {
					uriSet.add(paramValue);
				}
			}
		}
		for (TermsMappingRule mappingRule : mrTermList) {
			//final String termUri = mappingRule.getTrgTerm().getUri();
			//if (termUri.contains("#CONFIRM") || termUri.contains("#ASSESS")) continue;
			final TermExpr termExpr = mappingRule.getTrgTermExpr();
			if (termExpr.isSimpleConfirmationTerm() || termExpr.isSimpleAssessmentTerm()) continue;
			if (termExpr.isTerm()) {
				final Term term = (Term) termExpr;
				uriSet.add(term.getUri());
			} else {
				final TermExpression termExpression = (TermExpression) termExpr;
				for (TermExpr tmpTermExpr : termExpression.getTermExprList()) {
					if (tmpTermExpr.isTerm()) {
						final Term tmpTerm = (Term) tmpTermExpr;
						uriSet.add(tmpTerm.getUri());
					}
				}
			}
			
		}
		LogsArea.println(TECHNICAL, "REFERENCE MODEL TERMS:\n\n" + "In this Cohort, there is information about ** " + uriSet.size() + " ** Reference Model terms.\n");
		final List<String> termUriList = new ArrayList<>(uriSet);
		Collections.sort(termUriList);
		
		// For debugging... Print Reference Model Terms
		LogsArea.println(TECHNICAL, "Reference Model Terms: \n\n" + Util.listToMultiLineString(termUriList) + "\n");
		
		LogsArea.println(FILES, "********************************************* B. Harmonize Data based on Mappings ******************************************\n");

		// Load Reference Model
		final OntModel harmOnto = OntoDataRepo.getOntModel(harmOwlFilePath);
				
		LogsArea.println("DATA HARMONIZATION PROCESS ... Started !");
		
		LogsArea.println(CLINICAL, "\nFor each patient, apply Mapping Rules specified about Cohort Fields, one by one ...");
		
		// Create Cohort Instance
		createCohortEntity(harmOnto, termUriList);
		
		// Make Reference Model available to Data Transformation Services
		DataTransformation.setRefModel(harmOnto);
		// Make Terminology Alignment available to Data Transformation Services
		DataTransformation.setTermsMR(mrTermList);
		
		final OntProperty personHasData = harmOnto.getOntProperty(personHasDataUri);
		final OntProperty dataBelongToVisit = harmOnto.getOntProperty(dataBelongToVisitUri);
		
		// Create an Instance about a Harmonization .. Visit
		final Individual visitEntity = createVisitEntity(harmOnto);
		
		// Find/Apply Mapping Rule that creates an instance of a Person.
		FieldsMappingRule personMR = null;
		for (FieldsMappingRule mappingRule : mrFieldList) {
			if (mappingRule.getTrgEntityClass().getUri().equals(PersonUri)) {
				personMR = mappingRule; break;
			}
		}
		if (personMR == null)
			throw new RuntimeException("Cannot find a Mapping Rule that creates a Person");
		
		final Set<Class<? extends DataTransformation>> clsSet = new HashSet<>();
		clsSet.add(personMR.getDtService().getClass());
		
		final JSONArray personJA = new JSONArray();
		
		// For each patient
		int patientIndex = 0;
		for (Map<String, String> patientData : patientDataList) {
		 try {	
			// For debugging purposes
			LogsArea.println(FILES, "");
			LogsArea.println(FILES, ">> Patient: " + ++patientIndex);
			LogsArea.println(FILES, "----------------------------------------------------------------------------------------------------------------------------");
			
			// Creates an instance of a Person. All the other Entities will be linked with this one
			final Map<Integer, String> personFieldsData = getPatientDataForMR(patientData, personMR);
			final Tuple2<Boolean, List<Map<Integer, PropertyValue>>> personTuple = personMR.getDtService().transform(personFieldsData);
			if (personTuple == null) throw new RuntimeException("Cannot create an instance of Person !");
			final Map<Integer, PropertyValue> personEntityMap = personTuple._2().get(0);
			final Individual personEntity = createEntityBasedOnData(harmOnto, personMR, personEntityMap, personTuple._1());
			final JSONObject personJO = createJsonObjBasedOnData(harmOnto, personMR, personEntityMap);
			
			// Harmonize Data using the rest Mapping Rules
			final Set<String> entityStrRecSet = new HashSet<>();
			int enittyIgnCount = 0;
			for (FieldsMappingRule mappingRule : mrFieldList) {
			 try {
				// Ignore Mapping Rule about the Person
				if (mappingRule.equals(personMR)) continue;
				
				clsSet.add(mappingRule.getDtService().getClass());
				
				// For debugging ... The Mapping Rule
				log.trace("Mapping Rule: " + mappingRuleToOneLineStringV2(mappingRule));
				
				// Find the corresponding patient Data
				final Map<Integer, String> rulePatientData = getPatientDataForMR(patientData, mappingRule);
				
				// Find the Range of Values of Input Fields
				final Map<Integer, String> paramValueRangeMap = new HashMap<>();
				for(Entry<Integer, Term> entry : mappingRule.getSrcPropMap().entrySet()) {
					final Integer index = entry.getKey();
					final Term propTerm = entry.getValue();
					if (propTerm.getRangeUri() != null) { 
						paramValueRangeMap.put(index, propTerm.getRangeUri());
					}
				}
				mappingRule.getDtService().setValueRange(paramValueRangeMap);
				
				// For debugging ... Patient Data: Field/Value
				final Map<String, String> ruleFieldsPatientData = new HashMap<>();
				for (Entry<Integer, Term> entry : mappingRule.getSrcPropMap().entrySet()) {
					final String uri = entry.getValue().getUri();
					final String uid = paramUriToABC(uri);
					final String value = patientData.get(uid);
					ruleFieldsPatientData.put(uid, value);
				}
				log.trace("Patient Data: " + ruleFieldsPatientData);
				
				// Find Target Class - Properties Values (i.e., OWL Entities) based on the given Data
				Tuple2<Boolean, List<Map<Integer, PropertyValue>>> entityTuple = null;
				try {
					// Call MR service
					entityTuple = mappingRule.getDtService().transform(rulePatientData);
				} catch (Throwable sert) {
					final String errmsg = "Error when executing method transform()... " + 
						"Service: " + mappingRule.getDtService().getClass().getName() + 
						"\nArgs: " + mappingRule.getDtService().getArgs() + "\nInput: " + rulePatientData + "\nOutput: " + entityTuple;
					throw new RuntimeException(errmsg, sert);
				}
				
				// For debugging... Input/Output
				log.trace("Service: " + mappingRule.getDtService().getClass().getName() + " Input: " + rulePatientData + " Output: " + entityTuple);
				
				// Do not create an instance, if not being necessary
				if (entityTuple == null || entityTuple._1() == null || entityTuple._2() == null || entityTuple._2().isEmpty()) continue;
				
				// Check Amount of data returned for each Entity
				for (Map<Integer, PropertyValue> outmap : entityTuple._2()) {
					if (mappingRule.getTrgPropMap().size() != outmap.size()) {
						final String errmsg = "Inappropriate \"" + mappingRule.getDtService().getClass().getName() + "\" transform() method outcome ...\n" + 
							"Amount of Data returned by the MR service: " + mappingRule.getTrgPropMap().size() + " is DIFFERENT THAN the " + 
							"Amount of Properties specified in the Mapping Rule: " + outmap.size() +
							"\nService transform() method outcome:\n" + Util.mapToMultiLineString(outmap) +
							"\nMR Class Properties:\n" + Util.mapToMultiLineString(mappingRule.getTrgPropMap());
						throw new RuntimeException(errmsg);
					}
				}
				
				// For each Map create a separate Instance
				for (Map<Integer, PropertyValue> entityMap : entityTuple._2()) {
					
					// For debugging... Class/Properties/Values
					final StringBuilder propValueSb = new StringBuilder();
					for (Entry<Integer, Term> entry : mappingRule.getTrgPropMap().entrySet()) {
						final Integer index = entry.getKey();
						final String uri = entry.getValue().getUri();
						if (propValueSb.length() > 0) propValueSb.append("\n");
						propValueSb.append(" - " + uri + " --> " + entityMap.get(index));
					}
					log.trace("Harmonized Data: " + mappingRule.getTrgEntityClass() + "\n" + propValueSb);
					
					/* Double Entries (same) */
					
					// Ensure that there is no other Entity/Individual with Exactly the same Data
					// Except Biopsies and Other Auto-antibody Tests
					final boolean isBiopsy = 
						mappingRule.getTrgEntityClass().getLocalName().equals("Biopsy");
					final boolean isOtherAutoAntibodyTest = 
						mappingRule.getTrgEntityClass().getLocalName().equals("Laboratory-Test") && isEntityAboutAntibodies(entityMap.values());
					final boolean isPregnancy = 
						mappingRule.getTrgEntityClass().getLocalName().equals("Pregnancy");
					if (!isBiopsy && !isOtherAutoAntibodyTest && !isPregnancy) {
						final String entityStr = mappingRule.getTrgEntityClass().getLocalName() + " : " + propValueMapAsString(entityMap);
						if (entityStrRecSet.contains(entityTuple._1() + entityStr)) {
							log.debug("An Entity about " + mappingRule.getTrgEntityClass().getLocalName() + " is being ignored ! (already recorded)");
							enittyIgnCount++; continue;
						}
						entityStrRecSet.add(entityTuple._1() + entityStr);
					}
					
					/* JSON ENTITIES */
					
					// Create a JSON object with the properties and their values
					final JSONObject json = createJsonObjBasedOnData(harmOnto, mappingRule, entityMap);
					final String jsonKey = getCategoryKey(json.getString("type"));
					if (entityTuple._1()) {
						final String positiveStatements = "POSITIVE-STMT";
						if (!personJO.has(positiveStatements)) personJO.put(positiveStatements, new JSONObject());
						final JSONObject personPosJO = personJO.getJSONObject(positiveStatements);
						if (!personPosJO.has(jsonKey)) personPosJO.put(jsonKey, new JSONArray());
						personPosJO.getJSONArray(jsonKey).add(json);
					} else {
						final String negativeStatements = "NEGATIVE-STMT";
						if (!personJO.has(negativeStatements)) personJO.put(negativeStatements, new JSONObject());
						final JSONObject personNegJO = personJO.getJSONObject(negativeStatements);
						if (!personNegJO.has(jsonKey)) personNegJO.put(jsonKey, new JSONArray());
						personNegJO.getJSONArray(jsonKey).add(json);
					}
					
					/* OWL ENTITIES */
					
					// Create an individual of the given type with the properties/values generated
					final Individual dataEntity = createEntityBasedOnData(harmOnto, mappingRule, entityMap, entityTuple._1());
					
					// Link Data with Person, taking into account the output of the Data Transformation Service
					// i.e. The Person may (assertion) or may not (negative assertion) linked with the Data/Entity
					if (entityTuple._1()) {
						personEntity.addProperty(personHasData, dataEntity);
					} else {
						addNegativeProperty(harmOnto, personEntity, personHasData, dataEntity);
					}
					
					// Link Data with Visit
					dataEntity.setPropertyValue(dataBelongToVisit, visitEntity);
				}

			 } catch (Throwable mrt){
				final String errmsg = "Error when applying mapping rule: " + mappingRule.getClass().getSimpleName() + " :: " + mappingRuleToOneLineString(mappingRule);
				throw new RuntimeException(errmsg, mrt);
			 }
			} // END OF Mapping Rules Loop
			if (enittyIgnCount > 0) {
				log.debug("Number of same Entities being IGNORED: " + enittyIgnCount + " (data already recorded)");
			}
			
			personJA.add(personJO);
			
		 } catch (Throwable pt) {
			final String errmsg = "Error while processing data of patient: " + patientIndex;
			LogsArea.println(FILES, errmsg + "\n\n" + Throable.throwableAsString(pt) + "\n");
			throw new RuntimeException(errmsg, pt);
		 }
		} // END OF Patients Loop
		
		LogsArea.println();
		LogsArea.println("DATA HARMONIZATION PROCESS ... Successfully Completed !\n");
		
		final List<Class<? extends DataTransformation>> clsList = new ArrayList<>(clsSet);
		Collections.sort(clsList, new Comparator<Class<? extends DataTransformation>>() {
			@Override
			public int compare(Class<? extends DataTransformation> cls1, Class<? extends DataTransformation> cls2) {
				return cls1.getName().compareTo(cls2.getName());
			}
		});
		
		LogsArea.println(TECHNICAL, "Distinct Mapping Scenarios used: " + clsList.size() + "\n");
		LogsArea.println(TECHNICAL, "Data Transformation Services/Classes being used:\n\n" +
			Util.listToMultiLineString(clsList, new I_StringTransformer<Class<? extends DataTransformation>>() {
				@Override
				public String asString(Class<? extends DataTransformation> cls) {
					return cls.getName();
				}
			}) + "\n");
		
		/* ******************* C. Save Harmonize Data (OWL/JSON File) *******************  */
		
		// Reference Model Classes of Data
		final List<String> dataClassList = new ArrayList<>();
		for (String termUri : termUriList) {
			if (termUri.equals(PersonUri)) continue;
			if (termUri.startsWith(REFNS)) dataClassList.add(termUri);
		}
		
		/* Save to OWL file */
		
		// Harmonized Data Synopsis
		Ontology onto = harmOnto.getOntology("http://www.semanticweb.org/ntua/iccs/harmonicss/terminology");
		final String ontoLabel = onto.getLabel(null);
		onto.setLabel(ontoLabel + " + Cohort Harmonized Data", "en");
		onto.setComment(
			"This ontology contains the HARMONIZED PATIENT DATA of a particular Cohort.\n\n" + 
			"Cohort Data MS Excel File: " + new File(dataXlsFilePath).getName() + "\n\n" +
			"Amount of Patients: " + patientDataList.size() + "\n\n" + 
			"Cohort Data Harmonization was driven by the Mapping Rules specified (JSON File: " + new File(mappingsJsonFilePath).getName() + ") based on the Cohort Metadata Extracted.\n\n" + 
			"Amount of Cohort Fields Harmonized: " + columnsMappedCount + " (Amount of Cohort Fields: " + cohortFieldsCount + ")\n\n" +
			"Included information about the following " + dataClassList.size() + " concepts: \n\n" + 
			Util.listToMultiLineString(dataClassList, new I_StringTransformer<String>() {
				@Override public String asString(String uri) { return harmOnto.getOntClass(uri).getLabel(null); }
			}),
			"en");
		
		final String dateStr = "Data Harmonization process executed on Date: " + new SimpleDateFormat("yyyy/MM/dd").format(new Date());
		onto.addProperty(DC.date, dateStr);
		final String creatorStr = "File automatically generated by the Cohort Data Harmonization tool (version: " + version + ") "
			+ "developed by ICCS/NTUA within European Project HarmonicSS.";
		onto.addProperty(DC.creator, creatorStr);
		
		// Save Harmonized Patient Data to an OWL File
		LogsArea.println("Saving Harmonized Data to OWL ontology... path=" + genHarmOwlFilePath);
		OntoDataRepo.saveOntoToFile(harmOnto, genHarmOwlFilePath);
		LogsArea.println("Saving Harmonized Data to OWL ontology... Successfully Completed !\n");
		
		/* Save to JSON file */
		
		final JSONObject cohortJO = createCohortJsonObj(harmOnto, termUriList);
		cohortJO.put("number-Patients", patientDataList.size());
		cohortJO.put("number-Fields", cohortFieldsCount);
		cohortJO.put("number-Fields-Mapped", columnsMappedCount);
		
//		LogsArea.println(TECHNICAL, "A-BEFORE GC System " + new MemoryMessage().toParsableString());
//		System.gc();
//		LogsArea.println(TECHNICAL, "A-AFTER GC System " + new MemoryMessage().toParsableString());
		
		harmOnto.removeAll();
		
//		LogsArea.println(TECHNICAL, "B-BEFORE GC System " + new MemoryMessage().toParsableString());
//		System.gc();
//		LogsArea.println(TECHNICAL, "B-AFTER GC System " + new MemoryMessage().toParsableString());
		
		final JSONObject harmDataJO = new JSONObject();
		harmDataJO.put("date-Harmonization-Process-Run", new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
		harmDataJO.put("cohort", cohortJO);
		harmDataJO.put("person-JA", personJA);
		harmDataJO.put("file", creatorStr);
		final String genHarmJsonFilePath = genHarmOwlFilePath.substring(0, genHarmOwlFilePath.lastIndexOf('.')) + ".json";
		//Files.write(Paths.get(genHarmJsonFilePath), harmDataJO.toString(5, 0).getBytes());
		Files.write(Paths.get(genHarmJsonFilePath), harmDataJO.toString().getBytes());
		
		// Close Clinician Log File
		ClinLogFile.getInstance().closeFile();
	}
	
	private static Map<Integer, String> getPatientDataForMR(Map<String, String> patientData, FieldsMappingRule mappingRule) {
		final Map<Integer, String> rulePatientData = new HashMap<>();
		for (Entry<Integer, Term> entry : mappingRule.getSrcPropMap().entrySet()) {
			final Integer index = entry.getKey();
			final String uri = entry.getValue().getUri();
			final String uid = paramUriToABC(uri);
			final String value = patientData.get(uid);
			rulePatientData.put(index, value);
		}
		return rulePatientData;
	}
	
	private static Individual createEntityBasedOnData(final OntModel harmOnto, final FieldsMappingRule mr, final Map<Integer, PropertyValue> paramValuesMap, Boolean bool) {
		final OntClass entityClass = harmOnto.getOntClass(mr.getTrgEntityClass().getUri());
		final Individual entity = entityClass.createIndividual(NS + entityClass.getLocalName() + "-" + OntoDataRepo.getInstance().nextIndexAsStr(entityClass));
		for (Entry<Integer, Term> entry : mr.getTrgPropMap().entrySet()) {
			final int index = entry.getKey();
			final String propUri = entry.getValue().getUri();
			final OntProperty prop = harmOnto.getOntProperty(propUri);
			final PropertyValue propValue = paramValuesMap.get(index);
			if (propValue != null) {
				if (propValue instanceof PropertyValueList) {
					PropertyValueList pvList = (PropertyValueList) propValue;
					for (PropertyValue pv : pvList.getPropValueList()) {
						final RDFNode data = (pv != null) ? pv.toRDFNode(harmOnto, prop.getRange()) : null; 
						if (data != null) {
							checkPropValueCompatibility(prop, data);
							entity.addProperty(prop, data);
						}
					}
				} else {
					final RDFNode data;
					try { 
						data = (propValue != null) ? propValue.toRDFNode(harmOnto, prop.getRange()): null; 
					} catch (Throwable t) { 
						throw new RuntimeException("Error when executing method: propValue.toRDFNode() of Object: " + propValue, t); 
					}
					if (data != null) {
						checkPropValueCompatibility(prop, data);
						entity.setPropertyValue(prop, data);
					}
				}
			} // END OF IF (propValue != null)
		}
		final String prefix = (bool == Boolean.FALSE) ? "NOT.. " : ""; 
		LogsArea.println(CLINICAL, prefix + entityClass.getLocalName() + " : " + propValueMapAsString(paramValuesMap));
		LogsArea.println(TECHNICAL, prefix + entityClass.getLocalName() + " : " + propValueMapAsAnonymString(paramValuesMap));
		return entity;
	}
	
	private static final List<String> demoKeys = Util.newArrayList("Gender", "Ethnicity", "Education", "Occupation", "Body-Weight");
	private static final List<String> pregsmokKeys = Util.newArrayList("Pregnancy", "Tobacco-Consumption");
	private static final List<String> labtestsKeys = Util.newArrayList("Laboratory-Test", "Medical-Imaging-Test");
	private static final List<String> biopsKeys = Util.newArrayList("Biopsy");
	
	private static final List<String> questKeys = Util.newArrayList("Questionnaire-Score", "CACI-Comorbidity", "ESSDAI-Domain-AL");
	private static final List<String> intervKeys = Util.newArrayList("Medication", "Chemotherapy", "Surgery");
	private static final List<String> conditionKeys = Util.newArrayList("Symptom-or-Sign", "Diagnosis");
	
	
	private static String getCategoryKey(final String type) {
		if (demoKeys.contains(type)) return "demographic-JA";
		if (pregsmokKeys.contains(type)) return "pregnancy-smoking-JA";
		if (labtestsKeys.contains(type)) return "lab-Test-JA";
		if (biopsKeys.contains(type)) return "biopsy-Test-JA";
		if (questKeys.contains(type)) return "questionnaire-JA";
		if (conditionKeys.contains(type)) return "condition-JA";
		if (intervKeys.contains(type)) return "intervention-JA";
		return "other-JA";
	}
	
	private static JSONObject createJsonObjBasedOnData(final OntModel harmOnto, final FieldsMappingRule mr, final Map<Integer, PropertyValue> paramValuesMap) {
		final OntClass entityClass = harmOnto.getOntClass(mr.getTrgEntityClass().getUri());
		final JSONObject json = new JSONObject();
		json.put("type", entityClass.getLocalName());
		for (Entry<Integer, Term> entry : mr.getTrgPropMap().entrySet()) {
			final int index = entry.getKey();
			final String propUri = entry.getValue().getUri();
			final OntProperty prop = harmOnto.getOntProperty(propUri);
			final PropertyValue propValue = paramValuesMap.get(index);
			if (propValue != null) {
				if (propValue instanceof PropertyValueList) {
					final JSONArray ja = new JSONArray();
					PropertyValueList pvList = (PropertyValueList) propValue;
					for (PropertyValue pv : pvList.getPropValueList()) {
						final Object data = (pv != null) ? pv.toJsonObj() : null; 
						if (data != null) {
							ja.add(data);
						}
					}
					json.put(prop.getLocalName() + "-JA", ja);
				} else {
					final Object data;
					try { 
						data = (propValue != null) ? propValue.toJsonObj() : null; 
					} catch (Throwable t) { 
						throw new RuntimeException("Error when executing method: propValue.toRDFNode() of Object: " + propValue, t); 
					}
					if (data != null) {
						json.put(prop.getLocalName(), data);
					}
				}
			} // END OF IF (propValue != null)
		}
		return json;
	}
	
	private static void checkPropValueCompatibility(OntProperty prop, RDFNode data) {
		if (prop == null || data == null) return;
		final OntResource propRange = prop.getRange();
		if (propRange == null) return;
		
		// Ignore those cases when the value of a data type property can be DT1 or DT2
		if (prop.isDatatypeProperty() && propRange.isAnon()) return;
		
		// RDF Literals
		if (propRange.isOntLanguageTerm()) {
			if (data.isLiteral()) {
				final Literal dataLiteral = data.asLiteral();
				if (propRange.getURI().equals(dataLiteral.getDatatypeURI())) return;
				// Deliberately ignore incompatibility between date and dateTime data types
				if (propRange.getLocalName().equals("dateTime") && dataLiteral.getDatatypeURI().equals("http://www.w3.org/2001/XMLSchema#date")) return;
				log.error("The value of property \"" + prop.getLocalName() + "\" should be an RDF Literal of type \"" + propRange.getLocalName() + "\" ! "
					+ "Existing value \"" + data + "\" is an RDF Literal with DataType: \"" + dataLiteral.getDatatypeURI() + "\"");
			} else {
				log.error("The value of property \"" + prop.getLocalName() + "\" should be an RDF Literal of type \"" + propRange.getLocalName() + "\" ! "
					+ "Existing value: \"" + data + "\" is not an RDF Literal.");
			}
		}
		// OWL Instances
		if (propRange.isAnon()) {
			final OntClass propRangeCls = propRange.asClass();
			if (data.canAs(Individual.class)) {
				final Individual dataIndiv = data.as(Individual.class);
				if (indivClassIsaRangeClass(dataIndiv.getOntClass(), propRangeCls)) return;
				
				log.error("The value of property \"" + prop.getLocalName() + "\" should be an OWL instance (individual) of type \"" + propRange.getLocalName() + "\" ! "
					+ "Existing value \"" + data + "\" is an OWL instance (individual) of type: \"" + dataIndiv.getOntClass().getLocalName() + "\"");
			} else {
				log.error("The value of property \"" + prop.getLocalName() + "\" should be an OWL instance (individual) of type \"" + propRange.getLocalName() + "\" ! "
					+ "Existing value: \"" + data + "\" is not an OWL instance (individual).");
			}
		}
	}
	
	private static boolean indivClassIsaRangeClass(OntClass indivCls, OntClass propRangeCls) {
		// CLASS
		if (propRangeCls.isURIResource()) {
			if (propRangeCls.getURI().equals(indivCls.getURI())) return true;
			final ExtendedIterator<OntClass> extClsIt = propRangeCls.listSubClasses();
			while (extClsIt.hasNext()) {
				final OntClass propRangeSubCls = extClsIt.next();
				return indivClassIsaRangeClass(indivCls, propRangeSubCls);
			}
		} 
		// UNION EXPRESSION
		else if (propRangeCls.isUnionClass()) {
			final UnionClass unitClass = propRangeCls.asUnionClass();
			final ExtendedIterator<? extends OntClass> opExtIt = unitClass.listOperands();
			while (opExtIt.hasNext()) {
				final OntClass opCls = opExtIt.next();
				if (indivClassIsaRangeClass(indivCls, opCls)) return true;
			}
		} 
		// OTHER - Update IF needed
		else {
			log.warn("The Range of a property is neither an existing OWL class nor an OR exrpession.");
		}
		
		return false;
	}
	
	private static Individual createVisitEntity(final OntModel harmOnto) {
		final OntClass entityClass = harmOnto.getOntClass(HeathcareEntityVisitUri);
		final Individual entity = entityClass.createIndividual(NS + entityClass.getLocalName() + "-" + OntoDataRepo.getInstance().nextIndexAsStr(entityClass));
		entity.setPropertyValue(
			harmOnto.getOntProperty(visitDateUri), 
			new DateValue(new Date()).toRDFNode(harmOnto, null) 
		);
		entity.setPropertyValue(
			harmOnto.getOntProperty(visitNumberUri), 
			new IntegerValue(0).toRDFNode(harmOnto, null)
		);
		entity.setPropertyValue(
			harmOnto.getOntProperty(visitStatusUri), 
			new SimpleCodedValue("VIS-STAT-02", "Visit \"Closed\"").toRDFNode(harmOnto, null)
		);
		entity.setPropertyValue(
			harmOnto.getOntProperty(visitTypeUri),
			new SimpleCodedValue("VISIT-00", "Data Harmonization Process").toRDFNode(harmOnto, null)
		);
		return entity;
	}
	
	private static void createCohortEntity(final OntModel harmOnto, final List<String> termUriList) {
		final OntClass entityClass = harmOnto.getOntClass(CohortUri);
		final Individual entity = entityClass.createIndividual(NS + entityClass.getLocalName() + "-" + OntoDataRepo.getInstance().nextIndexAsStr(entityClass));
		final OntProperty ontProp = harmOnto.getOntProperty(cohortAttributeUri);
		for (String termUri : termUriList) {
			final OntClass ontClass = harmOnto.getOntClass(termUri);
			if (ontClass == null) throw new RuntimeException("Cannot FIND OWL class with URI: " + termUri);
			final String str = termUri + " :: " + ontClass.getLabel(null);
			entity.addProperty(ontProp, str);
		}
	}
	
	private static JSONObject createCohortJsonObj(final OntModel harmOnto, final List<String> termUriList) {
		final JSONObject json = new JSONObject();
		final JSONArray ja = new JSONArray();
		for (String termUri : termUriList) {
			final OntClass ontClass = harmOnto.getOntClass(termUri);
			if (ontClass == null) throw new RuntimeException("Cannot FIND OWL class with URI: " + termUri);
			final JSONObject jo = new JSONObject();
			jo.put("code", ontClass.getLocalName());
			jo.put("label", ontClass.getLabel(null));
			ja.add(jo);
		}
		json.put("term-JA", ja);
		return json;
	}
	
	/* Used in the "For Debugging ..." code */

	private static String mappingRuleToOneLineString(FieldsMappingRule mr) {
		final List<String> srcFieldsList = new ArrayList<>();
		for (Entry<Integer, Term> entry : mr.getSrcPropMap().entrySet()) {
			final Integer index = entry.getKey();
			final Term term = entry.getValue();
			srcFieldsList.add(index + ":" + paramUriToABC(term.getUri()) + "(" + term.getLabel() + ")");
		}
		final List<String> trgPropLocNameList = new ArrayList<>();
		for (Entry<Integer, Term> entry : mr.getTrgPropMap().entrySet()) {
			final Integer index = entry.getKey();
			final Term term = entry.getValue();
			trgPropLocNameList.add(index + ":" + term.getLocalName() + "(" + term.getLabel() + ")");
		}
		
		return 
			"Fields-MappingRule: " + 
				Util.listToOneLineString(srcFieldsList) + " --> " + mr.getTrgEntityClass().getLabel() + " " +
					Util.listToOneLineString(trgPropLocNameList) + " :: " + 
						(mr.getDtService().getClass().getSimpleName() + " : " + Util.mapToOneLineString(mr.getDtService().getArgs()));
	}

	private static String mappingRuleToOneLineStringV2(FieldsMappingRule mr) {
		final List<String> srcFieldsList = new ArrayList<>();
		for (Entry<Integer, Term> entry : mr.getSrcPropMap().entrySet()) {
			final Integer index = entry.getKey();
			final Term term = entry.getValue();
			srcFieldsList.add(index + ":" + paramUriToABC(term.getUri()));
		}
		final List<String> trgPropLocNameList = new ArrayList<>();
		for (Entry<Integer, Term> entry : mr.getTrgPropMap().entrySet()) {
			final Integer index = entry.getKey();
			final Term term = entry.getValue();
			trgPropLocNameList.add(index + ":" + term.getLocalName());
		}
		
		return 
			"Fields-MappingRule: " + Util.listToOneLineString(srcFieldsList) + 
				" --> " + mr.getTrgEntityClass().getLocalName() + " " +
				Util.listToOneLineString(trgPropLocNameList) + 
				" :: " +  (mr.getDtService().getClass().getSimpleName() + Util.mapToOneLineString(mr.getDtService().getArgs()));
	}

	private static String mappingRuleToString(TermsMappingRule mr) {
		return "Terms--MappingRule: " + mr.getSrcTermExpr().asString() + " " /*+ "--" + mr.getRelation()*/ + "--> " + mr.getTrgTermExpr().asString();
	}
	
	
	private static String propValueMapAsString(final Map<Integer, PropertyValue> paramValuesMap) {
		if (paramValuesMap == null || paramValuesMap.isEmpty()) return  "";
		final List<PropertyValue> propValueList = new ArrayList<>();
		for (Entry<Integer, PropertyValue> entry : paramValuesMap.entrySet()) {
			if (entry.getValue() != null) {
				propValueList.add(entry.getValue());
			}
		}
		return Util.listToOneLineString(propValueList, new I_StringTransformer<PropertyValue>() {
			@Override public String asString(PropertyValue pv) {
				return pv.asString();
			}
		});
	}
	
	private static String propValueMapAsAnonymString(final Map<Integer, PropertyValue> paramValuesMap) {
		if (paramValuesMap == null || paramValuesMap.isEmpty()) return  "";
		final List<PropertyValue> propValueList = new ArrayList<>();
		for (Entry<Integer, PropertyValue> entry : paramValuesMap.entrySet()) {
			if (entry.getValue() != null) {
				propValueList.add(entry.getValue());
			}
		}
		return Util.listToOneLineString(propValueList, new I_StringTransformer<PropertyValue>() {
			@Override public String asString(PropertyValue pv) {
				return pv.asAnonymString();
			}
		});
	}

	private static void addNegativeProperty(final OntModel harmOnto, Individual indiv, Property prop, RDFNode node) {
		final Resource r = harmOnto.createResource((String)null);
		r.addProperty(RDF.type, OWL2.NegativePropertyAssertion);
		r.addProperty(OWL2.sourceIndividual, indiv);
		r.addProperty(OWL2.assertionProperty, prop);
		r.addProperty(OWL2.targetIndividual, node);
	}
	
	private static boolean isEntityAboutAntibodies(Collection<PropertyValue> propValueCollections) {
		if (propValueCollections == null) return false;
		for (PropertyValue propertyValue : propValueCollections) {
			if (propertyValue instanceof SimpleCodedValue) {
				final SimpleCodedValue scv = (SimpleCodedValue) propertyValue;
				// Auto-antibody Test(BLOOD-500) or Other Autoantibody Test(BLOOD-550)
				if (scv.getCode().startsWith("BLOOD-5")) {
					return true;
				}
			}
		}
		return false;
	}
	
}
