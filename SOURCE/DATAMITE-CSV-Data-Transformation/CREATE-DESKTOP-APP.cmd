@ECHO OFF
REM SET JAVA_HOME=C:\Program Files\Java\jdk1.8.0_31
SET JAVA_HOME=C:\Program Files\Java\openjdk-8u392-b08-windows-64
SET PATH=%JAVA_HOME%\bin;%PATH%
ECHO JAVA_HOME=%JAVA_HOME%
ECHO.
CALL java -version
ECHO.
CALL ant -f desktop-app-build.xml
TIMEOUT /t 3