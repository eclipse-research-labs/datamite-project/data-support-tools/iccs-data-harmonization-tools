package ${PACKAGE-NAME};

import java.util.List;
import java.util.Map;

import org.timchros.core.tuple.Tuple2;

import java.util.HashMap;

import ntua.iccs.harmonicss.cohort.data.owl.PropertyValue;
import ntua.iccs.harmonicss.cohort.trans.DataTransformation;

/** ${CLASS-DESC} */
public class ${CLASS-NAME} extends DataTransformation {

	private enum ServiceArg { ${ARGS-NAME} }
	
	/** ${METHOD-DESC}
	 *  <p>
	 *  @return The amount of instances (i.e., List size() ) of OWL class ${ENTITY-CLASS} that should be created along with the parameters of each one of them (i.e., Map), otherwise <code>null</code> (if none instance should be created)
	 */
	@Override
	public Tuple2<Boolean, List<Map<Integer, PropertyValue>>> transform(Map<Integer, String> data) {
		
		// Input Fields
${METHOD-BODY-IN-FIELDS}

		// Additional Data / Arguments (if any)
${METHOD-BODY-IN-ARGS}
		
		compareGivenArgsWithExpected(getEnumNameSet(ServiceArg.class));
		
		// Data Processing

		System.out.println(" ** WHEN MR USED - THIS MSG SHOULD APPEAR IN YOUR SCREEN **");

		/* TODO:
		 * Write the code that produces the Property Values of an entity 
		 * ${ENTITY-CLASS}
		 * based on the given Data (Both Fields and Arguments) 
		 */
		
		// Since not implemented yet, return null so that none OWL instance created
		if (true) return null;
		
		final Map<Integer, PropertyValue> map = new HashMap<>();
		
${METHOD-BODY-OUT}
		
		return newPosTuple(map);
	}

}
