package org.ntua.ponte.test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.ntua.ponte.db2owl.util.SqlTypesRepository;

/**
 * This class is being used for testing purposes
 * 
 * @author Efthymios Chondrogiannis
 */
public class TestPlace {

	private static final String DB_URL = "jdbc:oracle:thin:@localhost:1521/xe";
//	private static final String DB_URL = "jdbc:mysql://ponte.grid.ece.ntua.gr:3306/cnr";
	
	private static final String ORACLE = "oracle";
	private static final String MYSQL = "mysql";
	
	public static void main(String[] args) {

		System.out.println("******** START ********");
		
		System.out.println(" > Part 1");
		
		// Check whether the given URL comes from an Oracle or MySQL database
		boolean isOracle = DB_URL.contains(ORACLE);
		boolean isMySql = DB_URL.contains(MYSQL);
		
		System.out.println("isOracle: " + isOracle);
		System.out.println("isMySql: " + isMySql);
		
		System.out.println(" > Part 2");
		
		final Map<Integer, String> sqlTypeMap = new HashMap<Integer, String>();
		
		for(Field field : Types.class.getFields()) {
			int mods = field.getModifiers();
			if(Modifier.isPublic(mods) && Modifier.isStatic(mods) && int.class.equals(field.getType())) {
				try {
					sqlTypeMap.put((Integer) field.get(null), Types.class.getName() + "." + field.getName());
				} catch(IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		for (Entry<Integer, String> entry : sqlTypeMap.entrySet()) {
			System.out.println(entry.getValue());
		}
		
		System.out.println(" > Part 3");
		
		System.out.println( SqlTypesRepository.classToString() );
		
		
		System.out.println("********* END *********");
		
	}

}
