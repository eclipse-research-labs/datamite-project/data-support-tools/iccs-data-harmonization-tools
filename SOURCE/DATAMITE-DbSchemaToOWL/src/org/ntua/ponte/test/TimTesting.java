package org.ntua.ponte.test;

public class TimTesting {

	public static void main(String[] args) {

		final String initStr = "\"liothyronine sodium AND thyroid hormone receptor, beta(THRB)\"";
		System.out.println("initStr = " + initStr);
		final String newStr = removeQuotes(initStr);
		System.out.println("newStr = " + newStr);
	}

	public static final String removeQuotes(final String str) {
		if (str == null) return null;
		String trimStr = str.trim();
		if (trimStr.length() < 2) return trimStr;
		if (trimStr.startsWith("\"")) trimStr = trimStr.substring(1, trimStr.length());
		if (trimStr.endsWith("\"")) trimStr = trimStr.substring(0, trimStr.length()-1);
		final String site = "site:www.ncbi.nlm.nih.gov";
		if (trimStr.contains(site)) {
			return trimStr;
		} else {
			return site + " " + trimStr;
		}
	}
	
}
