package org.ntua.ponte.db2owl;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ntua.ponte.db2owl.onto.SchemaOntology;
import org.ntua.ponte.db2owl.util.DbOntologyProvider;

@WebServlet("/GenerateSchemaOntology")
public class GenerateSchemaOntology extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	
    public GenerateSchemaOntology() {

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Get User Data
		final String dbURL = request.getParameter("dburl");
		final String dbUsername = request.getParameter("username");
		final String dbPassword = request.getParameter("password");
		
		final String ontoLabel = request.getParameter("ontoname");
		final String ontoURI = request.getParameter("ontourl");
		
		// Produce Ontology
		try {
			SchemaOntology onto = DbOntologyProvider.exportDbSchemaOntology(dbURL, dbUsername, dbPassword, ontoLabel, ontoURI);
			
			// Return OWL File
			response.setContentType("text/plain");
			response.setHeader("Content-disposition", "attachment; filename=Db-Schema.owl");
			final PrintWriter pw = response.getWriter();
//			pw.write("USER DATA:\n\n");
//			pw.write("dbURL: " + dbURL + "\n");
//			pw.write("dbUsername: " + dbUsername + "\n");
//			pw.write("dbPassword: " + dbPassword + "\n");
			onto.writeToPrintWriter(pw);
			pw.close();
		} catch(Throwable t) {
			throw new RuntimeException("Error when producing OWL ontology.", t);
		}
	}

}
