package org.ntua.ponte.db2owl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ntua.ponte.db2owl.onto.VocabOntology;
import org.ntua.ponte.db2owl.util.DbOntologyProvider;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@WebServlet("/GenerateVocabTableOntology")
public class GenerateVocabTableOntology extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public GenerateVocabTableOntology() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String dbURL = request.getParameter("dburl");
		final String dbUsername = request.getParameter("username");
		final String dbPassword = request.getParameter("password");
		final String ontoLabel = request.getParameter("ontoname");
		final String ontoURI = request.getParameter("ontourl");
		final String jsonStr = request.getParameter("json");

		System.out.println(" * " + dbURL + " - " + dbUsername + " - " + dbPassword + " :: " + ontoLabel + " - " + ontoURI);
		
		final JSONObject json = JSONObject.fromObject( jsonStr );
		
		try {
			final JSONArray tableJA = json.getJSONArray("tablesJA");
			final List<String> tableList = new ArrayList<String>();
			for (Object table : tableJA) {
				tableList.add("" + table);
			}
			final VocabOntology onto = DbOntologyProvider.exportDbTablesVocabOntology(dbURL, dbUsername, dbPassword, ontoLabel, ontoURI, tableList);
			response.setContentType("text/plain");
			response.setHeader("Content-disposition", "attachment; filename=Db-Tables-Vocab.owl");
			final PrintWriter pw = response.getWriter();
			onto.writeToPrintWriter(pw);
			pw.close();
		} catch(Throwable t) {
			throw new RuntimeException("Error when processing REQUEST..", t);
		}
	}

}
