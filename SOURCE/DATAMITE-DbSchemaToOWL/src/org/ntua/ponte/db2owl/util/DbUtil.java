package org.ntua.ponte.db2owl.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.timchros.core.util.Checks;

public class DbUtil {

	private DbUtil() {
		
	}
	
	public static Connection openConnection(final String dbUrl, final String username, final String password) throws Exception {
		// Check parameters provided
		Checks.checkNotNullArg(dbUrl, "dbUrl", username, "username", password, "password");
		
		// Load the appropriate database driver
		Class.forName("com.mysql.jdbc.Driver");
		
		// Specify Database properties
		final Properties props = new Properties();
		props.setProperty("user"	, username);
		props.setProperty("password", password);
		props.setProperty("charSet"	, "UTF-8");
		props.put("remarksReporting","true");
		
		// Open a Connection to the Database
		return DriverManager.getConnection(dbUrl, props);
	}

}
