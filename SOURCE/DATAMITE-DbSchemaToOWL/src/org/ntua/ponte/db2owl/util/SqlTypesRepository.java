package org.ntua.ponte.db2owl.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.XSD;

//import com.hp.hpl.jena.rdf.model.Resource;
//import com.hp.hpl.jena.vocabulary.XSD;

import org.timchros.core.util.Util;

/**
 * This class contains the <i>correspondence</i> between <b>SQL Data Types</b> and <b>OWL Data Types</b>.
 * 
 * @author Efthymios Chondrogiannis
 */
public class SqlTypesRepository {

	private static final Map<Integer, String> sqlTypeMap = new HashMap<Integer, String>();
	private static final Map<String, Resource> owlTypesMap = new HashMap<String, Resource>();
	
	static {
		// SQL Types Map
		for(Field field : Types.class.getFields()) {
			int mods = field.getModifiers();
			if(Modifier.isPublic(mods) && Modifier.isStatic(mods) && int.class.equals(field.getType())) {
				try {
					sqlTypeMap.put((Integer) field.get(null), Types.class.getName() + "." + field.getName());
				} catch(IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		// OWL Types Map
		owlTypesMap.put("java.sql.Types.INTEGER", XSD.integer);
		owlTypesMap.put("java.sql.Types.DECIMAL", XSD.xfloat);
		owlTypesMap.put("java.sql.Types.CHAR", XSD.xstring);
		owlTypesMap.put("java.sql.Types.VARCHAR", XSD.xstring);
		owlTypesMap.put("java.sql.Types.LONGVARCHAR", XSD.xstring);
		owlTypesMap.put("java.sql.Types.DATE", XSD.date);
		
		//TODO: Include more mappings among SQL and JAVA datatypes +++
		
	}
	
	/** Ensures that we will not create an instance of this class */
	private SqlTypesRepository() {
		
	}
	
	/**
	 * @param sqlTypeID
	 * @return
	 * 		Returns the appropriate SQL Type based on  the given ID.
	 */
	public static String getSqlTypeByID(int sqlTypeID) {
		if ( ! sqlTypeMap.containsKey(sqlTypeID) ) 
			throw new RuntimeException("Unexpected Error... Unknown SQL Type with ID \"" + sqlTypeID + "\".");
		return sqlTypeMap.get(sqlTypeID);
	}
	
	/**
	 * @param sqlType
	 * @return
	 * 		Returns the appropriate OWL Type based on the given SQL Type (string)
	 */
	public static Resource getOwlTypeBySqlType(String sqlType) {
		return owlTypesMap.get(sqlType);
	}
	
	/**
	 * @param sqlTypeID
	 * @return
	 * 		Returns the appropriate OWL Type based on the given SQL Type ID (int)
	 */
	public static Resource getOwlTypeBySqlTypeID(int sqlTypeID) {
		return owlTypesMap.get( getSqlTypeByID(sqlTypeID) );
	}

	/**
	 * Provides more information about this class.
	 * 
	 * @return
	 * 		A description of this class.
	 */
	public static String classToString() {
		return 
			"SqlTypesRepository [ mappings-size: \"" + owlTypesMap.size() + "\"" + "\n" +
				Util.mapToMultiLineString(owlTypesMap) + "\n" + 
			"]";
	}
	
}
