package org.ntua.ponte.db2owl.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.XSD;
import org.ntua.ponte.db2owl.onto.SchemaOntology;
import org.ntua.ponte.db2owl.onto.VocabOntology;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Checks;
import org.timchros.core.util.Throable;
import org.timchros.core.util.Util;

//import com.hp.hpl.jena.ontology.ObjectProperty;
//import com.hp.hpl.jena.ontology.OntClass;
//import com.hp.hpl.jena.ontology.OntProperty;
//import com.hp.hpl.jena.rdf.model.Resource;
//import com.hp.hpl.jena.util.iterator.ExtendedIterator;
//import com.hp.hpl.jena.vocabulary.XSD;

public class DbOntologyProvider {
	// jdbc:mysql://ponte.grid.ece.ntua.gr:3306/cnr
	// chondrog 	chondrog
	
	private static final String CREATOR = "Institute of Communications and Computer Systems of National Technical University of Athens - ICCS/NTUA";

//	/* DB -  CONNECTION - PROPERTIES */
//	public static final String DB_URL = "jdbc:mysql://147.102.33.126:3306/ponteglobaldb";
//	public static final String DB_USERNAME = "tsipo";
//	public static final String DB_PASSWORD = "freedom";
//	
//	/* ONTOLOGY - PROPERTIES */
//	public static final String ONTO_LABEL = "Schema-Ontology.owl";
//	public static final String ONTO_URI = "http://semantic.web/2011/06/IoprEhrDbOntology";
	
	/* DB -  CONNECTION - PROPERTIES */
	public static final String DB_URL = "jdbc:mysql://localhost:3306/test-db"; // "jdbc:mysql://localhost:3306/cnrnewdb";
	public static final String DB_USERNAME = "root";
	public static final String DB_PASSWORD = "";
	
	/* ONTOLOGY - PROPERTIES */
	public static final String ONTO_LABEL = "Schema-Ontology.owl";
	public static final String ONTO_URI = "http://semantic.web/2024/04/Test-DB-Schema-Ontology";
	
	/** For testing purposes... */
	public static void main(String[] args) throws Exception {
		System.out.println(" >> DbOntologyProvider: START - " + new Date());
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		
		
		System.out.println("Generate OWL ontology");
		final SchemaOntology onto = exportDbSchemaOntology(DB_URL, DB_USERNAME, DB_PASSWORD, ONTO_LABEL, ONTO_URI);
		System.out.println("Save to OWL File");
		onto.saveToFile("TEST-DATA/GenOntology.owl");
		System.out.println();
		System.out.println(onto.sumMessage());
		
		
//		final List<String> tableTupleList1 = new ArrayList<String>();
//		tableTupleList1.add("person_sex");
//		final VocabOntology vocabOntology1 =  exportDbTablesVocabOntology(DB_URL, DB_USERNAME, DB_PASSWORD, "HE TABLE VOCAB", ONTO_URI, tableTupleList1);
//		vocabOntology1.saveToFile("TEST-DATA/GenOntoTable-Values.owl");
//		System.out.println(vocabOntology1.sumMessage());
		

//		final List<Tuple2<String, List<String>>> tableTupleList2 = new ArrayList<Tuple2<String,List<String>>>();
//		tableTupleList2.add(Tuple2.newTuple2("person_data", Util.newArrayList("sex_id")));
//		final VocabOntology vocabOntology2 =  exportDbTablesFieldsVocabOntology(DB_URL, DB_USERNAME, DB_PASSWORD, "HE TABLE-FIELD VOCAB", ONTO_URI, tableTupleList2);
//		vocabOntology2.saveToFile("TEST-DATA/GenOntoTable-Field-Values.owl");
//		System.out.println(vocabOntology2.sumMessage());
		
		
		System.out.println("\n >> DbOntologyProvider: END - " + new Date());
	}
	
	public static VocabOntology exportDbTablesVocabOntology(final String dbUrl, final String username, final String password, final String ontoLabel, final String ontoURI, final List<String> tableList) throws Exception {
		final String ns = ontoURI + "#";
		final VocabOntology vocabOnto = new VocabOntology(ns);
		
		vocabOnto.setOntoProperties(ontoURI, ontoLabel, dbUrl, CREATOR, "TABLES");
		
		
		
		// Open connection to DB
		final Connection con = DbUtil.openConnection(dbUrl, username, password);
		final DatabaseMetaData meta = con.getMetaData();
		
		
		for (String table : tableList) {
			final String rootClassURI = ns + table;
			final OntClass root = vocabOnto.createRootOntClass(rootClassURI, table);
			
			// Get Table FIELDS
			final List<String> fieldsNameList = new ArrayList<String>();
    		ResultSet rsColumns = meta.getColumns(null, null, table.toUpperCase(), null);
    		while (rsColumns.next()) {
    			final String columnName = rsColumns.getString("COLUMN_NAME");
    			fieldsNameList.add(columnName);
    		}
    		rsColumns.close();
    		
    		// Create Annotation Properties
    		for (String fieldName : fieldsNameList) {
    			final String name = table + "_" + fieldName;
    			final String propURI = ns + name;
				vocabOnto.createAnnotationProperty(propURI, table + "_" + name);
			}
    		
    		// SQL Query for retrieving DISTINCT values
			final String SQL = "SELECT * FROM " + table;
    		
			// Execute Query
			final Statement stmt = con.createStatement();
			final ResultSet rs = stmt.executeQuery(SQL);
			
			// Get SQL Query Results
			int index = 0;
			while( rs.next() ) {
				index++;
				final String classURI = rootClassURI + "_" + index;
				// GET ALL VALUES
				final List<Tuple2<String, String>> annoList = new ArrayList<Tuple2<String,String>>();
				for (String fieldName : fieldsNameList) {
					final String name = table + "_" + fieldName;
	    			final String propURI = ns + name;
					final String valueToString = "" + rs.getObject(fieldName);
					annoList.add(Tuple2.newTuple2(propURI, valueToString));
				}
				
				// OWL SUB CLASS
				vocabOnto.createOntClassUnderRootClass(classURI, annoList, root);
			}
			
			// Close Objects
			rs.close();
		}
		
		// Close Connection
		con.close();	
		
		return vocabOnto;
	}
	
	public static VocabOntology exportDbTablesFieldsVocabOntology(final String dbUrl, final String username, final String password, final String ontoLabel, final String ontoURI, final List<Tuple2<String, List<String>>> tableTupleList) throws Exception {
		final String ns = ontoURI + "#";
		final VocabOntology vocabOnto = new VocabOntology(ns);
		
		vocabOnto.setOntoProperties(ontoURI, ontoLabel, dbUrl, CREATOR, "TABLES-FIELDS");
		
		// Open connection to DB
		final Connection con = DbUtil.openConnection(dbUrl, username, password);
	
		for (Tuple2<String, List<String>> tableTuple : tableTupleList) {
			final String table = tableTuple._1();
			final List<String> fieldsList = tableTuple._2();
			for (String field : fieldsList) {
				final String name = table + "_" + field;
				final String rootClassURI = ns + name;
				
				// ROOT OWL CLASS
				final OntClass root = vocabOnto.createRootOntClass(rootClassURI, table,  field);
				
				// SQL Query for retrieving DISTINCT values
				final String SQL = "SELECT DISTINCT " + field + " FROM " + table + " ORDER BY " + field;
				// Execute Query
				final Statement stmt = con.createStatement();
				final ResultSet rs = stmt.executeQuery(SQL);
				
				// Get SQL Query Results
				while( rs.next() ) {
					final String valueToString = "" + rs.getObject(1);
					final String classURI = rootClassURI + "_" + valueToString;
					
					// OWL SUB CLASS
					vocabOnto.createOntClassUnderRootClass(classURI, valueToString, root);
				}
				
				// Close Objects
				rs.close();
				stmt.close();
			}
		}
		
		// Close Connection
		con.close();	
		
		return vocabOnto;
	}
	
	public static SchemaOntology exportDbSchemaOntology(final String dbUrl, final String username, final String password, final String ontoLabel, final String ontoUri) throws Exception {
		// Check parameters provided
		Checks.checkNotNullArg(dbUrl, "dbUrl", username, "username", password, "password", ontoLabel,  "ontoLabel", ontoUri, "ontoUri");
		
		// Load the appropriate database driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		// Class.forName("com.mysql.jdbc.Driver");
		
		// Specify Database properties
		final Properties props = new Properties();
		props.setProperty("user"	, username);
		props.setProperty("password", password);
		props.setProperty("charSet"	, "UTF-8");
		props.put("remarksReporting","true");		// Important:: Enable comments !
		
		// Open a Connection to the Database
		final Connection connection = DriverManager.getConnection(dbUrl, props);
		
		// Create (an empty) OWL ontology
		SchemaOntology owlOntology = new SchemaOntology();
		owlOntology.addOntologyProperties(ontoUri, dbUrl, ontoLabel, CREATOR);
		owlOntology.setNamespacePrefix(ontoUri);
		
		final String namespace = ontoUri + "#";
		
		final int slash_index = dbUrl.lastIndexOf("/");
		if (slash_index <=0)
			throw new RuntimeException("Cannot find DB name from DB URL: " + dbUrl);
		final String db_name = dbUrl.substring(slash_index + 1);
//		System.out.println(db_name);
//		System.out.println("===============================");
		
		// Get DB TABLES 
		final DatabaseMetaData meta = connection.getMetaData();
		final String catalog = db_name; // null; 
		final String schemaPattern =   null; 
		final ResultSet dbTablesResultSet = meta.getTables(catalog, schemaPattern, null, new String[] {"TABLE"});  
		
	    // For each table...
	    while (dbTablesResultSet.next()) {    
	    	final String tableName = dbTablesResultSet.getString("TABLE_NAME"); 
	    	final String tableComment = dbTablesResultSet.getString("REMARKS");
	    
	    	// Ignore Magento tables 
	    	if (tableName.startsWith("pma_")) continue;
	    	
//	    	System.out.println();
//	    	System.out.println("- TABLE: " + tableName + " :: " + tableComment); 
//	    	System.out.println("------------------------------");
	    	
    		// >> CLASS: Create an owl:Class based on the NAME of each table
    		final String tableUri = namespace + tableName;
    		final OntClass ontClass = owlOntology.createOntClass(tableUri, tableName);
    		
    		// Find primary keys
    		final List<String> pkFieldList = new ArrayList<String>();
    		ResultSet exportedKeysRS = null; try { exportedKeysRS = meta.getPrimaryKeys(connection.getCatalog(), null, tableName); } catch (Exception e) { System.out.println(Throable.throwableAsSimpleOneLineString(e)); continue ; }
    		while (exportedKeysRS.next()){
    			final String pkColumnName = exportedKeysRS.getString("COLUMN_NAME");
    			pkFieldList.add(pkColumnName);
    		}
    		exportedKeysRS.close();
    		
    		// Find unique value fields
    		final List<String> uniqueFieldList = new ArrayList<String>();
    		ResultSet uniqueFieldsRs = null; try { uniqueFieldsRs =  meta.getIndexInfo(null, null, tableName, true, true);  } catch (Exception e) { System.out.println(Throable.throwableAsSimpleOneLineString(e)); continue ; }
            while(uniqueFieldsRs.next()) {
                final String uniqueFieldColumnName = uniqueFieldsRs.getString("COLUMN_NAME");
                uniqueFieldList.add(uniqueFieldColumnName);
            }
            uniqueFieldsRs.close();
    		
    		// Find foreign keys and put them in a map with the tables having them as primary.
    		final Map<String, String> foreignKeyMap = new HashMap<String, String>();
    		ResultSet importedKeysRS = null; try { importedKeysRS = meta.getImportedKeys(connection.getCatalog(), null, tableName); } catch (Exception e) { System.out.println(Throable.throwableAsSimpleOneLineString(e)); continue ; }
    		while (importedKeysRS.next()){
    			String fkColumnName = importedKeysRS.getString("FKCOLUMN_NAME");
    			String pkTableName = importedKeysRS.getString("PKTABLE_NAME");
    			foreignKeyMap.put(fkColumnName, pkTableName);
    		}
    		importedKeysRS.close();
    		
    		// Get Table FIELDS
    		ResultSet rsColumns = meta.getColumns(null, null, tableName.toUpperCase(), null);
    		
    		// Indicates whether table has a foreign key or not.
    		boolean hasTableForeignKey = false;

    		// Tmp List for Identifiers
    		final List<Tuple3<OntProperty, Boolean, Boolean>> tupleList = new ArrayList<Tuple3<OntProperty,Boolean,Boolean>>();
    		
    		final StringBuilder attrSb = new StringBuilder();
    		final StringBuilder relSb = new StringBuilder();
    		
    		// For each field...
    		while (rsColumns.next()) {
    			final String columnName = rsColumns.getString("COLUMN_NAME");
    			final int columnTypeID = rsColumns.getInt("DATA_TYPE");
    			final String columnComment = rsColumns.getString("REMARKS");
    			
    			boolean isColumnNullable = ( rsColumns.getInt("NULLABLE") == DatabaseMetaData.columnNullable );
    			
    			final String columnTypeName = SqlTypesRepository.getSqlTypeByID(columnTypeID);
    			final String propertyUri = namespace + tableName + "_" + columnName ;
    			
    			if ( foreignKeyMap.containsKey(columnName) ) {
    				hasTableForeignKey = true;
    				
//    				System.out.println("Field: " + columnName + "\t, Type: " + columnTypeName  + "\t, isNullable: " + isColumnNullable + ", isForeignKey: TRUE, refersToTable: " + foreignKeyMap.get(columnName));
    				
    				// >> Column is a Foreign Key... Create an ObjectProperty
    				final OntProperty objectProperty = owlOntology.createObjectProperty(propertyUri, columnName, tableName, columnTypeName);
    				
    				// Object Property DOMAIN
    				objectProperty.addDomain(ontClass);
 
					// Find Foreign Key Table and Add it to the Ontology (if not exists)
					final String rangeTableName = foreignKeyMap.get(columnName);
					final String rangeClassUri = namespace + rangeTableName;
					final OntClass rangeOntClass = owlOntology.createOntClass(rangeClassUri, rangeTableName);
					
					final String msg = "\"" + rangeTableName + "\"";
					if (relSb.length() > 0) relSb.append(" , ");
					relSb.append(msg);
					
					// Object Property RANGE
					objectProperty.addRange(rangeOntClass);
					
					// Add comment(s)
					final String autoGenComment = "This property is bing used for linking an instance of class \"" + tableName + "\" with an instance of class \"" + rangeTableName + "\".";
					objectProperty.addComment(autoGenComment, "EN");
					if (columnComment != null && !columnComment.equals("")) {
						objectProperty.addComment(columnComment, "EN");
					}
					
					// >> Add Class Cardinality Restriction(s)
					if (isColumnNullable) {
						owlOntology.createMaxCardinalityRestriction(ontClass, objectProperty, 1);
					} else {
						owlOntology.createCardinalityRestriction(ontClass, objectProperty, 1);
					}
    			} else {
//    				System.out.println("Field: " + columnName + "\t, Type: " + columnTypeName + "\t, is PrimaryKey: " + pkFieldList.contains(columnName) + "\t, UniqueField: " + uniqueFieldList.contains(columnName) + "\t, isNullable: " + isColumnNullable);
    				
    				final String msg = "\"" + columnName + "\"" + ((pkFieldList.contains(columnName)) ? " (PrimaryKey)" : (uniqueFieldList.contains(columnName)) ? " (Unique)" : "");
    				if (attrSb.length() > 0) attrSb.append(" , ");
    				attrSb.append(msg);
    				
        			// >> Column is a Simple Field... Create a DatatypeProperty
    				final OntProperty datatypeProperty = owlOntology.createDatatypeProperty(propertyUri, columnName, tableName, columnTypeName);
    				
    				// DOMAIN
    				datatypeProperty.addDomain(ontClass);
    				
    				// RANGE
    				final Resource xmlType = SqlTypesRepository.getOwlTypeBySqlType(columnTypeName);
    				if (xmlType != null) {
    					datatypeProperty.addRange(xmlType);
    				}
    				
    				// Add comment(s)
    				final String asStr = (xmlType != null ) ? "as a \"" + xmlType.getLocalName() + "\"" : "";
    				final String autoGenComment = "This property is being used for capturing \"" + columnName + "\" of a \"" + tableName + "\" " + asStr + ".";
    				final StringBuilder fieldCommentsSb = new StringBuilder();
    				fieldCommentsSb.append(autoGenComment);
					if (columnComment != null && !columnComment.equals("")) {
						fieldCommentsSb.append("\n\n" + columnComment);
					}
					datatypeProperty.addComment(fieldCommentsSb.toString(), "EN");
					
					// Add Super Datatype Property
    				if (pkFieldList.contains(columnName) || uniqueFieldList.contains(columnName)) {
    					tupleList.add(Tuple3.tuple(datatypeProperty, pkFieldList.contains(columnName), uniqueFieldList.contains(columnName)));
    				} else {
    					if (xmlType != null) {
	    					if (xmlType.equals(XSD.xboolean)) {
	    						owlOntology.setIsaPropertyType(datatypeProperty, SchemaOntology.booleanValueURI);
	    					}
	    					if (xmlType.equals(XSD.xstring)) {
	    						owlOntology.setIsaPropertyType(datatypeProperty, SchemaOntology.stringValueURI);
	    					}
	    					if (xmlType.equals(XSD.integer) || xmlType.equals(XSD.xfloat) || xmlType.equals(XSD.xdouble)) {
	    						owlOntology.setIsaPropertyType(datatypeProperty, SchemaOntology.numericValueURI);
	    					}
	    					if (xmlType.equals(XSD.date)) {
	    						owlOntology.setIsaPropertyType(datatypeProperty, SchemaOntology.datetimeValueURI);
	    					}
    					} else {
    						owlOntology.setIsaPropertyType(datatypeProperty, SchemaOntology.otherValueURI);
    					}
    				}
					
    				// >> Add Class Cardinality Restriction
    				if (isColumnNullable) {
    					owlOntology.createMaxCardinalityRestriction(ontClass, datatypeProperty, 1);
					} else {
						owlOntology.createCardinalityRestriction(ontClass, datatypeProperty, 1);
					}
    			}
    			
    		} /* End Field(s) WHILE block */
    		
    		// Add Table Comment(s)
    		if ( ! hasTableForeignKey ) {
				final String autoGenComm1 = "This class is being used for capturing the available VALUES or SYMBOLS (vocabulary) being used for \"" + tableName + "\".";
				ontClass.addComment(autoGenComm1, "EN");
			} else {
				final String autoGenComm1 = "This class is being used for capturing all the important PARAMETERS about \"" + tableName + "\".";
				ontClass.addComment(autoGenComm1, "EN");
			}
    		if (attrSb.length() > 0) {
    			final String autoGenComm2 = "\n\nAny instance of this class possibly HAS the following parameters: " + attrSb;
    			ontClass.addComment(autoGenComm2, "EN");
    		}
    		if (relSb.length() > 0) {
    			final String autoGenComm3 = "\n\nAny instance of this class may be LINKED with instances from the following classes: " + relSb;
    			ontClass.addComment(autoGenComm3, "EN");
    		}
    		if (tableComment != null && !tableComment.equals("")) {
    			ontClass.addComment(tableComment, "EN");
    		}
    		
    		// Add Super Data Properties
    		for (Tuple3<OntProperty, Boolean, Boolean> tuple : tupleList) {
    			final OntProperty ontProp = tuple._1();
    			final boolean isPrimaryKey = tuple._2();
    			if ( ! hasTableForeignKey ) {
    				if (isPrimaryKey) {
    					owlOntology.setIsaPropertyType(ontProp, SchemaOntology.pkCwithoutOPURI);
    				} else {
    					owlOntology.setIsaPropertyType(ontProp, SchemaOntology.identifierCwithoutOPURI);
    				}
    			} else {
    				if (isPrimaryKey) {
    					owlOntology.setIsaPropertyType(ontProp, SchemaOntology.pkCwithOPURI);
    				} else {
    					owlOntology.setIsaPropertyType(ontProp, SchemaOntology.indetifierCwithOPURI);
    				}
    			}
			}
    		
    		// Add Super Class
    		if ( ! hasTableForeignKey ) {
				owlOntology.setIsaCode(ontClass);
			} else {
				owlOntology.setIsaData(ontClass);
			}

	    } /* End Table(s) WHILE block */
		
	    // Add Super Object Properties
	    final ExtendedIterator<ObjectProperty> objPropIt = owlOntology.getOntModel().listObjectProperties();
	    while (objPropIt.hasNext()) {
			final ObjectProperty objProp = objPropIt.next();
			final OntClass rangeSuperClass = objProp.getRange().asClass().getSuperClass();
			if (SchemaOntology.isaCode(rangeSuperClass)) {
				owlOntology.setIsaAttribute(objProp);
			}
			if (SchemaOntology.isaData(rangeSuperClass)) {
				owlOntology.setIsaRelation(objProp);
			}
		}
		// Close Connection
		connection.close();
		
		return owlOntology;
	}	
	
}
