package org.ntua.ponte.db2owl.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Throable;

public class DbJsonDataProvider {
	
	private DbJsonDataProvider() {
		
	}
	
	public static final String DB_URL = "jdbc:mysql://localhost:3306/cnr";
	public static final String DB_USERNAME = "root";
	public static final String DB_PASSWORD = "";
	
	public static void main(String[] args) throws Exception {
		System.out.println(" >> DbJsonDataProvider: BEGIN");
		
		System.out.println( getTablesJSON(DB_URL, DB_USERNAME, DB_PASSWORD).toString() );
		System.out.println( getTablesFieldsJSON(DB_URL, DB_USERNAME, DB_PASSWORD).toString() );
		
		System.out.println(" >> DbJsonDataProvider: END");
	}
	
	/*** @return <code>true</code>, if we can open a connection to DB, otherwise <code>false</code> */
	public static boolean isAvailable(final String dbUrl, final String username, final String password) {
		try {
			// Open connection to DB
			final Connection con = DbUtil.openConnection(dbUrl, username, password);
			// Close connection to DB
			con.close();
			// Database is available
			return true;
		} catch (Throwable t) {
			// Cannot open connection to DB
			return false;
		}
	}
	
	public static Tuple2<List<Tuple2<String, List<String>>>, List<Tuple2<String, List<String>>>> getTablesFields(final String dbUrl, final String username, final String password) throws Exception {
		
		final List<Tuple2<String, List<String>>> dataList = new ArrayList<Tuple2<String, List<String>>>();
		final List<Tuple2<String, List<String>>> codeList = new ArrayList<Tuple2<String, List<String>>>();
		
		// Open connection to DB
		final Connection con = DbUtil.openConnection(dbUrl, username, password);
		
		final int slash_index = dbUrl.lastIndexOf("/");
		if (slash_index <=0)
			throw new RuntimeException("Cannot find DB name from DB URL: " + dbUrl);
		final String db_name = dbUrl.substring(slash_index + 1);
		
		// Get DB TABLES 
		final DatabaseMetaData meta = con.getMetaData();
		final String catalog = db_name; // null; 
		final String schemaPattern =   null; 
		final ResultSet dbTablesResultSet = meta.getTables(catalog, schemaPattern, null, new String[] {"TABLE"});  

	    // For each table...
	    while (dbTablesResultSet.next()) {    
	    	final String tableName = dbTablesResultSet.getString("TABLE_NAME"); 
		    		
		    // Find foreign keys and put them in a map with the tables having them as primary.
		    final Map<String, String> foreignKeyMap = new HashMap<String, String>();
		    ResultSet importedKeysRS = null; try { importedKeysRS = meta.getImportedKeys(con.getCatalog(), null, tableName); } catch (Exception e) { System.out.println(Throable.throwableAsSimpleOneLineString(e)); continue ; }
		    while (importedKeysRS.next()){
		    	String fkColumnName = importedKeysRS.getString("FKCOLUMN_NAME");
		    	String pkTableName = importedKeysRS.getString("PKTABLE_NAME");
		    	foreignKeyMap.put(fkColumnName, pkTableName);
		    }
		    importedKeysRS.close();
		    		
		    // Get Table FIELDS
		    ResultSet rsColumns = meta.getColumns(null, null, tableName.toUpperCase(), null);
		    		
		    // For each field...
		    boolean hasTableForeignKey = false;
		    final List<String> fieldsList = new ArrayList<String>();
		    while (rsColumns.next()) {
		    	final String columnName = rsColumns.getString("COLUMN_NAME");
		    	fieldsList.add(columnName);
		    	if ( foreignKeyMap.containsKey(columnName) ) {
		    		hasTableForeignKey = true;
				}
		    }  /* End Fields(s) WHILE block */

		    if ( hasTableForeignKey ) {
				dataList.add(Tuple2.newTuple2(tableName, fieldsList));
			} else {
				codeList.add(Tuple2.newTuple2(tableName, fieldsList));
			}

		} /* End Table(s) WHILE block */
		
		// Close Connection
		con.close();	
				
		return Tuple2.newTuple2(dataList, codeList);
	}
	
	public static JSONObject getTablesFieldsJSON(final String dbUrl, final String username, final String password) throws Exception {	
		final Tuple2<List<Tuple2<String, List<String>>>, List<Tuple2<String, List<String>>>> tuple = getTablesFields(dbUrl, username, password);
		final List<Tuple2<String, List<String>>> dataList = tuple._1();
		final List<Tuple2<String, List<String>>> codeList = tuple._2();
		
		final JSONObject json = new JSONObject();
		final JSONObject coreJson = new JSONObject();
		final JSONArray treeDataJsonArray = new JSONArray();
		
		final JSONObject dataJson = new JSONObject();
		dataJson.put("id", "TwithPK");
		dataJson.put("text", "Tables with Foreign Key");
		dataJson.put("icon", "IMG/category.png");
		final JSONArray dataJsonArray = new JSONArray();
		for (Tuple2<String, List<String>> dataTuple : dataList) {
			final String table = dataTuple._1();
			final JSONObject tableJsonObj = new JSONObject();
			tableJsonObj.put("id", table);
			tableJsonObj.put("text", table);
			tableJsonObj.put("icon", "IMG/table.png");
			final JSONArray tableFieldsJsonArray = new JSONArray();
			for (String field : dataTuple._2()) {
				final JSONObject fieldJsonObj = new JSONObject();
				fieldJsonObj.put("id", table + "-" + field);
				fieldJsonObj.put("text", field);
				fieldJsonObj.put("icon", "IMG/field.png");
				fieldJsonObj.put("table", table);
				tableFieldsJsonArray.add(fieldJsonObj);
			}
			tableJsonObj.put("children", tableFieldsJsonArray);
			dataJsonArray.add(tableJsonObj);
		}
		dataJson.put("children", dataJsonArray);
		
		final JSONObject codeJson = new JSONObject();
		codeJson.put("id", "TwithoutPK");
		codeJson.put("text", "Tables without Foreign Key");
		codeJson.put("icon", "IMG/category.png");
		final JSONArray codeJsonArray = new JSONArray();
		for (Tuple2<String, List<String>> codeTuple : codeList) {
			final String table = codeTuple._1();
			final JSONObject tableJsonObj = new JSONObject();
			tableJsonObj.put("id", table);
			tableJsonObj.put("text", table);
			tableJsonObj.put("icon", "IMG/table.png");
			final JSONArray tableFieldsJsonArray = new JSONArray();
			for (String field : codeTuple._2()) {
				final JSONObject fieldJsonObj = new JSONObject();
				fieldJsonObj.put("id", table + "-" + field);
				fieldJsonObj.put("text", field);
				fieldJsonObj.put("icon", "IMG/field.png");
				fieldJsonObj.put("table", table);
				tableFieldsJsonArray.add(fieldJsonObj);
			}
			tableJsonObj.put("children", tableFieldsJsonArray);
			codeJsonArray.add(tableJsonObj);
		}
		codeJson.put("children", codeJsonArray);
		
		treeDataJsonArray.add(dataJson);
		treeDataJsonArray.add(codeJson);
		coreJson.put("data", treeDataJsonArray);
		json.put("core", coreJson);
		
		return json;
	}	
	
	
	public static Tuple2<List<String>, List<String>> getTables(final String dbUrl, final String username, final String password) throws Exception {
		final List<String> dataList = new ArrayList<String>();
		final List<String> codeList = new ArrayList<String>();
		
		// Open connection to DB
		final Connection con = DbUtil.openConnection(dbUrl, username, password);

		final int slash_index = dbUrl.lastIndexOf("/");
		if (slash_index <=0)
			throw new RuntimeException("Cannot find DB name from DB URL: " + dbUrl);
		final String db_name = dbUrl.substring(slash_index + 1);
		
		// Get DB TABLES 
		final DatabaseMetaData meta = con.getMetaData();
		final String catalog = db_name; // null; 
		final String schemaPattern =   null; 
		final ResultSet dbTablesResultSet = meta.getTables(catalog, schemaPattern, null, new String[] {"TABLE"});  

		// For each table...
		while (dbTablesResultSet.next()) {    
			final String tableName = dbTablesResultSet.getString("TABLE_NAME"); 
		    		
		    // Find foreign keys and put them in a map with the tables having them as primary.
		    final Map<String, String> foreignKeyMap = new HashMap<String, String>();
		    ResultSet importedKeysRS = null; try { importedKeysRS = meta.getImportedKeys(con.getCatalog(), null, tableName); } catch (Exception e) { System.out.println(Throable.throwableAsSimpleOneLineString(e)); continue ; }
		    while (importedKeysRS.next()){
		    	String fkColumnName = importedKeysRS.getString("FKCOLUMN_NAME");
		    	String pkTableName = importedKeysRS.getString("PKTABLE_NAME");
		    	foreignKeyMap.put(fkColumnName, pkTableName);
		    }
		    importedKeysRS.close();
		    		
		    // Get Table FIELDS
		    ResultSet rsColumns = meta.getColumns(null, null, tableName.toUpperCase(), null);

		    // For each field...
		    boolean hasTableForeignKey = false;
		    while (rsColumns.next()) {
		    	final String columnName = rsColumns.getString("COLUMN_NAME");
		    	if ( foreignKeyMap.containsKey(columnName) ) {
		    		hasTableForeignKey = true;
				}
		    } /* End Field(s) WHILE block */
		    
		    if ( hasTableForeignKey ) {
				dataList.add(tableName);
			} else {
				codeList.add(tableName);
			}

		} /* End Table(s) WHILE block */
		
		// Close connection to DB
		con.close();
		
		return Tuple2.newTuple2(dataList, codeList);
	}
	
	public static JSONObject getTablesJSON(final String dbUrl, final String username, final String password) throws Exception {
		final Tuple2<List<String>, List<String>> tuple = getTables(dbUrl, username, password);
		final List<String> dataList = tuple._1();
		final List<String> codeList = tuple._2();
		
		final JSONObject json = new JSONObject();
		final JSONObject coreJson = new JSONObject();
		final JSONArray treeDataJsonArray = new JSONArray();
		
		final JSONObject dataJson = new JSONObject();
		dataJson.put("id", "TwithPK");
		dataJson.put("text", "Tables with Foreign Key");
		dataJson.put("icon", "IMG/category.png");
		final JSONArray dataJsonArray = new JSONArray();
		for (String table : dataList) {
			final JSONObject tableJsonObj = new JSONObject();
			tableJsonObj.put("id", table);
			tableJsonObj.put("text", table);
			tableJsonObj.put("icon", "IMG/table.png");
			dataJsonArray.add(tableJsonObj);
		}
		dataJson.put("children", dataJsonArray);
		
		final JSONObject codeJson = new JSONObject();
		codeJson.put("id", "TwithoutPK");
		codeJson.put("text", "Tables without Foreign Key");
		codeJson.put("icon", "IMG/category.png");
		final JSONArray codeJsonArray = new JSONArray();
		for (String table : codeList) {
			final JSONObject tableJsonObj = new JSONObject();
			tableJsonObj.put("id", table);
			tableJsonObj.put("text", table);
			tableJsonObj.put("icon", "IMG/table.png");
			codeJsonArray.add(tableJsonObj);
		}
		codeJson.put("children", codeJsonArray);
		
		treeDataJsonArray.add(dataJson);
		treeDataJsonArray.add(codeJson);
		coreJson.put("data", treeDataJsonArray);
		json.put("core", coreJson);
		
		return json;
	}	

}
