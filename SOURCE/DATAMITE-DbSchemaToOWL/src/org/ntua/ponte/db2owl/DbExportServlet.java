package org.ntua.ponte.db2owl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ntua.ponte.db2owl.onto.SchemaOntology;
import org.ntua.ponte.db2owl.onto.VocabOntology;
import org.ntua.ponte.db2owl.util.DbOntologyProvider;
import org.ntua.ponte.db2owl.util.DbJsonDataProvider;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Checks;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@WebServlet("/DbExportServlet")
public class DbExportServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public DbExportServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// DB Connection Details ( URL, USERNAME, PASSWORD )
		final String dbURL = request.getParameter("dbUrl");
		final String dbUsername = request.getParameter("dbUsername");
		final String dbPassword = request.getParameter("dbPassword");
		
		// REQUEST and INPUT DATA
		final String action = request.getParameter("action");
		final String jsonStr = request.getParameter("json");
		
		System.out.println(" * " + dbURL + " - " + dbUsername + " - " + dbPassword + " :: " + action + " - " + jsonStr);
		
		final JSONObject json = JSONObject.fromObject( jsonStr );
		
		// Check
		Checks.checkNotNullArg(dbURL, "dbURL", dbUsername, "dbUsername", dbPassword, "dbPassword", action, "action", json, "json");
		
		// PROCESS and RETURN the appropriate DATA
		try {
			if (action.equals("TEST")) {
				final PrintWriter pw = response.getWriter();
				response.setContentType("application/json");
				final JSONObject respJson = new JSONObject();
				respJson.put("msg", "Test Message !");
				pw.print(respJson.toString());
				pw.close();
			}
			if (action.equals("DB-SCHEMA-ONTOLOGY")) {
				final String ontoLabel = json.getString("onto-label");
				final String ontoURI = json.getString("onto-uri");
				final SchemaOntology onto = DbOntologyProvider.exportDbSchemaOntology(dbURL, dbUsername, dbPassword, ontoLabel, ontoURI);
				response.setContentType("text/plain");
				response.setHeader("Content-disposition", "attachment; filename=Db-Schema.owl");
				final PrintWriter pw = response.getWriter();
				onto.writeToPrintWriter(pw);
				pw.close();
			}
			if (action.equals("DB-TABLES")) {
				final PrintWriter pw = response.getWriter();
				response.setContentType("application/json");
				final JSONObject respJson = DbJsonDataProvider.getTablesJSON(dbURL, dbUsername, dbPassword);
				pw.print(respJson.toString());
				pw.close();
			}
			if (action.equals("DB-TABLES-VOCAB-ONTOLOGY")) {
				final String ontoLabel = json.getString("onto-label");
				final String ontoURI = json.getString("onto-uri");
				
				final JSONArray tableJA = json.getJSONArray("tablesJA");
				final List<String> tableList = new ArrayList<String>();
				for (Object table : tableJA) {
					tableList.add("" + table);
				}
				
				final VocabOntology onto = DbOntologyProvider.exportDbTablesVocabOntology(dbURL, dbUsername, dbPassword, ontoLabel, ontoURI, tableList);
				response.setContentType("text/plain");
				response.setHeader("Content-disposition", "attachment; filename=Db-Tables-Fields-Vocab.owl");
				final PrintWriter pw = response.getWriter();
				onto.writeToPrintWriter(pw);
				pw.close();
			}
			if (action.equals("DB-TABLES-FIELDS")) {
				final PrintWriter pw = response.getWriter();
				response.setContentType("application/json");
				final JSONObject respJson = DbJsonDataProvider.getTablesFieldsJSON(dbURL, dbUsername, dbPassword);
				pw.print(respJson.toString());
				pw.close();
			}
			if (action.equals("DB-TABLES-FIELDS-VOCAB-ONTOLOGY")) {
				final String ontoLabel = json.getString("onto-label");
				final String ontoURI = json.getString("onto-uri");
				
				// TODO: get from JSON String...
				final List<Tuple2<String, List<String>>> tableFieldsExportList = new ArrayList<Tuple2<String,List<String>>>();
				
				final VocabOntology onto = DbOntologyProvider.exportDbTablesFieldsVocabOntology(dbURL, dbUsername, dbPassword, ontoLabel, ontoURI, tableFieldsExportList);
				response.setContentType("text/plain");
				response.setHeader("Content-disposition", "attachment; filename=Db-Tables-Fields-Vocab.owl");
				final PrintWriter pw = response.getWriter();
				onto.writeToPrintWriter(pw);
				pw.close();
			}
		} catch(Throwable t) {
			throw new RuntimeException("Error when processing REQUEST: " +  action + " JSON-DATA: " + json.toString(), t);
		}
		
	}

}
