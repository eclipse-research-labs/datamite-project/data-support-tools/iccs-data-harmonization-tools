package org.ntua.ponte.db2owl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.ntua.ponte.db2owl.onto.VocabOntology;
import org.ntua.ponte.db2owl.util.DbOntologyProvider;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Util;

/**
 * Servlet implementation class GenerateVocabTableFieldOntology
 */
@WebServlet("/GenerateVocabTableFieldOntology")
public class GenerateVocabTableFieldOntology extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public GenerateVocabTableFieldOntology() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String dbURL = request.getParameter("dburl");
		final String dbUsername = request.getParameter("username");
		final String dbPassword = request.getParameter("password");
		final String ontoLabel = request.getParameter("ontoname");
		final String ontoURI = request.getParameter("ontourl");
		final String jsonStr = request.getParameter("json");

		System.out.println(" * " + dbURL + " - " + dbUsername + " - " + dbPassword + " :: " + ontoLabel + " - " + ontoURI);
		
		final JSONObject json = JSONObject.fromObject( jsonStr );
		
		try {
			final JSONArray tableJA = json.getJSONArray("tablesJA");
			final List<Tuple2<String, List<String>>> tableList = new ArrayList<Tuple2<String, List<String>>>();
			for (Object obj : tableJA) {
				final JSONObject jsonObj = (JSONObject) obj;
				final String table = jsonObj.getString("table");
				final String field = jsonObj.getString("field");
				tableList.add(Tuple2.newTuple2(table, Util.newArrayList(field)));
			}
			final VocabOntology onto = DbOntologyProvider.exportDbTablesFieldsVocabOntology(dbURL, dbUsername, dbPassword, ontoLabel, ontoURI, tableList);
			response.setContentType("text/plain");
			response.setHeader("Content-disposition", "attachment; filename=Db-Tables-Fields-Vocab.owl");
			final PrintWriter pw = response.getWriter();
			onto.writeToPrintWriter(pw);
			pw.close();
		} catch(Throwable t) {
			throw new RuntimeException("Error when processing REQUEST..", t);
		}
	}

}
