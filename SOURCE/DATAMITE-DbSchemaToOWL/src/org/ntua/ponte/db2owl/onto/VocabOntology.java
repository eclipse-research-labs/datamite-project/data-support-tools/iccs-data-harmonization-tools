package org.ntua.ponte.db2owl.onto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.ontology.Ontology;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.DC;
import org.timchros.core.tuple.Tuple2;

//import com.hp.hpl.jena.ontology.AnnotationProperty;
//import com.hp.hpl.jena.ontology.OntClass;
//import com.hp.hpl.jena.ontology.OntModel;
//import com.hp.hpl.jena.ontology.OntModelSpec;
//import com.hp.hpl.jena.ontology.OntResource;
//import com.hp.hpl.jena.ontology.Ontology;
//import com.hp.hpl.jena.rdf.model.ModelFactory;
//import com.hp.hpl.jena.util.iterator.ExtendedIterator;
//import com.hp.hpl.jena.vocabulary.DC;

public class VocabOntology {

	private static final String dbTableAnnoPropURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#dbTable";
	private static final String dbFieldAnnoPropURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#dbField";
	
	private final OntModel ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
	private final OntClass vocClass;
	
	public VocabOntology(String ns) {
		final String vocabClassURI =  ns + "Vocabulary";
		vocClass = ontModel.createClass(vocabClassURI);
		vocClass.addLabel("Vocabulary", "EN");
		
		final AnnotationProperty dbTableAnnoProp = ontModel.createAnnotationProperty(dbTableAnnoPropURI);
		dbTableAnnoProp.addLabel("Table", "EN");
		dbTableAnnoProp.addComment("A table from DB", "EN");
		
		final AnnotationProperty dbFieldAnnoProp = ontModel.createAnnotationProperty(dbFieldAnnoPropURI);
		dbFieldAnnoProp.addLabel("Field", "EN");
		dbFieldAnnoProp.addComment("A field from DB", "EN");
	}
	
	public void createAnnotationProperty(final String uri, final String name) {
		final AnnotationProperty dbFieldAnnoProp = ontModel.createAnnotationProperty(uri);
		dbFieldAnnoProp.addLabel(name, "EN");
	}
	
	public void setOntoProperties(String ontoUri, String ontoLabel, String dbUrl, String creator, String msg) {
		final Ontology ontology = ontModel.createOntology(ontoUri);
		// Label
		ontology.addLabel(ontoLabel, "en");
		// Comments
		ontology.addComment(
			"The ontology provides an ontological representation of " + msg + " values with specific meaning for datasource." + "\n\n" + 
			"Data presented have been automatically extracted from relational database: \"" + dbUrl + "\".", 
			"en"
		);
		// Date
		String dateStr = "Generated Date:" + new SimpleDateFormat("dd/MM/yyyy").format(new Date());
		ontology.addProperty(DC.date, dateStr);
		// Author
		ontology.addProperty(DC.creator, creator);
	}
	
	public OntClass createRootOntClass(final String uri, final String tableName, final String fieldName) {
		OntClass ontClass = ontModel.getOntClass(uri); 
		// Check IF the class already exists
		if ( ontClass == null ) {
			ontClass = ontModel.createClass(uri);
			// Annotation
			ontClass.addProperty(ontModel.getAnnotationProperty(dbTableAnnoPropURI), tableName);
			ontClass.addProperty(ontModel.getAnnotationProperty(dbFieldAnnoPropURI), fieldName);
			// Label
			final String label = tableName.replaceAll("_", " ") + " - " + fieldName.replaceAll("_", " ");
			ontClass.addLabel(label, "EN");
			// Add SuperClass
			ontClass.addSuperClass(vocClass);
		}
		return ontClass;
	}
	
	public OntClass createRootOntClass(final String uri, final String tableName) {
		final OntClass ontClass = ontModel.createClass(uri);
		// Annotation
		ontClass.addProperty(ontModel.getAnnotationProperty(dbTableAnnoPropURI), tableName);
		// Label
		final String label = tableName.replaceAll("_", " ");
		ontClass.addLabel(label, "EN");
		// Add SuperClass
		ontClass.addSuperClass(vocClass);
		return ontClass;
	}
	
	public OntClass createOntClassUnderRootClass(final String uri, final String name, final OntClass rootOntClass) {
		final OntClass ontClass = ontModel.createClass(uri);
		// Data - Label
		final String label = name.replaceAll("_", " ");
		ontClass.addLabel(label, "EN");
		// Add SuperClass
		ontClass.addSuperClass(rootOntClass);
		return ontClass;
	}
	
	public OntClass createOntClassUnderRootClass(final String uri, final List<Tuple2<String, String>> annoList, final OntClass rootOntClass) {
		final OntClass ontClass = ontModel.createClass(uri);
		// Data - Annotation Data
		for (Tuple2<String, String> annoTuple : annoList) {
			final String annoURI = annoTuple._1();
			final String annoValue = annoTuple._2();
			ontClass.addProperty(ontModel.getAnnotationProperty(annoURI), annoValue);
		}
		// Add SuperClass
		ontClass.addSuperClass(rootOntClass);
		return ontClass;
	}
	
	public void saveToFile(String path) throws IOException {
		final FileOutputStream fos = new FileOutputStream( new File(path) );
		ontModel.write(fos, "RDF/XML-ABBREV");
		fos.flush();
		fos.close();
	}
	
	public void writeToPrintWriter(PrintWriter pw) {
		ontModel.write(pw, "RDF/XML-ABBREV");
	}
	
	public String sumMessage() {
		final int classCount = elementsCount(ontModel.listClasses());
		final int objPropCount = elementsCount(ontModel.listObjectProperties());
		final int dataPropCount = elementsCount(ontModel.listDatatypeProperties());
		final int indivCount = elementsCount(ontModel.listIndividuals());
		return "Ontology { Classes: " + classCount + " , ObjectProperties: " + objPropCount + " , DatatypeProperties: " + dataPropCount + " , Individuals: " + indivCount + " }";
	}
	
	private static <T extends OntResource> int elementsCount(ExtendedIterator<T> extIt) {
		final List<T> tmpList = new ArrayList<T>();
		while (extIt.hasNext()) {
			T ontResource = extIt.next();
			if (ontResource.getURI() == null) continue;
			tmpList.add(ontResource);
		}
		return tmpList.size();
	}
	
}
