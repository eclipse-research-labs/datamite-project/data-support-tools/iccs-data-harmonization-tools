package org.ntua.ponte.db2owl.onto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.jena.ontology.CardinalityRestriction;
import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.MaxCardinalityRestriction;
import org.apache.jena.ontology.MinCardinalityRestriction;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.ontology.Ontology;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.DC;

//import com.hp.hpl.jena.ontology.CardinalityRestriction;
//import com.hp.hpl.jena.ontology.DatatypeProperty;
//import com.hp.hpl.jena.ontology.MaxCardinalityRestriction;
//import com.hp.hpl.jena.ontology.MinCardinalityRestriction;
//import com.hp.hpl.jena.ontology.ObjectProperty;
//import com.hp.hpl.jena.ontology.OntClass;
//import com.hp.hpl.jena.ontology.OntModel;
//import com.hp.hpl.jena.ontology.OntModelSpec;
//import com.hp.hpl.jena.ontology.OntProperty;
//import com.hp.hpl.jena.ontology.OntResource;
//import com.hp.hpl.jena.ontology.Ontology;
//import com.hp.hpl.jena.rdf.model.ModelFactory;
//import com.hp.hpl.jena.rdf.model.Property;
//import com.hp.hpl.jena.util.iterator.ExtendedIterator;
//import com.hp.hpl.jena.vocabulary.DC;

/**
 * This class contains all the necessary methods in order to create an OWL ontology
 * 
 * @author Efthymios Chondrogiannis
 */
public class SchemaOntology {

	private static final String dbTableAnnoPropURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#dbTable";
	private static final String dbFieldAnnoPropURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#dbField";
	private static final String dbDataTypeAnnoPropURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#dbDataType";
	
	private final OntModel ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
	
	//private static final String PREFIX = "voc";
	
	private static final String OWL_FILE_NAME = "OntoReprUpperOnto-v.0.4.owl";
	
	private String baseUri = null;
	
	public SchemaOntology() {
		if (new File(OWL_FILE_NAME).exists()) {
			// Locally 
			try {
				final String path = "WebContent/WEB-INF/classes/" + OWL_FILE_NAME;
				ontModel.read(new FileInputStream(path), null);
			} catch(Throwable t) { throw new RuntimeException("Cannot read OWL ontology from file.", t); }
		} else {
			// Server
			ontModel.read(Thread.currentThread().getContextClassLoader().getResourceAsStream(OWL_FILE_NAME), null);
		}
	}
	
	public OntModel getOntModel() {
		return this.ontModel;
	}
	
	public void setNamespacePrefix(String baseUri) {
		//this.baseUri = baseUri;
		//ontModel.setNsPrefix(PREFIX, baseUri + "#");
		//ontModel.setNsPrefix("base", baseUri);
	}
	
	/**
	 * Specifies the main ontology properties.
	 * 
	 * @param uri
	 * 		The Ontology URI 
	 * @param dbUrl
	 * 		The Database URL based on which the ontology has been produced
	 * @param label
	 * 		The Label of the Ontology
	 * @param creator
	 * 		The person or software component being responsible for the development of the ontology
	 */
	public void addOntologyProperties(String uri, String dbUrl, String label, String creator) {
				
		Ontology ontology = 
			(ontModel.getOntology(uri) == null) 
				? ontModel.createOntology(uri) 
				: ontModel.getOntology(uri) ;
				
		// Label
		ontology.addLabel(label, "en");
		// Comments
		ontology.addComment(
			"The ontology provides an ontological representation of the relational schema of database:  \"" + dbUrl + "\"." + "\n\n" + 
			"The ontology has been automatically generated based on DB-Schema-2-OWL tool.", 
			"en"
		);
		// Date
		String dateStr = "Generated Date:" + new SimpleDateFormat("dd/MM/yyyy").format(new Date());
		ontology.addProperty(DC.date, dateStr);
		// Author
		ontology.addProperty(DC.creator, creator);
		
	}
	
	/**
	 * Creates a new {@link OntClass} with the given <code>uri</code>, 
	 * in case that there is no other {@link OntClass} with the given <code>uri</code>.
	 * 
	 * @param uri	
	 * 		The <code>uri</code> of the {@link OntClass} that we are going to add to our ontology
	 * @return
	 * 		The new one or the existing one {@link OntClass}
	 */
	public OntClass createOntClass(final String uri, final String tableName) {
		OntClass ontClass = ontModel.getOntClass(uri); 
		if ( ontClass == null ) {
			ontClass = ontModel.createClass(uri);
			// Metadata - Table
			ontClass.addProperty(ontModel.getAnnotationProperty(dbTableAnnoPropURI), tableName);
			// Data - Label
			final String label = tableName.replaceAll("_", " ");
			ontClass.addLabel(label, "EN");
			
		}
		return ontClass;
	}
	
	/**
	 * Creates a new {@link DatatypeProperty} with the given <code>uri</code>,
	 * in case that there is no other {@link DatatypeProperty} with the given <code>uri</code>.
	 * 
	 * @param uri
	 * 		The <code>uri</code> of the {@link DatatypeProperty} that we are going to add to our ontology
	 * @return
	 * 		The new one or the existing one {@link DatatypeProperty}
	 */
	public DatatypeProperty createDatatypeProperty(final String uri, final String fieldName, final String tableName, final String columnTypeName) {
		DatatypeProperty datatypeProperty = ontModel.getDatatypeProperty(uri);
		if ( datatypeProperty == null ) {
			datatypeProperty = ontModel.createDatatypeProperty(uri);
			// Metadata - Table + Field + Type
			datatypeProperty.addProperty(ontModel.getAnnotationProperty(dbTableAnnoPropURI), tableName);
			datatypeProperty.addProperty(ontModel.getAnnotationProperty(dbFieldAnnoPropURI), fieldName);
			datatypeProperty.addProperty(ontModel.getAnnotationProperty(dbDataTypeAnnoPropURI), columnTypeName);
			// Data - Label 
			final String label = tableName.replaceAll("_", " ") + " - " + fieldName.replaceAll("_", " ");
			datatypeProperty.addLabel(label, "EN");
		}
		return datatypeProperty;
	}
	
	/**
	 * Creates a new {@link ObjectProperty} with the given <code>uri</code>,
	 * in case that there is no other {@link ObjectProperty} with the given <code>uri</code>.
	 * 
	 * @param uri
	 * 		The <code>uri</code> of the {@link ObjectProperty} that we are going to add to our ontology
	 * @return
	 * 		The new one or the existing one {@link ObjectProperty}
	 */
	public ObjectProperty createObjectProperty(final String uri, final String fieldName, final String tableName, final String columnTypeName) {
		ObjectProperty objectProperty = ontModel.getObjectProperty(uri);
		if ( objectProperty == null ) {
			objectProperty = ontModel.createObjectProperty(uri);
			// Metadata - Table + Field
			objectProperty.addProperty(ontModel.getAnnotationProperty(dbTableAnnoPropURI), tableName);
			objectProperty.addProperty(ontModel.getAnnotationProperty(dbFieldAnnoPropURI), fieldName);
			objectProperty.addProperty(ontModel.getAnnotationProperty(dbDataTypeAnnoPropURI), columnTypeName);
			// Data - Label 
			final String label = tableName.replaceAll("_", " ") + " - " + fieldName.replaceAll("_", " ");
			objectProperty.addLabel(label, "EN");
		}
		return objectProperty;
	}
	
	/**
	 * Creates an <b>Exact</b> CardinalityRestriction about the property given
	 * 
	 * @param property
	 * 		The property given
	 * @param exact
	 * 		The exact number of occurrences
	 * @return
	 * 		The {@link CardinalityRestriction} about the property given
	 */
	public void createCardinalityRestriction(OntClass ontClass, OntProperty ontProperty, int exact) {
		ontClass.addSuperClass(
			ontModel.createCardinalityRestriction(null, ontProperty, exact)
		);
	} 
	
	/**
	 * Creates a <b>Max</b> CardinalityRestriction about the property given
	 * 
	 * @param property
	 * 		The property given
	 * @param max
	 * 		The maximum number of occurrences
	 * @return
	 * 		The {@link MaxCardinalityRestriction} about the property provided
	 */
	public void createMaxCardinalityRestriction(OntClass ontClass, Property ontProperty, int max) {
		ontClass.addSuperClass(
			ontModel.createMaxCardinalityRestriction(null, ontProperty, max)
		);
	}
	
	/**
	 * Creates a <b>Min</b> CardinalityRestriction about the property given
	 * 
	 * @param property
	 * 		The property given
	 * @param min
	 * 		The minimum number of occurrences
	 * @return
	 * 		The {@link MinCardinalityRestriction} about the property provided
	 */
	public void createMinCardinalityRestriction(OntClass ontClass, Property ontProperty, int min) {
		ontClass.addSuperClass(
			ontModel.createMinCardinalityRestriction(null, ontProperty, min)
		);
	}	
	
	/**
	 * Specifies that owl class <code>ontClass2</code> is <b>superclass</b> of owl class <code>ontClass1</code>
	 * 
	 * @param ontClass1
	 * 		Sub-Class
	 * @param ontClass2
	 * 		Super-Class
	 */
	public void addSuperClass(OntClass ontClass1, OntClass ontClass2) {
		ontClass1.addSuperClass(ontClass2);
	}
	
	/**
	 * Saves the ontology in the file/path provided
	 * 
	 * @param path
	 * 		The PATH to the Ontology File
	 * @throws IOException
	 * 		In case an error occurs
	 */
	public void saveToFile(String path) throws IOException {
		FileOutputStream fos = new FileOutputStream( new File(path) );
		if (baseUri == null) {
			ontModel.write(fos, "RDF/XML-ABBREV");
		} else {
			ontModel.write(fos, "RDF/XML-ABBREV", baseUri);
		}
		fos.flush();
		fos.close();
	}
	
	public void writeToPrintWriter(PrintWriter pw) {
		if (baseUri == null) {
			ontModel.write(pw, "RDF/XML-ABBREV");
		} else {
			ontModel.write(pw, "RDF/XML-ABBREV", baseUri);
		}
	}
	
	public void print() {
		ontModel.write(System.out);
	}
	
	private static final String codeURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#C-without-OP";
	
	public void setIsaCode(OntClass ontClass) {
		final OntClass codeOntClass = ontModel.getOntClass(codeURI); 
		if ( codeOntClass != null ) {
			// ontClass.setSuperClass(codeOntClass);
			ontClass.addSuperClass(codeOntClass);
		}
	}
	
	public static boolean isaCode(OntResource ontRes) {
		if (ontRes == null) return false;
		return (ontRes.hasURI(codeURI)) ;
	}
	
	private static final String dataURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#C-with-OP";
	
	public void setIsaData(OntClass ontClass) {
		final OntClass dataOntClass = ontModel.getOntClass(dataURI); 
		if ( dataOntClass != null ) {
			// ontClass.setSuperClass(dataOntClass);
			ontClass.addSuperClass(dataOntClass);
		}
	}
	
	public static boolean isaData(OntResource ontRes) {
		if (ontRes == null) return false;
		return (ontRes.hasURI(dataURI));
	}
	
	private static final String attrURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#attribute";
	
	public void setIsaAttribute(OntProperty objProp) {
		final ObjectProperty attrObjProp = ontModel.getObjectProperty(attrURI);
		if ( attrObjProp != null ) {
			objProp.setSuperProperty(attrObjProp);
		}
	}
	
	private static final String relationURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#relation";
	
	public void setIsaRelation(OntProperty objProp) {
		final ObjectProperty relObjProp = ontModel.getObjectProperty(relationURI);
		if ( relObjProp != null ) {
			objProp.setSuperProperty(relObjProp);
		}
	}

	public static final String booleanValueURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#booleanValue";
	public static final String charValueURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#charValue";
	public static final String stringValueURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#stringValue";
	public static final String numericValueURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#numericValue";
	public static final String datetimeValueURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#datetimeValue";
	public static final String otherValueURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#otherValue";
	public static final String identifierCwithoutOPURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#identifier-C-without-OP";
	public static final String pkCwithoutOPURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#pk-C-without-OP";
	public static final String indetifierCwithOPURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#identifier-C-with-OP";
	public static final String pkCwithOPURI = "http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#pk-C-with-OP";
	
	public void setIsaPropertyType(OntProperty dataProp, final String uri) {
		final DatatypeProperty dataObjProp = ontModel.getDatatypeProperty(uri);
		if ( dataObjProp != null ) {
			dataProp.setSuperProperty(dataObjProp);
		}
	}
	
	//http://www.semanticweb.org/ontologies/ntua/iccs/dbschema#codeIdentifier
	
	
	public String sumMessage() {
		final int classCount = elementsCount(ontModel.listClasses());
		final int objPropCount = elementsCount(ontModel.listObjectProperties());
		final int dataPropCount = elementsCount(ontModel.listDatatypeProperties());
		final int indivCount = elementsCount(ontModel.listIndividuals());
		return "Ontology { Classes: " + classCount + " , ObjectProperties: " + objPropCount + " , DatatypeProperties: " + dataPropCount + " , Individuals: " + indivCount + " }";
	}

	private static <T extends OntResource> int elementsCount(ExtendedIterator<T> extIt) {
		final List<T> tmpList = new ArrayList<T>();
		while (extIt.hasNext()) {
			T ontResource = extIt.next();
			if (ontResource.getURI() == null) continue;
			tmpList.add(ontResource);
		}
		return tmpList.size();
	}
	
}
