## Database Schema to OWL ontology Web Application

### Step 1: Generate Web Application WAR File 

 * Prerequisites: 

	**JAVA 17** and **Apache Ant** should ve installed in your machine


### Step 2: Deploy and Run Web Application WAR File 

#### (a) Deploy & Run Locally

 * Access through the Web App GUI
	
	Place the WAR file in the Tomcat webapps folder
	
	Start Tomcat
	
	Access Web Application: 
	
	```
	http://localhost:8080/DbToOWL/
	```
	
#### (b) Containirize and Run 

 * Build the Docker Image using the following command: 
 
	```
	docker build -t db-schema-to-owl-image .
	```
	
 * Verifying the Docker Image
 
	```
	docker images
	```
	
 * Run the Docker Container 
 
	```
	docker run -i -t -d -p 8080:8080 db-schema-to-owl-image
	```
	
 * Access Web App
 
	```
	http://localhost:8080/DbToOWL/
	```

 * FIX PROBLEM WHEN ACCESSING AN "EXTERNAL" MYSQL Database
 
   - Create the network by using this command.
		docker network create my-network
   - Start a MySQL container and attach it the network.
		docker run -d --network my-network --network-alias mysql -v todo-mysql-data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=p -e MYSQL_DATABASE=todos mysql:5.7
   - Get your container ID by using the docker command ps command.
		docker ps -a
   - Confirm you have the database up and running, connect to the database
		docker exec -it <mysql-container-id> mysql -p
		e.g., docker exec -it a44df4a4d4dc mysql -p
   - In the MySQL shell, list the databases and verify you see the "todos" database.
		SHOW DATABASES;
   
   ...
   
   
 * The you can get the generated file(s) using commands:
 
	```
	docker ps -a
	```
	
	and
	
	```
	docker cp <containerId>:/file/path/within/container /host/path/target
	```
