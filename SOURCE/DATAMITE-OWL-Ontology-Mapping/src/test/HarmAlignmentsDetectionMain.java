package test;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.util.iterator.ExtendedIterator;

public class HarmAlignmentsDetectionMain {

	private static final String cohortOntoPath = "./ONTO/25-UU-Cohort-Metadata-v.0.2.owl";
	private static final String harmOntoPath = "./ONTO/HarmonicSS-Reference-Model+Vocabularies-v.0.9.owl";

	private static final String conduri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#COND-";
	private static final String sympuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#SYMPT-";
	private static final String druguri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#CHEM-";
	
	private static final String questuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#QUEST-";
	private static final String essdaiuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#ESSDAI-";
	private static final String caciuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#CACI-";
	
	private static final String blooduri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#BLOOD-";
	private static final String urineuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#URINE-";
	private static final String oraluri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#ORAL-";
	private static final String oculuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#OCULAR-";
	private static final String imguri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#IMG-";
	private static final String salgluri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#SAL-";
	private static final String biopsuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#BIOPSY-";
	
	private static final String sexuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#SEX-";
	private static final String ethnuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#ETHN-";
	private static final String educuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#EDU-";
	
	private static final String assessuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#ASSESS-";
	private static final String confirmuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#CONFIRM-";
	
	//final String condstr = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#BMI-";
	//final String condstr = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#IPI-RISK-";
	//final String condstr = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#PERFORM-";
	//final String condstr = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#ANA-PAT-";
	//final String condstr = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#CRYO-";
	//final String condstr = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#ACTLVL-";
	
	public static void main(String[] args) throws Exception {

		System.out.println(" >> HarmAlignmentsDetectionMain: Process STARTED at " + new Date());
		System.out.println();
		
		if (!new File(cohortOntoPath).exists()) return;
		if (!new File(harmOntoPath).exists()) return;
		
		/* ***************** A. Load Ontologies ***************** */
		
		System.out.println("Loading Cohort Ontology: ... " + cohortOntoPath);
		final OntModel cohortOntModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		cohortOntModel.read(new FileInputStream(new File(cohortOntoPath)), null);
		System.out.println("Loading Cohort Ontology: ... Successfully Completed !");
		
		System.out.println("Loading HarmonicSS Ontology: ... " + harmOntoPath);
		final OntModel harmOntModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		harmOntModel.read(new FileInputStream(new File(harmOntoPath)), null);
		System.out.println("Loading HarmonicSS Ontology: ... Successfully Completed !");
		
		/* ***************** B.1. Find Cohort Terms ***************** */
		
		final List<DatatypeProperty> cohortFieldList = new ArrayList<DatatypeProperty>();
		final ExtendedIterator<DatatypeProperty> cohortPropIt = cohortOntModel.listDatatypeProperties();
		while (cohortPropIt.hasNext()) {
			final DatatypeProperty prop =cohortPropIt.next();
			final String uri = prop.getURI();
			if (uri == null) continue;
			final String local = prop.getLocalName();
			if (local == null) continue;
			
			// TODO: EXCLUDE DATES
			final String comment = prop.getComment(null);
			if (comment.indexOf("Data Type: DATE") > 0) {
				//System.out.println(" ** IGNORE: " + prop.getLabel(null));
				continue;
			}
			
			cohortFieldList.add(prop);
		}
		Collections.sort(cohortFieldList, new Comparator<DatatypeProperty>() {
			@Override public int compare(DatatypeProperty p1, DatatypeProperty p2) {
				return p1.getLocalName().compareTo(p2.getLocalName());
			}
		});
		
		// For debugging ... Cohort Fields
		System.out.println("FIELDS:");
		int fieldIndex = 0;
		for (DatatypeProperty prop : cohortFieldList) {
			fieldIndex++;
			System.out.println(fieldIndex + " : " + prop.getLabel(null));
		}
		
		final Map<OntClass, List<OntClass>> cohortSetMap = new HashMap<OntClass, List<OntClass>>();
		final ExtendedIterator<OntClass> cohortClsIt = cohortOntModel.listClasses();
		while (cohortClsIt.hasNext()) {
			final OntClass ontClass =cohortClsIt.next();
			final String uri = ontClass.getURI();
			if (uri == null) continue;
			final String local = ontClass.getLocalName();
			if (local == null) continue;
			if (!local.startsWith("Domain") || !local.contains("Term")) continue;
			
			final OntClass superOntClass = ontClass.getSuperClass();
			if (!cohortSetMap.containsKey(superOntClass)) { 
				cohortSetMap.put(superOntClass, new ArrayList<OntClass>());
			}
			cohortSetMap.get(superOntClass).add(ontClass);
		}
		for (Entry<OntClass, List<OntClass>> entry : cohortSetMap.entrySet()) {
			Collections.sort(entry.getValue(), new Comparator<OntClass>() {
				@Override public int compare(OntClass o1, OntClass o2) {
					return o1.getLocalName().compareTo(o2.getLocalName());
				}
			});
		}
		
		// For debugging ... Cohort Controlled set(s) of Terms
		System.out.println("VALUES:");
		int setIndex = 0;
		for (Entry<OntClass, List<OntClass>> entry : cohortSetMap.entrySet()) {
			setIndex++;
			System.out.println("Set-" + setIndex);
			for (OntClass ontClass : entry.getValue()) {
				System.out.println(" - " + ontClass.getLabel(null));
			}
		}
		
		// For debugging ... TOTAL Cohort Terms
		int cohortTermsCount = cohortFieldList.size();
		for (Entry<OntClass, List<OntClass>> entry : cohortSetMap.entrySet()) {
			cohortTermsCount += entry.getValue().size();
		}
		System.out.println("\n ** cohortTermsCount: " + cohortTermsCount);
		
		
		/* ***************** B.2. Find HarmonicSS Terms ***************** */
		
		final Map<String, List<OntClass>> harmSetMap = new HashMap<String, List<OntClass>>();

		harmSetMap.put(sexuri, new ArrayList<OntClass>());
		harmSetMap.put(ethnuri, new ArrayList<OntClass>());
		harmSetMap.put(educuri, new ArrayList<OntClass>());
		harmSetMap.put(blooduri, new ArrayList<OntClass>());
		harmSetMap.put(urineuri, new ArrayList<OntClass>());
		harmSetMap.put(oraluri, new ArrayList<OntClass>());
		harmSetMap.put(oculuri, new ArrayList<OntClass>());
		harmSetMap.put(imguri, new ArrayList<OntClass>());
		harmSetMap.put(salgluri, new ArrayList<OntClass>());
		harmSetMap.put(biopsuri, new ArrayList<OntClass>());
		harmSetMap.put(questuri, new ArrayList<OntClass>());
		harmSetMap.put(essdaiuri, new ArrayList<OntClass>());
		harmSetMap.put(caciuri, new ArrayList<OntClass>());
		harmSetMap.put(conduri, new ArrayList<OntClass>());
		harmSetMap.put(sympuri, new ArrayList<OntClass>());
		harmSetMap.put(druguri, new ArrayList<OntClass>());
		harmSetMap.put(assessuri, new ArrayList<OntClass>());
		harmSetMap.put(confirmuri, new ArrayList<OntClass>());
		
		final ExtendedIterator<OntClass> harmClsIt = harmOntModel.listClasses();
		while (harmClsIt.hasNext()) {
			final OntClass ontClass =harmClsIt.next();
			final String uri = ontClass.getURI();
			
			if (uri == null) continue;
			if (uri.contains("reference-model")) continue;
			
			for (Entry<String, List<OntClass>> entry : harmSetMap.entrySet()) {
				if (uri.startsWith(entry.getKey())) {
					entry.getValue().add(ontClass);
					break;
				}
			}
		}
		for (Entry<String, List<OntClass>> entry : harmSetMap.entrySet()) {
			Collections.sort(entry.getValue(), new Comparator<OntClass>() {
				@Override public int compare(OntClass o1, OntClass o2) {
					return o1.getLocalName().compareTo(o2.getLocalName());
				}
			});
		}
		
		// For debugging ... TOTAL HarmonicSS Terms
		int harmTermsCount = 0;
		for (Entry<String, List<OntClass>> entry : harmSetMap.entrySet()) {
			harmTermsCount += entry.getValue().size();
		}
		System.out.println("\n ** harmTermsCount: " + harmTermsCount);
		
		/* ***************** C.1. Fields Alignment ***************** */
		
		System.out.println("\nFields (" + cohortFieldList.size() + ") ... alignment:");
		System.out.println("-------------------------------------------------------");
		for (DatatypeProperty prop : cohortFieldList) {
			final String cohortField = prop.getLabel(null);
			
			OntClass harmOntClass = searchForTerm(harmOntModel, harmSetMap, true, cohortField);
			if (harmOntClass != null) {
				System.out.println(cohortField + " --> " + ontClassToString(harmOntModel, harmOntClass));
			}
		}
		
		/* ***************** C.2. Values Alignment ***************** */
		
		System.out.println("\nValues... alignment:");
		System.out.println("-------------------------------------------------------");
		for (Entry<OntClass, List<OntClass>> entry : cohortSetMap.entrySet()) {
			//System.out.println("For: " + entry.getKey() + " (" + entry.getValue().size() + ")");
			for (OntClass ontClass : entry.getValue()) {
				final String cohortTerm = ontClass.getLabel(null);
				OntClass harmOntClass = searchForTerm(harmOntModel, harmSetMap, false, cohortTerm);
				if (harmOntClass != null) {
					System.out.println(cohortTerm + " --> " + ontClassToString(harmOntModel, harmOntClass));
				}
			}
		}
		System.out.println();
		System.out.println(" >> HarmAlignmentsDetectionMain: Process FINISHED at " + new Date());
	}

	
	public static String ontClassToString(OntModel harmOntModel, OntClass ontClass) {
		if (ontClass == null) return null;
		
		final RDFNode acronymNode = ontClass.getPropertyValue(harmOntModel.getAnnotationProperty("http://www.semanticweb.org/ntua/iccs/harmonicss/terminology#acronym"));
		final String acronym = (acronymNode != null) ? acronymNode.asLiteral().getString() : null ;
		
		final RDFNode akaNode = ontClass.getPropertyValue(harmOntModel.getAnnotationProperty("http://www.semanticweb.org/ntua/iccs/harmonicss/terminology#aka"));
		final String aka = (akaNode != null) ? akaNode.asLiteral().getString() : null ;
		
		// Based on Codes Format for detecting the Domain the term belongs to
		OntClass superOntClass = null;
		final int dashIndex = ontClass.getLocalName().indexOf('-');
		if (dashIndex > 0) {
			final String prefix = ontClass.getLocalName().substring(0, dashIndex+1);
			superOntClass = ontClass.getSuperClass();
			while (superOntClass != null && superOntClass.getLocalName().startsWith(prefix)) {
				superOntClass = superOntClass.getSuperClass();
			}
		}
		
		return ontClass.getLabel(null) 
				+ ((acronym != null) ? " (" + acronym + ")" : "") 
				+ ((aka != null) ? " AKA " + aka : "")
				+ ((superOntClass != null) ? " --ISA--> " + superOntClass.getLabel(null) : "");
	}
	
	public static OntClass searchForTerm(final OntModel harmOntModel, final Map<String, List<OntClass>> harmSetMap, final boolean isField, final String cohortTerm) {
		
		for (Entry<String, List<OntClass>> entry : harmSetMap.entrySet()) {
			for (OntClass ontClass : entry.getValue()) {
				final String harmTerm = ontClass.getLabel(null);
				
				// Label
				if (compareStrings(cohortTerm, harmTerm)) return ontClass;
				
				// Abbreviation
				final RDFNode acronymNode = ontClass.getPropertyValue(harmOntModel.getAnnotationProperty("http://www.semanticweb.org/ntua/iccs/harmonicss/terminology#acronym"));
				final String acronym = (acronymNode != null) ? acronymNode.asLiteral().getString() : null ;
				if (acronym != null) {
					if (compareStrings(cohortTerm,acronym)) return ontClass;
				}
				
				// Synonyms
				final RDFNode akaNode = ontClass.getPropertyValue(harmOntModel.getAnnotationProperty("http://www.semanticweb.org/ntua/iccs/harmonicss/terminology#aka"));
				final String aka = (akaNode != null) ? akaNode.asLiteral().getString() : null ;
				if (aka != null) {
					final String[] synonyms = aka.split(",");
					for (String synonym : synonyms) {
						if (compareStrings(cohortTerm, synonym.trim())) return ontClass;
					}
				}
				
			} // End of TERMS loop
			
		} // End of VOCABULARIES loop
		
		return null;
	}
	
	public static boolean compareStrings(final String str1, final String str2) {
		// TODO: detect if abbreviation
		// e.g., when less than 3 chars, or most of them if not all are capitals
		
		// TODO:
		// Ignore stop words and use stems
		
		return str1.equalsIgnoreCase(str2);
	}
	
}
