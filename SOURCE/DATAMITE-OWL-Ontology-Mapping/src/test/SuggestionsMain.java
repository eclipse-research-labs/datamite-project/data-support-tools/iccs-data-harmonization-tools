package test;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.ntua.web.oat.OntoFileUploadServlet;
import org.ntua.web.oat.SuggestionsServlet;
import org.timchros.core.util.Util;

import net.sf.json.JSONObject;

public class SuggestionsMain {

	private static final String harmPath = 
		"WebContent/WEB-INF/classes/" + OntoFileUploadServlet.HARMONICSS_REFERENCE_MODEL;
	
	private static final String HARM_VOCAB_NS = 
		"http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#";
	
	/** Map with the Prefix (i.e., Local Name Prefix) of Terms from each Domain */
	private static final Map<String, String> harmTermPrefixMap = Util.newHashMap(
			// About Demographic Characteristics
			"Sex", "SEX-",
			"Ethnicity", "ETHN-",
			"Education-Level", "EDU-",
			"Body-Mass-Index", "BMI-",
			// About Smoking Status and Pregnancies
			"Tobacco-Consumption-Status", "SMOK-",
			"Pregnancy-Outcome", "PREG-",
			// About Medical Conditions and Symptoms/Signs
			"Medical-Condition", "COND-",
			"Lymphoma-Organ", "ORGAN",
			"Lymphoma-Performance-Status", "PERFORM-",
			"Symptom-Sign", "SYMPT-",
			// About Drugs
			"Drug-Substance", "CHEM-",
			// About Questionnaires
			"Questionnaire-Score", "QUEST-",
			"ESSDAI-Domain", "ESSDAI-",
			"CACI-Condition", "CACI-",
			// About Questionnaires Outcome
			"Activity-Level", "ACTLVL-",
			"Risk-Non-Hodgkin-Lymphoma", "IPI-RISK-",
			// About Medical Tests
			"Blood-Test", "BLOOD-",
			"Urine-Test", "URINE-",
			"Oral-Test", "ORAL-",
			"Ocular-Test", "OCULAR-",
			"Medical-Imaging-Test", "IMG-",
			"Salivary-Gland-Biopsy", "SAL-",
			"Biopsy-Test", "BIOPSY-",
			// About Medical Tests Outcome
			"Antinuclear-Antibody-Pattern", "ANA-PAT-",
			"Cryoglubin-Type", "CRYO-",
			// Other Terms
			"Specialty-Department", "SPEC-",
			"Assessment", "ASSESS-",
			"Confirmation", "CONFIRM-"
		);
	
	static {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.WARN);
	}
	
	private static final String cohortOntFilePath = "ONTO/Test-UU-2018-10-02-Cohort-Metadata-FINAL-v.0.3.3.xlsx.owl";
	
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		
		System.out.println("Loading ...");
		
		final OntModel harmOntModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		harmOntModel.read(new FileInputStream(new File(harmPath)), null);

		final OntModel cohortOntModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		cohortOntModel.read(new FileInputStream(new File(cohortOntFilePath)), null);
		
		final JSONObject json = SuggestionsServlet.getSimilarityResponse(cohortOntModel, harmOntModel, new JSONObject(), new JSONObject());
		System.out.println(json.toString(5,5));
		
		
		System.out.println("End");
		
		if (true) return;
		
		System.out.println(" ** HarmonicSS ** ");
		
		// Map with the List of Terms that belong to a specific Domain
		final Map<OntClass, List<OntClass>> harmLocTermsMap = new HashMap<OntClass, List<OntClass>>();
		for (Entry<String, String> entry : harmTermPrefixMap.entrySet()) {
			final OntClass categOntClass = harmOntModel.getOntClass(HARM_VOCAB_NS + entry.getKey());
			if (categOntClass == null) throw new RuntimeException("Cannot find OntClass for \"" + entry.getValue() + "\".");
			harmLocTermsMap.put(categOntClass, new ArrayList<OntClass>());
		}
		
		// Find the HarmonicSS terms and place them in the appropriate category
		final ExtendedIterator<OntClass> harmClsIt = harmOntModel.listClasses();
		while (harmClsIt.hasNext()) {
			final OntClass ontClass =harmClsIt.next();
			final String ns = ontClass.getNameSpace();
			final String localname = ontClass.getLocalName();
			if (ns == null || localname  == null) continue;
			// Focus on Vocabularies
			if (!ns.equals(HARM_VOCAB_NS)) continue;
			// Ignore some terms
			if (localname.startsWith("CACI-C")) continue;
			if (localname.startsWith("SYMPT-CAT-")) continue;
			// Place terms in the Appropriate Category (if any) based on the Prefix of the Local Name			
			for (Entry<OntClass, List<OntClass>> entry : harmLocTermsMap.entrySet()) {
				final String prefix =  harmTermPrefixMap.get(entry.getKey().getLocalName());
				if (localname.startsWith(prefix)) {
					entry.getValue().add(ontClass);
					break;
				}
			}
		} // END OF HarmonicSS OntClass LOOP
		
		// Sort terms recorded for each category based on their name
		for (Entry<OntClass, List<OntClass>> entry : harmLocTermsMap.entrySet()) {
			System.out.println(entry.getKey().getLocalName() + " --> " +  entry.getValue().size());
			Collections.sort(entry.getValue(), new Comparator<OntClass>() {
				@Override public int compare(OntClass o1, OntClass o2) {
					return o1.getLocalName().compareTo(o2.getLocalName());
				}
			});
		}
		
		System.out.println(" ** Cohort ** ");
		
		
		
		// Map with the List of Terms (i.e., Local Name) starting with the specific Prefix
		final Map<OntClass, List<OntClass>> cohortTermsMap = new HashMap<OntClass, List<OntClass>>();
		
		// Find the Cohort terms and place them in the appropriate category
		final ExtendedIterator<OntClass> cohortClsIt = cohortOntModel.listClasses();
		while (cohortClsIt.hasNext()) {
			final OntClass ontClass =cohortClsIt.next();
			final String localname = ontClass.getLocalName();
			if (localname  == null) continue;
			// Focus on Vocabularies
			if (!localname.contains("Domain") || !localname.contains("Term")) continue;
			// Place terms in the Appropriate Category (if any) based on the Prefix of the Local Name
			final OntClass categOntClass = ontClass.getSuperClass();
			if (!cohortTermsMap.containsKey(categOntClass)) cohortTermsMap.put(categOntClass, new ArrayList<>());
			cohortTermsMap.get(categOntClass).add(ontClass);
		} // END OF HarmonicSS OntClass LOOP		
		
		// Sort terms recorded for each category based on their name
		for (Entry<OntClass, List<OntClass>> entry : cohortTermsMap.entrySet()) {
			System.out.println(entry.getKey().getLocalName() + " --> " +  entry.getValue().size());
			Collections.sort(entry.getValue(), new Comparator<OntClass>() {
				@Override public int compare(OntClass o1, OntClass o2) {
					return o1.getLocalName().compareTo(o2.getLocalName());
				}
			});
		}
		
		System.out.println(">> SuggestionsMain: Process FINISHED at " + new Date());
		
	}

}
