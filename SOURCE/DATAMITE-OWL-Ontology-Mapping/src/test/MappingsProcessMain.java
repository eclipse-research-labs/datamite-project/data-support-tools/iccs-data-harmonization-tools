package test;

import static org.ntua.web.oat.SuggestionsServlet.checkTermMapped;
import static org.ntua.web.oat.SuggestionsServlet.checkFieldMapped;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import net.sf.json.JSONObject;

public class MappingsProcessMain {

	private static final String path = "C:/Users/Efthymios/Downloads/QMUL-2019-04-11-Mapping-Rules-v.2.json";
	
	public static void main(String[] args) throws Exception {

		System.out.println(" >> MappingsProcessMain: BEGIN");
		
		final Properties prop = new Properties();
		prop.load(new FileInputStream(new File(path)));
		
		// Read Mappings
		final InputStream is =  new FileInputStream(path);
		final String text = IOUtils.toString(is, StandardCharsets.UTF_8.name());
		is.close();
		
		final JSONObject json = JSONObject.fromObject(text);
		System.out.println(json.toString(1,1));
		
		// JSON object with "jsonarray"
		final JSONObject cor = json.getJSONObject("correspondences");
		
		System.out.println(checkFieldMapped(cor, "http://www.semanticweb.org/ntua/iccs/harmonicss/cohort#Parameter-002"));
		System.out.println(checkFieldMapped(cor, "http://www.semanticweb.org/ntua/iccs/harmonicss/cohort#Parameter-999"));
		System.out.println();
		System.out.println(checkTermMapped(cor, "http://www.semanticweb.org/ntua/iccs/harmonicss/cohort#Domain-002-Term-001"));
		System.out.println(checkTermMapped(cor, "http://www.semanticweb.org/ntua/iccs/harmonicss/cohort#Domain-002-Term-999"));
		
		System.out.println(" >> MappingsProcessMain: END");
		
	}
	


}
