package test;

import static org.ntua.web.oat.ServletUtil.toList;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.Restriction;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.ntua.web.oat.OntResourceUtil;
import org.ntua.web.oat.OntoFileUploadServlet;
import org.timchros.core.transformers.I_StringTransformer;
import org.timchros.core.util.Util;

public class OntoTestringMain {
	
	private static final String harmPath = 
			"TEST-DATA/" + "Target-Reference-Model+Vocabularies-v.0.1.owl"; // + OntoFileUploadServlet.HARMONICSS_REFERENCE_MODEL;
			// "WebContent/WEB-INF/classes/" + OntoFileUploadServlet.HARMONICSS_REFERENCE_MODEL;
	
	static {
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.WARN);
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {

		System.out.println(" >> OntoTestringMain: Process STARTED at " + new Date());
		System.out.println();
		
//		final String ontoFilePath = "C:/Users/Efthymis/Desktop/NEW-LATEST/HarmonicSS-Vocabularies-v.0.3.owl";
//		if (!new File(ontoFilePath).exists()) return;
		
		System.out.println("Loading Ontology: ... " + harmPath);
		final OntModel ontModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		ontModel.read(new FileInputStream(new File(harmPath)), null);
		System.out.println("Loading Ontology: ... Successfully Completed !");
		
		final String uri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Medical-Condition";
		final String other = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#COND-071000";
		
		final boolean bol = termIsA(ontModel.getOntClass(other), ontModel.getOntClass(uri));
		System.out.println(bol);
		
		if (true) return;
		
		System.out.println(new ArrayList<>(getSubClassesFor(ontModel.getOntClass(uri))).size());
		
		if (true) return;
		
		final String clsUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#Blood-Test";
		final OntClass cls = ontModel.getOntClass(clsUri);
		
		final List<OntClass> theSuperOntClassList = new ArrayList<>(getSuperOntClassSet(cls));
		Collections.sort(theSuperOntClassList, new Comparator<OntClass>() {
			@Override public int compare(OntClass o1, OntClass o2) {
				if (o1.getURI()!= null) return -1;
				if (o2.getURI()!= null) return +1;
				if (o1.isRestriction() && o2.isRestriction()) {
					Restriction r1 = o1.asRestriction();
					Restriction r2 = o2.asRestriction();
					if (r1.getOnProperty().getLocalName().equals("data-Belong-to-Visit")) return +1;
					if (r2.getOnProperty().getLocalName().equals("data-Belong-to-Visit")) return -1;
					return r1.getOnProperty().getLocalName().compareTo( r2.getOnProperty().getLocalName() );
					//return getLabelOrLocalName( r1.getOnProperty() ).compareTo( getLabelOrLocalName( r2.getOnProperty() ) );
				}
				return 0;
			}
		});
		System.out.println(Util.listToMultiLineString(theSuperOntClassList, new I_StringTransformer<OntClass>() {
			@Override public String asString(OntClass oc) {
				return OntResourceUtil.getOntClassAsHtml(ontModel, oc);
			}
		}));

		
		final String dpuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#code-Display-Name"; //"http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#boolean-Value";
		//final String dpuri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/reference-model#date-Event";
		final DatatypeProperty dp = ontModel.getDatatypeProperty(dpuri);
		//final OntResource range = dp.getRange();
		
		System.out.println( OntResourceUtil.getOntClassAsHtml( ontModel, dp.getDomain().as(OntClass.class) ) );
		System.out.println( OntResourceUtil.getOntClassAsHtml( ontModel, dp.getRange().as(OntClass.class) ) );
//		System.out.println(range.getClass().getSimpleName() + " : " + range.isClass());
//		OntClass rangeClass = range.asClass();
//		if (range.getLocalName() != null) {
//			System.out.println("*****");
//		} else {
//			if (rangeClass.isUnionClass()) {
//				UnionClass unionClass = rangeClass.asUnionClass();
////				unionClass.get
//			}
//			//OntClass rangeClass = range.asClass();
//			
//		}

		if (true) return;
		
		final List<OntClass> superOntClassList = toList(cls.listSuperClasses());
		Collections.sort(superOntClassList, new Comparator<OntClass>() {
			@Override public int compare(OntClass o1, OntClass o2) {
				if (o1.getURI()!= null) return -1;
				if (o2.getURI()!= null) return +1;
				if (o1.isRestriction() && o2.isRestriction()) {
					Restriction r1 = o1.asRestriction();
					Restriction r2 = o2.asRestriction();
					//return getLabelOrLocalName( r1.getOnProperty() ).compareTo( getLabelOrLocalName( r2.getOnProperty() ) );
					return r1.getOnProperty().getLocalName().compareTo( r2.getOnProperty().getLocalName() );
				}
				return 0;
			}
		});
		final List<String> superOntClassDescrList = new ArrayList<String>();
		for (OntClass superOntClass : superOntClassList) {
			superOntClassDescrList.add(OntResourceUtil.getOntClassAsHtml(ontModel, superOntClass));
		}
		
		final List<OntClass> inheritedSuperOntClassList = new ArrayList<OntClass>();
		for (OntClass superOntClass : superOntClassList) {
			if (superOntClass.getURI() != null) {
				List<OntClass> supSupOntClassList = toList( superOntClass.listSuperClasses(false) );
				for (OntClass supSupOntClass : supSupOntClassList) {
					if (!inheritedSuperOntClassList.contains(supSupOntClass)) {
						inheritedSuperOntClassList.add(supSupOntClass);
					}
				}
			}
		}
		for (OntClass inheritedSuperOntClass : inheritedSuperOntClassList) {
			superOntClassDescrList.add(OntResourceUtil.getOntClassAsHtml(ontModel, inheritedSuperOntClass));
		}
		
		System.out.println(Util.listToMultiLineString(superOntClassDescrList));
		
		if (true) return;
		
		final String NS = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabularies#";
		final String codeAnnoPropURI = NS + "code";
		final String descAnnoPropURI = NS + "description";
		final AnnotationProperty codeAnnoProp = ontModel.getAnnotationProperty(codeAnnoPropURI);
		final AnnotationProperty descAnnoProp = ontModel.getAnnotationProperty(descAnnoPropURI);
		
		final String termURI = NS + "Vocab-003-024";
		final OntClass ontClass = ontModel.getOntClass(termURI);
		
		System.out.println(ontClass);
		final RDFNode code = ontClass.getPropertyValue(codeAnnoProp);
		final String codeStr = (code != null) ? ("" + code).trim() : "";
		final RDFNode desc = ontClass.getPropertyValue(descAnnoProp);
		final String descStr = (desc != null) ? ("" + desc).trim() : "";
		
		if (codeStr.length() > 0) System.out.println("code: " + codeStr);
		if (descStr.length() > 0) System.out.println("desc: " + descStr);
		
		//
		System.out.println("....");
		
		ExtendedIterator<AnnotationProperty> annoPropIter = ontModel.listAnnotationProperties();
		while (annoPropIter.hasNext()) {
			final AnnotationProperty annoProp = (AnnotationProperty) annoPropIter.next();
			final RDFNode rdfNode = ontClass.getPropertyValue(annoProp);
			if (rdfNode != null) {
				System.out.println(" ** " + annoProp.getLocalName() + " :: " + rdfNode);
			}
		}
		
		System.out.println();
		System.out.println(" >> OntoTestringMain: Process TERMINATED at " + new Date());
		
	}
	
	private static boolean termIsA(final OntClass term, final OntClass broadTerm) {
		if (term == null || broadTerm == null) return false;
		if (broadTerm.getURI().equals(term.getURI())) return true;
		final ExtendedIterator<OntClass> exIt = term.listSuperClasses();
		while (exIt.hasNext()) {
			final OntClass superOntClass = exIt.next();
			
			if (termIsA(superOntClass, broadTerm)) return true;
		}
		return false;
	}
	
    private static Set<OntClass> getSubClassesFor(final OntClass ontClass) {
    	final Set<OntClass> ontClassSet = new HashSet<>();
    	updateSubClassesFor(ontClassSet, ontClass);
    	return ontClassSet;
    }
    
    private static void updateSubClassesFor(final Set<OntClass> ontClassSet, final OntClass ontClass) {
    	if (ontClass.getLocalName() != null) ontClassSet.add(ontClass);
    	final ExtendedIterator<OntClass> exIt = ontClass.listSubClasses();
    	while (exIt.hasNext()) {
			OntClass subOntClass = exIt.next();
			updateSubClassesFor(ontClassSet, subOntClass);
		}
    }
	
	private static Set<OntClass> getSuperOntClassSet(final OntClass ontClass) {
		final Set<OntClass> ontClassSet = new HashSet<>();
		updateSuperOntClassSet(ontClass, ontClassSet);
		return ontClassSet;
	}
	
	private static void updateSuperOntClassSet(final OntClass ontClass, final Set<OntClass> ontClassSet) {
		if (ontClass == null) return;
		if (!ontClass.isURIResource()) ontClassSet.add(ontClass);
		final List<OntClass> superOntClassList = toList(ontClass.listSuperClasses());
		for (OntClass superOntClass : superOntClassList) {
			updateSuperOntClassSet(superOntClass, ontClassSet);
		}
	}

}
