package test;

import org.ntua.web.oat.xls.OntologyGenService;

public class LoadCohortDataMain {
	private static final String METADATA_XLS_FILE_PATH = 
		"TEST-DATA/Test-Data-Excel-File-Metadata.xlsx";
		//"C:/Users/Efthymios/Downloads/Apache-Tomcat-with-OAT/Metadata CUMB FINISHED.xlsx";
	
	private static final String METADATA_GENERATED_OWL_FILE_PATH = 
		"TEST-DATA/Test-Data-Metadata-Ontology.owl";

	public static void main(String[] args) throws Exception {

		OntologyGenService.generateOntology(METADATA_XLS_FILE_PATH, METADATA_GENERATED_OWL_FILE_PATH);
	}	
	
}
