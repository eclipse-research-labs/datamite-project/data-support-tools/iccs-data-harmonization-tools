package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import org.apache.commons.io.IOUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MapScenariosMain {

	private static final String MAPPING_SCENARIOS_JSON_FILE_PATH = 
		"TEST-DATA/Mapping-Scenarios.json";
		//"./WebContent/WEB-INF/classes/Mapping-Scenarios.json";
	
	private static final String MAPPING_SCENARIOS_HTML_FILE_PATH = 
		"TEST-DATA/Mapping-Scenarios.html";
	
	private static final String TAB = "\t";
	private static final String NL = "\n";
	
	public static void main(String[] args) throws Exception {
		// Read File
		final InputStream is = new FileInputStream(MAPPING_SCENARIOS_JSON_FILE_PATH);
		final String jsonStr = IOUtils.toString(is, StandardCharsets.UTF_8.name());
		is.close();
		final JSONObject json = JSONObject.fromObject(jsonStr);
		System.out.println(json.toString(5));
		
		// Process Data
		final StringBuilder sb = new StringBuilder();

		sb.append("<html><head>" + NL);
		sb.append(TAB + "<title>HarmonicSS - Mapping Scenarios</title>" + NL);
		sb.append(
			TAB + "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>" + NL +
			TAB	+ "<style type=\"text/css\">" + NL +
			TAB + TAB + "body { margin: 0px; padding: 0px; }" + NL +
			TAB	+ TAB + "td { font: 1.0em sans-serif; }" + NL +
			TAB	+ TAB + "h1 { font: 1.5em sans-serif; font-weight: bold; color: darkred; margin: 10px 10px 20px 10px; }" + NL +
			TAB	+ TAB + "h2 { font: 1.3em sans-serif; font-weight: bold; margin: 10px; }" + NL +
			TAB	+ TAB + "h3 { font: 1.2em sans-serif; font-weight: bold; color: black; padding-bottom: 5px; border-bottom: 1px dashed darkred; padding: 5px 0px 5px 0px; margin: 0px 0px 6px 0px; }" + NL +
			TAB	+ TAB + "h4 { font: 1.1em sans-serif; font-weight: bold; color: #8A4B08; padding: 0px; margin: 0px; }" + NL +
			TAB	+ TAB + "p  { font: 1.0em sans-serif; padding: 0px; margin: 10px 0px 0px 10px; }" + NL +
			TAB	+ TAB + "table { width: 100%; padding: 10px; }" + NL +
			TAB	+ TAB + "table th { width: 10%; background-color: #e6e6e6; font: 1.1em sans-serif; font-weight: bold; text-align: left; padding: 5px 10px; }" + NL +
			TAB	+ TAB + "table td { background-color: #e6e6e6; text-align: left; padding: 5px 10px; }" + NL +
			TAB	+ TAB + ".entity1row th, .entity1row td { background-color: #edf5ff; }" + NL +
			TAB	+ TAB + ".entity2row th, .entity2row td { background-color: #f7fff1; }" + NL +
			TAB	+ TAB + ".servicerow th, .servicerow td { background-color: #fff2e6; }" + NL +
			TAB + TAB + ".entity1table, .entity2table, .servicetable { width: 100%; padding: 0px; }" + NL +
			TAB	+ TAB + ".entity1table th, .entity2table th, .servicetable th { font: 1.0em sans-serif; }" + NL +
			TAB + TAB + ".footer { border-top: 1px dashed gray; margin-top: 10px; padding-top: 10px; padding-bottom: 5px; text-align: center; color:#585858; font-size:70%; background-color: #E1E1E1; }" + NL +
			TAB	+ "</style>" + NL 
		);
		
		sb.append("</head><body>" + NL + NL);
		
		sb.append("<h1>Mapping Scenarios</h1>" + NL + NL);		
		
		final JSONArray categJA = json.getJSONArray("categJA");
		for (int i = 0; i < categJA.size(); i++) {
			final JSONObject categJO = categJA.getJSONObject(i);
			final String categName = categJO.getString("categ");
			sb.append("<h2>About.. " + categName + "</h2>" + NL + NL);
			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
			int categIndex = 0 ;
			for (int j = 0; j < mapscenJA.size(); j++) {
				categIndex++;
				// Mapping Scenario
				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);
				final String uri = mapscenJO.getString("uri").trim();
				final String name = mapscenJO.getString("name").trim();
				final String desc = mapscenJO.getString("desc").trim();
				sb.append("<p>(" + categIndex + ") " + uri + "</p>");
				sb.append("<table class='scenariotable'>");
				sb.append("<tr><th>Name</th><td><b>" + name + "</b></td></tr>");
				sb.append("<tr><th>Description</th><td>" + desc + "</td></tr>");
				// Entity 1
				final JSONObject ent1JO = mapscenJO.getJSONObject("entity1");
				final JSONArray ent1JA = ent1JO.getJSONArray("ja");
				int fieldsCount = 0;
				sb.append("<tr class='entity1row'><th>Entity 1</th><td>");
				sb.append("<table class='entity1table'>");
				for (Object obj : ent1JA) {
					fieldsCount++;
					final JSONObject paramJO = (JSONObject)obj;
					final String paramName = paramJO.getString("name");
					sb.append("<tr><th>Field " + fieldsCount + "</th><td>" + paramName + "</td></tr>");
				}
				sb.append("</table>");
				sb.append("</td></tr>");
				// Entity 2
				final JSONObject ent2JO = mapscenJO.getJSONObject("entity2");
				final String clsuri = ent2JO.getString("uri").trim();
				final JSONArray ent2JA = ent2JO.getJSONArray("ja");
				sb.append("<tr class='entity2row'><th>Entity 2</th><td>");
				sb.append("<table class='entity2table'>");
				sb.append("<tr><th>Class</th><td>" + clsuri + "</td></tr>");
				for (Object obj : ent2JA) {
					final JSONObject paramJO = (JSONObject)obj;
					final String propuri = paramJO.getString("uri");
					sb.append("<tr><th>Property</th><td>" + propuri + "</td></tr>");
				}
				sb.append("</table>");
				sb.append("</td></tr>");
				// Class/Service
				final JSONObject dtJO = mapscenJO.getJSONObject("datatrans");
				final String servuri = mapscenJO.getString("uri").trim();
				final String servdesc = mapscenJO.getString("desc").trim();
				final JSONArray servJA = dtJO.getJSONArray("ja");
				sb.append("<tr class='servicerow'><th>Service</th><td>");
				sb.append("<table class='servicetable'>");
				sb.append("<tr><td colspan='2'>" + servuri + "</td></tr>");
				sb.append("<tr><td colspan='2'>" + servdesc + "</td></tr>");
				for (Object obj : servJA) {
					final JSONObject attrJO = (JSONObject)obj;
					final String paramName = attrJO.getString("name");
					final String paramType = attrJO.getString("datatype");
					final String paramTypeUri = (attrJO.has("uri")) ? attrJO.getString("uri") : null ;
					final boolean canBeEmpty = attrJO.has("canBeEmpty");
					final String title = (paramTypeUri != null) ? " title='" + paramTypeUri + "'" : "";
					sb.append("<tr><th>" + paramName + "</th><td" + title + ">" + paramType + (canBeEmpty ? " ( Optional )" : "") + "</td></tr>");
				}
				sb.append("</table>");
				sb.append("</td></tr>");
				
				sb.append("</table>" + NL + NL);
			} // END OF Mapping Scenarios Loop
		} // END OF Categories Loop
		
		sb.append(NL + "<br>" + NL + "<p class=\"footer\">Document automatically generated on " + new Date() + "</p>" +  NL);
		
		sb.append(NL + "</body></html>");
		
		// Save Data to File	
		final Writer out = new BufferedWriter(
			new OutputStreamWriter(
				new FileOutputStream(
					new File(MAPPING_SCENARIOS_HTML_FILE_PATH)), "UTF8"));
		out.append(sb.toString());
		out.flush();
		out.close();
		
		System.out.println("Data saved to File..");
	}
	
}
