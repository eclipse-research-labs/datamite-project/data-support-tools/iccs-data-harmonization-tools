package test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.ontology.impl.OntModelImpl;
import org.apache.jena.ontology.impl.OntResourceImpl;
import org.apache.jena.vocabulary.XSD;
import org.timchros.core.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class TestingPlace {

	private static final String OAT_PROPERTIES_FILE_PATH = 
		"TEST-DATA//oat.properties";
	
	public static JSONArray dateFormatJA = new JSONArray();
	public static JSONArray unitJA = new JSONArray();
	public static JSONArray numFormatJA = new JSONArray();
	
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {
		
    	final InputStream propis = new FileInputStream(OAT_PROPERTIES_FILE_PATH);
    	final Properties prop = new Properties();
    	prop.load(propis);
    	propis.close();
    	
    	System.out.println(prop);
    	
    	dateFormatJA = JSONArray.fromObject(prop.get("date.format.ja"));
    	unitJA = JSONArray.fromObject(prop.get("unit.ja"));
    	numFormatJA = JSONArray.fromObject(prop.get("number.format.ja"));
    	
		if (true) return;
		
		final JSONArray ja = new JSONArray();
		ja.add("yyyy");
		ja.add("yyyy/MM/dd");
		//ja.add("yyyy/M/d");
		
		System.out.println(ja);
		
		System.out.println();
		final Date date = new Date();
		for (Object pattern : ja) {
			SimpleDateFormat sdf = new SimpleDateFormat("" +pattern);
			System.out.println(sdf.format(date));
		}
		
		if (true) return;
		
		JSONObject js = new JSONObject();
		js.put("msg", "json object");
		
		System.out.println(js);
		
		System.out.println("start");
		
		final List<org.apache.jena.rdf.model.Resource> typeResList = Util.newArrayList(
			XSD.xboolean, org.apache.jena.vocabulary.XSD.xbyte, XSD.xshort, XSD.xlong, XSD.xint, XSD.integer, XSD.xfloat, XSD.xdouble, XSD.xstring, XSD.date, XSD.time, XSD.dateTime
		);
		for (org.apache.jena.rdf.model.Resource resource : typeResList) {
			org.apache.jena.ontology.OntResource ontRes = new OntResourceImpl(resource.asNode(), (OntModelImpl) new OntModelImpl(OntModelSpec.OWL_MEM));
			System.out.println(getName(ontRes));
		}

		System.out.println("end");		
	}

	public static String getName(final OntResource ontoRes) {
		if (ontoRes == null) return null;
		final String ontResLab = ontoRes.getLabel(null);
		
		if (ontResLab != null) return ontResLab;
		final String ontResName = ontoRes.getLocalName();
		return ontResName;
	}
	
}
