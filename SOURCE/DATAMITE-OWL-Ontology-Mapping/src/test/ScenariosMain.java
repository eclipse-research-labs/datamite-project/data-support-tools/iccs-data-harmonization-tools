package test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ScenariosMain {

	private static final String MAPPING_SCENARIOS_JSON_FILE_PATH = 
			"TEST-DATA/Mapping-Scenarios.json";
	
	@SuppressWarnings("unused")
	public static void main(String[] args) throws Exception {

		//System.out.println(getAllMapScenarios().toString(5,5 ));
		//System.out.println(getMapScenarioDetails(null).toString(5,5 ));
		
		final InputStream is = new FileInputStream(MAPPING_SCENARIOS_JSON_FILE_PATH); //Thread.currentThread().getContextClassLoader().getResourceAsStream("Mapping-Scenarios.json");
		final String jsonStr = IOUtils.toString(is, StandardCharsets.UTF_8.name());
		is.close();
		System.out.println(jsonStr);
		
		final JSONObject json = JSONObject.fromObject(jsonStr);
		
		final List<String> uriList = new ArrayList<>();
		final JSONArray categJA = json.getJSONArray("categJA");
		for (int i = 0; i < categJA.size(); i++) {
			final JSONObject categJO = categJA.getJSONObject(i);
			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
			for (int j = 0; j < mapscenJA.size(); j++) {
				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);
				final JSONObject entityJO = mapscenJO.getJSONObject("entity2");
				final String classURI = entityJO.getString("uri").trim();
				uriList.add(classURI);
				final JSONArray propJA = entityJO.getJSONArray("ja");
				for (int k = 0; k < propJA.size(); k++) {
					final JSONObject propJO = propJA.getJSONObject(k);
					final String propURI = propJO.getString("uri").trim();
					uriList.add(propURI);
				} // END OF Entity 2 Prop URIs
				
				if (mapscenJO.has("datatrans")) {
					final JSONObject datatransJO = mapscenJO.getJSONObject("datatrans");
					final JSONArray attrJA = datatransJO.getJSONArray("ja");
					for (int k = 0; k < attrJA.size(); k++) {
						final JSONObject attrJO = attrJA.getJSONObject(k);
						if (attrJO.has("datatype") && attrJO.getString("datatype").equalsIgnoreCase("CLASS") && attrJO.has("uri")) {
							final String domainUri = attrJO.getString("uri");
							System.out.println(" ** URI: " + domainUri);
						}
					}
				}
				
			} // END OF Mapping Rules
		} // END OF Categories
		
//		for (Object uri : uriList) {
//			System.out.println(uri);
//		}
		
//		if (true) return;
//		
		
		
		final String testURI = "http://abcdefg-1-1";
//		final JSONArray categJA = json.getJSONArray("categJA");
//		for (int i = 0; i < categJA.size(); i++) {
//			final JSONObject categJO = categJA.getJSONObject(i);
//			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
//			for (int j = 0; j < mapscenJA.size(); j++) {
//				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);
//				final String uri = mapscenJO.getString("uri").trim();
//				if (uri.equals(testURI.trim())) {
//					System.out.println(mapscenJO);
//				}
//			}
//		}
	}

	public static JSONObject getAllMapScenarios() {
		final JSONObject json = new JSONObject();
		
		final JSONArray categja = new JSONArray();
		for (int i = 1; i <= 5; i++) {
			final JSONObject categJson = new JSONObject();
			categJson.put("name", "Category " + i);
			final JSONArray mapscenja = new JSONArray();
			for (int j = 1; j <= 6; j++) {
				final JSONObject mapScenJson = new JSONObject();
				mapScenJson.put("scenname", "Mapping Scenario: " + i + "-" + j);
				mapScenJson.put("scenuri", "http://abcdefg-" + i + "-" + j);
				mapscenja.add(mapScenJson);
			}
			categJson.put("mapscenja", mapscenja);
			categja.add(categJson);
		}
		
		json.put("categja", categja);
		return json;
	}
	
	public static JSONObject getMapScenarioDetails(final String uri) {
		final JSONObject json = new JSONObject();
		
		final JSONObject entity1json = new JSONObject();
		final JSONArray entity1ja = new JSONArray();
		for (int i = 0; i < 3; i++) {
			final JSONObject propjson = new JSONObject();
			propjson.put("name", "Cohort Property Name " + i);
			entity1ja.add(propjson);
		}
		entity1json.put("ja", entity1ja);
		
		final JSONObject entity2json = new JSONObject();
		entity2json.put("uri", "http://entity-uri");
		final JSONArray entity2ja = new JSONArray();
		for (int i = 0; i < 4; i++) {
			final JSONObject propjson = new JSONObject();
			propjson.put("uri", "http://entity/proprety-uri" + i);
			if (i % 2 == 0 ) {
				propjson.put("proptype", "DT");
			} else {
				propjson.put("proptype", "OP");
			}
			entity2ja.add(propjson);
		}
		entity2json.put("ja", entity2ja);
		
		final JSONObject datatransjson = new JSONObject();
		datatransjson.put("uri", "http://entity-uri");
		final JSONArray serviceja = new JSONArray();
		for (int i = 0; i < 5; i++) {
			final JSONObject servicejson = new JSONObject();
			servicejson.put("name", "name" + i);
			serviceja.add(servicejson);
		}
		datatransjson.put("ja", serviceja);
		
		json.put("entity1", entity1json);
		json.put("entity2", entity2json);
		json.put("datatrans", datatransjson);
		
		return json;
	}
	
}
