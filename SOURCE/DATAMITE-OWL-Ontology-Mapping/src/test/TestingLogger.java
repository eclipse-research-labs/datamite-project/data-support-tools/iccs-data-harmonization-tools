package test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class TestingLogger {

	private static final String LOG4J_PROPERTIES_FILE_PATH =
		"TEST-DATA/log4j.properties";
	
	public static void main(String[] args) throws FileNotFoundException, IOException {

		final Logger  log = Logger.getLogger(TestingLogger.class.getSimpleName() + " ... ");
		final Properties prop  = new Properties();
		prop.load(new FileInputStream(LOG4J_PROPERTIES_FILE_PATH));
		PropertyConfigurator.configure(prop);
		
		log.debug("test");
		log.info("test");
		
	}

}
