package org.ntua.web.oat;

import static org.ntua.web.oat.ServletUtil.doubleToString;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.XSD;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.ntua.web.oat.MappingScenariosRep.Category;
import org.ntua.web.oat.pat.onto.A_Pattern;
import org.ntua.web.oat.pat.onto.cls.SimpleOntoClassPattern;
import org.ntua.web.oat.pat.onto.dataprop.SimpleDataPropPattern;
import org.ntua.web.oat.sim.tech.PorterStemmer;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Checks;
import org.timchros.core.util.Throable;
import org.timchros.core.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * This Servlet is being used for retrieving the similarities among the elements of the given ontologies
 * 
 * @author Efthymios Chondrogiannis
 */
@WebServlet("/SuggestionsServlet")
public class SuggestionsServlet extends HttpServlet {
	
	final List<Resource> typeResList = Util.newArrayList(
		XSD.xboolean, XSD.xbyte, XSD.xshort, XSD.xlong, XSD.xint, XSD.integer, XSD.xfloat, XSD.xdouble, XSD.xstring, XSD.date, XSD.time, XSD.dateTime
	);
	
	private static final Logger log = LogFactory.getLoggerForClass(SuggestionsServlet.class);
	
	private static final long serialVersionUID = 1L;

	private static final String timePattern = "HH:mm:ss";
	private static final SimpleDateFormat timeFormat = new SimpleDateFormat(timePattern);
	
//	private static double THRESHOLD = 0.5;
//	static {
//		try {
//		    final Properties prop  = new Properties();
//			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("oat.properties"));
//			final String thresholdStr = prop.getProperty("threshold");
//			THRESHOLD = doubleFromString(thresholdStr);
//			log.info("Loaded from oat.properties FILE... THRESHOLD: " + doubleToString(THRESHOLD));
//		} catch(Throwable t) {
//			final String msg = "Unable to read THRESHOLD from oat.properties FILE...";
//			log.error(msg + "\n" + Throable.throwableAsString(t));
//			log.warn(msg + " THRESHOLD: " + THRESHOLD);
//		}
//	}
	
	/**
     * @see HttpServlet#HttpServlet()
     */
    public SuggestionsServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String sid = ServletUtil.getSessionID();
		final String jsonAstr = request.getParameter("ontoAJsonStr");
		final String jsonBstr = request.getParameter("ontoBJsonStr");
		final String jsonMstr = request.getParameter("mappingsJsonStr");
		final String jsonRstr = request.getParameter("rejectJsonStr");
		
		try {
			
			log.debug("SID: " + sid + " ... Getting SUGGESTIONS");
			
			final JSONObject jsonA = JSONObject.fromObject(jsonAstr);
			final JSONObject jsonB = JSONObject.fromObject(jsonBstr);
			final JSONObject jsonM = JSONObject.fromObject(jsonMstr);
			final JSONObject jsonR = JSONObject.fromObject(jsonRstr);
			
			log.trace("jsonA: \n" + jsonA.toString(5,5) + "\n\n");
			log.trace("jsonB: \n" + jsonB.toString(5,5) + "\n\n");
			log.trace("jsonM: \n" + jsonM.toString(5,5) + "\n\n");
			log.trace("jsonR: \n" + jsonR.toString(5,5) + "\n\n");
			
			final String ontoAuid = jsonA.getString("uid");
			final String ontoBuid = jsonB.getString("uid");
			
			log.trace("OntUIDs:: ontoAuid: " + ontoAuid + " , ontoBuid: " + ontoBuid);
			
			final OntModel ont1Model = OntoRep.getInstance().getOntoModelByUID(ontoAuid);
			final OntModel ont2Model = OntoRep.getInstance().getOntoModelByUID(ontoBuid);
			
			final JSONObject json = getSimilarityResponse(ont1Model, ont2Model, jsonM, jsonR);
			
			response.setContentType("application/json; charset=UTF-8");
			final PrintWriter pw = response.getWriter();
			pw.print(json.toString());
			pw.close();
			
			final JSONArray termsJA = json.has("vocabsjsonarray") ? json.getJSONArray("vocabsjsonarray") : new JSONArray();
			final JSONArray fieldsJA = json.has("fieldsjsonarray") ? json.getJSONArray("fieldsjsonarray") : new JSONArray();
			
			log.info("SID: " + sid + " ... Getting SUGGESTIONS Successfully Completed ! Found: " + fieldsJA.size() + " Fields and " + termsJA.size() +" Set of Terms.");
			log.trace("SID: " + sid + "\nTERMS::\n" + termsJA.toString(5,5));
			log.trace("SID: " + sid + "\nFIELDS::\n" + fieldsJA.toString(5,5));
		} catch(Throwable t) {
			log.error(Throable.throwableAsString(t));
			final JSONObject errorJsonObj = new JSONObject();
			String returnMsg = Throable.throwableAsSimpleOneLineString(t);
			if (returnMsg != null && returnMsg.length() > 200) returnMsg = returnMsg.substring(0, 200) + " ...";
			errorJsonObj.put("error", returnMsg);
			response.setContentType("application/json; charset=UTF-8");
			final PrintWriter out = response.getWriter();
			out.print(errorJsonObj.toString());
			out.close();
		}
	
	}
	
	private static final String HARM_VOCAB_NS = 
			"http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#";

	/** Map with the Prefix (i.e., Local Name Prefix) of Terms from each Domain */
	private static final Map<String, String> harmTermPrefixMap = Util.newHashMap(
			// About Demographic Characteristics
			"Sex", "SEX-",
			"Ethnicity", "ETHN-",
			"Education-Level", "EDU-",
			"Body-Mass-Index", "BMI-",
			// About Smoking Status and Pregnancies
			"Tobacco-Consumption-Status", "SMOK-",
			"Pregnancy-Outcome", "PREG-",
			// About Medical Conditions and Symptoms/Signs
			"Medical-Condition", "COND-",
			"Lymphoma-Organ", "ORGAN",
			"Lymphoma-Performance-Status", "PERFORM-",
			"Symptom-Sign", "SYMPT-",
			// About Drugs
			"Drug-Substance", "CHEM-",
			// About Questionnaires
			"Questionnaire-Score", "QUEST-",
			"ESSDAI-Domain", "ESSDAI-",
			"CACI-Condition", "CACI-",
			// About Questionnaires Outcome
			"Activity-Level", "ACTLVL-",
			"Risk-Non-Hodgkin-Lymphoma", "IPI-RISK-",
			// About Medical Tests
			"Blood-Test", "BLOOD-",
			"Urine-Test", "URINE-",
			"Oral-Test", "ORAL-",
			"Ocular-Test", "OCULAR-",
			"Medical-Imaging-Test", "IMG-",
			"Salivary-Gland-Biopsy", "SAL-",
			"Biopsy-Test", "BIOPSY-",
			// About Medical Tests Outcome
			"Antinuclear-Antibody-Pattern", "ANA-PAT-",
			"Cryoglubin-Type", "CRYO-",
			// Other Terms
			"Specialty-Department", "SPEC-",
			"Assessment", "ASSESS-",
			"Confirmation", "CONFIRM-"
		);
	
	/** Searching for similarities among Fields and Reference Model terms */
	public static synchronized JSONObject getSimilarityResponse(final OntModel ont1Model, final OntModel ont2Model, final JSONObject jsonM, final JSONObject jsonR) throws ServletException, IOException {
		Checks.checkNotNullArg(ont1Model, "ont1Model", ont2Model, "ont2Model");
		
		// CHECK IF ONTOLOGIES ARE THE SAME
		// Revise if necessary and whether we can check it with a different way
		
		// Get Classes
		final List<OntClass> ont1ClassList = ServletUtil.toList(ont1Model.listClasses());
		final List<OntClass> ont2ClassList = ServletUtil.toList(ont2Model.listClasses());
		// Get Object Properties
		final List<ObjectProperty> ont1ObjPropList = ServletUtil.toList(ont1Model.listObjectProperties());
		final List<ObjectProperty> ont2ObjPropList = ServletUtil.toList(ont2Model.listObjectProperties());
		// Get Datatype Properties
		final List<DatatypeProperty> ont1DataPropList = ServletUtil.toList(ont1Model.listDatatypeProperties());
		final List<DatatypeProperty> ont2DataPropList = ServletUtil.toList(ont2Model.listDatatypeProperties());
		// Get Individuals
		final List<Individual> ont1IndivList = ServletUtil.toList(ont1Model.listIndividuals());
		final List<Individual> ont2IndivList = ServletUtil.toList(ont2Model.listIndividuals());		
		
		// Check IF further processing required
		if (ont1ClassList.size() == ont2ClassList.size() && 
			ont1ObjPropList.size() == ont2ObjPropList.size() && 
			ont1DataPropList.size() == ont2DataPropList.size() && 
			ont1IndivList.size() == ont2IndivList.size()) 
		{
			final JSONObject errorObjResp = new JSONObject();
			errorObjResp.put("datetime", timeFormat.format( new Date() ) );
			errorObjResp.put("error", "Ontologies uploaded are probably the same !");
			return errorObjResp;
		}
		
		// PART A : GET ONTOLOGICAL ELEMENTS SPECIFIED
		
		// Find the Cohort terms and place them in the appropriate category
		final Map<OntClass, List<OntClass>> cohortTermsMap = new HashMap<OntClass, List<OntClass>>();
		final ExtendedIterator<OntClass> cohortClsIt = ont1Model.listClasses();
		while (cohortClsIt.hasNext()) {
			final OntClass ontClass =cohortClsIt.next();
			final String localname = ontClass.getLocalName();
			if (localname  == null) continue;
			// Focus on Vocabularies
			if (!localname.contains("Domain") || !localname.contains("Term")) continue;
			// Place terms in the Appropriate Category (if any) based on the Prefix of the Local Name
			final OntClass categOntClass = ontClass.getSuperClass();
			if (!cohortTermsMap.containsKey(categOntClass)) cohortTermsMap.put(categOntClass, new ArrayList<>());
			cohortTermsMap.get(categOntClass).add(ontClass);
		}
		// Sort terms recorded for each category based on their name
		for (Entry<OntClass, List<OntClass>> entry : cohortTermsMap.entrySet()) {
			//System.out.println(entry.getKey().getLocalName() + " --> " +  entry.getValue().size());
			Collections.sort(entry.getValue(), new Comparator<OntClass>() {
				@Override public int compare(OntClass o1, OntClass o2) {
					return o1.getLocalName().compareTo(o2.getLocalName());
				}
			});
		}
		
		// Find the Cohort Fields
		final List<DatatypeProperty> cohortFieldsList = new ArrayList<>();
		final ExtendedIterator<DatatypeProperty> cohortPropIt = ont1Model.listDatatypeProperties();
		while (cohortPropIt.hasNext()) {
			final DatatypeProperty datatypeProperty = cohortPropIt.next();
			final String local = datatypeProperty.getLocalName();
			if (!local.startsWith("Parameter-")) continue;
			cohortFieldsList.add(datatypeProperty);
		}
		
		// Find the HarmonicSS terms and place them in the appropriate category
		final Map<OntClass, List<OntClass>> harmLocTermsMap = new HashMap<OntClass, List<OntClass>>();
		for (Entry<String, String> entry : harmTermPrefixMap.entrySet()) {
			final OntClass categOntClass = ont2Model.getOntClass(HARM_VOCAB_NS + entry.getKey());
			if (categOntClass == null) throw new RuntimeException("Cannot find OntClass for \"" + entry.getValue() + "\".");
			harmLocTermsMap.put(categOntClass, new ArrayList<OntClass>());
		}
		final ExtendedIterator<OntClass> harmClsIt = ont2Model.listClasses();
		while (harmClsIt.hasNext()) {
			final OntClass ontClass =harmClsIt.next();
			final String ns = ontClass.getNameSpace();
			final String localname = ontClass.getLocalName();
			if (ns == null || localname  == null) continue;
			// Focus on Vocabularies
			if (!ns.equals(HARM_VOCAB_NS)) continue;
			// Ignore some terms
			if (MappingScenariosRep.ignTermWithCode(localname)) continue;
			if (localname.startsWith("CACI-C")) continue;
			if (localname.startsWith("SYMPT-CAT-")) continue;
			// Place terms in the Appropriate Category (if any) based on the Prefix of the Local Name			
			for (Entry<OntClass, List<OntClass>> entry : harmLocTermsMap.entrySet()) {
				final String prefix =  harmTermPrefixMap.get(entry.getKey().getLocalName());
				if (localname.startsWith(prefix)) {
					entry.getValue().add(ontClass);
					break;
				}
			}
		}		
		// Sort terms recorded for each category based on their name
		for (Entry<OntClass, List<OntClass>> entry : harmLocTermsMap.entrySet()) {
			//System.out.println(entry.getKey().getLocalName() + " --> " +  entry.getValue().size());
			Collections.sort(entry.getValue(), new Comparator<OntClass>() {
				@Override public int compare(OntClass o1, OntClass o2) {
					return o1.getLocalName().compareTo(o2.getLocalName());
				}
			});
		}
		
		// PART B : FIND CANDIDATE MAPPING RULES
		
		// B.1. Cohort Controlled Set of Terms Possible Alignments
		final Map<OntClass, List<Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>>>> vocabMap = 
			getPossibleTermsAlignment(cohortTermsMap, ont1Model, harmLocTermsMap, ont2Model, jsonM, jsonR);
		
		// B.2. Cohort Fields possibly Mappings/Scenarios
		final List<Tuple3<A_Pattern, A_Pattern, Tuple3<Double, String, List<String>>>> fields = 
			getPossibleFieldsMappings(cohortFieldsList, ont1Model, harmLocTermsMap, ont2Model, jsonM, jsonR);

		// PART C : PREPARE JSON RESPONSE
		
		// C.0 Sort Elements By Name (Vocabularies & TODO: Terms - if being necessary) 
		final List<Entry<OntClass, List<Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>>>>> vocabList = new ArrayList<>(vocabMap.entrySet());
		Collections.sort(vocabList, new Comparator<Entry<OntClass, List<Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>>>>>() {
			@Override public int compare(Entry<OntClass, List<Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>>>> e1, Entry<OntClass, List<Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>>>> e2) {
				return e1.getKey().getLocalName().compareToIgnoreCase(e2.getKey().getLocalName());
			}
		});
		
		// C.1 Cohort Fields
		// TODO: Revise when Fields suggestions Implemented
		final JSONArray fieldsJsonArray = new JSONArray();
		for (Tuple3<A_Pattern, A_Pattern, Tuple3<Double, String, List<String>>> tuple : fields) {
			final JSONObject json = new JSONObject();
			json.put("entity1", tuple._1().toJson());
			json.put("entity2", tuple._2().toJson());
			json.put("relation", "Linked With"); // TODO: change // OLD: Equivalent
			json.put("similarity", doubleToString( tuple._3()._1() ) );
			json.put("simcomments", tuple._3()._2() );
			json.put("scenariosja", prepareScenariosJA(tuple._3()._3()) );
			fieldsJsonArray.add(json);
		}
	
		// C.2 Controlled Set of Terms
		final JSONArray vocabJsonArray = new JSONArray();
		for (Entry<OntClass, List<Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>>>> entry : vocabList) {
			final JSONObject vocabJson = new JSONObject();
			vocabJson.put("domain", entry.getKey().getLabel(null) + 
				" (Terms Specified: " + cohortTermsMap.get(entry.getKey()).size() + " / Suggestions about: " + getDistinctCohortTerms(entry.getValue()) + ")");
			final JSONArray jsonArray = new JSONArray();
			for (Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>> tuple : entry.getValue()) {
				final JSONObject json = new JSONObject();
				json.put("entity1", tuple._1().toJson());
				json.put("entity2", tuple._2().toJson());
				json.put("relation", "Equivalent");
				json.put("similarity", doubleToString( tuple._3()._1() ) );
				json.put("simcomments", tuple._3()._2() );
				jsonArray.add(json);	
			}
			vocabJson.put("termsjsonarray", jsonArray);
			vocabJsonArray.add(vocabJson);
		}
	
		final JSONObject jsonObjResp = new JSONObject();
		jsonObjResp.put("datetime", timeFormat.format( new Date() ) );
		jsonObjResp.put("vocabsjsonarray", vocabJsonArray);
		jsonObjResp.put("fieldsjsonarray", fieldsJsonArray);
		
		return jsonObjResp;
	}	
	
	private static JSONArray prepareScenariosJA(final List<String> scenarioUriList) {
		final JSONArray ja = new JSONArray();
		for (String uri : scenarioUriList) {
			JSONObject scenarioJO = null;
			
			final JSONArray categJA = MappingScenariosRep.getInstance().getMapScenJson().getJSONArray("categJA");
    		for (int i = 0; i < categJA.size(); i++) {
    			final JSONObject categJO = categJA.getJSONObject(i);
    			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
    			for (int j = 0; j < mapscenJA.size(); j++) {
    				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);
    				final String scenuri = mapscenJO.getString("uri");
    				if (scenuri.equals(uri)) {
    					scenarioJO = mapscenJO; break;
    				}
    			}
    			if (scenarioJO != null) break;
    		}
			
    		if (scenarioJO == null) throw new RuntimeException("Unexpected error.. Scenarion not found: " + uri);
			
			final JSONObject jo = new JSONObject();
			jo.put("uri", uri);
			jo.put("name", scenarioJO.getString("name"));
			jo.put("desc", scenarioJO.getString("desc"));
			ja.add(jo);
		}
		return ja;
	}
	
	private static int getDistinctCohortTerms(List<Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>>> tupleList) {
		final Set<String> uriSet = new HashSet<>();
		for (Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>> tuple : tupleList) {
			if (tuple.get_1() instanceof SimpleOntoClassPattern) {
				SimpleOntoClassPattern data = (SimpleOntoClassPattern) tuple.get_1();
				uriSet.add(data.getOntoClass().getURI());
			}
			if (tuple.get_1() instanceof SimpleDataPropPattern) {
				SimpleDataPropPattern data = (SimpleDataPropPattern) tuple.get_1();
				uriSet.add(data.getDataProp().getURI());
			}
			// check if we need to do something else for the other patterns
		}
		
		return uriSet.size();
	}
	
	private static Map<OntClass, List<Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>>>> getPossibleTermsAlignment
		(final Map<OntClass, List<OntClass>> cohortTermsMap, final OntModel cohortOntModel, 
				final Map<OntClass, List<OntClass>> harmTermsMap, final OntModel harmOntModel , 
					JSONObject jsonM, JSONObject jsonR) {
		
		Checks.checkNotNullArg(
			harmTermsMap, "harmTermsMap", cohortTermsMap, "cohortTermsMap",
			harmOntModel, "harmOntModel", cohortOntModel, "cohortOntModel",
			jsonM, "jsonM", jsonR, "jsonR"
		);
		
		final Map<OntClass, List<Tuple3<A_Pattern, A_Pattern, Tuple2<Double, String>>>> map = new HashMap<>();
		
		// Detect and Provide also the broader domain in which the suggestions belong to
		for (Entry<OntClass, List<OntClass>> entry : cohortTermsMap.entrySet()) {
			final OntClass categOntClass = entry.getKey();
			for (OntClass termOntClass : entry.getValue()) { 
				// Ignore Terms already Mapped
				if (checkTermMapped(jsonM, termOntClass.getURI()))continue;
				
				final List<Tuple2<OntClass, String>> harmOntClassList = searchForTerm(harmOntModel, harmTermsMap, true, termOntClass.getLabel(null));
				
				// TODO: Ignore suggestion already rejected 
				// checkTermMapped(jsonR, termOntClass.getURI())
				
				// TODO: IF More than one... more mapping options... 
				if (harmOntClassList.size() == 1) {
					if (!map.containsKey(categOntClass)) map.put(categOntClass, new ArrayList<>());
					final Tuple2<OntClass, String> ontClassAlign = harmOntClassList.get(0);
					map.get(categOntClass).add( new Tuple3<A_Pattern, A_Pattern, Tuple2<Double,String>>(
						new SimpleOntoClassPattern(termOntClass), 
						new SimpleOntoClassPattern(ontClassAlign.get_1()), 
						new Tuple2<>(1.0, ontClassAlign.get_2())
					));
				} else {
					//System.out.println(" ** " + harmOntClassList);
				}
			}
		}
		return map;
	}
	
	private static final String symptomUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Symptom-Sign";
	private static final String medConditionUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Medical-Condition";
	private static final String drugUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Drug-Substance";
	private static final String questScoreUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Questionnaire-Score";
	private static final String bloodTestUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Blood-Test";
	private static final String urineTestUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Urine-Test";
	private static final String oralTestUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Oral-Test";
	private static final String ocularTestUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Ocular-Test";
	private static final String biopsyTestUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/terminology/vocabulary#Biopsy-Test";
	
	private static List<Tuple3<A_Pattern, A_Pattern, Tuple3<Double, String, List<String>>>> getPossibleFieldsMappings
	(final List<DatatypeProperty> cohortFieldsList, final OntModel cohortOntModel, 
			final Map<OntClass, List<OntClass>> harmTermsMap, final OntModel harmOntModel , 
				JSONObject jsonM, JSONObject jsonR) {
		
		Checks.checkNotNullArg(
				harmTermsMap, "harmTermsMap", cohortFieldsList, "cohortFieldsList",
				harmOntModel, "harmOntModel", cohortOntModel, "cohortOntModel",
				jsonM, "jsonM", jsonR, "jsonR"
			);
		
		final List<Tuple3<A_Pattern, A_Pattern, Tuple3<Double, String, List<String>>>> harmOntPropList = new ArrayList<>();
		
		for (DatatypeProperty cohortField : cohortFieldsList) {
			// Ignore Fields already Mapped
			if (checkFieldMapped(jsonM, cohortField.getURI())) continue;
			
			final List<Tuple2<OntClass, String>> harmOntClassList = searchForTerm(harmOntModel, harmTermsMap, true, cohortField.getLabel(null));
			
			//if (harmOntClassList.size() > 0) System.out.println(Util.listToMultiLineString(harmOntClassList));
			
			// TODO: Ignore Fields already rejected
			// checkFieldMapped(jsonR, cohortField.getURI())
			
			// TODO: IF More than one... more mapping options... 
			if (harmOntClassList.size() == 1) {
				// Depending on the type of Reference Model Class provide a List with possible scenarios
				final Tuple2<OntClass, String> ontClassAlign = harmOntClassList.get(0);
				final List<String> scenarioUriList = new ArrayList<>();
				if (termIsA(ontClassAlign._1(), harmOntModel.getOntClass(symptomUri))) {
					scenarioUriList.addAll(MappingScenariosRep.getInstance().getMapScenUrisForCategory(Category.Symptom));
				} else if (termIsA(ontClassAlign._1(), harmOntModel.getOntClass(medConditionUri))) {
					scenarioUriList.addAll(MappingScenariosRep.getInstance().getMapScenUrisForCategory(Category.Diagnosis));
				} else if (termIsA(ontClassAlign._1(), harmOntModel.getOntClass(drugUri))) {
					scenarioUriList.addAll(MappingScenariosRep.getInstance().getMapScenUrisForCategory(Category.Medication));
				} else if (termIsA(ontClassAlign._1(), harmOntModel.getOntClass(questScoreUri))) {
					scenarioUriList.addAll(MappingScenariosRep.getInstance().getMapScenUrisForCategory(Category.Score));
				} else if (termIsA(ontClassAlign._1(), harmOntModel.getOntClass(bloodTestUri)) || termIsA(ontClassAlign._1(), harmOntModel.getOntClass(urineTestUri)) || termIsA(ontClassAlign._1(), harmOntModel.getOntClass(oralTestUri)) || termIsA(ontClassAlign._1(), harmOntModel.getOntClass(ocularTestUri))) {
					scenarioUriList.addAll(MappingScenariosRep.getInstance().getMapScenUrisForCategory(Category.Lab_Test));
				} else if (termIsA(ontClassAlign._1(), harmOntModel.getOntClass(biopsyTestUri))) {
					scenarioUriList.addAll(MappingScenariosRep.getInstance().getMapScenUrisForCategory(Category.Biopsy_Test));
				} else {
					System.out.println("Under construction !");
				}

				harmOntPropList.add( new Tuple3<A_Pattern, A_Pattern, Tuple3<Double,String, List<String>>>(
					new SimpleDataPropPattern(cohortField), 
					new SimpleOntoClassPattern(ontClassAlign.get_1()), 
					new Tuple3<>(1.0, ontClassAlign.get_2(), scenarioUriList)
				));
			}
			
//			else if (harmOntClassList.size() > 1) {
//				for (Tuple2<OntClass, String> tuple : harmOntClassList) {
//					harmOntPropList.add( new Tuple3<A_Pattern, A_Pattern, Tuple2<Double,String>>(
//						new SimpleDataPropPattern(cohortField), 
//						new SimpleOntoClassPattern(tuple.get_1()), 
//						new Tuple2<>(new Double(1), tuple.get_2())
//					));
//				}
//			}
		}
		
		return harmOntPropList;
	}
	
	private static boolean termIsA(final OntClass term, final OntClass broadTerm) {
		if (term == null || broadTerm == null) return false;
		if (broadTerm.getURI().equals(term.getURI())) return true;
		final ExtendedIterator<OntClass> exIt = term.listSuperClasses();
		while (exIt.hasNext()) {
			final OntClass superOntClass = exIt.next();
			
			if (termIsA(superOntClass, broadTerm)) return true;
		}
		return false;
	}
	
	// Returns not only the element but also how mapping was found (e.g. based on the acronym, synonym, etc.)
	private static List<Tuple2<OntClass, String>> searchForTerm(final OntModel harmOntModel, final Map<OntClass, List<OntClass>> harmSetMap, final boolean isField, final String cohortTerm) {
		
		final List<Tuple2<OntClass, String>> sameList = new ArrayList<>();
		
		for (Entry<OntClass, List<OntClass>> entry : harmSetMap.entrySet()) {
			for (OntClass ontClass : entry.getValue()) {
				final String harmTerm = ontClass.getLabel(null);
				
				// Label
				final Tuple2<Boolean, String> labelCompTuple = compareStrings(cohortTerm, harmTerm);
				if (labelCompTuple._1()) {
					sameList.add(newOntClassTuple(ontClass, "The concepts have the same name. "  + labelCompTuple._2()));
					continue;
				}
				
				// Abbreviation
				final RDFNode acronymNode = ontClass.getPropertyValue(harmOntModel.getAnnotationProperty("http://www.semanticweb.org/ntua/iccs/harmonicss/terminology#acronym"));
				final String acronym = (acronymNode != null) ? acronymNode.asLiteral().getString() : null ;
				if (acronym != null) {
					if (compareAcronyms(cohortTerm,acronym)) {
						sameList.add(newOntClassTuple(ontClass, "The concept has the same acronym."));
						continue;
					}
				}
				
				// Synonyms
				final RDFNode akaNode = ontClass.getPropertyValue(harmOntModel.getAnnotationProperty("http://www.semanticweb.org/ntua/iccs/harmonicss/terminology#aka"));
				final String aka = (akaNode != null) ? akaNode.asLiteral().getString() : null ;
				if (aka != null) {
					boolean synonymFound = false;
					final String[] synonyms = aka.split(",");
					for (String synonym : synonyms) {
						final Tuple2<Boolean, String> synonymCompTuple = compareStrings(cohortTerm, synonym.trim());
						if (synonymCompTuple._1()) {
							sameList.add(newOntClassTuple(ontClass, "There is another term with the same name (synonym). "  + synonymCompTuple._2()));
							synonymFound = true; break;
						}
					}
					if (synonymFound) continue;
				}
				
			} // End of TERMS loop
			
		} // End of VOCABULARIES loop
		
		return sameList;
	}
	
	private static final Tuple2<OntClass, String> newOntClassTuple(final OntClass cls, final String msg) {
		return new Tuple2<OntClass, String>(cls, msg);
	}
	
	public static boolean compareAcronyms(final String str1, final String str2) {
		return str1.equalsIgnoreCase(str2);
	}
	
	public static Tuple2<Boolean, String> compareStrings(final String str1, final String str2) {
		final Tuple2<Boolean, String> tuple = arePhrasesSame(str1, str2);
		
//		if (tuple._1() && !str1.equalsIgnoreCase(str2)) {
//			System.out.println(" ** " + str1 + " ** " + str2 + " **");
//		}
		
		return tuple;
	}
	
/*
	public static final boolean checkPhrase1ContainsPhrase2(String phrase1, String phrase2) {
		// Check null or empty phrases
		if (phrase1 == null || phrase2 == null)
			return false;
		if (phrase1.trim().equals("") || phrase2.trim().equals(""))
			return false;
		
		if (phrase1.length() < phrase2.length()) return false;
		
		if (phrase1.indexOf(phrase2 + " ") >= 0 || phrase1.indexOf(" " + phrase2) >= 0) return true;
		
		final String phrase1toLowerCase = phrase1.toLowerCase();
		final String phrase2toLowerCase = phrase2.toLowerCase();
		
		if (phrase1toLowerCase.indexOf(phrase2toLowerCase + " ") >= 0 || phrase1toLowerCase.indexOf(" " + phrase2toLowerCase) >= 0) return true;
		
		final List<String> str1TokenList = getMainTokens(phrase1toLowerCase);
		final List<String> str2TokenList = getMainTokens(phrase2toLowerCase);
		
		if (str1TokenList.isEmpty() || str2TokenList.isEmpty())
			return false;
		if (str1TokenList.size() != str2TokenList.size())
			return false;
		
		for (String str2 : str2TokenList) {
			if (!existTermInList(str2, str1TokenList)) return false;
		}
		
		return true;
	}
*/	
	
	// Returns info about matching process (e.g., exactly the same)
	public static final Tuple2<Boolean, String> arePhrasesSame(String phrase1, String phrase2) {
		// Check null or empty phrases
		if (phrase1 == null || phrase2 == null)
			return FalseResponse;
		if (phrase1.trim().equals("") || phrase2.trim().equals(""))
			return FalseResponse;
		
		final String phrase1toLowerCase = phrase1.toLowerCase();
		final String phrase2toLowerCase = phrase2.toLowerCase();
		
		// Exactly the same Sequence of characters
		if (phrase1toLowerCase.equals(phrase2toLowerCase)) 
			return newTrueResponse("Strings \"" + phrase1 +"\" and \"" + phrase2 + "\" consist of the same sequence of characters.");
		
		final List<String> str1TokenList = getMainTokens(phrase1toLowerCase);
		final List<String> str2TokenList = getMainTokens(phrase2toLowerCase);
		
//		if (phrase1toLowerCase.indexOf("stomi") >= 0 && phrase2toLowerCase.indexOf("stomi") >= 0)
//			System.out.println(phrase1 + " -- " + phrase2);
		
		// TODO: Check & Comment / Uncomment
		if (phrase2toLowerCase.length() > 3 && phrase1toLowerCase.indexOf(phrase2toLowerCase) >= 0) {
			// System.out.println(phrase1 + " -- " + phrase2);
			return newTrueResponse("Strings \"" + phrase1 +"\" contains \"" + phrase2 + "\".");
		}
		
		final String phrase1LowCasClean = remPunChar(phrase1toLowerCase);
		final String phrase2LowCasClean = remPunChar(phrase2toLowerCase);
		
		if (phrase2LowCasClean.length() > 3 && phrase1LowCasClean.indexOf(phrase2LowCasClean) >= 0) {
			// System.out.println(phrase1 + " -- " + phrase2);
			return newTrueResponse("Strings \"" + phrase1 +"\" probably contains \"" + phrase2 + "\".");
		}
		
//		// TODO: Comment
//		//System.out.println(phrase1 + " -- " + phrase2);
//		if (str1TokenList.size() >= str2TokenList.size()) {
//			if (phrase1toLowerCase.indexOf("ant") >= 0 && phrase2toLowerCase.indexOf("ant") >= 0)
//				System.out.println(str1TokenList + " -- " + str2TokenList);
//			// TODO: check if ALL str 2 tokens exist in str 1
//		}
		
		
		if (str1TokenList.isEmpty() || str2TokenList.isEmpty())
			return FalseResponse;
		if (str1TokenList.size() != str2TokenList.size())
			return FalseResponse;
		
		// Examine tokens, ignoring their order
		for (String str1 : str1TokenList) {
			if (!existTermInList(str1, str2TokenList)) return FalseResponse;
		}
		for (String str2 : str2TokenList) {
			if (!existTermInList(str2, str1TokenList)) return FalseResponse;
		}
		
		return newTrueResponse("Strings \"" + phrase1 +"\" and \"" + phrase2 + "\" consist of the same main elements.");
	}
	
	private static String remPunChar(final String str) {
		if (str == null) return null;
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == '-' || c == '/') continue;
			sb.append(c);
		}
		return sb.toString();
	}
	
	private static final Tuple2<Boolean, String> FalseResponse =new Tuple2<Boolean, String>(Boolean.FALSE, null);

	
	private static final Tuple2<Boolean, String> newTrueResponse(final String str) {
		return new Tuple2<Boolean, String>(Boolean.TRUE, str);
	}
	
	private static final boolean existTermInList(final String term, final List<String> termsList) {
		if (term == null || termsList.isEmpty()) return false;
		for (String otherTerm : termsList) {
			if (areWordsSame(term, otherTerm)) return true;
		}
		return false;
	}
	
	private static final List<String> getMainTokens(String phrase) {
		final List<String> tokenList = new ArrayList<String>();
		// TODO: Review: Punctuation characters are ignored during the parsing... splitting of the given phrase
		final String splitRegex = "[\\s\\t\\n\\.,;?!><=/]+";
		final String[] tokenArray = phrase.split(splitRegex);
		for (int i = 0; i < tokenArray.length; i++) {
			final String token = tokenArray[i];
			// Ignore Stop words
			if (isStopWord(token))
				continue;
			// Ignore some other Non-important words
			if (isOtherNonImportantWord(token))
				continue;
			tokenList.add(token);
		}
		return tokenList;
	}
	
	private static final List<String> stopWordList = org.timchros.core.util.Util.newArrayList(
			"a", "able", "about", "across", "after", "all", "almost", "also", "am", "among", "an", "and", "any", "are",  "as", "at", "be", "because", "been", "but", "by", "can", "cannot", "could", "dear", "did", "do", "does", "either", "else", "ever", "every", "for", "from", "get", "got", "had", "has", "have", "he", "her", "hers", "him", "his", "how", "however", "i", "if", "in", "into", "is", "it", "its", "just", "least", "let", "like", "likely", "may", "me", "might", "most", "must", "my", "neither", "no", "nor", "not", "of", "off", "often", "on", "only", "or", "other", "our", "own", "rather", "said", "say", "says", "she", "should", "since", "so", "some", "than", "that", "the", "their", "them", "then", "there", "these", "they", "this", "tis", "to", "too", "twas", "us", "wants", "was", "we", "were", "what", "when", "where", "which", "while", "who", "whom", "why", "will", "with", "would", "yet", "you", "your"
		);
	
	// TODO: review
	private static boolean isStopWord(String word) {
		return stopWordList.contains(word);
	}
	
	private static final List<String> otherWordList = org.timchros.core.util.Util.newArrayList(
			"test", "check", "presence", "disease", "symptom", "condition", "phenomenon"
		);
	
	// TODO: review use Stem
	private static boolean isOtherNonImportantWord(String word) {
		return otherWordList.contains(word);
	}
	
	private static boolean areWordsSame(String word1, String word2) {
		if (word1 == null || word2 == null)
			return false;
		// TODO: check if we need to remove punctuation chars from beginning/end
		if (word1.equals(word2))
			return true;
		// Check stems on condition that both words consist of more than 3 characters
		if (word1.length() <= 3 || word2.length() <= 3) return false;
		final String word1stem = getStemmedWord(word1);
		final String word2stem = getStemmedWord(word2);
		if (word1stem.equals(word2stem))
			return true;
		return false;
	}
	
	private static String getStemmedWord(String word) {
		PorterStemmer stemmer = new PorterStemmer();
		stemmer.add(word.toCharArray(), word.length());
		stemmer.stem();
		String stemmedWord = stemmer.toString();
		if (stemmedWord.endsWith("'")) stemmedWord = stemmedWord.substring(0, stemmedWord.length()-1);
		return stemmedWord;
	}	
	
	private static final String harmPath = 
		"WebContent/WEB-INF/classes/" + OntoFileUploadServlet.HARMONICSS_REFERENCE_MODEL;
	private static final String cohortOntFilePath = 
		"ONTO/UNIRO-2019-04-15-Cohort-Metadata-FINAL-v.0.4.4.xlsx.owl";
		//"ONTO/25-UU-Cohort-Metadata-v.0.2.xlsx.owl";
	
	/** For testing purposes... */
	public static void main(String[] args) throws Exception {
		
		System.out.println(">> SuggestionsServlet: BEGIN: " + new Date() + "\n");
		
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.WARN);
		
		final JSONObject jsonM = JSONObject.fromObject("{ jsonarray: [] }");
		final JSONObject jsonR = JSONObject.fromObject("{ jsonarray: [] }");
		
		final OntModel cohortOntModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		cohortOntModel.read(new FileInputStream(new File(cohortOntFilePath)), null);
		
		final OntModel harmOntModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
		harmOntModel.read(new FileInputStream(new File(harmPath)), null);
		
		//new SuggestionsServlet().getSimilarityResponse(ont1Model, ont2Model, jsonM, jsonR);
		JSONObject jo = SuggestionsServlet.getSimilarityResponse(cohortOntModel, harmOntModel, jsonM, jsonR);
		System.out.println(jo);
		
		System.out.println("\n>> SuggestionsServlet: END: " + new Date());
	}
	
	public static boolean checkTermMapped(final JSONObject json, final String termUri) {
		if (json == null || json.isEmpty()) return false;
		if (!json.has("jsonarray")) return false;
		final JSONArray ja = json.getJSONArray("jsonarray");
		if (ja == null || ja.isEmpty()) return false;
		for (int i = 0; i < ja.size(); i++) {
			final JSONObject mapJO = ja.getJSONObject(i);
			final JSONObject entity1JO = mapJO.getJSONObject("entity1");
			final String entity1pid = entity1JO.getString("pid");
			if (entity1pid.equals("SimpleClassPattern")) {
				final String mapTermUri = entity1JO.getString("classuri");
				if (mapTermUri.equals(termUri)) return true;
			} 
		}
		return false;
	}
	
	public static boolean checkFieldMapped(final JSONObject json, final String termUri) {
		if (json == null || json.isEmpty()) return false;
		if (!json.has("jsonarray")) return false;
		final JSONArray ja = json.getJSONArray("jsonarray");
		if (ja == null || ja.isEmpty()) return false;
		for (int i = 0; i < ja.size(); i++) {
			final JSONObject mapJO = ja.getJSONObject(i);
			final JSONObject entity1JO = mapJO.getJSONObject("entity1");
			final String entity1pid = entity1JO.getString("pid");
			if (entity1pid.equals("SimplePropertyPattern")) {
				final String mapTermUri = entity1JO.getString("propertyuri");
				if (mapTermUri.equals(termUri)) return true;
			} else if (entity1pid.equals("PropertiesCollectionPattern")) {
				final JSONArray propJA = entity1JO.getJSONArray("proparray");
				for (int j = 0; j < propJA.size(); j++) {
					final JSONObject propJO = propJA.getJSONObject(j);
					final String proppid = propJO.getString("pid");
					if (proppid.equals("SimplePropertyPattern")) {
						final String mapTermUri = propJO.getString("propertyuri");
						if (mapTermUri.equals(termUri)) return true;
					}
				}
			}
		}
		return false;
	}
	
	
	

	
}
