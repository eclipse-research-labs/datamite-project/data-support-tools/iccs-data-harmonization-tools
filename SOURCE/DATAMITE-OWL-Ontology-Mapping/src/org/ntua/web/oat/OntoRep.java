package org.ntua.web.oat;

import static org.timchros.core.util.Checks.checkNotNullArg;

import java.io.File;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.log4j.Logger;
import org.timchros.core.log.messages.MemoryMessage;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Throable;


/**
 * A repository with <i>active</i> ontologies loaded.
 * 
 * @author Efthymios Chondrogiannis
 */
public class OntoRep {
	
	private static final Logger log = LogFactory.getLoggerForClass(OntoRep.class);
	
	private static final String datetimePattern = "yyyyMMddHHmmssSSS";
	private static final SimpleDateFormat datetimeFormater = new SimpleDateFormat(datetimePattern);
	
	private static OntoRep instance = null;
	
	public static OntoRep getInstance() {
		synchronized (OntoRep.class) {
			if (instance == null) {
				instance = new OntoRep();
			}
		}
		return instance;
	}
	
	/** @return A <b>unique ID</b> based on current {@link Date} and {@link SimpleDateFormat} pattern <code>"yyyyMMddHHmmssSSS"</code>.  */
	public static String produceSessionUID() {
		return datetimeFormater.format(new Date());
	}
	
	/** @return The <b>hash</b> of the given String producing a <b>hexadecimal</b> representation of its {@link MessageDigest} using <b>MD5</b> algorithm. */
	public static String produceHashForString(final String str) {
		try {
			final byte[] bytesOfMessage = str.getBytes("UTF-8");
			final MessageDigest md = MessageDigest.getInstance("MD5");
			final byte[] thedigest = md.digest(bytesOfMessage);
			final String digest = hexEncode(thedigest);
			return digest;
		} catch (Throwable t) {
			final String msg = "Error while producing hash code for string \"" + str + "\".";
			log.error(msg + "\n" + Throable.throwableAsString(t));
			throw new RuntimeException(msg, t);
		}
	}
	
	/** @return A String consisting of <b>hexadecimal</b> digits*/
	private static String hexEncode(byte[] aInput) {
		final StringBuilder result = new StringBuilder();
		char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		for (int idx = 0; idx < aInput.length; ++idx) {
			byte b = aInput[idx];
			result.append(digits[(b & 0xf0) >> 4]);
			result.append(digits[b & 0x0f]);
		}
		return result.toString();
	}
	
	/** @return A <b>unique ID</b> using both <b>Session ID</b> and name <b>Hash</b>. */
	public static String produceOntoModelUID(final String name) {
		return produceSessionUID() + produceHashForString(name);
	}
	
	/** A Map with "active" ontologies */
	private final Map<String, OntModel> ontoMap = new HashMap<String, OntModel>();
	
	/** Ensures we will not create an instance of this class */
	private OntoRep() {
		
	}
	
	public void clean() {
		ontoMap.clear();
	}
	
	public Map<String, OntModel> getOntoMap() {
		return this.ontoMap;
	}
	
	public void addOntoModelWithUID(final String ontoUID, final OntModel onto) {
		checkNotNullArg(ontoUID, "ontoUID", onto, "onto");
		cleanAndCheckRep();
		this.ontoMap.put(ontoUID, onto);
	}
	
	
	private static int HOURS_LIMIT = 2;
	private static int MAX_ACTIVE_ONTOLOGIES = 20;
	
	/** Clean {@link OntModel} being in our repository/memory */
	private synchronized void cleanAndCheckRep() {
		removeOntModelsLessThanMinutes(HOURS_LIMIT * 60);
		// Check Repository
		if (this.ontoMap.size() >= MAX_ACTIVE_ONTOLOGIES) {
			final MemoryMessage mm = new MemoryMessage();
			final float availableMemory = mm.getMaxMemory() - (mm.getTotalMemory() - mm.getFreeMemory());
			final String msg = "*** System is too busy ( " + this.ontoMap.size() + " ontologies are under process ) !!! " + mm.toParsableString() + " --> AvailableMemory: " + availableMemory + " MB";
			log.warn(msg);
			if (availableMemory < 100) { // 100MB
				removeOntModelsLessThanMinutes(30);
			}
		}
	}
	
	private void removeOntModelsLessThanMinutes(final int minutesLimit) {
		// Clean Repository
		final Map<String, OntModel> tmpOntoMap = new HashMap<String, OntModel>(); 
		for (Entry<String, OntModel> entry : ontoMap.entrySet()) {
			try {
				final Calendar cal1 = Calendar.getInstance();
				final Date date = datetimeFormater.parse(entry.getKey().substring(0, datetimePattern.length()));
				cal1.setTime(date);
				final Calendar cal2 = Calendar.getInstance();
				cal2.setTime(new Date());
				final long ms = cal2.getTimeInMillis() - cal1.getTimeInMillis();
				log.trace("ms: " + ms);
				final long minutes = ms / ( 1000 * 60 );
				// Remain in repository IF ontologies loaded in LESS than 6 hours
				if (minutes <= minutesLimit) {
					tmpOntoMap.put(entry.getKey(), entry.getValue());
				} else {
					log.warn("* Ontology with UID \"" + entry.getKey() + "\" removed from our repository(memory), since it has been uploaded before " + minutes + " minutes(s) - (elapsed time: " + minutes + "min )");
				}
			} catch (ParseException e) {
				final String msg = "Unexpected ParseException error...";
				log.error(msg + "\t" + Throable.throwableAsString(e));
				throw new RuntimeException(msg, e);
			}
		}
		ontoMap.clear();
		ontoMap.putAll(tmpOntoMap);
	}
	
	/** @return An ontology either from Repository or Loading from File based on the OntologyUID existing in Json Data provided */
	public synchronized OntModel getOntoModelByJsonStr(final String jsonStr) {
		checkNotNullArg(jsonStr, "jsonStr");
		final JSONObject json = JSONObject.fromObject(jsonStr);
		final String ontoUID = json.getString("uid");
		checkNotNullArg(ontoUID, "ontoUID");
		// Check in Memory DB
		if (!this.ontoMap.containsKey(ontoUID)) {
			try {
				final JSONObject mainJson = json.getJSONObject("main");
				final JSONArray importsJsonArray = json.getJSONArray("imports");
				
				final String mainOntPath = mainJson.getString("path");
				final List<Tuple2<String, String>> uriPathList = new ArrayList<Tuple2<String,String>>();
				final Iterator<?> it = importsJsonArray.iterator();
				while (it.hasNext()) {
					final JSONObject jsonObj = (JSONObject) it.next();
				 	if (jsonObj == null) continue;
				 	final String importUri = "" + jsonObj.getString("uri");
				 	final String importPath = "" + jsonObj.getString("path");
				 	if (importUri == null || importPath == null) continue;
				 	uriPathList.add(new Tuple2<String, String>(importUri, new File(importPath).toURI().toASCIIString()));
				}
				final OntModel ontModel = ServletUtil.readOntModel(mainOntPath, uriPathList);
				addOntoModelWithUID(ontoUID, ontModel);
				log.trace("Ontology succesfully Loaded based on JSON data provided and putted in our repository with UID \"" + ontoUID + "\". Follows JSON data given:\n" + jsonStr);
				log.info(new MemoryMessage().toParsableString());
			} catch(Throwable t) {
				final String msg = "Error while reading OntModel...";
				log.error(msg + "\n" + Throable.throwableAsString(t));
				throw new RuntimeException(msg, t);
			}
		}
		return this.ontoMap.get(ontoUID);
	}
	
	/** @return An ontology with the given unique ID ( OntologyUID = sessionID + OntoNameHash )*/
	public synchronized OntModel getOntoModelByUID(final String ontoUID) {
		checkNotNullArg(ontoUID, "ontoUID");
		if (this.ontoMap.containsKey(ontoUID)) {
			return this.ontoMap.get(ontoUID);
		} else {
			final String msg = "Repository do not contain an ontology with UID: \"" + ontoUID + "\"... Repository.size(): " + this.ontoMap.size();
			log.error(msg);
			throw new RuntimeException(msg);
		}
	}
	
	public String toString() {
		return OntoRep.class.getSimpleName() + " { ontoMap.size(): " + this.ontoMap.size() + " }";
	}
	
	/** For testing purposes... */
	public static void main(String[] args) {
		System.out.println(" >> OntoRep: Process STARTED at " + new Date() + "\n");
	
		OntoRep.getInstance().addOntoModelWithUID(produceOntoModelUID("aaa"), ModelFactory.createOntologyModel());
		
		OntoRep.getInstance().addOntoModelWithUID(produceOntoModelUID("bbb"), ModelFactory.createOntologyModel());
		
		System.out.println(OntoRep.getInstance());
		
		System.out.println("\n >> OntoRep: Process COMPLETED at " + new Date());
	}

}
