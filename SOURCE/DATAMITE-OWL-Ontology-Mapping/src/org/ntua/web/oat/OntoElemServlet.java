package org.ntua.web.oat;

import static org.ntua.web.oat.ServletUtil.toJsonArray;
import static org.ntua.web.oat.ServletUtil.toList;
import static org.ntua.web.oat.ServletUtil.getLabelOrLocalName;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.ontology.Ontology;
import org.apache.jena.ontology.Restriction;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.log4j.Logger;
import org.timchros.core.interfaces.I_Check;
import org.timchros.core.transformers.I_StringTransformer;
import org.timchros.core.util.Throable;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


/**
 * This Servlet is being used for retrieving the definition of an Ontological Element.
 * <p>
 * Also it is being used for retrieving all the classes/properties/individuals specified as flat lists.
 * 
 * @author Efthymios Chondrogiannis
 */
@WebServlet("/OntoElemServlet")
public class OntoElemServlet extends HttpServlet {
	
	private static final Logger log = LogFactory.getLoggerForClass(OntoElemServlet.class);
	
	private static final long serialVersionUID = 1L;

    public OntoElemServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/** Request parameters name: ontuid , uri , action */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	try {
		// SessionID
		final String sid = OntoRep.produceSessionUID();
		
		// Request Parameters provided
		final String divIndex = request.getParameter("div");
		final String jsonStr = request.getParameter("jsonStr");
		final String ontouid = (jsonStr != null) ? JSONObject.fromObject(jsonStr).getString("uid") : null;
		final String uri = request.getParameter("uri");
		final String action = request.getParameter("method");
		log.trace("OntoUID: " + ontouid + " , SID: " + sid + " :: REQUEST --> URI: " + uri + " , ACTION: \"" + action + "\"");
		
		// Check
		if (ontouid == null || uri == null || action == null) {
			log.error("NULL ARGUMENT [" + sid + "] :: OntoUID: " + ontouid + " , URI: " + uri + " , ACTION: \"" + action + "\"");
			return;
		}
		
		// Getting OntModel from Repository
		final OntModel ontModel = OntoRep.getInstance().getOntoModelByUID(ontouid);
		// Check
		if (ontModel == null) {
			log.error("NULL ONT-MODEL [" + sid + "] :: OntoUID: \"" + ontouid + "\" !!!");
			return;
		}
		
		// FIND DATA
		JSONObject json = new JSONObject();
		if (action.equalsIgnoreCase("GetElementDefinition")) {
			final OntResource ontResource = ontModel.getOntResource(uri);
			if ( ontResource.isClass()) {
				json = getOntClassDefinition(ontModel, uri);
			} else if (ontResource.isObjectProperty()) {
				json = getObjectPropertyDefinition(ontModel, uri);
			} else if (ontResource.isDatatypeProperty()) {
				json = getDatatypePropertyDefinition(ontModel, uri);
			} else if (ontResource.isIndividual()) {
				json = getIndividualDefinition(ontModel, uri);
			}
		} 
		else if ( action.equalsIgnoreCase("GetOntologyDefinition") ) {
			json = getOntologyDefinition(ontModel, uri);
		}
		else  if (action.equalsIgnoreCase("AllElements")) {
			json = getAllElements(ontModel);
		} else {
			log.warn("NOT SUPPORTED ACTION [" + sid + "] :: Action: \"" + action + "\".");
		}
		
		json.put("div", divIndex);
		
		// Response
		response.setContentType("application/json; charset=UTF-8");
		final PrintWriter pw = response.getWriter();
		pw.print(json.toString());
		pw.close();
		
		final String msg = (json.isEmpty()) ? "JSON is EMPTY !!! " : "JSON-LENGTH: " + json.toString().length();
		log.debug("OntoUID: " + ontouid + " , SID: " + sid +  " :: REQUEST --> URI: " + uri + " , ACTION: \"" + action + "\" , RESPONSE --> " + msg);
		log.trace("JSON \n" + json.toString(5, 5));
	} catch(Throwable tt) {
		final String msg = "Error when getting element... " + tt.getMessage();
		log.error(msg + "\n" + Throable.throwableAsString(tt));

		final JSONObject errorJsonObj = new JSONObject();
		String returnMsg = Throable.throwableAsSimpleOneLineString(tt);
		if (returnMsg != null && returnMsg.length() > 200) returnMsg = returnMsg.substring(0, 200) + " ...";
		errorJsonObj.put("error", returnMsg);
		response.setContentType("application/json; charset=UTF-8");
		final PrintWriter out = response.getWriter();
		out.print(errorJsonObj.toString());
		out.close();
	}
	}
	
	public synchronized JSONObject getAllElements(final OntModel ontModel) {
		final JSONObject json = new JSONObject();
		
		final List<OntClass> ontClassList = toList(ontModel.listClasses(), new I_Check<OntClass>() {
			@Override public boolean isTrue(OntClass ontClass) { return (ontClass.getURI() != null); }
		});
		final List<ObjectProperty> objPropList = toList(ontModel.listObjectProperties());
		final List<DatatypeProperty> dataPropList = toList(ontModel.listDatatypeProperties());
		final List<Individual> indivList = toList(ontModel.listIndividuals());
		
		json.put("classja", toJsonObjArray(ontClassList));
		json.put("objpropja", toJsonObjArray(objPropList));
		json.put("datapropja", toJsonObjArray(dataPropList));
		json.put("indivja", toJsonObjArray(indivList));
		return json;
	}
	
	
	// NOT USED
	private synchronized JSONObject getOntologyDefinition(final OntModel ontModel, String uri) {
		final JSONObject json = new JSONObject();
		
		final Ontology ontology = ontModel.getOntology(uri);
		if (ontology == null) return json;
		
		final String ontologyName = getLabelOrLocalName( ontology ); //ontology.getLabel(null);
		json.put("label", ontologyName);
		final String ontologyUri = ontology.getURI();
		json.put("uri", ontologyUri);
		final String ontologyComment = ontology.getComment(null);
		if (ontologyComment != null) json.put("comment", ontologyComment);
		
		return json;
	}
	
	private synchronized JSONObject getOntClassDefinition(final OntModel ontModel, String uri) {
		final JSONObject json = new JSONObject();
		
		final OntClass ontClass = ontModel.getOntClass(uri);
		if (ontClass==null) return json;
		
		final String ontClassUri = ontClass.getURI();
		final String ontClassLabel = ontClass.getLabel(null);
		final String ontClassName = (ontClassLabel != null) ? ontClassLabel : ontClass.getLocalName();
		
		final String ontClassComment = ServletUtil.fixUris( getOntoComments(ontModel, ontClass) );
		
		// Super-Classes
		// ALSO terms presented above.. some other bottom
//		final List<OntClass> superOntClassList = toList(ontClass.listSuperClasses());
//		Collections.sort(superOntClassList, new Comparator<OntClass>() {
//			@Override public int compare(OntClass o1, OntClass o2) {
//				if (o1.getURI()!= null) return -1;
//				if (o2.getURI()!= null) return +1;
//				if (o1.isRestriction() && o2.isRestriction()) {
//					Restriction r1 = o1.asRestriction();
//					Restriction r2 = o2.asRestriction();
//					//return getLabelOrLocalName( r1.getOnProperty() ).compareTo( getLabelOrLocalName( r2.getOnProperty() ) );
//					return r1.getOnProperty().getLocalName().compareTo( r2.getOnProperty().getLocalName() );
//				}
//				return 0;
//			}
//		});
		
		final List<OntClass> superOntClassList = new ArrayList<>(getSuperOntClassSet(ontClass));
		Collections.sort(superOntClassList, new Comparator<OntClass>() {
			@Override public int compare(OntClass o1, OntClass o2) {
				if (o1.getURI()!= null) return -1;
				if (o2.getURI()!= null) return +1;
				if (o1.isRestriction() && o2.isRestriction()) {
					Restriction r1 = o1.asRestriction();
					Restriction r2 = o2.asRestriction();
					if (r1.getOnProperty().getLocalName().equals("data-Belong-to-Visit")) return +1;
					if (r2.getOnProperty().getLocalName().equals("data-Belong-to-Visit")) return -1;
					return r1.getOnProperty().getLocalName().compareTo( r2.getOnProperty().getLocalName() );
				}
				return 0;
			}
		});
		
		final List<String> superOntClassDescrList = new ArrayList<String>();
		for (OntClass superOntClass : superOntClassList) {
			superOntClassDescrList.add(OntResourceUtil.getOntClassAsHtml(ontModel, superOntClass));
		}
		
//		// Inherited Super-Classes
//		final List<OntClass> inheritedSuperOntClassList = new ArrayList<OntClass>();
//		for (OntClass superOntClass : superOntClassList) {
//			if (superOntClass.getURI() != null) {
//				List<OntClass> supSupOntClassList = toList( superOntClass.listSuperClasses(false) );
//				for (OntClass supSupOntClass : supSupOntClassList) {
//					if (!inheritedSuperOntClassList.contains(supSupOntClass)) {
//						inheritedSuperOntClassList.add(supSupOntClass);
//					}
//				}
//			}
//		}
//		Collections.sort(inheritedSuperOntClassList, new Comparator<OntClass>() {
//			@Override public int compare(OntClass o1, OntClass o2) {
//				if (o1.getURI()!= null) return -1;
//				if (o2.getURI()!= null) return +1;
//				if (o1.isRestriction() && o2.isRestriction()) {
//					Restriction r1 = o1.asRestriction();
//					Restriction r2 = o2.asRestriction();
//					return getLabelOrLocalName( r1.getOnProperty() ).compareTo( getLabelOrLocalName( r2.getOnProperty() ) );
//				}
//				return 0;
//			}
//		});
		final List<String> superSuperOntClassDescrList = new ArrayList<String>();
//		for (OntClass inhSuperOntClass : inheritedSuperOntClassList) {
//			superSuperOntClassDescrList.add(OntResourceUtil.getOntClassAsHtml(ontModel, inhSuperOntClass));
//		}
		
		//Subclasses
		final List<OntClass> subOntClassList = toList(ontClass.listSubClasses(false));
		Collections.sort(subOntClassList, new Comparator<OntClass>() {
			@Override public int compare(OntClass o1, OntClass o2) {
				if (o1.getURI()!= null) return -1;
				if (o2.getURI()!= null) return +1;
				if (o1.isRestriction() && o2.isRestriction()) {
					Restriction r1 = o1.asRestriction();
					Restriction r2 = o2.asRestriction();
					return getLabelOrLocalName( r1.getOnProperty() ).compareTo( getLabelOrLocalName( r2.getOnProperty() ) );
				}
				return 0;
			}
		});
		final List<String> subOntClassDescrList = new ArrayList<String>();
		for (OntClass subOntClass : subOntClassList) {
			subOntClassDescrList.add(OntResourceUtil.getOntClassAsHtml(ontModel, subOntClass));
		}

		// Within the Range of Object Properties
		final List<ObjectProperty> objPropList = toList( ontModel.listObjectProperties() );
		final List<OntClass> superClassList = toList( ontClass.listSuperClasses(false) );
		final List<OntClass> rangeClassList = new ArrayList<OntClass>();
		rangeClassList.add(ontClass);
		for (OntClass cls : superClassList) {
			//TODO: Fix... Union - Intersection
			if (cls.getURI() == null) continue;
			rangeClassList.add(cls);
		}
		final List<ObjectProperty> inRangeObjPropList = new ArrayList<ObjectProperty>();
		for (ObjectProperty objProp : objPropList) {
			final OntResource rangeOntClassResource = objProp.getRange();
			if (rangeOntClassResource != null) {
				final OntClass rangeOntClass = rangeOntClassResource.asClass();
				if (rangeClassList.contains(rangeOntClass)) {
					inRangeObjPropList.add(objProp);
				}
			}
		}
		Collections.sort(inRangeObjPropList, new ServletUtil.OntResourceLocalNameComparator());
		final List<String> inRangeObjPropStrList = new ArrayList<String>();
		for (ObjectProperty inRangeObjProp : inRangeObjPropList) {
			inRangeObjPropStrList.add("<a href=\"" + inRangeObjProp.getURI() + "\">" + getLabelOrLocalName(inRangeObjProp) + "</a>");
		}
		
		// Instances
		final List<String> instanceList = toList( ontClass.listInstances(), new I_StringTransformer<OntResource>() {
			@Override public String asString(OntResource o) {
				return "<a href=\"" + o.getURI() + "\">" + getLabelOrLocalName(o) + "</a>";
			}
		});
		
		// TODO: Fix
		final List<String> disjointWithList = toList( ontClass.listDisjointWith(), new I_StringTransformer<OntClass>() {
			@Override public String asString(OntClass o) {
				return OntResourceUtil.getOntClassAsHtml(ontModel, o);
			}
		});
		
		final List<String> seeAlsoList = toList( ontClass.listSeeAlso(), new I_StringTransformer<RDFNode>() {
			@Override public String asString(RDFNode rdfNode) {
				return "" + rdfNode;
			}
		});
		
		final JSONArray otherprop = getOtherAnnotationProperties(ontModel, ontClass);
		
		json.put("uri", ontClassUri);
		json.put("label", ontClassName);
		json.put("comment", ontClassComment);
		json.put("superclasses", toJsonArray(superOntClassDescrList));
		json.put("inheritedsuperclasses", toJsonArray(superSuperOntClassDescrList));
		json.put("subclasses", toJsonArray(subOntClassDescrList));
		json.put("withintherange", toJsonArray(inRangeObjPropStrList));
		json.put("instances", toJsonArray(instanceList));
		json.put("disjointwith", toJsonArray(disjointWithList));
		if (!otherprop.isEmpty()) json.put("otherprop", otherprop);
		json.put("seealso", toJsonArray(seeAlsoList));
		
		return json;
	}
	
	private synchronized Set<OntClass> getSuperOntClassSet(final OntClass ontClass) {
		final Set<OntClass> ontClassSet = new HashSet<>();
		updateSuperOntClassSet(ontClass, ontClassSet);
		return ontClassSet;
	}
	
	private synchronized void updateSuperOntClassSet(final OntClass ontClass, final Set<OntClass> ontClassSet) {
		if (ontClass == null) return;
		if (!ontClass.isURIResource()) ontClassSet.add(ontClass);
		final List<OntClass> superOntClassList = toList(ontClass.listSuperClasses());
		for (OntClass superOntClass : superOntClassList) {
			updateSuperOntClassSet(superOntClass, ontClassSet);
		}
	}
	
	private synchronized String getOntoComments(final OntModel ontModel, final OntClass ontClass) {
		final StringBuilder sb = new StringBuilder();
		final String comment = ontClass.getComment(null);
		if (comment != null) {
			sb.append(comment.trim());
		}
		return sb.toString();
	}
	
	public synchronized JSONObject getObjectPropertyDefinition(final OntModel ontModel, String uri) {
		final JSONObject json = new JSONObject();
		
		final ObjectProperty objProperty = ontModel.getObjectProperty(uri);
		if (objProperty==null) return json;
		
		final String objPropUri = objProperty.getURI();
		final String objPropLabel = objProperty.getLabel(null);
		final String objPropName = (objPropLabel != null) ? objPropLabel : objProperty.getLocalName();
		final String objPropComment = ServletUtil.fixUris( objProperty.getComment(null) );
		
		final OntResource domainOntResource = objProperty.getDomain();
		final String domainOntResourceHtml = 
				(domainOntResource != null) 
					? OntResourceUtil.getOntClassAsHtml( ontModel, domainOntResource.as(OntClass.class) ) : null;
		
		final OntResource rangeOntResource = objProperty.getRange();
		final String rangeOntResourceHtml = 
				(rangeOntResource != null) 
					? OntResourceUtil.getOntClassAsHtml( ontModel, rangeOntResource.as(OntClass.class) )  : null;
		
		final List<String> seeAlsoList = toList( objProperty.listSeeAlso(), new I_StringTransformer<RDFNode>() {
			@Override public String asString(RDFNode rdfNode) {
				return "" + rdfNode;
			}
		});
		
		final JSONArray otherprop = getOtherAnnotationProperties(ontModel, objProperty);
		
		json.put("uri", objPropUri);
		json.put("label", objPropName);
		json.put("comment", objPropComment);
		json.put("domain", domainOntResourceHtml);
		json.put("range", rangeOntResourceHtml);
		if (!otherprop.isEmpty()) json.put("otherprop", otherprop);
		json.put("seealso", toJsonArray(seeAlsoList));
		
		return json;
	}

	
	private synchronized JSONObject getDatatypePropertyDefinition(final OntModel ontModel, String uri) {
		final JSONObject json = new JSONObject();
		
		final DatatypeProperty dataProperty = ontModel.getDatatypeProperty(uri);
		if (dataProperty==null) return json;
		
		final String dataPropUri = dataProperty.getURI();
		final String dataPropLabel = dataProperty.getLabel(null);
		final String dataPropName = (dataPropLabel != null) ? dataPropLabel : dataProperty.getLocalName();
		final String dataPropComment = ServletUtil.fixUris( dataProperty.getComment(null) );
		
		final OntResource domainOntResource = dataProperty.getDomain();
		final String domainOntResourceLink = 
				(domainOntResource != null) 
					? OntResourceUtil.getOntClassAsHtml( ontModel, domainOntResource.as(OntClass.class) ) : null;
		
		final OntResource rangeOntResource = dataProperty.getRange();
		final String rangeOntResourceLink = 
				(rangeOntResource != null) 
					? OntResourceUtil.getOntClassAsHtml( ontModel, rangeOntResource.as(OntClass.class) ) : null;
		
		final List<String> seeAlsoList = toList( dataProperty.listSeeAlso(), new I_StringTransformer<RDFNode>() {
			@Override public String asString(RDFNode rdfNode) {
				return "" + rdfNode;
			}
		});
		
		final JSONArray otherprop = getOtherAnnotationProperties(ontModel, dataProperty);
		
		json.put("uri", dataPropUri);
		json.put("label", dataPropName);
		json.put("comment", dataPropComment);
		json.put("domain", domainOntResourceLink);
		json.put("range", rangeOntResourceLink);
		if (!otherprop.isEmpty()) json.put("otherprop", otherprop);
		json.put("seealso", toJsonArray(seeAlsoList));
		
		return json;
	}
	
	public synchronized JSONObject getIndividualDefinition(final OntModel ontModel, String uri) {
		final JSONObject json = new JSONObject();
		
		final Individual individual = ontModel.getIndividual(uri);
		if (individual==null) return json;
		
		final String individualUri = individual.getURI();
		final String individualLabel = ServletUtil.getLabelOrLocalName(individual);
		final String individualName = (individualLabel != null) ? individualLabel : individual.getLocalName();
		final String individualComment = ServletUtil.fixUris( individual.getComment(null) );
	
		final List<Resource> belongToResourceList = toList( individual.listRDFTypes(true) );
		Collections.sort(belongToResourceList, new Comparator<Resource>() {
			@Override public int compare(Resource r1, Resource r2) {
				return r1.getLocalName().compareTo(r2.getLocalName());
			}
		});
		final List<String> belongToOntClassHtmlList = new ArrayList<String>();
		for (Resource resource : belongToResourceList) {
			if (resource.canAs(OntClass.class)) {
				belongToOntClassHtmlList.add( OntResourceUtil.getOntClassAsHtml(ontModel, resource.as(OntClass.class)) );
			}
		}
		
		final StmtIterator stmtIt = individual.listProperties();
		final List<Statement> stmtList = new ArrayList<Statement>();
		while (stmtIt.hasNext()) {
			Statement stmt = (Statement) stmtIt.next();
			if (stmt.getPredicate().getLocalName().equals("type")) continue;
			stmtList.add(stmt);
		}
		Collections.sort(stmtList, new Comparator<Statement>() {
			@Override public int compare(Statement s1, Statement s2) {
				return s1.getPredicate().getLocalName().compareTo(s2.getPredicate().getLocalName());
			}
		});
		
		final List<String> stmtDescList = new ArrayList<String>();
		for (Statement stmt : stmtList) {
			final String printPredicate =
				(stmt.getPredicate().canAs(OntResource.class))
					? ServletUtil.getLabelOrLocalName(stmt.getPredicate().as(OntResource.class))
					: stmt.getPredicate().getLocalName();
			stmtDescList.add(
				"<a href=\"" + stmt.getPredicate().getURI() + "\">" + printPredicate + "</a>" +
				" : " + 
				"<div class=\"stmtvalue\">" + ServletUtil.fixUris( stmt.getObject().asLiteral().getString() ) + "</div>"
			);
		}
		
		final List<String> seeAlsoList = toList( individual.listSeeAlso(), new I_StringTransformer<RDFNode>() {
			@Override public String asString(RDFNode rdfNode) {
				return "" + rdfNode;
			}
		});
		
		json.put("uri", individualUri);
		json.put("label", individualName);
		json.put("comment", individualComment);
		json.put("belongs", toJsonArray(belongToOntClassHtmlList));
		json.put("statements", toJsonArray(stmtDescList));
		json.put("seealso", toJsonArray(seeAlsoList));
		
		return json;
	}
	
	public static <T extends OntResource> JSONArray toJsonObjArray(List<T> list) {
		if (list == null || list.size() == 0) return null;
		final JSONArray jsonArray = new JSONArray();
		for (T ontResource : list) {	
			final JSONObject jsonObj = new JSONObject();
			jsonObj.put("value", getLabelOrLocalName(ontResource));
			jsonObj.put("uri", ontResource.getURI());
			jsonArray.add(jsonObj);
		}
		return jsonArray;
	}
	
	private static JSONArray getOtherAnnotationProperties(OntModel ontModel, OntResource ontResource) {
		final JSONArray otherprop = new JSONArray();
		final ExtendedIterator<AnnotationProperty> anExtIt = ontModel.listAnnotationProperties();
		while (anExtIt.hasNext()) {
			AnnotationProperty annotationProperty = (AnnotationProperty) anExtIt.next();
			final String label = annotationProperty.getLabel(null);
			final String name = (label != null) ? label : annotationProperty.getLocalName();
			if (name.equalsIgnoreCase("Parameter UID")) continue;
			// TODO: check
			if (name.equalsIgnoreCase("values-of-Parameter")) continue;
			final RDFNode rdfNode = ontResource.getPropertyValue(annotationProperty);
			
			if (rdfNode != null && rdfNode.isLiteral()) {
				final String literal = rdfNode.asLiteral().getString();
				final String value = literal;
				final JSONObject jo = new JSONObject();
				jo.put("name", name);
				jo.put("value", value);
				otherprop.add(jo);
			}
			if (rdfNode != null && rdfNode.isURIResource()) {
				final Resource resource = rdfNode.asResource();
				if (resource.canAs(OntClass.class)) {
					OntClass ontClass = resource.as(OntClass.class);
					final String value = "<a href=\"" + ontClass.getURI() + "\">" + getLabelOrLocalName(ontClass) + "</a>";;
					final JSONObject jo = new JSONObject();
					jo.put("name", name);
					jo.put("value", value);
					otherprop.add(jo);
				}
			}
		}
		return otherprop;
	}
	
}
