package org.ntua.web.oat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.jena.ontology.OntDocumentManager;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.Ontology;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.vocabulary.DC;
import org.apache.log4j.Logger;
import org.ntua.web.oat.xls.OntologyGenService;
import org.timchros.core.util.Throable;


/**
 * This Servlet is being used for uploading OWL files on server.
 * <p>
 * After uploading the OWL file, the servlet returns the main parameters of the ontology as a JSON string.
 * 
 * @author Efthymios Chondrogiannis
 */
@WebServlet("/OntoFileUploadServlet")
public class OntoFileUploadServlet extends HttpServlet {
	
	private static final Logger log = LogFactory.getLoggerForClass(OntoFileUploadServlet.class);
	
	public static final String HARMONICSS_REFERENCE_MODEL = "HarmonicSS-Reference-Model+Vocabularies-v.0.9.3.owl";
	
	private static String FOLDER = "./OAT-INPUT-ONTO";
	
	static {
		try {
		    final Properties prop  = new Properties();
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("oat.properties"));
			FOLDER = prop.getProperty("ontodir");
			log.info("Loaded from oat.properties FILE... FOLDER: " + FOLDER);
			
			final File folder = new File(FOLDER);
			if (!folder.exists()) {
				log.trace("Creating FOLDER(s) ... \"" + FOLDER + "\" ...\n");
				final boolean result = folder.mkdirs();
				if (!result) {
					log.error("Cannot create FOLDER(s) ..\"" + FOLDER + "\" !!!");
				}
			}
		} catch(Throwable t) {
			final String msg = "Unable to read \"oat.properties\" FILE...";
			log.error(msg + "\n" + Throable.throwableAsString(t));
			log.warn(msg + " FOLDER: " + FOLDER);
		}
	}
	
	private static final long serialVersionUID = 1L;
    
	private String tmp = null;
	
	private static final String fileNamePrefixPattern = "yyyyMMddHHmmssSSS";
	private static final SimpleDateFormat prefixFormater = new SimpleDateFormat(fileNamePrefixPattern);
	
    public OntoFileUploadServlet() {
        super();
    }
    
    public static String getFolder() {
    	return FOLDER;
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		final String sid = ServletUtil.getSessionID();
		
		response.setContentType("application/json; charset=UTF-8");
		final PrintWriter out = response.getWriter();

		boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);
		if (!isMultipartContent) {
			log.warn("Request is NOT MultipartContent ! Loading response from \"tmp\" local variable.");
			out.print(tmp);
			out.close();
			return;
		}

		final FileItemFactory factory = new DiskFileItemFactory();
		final ServletFileUpload upload = new ServletFileUpload(factory);
		try {
			@SuppressWarnings("unchecked")
			final List<FileItem> fields = upload.parseRequest(request);
			final Iterator<FileItem> it = fields.iterator();
			if (!it.hasNext()) {
				return;
			}
			final JSONArray jsonArray = new JSONArray();
			while (it.hasNext()) {
				final FileItem fileItem = it.next();
				final String name = fileItem.getFieldName();
				log.trace("SID: " + sid + " :: Hanlde element with name: \"" + name + "\" -- " + fileItem);
				
				if (name.startsWith("ontourl") && fileItem.getString() != null && !fileItem.getString().equals("")) {
					final String url = fileItem.getString();
					if (url == null || url.trim().equals("")) continue;
					final String urlTrim = url.trim();
					
					try {
						final String path;
						final String downFileName;
						final OntModel ontModel; //x
						if (urlTrim.equals("HarmonicSS-Reference-Model")) {
							downFileName = HARMONICSS_REFERENCE_MODEL;
							path = FOLDER + "/" + downFileName;
							ontModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
							ontModel.read(Thread.currentThread().getContextClassLoader().getResourceAsStream(downFileName), null);
							saveOntoToFile(ontModel, path);							
						} else {
							int slashIndex = url.lastIndexOf('/');
							if (slashIndex <= 0) throw new RuntimeException("Cannot Find File Name (i.e., slash) from URL: " + url);
							downFileName = "Onto-" + ServletUtil.getSessionID() + "-" + url.substring(slashIndex+1, url.length());
							final String filepath = FOLDER + "/" + downFileName;
							
							final String ontoContent = HttpRequest.getPageContent(urlTrim);
							FilesUtil.saveToFileUTF8(FOLDER, downFileName, ontoContent);
							
							log.info("SID: " + sid + " :: File downloaded from URL: " + urlTrim + " saved to FOLDER: " + FOLDER + " with NAME: " + downFileName);
							
							// IF THE FILE IS AN EXCEL DOCUMENT, CREATE THE ONTOLOGY 
							
							int dotIndex = filepath.lastIndexOf('.');
							if (dotIndex <= 0) throw new RuntimeException("Cannot Find File Name suffix ! (dot char)");
							final String suffix = filepath.substring(dotIndex + 1, filepath.length()).toLowerCase();
							
							if (suffix.equals("owl")) {
								path = filepath;
							} else if (suffix.equals("xls") || suffix.equals("xlsx")) {
								path = filepath + ".owl";
								OntologyGenService.generateOntology(filepath, path);
								log.info("SID: " + sid + " :: Ontology File Generated: \"" + path + "\".");
							} else {
								throw new RuntimeException("The uploaded file is not an OWL or XLS file.");
							}
							
							// Load ontology ignoring imported ones in order to find the URIs of ontologies imported
							OntDocumentManager.getInstance().setProcessImports(false);
							ontModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
							// ontModel.read(new FileInputStream(new File(path)), null);
							ontModel.read( new InputStreamReader(new FileInputStream(new File(path)), Charset.forName("UTF-8")), null);
						}
						
						
						/* Creating JSON Response - START */
						
						final JSONObject ontJson = new JSONObject();
						
						final String ontoUID = OntoRep.produceOntoModelUID(downFileName);
						ontJson.put("uid", ontoUID);
						ontJson.put("path", path);
						final Ontology ontology = ServletUtil.getOntology(ontModel);
						final String ontLabel = ontology.getLabel(null);
						if (ontLabel != null) {
							ontJson.put("label", ontLabel);
						} else {
							ontJson.put("label", fileItem.getName());
						}
						final String ontUri = ontology.getURI();
						ontJson.put("uri", ontUri);
						final String comment = ontology.getComment(null);
						if (comment != null) ontJson.put("comment", ServletUtil.fixUris(comment));
						final String ontoDate = ServletUtil.getOntoPropertyValue(ontology, DC.date.getURI());
						if (ontoDate != null) ontJson.put("date", ontoDate);
						final String creator = ServletUtil.getOntoPropertyValue(ontology, DC.creator.getURI());
						if (creator != null) ontJson.put("creator", creator);
						final String ontVersionInfo = ontology.getVersionInfo();
						if (ontVersionInfo != null) ontJson.put("versioninfo", ontVersionInfo);
						final JSONArray importsJArray = new JSONArray();
						final List<String> importUriList = ServletUtil.getImports(ontModel);
						for (String uri : importUriList) {
							importsJArray.add(uri);
						}
						ontJson.put("imports", importsJArray);
						
						jsonArray.add(ontJson);
						
						/* Creating JSON Response - END */
						log.info("SID: " + sid + " :: Ontology File DOWNLOADED from URL: \"" + urlTrim + "\" , Imports-size: " + importsJArray.size());
						log.trace("SID: " + sid + " , JSON:\n" + ontJson.toString(5,5));
					} catch (Throwable t) {
						final String emsg = "SID: " + sid + " :: PROBLEM :: Unable to DOWNLOAD/PARSE Ontology File from URL \"" + urlTrim + "\"";
						log.error(emsg + "\n" + Throable.throwableAsString(t));
						
						final JSONObject errorJsonObj = new JSONObject();
						String returnMsg = Throable.throwableAsSimpleOneLineString(t);
						if (returnMsg != null && returnMsg.length() > 200) returnMsg = returnMsg.substring(0, 200) + " ...";
						errorJsonObj.put("error", returnMsg);
						response.setContentType("application/json; charset=UTF-8");
						tmp = errorJsonObj.toString();
						out.print(errorJsonObj.toString());
						out.close();
						return;
					}
				}
				if (name.startsWith("ontofile") && fileItem.getName() != null && !fileItem.getName().equals("")) {
					//log.info("SID: " + sid + " :: fileItem.getName(): \"" + fileItem.getName() + "\".");
					if (fileItem == null || fileItem.getName() == null) continue;
					final String fnamestr = fileItem.getName();
					final int index = Math.max(fnamestr.lastIndexOf('/'), fnamestr.lastIndexOf('\\'));
					final String fname = (index > 0) ? fnamestr.substring(index + 1, fnamestr.length()): fnamestr; //name;
					
					final String newFileName = prefixFormater.format(new Date()) + "-" + fname;
					final String uploadFilePath = FOLDER + "/" + newFileName;
					final File uploadedFile = new File(uploadFilePath);
					try {
						fileItem.write(uploadedFile);
						final String filepath = uploadedFile.getAbsolutePath();
						
						// IF THE FILE IS AN EXCEL DOCUMENT, CREATE THE ONTOLOGY 
						
						int dotIndex = filepath.lastIndexOf('.');
						if (dotIndex <= 0) throw new RuntimeException("Cannot Find File Name suffix !");
						final String suffix = filepath.substring(dotIndex + 1, filepath.length()).toLowerCase();
						
						final String path;
						if (suffix.equals("owl")) {
							path = filepath;
						} else if (suffix.equals("xls") || suffix.equals("xlsx")) {
							path = filepath + ".owl";
							OntologyGenService.generateOntology(filepath, path);
							log.info("SID: " + sid + " :: Ontology File Generated: \"" + path + "\".");
						} else {
							throw new RuntimeException("The uploaded file is not an OWL or XLS file. File name suffix: " + suffix);
						}
						
						// Load ontology ignoring imported ones in order to find the URIs of ontologies imported
						OntDocumentManager.getInstance().setProcessImports(false);
						final OntModel ontModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
						//ontModel.read(new FileInputStream(new File(path)), null);
						ontModel.read( new InputStreamReader(new FileInputStream(new File(path)), Charset.forName("UTF-8")), null);
						
						
						/* Creating JSON Response - START */
						
						final JSONObject ontJson = new JSONObject();
						
						final String ontoUID = OntoRep.produceOntoModelUID(fileItem.getName());
						ontJson.put("uid", ontoUID);
						ontJson.put("path", path);
						final Ontology ontology = ServletUtil.getOntology(ontModel);
						final String ontLabel = ontology.getLabel(null);
						if (ontLabel != null) {
							ontJson.put("label", ontLabel);
						} else {
							ontJson.put("label", fileItem.getName());
						}
						final String ontUri = ontology.getURI();
						ontJson.put("uri", ontUri);
						final String comment = ontology.getComment(null);
						if (comment != null) ontJson.put("comment", ServletUtil.fixUris(comment));
						final String ontoDate = ServletUtil.getOntoPropertyValue(ontology, DC.date.getURI());
						if (ontoDate != null) ontJson.put("date", ontoDate);
						final String creator = ServletUtil.getOntoPropertyValue(ontology, DC.creator.getURI());
						if (creator != null) ontJson.put("creator", creator);
						final String ontVersionInfo = ontology.getVersionInfo();
						if (ontVersionInfo != null) ontJson.put("versioninfo", ontVersionInfo);
						final JSONArray importsJArray = new JSONArray();
						final List<String> importUriList = ServletUtil.getImports(ontModel);
						for (String uri : importUriList) {
							importsJArray.add(uri);
						}
						ontJson.put("imports", importsJArray);
						
						jsonArray.add(ontJson);
						
						/* Creating JSON Response - END */
						log.info("SID: " + sid + " :: Ontology File: \"" + uploadedFile.getName() + "\" , Imports-size: " + importsJArray.size());
						log.trace("SID: " + sid + " , JSON:\n" + ontJson.toString(5,5));
					} catch (Throwable t) {
						final String emsg = "SID: " + sid + " :: PROBLEM :: Unable to UPLOAD/PARSE File with name \"" + fileItem.getName() + "\"";
						log.error(emsg + "\n" + Throable.throwableAsString(t));

						final JSONObject errorJsonObj = new JSONObject();
						String returnMsg = Throable.throwableAsSimpleOneLineString(t);
						if (returnMsg != null && returnMsg.length() > 200) returnMsg = returnMsg.substring(0, 200) + " ...";
						errorJsonObj.put("error", returnMsg);
						response.setContentType("application/json; charset=UTF-8");
						tmp = errorJsonObj.toString();
						out.print(errorJsonObj.toString());
						out.close();
						return;
					}
				}
			}
			
			final JSONObject json = new JSONObject();
			json.put("jsonarray", jsonArray);
			log.debug("SID: " + sid + " :: " + jsonArray.size() + " ontology FILE provided (uploaded/downloaded) !");
			tmp = json.toString();
			out.print(json.toString());
			out.close();
		} catch (Throwable tt) {
			final String msg = "Error when providing file(s)... " + tt.getMessage();
			log.error(msg + "\n" + Throable.throwableAsString(tt));

			final JSONObject errorJsonObj = new JSONObject();
			String returnMsg = Throable.throwableAsSimpleOneLineString(tt);
			if (returnMsg != null && returnMsg.length() > 200) returnMsg = returnMsg.substring(0, 200) + " ...";
			errorJsonObj.put("error", returnMsg);
			response.setContentType("application/json; charset=UTF-8");
			tmp = errorJsonObj.toString();
			out.print(errorJsonObj.toString());
			out.close();
		}
	}

	private static void saveOntoToFile(OntModel ontModel, String path) throws IOException {
		final FileOutputStream fos = new FileOutputStream( new File(path) );
		fos.write("<?xml version=\"1.0\"?>\n".getBytes());
		ontModel.write(fos, "RDF/XML");
		fos.flush();
		fos.close();
	}
}
