package org.ntua.web.oat;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Resource;
import org.apache.log4j.Logger;
import org.ntua.web.oat.pat.AbstractElementPatternRep;
import org.ntua.web.oat.pat.ElementPattern;
import org.timchros.core.util.Checks;
import org.timchros.core.util.Throable;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * This Servlet is being used for exporting the correspondences specified in the desirable format.
 * 
 * @author Efthymios Chondrogiannis
 */
@WebServlet("/MappingsExportServlet")
public class MappingsExportServlet extends HttpServlet {
	
	private static final Logger log = LogFactory.getLoggerForClass(MappingsExportServlet.class);
	
	private static final long serialVersionUID = 1L;

	private static final String TAB = "\t";
	private static final String NL = "\n";
	
    public MappingsExportServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String sid = ServletUtil.getSessionID();
		final String formatId = request.getParameter("format");
		final String origin = request.getParameter("origin");
		final String ontologiesStr = request.getParameter("ontologies");
		final String commentsStr = request.getParameter("ontoMapComments");
		final String correspondencesStr = request.getParameter("correspondences");
		
		log.debug("SID: " + sid + " :: FORMAT: " + formatId + " ... EXPORT MAPPING");
		log.trace("ONTOLOGIES-STR: " + ontologiesStr);
		log.trace("CORRESPONDENCES-STR: " + correspondencesStr);
		
		if (ontologiesStr == null) log.warn("ontologiesStr is Null !!!" );
		if (correspondencesStr == null) log.warn("correspondencesStr is null !!!" );
		
		// TODO: CHECH IF WE SHOULD USE THE FOLLOWING ONES
		// I THINK IT HAS BEEN SOLVED
		 final JSONObject ontologies = JSONObject.fromObject(new String(ontologiesStr.getBytes(), "UTF-8"));
		 final JSONObject correspondences = JSONObject.fromObject(new String(correspondencesStr.getBytes(), "UTF-8"));
//		final JSONObject ontologies = JSONObject.fromObject(ontologiesStr);
//		final JSONObject correspondences = JSONObject.fromObject(correspondencesStr);
		
		response.setContentType("text/plain; charset=UTF-8");
		final PrintWriter pw = response.getWriter();
		
		try {
			if ("1".equals(formatId)) { 
				// JSON
				response.setHeader("Content-disposition", "attachment; filename=Mapping.json");
				pw.print( mappingToJSON(origin, ontologies, correspondences, commentsStr) );
			} else if ("2".equals(formatId)) { 
				// XML - EDOAL
				response.setHeader("Content-disposition", "attachment; filename=Mapping.xml");
				pw.print( mappingToEDOAL(origin, ontologies, correspondences, commentsStr) );
			} else if ("3".equals(formatId)) { 
				// HTML
				response.setHeader("Content-disposition", "attachment; filename=Mapping.html");
				pw.print( mappingToHTML(origin, ontologies, correspondences, commentsStr) );
			} else {
				response.setHeader("Content-disposition", "attachment; filename=FormatError.txt");
				pw.print("None Exprot-Format specified !!!");
			}
		} catch(Throwable t) {
			final String msg = "Unexpected error when exporting mappings in the format provided...";
			log.error(msg + "\n" + t);
			response.setHeader("Content-disposition", "attachment; filename=ProcessingError.txt");
			pw.print(msg + "\n\n" + Throable.throwableAsSimpleMultiLineString(t));
		}
		
		pw.close();
		
		log.info("SID: " + sid + " :: FORMAT: " + formatId + " ... EXPORT MAPPING Successfully Provided for Ontologies given !");
	}
	
	private String mappingToJSON(final String origin, final JSONObject ontologies,final JSONObject correspondences, final String comments) {
		final JSONObject mappingJson = new JSONObject();

		// Find Cohort OntModel
		final String srcOntoUid = (ontologies != null && ontologies.has("ontA")) ? ontologies.getJSONObject("ontA").getString("uid") : null;
		if (srcOntoUid == null || srcOntoUid.trim().equals("")) 
			throw new RuntimeException("Cannot find Cohort Ontology UID !");
		
		final OntModel srcOntModel = OntoRep.getInstance().getOntoModelByUID(srcOntoUid);
		if (srcOntModel == null) 
			throw new RuntimeException("Cannot Find Cohort Ontology with UID: \"" + srcOntoUid + "\"");
		
		// Field Value Annotation Property
		final String paramRangeAnnoPropUri = "http://www.semanticweb.org/ntua/iccs/harmonicss/cohort#parameter-Value";
		final AnnotationProperty paramRangeAnnoProp = srcOntModel.getAnnotationProperty(paramRangeAnnoPropUri);
		
		// Update
		final JSONArray jsonArray = correspondences.getJSONArray("jsonarray");
		for (int i = 0; i < jsonArray.size(); i++) {
			final JSONObject mrJson = jsonArray.getJSONObject(i);
			final JSONObject entity1Json = mrJson.getJSONObject("entity1");
			final String pid = entity1Json.getString("pid");
			if (pid.equals("SimplePropertyPattern")) {
				final String paramUri = entity1Json.getString("propertyuri");
				final DatatypeProperty dataProp = srcOntModel.getDatatypeProperty(paramUri);
				final Resource resource = dataProp.getPropertyResourceValue(paramRangeAnnoProp);
				if (resource != null) {
					final String valuerange = resource.toString().trim();
					if (!valuerange.equals("")) {
						entity1Json.put("valuerange", valuerange);
					}
				}				
			} else if (pid.equals("PropertiesCollectionPattern")) {
				final JSONArray entity1propJA = entity1Json.getJSONArray("proparray");
				for (int j = 0; j < entity1propJA.size(); j++) {
					final JSONObject e1propJson = entity1propJA.getJSONObject(j);
					final String paramUri = e1propJson.getString("propertyuri");
					final DatatypeProperty dataProp = srcOntModel.getDatatypeProperty(paramUri);
					final Resource resource = dataProp.getPropertyResourceValue(paramRangeAnnoProp);
					if (resource != null) {
						final String valuerange = resource.toString().trim();
						if (!valuerange.equals("")) {
							e1propJson.put("valuerange", valuerange);
						}
					}
				}
			}
		} // END OF Mapping Rules Loop
		
		// Return
		mappingJson.put("ontologies", ontologies);
		mappingJson.put("origin", origin.replaceAll("\\n+", " ").replaceAll("\\s+", " "));
		mappingJson.put("comments", comments.replaceAll("\\n+", " ").replaceAll("\\s+", " "));
		mappingJson.put("correspondences", correspondences);
		
		return mappingJson.toString(5);
	}

	private String mappingToEDOAL(final String maprulesOrigin, final JSONObject ontologies,final JSONObject correspondences, final String mappingComments) {
		Checks.checkNotNullArg(ontologies, "ontologies", correspondences, "correspondences");
		
		final JSONObject ontAjson = ontologies.getJSONObject("ontA");
		final String ontoAuri = ontAjson.getString("uri");
		final JSONObject ontBjson = ontologies.getJSONObject("ontB");
		final String ontoBuri = ontBjson.getString("uri");
		final JSONArray jsonArray = correspondences.getJSONArray("jsonarray");
		
		final StringBuilder sb = new StringBuilder();
		
		sb.append("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" + NL + NL);
		
		final ElementPattern xmlIntroductionPattern = new ElementPattern(new String(AbstractElementPatternRep.XmlIntroductionPattern));
		xmlIntroductionPattern.setVarValue("OntoAuri", ontoAuri + "#");
		xmlIntroductionPattern.setVarValue("OntoBuri", ontoBuri + "#");
		sb.append(xmlIntroductionPattern.toString() + NL + NL);
		
		sb.append("<!-- This File has been automatically generated by Ontologies Alignment Tool on " + new Date() + " . -->" + NL + NL);
		
		final ElementPattern rdfIntroductionPattern = new ElementPattern(new String(AbstractElementPatternRep.RdfIntroductionPattern));
		rdfIntroductionPattern.setVarValue("OntoAuri", ontoAuri + "#");
		rdfIntroductionPattern.setVarValue("OntoBuri", ontoBuri + "#");
		sb.append(rdfIntroductionPattern.toString() + NL + NL);
		
		sb.append("  <Alignment rdf:about=\"http://www.semanticweb.org/ontologies/CorrespondencePatterns/\">" + NL + NL);
		
		final ElementPattern alignmentIntroductionPattern = new ElementPattern(new String(AbstractElementPatternRep.AlignmentIntroductionPattern));
		alignmentIntroductionPattern.setVarValue("creationdate", "" + new Date());
		alignmentIntroductionPattern.setVarValue("OntoAuri", ontoAuri);
		alignmentIntroductionPattern.setVarValue("OntoBuri", ontoBuri);
		sb.append(alignmentIntroductionPattern.toString() + NL + NL);
		
		sb.append(TAB + "<!-- The " + jsonArray.size() + " Mapping Rule(s) specified by: " + maprulesOrigin + " -->" + NL + NL);
		
		if (mappingComments != null && !mappingComments.trim().equals("")) {
			sb.append(TAB + "<!-- " + mappingComments.replaceAll("\\n+", " ").replaceAll("\\s+", " ") + " -->" + NL + NL);
		}
		
		for (int i = 0; i < jsonArray.size(); i++) {
			final JSONObject json = jsonArray.getJSONObject(i);
			final JSONObject entity1Json = json.getJSONObject("entity1");
			final JSONObject entity2Json = json.getJSONObject("entity2");
			final JSONObject directTransformationJson = 
				(json.has("directTransformation")) ? json.getJSONObject("directTransformation") : null;
			final JSONObject inverseTransformationJson = 
				(json.has("inverseTransformation")) ? json.getJSONObject("inverseTransformation") : null;	
			final String relation = 
				(json.has("relation")) ? json.getString("relation") : null;
			final String direction = 
				(json.has("direction")) ? json.getString("direction") : null;
			final String comments = 
				(json.has("comments")) ? json.getString("comments") : null;
			
			final String confidenceValue = 
						(json.has("similarity")) ? json.getString("similarity") : null;	
			
			final String originStr =  (confidenceValue != null) 
				? TAB + "<!-- MAPPING RULE created by ACCEPTING the SUGGESTED ONE with CONFIDENCE VALUE at the time of acceptance " + confidenceValue + " -->" + NL
				: TAB + "<!-- MAPPING RULE has been MANUALLY SPECIFIED  -->" + NL;				
						
			String commentsStr = "";
			if (comments != null && !comments.trim().equals("")) {
				commentsStr = TAB + "<!-- Description: " + comments.replaceAll("\\n+", " ").replaceAll("\\s+", " ") + " -->" + NL;
			}
			
			String directionStr = "";
			if (direction != null && !direction.trim().equals("")) {
				directionStr = TAB + "<!-- Direction: " + direction + " -->" + NL;
			}
			
			String transformationStr = "";
			if ( (directTransformationJson != null && !directTransformationJson.isNullObject()) || (inverseTransformationJson != null && !inverseTransformationJson.isNullObject())) {
				transformationStr += TAB + TAB + TAB + "<edoal:Transformation edoal:type=\"o-\">" + NL;
				if (directTransformationJson != null && !directTransformationJson.isNullObject()) {
					log.trace("directTransformationJson: \"" + directTransformationJson + "\"");
					transformationStr +=
						TAB + TAB + TAB + TAB + "<edoal:entity1>" + NL +
						createTransformationXml(directTransformationJson, TAB+TAB+TAB+TAB+TAB) + NL +
						TAB + TAB + TAB + TAB + "</edoal:entity1>" + NL;
				}
				if (inverseTransformationJson != null && !inverseTransformationJson.isNullObject()) {
					log.trace("inverseTransformationJson: \"" + inverseTransformationJson + "\"");
					transformationStr +=
						TAB + TAB + TAB + TAB + "<edoal:entity2>" + NL +
						createTransformationXml(inverseTransformationJson, TAB+TAB+TAB+TAB+TAB) +  NL +
						TAB + TAB + TAB + TAB + "</edoal:entity2>" + NL;
				}
				transformationStr += TAB + TAB + TAB + "</edoal:Transformation>" + NL;
			}
			
			sb.append(
				commentsStr + directionStr + originStr +
				TAB + "<map>" + NL +
				TAB + TAB + "<Cell rdf:about=\"MappingRule_" + (i + 1) + "\">" + NL +
				TAB + TAB + TAB + "<entity1>" + NL +
				createEntityXml(entity1Json, TAB+TAB+TAB+TAB) + NL +
				TAB + TAB + TAB + "</entity1>" + NL +
				TAB + TAB + TAB + "<entity2>" + NL +
				createEntityXml(entity2Json, TAB+TAB+TAB+TAB) + NL +
				TAB + TAB + TAB + "</entity2>" + NL +
				transformationStr +
				TAB + TAB + TAB + "<measure rdf:datatype=\"&xsd;float\">" + ((maprulesOrigin.equalsIgnoreCase("system")) ? confidenceValue : "1.0") + "</measure>" + NL +
				TAB + TAB + TAB + "<relation>" + relation + "</relation>" + NL +
				TAB + TAB + "</Cell>" + NL +
				TAB + "</map>" + NL + NL
			);
		}
		
		sb.append("  </Alignment>" + NL);
		sb.append("</rdf:RDF>");
		
		return sb.toString();
	}
	
	private String createEntityXml(JSONObject entityJson, String prefix) {
		final String pid = entityJson.getString("pid");
		
		/*
		 * Class Patterns Instance
		 */
		
		if ("SimpleClassPattern".equals(pid)) {
			final String classUri = entityJson.getString("classuri");
			final String className = entityJson.getString("classname");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.SimpleClassPattern) );
			pattern.setVarValue("classuri", classUri);
			pattern.setVarValue("classname", className);
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("ClassUnionPattern".equals(pid) || "ClassIntersectionPattern".equals(pid)) {
			final JSONArray classJsonArray = entityJson.getJSONArray("classarray");
			
			final ElementPattern pattern = 
				("ClassUnionPattern".equals(pid)) 
					? new ElementPattern( new String(AbstractElementPatternRep.UnionClassPattern) )
					: new ElementPattern( new String(AbstractElementPatternRep.IntersectionClassPattern) );
			
			final StringBuilder sb = new StringBuilder();
			final String classListPrefix = pattern.getVarPrefix("classlist");
			final List<Object> classList = jsonArrayToList(classJsonArray);
			final Object[] classArray = classList.toArray(new Object[classList.size()]);
			for (int i = 0; i < classArray.length; i++) {
				JSONObject jsonObject = (JSONObject) classArray[i];
				sb.append(createEntityXml( jsonObject, classListPrefix));
				if (i < classArray.length - 1) {
					sb.append(NL);
				}
			}
			pattern.setVarValue("classlist", sb.toString());
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("ClassByAttributeValueRestrictionPattern".equals(pid) || "ClassByAttributeOccurencePattern".equals(pid)) {
			final JSONObject classJson = entityJson.getJSONObject("cls");
			final JSONObject propertyJson = entityJson.getJSONObject("property");
			final String restrictionUri = entityJson.getString("restrictionuri");
			final String restrictionDesc = entityJson.getString("restrictiondesc");
			final String restrictionValue = entityJson.getString("restrictionvalue");
			
			final String restrictionDescStr = 
				(restrictionDesc == null || restrictionDesc.trim().equals("")) 
					? "Non description provided !"
					: restrictionDesc.replaceAll("\\n", " ") ;
			
			final ElementPattern pattern = 
				("ClassByAttributeValueRestrictionPattern".equals(pid)) 	
					? new ElementPattern( new String(AbstractElementPatternRep.ClassByAttributeValueRestrictionPattern) )
					: new ElementPattern( new String(AbstractElementPatternRep.ClassByAttributeOccurenceNumberPattern) );
					
			final String classPrefix = pattern.getVarPrefix("class");
			pattern.setVarValue("class", createEntityXml(classJson, classPrefix));
			final String propertyPrefix = pattern.getVarPrefix("property");
			pattern.setVarValue("property", createEntityXml(propertyJson, propertyPrefix));
			pattern.setVarValue("comparatoruri", restrictionUri);
			pattern.setVarValue("description", restrictionDescStr);
			pattern.setVarValue("value", restrictionValue);
			pattern.addPrefix(prefix);
			return pattern.toString();		
		}
		
		if ("ClassWithPropertiesRestrictionPattern".equals(pid)) {
			final JSONObject classJson = entityJson.getJSONObject("cls");
			final JSONArray propJsonArray = entityJson.getJSONArray("proparray");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.ClassWithPropertiesRestrictionPattern) );
			final String classPrefix = pattern.getVarPrefix("class");
			pattern.setVarValue("class", createEntityXml(classJson, classPrefix));
			pattern.addPrefix(prefix);
			final StringBuilder sb = new StringBuilder();
			final String propListPrefix = pattern.getVarPrefix("proplist");
			final List<Object> propList = jsonArrayToList(propJsonArray);
			final Object[] propArray = propList.toArray(new Object[propList.size()]);
			for (int i = 0; i < propArray.length; i++) {
				final JSONObject jsonObject = (JSONObject) propArray[i];
				sb.append(createEntityXml( jsonObject, propListPrefix));
				if (i < propArray.length - 1) {
					sb.append(NL);
				}
			}
			pattern.setVarValue("proplist", sb.toString());
			pattern.addPrefix(prefix);
			
			
			return pattern.toString();		
		}
		
		/*
		 * Relation Patterns Instance
		 */
		
		if ("SimpleRelationPattern".equals(pid)) {
			final String relationUri = entityJson.getString("relationuri");
			final String relationName = entityJson.getString("relationname");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.SimpleRelationPattern) );
			pattern.setVarValue("relationuri", relationUri);
			pattern.setVarValue("relationname", relationName);
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("InverseRelationPattern".equals(pid)) {
			final JSONObject relationJson = entityJson.getJSONObject("relation");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.InverseRelationPattern) );
			final String relationPrefix = pattern.getVarPrefix("relation");
			pattern.setVarValue("relation", createEntityXml(relationJson, relationPrefix));
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("RelationDomainRestrictionPattern".equals(pid)) {
			final JSONObject relationJson = entityJson.getJSONObject("relation");
			final JSONObject domainClassJson = entityJson.getJSONObject("domainclass");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.RelationDomainRestrictionPattern) );
			final String relationPrefix = pattern.getVarPrefix("relation");
			pattern.setVarValue("relation", createEntityXml(relationJson, relationPrefix));
			final String classPrefix = pattern.getVarPrefix("class");
			pattern.setVarValue("class", createEntityXml(domainClassJson, classPrefix));
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("RelationRangeRestrictionPattern".equals(pid)) {
			final JSONObject relationJson = entityJson.getJSONObject("relation");
			final JSONObject rangeClassJson = entityJson.getJSONObject("rangeclass");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.RelationRangeRestrictionPattern) );
			final String relationPrefix = pattern.getVarPrefix("relation");
			pattern.setVarValue("relation", createEntityXml(relationJson, relationPrefix));
			final String classPrefix = pattern.getVarPrefix("class");
			pattern.setVarValue("class", createEntityXml(rangeClassJson, classPrefix));
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("RelationPathPattern".equals(pid)) {
			final JSONArray relJsonArray = entityJson.getJSONArray("relationarray");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.RelationPathPattern) );
			final StringBuilder sb = new StringBuilder();
			final String relListPrefix = pattern.getVarPrefix("rellist");
			final List<Object> relList = jsonArrayToList(relJsonArray);
			final Object[] propArray = relList.toArray(new Object[relList.size()]);
			for (int i = 0; i < propArray.length; i++) {
				final JSONObject jsonObject = (JSONObject) propArray[i];
				sb.append(createEntityXml( jsonObject, relListPrefix));
				if (i < propArray.length - 1) {
					sb.append(NL);
				}
			}
			pattern.setVarValue("rellist", sb.toString());
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("RelationDomainRangeRestrictionPattern".equals(pid)) {
			final JSONObject relationJson = entityJson.getJSONObject("relation");
			final JSONObject domainClassJson = entityJson.getJSONObject("domainclass");
			final JSONObject rangeClassJson = entityJson.getJSONObject("rangeclass");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.RelationDomainRangeRestrictionPattern) );
			final String relationPrefix = pattern.getVarPrefix("relation");
			pattern.setVarValue("relation", createEntityXml(relationJson, relationPrefix));
			final String domainClassPrefix = pattern.getVarPrefix("domain-class");
			pattern.setVarValue("domain-class", createEntityXml(domainClassJson, domainClassPrefix));
			final String rangeClassPrefix = pattern.getVarPrefix("range-class");
			pattern.setVarValue("range-class", createEntityXml(rangeClassJson, rangeClassPrefix));
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		/*
		 * Property Patterns Instance
		 */
		
		if ("SimplePropertyPattern".equals(pid)) {
			final String propertyUri = entityJson.getString("propertyuri");
			final String propertyName = entityJson.getString("propertyname");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.SimplePropertyPattern) );
			pattern.setVarValue("propertyuri", propertyUri);
			pattern.setVarValue("propertyname", propertyName);
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("PropertyDomainRestrictionPattern".equals(pid)) {
			final JSONObject propertyJson = entityJson.getJSONObject("property");
			final JSONObject domainClassJson = entityJson.getJSONObject("domainclass");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.PropertyDomainRestrictionPattern) );
			final String propertyPrefix = pattern.getVarPrefix("property");
			pattern.setVarValue("property", createEntityXml(propertyJson, propertyPrefix));
			final String classPrefix = pattern.getVarPrefix("class");
			pattern.setVarValue("class", createEntityXml(domainClassJson, classPrefix));
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("PropertyRangeRestrictionPattern".equals(pid)) {
			final JSONObject propertyJson = entityJson.getJSONObject("property");
			final String rangeDatatype = entityJson.getString("rangedatatype");		
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.PropertyDatatypeRestrictionPattern) );
			final String propertyPrefix = pattern.getVarPrefix("property");
			pattern.setVarValue("property", createEntityXml(propertyJson, propertyPrefix));
			pattern.setVarValue("datatype", rangeDatatype);
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("PropertyValueRestrictionPattern".equals(pid)) {
			final JSONObject propertyJson = entityJson.getJSONObject("property");
			final String restrictionUri = entityJson.getString("restrictionuri");
			final String restrictionDesc = entityJson.getString("restrictiondesc");
			final String restrictionValue = entityJson.getString("restrictionvalue");
			final String restrictionDescStr = 
					(restrictionDesc == null || restrictionDesc.trim().equals("")) 
						? "Non description provided !"
						: restrictionDesc.replaceAll("\\n", " ") ;
				
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.PropertyValueRestrictionPattern) );
						
				final String propertyPrefix = pattern.getVarPrefix("property");
				pattern.setVarValue("property", createEntityXml(propertyJson, propertyPrefix));
				pattern.setVarValue("comparatoruri", restrictionUri);
				pattern.setVarValue("description", restrictionDescStr);
				pattern.setVarValue("value", restrictionValue);
				pattern.addPrefix(prefix);
				return pattern.toString();		
		}
		
		if ("RelationPropertyPathPattern".equals(pid)) {
			final JSONObject relationJson = entityJson.getJSONObject("relation");
			final JSONObject propertyJson = entityJson.getJSONObject("property");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.RelationPropertyPattern) );
			final String relationPrefix = pattern.getVarPrefix("relation");
			pattern.setVarValue("relation", createEntityXml(relationJson, relationPrefix));
			final String propertyPrefix = pattern.getVarPrefix("property");
			pattern.setVarValue("property", createEntityXml(propertyJson, propertyPrefix));
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		if ("PropertyDomainRangeRestrictionPattern".equals(pid)) {
			final JSONObject propertyJson = entityJson.getJSONObject("property");
			final JSONObject domainClassJson = entityJson.getJSONObject("domainclass");
			final String rangeDatatype = entityJson.getString("rangedatatype");		
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.PropertyDomainClassDatatypeRestrictionPattern) );
			final String propertyPrefix = pattern.getVarPrefix("property");
			pattern.setVarValue("property", createEntityXml(propertyJson, propertyPrefix));
			final String classPrefix = pattern.getVarPrefix("class");
			pattern.setVarValue("class", createEntityXml(domainClassJson, classPrefix));
			pattern.setVarValue("datatype", rangeDatatype);
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		/*
		 * Individual Patterns Instance
		 */
		
		if ("SimpleInstancePattern".equals(pid)) {
			final String instanceUri = entityJson.getString("instanceuri");
			final String instanceName = entityJson.getString("instancename");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.SimpleInstancePattern) );
			pattern.setVarValue("instanceuri", instanceUri);
			pattern.setVarValue("instancename", instanceName);
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		/*
		 * Other Patterns Instance
		 */
		
		if ("PropertiesCollectionPattern".equals(pid)) {
			final JSONArray propJsonArray = entityJson.getJSONArray("proparray");
			final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.PropertiesCollectionPattern) );
			final StringBuilder sb = new StringBuilder();
			final String propListPrefix = pattern.getVarPrefix("proplist");
			final List<Object> propList = jsonArrayToList(propJsonArray);
			final Object[] propArray = propList.toArray(new Object[propList.size()]);
			for (int i = 0; i < propArray.length; i++) {
				final JSONObject jsonObject = (JSONObject) propArray[i];
				sb.append(createEntityXml( jsonObject, propListPrefix));
				if (i < propArray.length - 1) {
					sb.append(NL);
				}
			}
			pattern.setVarValue("proplist", sb.toString());
			pattern.addPrefix(prefix);
			return pattern.toString();
		}
		
		return "<!-- Under construction ! -->";
	}
	
	private String createTransformationXml(JSONObject transformationJson, String prefix) {
		final String transformationUri = transformationJson.getString("uri");
		final JSONArray argsJsonArray = transformationJson.getJSONArray("arguments");
		final String description = transformationJson.getString("description");
		
		final ElementPattern pattern = new ElementPattern( new String(AbstractElementPatternRep.TransformationPattern) );
		
		final String argsPrefix = pattern.getVarPrefix("arglist");
		final StringBuilder argsSb = new StringBuilder();
		List<Object> argsList = jsonArrayToList(argsJsonArray);
		if (!argsList.isEmpty()) {
			Object[] argsArray = argsList.toArray(new Object[argsList.size()]);
			for (int i = 0; i < argsArray.length; i++) {
				JSONObject jsonObject = (JSONObject) argsArray[i];
				final ElementPattern argPattern = new ElementPattern( new String(AbstractElementPatternRep.PairArgumentValuePattern) );
				argPattern.setVarValue("name", jsonObject.getString("argname"));
				argPattern.setVarValue("value", jsonObject.getString("argvalue"));
				argPattern.addPrefix(argsPrefix);
				argsSb.append(argPattern.toString());
				if (i < argsArray.length - 1) {
					argsSb.append(NL);
				}
			}
		} else {
			argsSb.append(argsPrefix + "<!-- No argument specified. -->");
		}
		
		final String descriptionStr = 
				(description == null || description.trim().equals("")) 
					? "Non description provided !"
					: description.replaceAll("\\n", " ") ;
		
		pattern.setVarValue("serviceuri", transformationUri);
		pattern.setVarValue("description", descriptionStr);
		pattern.setVarValue("arglist", argsSb.toString());
		pattern.addPrefix(prefix);
		return pattern.toString();
	}
	
	/** @return The HTML document with mappings specified */
	private String mappingToHTML(final String maprulesOrigin, final JSONObject ontologies,final JSONObject correspondences, final String mappingComments) {
		Checks.checkNotNullArg(ontologies, "ontologies", correspondences, "correspondences");
		
		final JSONObject ontAjson = ontologies.getJSONObject("ontA");
		final String ontoAlabel = ontAjson.getString("label");
		final String ontoAuri = ontAjson.getString("uri");
		final JSONObject ontBjson = ontologies.getJSONObject("ontB");
		final String ontoBlabel = ontBjson.getString("label");
		final String ontoBuri = ontBjson.getString("uri");
		final JSONArray jsonArray = correspondences.getJSONArray("jsonarray");
		
		final StringBuilder sb = new StringBuilder();
		
		final String title = "Mapping between \"" + ontoAlabel + "\" and \"" + ontoBlabel + "\"";
		
		sb.append("<html><head>" + NL);
		sb.append(TAB + "<title>" + title + "</title>" + NL);
		sb.append(TAB + "<meta charset=\"utf-8\" />" + NL);
		sb.append(
			TAB + "<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>" + NL +
			TAB	+ "<style type=\"text/css\">" + NL +
			TAB + TAB + "body { margin: 0px; padding: 0px; }" + NL +
			TAB	+ TAB + "td { font: 1.0em sans-serif; }" + NL +
			TAB	+ TAB + "h1 { font: 1.5em sans-serif; font-weight: bold; color: darkred; margin: 15px 25px; }" + NL +
			TAB	+ TAB + "h2 { font: 1.1em sans-serif; font-weight: bold; }" + NL +
			TAB	+ TAB + "h3 { font: 1.2em sans-serif; font-weight: bold; color: black; padding-bottom: 5px; border-bottom: 1px dashed darkred; padding: 5px 0px 5px 0px; margin: 0px 0px 6px 0px; }" + NL +
			TAB	+ TAB + "h4 { font: 1.1em sans-serif; font-weight: bold; color: #8A4B08; padding: 0px; margin: 0px; }" + NL +
			TAB	+ TAB + "p  { font: 1.0em sans-serif; padding: 0px; margin: 0px; }" + NL +
			TAB	+ TAB + ".ontoDiv { padding: 10px 20px; margin: 10px; background-color: #E1FAED; border: 1px solid #AFC1B8; border-radius: 15px; }" + NL +
			TAB	+ TAB + ".overNotesDiv { padding: 10px 20px; margin: 10px; background-color: #F5F5F5; border: 1px solid #DDDDDD; border-radius: 15px; }" + NL +
			TAB	+ TAB + ".mappingDiv  { margin-top: 10px; margin-bottom: 10px; }" + NL +
			TAB	+ TAB + ".mapRuleDiv  { padding: 10px 20px; margin: 10px; background-color: #DCE2EC; border: 1px solid #B1B7BF; border-radius: 15px; }" + NL +
			TAB + TAB + ".data , .data1 , .data2 , .data3 { margin: 5px 0px 6px 50px; padding: 0px; }" + NL +
			TAB	+ TAB + ".data1 td   { color: #0B0B61; }" + NL +
			TAB	+ TAB + ".data2 td   { color: #0B3B0B; }" + NL +
			TAB	+ TAB + ".data3 td   { color: darkred }" + NL +
			TAB + TAB + ".notesSum   { margin-left: 50px; color: blue; cursor: hand; cursor: pointer;  } .notesSum:hover { color: red; }"  + NL +
			TAB + TAB + ".notesDiv   { margin-left: 50px; padding: 10px; background-color: white; border-radius: 15px; }" + NL +
			TAB + TAB + ".balloonH3  { width: 100%; color: darkred; font-size: 0.9em; border-bottom: 0px !important;	}" + NL +
			TAB + TAB + ".balloonP   { margin-left: 10px; color: darkred; font-size: 0.9em;	}" + NL +
			TAB + TAB + ".balloonH4C { width: 100%; color: black; padding: 5px; font-size: 0.9em; }" + NL +
			TAB + TAB + ".balloonH4O { width: 100%; color: black; padding: 5px; font-size: 0.9em; }" + NL +
			TAB + TAB + ".balloonH4D { width: 100%; color: black; padding: 5px; font-size: 0.9em; }" + NL +
			TAB + TAB + ".balloonH4  { width: 100%; color: black; padding: 5px; font-size: 0.9em; }" + NL +
			TAB + TAB + ".balloonDiv { margin-left: 10px; padding-left: 10px; border-left: 1px dashed gray; width: 100%; }" + NL +
			TAB + TAB + ".balloonDiv h5 { text-weight: bold; color: darkblue; font-size: 1.0em; margin-bottom: 0px; padding-bottom: 0px; }" + NL +
			TAB + TAB + ".balloonDiv p { margin-left: 25px; padding-left: 10px; font-size: 0.9em; color: black; }" + NL + 
			TAB + TAB + ".balloonDiv table { margin-left: 40px !important; padding-left: 10px; margin-top: 10px; }" + NL + 
			TAB + TAB + ".balloonDiv table, .balloonDiv table tr { margin-left: 10px; text-align: center; }" + NL +
			TAB + TAB + ".balloonDiv table th { text-weight: bold; padding: 2px; margin: 2px; min-width: 100px; color: black; font-size: 0.9em; background-color: lightgray; }" + NL +
			TAB + TAB + ".balloonDiv table td { padding: 2px; margin: 2px; min-width: 100px; color: black; font-size: 0.9em; background-color: white; }" + NL +
			TAB + TAB + ".balloonDiv ul { margin-top: 10px; margin-left: 50px !important; padding-left: 10px; }" + NL +
			TAB + TAB + ".balloonDiv ul li { font-size: 0.8em; color: saddlebrown; padding: 0px; margin: 0px; }" + NL +
			TAB + TAB + ".footer { border-top: 1px dashed gray; margin-top: 10px; padding-top: 10px; padding-bottom: 5px; text-align: center; color:#585858; font-size:70%; background-color: #E1E1E1; }" + NL +
			TAB	+ "</style>" + NL +
			TAB	+ "<script>" + NL +
			TAB + TAB + "$( document ).ready(function() {" + NL +
			TAB + TAB + TAB + "$(\".notesDiv\").hide();" + NL +
			TAB + TAB + TAB + "$(\".notesSum\").click(function(){" + NL +
			TAB + TAB + TAB + "	$( \"#E\" + $(this).attr(\"id\") ).toggle();" + NL +
			TAB + TAB + TAB + "});" + NL +
			TAB + TAB + "});" + NL +
			TAB	+ "</script>" + NL
			
		);
		
		sb.append("</head><body>" + NL + NL);
		
		sb.append("<h1>" + title + "</h1>" + NL);
		sb.append("<div class=\"ontoDiv\">" + NL);
		sb.append(TAB + "<h2>" + ontoAlabel + "</h2>" + NL);
		sb.append(TAB + "<div class=\"data\"><p><strong>URI:</strong> " + ontoAuri + "</p></div>" + NL);
		sb.append(TAB + "<h2>" + ontoBlabel + "</h2>" + NL);
		sb.append(TAB + "<div class=\"data\"><p><strong>URI:</strong> " + ontoBuri + "</p></div>" + NL);
		sb.append("</div>" + NL + NL);
		
		// Overall Mapping Comments, Mapping Rules and their Origin
		sb.append("<div class=\"overNotesDiv\">" + NL);
		if (mappingComments != null && !mappingComments.trim().equals("")) 
			sb.append(TAB + "<p>" + mappingComments + "</p>" + NL);
		sb.append(TAB + "<p> Totally <b>" + jsonArray.size() + "</b> Mapping Rule(s)  specified by <b>" + maprulesOrigin + "</b> .</p>" + NL);
		sb.append("</div>" + NL + NL);
		
		sb.append("<div class=\"mappingDiv\">" + NL);
		
		for (int i = 0; i < jsonArray.size(); i++) {
			final JSONObject json = jsonArray.getJSONObject(i);
			final JSONObject entity1Json = json.getJSONObject("entity1");
			final JSONObject entity2Json = json.getJSONObject("entity2");
			final JSONObject directTransformationJson = 
				(json.has("directTransformation")) ? json.getJSONObject("directTransformation") : null;
			final JSONObject inverseTransformationJson = 
				(json.has("inverseTransformation")) ? json.getJSONObject("inverseTransformation") : null;	
			final String relation = 
				(json.has("relation")) ? json.getString("relation") : null;
//			final String direction = 
//				(json.has("direction")) ? json.getString("direction") : null;
			final String comments = 
				(json.has("comments")) ? json.getString("comments") : null;
				
//			final String confidenceValue = 
//					(json.has("similarity")) ? json.getString("similarity") : null;
//			final String confidenceValueComments = 
//					(json.has("simcomments")) ? json.getString("simcomments") : null;
//					
//			final String SUGGESTION_ACCEPTION = "Mapping Rule created by <b>ACCEPTING</b> the suggested one.";
//			final String MANUALLY_SPECIFIED = "Mapping Rule has been <b>MANUALLY</b> specified.";
//			final String origin =  (confidenceValue != null) ? SUGGESTION_ACCEPTION : MANUALLY_SPECIFIED;	
			
			sb.append(NL);
			
			sb.append("<div class=\"mapRuleDiv\">" + NL);
			sb.append(TAB + "<h3>Mapping Rule " + ( i + 1 ) + "</h3>" + NL);
			
			// Description - Comments
			if (comments != null && !comments.trim().equals("")) {
				sb.append(textToHtmlText(comments) + NL);
			}
			
			// Entity 1
			sb.append(TAB + "<h4>Entity 1:</h4>" + NL);
			sb.append(TAB + "<div class=\"data1\">" + createEntityHtml(entity1Json) + "</div>" + NL);
			
			// Entity 2
			sb.append(TAB + "<h4>Entity 2:</h4>" + NL);
			sb.append(TAB + "<div class=\"data2\">" + createEntityHtml(entity2Json) + "</div>" + NL);
			
			// Transformation(s)
			if (directTransformationJson != null && !directTransformationJson.isNullObject()) {
				sb.append(TAB + "<h4>Entity 1 to 2 Transformation:</h4>" + NL);
				sb.append(TAB + "<div class=\"data3\">" + createTransformationHtml(directTransformationJson) + "</div>" + NL);
			}
			if (inverseTransformationJson != null && !inverseTransformationJson.isNullObject()) {
				sb.append(TAB + "<h4>Entity 2 to 1 Transformation:</h4>" + NL);
				sb.append(TAB + "<div class=\"data3\">" + createTransformationHtml(inverseTransformationJson) + "</div>" + NL);
			}
			
			// Relation
			sb.append(TAB + "<h4>Relation:</h4>" + NL);
			sb.append(TAB + "<div class=\"data\">" + "<p>" + relation + "</p>" + "</div>" + NL);
			
//			// Direction
//			if (direction != null && !direction.trim().equals("")) {
//				sb.append(TAB + "<h4>Direction:</h4>" + NL);
//				sb.append(TAB + "<div class=\"data\">" + "<p>" + direction + "</p>" + "</div>" + NL);
//			}
			
//			// Origin
//			sb.append(TAB + "<h4>Origin:</h4>" + NL);
//			sb.append(TAB + "<div class=\"data\">" + origin + "</p>" + "</div>" + NL);
//			
//			if (confidenceValue != null  && !confidenceValue.trim().equals("")) {
//				sb.append(TAB + "<div class=\"notesSum\" id=\"CF" + i + "\"><p>The confidence value of suggested above mapping rule at the time of acceptance was " + confidenceValue + " .</p></div>" + NL);
//			}
//			if (confidenceValueComments != null  && !confidenceValueComments.trim().equals("")) {
//				sb.append(TAB + "<div class=\"notesDiv\" id=\"ECF" + i + "\">" + confidenceValueComments + "</div>" + NL);
//			}
			
			sb.append("</div> <!-- END OF A MAPPING RULE DIV -->" + NL);
		}
		
		sb.append(NL + "</div> <!-- END OF ALL MAPPING RULES DIV -->" + NL);
		
		sb.append(NL + "<br>" + NL + "<p class=\"footer\">" + NL +
			TAB + "Document automatically generated by <b>Ontologies Alignment Tool</b>, available at the following link: <a href=\"http://ponte.grid.ece.ntua.gr:8080/OntoMapping/\" target=\"_blank\">OAT</a> " + NL + "</p>" +  NL);
		
		sb.append(NL + "</body></html>");
		
		return sb.toString();
	}

	/** @return The HTML "view" of entity JSON object provided */
	private String createEntityHtml(JSONObject entityJson) {
		Checks.checkNotNullArg(entityJson, "entityJson");
		
		final String pid = entityJson.getString("pid");
		
		if ("SimpleClassPattern".equals(pid)) {
			String htmlResponse = 
					"<table>" +
						//"<tr><td colspan=\"2\"><strong>SimpleClassPattern</strong></td></tr>" +
						"<tr><td>Class-URI::</td><td>" + entityJson.getString("classuri") + " ( <b>" + entityJson.getString("classname") + "</b> )</td></tr>" +
					"</table>";
			return htmlResponse;
		} 
		
		if ("ClassUnionPattern".equals(pid) || "ClassIntersectionPattern".equals(pid)) {
			String htmlResponse = "<table><tr><td colspan=\"2\"><strong>" + pid + "</strong></td></tr>";
			final JSONArray jsonArray = entityJson.getJSONArray("classarray");
			for (Object object : jsonArray) {
				final JSONObject jsonObject = (JSONObject) object;
				htmlResponse += "<tr><td>Class:</td><td>" + createEntityHtml(jsonObject) + "</td></tr>";
			}
			htmlResponse += "</table>";
			return htmlResponse;
		}

		if ("ClassByAttributeValueRestrictionPattern".equals(pid) || "ClassByAttributeOccurencePattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>" + pid + "</strong></td></tr>" +
					"<tr><td>Class:</td><td>" + createEntityHtml( entityJson.getJSONObject("cls") ) + "</td></tr>" +
					"<tr><td>Property:</td><td>" + createEntityHtml( entityJson.getJSONObject("property") ) + "</td></tr>" +
					"<tr><td>Comparator:</td><td>" + entityJson.getString("restrictionuri") + "</td></tr>" +
					"<tr><td>Description:</td><td>" + entityJson.getString("restrictiondesc") + "</td></tr>" +
					"<tr><td>Value:</td><td>" + entityJson.getString("restrictionvalue") + "</td></tr>" +
				"</table>";
			return htmlResponse;
		}
		
		if ("ClassWithPropertiesRestrictionPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					//"<tr><td colspan=\"2\"><strong>" + pid + "</strong></td></tr>" +
					"<tr><td>Class:</td><td>" + createEntityHtml( entityJson.getJSONObject("cls") ) + "</td></tr>";
			final JSONArray jsonArray = entityJson.getJSONArray("proparray");
			for (Object object : jsonArray) {
				final JSONObject jsonObject = (JSONObject) object;
				htmlResponse += "<tr><td>Property:</td><td>" + createEntityHtml(jsonObject) + "</td></tr>";
			}
			htmlResponse += "</table>";
			return htmlResponse;	
		}

		// ------------------------------------------------------------------------------
		// -- Relation Patterns
		// ------------------------------------------------------------------------------
		
		if ("SimpleRelationPattern".equals(pid)) {
			String htmlResponse = 
					"<table>" +
						//"<tr><td colspan=\"2\"><strong>SimpleRelationPattern</strong></td></tr>" +
						"<tr><td>Relation-URI::</td><td>" + entityJson.getString("relationuri") + " ( <b>" + entityJson.getString("relationname") +  "</b> )</td></tr>" +
					"</table>";
			return htmlResponse;
			//return "<strong>Relation-URI:</strong> " + entityJson.getString("relationuri");
		} 
		
		if ("InverseRelationPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>InverseRelationPattern</strong></td></tr>" +
					"<tr><td>Relation:</td><td>" + createEntityHtml( entityJson.getJSONObject("relation") ) + "</td></tr>" +
				"</table>";
			return htmlResponse;
		}
		
		if ("RelationDomainRestrictionPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>RelationDomainRestrictionPattern</strong></td></tr>" +
					"<tr><td>Relation:</td><td>" + createEntityHtml( entityJson.getJSONObject("relation") ) + "</td></tr>" +
					"<tr><td>DomainClass:</td><td>" + createEntityHtml( entityJson.getJSONObject("domainclass") ) + "</td></tr>" +
				"</table>";
			return htmlResponse;
		} 
		
		if ("RelationRangeRestrictionPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>RelationRangeRestrictionPattern</strong></td></tr>" +
					"<tr><td>Relation:</td><td>" + createEntityHtml( entityJson.getJSONObject("relation") ) + "</td></tr>" +
					"<tr><td>RangeClass:</td><td>" + createEntityHtml( entityJson.getJSONObject("rangeclass") ) + "</td></tr>" +
				"</table>";
			return htmlResponse;
		} 
		
		if ("RelationPathPattern".equals(pid)) {
			String htmlResponse = "<table><tr><td colspan=\"2\"><strong>RelationPathPattern</strong></td></tr>";
			final JSONArray jsonArray = entityJson.getJSONArray("relationarray");
			for (Object object : jsonArray) {
				final JSONObject jsonObject = (JSONObject) object;
				htmlResponse += "<tr><td>Relation:</td><td>" + createEntityHtml(jsonObject) + "</td></tr>";
			}
			htmlResponse += "</table>";
			return htmlResponse;
		}
		
		if ("RelationDomainRangeRestrictionPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>RelationDomainRangeRestrictionPattern</strong></td></tr>" +
					"<tr><td>Relation:</td><td>" + createEntityHtml( entityJson.getJSONObject("relation") ) + "</td></tr>" +
					"<tr><td>DomainClass:</td><td>" + createEntityHtml( entityJson.getJSONObject("domainclass") ) + "</td></tr>" +
					"<tr><td>RangeClass:</td><td>" + createEntityHtml( entityJson.getJSONObject("rangeclass") ) + "</td></tr>" +
				"</table>";
			return htmlResponse;
		} 
		
		// ------------------------------------------------------------------------------
		// -- Property Patterns
		// ------------------------------------------------------------------------------
		
		if ("SimplePropertyPattern".equals(pid)) {
			String htmlResponse = 
					"<table>" +
						//"<tr><td colspan=\"2\"><strong>SimplePropertyPattern</strong></td></tr>" +
						"<tr><td>Property-URI::</td><td>" + entityJson.getString("propertyuri") + " ( <b>" + entityJson.getString("propertyname") + "</b> )" + "</td></tr>" +
					"</table>";
			return htmlResponse;
			//return "<strong>Property-URI:</strong> " + entityJson.getString("propertyuri");
		} 
		
		if ("PropertyDomainRestrictionPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>PropertyDomainRestrictionPattern</strong></td></tr>" +
					"<tr><td>Property:</td><td>" + createEntityHtml( entityJson.getJSONObject("property") ) + "</td></tr>" +
					"<tr><td>DomainClass:</td><td>" + createEntityHtml( entityJson.getJSONObject("domainclass") ) + "</td></tr>" +
				"</table>";
			return htmlResponse;
		} 

		if ("PropertyRangeRestrictionPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>PropertyRangeRestrictionPattern</strong></td></tr>" +
					"<tr><td>Property:</td><td>" + createEntityHtml( entityJson.getJSONObject("property") ) + "</td></tr>" +
					"<tr><td>RangeDatatype:</td><td>" + entityJson.getString("rangedatatype") + "</td></tr>" +
				"</table>";
			return htmlResponse;
		}

		if ("PropertyValueRestrictionPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>PropertyValueRestrictionPattern</strong></td></tr>" +
					"<tr><td>Property:</td><td>" + createEntityHtml( entityJson.getJSONObject("property") ) + "</td></tr>" +
					"<tr><td>Restriction:</td><td>" + entityJson.getString("restrictionuri") + "</td></tr>" +
					"<tr><td>Description:</td><td>" + entityJson.getString("restrictiondesc") + "</td></tr>" +
					"<tr><td>Value:</td><td>" + entityJson.getString("restrictionvalue") + "</td></tr>" +
				"</table>";
			return htmlResponse;
		}
		
		if ("RelationPropertyPathPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>RelationPropertyPathPattern</strong></td></tr>" +
					"<tr><td>Relation:</td><td>" + createEntityHtml(entityJson.getJSONObject("relation")) + "</td></tr>" +
					"<tr><td>Property:</td><td>" + createEntityHtml(entityJson.getJSONObject("property")) + "</td></tr>" +
				"</table>";
			return htmlResponse;
		}

		if ("PropertyDomainRangeRestrictionPattern".equals(pid)) {
			String htmlResponse = 
				"<table>" +
					"<tr><td colspan=\"2\"><strong>PropertyDomainRangeRestrictionPattern</strong></td></tr>" +
					"<tr><td>Property:</td><td>" + createEntityHtml( entityJson.getJSONObject("property") ) + "</td></tr>" +
					"<tr><td>DomainClass:</td><td>" + createEntityHtml( entityJson.getJSONObject("domainclass") ) + "</td></tr>" +
					"<tr><td>Datatype:</td><td>" + entityJson.getString("rangedatatype") + "</td></tr>" +
				"</table>";
			return htmlResponse;
		}

		// ------------------------------------------------------------------------------
		// -- Instance Patterns
		// ------------------------------------------------------------------------------
		
		if ("SimpleInstancePattern".equals(pid)) {
			String htmlResponse = 
					"<table>" +
						//"<tr><td colspan=\"2\"><strong>SimpleInstancePattern</strong></td></tr>" +
						"<tr><td>Instance-URI::</td><td>" + entityJson.getString("instanceuri") + " ( <b>" + entityJson.getString("instancename") + "</b> )</td></tr>" +
					"</table>";
			return htmlResponse;
		}
		
		// ------------------------------------------------------------------------------
		// -- Other Patterns
		// ------------------------------------------------------------------------------
		
		if ("PropertiesCollectionPattern".equals(pid)) {
			String htmlResponse = "<table>";
			//htmlResponse += "<tr><td colspan=\"2\"><strong>PropertiesCollectionPattern</strong></td></tr>";
			final JSONArray jsonArray = entityJson.getJSONArray("proparray");
			for (Object object : jsonArray) {
				final JSONObject jsonObject = (JSONObject) object;
				htmlResponse += "<tr><td>Property:</td><td>" + createEntityHtml(jsonObject) + "</td></tr>";
			}
			htmlResponse += "</table>";
			return htmlResponse;
		}
		
		return "<span style='color:darkred;'>The HTML representation of entity with PatternID \"" + pid + "\" is under construction !</span>";
	}
	
	/** @return The HTML "view" of transformation JSON object provided */
	private String createTransformationHtml(JSONObject transJson) {
		String argumentsStr = "";
		final JSONArray jsonArray = transJson.getJSONArray("arguments");
		for (Object object : jsonArray) {
			JSONObject jsonObject = (JSONObject) object;
			argumentsStr += jsonObject.getString("argname") + " : " + jsonObject.getString("argvalue") + " , ";
		}
		String argumentsRow = "";
		if (argumentsStr != "") {
			argumentsRow = "<tr><td>Arguments:</td><td>" + argumentsStr + "</td></tr>";
		}
		String descriptionRow = "";
		if (transJson.getString("description") != "") {
			descriptionRow = "<tr><td>Description:</td><td>" + transJson.getString("description") + "</td></tr>";
		}
		String htmlResponse = 
			"<table>" +
				"<tr><td>Transformation-Service:</td><td>" + transJson.getString("uri") + "</td></tr>" +
				descriptionRow +
				argumentsRow +
			"</table>";
		return htmlResponse;
	}
	
	private static List<Object> jsonArrayToList(JSONArray jsonArray) {
		final List<Object> list = new ArrayList<Object>();
		for (Object object : jsonArray) {
			list.add(object);
		}
		return list;
	}
	
	private static String textToHtmlText(final String text) {
		if (text == null) return null;
		final StringBuilder sb = new StringBuilder();
		final String[] lines = text.split("\\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			sb.append("<p>" + line + "</p>");
		}
		return sb.toString();
	}
	
}
