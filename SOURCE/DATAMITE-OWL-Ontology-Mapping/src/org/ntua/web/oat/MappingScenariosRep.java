package org.ntua.web.oat;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.log4j.Logger;
import org.timchros.core.util.Throable;
import org.timchros.core.util.Util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MappingScenariosRep {

	private static final Logger log = LogFactory.getLoggerForClass(MappingScenariosRep.class);
	
	public static enum Category {
		Lab_Test, Biopsy_Test, Symptom, Diagnosis, Medication, Score
	}
	
	private static MappingScenariosRep instance = null;
	
	public static MappingScenariosRep getInstance() {
		synchronized (MappingScenariosRep.class) {
			if (instance == null) {
				instance  = new MappingScenariosRep();
			}
			return instance;
		}
	}

	private static final String oatPropFileName = "oat.properties";
	public JSONArray dateFormatJA = new JSONArray();
	public JSONArray unitJA = new JSONArray();
	public JSONArray numFormatJA = new JSONArray();
	
	private static final String mapScenariosFileName = "Mapping-Scenarios.json";
	private JSONObject msJson = new JSONObject();	
	
	private static final String LOCAL_PATH = "WebContent/WEB-INF/classes/";
	
	private MappingScenariosRep() {
        try {
        	log.trace("Loading OAT Properties ...");
        	InputStream propis = Thread.currentThread().getContextClassLoader().getResourceAsStream(oatPropFileName);
        	if (propis == null) propis = new FileInputStream(LOCAL_PATH + oatPropFileName);
        	final Properties prop = new Properties();
        	prop.load(new InputStreamReader(propis, Charset.forName("UTF-8")));
        	propis.close();
        	dateFormatJA = JSONArray.fromObject(prop.get("date.format.ja"));
        	unitJA = JSONArray.fromObject(prop.get("unit.ja"));
        	numFormatJA = JSONArray.fromObject(prop.get("number.format.ja"));
        	
        	log.trace("Loading Reference Model for check/enrich Mapping Scenarios Entities ...");
        	InputStream ontois = Thread.currentThread().getContextClassLoader().getResourceAsStream(OntoFileUploadServlet.HARMONICSS_REFERENCE_MODEL);
        	if (ontois == null) ontois = new FileInputStream(LOCAL_PATH + OntoFileUploadServlet.HARMONICSS_REFERENCE_MODEL);
        	final OntModel ontModel = ModelFactory.createOntologyModel( OntModelSpec.OWL_MEM );
			ontModel.read(ontois, null);
			ontois.close();
        	
        	log.trace("Loading Mapping Scenarios ...");
    		InputStream msis = Thread.currentThread().getContextClassLoader().getResourceAsStream(mapScenariosFileName);
    		if (msis == null) msis = new FileInputStream(LOCAL_PATH + mapScenariosFileName);
    		final String jsonStr = IOUtils.toString(msis, StandardCharsets.UTF_8.name());
    		msis.close();
    		msJson = JSONObject.fromObject(jsonStr);
    		log.trace("Examine and Update Mapping Scenarios ...");
    		final Set<String> invUriSet = new HashSet<>();
    		final JSONArray categJA = msJson.getJSONArray("categJA");
    		for (int i = 0; i < categJA.size(); i++) {
    			final JSONObject categJO = categJA.getJSONObject(i);
    			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
    			for (int j = 0; j < mapscenJA.size(); j++) {
    				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);
    				final JSONObject entityJO = mapscenJO.getJSONObject("entity2");
    				final String classURI = entityJO.getString("uri").trim();
    				// Find Class based on URI and update JSON with Label
    				final OntClass ontClass = ontModel.getOntClass(classURI);
    				if (ontClass == null) {
    					invUriSet.add(classURI);
    				} else {
    					final String label = ontClass.getLabel(null);
    					entityJO.put("label", label);
    				}
    				final JSONArray propJA = entityJO.getJSONArray("ja");
    				for (int k = 0; k < propJA.size(); k++) {
    					final JSONObject propJO = propJA.getJSONObject(k);
    					final String propURI = propJO.getString("uri").trim();
    					// Find Property based on URI and add in JSON with Label and PropertyType
    					final OntResource ontResource = ontModel.getOntResource(propURI);
    					if (ontResource == null || (!ontResource.isObjectProperty() && !ontResource.isDatatypeProperty())) {
    						invUriSet.add(propURI);
    					} else {
    						final String label = ontResource.getLabel(null);
    						propJO.put("label", label);
    						if (ontResource.isObjectProperty()) {
    							propJO.put("proptype", "OP");
    						} else {
    							propJO.put("proptype", "DP");
    						}
    					}
    				} // END OF Entity 2 Prop URIs
    				
    				// Find terms for auto-complete or selection
    				if (mapscenJO.has("datatrans")) {
    					final JSONObject datatransJO = mapscenJO.getJSONObject("datatrans");
    					final JSONArray attrJA = datatransJO.getJSONArray("ja");
    					for (int k = 0; k < attrJA.size(); k++) {
    						final JSONObject attrJO = attrJA.getJSONObject(k);
    						if (attrJO.has("datatype") && attrJO.getString("datatype").equalsIgnoreCase("CLASS") && attrJO.has("uri")) {
    							final String domainUriStr = attrJO.getString("uri");
    							
    							final JSONArray classja = new JSONArray();
    							final String[] domainUriArray = domainUriStr.split(",");
    							for (String domainUri : domainUriArray) {
    								if (domainUri == null || domainUri.trim().equals("")) continue;
    								final String domainUriTrim = domainUri.trim();
    								final OntClass domainOntClass = ontModel.getOntClass(domainUriTrim);
        							if (domainOntClass == null) {
        		    					invUriSet.add(domainUriTrim);
        		    				} else {
        		    					classja.addAll( 
        		    						OntoElemServlet.toJsonObjArray(
        		    							 new ArrayList<>( ignNonCV(getSubClassesFor(domainOntClass)) )));
        		    				}
								}
    							if (!classja.isEmpty())
    								attrJO.put("classja", classja);
    						}
    						if (attrJO.has("datatype") && attrJO.getString("datatype").equalsIgnoreCase("DATE-FORMAT") && !attrJO.has("optionsja")) {
    							attrJO.put("optionsja", dateFormatJA);
    						}
    						if (attrJO.has("datatype") && attrJO.getString("datatype").equalsIgnoreCase("NUMBER-FORMAT") && !attrJO.has("optionsja")) {
    							attrJO.put("optionsja", numFormatJA);
    						}
    						if (attrJO.has("datatype") && attrJO.getString("datatype").equalsIgnoreCase("UNIT") && !attrJO.has("optionsja")) {
    							attrJO.put("optionsja", unitJA);
    						}
    					}
    				}
    				
    			} // END OF Mapping Rules
    		} // END OF Categories
    		
    		// Clear OntModel
    		ontModel.removeAll();
    		
    		if (!invUriSet.isEmpty()) {
    			final List<String> uriList = new ArrayList<>(invUriSet);
    			Collections.sort(uriList);
    			System.out.println(">> Invalid URIs: " + uriList.size());
    			Util.listToMultiLineString(uriList);
    			throw new RuntimeException("Mapping Scenarios contain " + uriList.size() + " invalid URI(s) !");
    		}
    		
    		log.info("Mapping Scenarios successfully Loaded from FILE: " + mapScenariosFileName);
        } catch(Throwable t) {
        	final String msg = "Unable to LOAD mapping scenarios from FILE: " + mapScenariosFileName;
			log.error(msg + "\n" + Throable.throwableAsString(t));
			throw new RuntimeException(msg, t);
        }		
	}
	
    private Set<OntClass> getSubClassesFor(final OntClass ontClass) {
    	final Set<OntClass> ontClassSet = new HashSet<>();
    	updateSubClassesFor(ontClassSet, ontClass);
    	ontClassSet.remove(ontClass);
    	return ontClassSet;
    }
    
    private void updateSubClassesFor(final Set<OntClass> ontClassSet, final OntClass ontClass) {
    	if (ontClass.getLocalName() != null) ontClassSet.add(ontClass);
    	final ExtendedIterator<OntClass> exIt = ontClass.listSubClasses();
    	while (exIt.hasNext()) {
			OntClass subOntClass = exIt.next();
			updateSubClassesFor(ontClassSet, subOntClass);
		}
    }
    
    private static final Set<String> ignSet = new HashSet<>(Util.newArrayList(
    	"SYMPT-CAT-A", "SYMPT-CAT-B",
       	"BLOOD-100", "BLOOD-200", "BLOOD-300", "BLOOD-400", "BLOOD-500", 
       	"BLOOD-600", "BLOOD-700", "BLOOD-800", "BLOOD-900",
       	"BIOPSY-10", "BIOPSY-20", "BIOPSY-30", "IMG-10", "IMG-20"
    ));
    
    public static boolean ignTermWithCode(final String code) {
    	if (code == null) return true;
    	return ignSet.contains(code);
    }
        
    /** Ignore non-coded values */
    private Set<OntClass> ignNonCV(final Set<OntClass> ontClassSet) {
    if (ontClassSet == null) return null;
      	final Set<OntClass> newOntClassSet = new HashSet<>();
      	for (OntClass ontClass : ontClassSet) {
      		final String locName = ontClass.getLocalName();
      		if (ignSet.contains(locName)) continue;
      		final int dashIndex = locName.lastIndexOf('-');
      		if (dashIndex <= 0) continue;
      		final String numStr = locName.substring(dashIndex + 1);
      		if (numStr.length() < 1 || !numStr.matches("\\d+")) continue;
      		newOntClassSet.add(ontClass);
      	}
      	return newOntClassSet;
    }
    
	public JSONObject getMapScenJson() {
		return msJson;
	}
	
	public List<String> getMapScenUrisForCategory(final Category category) {
		if (category == null) return null;
		final String categName = category.name().replaceAll("_", " ");
		final List<String> uriList = new ArrayList<>();
		final JSONArray categJA = msJson.getJSONArray("categJA");
		for (int i = 0; i < categJA.size(); i++) {
			final JSONObject categJO = categJA.getJSONObject(i);
			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
			for (int j = 0; j < mapscenJA.size(); j++) {
				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);
				if (!mapscenJO.has("subcateg")) continue;
				final String subCategory = mapscenJO.getString("subcateg");
				if (subCategory.equals(categName)) {
					final String msuri = mapscenJO.getString("uri");
					uriList.add(msuri);
				}
			}
		}
		//Collections.sort(uriList);
		return uriList;
	}
	
}
