package org.ntua.web.oat;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import org.timchros.core.util.Throable;

/**
 * Servlet implementation class MappingProvideServlet
 * 
 * @author Efthymios Chondrogiannis
 */
@WebServlet("/MappingProvideServlet")
public class MappingProvideServlet extends HttpServlet {
	
	private static final Logger log = LogFactory.getLoggerForClass(MappingProvideServlet.class);
	
	private static final long serialVersionUID = 1L;
    
	private static final String FOLDER = OntoFileUploadServlet.getFolder() + "/MAPPINGS";
	
	static {
		final File folder = new File(FOLDER);
		if (!folder.exists()) {
			log.trace("Creating FOLDER(s) ... \"" + FOLDER + "\" ...\n");
			final boolean result = folder.mkdirs();
			if (!result) {
				log.trace("Cannot create FOLDER(s) ..\"" + FOLDER + "\" !!!");
			}
		}
	}
	
	private static final String fileNamePrefixPattern = "yyyyMMddHHmmssSSS";
	private static final SimpleDateFormat prefixFormater = new SimpleDateFormat(fileNamePrefixPattern);

	String tmp = null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MappingProvideServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String sid = ServletUtil.getSessionID();
		
		boolean isMultipartContent = ServletFileUpload.isMultipartContent(request);
		if (!isMultipartContent) {
			log.warn("Request is NOT MultipartContent ! Loading response from \"tmp\" local variable.");
			response.setContentType("application/json");
			final PrintWriter out = response.getWriter();
			out.print(tmp);
			out.close();
			return;
		}
		
		final FileItemFactory factory = new DiskFileItemFactory();
		final ServletFileUpload upload = new ServletFileUpload(factory);
		
		try {
			@SuppressWarnings("unchecked")
			final List<FileItem> fields = upload.parseRequest(request);
			final Iterator<FileItem> it = fields.iterator();
			if (it.hasNext()) {
				final FileItem fileItem = it.next();
				final String name = fileItem.getFieldName();
				log.trace("SID: " + sid + " :: Hanlde element with name: \"" + name + "\" -- " + fileItem);
				if (name.startsWith("jsonfile") && fileItem.getName() != null && !fileItem.getName().equals("")) {
					final String newFileName = prefixFormater.format(new Date()) + "-Mapping.json";
					final String uploadFilePath = FOLDER + "/" + newFileName;
					final File uploadedFile = new File(uploadFilePath);
					try {
						fileItem.write(uploadedFile);
						final String path = uploadedFile.getAbsolutePath();
						final String content = FilesUtil.getUTF8TextFromFile(path);
						final JSONObject jsonObj = JSONObject.fromObject(content);
						final JSONObject corJsonObj = jsonObj.getJSONObject("correspondences");
						
						response.setContentType("application/json; charset=UTF-8");
						final PrintWriter out = response.getWriter();
						tmp = corJsonObj.toString();
						out.print(corJsonObj.toString());
						out.close();
						log.info("SID: " + sid + " :: Mapping File UPLOADED: \"" + uploadedFile.getName());
						log.trace("SID: " + sid + " , JSON:\n" + corJsonObj.toString(5,5));
					} catch (Throwable t) {
						final String emsg = "SID: " + sid + " :: PROBLEM :: Unable to UPLOAD/PARSE Mapping File with name \"" + fileItem.getName() + "\"";
						log.error(emsg + "\n" + Throable.throwableAsString(t));
						
						final JSONObject errorJsonObj = new JSONObject();
						String returnMsg = Throable.throwableAsSimpleOneLineString(t);
						if (returnMsg != null && returnMsg.length() > 200) returnMsg = returnMsg.substring(0, 200) + " ...";
						errorJsonObj.put("error", returnMsg);
						response.setContentType("application/json; charset=UTF-8");
						final PrintWriter out = response.getWriter();
						tmp = errorJsonObj.toString();
						out.print(errorJsonObj.toString());
						out.close();
					}
				}
			}
		} catch(Throwable tt) {
			final String msg = "Error when uploading the file... " + tt.getMessage();
			log.error(msg + "\n" + Throable.throwableAsString(tt));

			final JSONObject errorJsonObj = new JSONObject();
			String returnMsg = Throable.throwableAsSimpleOneLineString(tt);
			if (returnMsg != null && returnMsg.length() > 200) returnMsg = returnMsg.substring(0, 200) + " ...";
			errorJsonObj.put("error", returnMsg);
			response.setContentType("application/json; charset=UTF-8");
			final PrintWriter out = response.getWriter();
			tmp = errorJsonObj.toString();
			out.print(errorJsonObj.toString());
			out.close();
		}
		
	}

}
