package org.ntua.web.oat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.jena.ontology.OntDocumentManager;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.ontology.Ontology;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.timchros.core.interfaces.I_Check;
import org.timchros.core.transformers.I_StringTransformer;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.util.Checks;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * This class provides some useful methods for the Servlets of this Web Application.
 * 
 * @author Efthymios Chondrogiannis
 */
public class ServletUtil {

	private static final String datetimePattern = "yyyyMMddHHmmssSSS";
	private static final SimpleDateFormat formater = new SimpleDateFormat(datetimePattern);
	
	/**
	 * Ensures that we will not create an instance of this class
	 */
	private ServletUtil() {
		
	}
	
	/**
	 * @return A Unique ID
	 */
	public static String getSessionID() {
		return formater.format(new Date());
	}
	
	/**
	 * Provides a brief human readable description of the {@link OntResource} in terms of <b>label</b> or <b>local name</b>.
	 * 
	 * @param ontResource
	 * 		The resource provided
	 * @return
	 * 		The <b>label</b> of the {@link OntResource} if it is <i>not</i> <code>null</code>, otherwise the <b>local name</b>.
	 */
	public static <T extends OntResource> String getLabelOrLocalName(final T ontResource) {
		final String label = ontResource.getLabel(null);
		return ( label != null && !label.equals("")) ? label : ontResource.getLocalName();
	}
	
	/**
	 * @return 
	 * 		The first object in the list provided which is not <code>null</code>.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T notNullByOrderProvided(T... objectArray) {
		if (objectArray == null) return null;
		for (int i = 0; i < objectArray.length; i++) {
			T object = objectArray[i];
			if (object != null) return object;
		}
		return null;
	}
	
	/**
	 * Creates a {@link List} with the data available through {@link ExtendedIterator}.
	 * 
	 * @param extIterator
	 * 		An {@link ExtendedIterator} giving the ability to iterate over its elements.
	 * @return
	 * 		A {@link List} with the objects retrieved from the <code>extIterator</code>
	 */
	public static <T> List<T> toList(ExtendedIterator<T> extIterator) {
		List<T> list = new ArrayList<T>();
		if (extIterator == null) return list;
		while (extIterator.hasNext()) {
			T element = extIterator.next();
			list.add(element);
		}
		return list;
	}
	
	/**
	 * Creates a {@link List} with the data available through {@link ExtendedIterator} which satisfy the condition provided.
	 * 
	 * @param extIterator
	 * 		An {@link ExtendedIterator} giving the ability to iterate over its elements.
	 * @param checker
	 * 		Specifies the condition that the elements should satisfy.
	 * @return
	 * 		A {@link List} with the objects retrieved from the <code>extIterator</code> which satisfy the condition provided.
	 */
	public static <T> List<T> toList(ExtendedIterator<T> extIterator, I_Check<T> checker) {
		List<T> list = new ArrayList<T>();
		if (extIterator == null) return list;
		while (extIterator.hasNext()) {
			T element = extIterator.next();
			if (checker.isTrue(element)) {
				list.add(element);
			}
		}
		return list;
	}
	
	/**
	 * Creates a {@link List} with the elements available through the ExtendedIterator 
	 * after transforming the elements to Strings using the transformer provided.
	 * 
	 * @param extIterator
	 * 		An {@link ExtendedIterator} giving the ability to iterate over its elements.
	 * @param strTransformer
	 * 		Provides a String representation of an element of type <code>T</code>.
	 * @return
	 * 		A {@link List} with the objects/strings retrieved from the <code>extIterator</code> after applying the transformation provided.
	 */
	public static <T> List<String> toList(ExtendedIterator<? extends T> extIterator, I_StringTransformer<T> strTransformer) {
		List<String> list = new ArrayList<String>();
		if (extIterator == null) return list;
		while (extIterator.hasNext()) {
			T element = extIterator.next();
			list.add(strTransformer.asString(element));
		}
		return list;
	}
	
	/**
	 * Creates a {@link JSONArray} with the objects existing within the {@link List} provided.
	 * <p>
	 * Each element existing within the {@link JSONArray} is a {@link JSONObject} with a key <b>str</b>
	 * the value of which is a String - the <code>toString()</code> method of the objects existing within the list is being indirectly used.
	 * 
	 * @param list
	 * 		A {@link List} with objects of type <code>T</code>.
	 * @return
	 * 		A {@link JSONArray}
	 */
	public static <T> JSONArray toJsonArray(List<T> list) {
		if (list == null || list.size() == 0) return null;
		JSONArray jsonArray = new JSONArray();
		for (T object : list) {
			JSONObject member = new JSONObject();
			member.put("str", "" + object);			
			jsonArray.add(member);
		}
		return jsonArray;
	}
	
	/**
	 * Creates a {@link JSONArray} with the objects existing within the {@link List} provided,
	 * after transforming them to Strings using the transformer given.
	 * 
	 * @param list
	 * 		A {@link List} with objects of type <code>T</code>.
	 * @param strTransformer
	 * 		Provides a String representation of an element of type <code>T</code>.
	 * @return
	 * 		A {@link JSONArray} with the Strings
	 */
	public static <T> JSONArray toJsonArray(List<T> list, I_StringTransformer<T> strTransformer) {
		if (list == null || list.size() == 0) return null;
		JSONArray jsonArray = new JSONArray();
		for (T object : list) {	
			jsonArray.add(strTransformer.asString(object));
		}
		return jsonArray;
	}
	
	/**
	 * Replaces the URIs presented in the text with the corresponding anchor. 
	 * <p>
	 * The anchor will be defined in such way so that it opens in a new tab when clicked.
	 * 
	 * @param text
	 * 		The initial text
	 * @return
	 * 		The text provided with the URIs replaced by an anchor.
	 */
	public static String fixUris(String text) {
		if (text == null) return null;
		final StringBuilder sb = new StringBuilder();
		final String[] lines = text.split("\\n");
		for (int i = 0; i < lines.length; i++) {
			final String line = lines[i];
			final StringTokenizer tokens = new StringTokenizer(line);
			while (tokens.hasMoreElements()) {
				final String token = "" + tokens.nextElement();
				if (token.startsWith("http://") || token.startsWith("https://")) {
					sb.append("<a href=\"" + token + "\" class=\"extlink\" target=\"_blank\">" + token + "</a>");
				} else {
					sb.append(token + " ");
				}
			}
			if ( i != ( lines.length - 1 ) ) sb.append("\n");
		}
		return sb.toString();
	}
	
	/**
	 * The <b>local name</b> of the {@link OntResource}s is being used.
	 * 
	 * @author Efthymios Chondrogiannis
	 */
	public static class OntResourceLocalNameComparator implements Comparator<OntResource> {
		@Override public int compare(OntResource o1, OntResource o2) {
			return o1.getLocalName().compareTo(o2.getLocalName());
		}
	}
	
	/**
	 * 
	 * @param mainOntologyPath
	 * 		The path of the ontology on the server's disk.
	 * @param uriPathList
	 * 		A {@link List} with {@link Tuple2} the first element of which is the URI of an imported ontology, 
	 * 		whereas the second element is the path of the corresponding owl file in the disk
	 * @return
	 * 		The {@link OntModel}
	 * @throws FileNotFoundException
	 */
	public static synchronized OntModel readOntModel (final String mainOntologyPath, final List<Tuple2<String, String>> uriPathList) throws FileNotFoundException {
		final OntDocumentManager mgr = new OntDocumentManager();
		for (Tuple2<String, String> tuple : uriPathList) {
			mgr.addAltEntry(tuple._1(), tuple._2());
		}
		final OntModelSpec ontModelSpec = new OntModelSpec( OntModelSpec.OWL_MEM );
		ontModelSpec.setDocumentManager( mgr );
		final OntModel ontModel = ModelFactory.createOntologyModel( ontModelSpec );
		ontModel.read( new FileInputStream( new File(mainOntologyPath) ), null);
		return ontModel;
	}
	
	/**
	 * Finds the main {@link Ontology} specified within the {@link OntModel} given.
	 * 
	 * @param ontModel
	 * 		The {@link OntModel}
	 * @return
	 * 		The <i>main</i> {@link Ontology} if specified, otherwise <code>null</code>.
	 */
	public static synchronized Ontology getOntology(final OntModel ontModel) {
		
		if (ontModel == null) return null;
		
		final List<Ontology> ontList = toList( ontModel.listOntologies() );
		
		//System.out.println("****** ontList.size(): " + ontList.size());
		
		if (ontList.size() == 0) return null;
		
		if (ontList.size() == 1) {
			final Ontology theOntology = ontList.get(0);
			return theOntology;
		} else {
			Ontology ontology = null;
			final String defaultUri = ontModel.getNsPrefixURI("");
			if (defaultUri != null) {
				ontology = notNullByOrderProvided(
					ontModel.getOntology(defaultUri),
					ontModel.getOntology(defaultUri.substring(0, defaultUri.length()-1))
				);
			}
			if (ontology != null) {
				return ontology;
			} else {
				final Ontology theFirstOntology = ontList.get(0);
				return theFirstOntology;
			}
		}
	}
	
	/**
	 * Finds the value of the property given within the {@link Ontology} provided
	 * 
	 * @param ontology
	 * 		The {@link Ontology}
	 * @param uri
	 * 		The uri of the property of our interest
	 * @return
	 * 		The value if the appropriate {@link Statement} is being used, otherwise <code>null</code>.
	 */
	public static String getOntoPropertyValue(final Ontology ontology, final String uri) {
		StmtIterator stmtIt = ontology.listProperties();
		while (stmtIt.hasNext()) {
			Statement stmt = stmtIt.next();
			if (stmt.getPredicate().getURI().equals(uri)) return "" + stmt.getObject();
		}
		return null;
	}
	
	/**
	 * Finds the URIs of the imported ontologies.
	 * 
	 * @param ontModel
	 * 		The {@link OntModel}
	 * @return
	 * 		A {@link List} with the URIs of the imported ontologies
	 */
	public static List<String> getImports(final OntModel ontModel) {
		final List<Ontology> ontList = toList( ontModel.listOntologies() );
		final List<String> importUriList = new ArrayList<String>();
		for (Ontology ontology : ontList) {
			for (OntResource ontResource : toList( ontology.listImports() )) {
				importUriList.add(ontResource.getURI());
			}
		}
		return importUriList;
	}
	
	private static DecimalFormat df = new DecimalFormat("#.####");
	
	/** @return The double value as String with maximum 4 decimal digits */
	public static String doubleToString(Double doubleObj) {
		Checks.checkNotNullArg(doubleObj, "doubleObj");
		return df.format(doubleObj).replaceAll(",", ".");
	}
	
	/** @return The double based on string double given */
	public static Double doubleFromString(final String doubleStr) throws ParseException {
		Checks.checkNotNullArg(doubleStr, "doubleStr");
		return Double.parseDouble(doubleStr);
	}
	
	public static String makeString(final List<String> tokenList) {
		if (tokenList == null) return null;
		if (tokenList.isEmpty()) return "";
		final StringBuilder sb = new StringBuilder();
		for (String token : tokenList) {
			if (sb.length() > 0) sb.append(" ");
			sb.append(token);
		}
		return sb.toString();
		
	}
	
}
