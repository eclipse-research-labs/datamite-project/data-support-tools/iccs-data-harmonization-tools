package org.ntua.web.oat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A class with methods for loading data from web
 * 
 * @author Efthymios Chondrogiannis
 */
public class HttpRequest {

	private static final int BUFFER_SIZE=1024;
	
	/** Ensures we will not create an instance of this class */
	private HttpRequest() {
		
	}
	
	/** Executes an HTTP request and "gather" the data returned */
	public static String getPageContent(final String urlStr) throws IOException {
		// Site URL
		final URL siteUrl = new URL(urlStr);
		// Execute HTTP Request
		final HttpURLConnection conn = (HttpURLConnection) siteUrl.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("GET");
		// Read HTTP Response
		final StringBuilder responseSb = new StringBuilder();
		final BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF8"));
		final char[] buffer = new char[BUFFER_SIZE];
		int charsRead = 0;
		while ( (charsRead = br.read(buffer,0,BUFFER_SIZE)) != -1) {
			responseSb.append(buffer, 0, charsRead);
		}
	    br.close();		
		return responseSb.toString();
	}
	
	/** For testing purposes ... */
	public static void main(String[] args) throws Exception {
		final String url = "https://www.google.com/";
		System.out.println(getPageContent(url));
	}

}
