package org.ntua.web.oat.sim;

import static org.ntua.web.oat.ServletUtil.doubleToString;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntResource;
import org.apache.log4j.Logger;
import org.ntua.web.oat.LogFactory;
import org.ntua.web.oat.pat.onto.A_Pattern;
import org.ntua.web.oat.pat.onto.cls.SimpleOntoClassPattern;
import org.ntua.web.oat.pat.onto.dataprop.A_DataPropPattern;
import org.ntua.web.oat.pat.onto.indiv.SimpleIndivPattern;
import org.ntua.web.oat.pat.onto.objprop.A_ObjPropPattern;
import org.ntua.web.oat.sim.tech.HungarianAlgorithm;
import org.ntua.web.oat.sim.tech.LevenshteinDistance;
import org.ntua.web.oat.sim.tech.NGram;
import org.ntua.web.oat.sim.tech.PorterStemmer;
import org.timchros.core.tuple.Tuple2;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Checks;


/**
 * This class provides the necessary methods for retrieving the Similarity among the ontological elements
 * 
 * @author Thymios Chondrogiannis
 */
public class OntoSimilarityUtil {

	@SuppressWarnings("unused")
	private static final Logger log = LogFactory.getLoggerForClass(OntoSimilarityUtil.class);
	
	/** Ensures we will not create an instance of this class */
	private OntoSimilarityUtil() {
		
	}
	
	/** @return Similarity along with an explanation (HTML) data */
	public static <T extends OntResource> Tuple2<Double, String> getOntoPatternInstanceSimilarity(A_Pattern ontoPat1, A_Pattern ontoPat2, JSONObject jsonM, JSONObject jsonR) {
		Checks.checkNotNullArg(ontoPat1, "ontoPat1", ontoPat2, "ontoPat2", jsonM, "jsonM", jsonR, "jsonR");
		
		//log.trace("ontoPat1: " + ontoPat1 + " , ontoPat2: " + ontoPat2);
		
		// System.out.print("AA ");
		
		// Already Mapped - IGNORED
		if (OntoSimilarityUtil.existOntoPatternsInJson(ontoPat1, ontoPat2, jsonM)) 
			return Tuple2.newTuple2(1d, 
				"<h3 class='balloonH3'>OntoPatterns \"" + ontoPat1 + "\" and \"" + ontoPat2 + "\" are <b>already</b> Mapped ! (similarity 1)</h3>");
		
		//System.out.print("BB ");
		
		// Suggested in the past but REJECTED
		if (OntoSimilarityUtil.existOntoPatternsInJson(ontoPat1, ontoPat2, jsonR)) 
			return Tuple2.newTuple2(0d, 
				"<h3 class='balloonH3'>Mapping OntoPatterns \"" + ontoPat1 + "\" and \"" + ontoPat2 + "\" has been <b>rejected</b> in the past ! (similarity 0)</h3>");
		
		//System.out.print("CC ");
		
		// PROBLEM BETWEEN CC AND DD
		
		// CLASS
		if (ontoPat1 instanceof SimpleOntoClassPattern && ontoPat2 instanceof SimpleOntoClassPattern) {
			return getOntoResourceSimilarity( ((SimpleOntoClassPattern)ontoPat1).getOntoClass(), ((SimpleOntoClassPattern)ontoPat2).getOntoClass(), jsonM, jsonR);
		}
		
		//System.out.print("DD ");
		
		// OBJECT PROPERTY
		if (ontoPat1 instanceof A_ObjPropPattern && ontoPat2 instanceof A_ObjPropPattern) {
			final A_ObjPropPattern objPropPattern1 = (A_ObjPropPattern) ontoPat1;
			final A_ObjPropPattern objPropPattern2 = (A_ObjPropPattern) ontoPat2;
			final Tuple3<OntResource, String, OntResource> objPropImply1 = objPropPattern1.getImplyObjPropAttr();
			final Tuple3<OntResource, String, OntResource> objPropImply2 = objPropPattern2.getImplyObjPropAttr();
			return getOntoImplySimTuple("balloonH4O", objPropImply1, objPropImply2, jsonM, jsonR);
		}
		
		//System.out.print("EE ");
		
		// DATATYPE PROPERTY
		if (ontoPat1 instanceof A_DataPropPattern && ontoPat2 instanceof A_DataPropPattern) {
			final A_DataPropPattern objPropPattern1 = (A_DataPropPattern) ontoPat1;
			final A_DataPropPattern objPropPattern2 = (A_DataPropPattern) ontoPat2;
			final Tuple3<OntResource, String, OntResource> objPropImply1 = objPropPattern1.getImplyDataPropAttr();
			final Tuple3<OntResource, String, OntResource> objPropImply2 = objPropPattern2.getImplyDataPropAttr();
			return getOntoImplySimTuple("balloonH4D", objPropImply1, objPropImply2, jsonM, jsonR);
		}
		
		//System.out.print("FF ");
		
		// INDIVIDUALS
		if (ontoPat1 instanceof SimpleIndivPattern && ontoPat2 instanceof SimpleIndivPattern) {
			return getOntoResourceSimilarity( ((SimpleIndivPattern)ontoPat1).getIndividual(), ((SimpleIndivPattern)ontoPat2).getIndividual(), jsonM, jsonR);
		}
		
		//System.out.println();
		
		return Tuple2.newTuple2(0d, "");
	}
	
	public static Tuple2<Double, String> getOntoImplySimTuple(final String pCls, final Tuple3<OntResource, String, OntResource> propImply1, final Tuple3<OntResource, String, OntResource> propImply2, JSONObject jsonM, JSONObject jsonR) {
		Checks.checkNotNullArg(propImply1, "propImply1", propImply2, "propImply2");
		Checks.checkNotNullArg(propImply1._2(), "propImply1._2()", propImply2._2(), "propImply2._2()");

		// SIMILARITY AMONG KEY ELEMENTS
		final Tuple2<Double, String> propNameSim = 
			(propImply1._2() != null & propImply2._2() != null) 	
				? getSimilarity(propImply1._2(), propImply2._2()) 
				: Tuple2.newTuple2(0d, "<p>Similarity <b>0</b> - (Unexpected Problem) In one of two Properties NAME is NOT defined !</p>") ;
						
		final Tuple2<Double, String> domainSim = 
			(propImply1._1() != null && propImply2._1() != null) 
				? getOntoResourceSimilarity(propImply1._1(), propImply2._1(), jsonM, jsonR) : 
			(propImply1._1() == null && propImply2._1() == null) 
				? Tuple2.newTuple2(1d, "<p>Similarity <b>1</b> - The DOMAIN in both Properties is not defined - Hence match !</p>") 
				: Tuple2.newTuple2(0d, "<p>Similarity <b>0</b> - In one of two Properties DOMAIN is NOT defined !</p>") ;
						
		final Tuple2<Double, String> rangeSim = 
			(propImply1._3() != null && propImply2._3() != null) 
				? getOntoResourceSimilarity(propImply1._3(), propImply2._3(), jsonM, jsonR) : 
			(propImply1._3() == null && propImply2._3() == null) 
				? Tuple2.newTuple2(1d, "<p>Similarity <b>1</b> - The RANGE in both Properties is not defined - Hence match !</p>") 
				: Tuple2.newTuple2(0d, "<p>Similarity <b>0</b> - In one of two Properties RANGE is NOT defined !</p>") ;
		
		// OVERALL SIMILARITY
		final double sim = (double) ( (double) ( domainSim._1() + P * propNameSim._1() + rangeSim._1()) / (P + 2) );
		
		return Tuple2.newTuple2(sim, 
			"<h3 class='balloonH3'>Calculating similarity using Property NAME, DOMAIN and RANGE</h3>" +	
			"<p class='balloonP'><b>Formula:</b> ( Domain-Class-Sim + " + P + " * Prop-Name-Sim + Range-Class-Sim ) / " + ((int)P + 2) + " </p>" +
			"<p class='balloonP'><b>Similarity</b>: ( " + doubleToString(domainSim._1()) + " + " + P + " * " + doubleToString(propNameSim._1()) + " + " + doubleToString(rangeSim._1()) + " ) / " + ((int)P + 2) + " : <b>" + doubleToString(sim) +"</b></p>" +
			"<h4 class='" + pCls + "'>NAME Similarity:</h4>" + propNameSim._2() + 
			"<h4 class='balloonH4C'>Domain CLASS Similarity:</h4>" + domainSim._2() +
			"<h4 class='balloonH4C'>Range CLASS Similarity:</h4>" + rangeSim._2()
		);
	}
	
	/** Properties FACTOR */
	private static final int P = 2;
	
	/** Classes FACTOR */
//	private static final int C = 2;
	
	/** @return The similarity between {@link OntResource} provided taking into account existing mapping rules, mapping rules rejected and existing suggestions found */
	public static Tuple2<Double, String> getOntoResourceSimilarity(OntResource ontoRes1, OntResource ontoRes2, JSONObject jsonM, JSONObject jsonR) {
		Checks.checkNotNullArg(ontoRes1, "ontoRes1", ontoRes2, "ontoRes2", jsonM, "jsonM", jsonR, "jsonR");
		
		//log.trace("ontoRes1: " + ontoRes1 + " , ontoRes2: " + ontoRes2);
		
		final String ontRes1Lab = ontoRes1.getLabel(null);
		final String ontRes1Name = (ontRes1Lab != null) ? ontRes1Lab : ontoRes1.getLocalName();
		final String ontRes2Lab = ontoRes2.getLabel(null);
		final String ontRes2Name = (ontRes2Lab != null) ? ontRes2Lab : ontoRes2.getLocalName();
		
		// Already Mapped - IGNORED
		if (OntoSimilarityUtil.existOntoResourcesInJson(ontoRes1, ontoRes2, jsonM)) 
			return Tuple2.newTuple2(1d, 
				"<h3 class='balloonH3'>Entities \"" + ontRes1Name + "\" and \"" + ontRes2Name + "\" are <b>already</b> Mapped ! (similarity 1)</h3>");
		
		// Suggested in the past but REJECTED
		if (OntoSimilarityUtil.existOntoResourcesInJson(ontoRes1, ontoRes2, jsonR)) 
			return Tuple2.newTuple2(0d, 
				"<h3 class='balloonH3'>Mapping Entities \"" + ontRes1Name + "\" and \"" + ontRes2Name + "\" has been <b>rejected</b> in the past ! (similarity 0)</h3>");
		
		if (ontRes1Name == null || ontRes2Name == null) 
			return Tuple2.newTuple2(0d, "[ERROR] Cannot find the name of entities... ontRes1Name: " + ontRes1Name + " , ontRes2Name: " + ontRes2Name);
		
		// Find similarity among CLASSES based on their label 
		// using above (super) and below (sub) classes, if specified and being simple ones
		if (ontoRes1 instanceof OntClass && ontoRes2 instanceof OntClass) {
			// Similarity among Classes Label
			final Tuple2<Double, String> nameSim =  getSimilarity(ontRes1Name, ontRes2Name);
			
			//TODO: TAKE into account SUPER and SUB classes
//			final List<OntClass> superClsList1 = ontClassToList(((OntClass) ontoRes1).listSuperClasses());
//			final List<OntClass> superClsList2 = ontClassToList(((OntClass) ontoRes2).listSuperClasses());
//			Tuple2<Double, String> superMaxSim = null;
//			for (OntClass ontClass1 : superClsList1) {
//				final String lab1 = ontClass1.getLabel(null);
//				final String name1 = (lab1 != null) ? lab1 : ontClass1.getLocalName();
//				for (OntClass ontClass2 : superClsList2) {
//					final String lab2 = ontClass2.getLabel(null);
//					final String name2 = (lab2 != null) ? lab2 : ontClass2.getLocalName();
//					final Tuple2<Double, String> superNameSim =  getSimilarity(name1, name2);
//					if (superMaxSim == null) {
//						superMaxSim = superNameSim;
//					} else {
//						if (superNameSim._1() > superMaxSim._1()) {
//							superMaxSim = superNameSim;
//						}
//					}
//				}
//			}
			
			//TODO: TAKE into account properties they have
			
//			if (superMaxSim == null) {
				return Tuple2.newTuple2(nameSim._1(), 
					"<h3 class='balloonH3'>Calculating similarity using Classes NAME/LABEL: "  + doubleToString(nameSim._1()) + "</h3>" + nameSim._2());	
//			} else {
//				final double sim = (double) ( (double) ( C * nameSim._1() + superMaxSim._1()) / (C + 1) );
//				return Tuple2.newTuple2(sim, 
//					"<h3 class='balloonH3'>Calculating similarity using Classes NAME/LABEL as well as its SUPER (isa) classes: "  + doubleToString(nameSim._1()) + "</h3>" +	
//					"<p class='balloonP'><b>Formula:</b> ( " + C + " * Classes-Name-Sim + Best-Super-Class-Sim ) / " + ((int)C+2) + " </p>" +
//					"<p class='balloonP'><b>Similarity</b>: ( " + C + " * " + doubleToString(nameSim._1()) + " + " + doubleToString(superMaxSim._1()) + " ) / " + ((int)C+1) + " : <b>" + doubleToString(sim) +"</b></p>" +
//					"<h4 class='balloonH4C'>Class NAME Similarity:</h4>" + nameSim._2() + 
//					"<h4 class='balloonH4C'>Super class (best) Similarity:</h4>" + superMaxSim._2()
//				);
//			}
			
			//TODO: CHECK FOR INDIVIDUALS SIMILARITY (if presented)
		}
		
		// Find similarity among OBJECT PROPERTIES based on their label 
		// using both domain and their length, if specified and being simple ones
		if (ontoRes1 instanceof ObjectProperty && ontoRes2 instanceof ObjectProperty) {
			final ObjectProperty objProp1 = (ObjectProperty) ontoRes1;
			final OntResource objProp1domainCls = objProp1.getDomain();
			final OntResource objProp1rangeCls =  objProp1.getRange();
			final ObjectProperty objProp2 = (ObjectProperty) ontoRes2;
			final OntResource objProp2domainCls = objProp2.getDomain();
			final OntResource objProp2rangeCls = objProp2.getRange();
			
			//TODO: TAKE into account SUPER and SUB properties
			
			// SIMILARITY AMONG KEY ELEMENTS
			final Tuple2<Double, String> nameSim = 
				(ontRes1Name != null & ontRes2Name != null) 	
					? getSimilarity(ontRes1Name, ontRes2Name) 
					: Tuple2.newTuple2(0d, "<p>Similarity <b>0</b> - (Unexpected Problem) In one of two ObjectProperties NAME is NOT defined !</p>") ;
					
			final Tuple2<Double, String> domainSim = 
				(objProp1domainCls != null && objProp2domainCls != null) 
					? getOntoResourceSimilarity(objProp1domainCls, objProp2domainCls, jsonM, jsonR) : 
				(objProp1domainCls == null && objProp2domainCls == null) 
					? Tuple2.newTuple2(1d, "<p>Similarity <b>1</b> - The DOMAIN in both ObjectProperties is not defined - Hence match !</p>") 
					: Tuple2.newTuple2(0d, "<p>Similarity <b>0</b> - In one of two ObjectProperties DOMAIN is NOT defined !</p>") ;
					
			final Tuple2<Double, String> rangeSim = 
				(objProp1rangeCls != null && objProp2rangeCls != null) 
					? getOntoResourceSimilarity(objProp1rangeCls, objProp2rangeCls, jsonM, jsonR) : 
				(objProp1rangeCls == null && objProp2rangeCls == null) 
					? Tuple2.newTuple2(1d, "<p>Similarity <b>1</b> - The RANGE in both ObjectProperties is not defined - Hence match !</p>") 
					: Tuple2.newTuple2(0d, "<p>Similarity <b>0</b> - In one of two ObjectProperties RANGE is NOT defined !</p>") ;
					
			final double sim = (double) ( (double) ( domainSim._1() + P * nameSim._1() + rangeSim._1()) / (P + 2) );
			return Tuple2.newTuple2(sim, 
				"<h3 class='balloonH3'>Calculating similarity using ObjectProperty NAME, DOMAIN and RANGE</h3>" +	
				"<p class='balloonP'><b>Formula:</b> ( Domain-Class-Sim + " + P + " * Prop-Name-Sim + Range-Class-Sim ) / " + ((int)P+2) + " </p>" +
				"<p class='balloonP'><b>Similarity</b>: ( " + doubleToString(domainSim._1()) + " + " + P + " * " + doubleToString(nameSim._1()) + " + " + doubleToString(rangeSim._1()) + " ) / " + ((int)P+2) + " : <b>" + doubleToString(sim) +"</b></p>" +
				"<h4 class='balloonH4O'>NAME Similarity:</h4>" + nameSim._2() + 
				"<h4 class='balloonH4C'>Domain CLASS Similarity:</h4>" + domainSim._2() +
				"<h4 class='balloonH4C'>Range CLASS Similarity:</h4>" + rangeSim._2()
			);
				
		}

		// Find similarity among DATATYPE PROPERTIES based on their label 
		// using both domain and their length, if specified and being simple ones
		if (ontoRes1 instanceof DatatypeProperty && ontoRes2 instanceof DatatypeProperty) {
			final DatatypeProperty dataProp1 = (DatatypeProperty) ontoRes1;
			final OntResource dataProp1domainCls = dataProp1.getDomain();
			final OntResource dataProp1rangeCls = dataProp1.getRange();
			final DatatypeProperty dataProp2 = (DatatypeProperty) ontoRes2;
			final OntResource dataProp2domainCls = dataProp2.getDomain();
			final OntResource dataProp2rangeCls = dataProp2.getRange();
			
			//TODO: TAKE into account SUPER and SUB properties
			
			// SIMILARITY AMONG KEY ELEMENTS
			final Tuple2<Double, String> nameSim = 
				(ontRes1Name != null & ontRes2Name != null) 	
					? getSimilarity(ontRes1Name, ontRes2Name) 
					: Tuple2.newTuple2(0d, "<p>Similarity <b>0</b> - (Unexpected Problem) In one of two DatatypeProperties NAME is NOT defined !</p>") ;
			final Tuple2<Double, String> domainSim = 
				(dataProp1domainCls != null && dataProp2domainCls != null) 
					? getOntoResourceSimilarity(dataProp1domainCls, dataProp2domainCls, jsonM, jsonR) : 
				(dataProp1domainCls == null && dataProp2domainCls == null) 
					? Tuple2.newTuple2(1d, "<p>Similarity <b>1</b> - The DOMAIN in both DatatypeProperties is not defined - Hence match !</p>") 
					: Tuple2.newTuple2(0d, "<p>Similarity <b>0</b> - In one of two DatatypeProperties DOMAIN is NOT defined !</p>") ;
			final Tuple2<Double, String> rangeSim = 
				(dataProp1rangeCls != null && dataProp2rangeCls != null) 
					? getOntoResourceSimilarity(dataProp1rangeCls, dataProp2rangeCls, jsonM, jsonR) : 
				(dataProp1rangeCls == null && dataProp2rangeCls == null) 
					? Tuple2.newTuple2(1d, "<p>Similarity <b>1</b> - The RANGE in both DatatypeProperties is not defined - Hence match !</p>") 
					: Tuple2.newTuple2(0d, "<p>Similarity <b>0</b> - In one of two DatatypeProperties RANGE is NOT defined !</p>") ;
				
			final double sim = (double) ( (double) ( domainSim._1() + P * nameSim._1() + rangeSim._1()) / (P + 2) );
			return Tuple2.newTuple2(sim, 
				"<h3 class='balloonH3'>Calculating similarity using DatatypeProperty DOMAIN, NAME and RANGE</h3>" +	
				"<p class='balloonP'><b>Formula:</b> ( Domain-Class-Sim + " + P + " * Prop-Name-Sim + Range-Type-Sim ) / " + ((int)P+2) + " </p>" +
				"<p class='balloonP'><b>Similarity</b>: ( " + doubleToString(domainSim._1()) + " + " + P + " * " + doubleToString(nameSim._1()) + " + " + doubleToString(rangeSim._1()) + " ) / " + ((int)P+2) + " : <b>" + doubleToString(sim) +"</b></p>" +
				"<h4 class='balloonH4D'>NAME Similarity:</h4>" + nameSim._2() + 
				"<h4 class='balloonH4C'>Domain CLASS Similarity:</h4>" + domainSim._2() +
				"<h4 class='balloonH4C'>Range CLASS Similarity:</h4>" + rangeSim._2()
			);
		}
		
		// Find similarity among INDIVIDUALS based on their label 
		// using both individual and the class it belongs
		if (ontoRes1 instanceof Individual && ontoRes2 instanceof Individual) {
			final Tuple2<Double, String> nameSim = getSimilarity(ontRes1Name, ontRes2Name);
			final Individual indiv1 = (Individual) ontoRes1;
			final Individual indiv2 = (Individual) ontoRes2;
			final Tuple2<Double, String> classSim = getOntoResourceSimilarity(indiv1.getOntClass(), indiv2.getOntClass(), jsonM, jsonR);
			final double sim = ( (double) ( (double) ( nameSim._1() + classSim._1()) / 2 ) );
			return Tuple2.newTuple2(sim, 
				"<h3 class='balloonH3'>Calculating similarity using Individuals NAME and CLASS</h3>" +	
				"<p class='balloonP'><b>Formula:</b> ( Individuals-Name-Sim + Individuals-Class-Sim ) / 2 </p>" +
				"<p class='balloonP'><b>Similarity</b>: ( " + doubleToString(nameSim._1()) + " + " + doubleToString(classSim._1()) + " ) / 2 : <b>" + doubleToString(sim) + "</b></p>" +
				"<h4 class='balloonH4>Individuals NAME Similarity:</h4>" + nameSim._2() + 
				"<h4 class='balloonH4>Individuals CLASS Similarity:</h4>" + classSim._2()
			);
		}
		
		return getSimilarity(ontRes1Name, ontRes2Name);
	}
	
	/* SIMILARITY AMONG PHRASES */
	
	@SuppressWarnings("unused")
	public static final Tuple2<Double, String> getSimilarity(String phrase1, String phrase2) {
		try{
		
		if (phrase1 == null || phrase2 == null) return Tuple2.newTuple2(0d, "Null Arguments");
		if (phrase1.trim().equals("") || phrase2.trim().equals("")) return Tuple2.newTuple2(0d, "Empty Arguments");
		
		final String phrase1toLowerCase = phrase1.toLowerCase();
		final String phrase2toLowerCase = phrase2.toLowerCase();
		
		if (phrase1toLowerCase.equals(phrase2toLowerCase)) return Tuple2.newTuple2(1d, "<p>Similarity 1 - Same sequence of characters.</p>");
		
		if (true) return Tuple2.newTuple2(0d, "no check");
		
		// Tokenization (tokens of phrase) & Elimination (ignore stop words)
		final List<String> str1TokenList = getMainTokens(phrase1toLowerCase);
		final List<String> str2TokenList = getMainTokens(phrase2toLowerCase);
		
		String[] str1TokenArray = str1TokenList.toArray(new String[str1TokenList.size()]);
		String[] str2TokenArray = str2TokenList.toArray(new String[str2TokenList.size()]);
		
		final StringBuilder simSb = new StringBuilder();
		simSb.append("<h5>Similarity among tokens</h5>");
		simSb.append("<p><b>Formula:</b> ( Stems-EditDistance-Similarity + Stems-N-3-Gram-Similarity ) / 2 </p>");
		simSb.append("<ul>");
		
		// Hungarian Alogithm
		double[][] similarityMatrix = new double[str1TokenArray.length][str2TokenArray.length];
		for (int i = 0; i < str1TokenArray.length; i++) {
			final String str1Token = str1TokenArray[i];
			for (int j = 0; j < str2TokenArray.length; j++) {
				final String str2Token = str2TokenArray[j];
				final Tuple2<Float, String> simTuple = getSimilarityAmongWords(str1Token, str2Token);
				float similarity = simTuple._1();
				simSb.append("<li>" + simTuple._2() + "</li>");
				similarityMatrix[i][j] = similarity;
			}
		}
		simSb.append("</ul>");
		
		String sumType = "max";		
		double[][] array = similarityMatrix;		
		if (array.length > array[0].length) {
			array = HungarianAlgorithm.transpose(array);
		}
		int[][] assignment = new int[array.length][2];
		assignment = HungarianAlgorithm.hgAlgorithm(array, sumType);	//Call Hungarian algorithm.
		
		final StringBuilder matrixSb = new StringBuilder();
		matrixSb.append("<table>");
		matrixSb.append("<tr><th><br></th>");
		for (int j = 0; j < str2TokenArray.length; j++) {
			String str2Token = str2TokenArray[j];
			matrixSb.append("<th>" + str2Token + "</th>");
		}
		matrixSb.append("</tr>");
		
		for (int i = 0; i < str1TokenArray.length; i++) {
			String str1Token = str1TokenArray[i];
			matrixSb.append("<tr><th>" + str1Token + "</th>");
			for (int j = 0; j < str2TokenArray.length; j++) {
				double sim = similarityMatrix[i][j];
				// Check if matching selected
				boolean existsInBestAssignMap = false;
				for (int k=0; k < assignment.length; k++) {
					if ( (assignment[k][0] == i) && (assignment[k][1] == j) ) {
						existsInBestAssignMap = true; 
						break;
					}
				}
				// Properly present similarity
				if (existsInBestAssignMap) {
					matrixSb.append("<td><span style='color: darkmagenta; font-weight: bold;'>" + doubleToString(sim) + "</span></td>");	
				} else {
					matrixSb.append("<td>" + doubleToString(sim) + "</td>");
				}
			}
			matrixSb.append("</tr>");
		}
		matrixSb.append("</table>");
		matrixSb.append("<p>Stop words (if presented) are being ignored</p>");
		
		double sum = 0;
		String sumStr = "";
		for (int i=0; i<assignment.length; i++) {
			sum += array[assignment[i][0]][assignment[i][1]];
			if (sumStr.length() > 0) sumStr += " + ";
			sumStr += doubleToString(array[assignment[i][0]][assignment[i][1]]);
		}
		final int maxTokens = Math.max(str1TokenArray.length, str2TokenArray.length);
		double phraseSimilarity = (double) ( sum / maxTokens );			
		
		final StringBuilder sb =  new StringBuilder();
		sb.append("<div class='balloonDiv'>");
		sb.append("<h5>Phrases Similarity based on Hungarian Algorithm:</h5>"); 
		sb.append("<p><b>Phrases:</b> \"" + phrase1 + " and \"" + phrase2 + "\" </p>");
		sb.append("<p><b>Formula:</b> Best-Token-Matching-Similarity-Sum / Max-Tokens-Number </p>");
		sb.append("<p><b>Similarity:</b> <code>( " + sumStr + " ) / " + maxTokens + " = " + doubleToString(sum) + " / " + maxTokens + " = " + doubleToString(phraseSimilarity) + "</code> </p>");
		sb.append(matrixSb.toString());
		sb.append(simSb.toString());
		sb.append("</div>");
		
		return Tuple2.newTuple2(phraseSimilarity, sb.toString());
		
		} catch (Throwable t) {
			return Tuple2.newTuple2(0d, "Unexpected Error: " + t.getMessage());
		}
	}
		
	/**
	 * Finds the main <b>tokens</b> of the given phrase, ignoring <b>punctuation</b> characters and <b>stop words</b>.
	 * 
	 * @param phrase
	 * 		A human phrase
	 * @return
	 * 		The main tokens of the phase
	 */
	private static final List<String> getMainTokens(String phrase) {
		final List<String> tokenList = new ArrayList<String>();
		// Punctuation characters are ignored during the parsing... splitting of the given phrase
		final String splitRegex = "[\\s\\t\\n\\.,;?!><=/]+";
		final String[] tokenArray = phrase.split(splitRegex);
		for (int i = 0; i < tokenArray.length; i++) {
			final String token = tokenArray[i];
			if (isStopWord(token)) continue;
			tokenList.add(token);
		}
		return tokenList;
	}
	
	/**
	 * Checks if a word is a <b>stop word</b> or not.
	 * <p>
	 * Stop Words for English Language downlaoded from Wikipedia.
	 * 
	 * @param word
	 * 		A String of characters
	 * @return
	 * 		<code>true</code> if the word provided is a <b>stop word</b>, otherwise <code>false</code>
	 * @see 
	 * 		Stop Words definition, available at http://en.wikipedia.org/wiki/Stop_words
	 */
	private static boolean isStopWord(String word) {
		final List<String> stopWordList = org.timchros.core.util.Util.newArrayList(
			"a", "able", "about", "across", "after", "all", "almost", "also", "am", "among", "an", "and", "any", "are",  "as", "at", "be", "because", "been", "but", "by", "can", "cannot", "could", "dear", "did", "do", "does", "either", "else", "ever", "every", "for", "from", "get", "got", "had", "has", "have", "he", "her", "hers", "him", "his", "how", "however", "i", "if", "in", "into", "is", "it", "its", "just", "least", "let", "like", "likely", "may", "me", "might", "most", "must", "my", "neither", "no", "nor", "not", "of", "off", "often", "on", "only", "or", "other", "our", "own", "rather", "said", "say", "says", "she", "should", "since", "so", "some", "than", "that", "the", "their", "them", "then", "there", "these", "they", "this", "tis", "to", "too", "twas", "us", "wants", "was", "we", "were", "what", "when", "where", "which", "while", "who", "whom", "why", "will", "with", "would", "yet", "you", "your"
		);
		return stopWordList.contains(word);
	}
	
	/**
	 * Checks if the given String is a ("main") <b>punctuation</b> character or not.
	 * 
	 * @param str
	 * 		A String of characters
	 * @return
	 * 		<code>true</code> if the given String is a punctuation character, otherwise <code>false</code>
	 */
	@SuppressWarnings("unused")
	private static boolean isPunctuationCharacter(String str) {
		final List<String> punctuationCharacterList = org.timchros.core.util.Util.newArrayList(
			".", ",", ";", "?", "!", "(", ")", "[", "]", "|" , "/", "\\", ">", "<", "="
		);
		return punctuationCharacterList.contains(str);
	}
	
	/**
	 * Finds the similarity between the two words provided, using the following techniques
	 * <p>
	 * <ul>
	 * 		<li>Porter Stemming Algorithm</li>
	 * 		<li>Levenshtein Distance Algoirithm</li>
	 * 		<li>Prefix - Suffix</li>
	 * </ul>
	 * This method, initially retrieves the <i>stem</i> of the given words and accordingly 
	 * calculates the <i>edit distance</i> among the two given stemmed strings.
	 * The similarity calculated is increased in case one Strings <i>start or ends</i> with the other one.
	 * 
	 * @param word1
	 * 		A String of characters
	 * @param word2
	 * 		A String of characters
	 * @return
	 * 		The similarity (a float number between 0 and 1) between the two Strings
	 */
	@SuppressWarnings("unused")
	public static Tuple2<Float, String> getSimilarityAmongWords(String word1, String word2) {
		if (word1 == null || word2 == null) return Tuple2.newTuple2(0f, "Null Tokens.");
		if (word1.equals(word2)) return Tuple2.newTuple2(1f, "\"" + word1 + "\" , \"" + word2 + "\": <b>1</b> , since tokens are the <b>same</b>.");
		// Stemming... Porter Stemming Algorithm
		final String word1stem = getStemmedWord(word1);
		final String word2stem = getStemmedWord(word2);
		if (word1stem.equals(word2stem)) return Tuple2.newTuple2(1f, "\"" + word1 + "\" , \"" + word2 + "\": <b>1</b> , since tokens have the <b>same stem</b>.");
		
		// For accelerating similarity detection process
		if (true) return Tuple2.newTuple2(0f, "");
		
		// EditDistance... Levenshtein Distance Algoirithm
		final float editDistanceSim = getEditDistanceSimilarity(word1stem, word2stem);
		final double n3gramSim = getN3GramSimilarity(word1stem, word2stem);
		final float avgSim = (float) (editDistanceSim + n3gramSim) / 2;
		final StringBuilder sb = new StringBuilder();
		sb.append("\"" + word1 + "\" , \"" + word2 + "\": ( " + 
			doubleToString((double)editDistanceSim) + " + " + 
			doubleToString((double)n3gramSim) + " ) / 2 = <b>" + doubleToString((double)avgSim) + "</b>");
		return Tuple2.newTuple2(avgSim, sb.toString());
	}
	
	/* SIMILARITY DETECTION ALGORITHMS / TECHNIQUES */
	
	/**
	 * Finds the stem of the given word based on the <b>Porter Stemming Algorithm</b>.
	 * 
	 * @param word
	 * 		A String of characters
	 * @return
	 * 		The stem of the given String
	 * @see
	 * 		Stemming, available at http://en.wikipedia.org/wiki/Stemming
	 */
	private static String getStemmedWord(String word) {
		PorterStemmer stemmer = new PorterStemmer();
		stemmer.add(word.toCharArray(), word.length());
		stemmer.stem();
		String stemmedWord = stemmer.toString();
		if (stemmedWord.endsWith("'")) stemmedWord = stemmedWord.substring(0, stemmedWord.length()-1);
		return stemmedWord;
	}
	
	/**
	 * Finds the similarity (a float number between 0 and 1) based on the Edit Distance between two string of characters.
	 * <p>
	 * In order to find the edit distance between Strings we have used <b>Levenshtein Distance</b>.
	 * <p>
	 * The similarity arises by the division of edit distance with the maximum length of the two strings.
	 * <p>
	 * <code>similarity: 1 - ( EditDistance / Maximum String Length )</code>
	 * 
	 * @param str1
	 * 		The first String
	 * @param str2
	 * 		The second String
	 * @return
	 * 		The similarity among the given Strings based on the Levenshtein Distance alogirthm.
	 * @see
	 * 		Edit Distance, available at http://en.wikipedia.org/wiki/Edit_distance
	 */
	private static float getEditDistanceSimilarity(String str1, String str2) {
		int distance = LevenshteinDistance.computeLevenshteinDistance(str1, str2);
		int length = Math.max(str1.length(), str2.length());
		return ( 1 - ( (float) distance / length ) );
	}
	
	/**
	 * Finds the similarity between the given Strings based on <b>N-3-Gram</b>.
	 * 
	 * @param str1
	 * 		A String of characters
	 * @param str2
	 * 		A String of characters
	 * @return
	 * 		The similarity of the given Strings using N-3-Gram.
	 * @see
	 * 		N-Gram, available at http://en.wikipedia.org/wiki/N-gram
	 */
	private static double getN3GramSimilarity(String str1, String str2) {
		NGram nGram = new NGram();
		double n3gramSimilarity = nGram.getSimilarity(str1, str2, 3);
		return n3gramSimilarity;
	}
	
	/* HANDLE MAPPING RULES SPECIFIED OR REJECTED */
	
	/** @return <code>true</code> if ontological ELEMENTS exist in JSON map, otherwise <code>false</code> */
	public static <T extends OntResource> boolean existOntoResourcesInJson(T o1, T o2, JSONObject mappingJson) {
		final JSONArray mappings = mappingJson.getJSONArray("jsonarray");
		// CLASSES
		if (o1.canAs(OntClass.class) && o2.canAs(OntClass.class)) {
			final String o1clsUri = o1.as(OntClass.class).getURI();
			final String o2clsUri = o2.as(OntClass.class).getURI();
			if (o1clsUri == null || o2clsUri == null) return false;
			for (Object object : mappings) {
				final JSONObject correspJson = (JSONObject) object;
				final JSONObject entity1Json = correspJson.getJSONObject("entity1");
				final JSONObject entity2Json = correspJson.getJSONObject("entity2");
				if ("SimpleClassPattern".equals(entity1Json.getString("pid")) && "SimpleClassPattern".equals(entity2Json.getString("pid")) ) {
					final String entity1clsUri = entity1Json.getString("classuri");
					final String entity2clsUri = entity2Json.getString("classuri");
					if (o1clsUri.equals(entity1clsUri) && o2clsUri.equals(entity2clsUri)) return true; 
				}	
			}
		}
		// OBJECT PROPERTIES
		if (o1.canAs(ObjectProperty.class) && o2.canAs(ObjectProperty.class)) {
			final String o1propUri = o1.as(ObjectProperty.class).getURI();
			final String o2propUri = o2.as(ObjectProperty.class).getURI();
			if (o1propUri == null || o2propUri == null) return false;
			for (Object object : mappings) {
				final JSONObject correspJson = (JSONObject) object;
				final JSONObject entity1Json = correspJson.getJSONObject("entity1");
				final JSONObject entity2Json = correspJson.getJSONObject("entity2");
				if ("SimpleRelationPattern".equals(entity1Json.getString("pid")) && "SimpleRelationPattern".equals(entity2Json.getString("pid")) ) {
					final String entity1propUri = entity1Json.getString("relationuri");
					final String entity2propUri = entity2Json.getString("relationuri");
					if (o1propUri.equals(entity1propUri) && o2propUri.equals(entity2propUri)) return true; 
				}	
			}
		}
		// DATATYPE PROPERTIES
		if (o1.canAs(DatatypeProperty.class) && o2.canAs(DatatypeProperty.class)) {
			final String o1propUri = o1.as(DatatypeProperty.class).getURI();
			final String o2propUri = o2.as(DatatypeProperty.class).getURI();
			if (o1propUri == null || o2propUri == null) return false;
			for (Object object : mappings) {
				final JSONObject correspJson = (JSONObject) object;
				final JSONObject entity1Json = correspJson.getJSONObject("entity1");
				final JSONObject entity2Json = correspJson.getJSONObject("entity2");
				if ("SimplePropertyPattern".equals(entity1Json.getString("pid")) && "SimplePropertyPattern".equals(entity2Json.getString("pid")) ) {
					final String entity1propUri = entity1Json.getString("propertyuri");
					final String entity2propUri = entity2Json.getString("propertyuri");
					if (o1propUri.equals(entity1propUri) && o2propUri.equals(entity2propUri)) return true;
				}	
			}
		}
		// INDIVIDUALS
		if (o1.canAs(Individual.class) && o2.canAs(Individual.class)) {
			final String o1indivUri = o1.as(Individual.class).getURI();
			final String o2indivUri = o2.as(Individual.class).getURI();
			if (o1indivUri == null || o2indivUri == null) return false;
			for (Object object : mappings) {
				final JSONObject correspJson = (JSONObject) object;
				final JSONObject entity1Json = correspJson.getJSONObject("entity1");
				final JSONObject entity2Json = correspJson.getJSONObject("entity2");
				if ("SimpleInstancePattern".equals(entity1Json.getString("pid")) && "SimpleInstancePattern".equals(entity2Json.getString("pid")) ) {
					final String entity1propUri = entity1Json.getString("instanceuri");
					final String entity2propUri = entity2Json.getString("instanceuri");
					if (o1indivUri.equals(entity1propUri) && o2indivUri.equals(entity2propUri)) return true;
				}	
			}
		}
		return false;
	}
	
	/** @return <code>true</code> if ontological PATTERNS exist in JSON map, otherwise <code>false</code> */
	public static boolean existOntoPatternsInJson(A_Pattern ap1, A_Pattern ap2, JSONObject mappingJson) {
		final JSONArray mappings = mappingJson.getJSONArray("jsonarray");
		for (Object object : mappings) {
			final JSONObject correspJson = (JSONObject) object;
			final JSONObject entity1Json = correspJson.getJSONObject("entity1");
			final JSONObject entity2Json = correspJson.getJSONObject("entity2");
			if (ap1.equalsWithJson(entity1Json) && ap2.equalsWithJson(entity2Json)) return true;
		}
		return false;
	}
	
}
