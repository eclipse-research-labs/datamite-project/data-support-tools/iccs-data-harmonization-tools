package org.ntua.web.oat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

/**
 * This class provides useful method for loading/saving files
 * 
 * @author Efthymios Chondrogiannis
 */
public class FilesUtil {
	
	private static final int BUFFER_SIZE=1024;

	/** Save data provided into a UTF-8 (text) File in the location specified. */
	public static void saveToFileUTF8(final String folderPath, final String name, final String pageContent) throws IOException {
		if (name == null || pageContent == null) return;
		final String path = folderPath +  "/" + name;
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(path), "UTF8"));
			writer.write(pageContent);
			writer.flush();
		} finally {
			if (writer != null) try { writer.close(); } catch (Throwable tt) {  }
		}
	}
	
	/** @return A String with UTF-8 (text) data existing in File specified. */
	public static String getUTF8TextFromFile(final String path) throws IOException {
		if (path == null) return null;
		BufferedReader br = null;
		try {
			final StringBuilder responseSb = new StringBuilder();
			br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"));
			final char[] buffer = new char[BUFFER_SIZE];
			int charsRead = 0;
			while ( (charsRead = br.read(buffer,0,BUFFER_SIZE)) != -1) {
				responseSb.append(buffer, 0, charsRead);
			}
		    br.close();		
			return responseSb.toString();
		} finally {
			if (br!=null ) { try { br.close(); } catch(Throwable bre){ throw new RuntimeException("Error in br.close();", bre); } }
		}
	}
	
	/** For testing purposes ... */
	public static void main(String[] args) throws Exception {
		final String path = "./WebContent/WEB-INF/web.xml";
		System.out.println(getUTF8TextFromFile(path));
	}

}
