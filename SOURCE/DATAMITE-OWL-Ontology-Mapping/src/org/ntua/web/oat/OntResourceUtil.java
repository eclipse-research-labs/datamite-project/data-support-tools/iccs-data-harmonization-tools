package org.ntua.web.oat;

import static org.ntua.web.oat.ServletUtil.getLabelOrLocalName;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.jena.ontology.AllValuesFromRestriction;
import org.apache.jena.ontology.CardinalityRestriction;
import org.apache.jena.ontology.ComplementClass;
import org.apache.jena.ontology.HasValueRestriction;
import org.apache.jena.ontology.IntersectionClass;
import org.apache.jena.ontology.MaxCardinalityRestriction;
import org.apache.jena.ontology.MinCardinalityRestriction;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.ontology.Restriction;
import org.apache.jena.ontology.SomeValuesFromRestriction;
import org.apache.jena.ontology.UnionClass;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.shared.Lock;
import org.apache.log4j.Logger;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Checks;

public class OntResourceUtil {

	private static final Logger log = LogFactory.getLoggerForClass(OntResourceUtil.class);
	
	public static void main(String[] args) {
		
	}
	
	/**
	 * Produces an HTML representation of the class given.
	 * 
	 * @param ontModel
	 * 		The {@link OntModel}
	 * @param ontClass
	 * 		The {@link OntClass}
	 * @return
	 * 		An HTML representation of the class provided
	 */
	public static String getOntClassAsHtml(OntModel ontModel, OntClass ontClass) {
		Checks.checkNotNullArg(ontModel, "ontModel", ontClass, "ontClass");
		ontClass.getModel().enterCriticalSection(Lock.READ);
    	try {
	    	if (ontClass.isAnon()) {
		    	if (ontClass.isRestriction()) {   		
		    		final Restriction rest = ontClass.asRestriction();
		    		final OntProperty onProperty = rest.getOnProperty();
		    		String labelStr = "<a href=\"" + onProperty.getURI() + "\">" + getLabelOrLocalName(onProperty) + "</a>";
		    		// Universal and Existential Restriction
		    		if (rest.isAllValuesFromRestriction() || rest.isSomeValuesFromRestriction()) { 
			    		Resource fillerRes = null;
			    		if (rest.isAllValuesFromRestriction()) {
			    			AllValuesFromRestriction avfRest = rest.asAllValuesFromRestriction();
			    			fillerRes = avfRest.getAllValuesFrom();
			    			labelStr += " <div class=\"operator\">only</div> ";
			    		} else {
			    			SomeValuesFromRestriction svfRest = rest.asSomeValuesFromRestriction();
			    			fillerRes = svfRest.getSomeValuesFrom();
			    			labelStr += " <div class=\"operator\">some</div> ";
			    		}
			    		if (fillerRes.canAs(OntClass.class)) { 
		    				OntClass avf = (OntClass) fillerRes.as(OntClass.class);
		    				labelStr += getOntClassAsHtml(ontModel, avf);
		    			} else {
		    				try {
		    					labelStr += "<span style='font-size: 0.8em'>" + getLabelOrLocalName( (OntResource) fillerRes.as(OntResource.class)) + "</span>";
		    				} catch (Exception e) {
		    					labelStr += " ??? ";
		    				}
		    			}
			    	// Value Restriction
		    		} else if (rest.isHasValueRestriction()) {
		    			HasValueRestriction hvRest = rest.asHasValueRestriction();
		    			labelStr += " <div class=\"operator\">value</div> ";
		    			final RDFNode fillerNode = hvRest.getHasValue();
			    		if (fillerNode.isResource()) {
			    			OntResource ontRes = fillerNode.as(OntResource.class);
			    			labelStr += "<a href=\"" + ontRes.getURI() + "\">" + getLabelOrLocalName(ontRes) + "</a>";;
			    		} else {
			    			labelStr += ((Literal) fillerNode.as(Literal.class)).getLexicalForm(); 
			    		}
		    		// Cardinality Restriction
		    		} else if (rest.isMinCardinalityRestriction()) {
		    			final MinCardinalityRestriction mcRest = rest.asMinCardinalityRestriction();
		    			labelStr += " <div class=\"operator\">min</div> ";
		    			labelStr += " <div class=\"number\">" + mcRest.getMinCardinality() + "</div> ";
		    		} else if (rest.isMaxCardinalityRestriction()) {
		    			final MaxCardinalityRestriction mcRest = rest.asMaxCardinalityRestriction();
		    			labelStr += " <div class=\"operator\">max</div>";
		    			labelStr += " <div class=\"number\">" + mcRest.getMaxCardinality() + "</div> ";
		    		} else if (rest.isCardinalityRestriction()) {
		    			final CardinalityRestriction cRest = rest.asCardinalityRestriction();
		    			labelStr += " <div class=\"operator\">exactly</div> ";
		    			labelStr += " <div class=\"number\">" + cRest.getCardinality() + "</div> ";
		    		}
		    		// Cardinality Q Restriction
		    		else if (isCardinalityQRestriction(ontModel, rest)) {
		    			final Tuple3<String, Literal, Resource> restTuple3 = asCardinalityQRestriction(ontModel, rest);
		    			labelStr += " <div class=\"operator\">" + restTuple3._1() + "</div> ";
		    			labelStr += " <div class=\"number\">" + restTuple3._2().getLexicalForm() + "</div> ";
		    			final Resource fillerRes = restTuple3._3();
		    			if (fillerRes.canAs(OntClass.class)) { 
		    				OntClass avf = (OntClass) fillerRes.as(OntClass.class);
		    				if (avf.isOntLanguageTerm()) {
		    					labelStr += "<a href=\"http://www.w3.org/TR/xmlschema-2/#" + avf.getLocalName() + "\" target=\"_blank\">" + avf.getLocalName() + "</a>";
		    				} else {
		    					labelStr += getOntClassAsHtml(ontModel, avf);
		    				}
		    			} else {
		    				// TODO: Check - FIX
		    				try {
		    					labelStr += "<a href=\"http://www.w3.org/TR/xmlschema-2/#" + fillerRes.getLocalName() + "\" target=\"_blank\">" + fillerRes.getLocalName() + "</a>";
		    				} catch (Exception e) {
		    					labelStr += " ??? ";
		    				}
		    			}
		    		}
		    		return labelStr;
		    	} else if (isBooleanClassExpression(ontClass)) {
		    		String htmlStr = "( ";
		    		if (ontClass.isComplementClass()) {
		    			htmlStr += "<div class=\"operator\">not</div> ";
		    			final ComplementClass ccls = (ComplementClass) ontClass.as(ComplementClass.class);
		    			htmlStr += getOntClassAsHtml(ontModel, ccls.getOperand());		    			
		    		} else if (ontClass.isIntersectionClass()) {
		    			final IntersectionClass icls = (IntersectionClass) ontClass.as(IntersectionClass.class);
		    			for (Iterator<? extends OntClass> operandIt = icls.listOperands(); operandIt.hasNext();) {
		    				final OntClass operand = (OntClass) operandIt.next();
		    				htmlStr += getOntClassAsHtml(ontModel, operand);
		    				if (operandIt.hasNext()) {
		    					htmlStr += " <div class=\"operator\">and</div> ";
		    				}
		    			}
		    		} else if (ontClass.isUnionClass()) {
		    			final UnionClass icls = (UnionClass) ontClass.as(UnionClass.class);
		    			for (Iterator<? extends OntClass> operandIt = icls.listOperands(); operandIt.hasNext();) {
		    				final OntClass operand = (OntClass) operandIt.next();
		    				htmlStr += getOntClassAsHtml(ontModel, operand);
		    				if (operandIt.hasNext()) {
		    					htmlStr += " <div class=\"operator\">or</div> ";
		    				}
		    			}
		    		}
		    		htmlStr += " )";
		    		return htmlStr;
		    	} else {
		    		return "<a>Under Construction !!</a>";
		    	}
	    	} else {
	    		if (ontClass.isOntLanguageTerm()) { 
	    			return "<a href=\"http://www.w3.org/TR/xmlschema-2/#" + ontClass.getLocalName() + "\" target=\"_blank\">" + ontClass.getLocalName() + "</a>";
	    		} else {
	    			return "<a href=\"" + ontClass.getURI() + "\">" + getLabelOrLocalName(ontClass) + "</a>";
	    		}
	    	}
    	} catch (Exception e) {
    		log.warn("Unable to produce HTML view of class: [" + ontClass.getClass() + "]");
    		return "??????";
    	} finally {
    		ontClass.getModel().leaveCriticalSection();
    	}
	}
    
	/** @return <code>true</code> if {@link OntClass} provided is the Complement of a class or it consists of a Union or Intersection of classes, otherwise <code>false</code> */
    private static synchronized boolean isBooleanClassExpression(OntClass cls) {
    	return (cls.isComplementClass() || cls.isIntersectionClass() || cls.isUnionClass());
    }

	public static boolean isCardinalityQRestriction(OntModel ontModel, Restriction restriction) {
		List<String> predicateList = new ArrayList<String>();
		for (StmtIterator i = ontModel.listStatements(restriction, null, (RDFNode) null); i.hasNext(); ) {
        	Statement stmt = i.nextStatement();
        	predicateList.add(stmt.getPredicate().getURI());
    	}
		if (predicateList.contains("http://www.w3.org/2002/07/owl#qualifiedCardinality")) return true;
		if (predicateList.contains("http://www.w3.org/2002/07/owl#minQualifiedCardinality")) return true;
		if (predicateList.contains("http://www.w3.org/2002/07/owl#maxQualifiedCardinality")) return true;
		return false;
	}
	
	private static Tuple3<String, Literal, Resource> asCardinalityQRestriction(OntModel ontModel, Restriction restriction) {
		List<String> predicateList = new ArrayList<String>();
		StmtIterator stmtIt = ontModel.listStatements(restriction, null, (RDFNode) null);
		List<Statement> stmtList = new ArrayList<Statement>();
		while ( stmtIt.hasNext() ) {
        	Statement stmt = stmtIt.nextStatement();
        	predicateList.add(stmt.getPredicate().getURI());
        	stmtList.add(stmt);
    	}
		String operator = null;
		Literal value = null;
		for (Statement stmt : stmtList) {
        	if (stmt.getPredicate().getURI().equals("http://www.w3.org/2002/07/owl#qualifiedCardinality")) {
        		operator = "exactly";
        		value = stmt.getObject().asLiteral();
        		break;
        	}
        	if (stmt.getPredicate().getURI().equals("http://www.w3.org/2002/07/owl#minQualifiedCardinality")) {
        		operator = "min";
        		value = stmt.getObject().asLiteral();
        		break;
        	}
        	if (stmt.getPredicate().getURI().equals("http://www.w3.org/2002/07/owl#maxQualifiedCardinality")) {
        		operator = "max";
        		value = stmt.getObject().asLiteral();
        		break;
        	}
    	}
		Resource type =
				(restriction.getOnProperty().isObjectProperty())
					? getStatementObject(stmtList, "http://www.w3.org/2002/07/owl#onClass").asResource()
					: getStatementObject(stmtList, "http://www.w3.org/2002/07/owl#onDataRange").asResource();
		
		return new Tuple3<String, Literal, Resource>(operator, value, type);
	}
	
	private static RDFNode getStatementObject(List<Statement> stmtList, String predicateUri) {
		for (Statement stmt : stmtList) {
			if (stmt.getPredicate().getURI().equals(predicateUri)) return stmt.getObject();
		}
		return null;
	}
    
}
