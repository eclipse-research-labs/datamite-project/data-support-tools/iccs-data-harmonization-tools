package org.ntua.web.oat;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.timchros.core.util.Checks;
import org.timchros.core.util.Format;

/**
 * The OAT Logging System
 * 
 * @author Efthymios Chondrogiannis
 */
public class LogFactory {

	/* Logging System Configuration */
	static {
		try {
		    final Properties prop  = new Properties();
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("log4j.properties"));
			PropertyConfigurator.configure(prop);
			getLoggerForClass(LogFactory.class).debug(" ------------------------------- OAT ----------------------------");
			getLoggerForClass(LogFactory.class).info("Logging System successfully initialized !");
		} catch(Throwable t) {
			// TODO: Comment for running locally
			// throw new RuntimeException("Unable to properly configure log4j.", t);
		}
	}
	
	/** @return A {@link Logger} with the <b>SimpleName</b> of class provided ( augmented with white spaces so its length being 25 characters ) */
	public static Logger getLoggerForClass(Class<?> cls) {
		Checks.checkNotNullArg(cls, "cls");
		return Logger.getLogger(Format.formatStringLeftByMaxLength(cls.getSimpleName(), 25));
	}

}
