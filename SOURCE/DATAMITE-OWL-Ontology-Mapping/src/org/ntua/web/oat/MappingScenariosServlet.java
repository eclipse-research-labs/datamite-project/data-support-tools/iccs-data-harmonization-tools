package org.ntua.web.oat;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.timchros.core.util.Throable;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@WebServlet("/MappingScenariosServlet")
public class MappingScenariosServlet extends HttpServlet {
	
	private static final Logger log = LogFactory.getLoggerForClass(MappingScenariosServlet.class);
	
	private static final long serialVersionUID = 1L;

	/**
     * @see HttpServlet#HttpServlet()
     */
	public MappingScenariosServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 * 
	 * Request parameters name: ontuid , uri , action
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// SessionID
			final String sid = OntoRep.produceSessionUID();
			
			// Request Parameters provided
			final String action = request.getParameter("method");
			final String uri = request.getParameter("uri");
			log.trace("SID: " + sid + " :: REQUEST --> ACTION: \"" + action + "\" URI: " + uri);
			
			// FIND DATA
			JSONObject json = new JSONObject();
			if (action.equalsIgnoreCase("GET-ALL-MAP-SCENARIOS")) {
				json = getAllMapScenarios();
			} else if ( action.equalsIgnoreCase("GET-MAP-SCENARIO-DETAILS") ) {
				if (uri == null || uri.trim().equals(""))
					throw new RuntimeException("\"uri\" not specified !");
				json = getMapScenarioDetails(uri.trim());
			} else {
				log.warn("NOT SUPPORTED ACTION [" + sid + "] :: Action: \"" + action + "\".");
			}
			
			// Response
			response.setContentType("application/json; charset=UTF-8");
			final PrintWriter pw = response.getWriter();
			pw.print(json.toString());
			pw.close();
			
			final String msg = (json.isEmpty()) ? "JSON is EMPTY !!! " : "JSON-LENGTH: " + json.toString().length();
			log.debug("SID: " + sid + " :: REQUEST --> ACTION: \"" + action + "\" URI: " + uri + " :: RESPONSE --> " + msg);
			log.trace("JSON \n" + json.toString(5, 5));
		} catch(Throwable tt) {
			final String msg = "Error when getting element... " + tt.getMessage();
			log.error(msg + "\n" + Throable.throwableAsString(tt));
	
			final JSONObject errorJsonObj = new JSONObject();
			String returnMsg = Throable.throwableAsSimpleOneLineString(tt);
			if (returnMsg != null && returnMsg.length() > 200) returnMsg = returnMsg.substring(0, 200) + " ...";
			errorJsonObj.put("error", returnMsg);
			response.setContentType("application/json; charset=UTF-8");
			final PrintWriter out = response.getWriter();
			out.print(errorJsonObj.toString());
			out.close();
		}
	}
	
	public synchronized JSONObject getAllMapScenarios() {
		return MappingScenariosRep.getInstance().getMapScenJson();
	}
	
	public synchronized JSONObject getMapScenarioDetails(final String uri) {
		final JSONArray categJA = MappingScenariosRep.getInstance().getMapScenJson().getJSONArray("categJA");
		for (int i = 0; i < categJA.size(); i++) {
			final JSONObject categJO = categJA.getJSONObject(i);
			final JSONArray mapscenJA = categJO.getJSONArray("mapscenJA");
			for (int j = 0; j < mapscenJA.size(); j++) {
				final JSONObject mapscenJO = mapscenJA.getJSONObject(j);
				final String mapScenUri = mapscenJO.getString("uri").trim();
				if (mapScenUri.equals(uri.trim())) {
					return mapscenJO;
				}
			}
		}
		throw new RuntimeException("Cannot find Mapping Scenarion with URI: " + uri);
	}
	
}
