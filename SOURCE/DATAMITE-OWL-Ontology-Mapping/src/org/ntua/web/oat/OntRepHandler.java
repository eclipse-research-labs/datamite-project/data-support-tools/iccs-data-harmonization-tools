package org.ntua.web.oat;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class OntRepHandler
 */
@WebServlet("/OntRepHandler")
public class OntRepHandler extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OntRepHandler() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final PrintWriter pw = response.getWriter();
		final String action = request.getParameter("action");
		if (action == null || action.trim().equals("")) {
			pw.print("None action provided !");
		} else {
			final String actionTrim = action.trim();
			if (actionTrim.equals("clean")) {
				OntoRep.getInstance().clean();
				pw.print("<h2>Map is now empty !</h2>");
			} if (actionTrim.equals("show")) {
				pw.print("<h2>Map size: " + OntoRep.getInstance().getOntoMap().size() + "</h2>");
			} else {
				pw.print("Not supported action: " + actionTrim + " !");
			}
		}
		
	}

}
