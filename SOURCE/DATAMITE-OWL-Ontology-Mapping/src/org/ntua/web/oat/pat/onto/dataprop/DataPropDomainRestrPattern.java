package org.ntua.web.oat.pat.onto.dataprop;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntResource;
import org.ntua.web.oat.pat.onto.cls.SimpleOntoClassPattern;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONObject;

public class DataPropDomainRestrPattern extends A_DataPropPattern {

	private final DatatypeProperty dataProp;
	private final OntClass domainClass;
	
	public DataPropDomainRestrPattern(DatatypeProperty dataProp, OntClass domainClass) {
		super();
		this.dataProp = dataProp;
		this.domainClass = domainClass;
	}

	public DatatypeProperty getDataProp() {
		return dataProp;
	}

	public OntClass getDomainClassPattern() {
		return domainClass;
	}

	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(dataProp);
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyDataPropAttr() {
		return Tuple3.tuple((OntResource) domainClass, getName(dataProp), dataProp.getRange());
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "PropertyDomainRestrictionPattern");
		json.put("property", new SimpleDataPropPattern(dataProp).toJson());
		json.put("domainclass", new SimpleOntoClassPattern(domainClass).toJson());
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"PropertyDomainRestrictionPattern".equals(pid)) return false;
		final JSONObject propertyJson = json.has("property") ? json.getJSONObject("property") : null;
		if (!new SimpleDataPropPattern(this.dataProp).equalsWithJson(propertyJson)) return false;
		final JSONObject domainclassJson = json.has("domainclass") ? json.getJSONObject("domainclass") : null;
		if (!new SimpleOntoClassPattern(this.domainClass).equalsWithJson(domainclassJson)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataProp == null) ? 0 : dataProp.getURI().hashCode());
		result = prime * result + ((domainClass == null) ? 0 : domainClass.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		DataPropDomainRestrPattern other = (DataPropDomainRestrPattern) obj;
		if (dataProp == null) { if (other.dataProp != null) return false;
		} else if (!dataProp.getURI().equals(other.dataProp.getURI())) return false;
		if (domainClass == null) { if (other.domainClass != null) return false;
		} else if (!domainClass.getURI().equals(other.domainClass.getURI())) return false;
		return true;
	}

	@Override
	public String toString() {
		return "DataProp { (" + getName(domainClass)  +") " + getName(dataProp) +" }-DDR";
	}
	
}
