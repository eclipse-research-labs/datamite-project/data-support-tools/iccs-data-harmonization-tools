package org.ntua.web.oat.pat.onto.cls;

import org.ntua.web.oat.pat.onto.dataprop.A_DataPropPattern;

public class ClassByAttributeValueRestrictionPattern extends A_OntoClassPattern {

	private final A_OntoClassPattern classPattern;
	private final A_DataPropPattern propertyPattern;
	private final String comparator;
	private final Object value;
	
	public ClassByAttributeValueRestrictionPattern(A_OntoClassPattern classPattern,A_DataPropPattern propertyPattern, String comparator, Object value) {
		super();
		this.classPattern = classPattern;
		this.propertyPattern = propertyPattern;
		this.comparator = comparator;
		this.value = value;
	}

	public A_OntoClassPattern getClassPattern() {
		return classPattern;
	}

	public A_DataPropPattern getPropertyPattern() {
		return propertyPattern;
	}

	public String getComparator() {
		return comparator;
	}

	public Object getValue() {
		return value;
	}

	@Override
	public String toString() {
		return 
			ClassByAttributeValueRestrictionPattern.class.getSimpleName() + " [" +
				"classPattern = \"" + classPattern + "\" , " +
				"propertyPattern = \"" + propertyPattern + "\" , " +
				"comparator = \"" + ( ( comparator != null ) ? comparator.getClass().getName() : "null" ) + "\" , " +
				"value = \"" + value + "\" " +
			"]";
	}
	
}
