package org.ntua.web.oat.pat;

/**
 * The class contains the EDOAL/XML representation of ontological patterns used in an abstract way
 * 
 * @author Efthymios Chondrogiannis
 */
public final class AbstractElementPatternRep {

	private static final String NL = "\n";
	private static final String TAB = "\t";

	/** Ensures we will not create an instance of this class */
	private AbstractElementPatternRep() {
		
	}
	
	public static final String XmlIntroductionPattern =
			"<!DOCTYPE rdf:RDF [" + NL +
			"<!ENTITY xsd \"http://www.w3.org/2001/XMLSchema#\">" + NL +
			"<!ENTITY ontoA \"${OntoAuri}\">" + NL +
			"<!ENTITY ontoB \"${OntoBuri}\">" + NL +
			"]>";
	
	public static final String RdfIntroductionPattern = 
			"<rdf:RDF " + NL +
			"  xmlns=\"http://knowledgeweb.semanticweb.org/heterogeneity/alignment#\"" + NL +
			"  xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" + NL +
			"  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"" + NL +
			"  xmlns:align=\"http://knowledgeweb.semanticweb.org/heterogeneity/alignment#\"" + NL +
			"  xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"" + NL +
			"  xmlns:dc=\"http://purl.org/dc/elements/1.1/\"" + NL +
			"  xmlns:edoal=\"http://ns.inria.org/edoal/1.0/#\"" + NL +
			"  xmlns:ontoA=\"${OntoAuri}\"" + NL +
			"  xmlns:ontoB=\"${OntoBuri}\"" + NL +
			">";

	
	public static final String AlignmentIntroductionPattern = 
			TAB + "<xml>yes</xml>" + NL +
			TAB + "<dc:creator>Ontologies Alignment Tool</dc:creator>" + NL +
			TAB + "<dc:date>${creationdate}</dc:date>" + NL +
			TAB + "<method>semi-automatic</method>" + NL +
			TAB + "<purpose>Alignment Specification</purpose>" + NL +
			TAB + "<level>2EDOAL</level>" + NL +
			TAB + "<type>**</type>" + NL + 
			TAB + "<onto1>" + NL +
			TAB + TAB + "<Ontology rdf:about=\"&ontoA;\">" + NL +
			TAB + TAB + TAB + "<formalism>" + NL +
			TAB + TAB + TAB + TAB + "<Formalism>" + NL +
			TAB + TAB + TAB + TAB + TAB + "<uri>${OntoAuri}</uri>" + NL +
			TAB + TAB + TAB + TAB + TAB + "<name>owl</name>" + NL +
			TAB + TAB + TAB + TAB + "</Formalism>" + NL +
			TAB + TAB + TAB + "</formalism>" + NL +
			TAB + TAB + "</Ontology>" + NL +
			TAB + "</onto1>" + NL +
			TAB + "<onto2>" + NL +
			TAB + TAB + "<Ontology rdf:about=\"&ontoB;\">" + NL +
			TAB + TAB + TAB + "<formalism>" + NL +
			TAB + TAB + TAB + TAB + "<Formalism>" + NL +
			TAB + TAB + TAB + TAB + TAB + "<uri>${OntoBuri}</uri>" + NL +
			TAB + TAB + TAB + TAB + TAB + "<name>owl</name>" + NL +
			TAB + TAB + TAB + TAB + "</Formalism>" + NL +
			TAB + TAB + TAB + "</formalism>" + NL +
			TAB + TAB + "</Ontology>" + NL +
			TAB + "</onto2>";

	// Class Patterns
	
	public static final String SimpleClassPattern = 
			"<edoal:Class rdf:about=\"${classuri}\"/> <!-- ${classname} -->";
	
	public static final String UnionClassPattern =
			"<edoal:Class>" + NL +
			TAB + "<edoal:or rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${classlist}" + NL +
			TAB + "</edoal:or>" + NL +
			"</edoal:Class>";

	public static final String IntersectionClassPattern =
			"<edoal:Class>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${classlist}" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Class>";

	public static final String ClassByAttributeValueRestrictionPattern = 
			"<edoal:Class>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${class}" + NL +
			TAB + TAB + "<edoal:AttributeValueRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:onAttribute>" + NL +
			TAB + TAB + TAB + TAB + "${property}" + NL +
			TAB + TAB + TAB + "</edoal:onAttribute>" + NL +
			TAB + TAB + TAB + "<!-- ${description} -->" + NL +
			TAB + TAB + TAB + "<edoal:comparator rdf:resource=\"${comparatoruri}\" />" + NL +
			TAB + TAB + TAB + "<edoal:value>" + NL +
			TAB + TAB + TAB + TAB + "<edoal:Literal edoal:type=\"&xsd;string\" edoal:string=\"${value}\" />" + NL +
			TAB + TAB + TAB + "</edoal:value>" + NL +
			TAB + TAB + "</edoal:AttributeValueRestriction>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Class>";
	
	public static final String ClassByAttributeOccurenceNumberPattern =
			"<edoal:Class>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${class}" + NL +
			TAB + TAB + "<edoal:AttributeOccurenceRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:onAttribute>" + NL +
			TAB + TAB + TAB + TAB + "${property}" + NL +
			TAB + TAB + TAB + "</edoal:onAttribute>" + NL +
			TAB + TAB + TAB + "<!-- ${description} -->" + NL +
			TAB + TAB + TAB + "<edoal:comparator rdf:resource=\"${comparatoruri}\"/>" + NL +
			TAB + TAB + TAB + "<edoal:value>${value}</edoal:value>" + NL +
			TAB + TAB + "</edoal:AttributeOccurenceRestriction>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Class>";

	public static final String ClassWithPropertiesRestrictionPattern =
			"<edoal:Class>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${class}" + NL +
			TAB + TAB + "<edoal:AttributeOccurenceRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:onAttribute>" + NL +
			TAB + TAB + TAB + TAB + "<edoal:Property>" + NL +
			TAB + TAB + TAB + TAB + TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + TAB + TAB + TAB + TAB + "${proplist}" + NL +
			TAB + TAB + TAB + TAB + TAB + "</edoal:and>" + NL +
			TAB + TAB + TAB + TAB + "</edoal:Property>" +
			TAB + TAB + TAB + "</edoal:onAttribute>" + NL +
			TAB + TAB + TAB + "<!-- ${description} -->" + NL +
			TAB + TAB + "</edoal:AttributeOccurenceRestriction>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Class>";
	
	// Relation Patterns
	
	public static final String SimpleRelationPattern =
			"<edoal:Relation rdf:about=\"${relationuri}\"/> <!-- ${relationname} -->";

	public static final String InverseRelationPattern =
			"<edoal:Relation>" + NL +
			TAB + "<edoal:inverse>" + NL +
			TAB + TAB + "${relation}" + NL +
			TAB + "</edoal:inverse>" + NL +
			"</edoal:Relation>";

	public static final String RelationDomainRestrictionPattern =
			"<edoal:Relation>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${relation}" + NL +
			TAB + TAB + "<edoal:RelationDomainRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:class>" + NL +
			TAB + TAB + TAB + TAB + "${class}" + NL +
			TAB + TAB + TAB + "</edoal:class>" + NL +
			TAB + TAB + "</edoal:RelationDomainRestriction>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Relation>";
	
	public static final String RelationRangeRestrictionPattern =
			"<edoal:Relation>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${relation}" + NL +
			TAB + TAB + "<edoal:RelationCoDomainRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:class>" + NL +
			TAB + TAB + TAB + TAB + "${class}" + NL +
			TAB + TAB + TAB + "</edoal:class>" + NL +
			TAB + TAB + "</edoal:RelationCoDomainRestriction>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Relation>";
	
	public static final String RelationPathPattern =
			"<edoal:Relation>" + NL +
			TAB + "<edoal:compose rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${rellist}" + NL +
			TAB + "</edoal:compose>" + NL +
			"</edoal:Relation>";
	
	public static final String RelationDomainRangeRestrictionPattern = 
			"<edoal:Relation>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${relation}" + NL +
			TAB + TAB + "<edoal:RelationDomainRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:class>" + NL +
			TAB + TAB + TAB + TAB + "${domain-class}" + NL +
			TAB + TAB + TAB + "</edoal:class>" + NL +
			TAB + TAB + "</edoal:RelationDomainRestriction>" + NL +
			TAB + TAB + "<edoal:RelationCoDomainRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:class>" + NL +
			TAB + TAB + TAB + TAB + "${range-class}" + NL +
			TAB + TAB + TAB + "</edoal:class>" + NL +
			TAB + TAB + "</edoal:RelationCoDomainRestriction>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Relation>";			
	
	// Property Patterns
	
	public static final String SimplePropertyPattern =
			"<edoal:Property rdf:about=\"${propertyuri}\"/> <!-- ${propertyname} -->";
	
	public static final String PropertyDomainRestrictionPattern =
			"<edoal:Property>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${property}" + NL +
			TAB + TAB + "<edoal:PropertyDomainRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:class>" + NL +
			TAB + TAB + TAB + TAB + "${class}" + NL +
			TAB + TAB + TAB + "</edoal:class>" + NL +
			TAB + TAB + "</edoal:PropertyDomainRestriction>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Property>";		
	
	public static final String PropertyDatatypeRestrictionPattern =
			"<edoal:Property>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${property}" + NL +
			TAB + TAB + "<edoal:PropertyTypeRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:datatype>" + NL +
			TAB + TAB + TAB + TAB + "<Datatype rdf:about=\"${datatype}\"/>" + NL +
			TAB + TAB + TAB + "</edoal:datatype>" + NL +
			TAB + TAB + "</edoal:PropertyTypeRestriction>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Property>";			
	
	public static final String PropertyValueRestrictionPattern =
			"<edoal:Property>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${property}" + NL +
			TAB + TAB + "<!-- ${description} -->" + NL +
			TAB + TAB + "<edoal:comparator rdf:resource=\"${comparatoruri}\" />" + NL +
			TAB + TAB + "<edoal:value>" + NL +
			TAB + TAB + TAB + "<edoal:Literal edoal:type=\"&xsd;string\" edoal:string=\"${value}\" />" + NL +
			TAB + TAB + "</edoal:value>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Property>";			
	
	public static final String RelationPropertyPattern =
			"<edoal:Property>" + NL +
			TAB + "<edoal:compose rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${relation}" + NL +
			TAB + TAB + "${property}" + NL +
			TAB + "</edoal:compose>" + NL +
			"</edoal:Property>";	
	
	public static final String PropertyDomainClassDatatypeRestrictionPattern =
			"<edoal:Property>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${property}" + NL +
			TAB + TAB + "<edoal:PropertyDomainRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:class>" + NL +
			TAB + TAB + TAB + TAB + "${class}" + NL +
			TAB + TAB + TAB + "</edoal:class>" + NL +
			TAB + TAB + "</edoal:PropertyDomainRestriction>" + NL +
			TAB + TAB + "<edoal:PropertyTypeRestriction>" + NL +
			TAB + TAB + TAB + "<edoal:datatype>" + NL +
			TAB + TAB + TAB + TAB + "<Datatype rdf:about=\"${datatype}\"/>" + NL +
			TAB + TAB + TAB + "</edoal:datatype>" + NL +
			TAB + TAB + "</edoal:PropertyTypeRestriction>" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Property>";	
	
	// Instance Patterns
	
	public static final String SimpleInstancePattern =
			"<edoal:Instance rdf:about=\"${instanceuri}\"/> <!-- ${instancename} -->";
	
	// Other Patterns
	
	public static final String PropertiesCollectionPattern =
			"<edoal:Property>" + NL +
			TAB + "<edoal:and rdf:parseType=\"Collection\">" + NL +
			TAB + TAB + "${proplist}" + NL +
			TAB + "</edoal:and>" + NL +
			"</edoal:Property>";
	

	// Transformation Patterns
	
	public static final String TransformationPattern =
		"<!-- ${description} -->" + NL +
		"<edoal:Apply edoal:operator=\"${serviceuri}\">" + NL +
		TAB + "<edoal:arguments rdf:parseType=\"Collection\">" + NL +
		TAB + TAB + "${arglist}" + NL +
		TAB + "</edoal:arguments>" + NL +
		"</edoal:Apply>";
	
	public static final String PairArgumentValuePattern =
		"<edoal:Literal edoal:type=\"&xsd;string\" edoal:string=\"-${name}\" />" + NL +
		"<edoal:Literal edoal:type=\"&xsd;string\" edoal:string=\"${value}\" />";
	

	/** For testing purposes */
	public static void main(String[] args) throws Exception {
		ElementPattern ep = new ElementPattern( new String(PropertyDomainRestrictionPattern) );
		ep.addPrefix("\t");
		ep.setVarValue("property", "TheProperty!");
		ep.setVarValue("class", "TheClass!");
		System.out.println(
			ep
		);
	}
	
}
