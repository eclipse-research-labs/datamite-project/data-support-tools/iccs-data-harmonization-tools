package org.ntua.web.oat.pat.onto.dataprop;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.OntResource;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONObject;

public class SimpleDataPropPattern extends A_DataPropPattern {

	private final DatatypeProperty dataProp;

	public SimpleDataPropPattern(DatatypeProperty dataProp) {
		super();
		this.dataProp = dataProp;
	}

	public DatatypeProperty getDataProp() {
		return dataProp;
	}
	
	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(dataProp);
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyDataPropAttr() {
		return Tuple3.tuple(dataProp.getDomain(), getName(dataProp), dataProp.getRange());
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "SimplePropertyPattern");
		json.put("propertyuri", this.dataProp.getURI());
		json.put("propertyname", getName(this.dataProp));
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"SimplePropertyPattern".equals(pid)) return false;
		final String propertyuri = json.has("propertyuri") ? json.getString("propertyuri") : null;
		if (!this.dataProp.getURI().equals(propertyuri)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataProp == null) ? 0 : dataProp.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		SimpleDataPropPattern other = (SimpleDataPropPattern) obj;
		if (dataProp == null) { if (other.dataProp != null) return false;
		} else if (!dataProp.getURI().equals(other.dataProp.getURI())) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "DP[" + getName(dataProp) + "]";
	}
	
}
