package org.ntua.web.oat.pat.onto.cls;

import org.ntua.web.oat.pat.onto.dataprop.A_DataPropPattern;

public class ClassByAttributeOccurrencePattern extends A_OntoClassPattern {

	private final A_OntoClassPattern classPattern;
	private final A_DataPropPattern propertyPattern;
	
	public ClassByAttributeOccurrencePattern(A_OntoClassPattern classPattern, A_DataPropPattern propertyPattern) {
		super();
		this.classPattern = classPattern;
		this.propertyPattern = propertyPattern;
	}

	public A_OntoClassPattern getClassPattern() {
		return classPattern;
	}

	public A_DataPropPattern getPropertyPattern() {
		return propertyPattern;
	}

	@Override
	public String toString() {
		return 
			"ClassByAttributeOccurencePattern [" +
				"classPattern = \"" + classPattern + "\" , " +
				"propertyPattern = \"" + propertyPattern + "\" " +
			"]";
	}

}
