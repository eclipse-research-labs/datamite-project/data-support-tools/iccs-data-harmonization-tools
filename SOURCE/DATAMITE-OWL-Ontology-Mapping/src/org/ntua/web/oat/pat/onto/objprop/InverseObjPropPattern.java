package org.ntua.web.oat.pat.onto.objprop;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntResource;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONObject;

public final class InverseObjPropPattern extends A_ObjPropPattern {

	private final ObjectProperty objProp;

	public InverseObjPropPattern(ObjectProperty relationPattern) {
		super();
		this.objProp = relationPattern;
	}

	public ObjectProperty getObjProp() {
		return objProp;
	}

	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(objProp);
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyObjPropAttr() {
		return Tuple3.tuple(objProp.getRange(), getName(objProp), objProp.getDomain());
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "InverseRelationPattern");
		json.put("relation", new SimpleObjPropPattern(objProp).toJson());
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"InverseRelationPattern".equals(pid)) return false;
		final JSONObject relationJson = json.has("relation") ? json.getJSONObject("relation") : null;
		if (!new SimpleObjPropPattern(this.objProp).equals(relationJson)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objProp == null) ? 0 : objProp.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		InverseObjPropPattern other = (InverseObjPropPattern) obj;
		if (objProp == null) { if (other.objProp != null) 
			return false;
		} else if (!objProp.getURI().equals(other.objProp.getURI()))
			return false;
		return true;
	}	
	
	@Override
	public String toString() {
		return "ObjProp[ " + getName(objProp) + "]-InverseR";
	}

}
