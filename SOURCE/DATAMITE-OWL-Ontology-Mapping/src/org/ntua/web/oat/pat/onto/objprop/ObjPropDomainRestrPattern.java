package org.ntua.web.oat.pat.onto.objprop;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntResource;
import org.ntua.web.oat.pat.onto.cls.SimpleOntoClassPattern;
import org.timchros.core.tuple.Tuple3;

/**
 * @author Thymios Chondrogiannis
 */
public final class ObjPropDomainRestrPattern extends A_ObjPropPattern {

	private final ObjectProperty objProp;
	private final OntClass domainClass;
	
	public ObjPropDomainRestrPattern(ObjectProperty objProp, OntClass domainClass) {
		super();
		this.objProp = objProp;
		this.domainClass = domainClass;
	}

	public ObjectProperty getObjProp() {
		return objProp;
	}

	public OntClass getDomainClass() {
		return domainClass;
	}

	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(objProp);
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyObjPropAttr() {
		return Tuple3.tuple((OntResource) domainClass, getName(objProp), objProp.getRange());
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "RelationDomainRestrictionPattern");
		json.put("relation", new SimpleObjPropPattern(objProp).toJson());
		json.put("domainclass", new SimpleOntoClassPattern(domainClass).toJson());
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"RelationDomainRestrictionPattern".equals(pid)) return false;
		final JSONObject relationJson = json.has("relation") ? json.getJSONObject("relation") : null;
		if (!new SimpleObjPropPattern(this.objProp).equalsWithJson(relationJson)) return false;
		final JSONObject domainclassJson = json.has("domainclass") ? json.getJSONObject("domainclass") : null;
		if (!new SimpleOntoClassPattern(this.domainClass).equalsWithJson(domainclassJson)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((domainClass.getURI() == null) ? 0 : domainClass.getURI().hashCode());
		result = prime * result + ((objProp.getURI() == null) ? 0 : objProp.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ObjPropDomainRestrPattern other = (ObjPropDomainRestrPattern) obj;
		if (domainClass == null) { if (other.domainClass != null) return false;
		} else if (!domainClass.getURI().equals(other.domainClass.getURI())) return false;
		if (objProp == null) { if (other.objProp != null) return false;
		} else if (!objProp.getURI().equals(other.objProp.getURI())) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ObjProp { (" + getName(domainClass)  +") " + getName(objProp) +" }-RDR";
	}
	
}
