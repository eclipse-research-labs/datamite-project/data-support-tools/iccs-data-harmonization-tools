package org.ntua.web.oat.pat.onto.cls;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntResource;

import net.sf.json.JSONObject;

public final class SimpleOntoClassPattern extends A_OntoClassPattern {

	private final OntClass ontoCls;

	public SimpleOntoClassPattern(OntClass ontoCls) {
		super();
		this.ontoCls = ontoCls;
	}

	public OntClass getOntoClass() {
		return ontoCls;
	}
	
	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(ontoCls);
		return list;
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "SimpleClassPattern");
		json.put("classuri", this.ontoCls.getURI());
		json.put("classname", getName(this.ontoCls));
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"SimpleClassPattern".equals(pid)) return false;
		final String classuri = json.has("classuri") ? json.getString("classuri") : null;
		if (!this.ontoCls.getURI().equals(classuri)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ontoCls == null) ? 0 : ontoCls.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		SimpleOntoClassPattern other = (SimpleOntoClassPattern) obj;
		if (ontoCls == null) { if (other.ontoCls != null) 
			return false;
		} else if (!ontoCls.getURI().equals(other.ontoCls.getURI()))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return 
			"C[" + getName(ontoCls) + "]";
	}
	
}
