package org.ntua.web.oat.pat.onto.dataprop;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntResource;
import org.ntua.web.oat.ServletUtil;
import org.ntua.web.oat.pat.onto.objprop.SimpleObjPropPattern;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Util;

public class ObjPropDataPropPathPattern extends A_DataPropPattern {

	private final ObjectProperty objProp;
	private final DatatypeProperty dataProp;
	
	public ObjPropDataPropPathPattern(ObjectProperty objProp, DatatypeProperty dataProp) {
		this.objProp= objProp;
		this.dataProp = dataProp;
	}

	public ObjectProperty getObjProp() {
		return objProp;
	}

	public DatatypeProperty getDataProp() {
		return dataProp;
	}
	
	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(objProp);
		list.add(dataProp);
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyDataPropAttr() {
		final List<String> nameTokenList = Util.newArrayList(getName(objProp).toLowerCase().split("\\s+"));
		final String[] dataPropNameTokens = getName(dataProp).toLowerCase().split("\\s+");
		for (String token : dataPropNameTokens) {
			if (!nameTokenList.contains(token)) {
				nameTokenList.add(token);
			}
		}
		final String combPropName = ServletUtil.makeString(nameTokenList);
		return Tuple3.tuple(objProp.getDomain(), combPropName, dataProp.getRange());
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "RelationPropertyPathPattern");
		json.put("relation", new SimpleObjPropPattern(objProp).toJson());
		json.put("property", new SimpleDataPropPattern(dataProp).toJson());
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"RelationPropertyPathPattern".equals(pid)) return false;
		final JSONObject relationJson = json.has("relation") ? json.getJSONObject("relation") : null;
		if (!new SimpleObjPropPattern(this.objProp).equalsWithJson(relationJson)) return false;
		final JSONObject propertyJson = json.has("property") ? json.getJSONObject("property") : null;
		if (!new SimpleDataPropPattern(this.dataProp).equalsWithJson(propertyJson)) return false;
		return true;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objProp == null) ? 0 : objProp.getURI().hashCode());
		result = prime * result + ((dataProp == null) ? 0 : dataProp.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ObjPropDataPropPathPattern other = (ObjPropDataPropPathPattern) obj;
		if (objProp == null) { if (other.objProp != null) return false;
		} else if (!objProp.getURI().equals(other.objProp.getURI())) return false;
		if (dataProp == null) { if (other.dataProp != null) return false;
		} else if (!dataProp.getURI().equals(other.dataProp.getURI())) return false;
		return true;
	}	
	
	@Override
	public String toString() {
		return "DataProp{ " + getName(objProp) + " --> " + getName(dataProp) +" }-PATH";
	}

}
