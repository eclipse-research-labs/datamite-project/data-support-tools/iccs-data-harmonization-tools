package org.ntua.web.oat.pat.onto.objprop;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntResource;
import org.ntua.web.oat.ServletUtil;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Util;

public final class ObjPropPathPattern extends A_ObjPropPattern {

	private final List<ObjectProperty> objPropList = new ArrayList<ObjectProperty>();
	
	public ObjPropPathPattern(final List<ObjectProperty> objPropList) {
		this.objPropList.addAll(objPropList);
	}

	public List<ObjectProperty> getObjPropList() {
		return objPropList;
	}

	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		for (ObjectProperty objProp : objPropList) {
			list.add(objProp);
		}
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyObjPropAttr() {
		final List<String> nameTokenList = new ArrayList<String>();
		for (ObjectProperty objProp : objPropList) {
			final String[] objPropNameTokens = getName(objProp).toLowerCase().split("\\s+");
			for (String token : objPropNameTokens) {
				if (!nameTokenList.contains(token)) {
					nameTokenList.add(token);
				}
			}
		}
		final String combPropName = ServletUtil.makeString(nameTokenList);
		return Tuple3.tuple(objPropList.get(0).getDomain(), combPropName, objPropList.get(objPropList.size()-1).getRange());
	}
	
	@Override
	public JSONObject toJson() {
		final JSONArray jsonArray = new JSONArray();
		for (ObjectProperty objProp : objPropList) {
			jsonArray.add(new SimpleObjPropPattern(objProp).toJson());
		}
		final JSONObject json = new JSONObject();
		json.put("pid", "RelationPathPattern");
		json.put("relationarray", jsonArray);
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"RelationPathPattern".equals(pid)) return false;
		final JSONArray relationJsonArray = json.has("relationarray") ? json.getJSONArray("relationarray") : null;
		if (relationJsonArray == null || relationJsonArray.size() != this.objPropList.size()) return false;
		for (int i = 0; i < relationJsonArray.size(); i++) {
			final JSONObject relationJsonObj = relationJsonArray.getJSONObject(i);
			final SimpleObjPropPattern objPat = new SimpleObjPropPattern(objPropList.get(i));
			if (!objPat.equalsWithJson(relationJsonObj)) return false;
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objPropList == null) ? 0 : objPropList.hashCode());
		for (ObjectProperty objProp : objPropList) {
			result = prime * result + ((objProp.getURI() == null) ? 0 : objProp.getURI().hashCode());
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ObjPropPathPattern other = (ObjPropPathPattern) obj;
		if (objPropList == null) {
			if (other.objPropList != null) return false;
		} else {
			for (ObjectProperty objProp : objPropList) {
				if (!other.getObjPropList().contains(objProp)) return false;
			}
			for (ObjectProperty otherObjProp : other.getObjPropList()) {
				if (!objPropList.contains(otherObjProp)) return false;
			}
		}
		return true;
	}

	@Override
	public String toString() {
		if (objPropList.size() > 1) {
			final StringBuilder sb = new StringBuilder();
			sb.append(objPropList.get(0).toString());
			for (int i = 1; i < objPropList.size(); i++) {
				sb.append(" --> ");
				sb.append(objPropList.get(i).toString());
			}
			return "ObjProp { " + sb + " }-RDR";
		} else {
			return "ObjProp{ " + Util.listToOneLineString(objPropList) + " }-PATH";
		}
	}

}
