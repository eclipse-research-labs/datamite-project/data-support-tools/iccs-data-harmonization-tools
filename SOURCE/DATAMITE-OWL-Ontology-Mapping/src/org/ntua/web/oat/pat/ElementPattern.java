package org.ntua.web.oat.pat;

/**
 * This class contains useful methods for abstract EDOAL/XML patterns so that we provide the specific elements used
 * 
 * @author Efthymios Chondrogiannis
 */
public final class ElementPattern {

	private String patternStr;

	public ElementPattern(String patternStr) {
		this.patternStr = patternStr;
	}
	
	public void addPrefix(final String prefix) {
		StringBuilder sb = new StringBuilder();
		String[] lines = patternStr.split("\\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			sb.append(prefix + line);
			if (i < lines.length - 1) {
				sb.append("\n");
			}
		}
		this.patternStr = sb.toString();
	}
	
	public String getVarPrefix(final String variable) {
		final String tag = "${" + variable + "}";
		String[] lines = patternStr.split("\\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			int index = line.indexOf(tag);
			if (index >= 0) {
				return line.substring(0, index);
			}
		}
		return "";
	}
	
	public void setVarValue(final String variable, final String value) {
//		final String tag = "${" + variable + "}";
//		int index = this.patternStr.indexOf(tag);
//		if (index < 0) throw new RuntimeException("Cannot find variable \"" + variable + "\" in the following pattern: \n" + this.patternStr);
//		this.patternStr = this.patternStr.substring(0, index) + value + this.patternStr.substring(index + tag.length(), this.patternStr.length());
		final StringBuilder sb = new StringBuilder();
		final String tag = "${" + variable + "}";
		String[] lines = patternStr.split("\\n");
		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			int index = line.indexOf(tag);
			if (index >= 0) {
				final String prefix = line.substring(0, index);
				if (prefix.matches("[\\s\\t]+")) {
					sb.append(value);
				} else {
					sb.append( line.substring(0, index) + value + line.substring(index + tag.length(), line.length()) );
				}
			} else {
				sb.append(line);
			}
			if (i < lines.length - 1) {
				sb.append("\n");
			}
		}
		this.patternStr = sb.toString();
	}

	@Override
	public String toString() {
		return this.patternStr;
	}
	
	
	
}
