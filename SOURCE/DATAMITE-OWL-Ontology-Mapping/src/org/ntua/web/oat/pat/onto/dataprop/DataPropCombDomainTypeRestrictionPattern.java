package org.ntua.web.oat.pat.onto.dataprop;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntResource;
import org.ntua.web.oat.pat.onto.cls.SimpleOntoClassPattern;
import org.timchros.core.tuple.Tuple3;

public class DataPropCombDomainTypeRestrictionPattern  extends A_DataPropPattern {

	private final DatatypeProperty dataProp;
	private final OntClass domainClass;
	private final OntResource typeRes;
	
	public DataPropCombDomainTypeRestrictionPattern(DatatypeProperty dataProp, OntClass domainClass, OntResource typeRes) {
		super();
		this.dataProp = dataProp;
		this.domainClass = domainClass;
		this.typeRes = typeRes;
	}

	public DatatypeProperty getDataProp() {
		return dataProp;
	}
	
	public OntClass getDomainClassPattern() {
		return domainClass;
	}

	public OntResource getTypeRes() {
		return typeRes;
	}

	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(dataProp);
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyDataPropAttr() {
		return Tuple3.tuple( (OntResource) domainClass, getName(dataProp), typeRes);
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "PropertyDomainRangeRestrictionPattern");
		json.put("property", new SimpleDataPropPattern(dataProp).toJson());
		json.put("domainclass", new SimpleOntoClassPattern(domainClass).toJson());
		json.put("rangedatatype", typeRes.getURI()); // TODO: or getName(typeRes)
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"PropertyDomainRangeRestrictionPattern".equals(pid)) return false;
		final JSONObject propertyJson = json.has("property") ? json.getJSONObject("property") : null;
		if (!new SimpleDataPropPattern(this.dataProp).equalsWithJson(propertyJson)) return false;
		final JSONObject domainclassJson = json.has("domainclass") ? json.getJSONObject("domainclass") : null;
		if (!new SimpleOntoClassPattern(this.domainClass).equalsWithJson(domainclassJson)) return false;
		final String rangedatatype = json.has("rangedatatype") ? json.getString("rangedatatype") : null;
		if (!this.typeRes.getURI().equals(rangedatatype)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataProp == null) ? 0 : dataProp.hashCode());
		result = prime * result + ((domainClass == null) ? 0 : domainClass.hashCode());
		result = prime * result + ((typeRes == null) ? 0 : typeRes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		DataPropCombDomainTypeRestrictionPattern other = (DataPropCombDomainTypeRestrictionPattern) obj;
		if (dataProp == null) { if (other.dataProp != null) return false;
		} else if (!dataProp.getURI().equals(other.dataProp.getURI())) return false;
		if (domainClass == null) { if (other.domainClass != null) return false;
		} else if (!domainClass.getURI().equals(other.domainClass.getURI())) return false;
		if (typeRes == null) { if (other.typeRes != null) return false;
		} else if (!typeRes.getURI().equals(other.typeRes.getURI())) return false;
		return true;
	}

	@Override
	public String toString() {
		return "DataProp { (" + getName(domainClass) + ") " + getName(dataProp) +" (" + getName(typeRes) + ") }-D*R";
	}

}
