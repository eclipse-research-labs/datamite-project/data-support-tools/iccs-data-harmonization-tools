package org.ntua.web.oat.pat.onto.indiv;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntResource;

import net.sf.json.JSONObject;

public class SimpleIndivPattern extends A_IndivPattern {

	private final Individual individual;

	public SimpleIndivPattern(Individual individual) {
		super();
		this.individual = individual;
	}

	public Individual getIndividual() {
		return individual;
	}
	
	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(individual);
		return list;
	}

	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "SimpleInstancePattern");
		json.put("instanceuri", this.individual.getURI());
		json.put("instancename", getName(this.individual));
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"SimpleInstancePattern".equals(pid)) return false;
		final String instanceuri = json.has("instanceuri") ? json.getString("instanceuri") : null;
		if (!this.individual.getURI().equals(instanceuri)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((individual == null) ? 0 : individual.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		SimpleIndivPattern other = (SimpleIndivPattern) obj;
		if (individual == null) { if (other.individual != null) 
			return false;
		} else if (!individual.getURI().equals(other.individual.getURI()))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "I[" + getName(individual) + "]";
	}
	
}
