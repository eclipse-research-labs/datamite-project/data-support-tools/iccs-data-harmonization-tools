package org.ntua.web.oat.pat.onto.cls;

import java.util.ArrayList;
import java.util.List;

public class ClassUnionPattern extends A_OntoClassPattern {

	private final List<A_OntoClassPattern> classPatternList = new ArrayList<A_OntoClassPattern>();

	public ClassUnionPattern(List<A_OntoClassPattern> classPatternList) {
		this.classPatternList.addAll(classPatternList);
	}

	public List<A_OntoClassPattern> getClassPatternList() {
		return classPatternList;
	}

	@Override
	public String toString() {
		return 
			"ClassUnionPattern [" +
				"classPatternList = \"" + classPatternList + "\" " +
			"]";
	}
	
}
