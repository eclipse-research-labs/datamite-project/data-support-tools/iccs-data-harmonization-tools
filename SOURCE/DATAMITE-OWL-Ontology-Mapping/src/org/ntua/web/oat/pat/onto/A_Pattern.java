package org.ntua.web.oat.pat.onto;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.OntResource;

import net.sf.json.JSONObject;

/**
 * This class is being used to indicate that the class extended it is an Abstract Pattern
 * 
 * @author Thymios Chondrogiannis
 */
public abstract class A_Pattern {
	
	/** @return The appropriate JSON object with ontological pattern data */
	public JSONObject toJson() {
		return JSONObject.fromObject("{ note: none }");
	}
	
	// TO DO : Init with JSON
	
	/** @return <code>true</code> if this object is semantically the same with the JSON object given */
	public boolean equalsWithJson(JSONObject json) {
		return false;
	}
	
	/** @return A List with critical elements of ontological pattern */
	public List<OntResource> getCriticalElements() {
		return new ArrayList<OntResource>();
	}
	
	/** @return Label or Local Name of the {@link OntResource} given */
	protected String getName(final OntResource ontoRes) {
		if (ontoRes == null) return null;
		final String ontResLab = ontoRes.getLabel(null);
		if (ontResLab != null) return ontResLab;
		final String ontResName = ontoRes.getLocalName();
		return ontResName;
	}
	
}
