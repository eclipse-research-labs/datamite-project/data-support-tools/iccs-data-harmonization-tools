package org.ntua.web.oat.pat.onto.objprop;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntResource;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONObject;

public final class SimpleObjPropPattern extends A_ObjPropPattern {

	private final ObjectProperty objProp;

	public SimpleObjPropPattern(ObjectProperty objProp) {
		super();
		this.objProp = objProp;
	}

	public ObjectProperty getObjProp() {
		return objProp;
	}
	
	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(objProp);
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyObjPropAttr() {
		return Tuple3.tuple(objProp.getDomain(), getName(objProp), objProp.getRange());
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "SimpleRelationPattern");
		json.put("relationuri", this.objProp.getURI());
		json.put("relationname", getName(this.objProp));
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"SimpleRelationPattern".equals(pid)) return false;
		final String relationuri = json.has("relationuri") ? json.getString("relationuri") : null;
		if (!this.objProp.getURI().equals(relationuri)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((objProp == null) ? 0 : objProp.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		SimpleObjPropPattern other = (SimpleObjPropPattern) obj;
		if (objProp == null) { if (other.objProp != null) 
			return false;
		} else if (!objProp.getURI().equals(other.objProp.getURI()))
			return false;
		return true;
	}	
	
	@Override
	public String toString() {
		return "OP[" + getName(objProp) + "]";
	}
	
}
