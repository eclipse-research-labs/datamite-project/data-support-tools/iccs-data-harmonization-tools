package org.ntua.web.oat.pat.onto.dataprop;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.OntResource;
import org.timchros.core.tuple.Tuple3;

import net.sf.json.JSONObject;

public class DataPropTypeRestrictionPattern extends A_DataPropPattern {

	private final DatatypeProperty dataProp;
	private final OntResource typeRes;
	
	public DataPropTypeRestrictionPattern(DatatypeProperty dataProp, OntResource typeRes) {
		super();
		this.dataProp = dataProp;
		this.typeRes = typeRes;
	}

	public DatatypeProperty getDataProp() {
		return dataProp;
	}

	public OntResource getTypeRes() {
		return typeRes;
	}

	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(dataProp);
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyDataPropAttr() {
		return Tuple3.tuple(dataProp.getDomain(), getName(dataProp), typeRes);
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "PropertyRangeRestrictionPattern");
		json.put("property", new SimpleDataPropPattern(dataProp).toJson());
		json.put("rangedatatype", typeRes.getURI()); // TODO: or getName(typeRes)
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"PropertyRangeRestrictionPattern".equals(pid)) return false;
		final JSONObject propertyJson = json.has("property") ? json.getJSONObject("property") : null;
		if (!new SimpleDataPropPattern(this.dataProp).equalsWithJson(propertyJson)) return false;
		final String rangedatatype = json.has("rangedatatype") ? json.getString("rangedatatype") : null;
		if (!this.typeRes.getURI().equals(rangedatatype)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataProp == null) ? 0 : dataProp.getURI().hashCode());
		result = prime * result + ((typeRes == null) ? 0 : typeRes.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		DataPropTypeRestrictionPattern other = (DataPropTypeRestrictionPattern) obj;
		if (dataProp == null) { if (other.dataProp != null) return false;
		} else if (!dataProp.getURI().equals(other.dataProp.getURI())) return false;
		if (typeRes == null) { if (other.typeRes != null) return false;
		} else if (!typeRes.getURI().equals(other.typeRes.getURI())) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "DataProp { " + getName(dataProp) +" (" + getName(typeRes) + ") }-DTR";
	}
	

}
