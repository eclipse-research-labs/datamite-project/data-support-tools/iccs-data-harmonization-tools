package org.ntua.web.oat.pat.onto.dataprop;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.OntResource;
import org.timchros.core.tuple.Tuple3;

public class PropertyValueRestrictionPattern extends A_DataPropPattern {

	private final DatatypeProperty dataProp;
	private final String comparator;
	private final String value;
	
	public PropertyValueRestrictionPattern(DatatypeProperty dataProp, String comparator, String value) {
		super();
		this.dataProp = dataProp;
		this.comparator = comparator;
		this.value = value;
	}

	public DatatypeProperty getDataProp() {
		return dataProp;
	}

	public String getComparator() {
		return comparator;
	}

	public String getValue() {
		return value;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyDataPropAttr() {
		return null;
	}

	@Override
	public String toString() {
		return 
			"PropertyValueRestrictionPattern [" +
				"dataProp = \"" + dataProp + "\" , " +
				"comparator = \"" + ( ( comparator != null ) ? comparator.getClass().getName() : "null" ) + "\" , " +
				"value = \"" + value + "\" " +
			"]";
	}
	
}
