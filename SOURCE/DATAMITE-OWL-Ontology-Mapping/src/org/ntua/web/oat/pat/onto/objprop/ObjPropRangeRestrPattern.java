package org.ntua.web.oat.pat.onto.objprop;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntResource;
import org.ntua.web.oat.pat.onto.cls.SimpleOntoClassPattern;
import org.timchros.core.tuple.Tuple3;

public final class ObjPropRangeRestrPattern extends A_ObjPropPattern {

	private final ObjectProperty objProp;
	private final OntClass rangeClass;
	
	public ObjPropRangeRestrPattern(ObjectProperty objProp, OntClass rangeClass) {
		super();
		this.objProp = objProp;
		this.rangeClass = rangeClass;
	}

	public ObjectProperty getObjProp() {
		return objProp;
	}

	public OntClass getRangeClass() {
		return rangeClass;
	}

	@Override
	public List<OntResource> getCriticalElements() {
		final List<OntResource> list = new ArrayList<OntResource>();
		list.add(objProp);
		return list;
	}
	
	@Override
	public Tuple3<OntResource, String, OntResource> getImplyObjPropAttr() {
		return Tuple3.tuple(objProp.getDomain(), getName(objProp), (OntResource) rangeClass);
	}
	
	@Override
	public JSONObject toJson() {
		final JSONObject json = new JSONObject();
		json.put("pid", "RelationRangeRestrictionPattern");
		json.put("relation", new SimpleObjPropPattern(objProp).toJson());
		json.put("rangeclass", new SimpleOntoClassPattern(rangeClass).toJson());
		return json;
	}
	
	@Override
	public boolean equalsWithJson(JSONObject json) {
		final String pid = json.has("pid") ? json.getString("pid") : null;
		if (!"RelationRangeRestrictionPattern".equals(pid)) return false;
		final JSONObject relationJson = json.has("relation") ? json.getJSONObject("relation") : null;
		if (!new SimpleObjPropPattern(this.objProp).equalsWithJson(relationJson)) return false;
		final JSONObject rangeclassJson = json.has("rangeclass") ? json.getJSONObject("rangeclass") : null;
		if (!new SimpleOntoClassPattern(this.rangeClass).equalsWithJson(rangeclassJson)) return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rangeClass.getURI() == null) ? 0 : rangeClass.getURI().hashCode());
		result = prime * result + ((objProp.getURI() == null) ? 0 : objProp.getURI().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ObjPropRangeRestrPattern other = (ObjPropRangeRestrPattern) obj;
		if (rangeClass == null) { if (other.rangeClass != null) return false;
		} else if (!rangeClass.getURI().equals(other.rangeClass.getURI())) return false;
		if (objProp == null) { if (other.objProp != null) return false;
		} else if (!objProp.getURI().equals(other.objProp.getURI())) return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "ObjProp { " + getName(objProp) +" (" + getName(rangeClass) + ") }-RRR";
	}
	
}
