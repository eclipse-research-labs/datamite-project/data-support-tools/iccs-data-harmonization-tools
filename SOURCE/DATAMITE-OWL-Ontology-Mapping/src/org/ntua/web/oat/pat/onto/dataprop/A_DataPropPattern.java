package org.ntua.web.oat.pat.onto.dataprop;

import org.apache.jena.ontology.OntResource;
import org.ntua.web.oat.pat.onto.A_Pattern;
import org.timchros.core.tuple.Tuple3;

public abstract class A_DataPropPattern extends A_Pattern {

	public abstract Tuple3<OntResource, String, OntResource> getImplyDataPropAttr();
	
}
