package org.ntua.web.oat;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import net.sf.json.JSONObject;

import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.ObjectProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.log4j.Logger;
import org.timchros.core.transformers.I_StringTransformer;
import org.timchros.core.util.Throable;


/**
 * Provides HTML data for LEFT and RIGHT panels of OAT.
 * 
 * @author Efthymios Chondrogiannis
 */
@WebServlet("/OntoPanelServlet")
public class OntoPanelServlet extends HttpServlet {
	
	private static final Logger log = LogFactory.getLoggerForClass(OntoPanelServlet.class);
	
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unused")
	private static final String TopObjectPropertyUrl = "http://www.w3.org/2002/07/owl#topObjectProperty";
	@SuppressWarnings("unused")
	private static final String TopDatatypePropertyUrl = "http://www.w3.org/2002/07/owl#topDatatypeProperty";
	
    public OntoPanelServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			final String sid = ServletUtil.getSessionID();
			
			// Input Data
			final String jsonStr = request.getParameter("jsonStr");
			final String elements = request.getParameter("elements");
			
			log.trace("SID: " + sid + " , ACTION: \"" + elements + "\" , JSON: " + jsonStr);
			if (jsonStr == null ) return;
			
			// Read/Get OntModel
			final OntModel ontModel = OntoRep.getInstance().getOntoModelByJsonStr(jsonStr);
			
			String strResponse = "";
			if ("Classes".equals(elements)) {
				strResponse = getClassesHtml(ontModel);
			} else if ("ObjectProperties".equals(elements)) { 
				strResponse = getObjectPropertiesHtml(ontModel);
			} else if ("DatatypeProperties".equals(elements)) {
				strResponse = getDatatypePropertiesHtml(ontModel);
			} else if ("Individuals".equals(elements)) {
				strResponse = getIndividualsHtml(ontModel);
			}
			
			// Response
			response.setContentType("text/plain; charset=UTF-8");
			final PrintWriter pw = response.getWriter();
			pw.print(strResponse);
			pw.close();
			
			log.debug("SID: " + sid + " :: ACTION: \"" + elements + "\" Successfully Completed !!! ResponseLength: " + strResponse.length());
			log.trace("Response-HTML-DATA:\n\t" + strResponse);
		} catch(Throwable tt) {
			final String msg = "Error when uploading the file... " + tt.getMessage();
			log.error(msg + "\n" + Throable.throwableAsString(tt));
	
			final JSONObject errorJsonObj = new JSONObject();
			String returnMsg = Throable.throwableAsSimpleOneLineString(tt);
			if (returnMsg != null && returnMsg.length() > 200) returnMsg = returnMsg.substring(0, 200) + " ...";
			errorJsonObj.put("error", returnMsg);
			response.setContentType("application/json; charset=UTF-8");
			final PrintWriter out = response.getWriter();
			out.print(errorJsonObj.toString());
			out.close();
		}
	}
	
	private boolean isRootClass(OntClass ontClass, List<OntClass> ontClassList) {
		for (OntClass ontClassElement : ontClassList) {
			if (ontClass.hasSuperClass(ontClassElement, true)) return false;
		}
		return true;
	}
	
	private synchronized String getClassesHtml(final OntModel ontModel) {
		final ExtendedIterator<OntClass> ontClassExtIterator = ontModel.listClasses();
		final List<OntClass> ontClassList = new ArrayList<OntClass>();
		while (ontClassExtIterator.hasNext()) {
			final OntClass ontClass = ontClassExtIterator.next();
			if (ontClass.getURI() == null) continue;
			if (ontClass.getURI().equals("http://www.w3.org/2002/07/owl#Thing")) continue;
			ontClassList.add(ontClass);
		}
		
		final List<OntClass> rootOntClassList = new ArrayList<OntClass>();
		for (OntClass ontClass : ontClassList) {
			if (isRootClass(ontClass, ontClassList)){
				rootOntClassList.add(ontClass); 
			}
		}
		
		Collections.sort(rootOntClassList, 
				new Comparator<OntClass>() {
					@Override public int compare(OntClass o1, OntClass o2) {
						return o1.getLocalName().compareTo(o2.getLocalName());
					}
				}
			);
		
		final StringBuilder sb = new StringBuilder();
		sb.append("<li class=\"expandable\"><div class=\"hitarea expandable-hitarea\"></div>" + "<a>Thing</a>" + "<ul style=\"display: none;\">");
		for (OntClass ontClass : rootOntClassList) {
			sb.append( htmlString(ontClass) );
		}
		sb.append("</ul></li>");
		
		return sb.toString();
	}
	
	private synchronized String getObjectPropertiesHtml(final OntModel ontModel) {
		final ExtendedIterator<ObjectProperty> ontoPropertyExtIterator = ontModel.listObjectProperties();
		final List<OntProperty> ontPropertyList = new ArrayList<OntProperty>();
		while (ontoPropertyExtIterator.hasNext()) {
			final OntProperty ontProperty = ontoPropertyExtIterator.next();
			ontPropertyList.add(ontProperty);
		}
		
		final List<OntProperty> rootOntPropertyList = new ArrayList<OntProperty>();
		for (OntProperty ontProperty : ontPropertyList) {
			if (isRootProperty(ontProperty, ontPropertyList)){
				rootOntPropertyList.add(ontProperty);
			}
		}
		
		Collections.sort(rootOntPropertyList, 
			new Comparator<OntProperty>() {
				@Override public int compare(OntProperty o1, OntProperty o2) {
					return o1.getLocalName().compareTo(o2.getLocalName());
				}
			}
		);
		
		final StringBuilder sb = new StringBuilder();
		sb.append("<li class=\"expandable\"><div class=\"hitarea expandable-hitarea\"></div>" + "<a>topObjectProperty</a>" + "<ul style=\"display: none;\">");
		for (OntProperty ontProperty : rootOntPropertyList) {
			sb.append( htmlString(ontProperty) );
		}
		sb.append("</ul></li>");
		return sb.toString();
	}
	
	private synchronized String getDatatypePropertiesHtml(final OntModel ontModel) {
		final ExtendedIterator<DatatypeProperty> ontoPropertyExtIterator = ontModel.listDatatypeProperties();
		final List<OntProperty> ontPropertyList = new ArrayList<OntProperty>();
		while (ontoPropertyExtIterator.hasNext()) {
			final OntProperty ontProperty = ontoPropertyExtIterator.next();
			ontPropertyList.add(ontProperty);
		}
		
		final List<OntProperty> rootOntPropertyList = new ArrayList<OntProperty>();
		for (OntProperty ontProperty : ontPropertyList) {
			if (isRootProperty(ontProperty, ontPropertyList)){
				rootOntPropertyList.add(ontProperty);
			}
		}
		
		Collections.sort(rootOntPropertyList, 
			new Comparator<OntProperty>() {
				@Override public int compare(OntProperty o1, OntProperty o2) {
					return o1.getLocalName().compareTo(o2.getLocalName());
				}
			}
		);
		
		final StringBuilder sb = new StringBuilder();
		sb.append("<li class=\"expandable\"><div class=\"hitarea expandable-hitarea\"></div>" + "<a>topDatatypeProperty</a>" + "<ul style=\"display: none;\">");
		for (OntProperty ontProperty : rootOntPropertyList) {
			sb.append( htmlString(ontProperty) );
		}
		sb.append("</ul></li>");
		return sb.toString();
	}
	
	private synchronized String getIndividualsHtml(final OntModel ontModel) {
		final ExtendedIterator<Individual> individualExtIterator = ontModel.listIndividuals();
		final List<Individual> individualList = new ArrayList<Individual>();
		while (individualExtIterator.hasNext()) {
			final Individual ontProperty = individualExtIterator.next();
			individualList.add(ontProperty);
		}
		
		Collections.sort(individualList, 
			new Comparator<Individual>() {
				@Override public int compare(Individual o1, Individual o2) {
					return o1.getLocalName().compareTo(o2.getLocalName());
				}
			}
		);
		
		final StringBuilder sb = new StringBuilder();
		sb.append("<li class=\"expandable\"><div class=\"hitarea expandable-hitarea\"></div>" + "<a>topIndividual</a>" + "<ul style=\"display: none;\">");
		for (Individual individual : individualList) {
			sb.append( "<li>" + individualHtml(individual) + "</li>" );
		}
		sb.append("</ul></li>");
		return sb.toString();
	}
	
	
	private String htmlString(OntClass ontClass) {
		if (ontClass == null) return "";
		if (ontClass.hasSubClass()) {
			final ExtendedIterator<OntClass> ontClassExtIterator = ontClass.listSubClasses(true);
			final List<OntClass> ontClassList = new ArrayList<OntClass>();
			while (ontClassExtIterator.hasNext()) {
				final OntClass tmpOntClass = ontClassExtIterator.next();
				ontClassList.add(tmpOntClass);
			} 
			Collections.sort(ontClassList, 
				new Comparator<OntClass>() {
					@Override public int compare(OntClass o1, OntClass o2) {
						return o1.getLocalName().compareTo(o2.getLocalName());
					}
				}
			);
			return 
				"<li class=\"expandable\"><div class=\"hitarea expandable-hitarea\"></div>" + ontClassHtml(ontClass) + 
					"<ul style=\"display: none;\">" + 
						ontClassListToString(ontClassList, 
							new I_StringTransformer<OntClass>() {
								@Override public String asString(OntClass ontClass) {
									return htmlString(ontClass);
								}
							}
						) + 
					"</ul>" + 
				"</li>";
			
		} else {
			return "<li>" + ontClassHtml(ontClass) + "</li>";
		}
	}
	
	public static String ontClassListToString(List<OntClass> ontClassList, I_StringTransformer<OntClass> strTransformer) {
		final StringBuilder sb = new StringBuilder();
		for (OntClass ontClass : ontClassList) {
			sb.append(strTransformer.asString(ontClass));
		}
		return sb.toString();
	}
	
	private String ontClassHtml(OntClass ontClass) {
		final String uri = ontClass.getURI();
		if (uri != null) {
			//if (uri.equals("http://www.w3.org/2002/07/owl#Thing")) return "Thing";
			String label = ontClass.getLabel(null);
			if (label == null) {
				label = ontClass.getLocalName(); 
			}
			return 
				"<a href=\"" + uri + "\">" +
					label +
				"</a>";	
		} else {
			return uri;
		}
	}
	
	private boolean isRootProperty(OntProperty ontProperty, List<OntProperty> ontPropertyList) {
		for (OntProperty ontPropertyElement : ontPropertyList) {
			if (ontProperty.hasSuperProperty(ontPropertyElement, true)) return false;
		}
		return true;
	}
	
	public static String htmlString(OntProperty ontProperty) {
		if (ontProperty == null) return "";
		
		final ExtendedIterator<? extends OntProperty> ontClassExtIterator = ontProperty.listSubProperties(true);
		final List<OntProperty> ontPropList = new ArrayList<OntProperty>();
		while (ontClassExtIterator.hasNext()) {
			final OntProperty tmpOntProp = ontClassExtIterator.next();
			ontPropList.add(tmpOntProp);
		}
		Collections.sort(ontPropList, 
				new Comparator<OntProperty>() {
					@Override public int compare(OntProperty o1, OntProperty o2) {
						return o1.getLocalName().compareTo(o2.getLocalName());
					}
				}
			);
		if (ontPropList.size() > 0) {
			return 
				"<li class=\"expandable\"><div class=\"hitarea expandable-hitarea\"></div>" + ontPropertyHtml(ontProperty) + 
					"<ul style=\"display: none;\">" + 
						ontPropertyListToString(ontPropList, 
							new I_StringTransformer<OntProperty>() {
								@Override public String asString(OntProperty ontProperty) {
									return htmlString(ontProperty);
								}
							}
						) + 
					"</ul>" + 
				"</li>";
			
		} else {
			return "<li>" + ontPropertyHtml(ontProperty) + "</li>";
		}
	}
	
	public static String ontPropertyListToString(List<OntProperty> ontClassList, I_StringTransformer<OntProperty> strTransformer) {
		final StringBuilder sb = new StringBuilder();
		for (OntProperty ontClass : ontClassList) {
			sb.append(strTransformer.asString(ontClass));
		}
		return sb.toString();
	}	
	
	public static String ontPropertyHtml(OntProperty ontProperty) {
		final String uri = ontProperty.getURI();
		if (uri != null) {
			//if (uri.equals("http://www.w3.org/2002/07/owl#Thing")) return "Thing";
			String label = ontProperty.getLabel(null);
			if (label == null) {
				label = ontProperty.getLocalName(); 
			}
			return 
				"<a href=\"" + uri + "\">" +
					label +
				"</a>";	
		} else {
			return uri;
		}
	}
	
	public static String individualHtml(Individual individual) {
		final String uri = individual.getURI();
		if (uri != null) {
			//if (uri.equals("http://www.w3.org/2002/07/owl#Thing")) return "Thing";
			String label = individual.getLabel(null);
			if (label == null) {
				label = individual.getLocalName(); 
			}
			return 
				"<a href=\"" + uri + "\">" +
					label +
				"</a>";	
		} else {
			return uri;
		}
	}
	
}

