package org.ntua.web.oat.xls.cls;

import java.util.HashSet;
import java.util.Set;

public class VocabData {

	private final String name;
	private final Set<String> attrIDsSet = new HashSet<String>();
	private final Set<TermData> termsSet = new HashSet<TermData>();
	
	public VocabData(String name) {
		this.name = name;
	}

	public VocabData(String name, Set<String> attrIDsSet) {
		this.name = name;
		this.attrIDsSet.addAll(attrIDsSet);
	}
	
	public VocabData(String name, Set<String> attrIDsSet, Set<TermData> termsSet) {
		this.name = name;
		this.attrIDsSet.addAll(attrIDsSet);
		this.termsSet.addAll(termsSet);
	}
	
	public String getName() {
		return name;
	}

	public Set<String> getAttrIDsSet() {
		return attrIDsSet;
	}

	public Set<TermData> getTermsSet() {
		return termsSet;
	}

	@Override
	public String toString() {
		return "VocabData [name=" + name+ ", attrIDsSet=" + attrIDsSet + ", termsSet=" + termsSet + "]";
	}
	
}
