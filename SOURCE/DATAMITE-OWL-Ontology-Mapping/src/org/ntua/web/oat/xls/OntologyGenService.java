package org.ntua.web.oat.xls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.DatatypeProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.Ontology;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.XSD;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.timchros.core.tuple.Tuple3;
import org.timchros.core.util.Checks;


import org.ntua.web.oat.xls.cls.AttrData;
import org.ntua.web.oat.xls.cls.CohortData;
import org.ntua.web.oat.xls.cls.TermData;
import org.ntua.web.oat.xls.cls.VocabData;

public class OntologyGenService {

	private static final String CREATOR = "Institute of Communications and Computer Systems of National Technical University of Athens - ICCS/NTUA";
	
	/**
	 * For testing purposes ...
	 */
	public static void main(String[] args) throws Exception {

		System.out.println(" >> OntologyGenService:: Process STARTED at " + new Date() + " ... ");
		
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.WARN);
		
		final String xlsFilePath = 
			"C:/Users/Efthymios/Desktop/WSP/" + "QMUL-2019-04-11-Cohort-Metadata-FINAL-v.0.3.xlsx";	
		final String owlFilePath = 
			"C:/Users/Efthymios/Desktop/WSP/" + "QMUL-Cohort-Ontology.owl";
		
		generateOntology(xlsFilePath, owlFilePath);
		
		System.out.println(" >> OntologyGenService:: Process COMPLETED at " + new Date() + " ... ");
	}
	
	public static synchronized void generateOntology(String xlsFilePath, String owlFilePath) throws Exception {
		Checks.checkNotNullArg(xlsFilePath, "xlsFilePath", owlFilePath, "owlFilePath");
		
		// LOAD XLS FILE (Parameters and Values)
		final Tuple3<CohortData, List<AttrData>, List<VocabData>> tuple = getDataFromXLS(xlsFilePath);
		
		// CREATE OWL ONTOLOGY
		final OntModel ontModel = createOntology(tuple);
		
		// SAVE to FILE
		saveOntoToFile(ontModel, owlFilePath);
	}
	
 	public static OntModel createOntology(final Tuple3<CohortData, List<AttrData>, List<VocabData>> tuple) throws Exception {
 		Checks.checkNotNullArg(tuple, "tuple");
 		
 		final CohortData cohortData = tuple._1();
 		final List<AttrData> attrDataList = tuple._2();
 		final List<VocabData> vocabDataList = tuple._3();
 		
 		final String OntologyURI = "http://www.semanticweb.org/ntua/iccs/datamite/cohort";
        final String NS = OntologyURI + "#";
        
        // TODO: Present additional cohortData in comments 
        // including fields number, Vocabs number and notes
        
        final OntModel ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
        final Ontology onto = ontModel.createOntology(OntologyURI);
        final String ontoLabel = "Cohort (Metadata)";
        onto.addLabel(ontoLabel, "en");
        onto.addComment(
			"An ontological representation of the Cohort parameters. \n\n" +
			"The ontology has been automatically generated based on the uploaded Excel Document with Cohort Metadata (extracted on Date: <b>" + cohortData.getXlsMetadataDate() + "</b>).\n\n" + 
			"This document has been also automatically generated based on the data recorded about <b>" + cohortData.getXlsPatientNumber() + " entities</b>.\n\n" +
			"For each entity, there are <b>" + cohortData.getXlsPatientField() + " Fields</b> (Parameters aka Properties), the value of which " +
			"often come from one out of <b>" + cohortData.getXlsVocabularies() + "</b> different controlled <b>Set of Terms</b>.\n\n" +
			((cohortData.getXlsNotes() != null && !cohortData.getXlsNotes().trim().equals("")) ? cohortData.getXlsNotes() + "\n\n" : "")
			// + "Metadata extracted have been revised by technical experts (in close collaboration with DB experts)."
			, "en"
		);
		final String dateStr = "Generated Date:" + new SimpleDateFormat("dd/MM/yyyy").format(new Date());
		onto.addProperty(DC.date, dateStr);
		onto.addProperty(DC.creator, CREATOR);
		
		// PART A: REFERENCE MODEL
		
		final String exParamUidURI = NS + "parameter-UID";
		final AnnotationProperty exParamUidAP = ontModel.createAnnotationProperty(exParamUidURI);
		exParamUidAP.setLabel("Parameter UID", "en");
		exParamUidAP.setComment("Indicates the position of the parameter/field in the Excell document.", "en");
		
		final String paramValuesSetURI = NS + "parameter-Value";
		final AnnotationProperty paramValuesSetAP = ontModel.createAnnotationProperty(paramValuesSetURI);
		paramValuesSetAP.setLabel("Parameter Value", "en");
		
        // A.1 Root Classes & Properties
		final String dataURI = NS + "Person-Data";
		final OntClass dataOntClass = ontModel.createClass(dataURI);
		dataOntClass.setLabel("Data", "en");
		dataOntClass.setComment("Data recorded about each entity.", "en");

		// Find Categories and assign a distinct number to each one of them
        int count = 0;
        final Map<String, Integer> categMap = new HashMap<String, Integer>();
        for (AttrData attrData : attrDataList) {
        	if (attrData.getCategory() != null) {
        		if (!categMap.containsKey(attrData.getCategory())) categMap.put(attrData.getCategory(), ++count);
        	}
        	if (attrData.getSubCategory() != null) {
        		if (!categMap.containsKey(attrData.getSubCategory())) categMap.put(attrData.getSubCategory(), ++count);
        	}
		}
        
		// A.2 Create SubClasses (Categories) and Parameters of each one
        final String ABOUT = "About .. ";
        for (AttrData attrData : attrDataList) {

        	// Create Categories Properties
        	DatatypeProperty categProp = null;
        	if (attrData.getBroadNarrowCategories() == null) {
        		final String otherPropURI = NS + "Other-Parameters";
        		categProp = ontModel.getDatatypeProperty(otherPropURI);
        		if (categProp == null) {	
        			categProp = ontModel.createDatatypeProperty(otherPropURI);
        			categProp.setLabel("Other Parameters", "en");
        		}
        	} else {
        		DatatypeProperty broadCategProp = null;
        		final String broadCategCls = attrData.getBroadNarrowCategories()._1();
            	if (broadCategCls !=null) {
            		int broadCategIndex = categMap.get(broadCategCls);
            		final String broadCategPropURI = NS + "Category-" + get3DigitNum(broadCategIndex);
            		broadCategProp = ontModel.getDatatypeProperty(broadCategPropURI);
            		if (broadCategProp == null) {
            			broadCategProp = ontModel.createDatatypeProperty(broadCategPropURI);
            			broadCategProp.setLabel(ABOUT + broadCategCls, "en");
            		}
            	}
            	final String narrowCategCls = attrData.getBroadNarrowCategories()._2();
            	final int narrowCategIndex = categMap.get(narrowCategCls);
            	final String narrowCategPropURI = NS + "Category-" + get3DigitNum(narrowCategIndex);
            	categProp = ontModel.getDatatypeProperty(narrowCategPropURI);
            	if (categProp == null) {
            		categProp = ontModel.createDatatypeProperty(narrowCategPropURI);
            		categProp.setLabel(ABOUT + narrowCategCls, "en");
            		if (broadCategProp != null) {
            			categProp.addSuperProperty(broadCategProp);
            		}
        		}
        	}        	
        	
        	// Create Data Properties
			final DatatypeProperty dataProp = ontModel.createDatatypeProperty(NS + "Parameter-" + convertAbbrIdToNumber(attrData.getID()) );
			dataProp.setLabel(attrData.getName(), "en");
			dataProp.addLiteral(exParamUidAP, attrData.getID());
			
			final StringBuilder commentSb = new StringBuilder();
			if (attrData.hasDescription()) 
				commentSb.append(attrData.getDescription());
			if (attrData.hasDataType()) 
				commentSb.append("\n\nData Type: " + attrData.getDataType());
			if (attrData.hasDataNotes()) 
				commentSb.append("\n\n" + attrData.getDataNotes());
			commentSb.append("\n\nCan be Empty: " + attrData.getCanBeEmpty() + " , Has Many Terms/Values: " + attrData.getHasManyValues());
			
			dataProp.setComment(commentSb.toString(), "en");
			dataProp.setDomain(dataOntClass);
			dataProp.setRange(XSD.xstring);
			dataProp.addSuperProperty(categProp);
			
			// Add restriction about an entity of the Class "Patient Data"
			dataOntClass.addSuperClass(ontModel.createCardinalityRestriction(null, dataProp, 1));
			
        } // END OF attrDataList LOOP
		
        // PART B: VOCABULARIES
        
		final String annoPropURI = NS + "values-of-Parameter";
		final AnnotationProperty newAnnoProp = ontModel.createAnnotationProperty(annoPropURI);
		newAnnoProp.setLabel("Values of Parameter", "en");
		newAnnoProp.setComment("Parameter UID followed by its NAME, separated by semicolon (:)", "en");
        
     	// B.1. Root OWL classes
        final String vocabURI = NS + "Vocab";
        final OntClass vocabOntClass = ontModel.createClass(vocabURI);
        vocabOntClass.setLabel("Term", "en");
        vocabOntClass.setComment("A value from a predefined set of Terms", "en");
        
        // B.2. Domains OWL classes
        int vocIndex = 0; 
        for (VocabData vocab : vocabDataList) {
        	vocIndex++;
        	final String domainURI = NS + "Domain-" + get3DigitNum(vocIndex);
        	final OntClass domainOntClass = ontModel.createClass(domainURI);
        	domainOntClass.setLabel(vocab.getName(), "en");
        	domainOntClass.setComment("Set of terms used in the value of " + vocab.getAttrIDsSet().size() + " parameter(s).", "en");
        	domainOntClass.addSuperClass(vocabOntClass);
        	final List<String> propIDsList = new ArrayList<>(vocab.getAttrIDsSet());
        	Collections.sort(propIDsList, attrIDsStrComparator);
        	for (String propID : propIDsList) {
        		final AttrData referAttrData = getAttrDataFromListByID(attrDataList, propID);
        		if (referAttrData == null)
        			throw new RuntimeException("Processing Data existing in the 3rd sheet (Columns about: \"" + vocab.getName() + "\")... Cannot FIND relevant Field with ID: \"" + propID + "\" in the Fields specified in the 2nd sheet !");
				final String annoData = referAttrData.getID() + " : " + referAttrData.getName();
				domainOntClass.addLiteral(newAnnoProp, annoData);
			}
        	
        	// Add a Reference in properties/fields to this Class/Values
        	for (String attrID : vocab.getAttrIDsSet()) {
				ExtendedIterator<DatatypeProperty> extIt = ontModel.listDatatypeProperties();
				while (extIt.hasNext()) {
					final DatatypeProperty prop = extIt.next();
					final String pid = "" + prop.getPropertyValue(exParamUidAP);
					if (pid != null && pid.equals(attrID)) {
						prop.addProperty(paramValuesSetAP, domainOntClass);
						break;
					}
				}
			}
        	
        	// B.3. Terms (of each Domain) OWL classes
        	final List<TermData> termList = new ArrayList<>(vocab.getTermsSet());
        	Collections.sort(termList, termDataComparator);
        	int termIndex = 0;
        	for (TermData term : termList) {
        		termIndex++;
        		final String termURI = domainURI + "-Term-" + get3DigitNum(termIndex);
        		final OntClass termOntClass = ontModel.createClass(termURI);
        		termOntClass.setLabel(term.getLabel(), "en");
        		if (term.hasDescription()) termOntClass.setComment(term.getDescription(), "en");
        		termOntClass.addSuperClass(domainOntClass);
			} // END OF termList LOOP

		} // END OF vocabDataList LOOP
        
 		return ontModel;
 	}
 	
 	private static AttrData getAttrDataFromListByID(final List<AttrData> attrDataList, final String attrID) {
		Checks.checkNotNullArg(attrDataList, "attrDataList", attrID, "attrID");
 		for (AttrData  attrData : attrDataList) {
			if (attrData.getID().equals(attrID)) {
				return attrData;
			}
		}
		return null;
 	}
 	
 	private static final Comparator<String> attrIDsStrComparator = new Comparator<String>() {
		@Override public int compare(String s1, String s2) {
			if (s1 == null || s2 == null) return 0;
			if (s1.length() == s2.length()) {
				return s1.compareTo(s2);
			} else {
				return s1.length() - s2.length();
			}
		}
	};
 	
 	private static final Comparator<TermData> termDataComparator = new Comparator<TermData>() {
		@Override public int compare(TermData t1, TermData t2) {
			return t1.getLabel().compareToIgnoreCase(t2.getLabel());
		}
	};
	
 	public static Tuple3<CohortData, List<AttrData>, List<VocabData>> getDataFromXLS(final String xlsFilePath) throws Exception {
		if (xlsFilePath == null || xlsFilePath.trim().equals("")) 
			throw new RuntimeException("Parameter: xlsFilePath NOT specified !");
		if ( (!xlsFilePath.toLowerCase().endsWith(".xls")) && (!xlsFilePath.toLowerCase().endsWith(".xlsx"))) 
			throw new RuntimeException("File \"" + xlsFilePath + "\" is NOT an Excel Document !");
		
		final Workbook workbook = 
			(xlsFilePath.toLowerCase().endsWith(".xls")) ? 
				new HSSFWorkbook(new FileInputStream( new File(xlsFilePath) ) ) :
			(xlsFilePath.toLowerCase().endsWith(".xlsx")) ? 
				new XSSFWorkbook( new FileInputStream( new File(xlsFilePath) ) ) : null;

		// Overview
		final Sheet sheet0 = workbook.getSheetAt(0);	
		
		final String xlsFileName = getCellData(sheet0.getRow(4).getCell(1));
		final String xlsFileSheets = getCellData(sheet0.getRow(5).getCell(1));
		final String xlsPatientNumber = getCellData(sheet0.getRow(7).getCell(1));
		final String xlsPatientField = getCellData(sheet0.getRow(8).getCell(1));
		final String xlsVocabularies = getCellData(sheet0.getRow(9).getCell(1));
		final String xlsMetadataDate = getCellData(sheet0.getRow(11).getCell(1));
		
		final StringBuilder notesSb = new StringBuilder();
		final Iterator<Row> rowIt = sheet0.iterator();
		while (rowIt.hasNext()) {
			final StringBuilder rowSb = new StringBuilder();
		   	final Row row = rowIt.next();
		    final int rowNum = row.getRowNum();
		    if (rowNum < 22) continue;
		    final Iterator<Cell> cellIt = row.cellIterator();
		    while (cellIt.hasNext()) {
		      	final Cell cell = (Cell) cellIt.next();
				final String cellData = getCellData(cell);
				if (cellData == null) continue;
				if (rowSb.length() > 0) rowSb.append(" ");
				rowSb.append(cellData);
		    } // END OF CELLs LOOP
		    if (rowSb.length() > 0) {
		    	if (notesSb.length() > 0) notesSb.append("\n\n");
				notesSb.append(rowSb);
		    }
		} // END OF ROWs LOOP
		
		final String xlsNotes = (notesSb.length() > 0) ? notesSb.toString() : null;
		final CohortData cohortData = new CohortData(xlsFileName, xlsFileSheets, xlsPatientNumber, xlsPatientField, xlsVocabularies, xlsMetadataDate, xlsNotes);
		
		// FIELDS
		final List<AttrData> attrDataList = new ArrayList<>();
		final Sheet sheet1 = workbook.getSheetAt(1);		
		final Iterator<Row> rowIt1 = sheet1.iterator();
		while (rowIt1.hasNext()) {
		   	final Row row = rowIt1.next();
		   	final int rowNum = row.getRowNum();
		   	if (rowNum == 0) continue;
			final String ID = getCellData(row.getCell(0));
			final String category = getCellData(row.getCell(1));
			final String subCategory = getCellData(row.getCell(2));
			final String name = getCellData(row.getCell(3));
			final String description = getCellData(row.getCell(4));
			final String dataType = getCellData(row.getCell(5));
			final String dataNotes = getCellData(row.getCell(6));
			final String canBeEmpty = getCellData(row.getCell(7));
			final String hasManyValues = getCellData(row.getCell(8));
		   	if (areStrsEmpty(ID, category, subCategory, name, description, dataType, dataNotes, canBeEmpty, hasManyValues)) continue;
		   	final AttrData attrData = new AttrData(ID, category, subCategory, name, description, dataType, dataNotes, canBeEmpty, hasManyValues);
		   	// System.out.println(rowNum + " :: " + attrData);
		   	attrDataList.add(attrData);
		} // END OF ROWs LOOP		
		
		// VALUES
		final Sheet sheet2 = workbook.getSheetAt(2);
		final Map<Integer, VocabData> vocMap = new HashMap<>();
		final Row row0 = sheet2.getRow(0);
		final Iterator<Cell> row0cellIt = row0.cellIterator();
		while (row0cellIt.hasNext()) {
			final Cell cell = (Cell) row0cellIt.next();
			final int colNum = cell.getColumnIndex();
			final String cellData = getCellData(cell);
			if (cellData == null) continue;
			final int index = cellData.indexOf(':');
			if (index < 0) throw new RuntimeException("Unable to find SEMICOLON character in column " + colNum + " of the third sheet !");
			final String str1 = cellData.substring(0, index);
			final Set<String> attrIDsSet = new HashSet<>();
			for (String id : str1.split(",")) { attrIDsSet.add(id.trim()); }
			final String str2 = cellData.substring(index+1, cellData.length());
			final String vocName = str2.trim();
			vocMap.put(colNum, new VocabData(vocName, attrIDsSet));
			//System.out.println(colNum + " :: " + vocName + " :: " + attrIDsSet);
		}
		
		for (Entry<Integer, VocabData> entry : vocMap.entrySet()) {
			final int colNum = entry.getKey().intValue();
			final Iterator<Row> sheet2RowIt = sheet2.iterator();
			while (sheet2RowIt.hasNext()) {
				final Row row = sheet2RowIt.next();
				final int rowNum = row.getRowNum();
				if (rowNum <= 1) continue;
				final String label = getCellData(row.getCell(colNum));
				if (label == null) continue;
				final String description = getCellData(row.getCell(colNum+1));
				entry.getValue().getTermsSet().add(new TermData(label, description));
			} // END OF ROWs LOOP	
		}
		final List<Entry<Integer, VocabData>> entryList = new ArrayList<>(vocMap.entrySet());
		Collections.sort(entryList, new Comparator<Entry<Integer, VocabData>>() {
			@Override public int compare(Entry<Integer, VocabData> e1, Entry<Integer, VocabData> e2) {
				return e1.getKey() - e2.getKey();
			}
		});
		
		final List<VocabData> vocabDataList = new ArrayList<>();
		for (Entry<Integer, VocabData> entry : entryList) {
			vocabDataList.add(entry.getValue());
		}
		
	    workbook.close();
	    
		return new Tuple3<CohortData, List<AttrData>, List<VocabData>>(cohortData, attrDataList, vocabDataList);
	}
 	
	private static final DateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
	private static final NumberFormat numFormater = new DecimalFormat("#.####");
	
	/** @return Data existing in a specific Cell of an Excel File in the form of a String */
	private static String getCellData(Cell cell) {
		if (cell == null || cell.toString().trim().equals("")) return null;
		if (cell.getCellTypeEnum() == CellType.STRING) {
			return cell.getStringCellValue().replaceAll("\\s+", " ").trim();
		} else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				return dateFormater.format(cell.getDateCellValue());
		    } else {
		    	return numFormater.format(cell.getNumericCellValue());
		    }
		} else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
			return ("" + cell.getBooleanCellValue()).toLowerCase();
		} else if (cell.getCellTypeEnum() == CellType.FORMULA) {
			return "FORMULA: " + cell;
		} else if (cell.getCellTypeEnum() == CellType.BLANK) {
			return null;
		} else if (cell.getCellTypeEnum() == CellType.ERROR) {
			return null;
		} else if (cell.getCellTypeEnum() == CellType._NONE) {
			System.out.println("NONE " + cell);
			return "NONE: " + cell;
		} else {
			System.out.println(" ****** " + cell);
			return cell.toString().trim();
		}
			
	}

	private static boolean areStrsEmpty(String... strs) {
 		boolean flag = true;
 		for (String str : strs) {
			if (str != null && !str.trim().equals("")) {
				flag = false; break;
			}
		}
 		return flag;
 	}
	
	private static void saveOntoToFile(OntModel ontModel, String path) throws IOException {
		final FileOutputStream fos = new FileOutputStream( new File(path) );
		fos.write("<?xml version=\"1.0\"?>\n".getBytes());
		ontModel.write(fos, "RDF/XML");
		fos.flush();
		fos.close();
	}
	
	private static String get2DigitNum(final int num) {
		if (num >= 10) return "" + num;
		return "0" + num;
	}
	
	private static String get3DigitNum(final int num) {
		if (num >= 100) return "" + num;
		if (num >= 10) return "0" + num;
		return "00" + num;
	}
	
	private static final int BASE = 26;
	
	
	private static String convertAbbrIdToNumber(final String id) {
		final int index = id.indexOf('-');
		
		if (index <= 0) {
			return get3DigitNum( abcStrToNumber(id) );
		} else {
			final String str1 = id.substring(0, index);
			final String str2 = id.substring(index+1, id.length());
			return get2DigitNum(Integer.parseInt(str1)) + "-" + get3DigitNum(abcStrToNumber(str2));
		}
	}
	
	private static int abcStrToNumber(final String str) {
		if (str == null || str.trim().equals(""))
			throw new RuntimeException("String is empty.. str= " + str);
		
		Integer n = 0;
		
		for (int i = 0; i < str.length() - 1; i++) {
			final char c = str.charAt(i);
			final int digit = (int) c - 'A' + 1;
			final int pow = str.length() - i - 1;
			final int number = digit * (int) Math.pow(BASE, pow);
			n += number;
		}
		final char lastChar = str.charAt(str.length()-1);
		final int lastNumber = (int) lastChar - 'A';
		n += lastNumber;
		
		return n;
	}
	
}
