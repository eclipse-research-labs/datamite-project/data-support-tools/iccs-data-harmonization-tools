package org.ntua.web.oat.xls.cls;


import org.timchros.core.tuple.Tuple2;

public class AttrData {

	private final String ID;
	private final String category;
	private final String subCategory;
	private final String name;
	private final String description;
	private final String dataType;
	private final String dataNotes;
	private final String canBeEmpty;
	private final String hasManyValues;
	
	public AttrData(String ID, String category, String subCategory, String name, String description, String dataType, String dataNotes, String canBeEmpty, String hasManyValues) {
		this.ID = ID.trim();
		this.category = (category != null) ? category.trim() : null;
		this.subCategory = (subCategory != null) ? subCategory.trim() : null;
		this.name = name.trim();
		this.description = (description != null) ? description.trim() : null;
		this.dataType = (dataType != null) ? dataType.trim() : "STRING";
		this.dataNotes = dataNotes;
		this.canBeEmpty = (canBeEmpty != null) ? canBeEmpty.trim() : "NO";
		this.hasManyValues = (hasManyValues != null) ? hasManyValues.trim() : "NO";;
	}

	public String getID() {
		return ID;
	}

	public Tuple2<String, String> getBroadNarrowCategories() {
		if (category != null && subCategory != null) {
			return Tuple2.tuple(category, subCategory);
		} else {
			if (subCategory != null) return Tuple2.tuple(null, subCategory);
			if (category != null) return Tuple2.tuple(null, category);
			return null;
		}
	}
	
//	public String getBroadCategory() {
//		if (category != null) return category;
//		if (subCategory != null) return subCategory;
//		return null;
//	}
	
	public String getCategory() {
		return category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	public boolean hasDescription() {
		return isNotNullOrEmpty(description);
	}

	public String getDataType() {
		return dataType;
	}
	
	public boolean hasDataType() {
		return isNotNullOrEmpty(dataType);
	}
	
	public String getDataNotes() {
		return dataNotes;
	}
	
	public boolean hasDataNotes() {
		return isNotNullOrEmpty(dataNotes);
	}

	public String getCanBeEmpty() {
		return canBeEmpty;
	}
	
	public boolean canBeEmpty() {
		return (canBeEmpty != null) && canBeEmpty.trim().equalsIgnoreCase("YES");
	}

	public String getHasManyValues() {
		return hasManyValues;
	}
	
	public boolean hasManyValues() {
		return (hasManyValues != null) && hasManyValues.trim().equalsIgnoreCase("YES");
	}
	
	private boolean isNotNullOrEmpty(String str) {
		return ((str != null) && !str.trim().equals("") && !str.trim().equals("-"));
	}

	@Override
	public String toString() {
		return "AttrData ["
				+ "ID=" + ID + ", "
				+ "category=" + category + ", "
				+ "subCategory=" + subCategory + ", "
				+ "name=" + name + ", "
				+ "description=" + description + ", "
				+ "dataType=" + dataType + ", "
				+ "dataNotes=" + dataNotes + ", "
				+ "canBeEmpty=" + canBeEmpty + ", "
				+ "hasManyValues=" + hasManyValues + 
			"]";
	}
	
}