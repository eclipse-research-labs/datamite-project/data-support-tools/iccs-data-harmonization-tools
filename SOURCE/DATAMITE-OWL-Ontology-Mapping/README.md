## Ontology Mapping/Alignment Web Application

### Step 1: Generate Web Application WAR File 

 * Prerequisites: 

	**JAVA 17** and **Apache Ant** should ve installed in your machine


### Step 2: Deploy and Run Web Application WAR File 

#### (a) Deploy & Run Locally

 * Access through the Web App GUI
	
	Place the WAR file in the Tomcat webapps folder
	
	Start Tomcat
	
	Access Web Application: 
	
	```
	http://localhost:8080/DOAT
	```
	
#### (b) Containirize and Run 

 * Build the Docker Image using the following command: 
 
	```
	docker build -t datamite-oat-image .
	```
	
 * Verifying the Docker Image
 
	```
	docker images
	```
	
 * Run the Docker Container 
 
	```
	docker run -i -t -d -p 8080:8080 datamite-oat-image
	```
	
 * Access Web App
 
	```
	http://localhost:8080/DOAT
	```

