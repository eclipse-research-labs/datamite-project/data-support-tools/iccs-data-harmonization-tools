// This JS file contains the methods being used in the middle screen (Mapping Workspace) 
// 
// @author: Efthymios Chondrogiannis

/**
 * A Global Variable 
 */
var guiIndex = 0;

/**
 * @return
 * 		Returns the current guiIndex, increased by one. 
 */
function getNextIndex() {
	guiIndex++;
	return guiIndex;
}

/**
 * This method initialize the JSON values of the hidden fields existing within Mapping Workspace tabs (Suggestions and View ALL Correspondences).  
 */
function mappingInit() {
	$("#jsonOntologyA").val("");
	$("#jsonOntologyPropertiesA").val("");
	$("#jsonOntologyFlatListsA").val("{}");
	
	//$("#jsonOntologyB").val("");
	//$("#jsonOntologyPropertiesB").val("");
	//$("#jsonOntologyFlatListsB").val("{}");
	
	var emptyJson = { jsonarray: [] };
	$("#suggestionsHiddenInput").val(JSON.stringify(emptyJson));
	$("#correspondencesHiddenInput").val(JSON.stringify(emptyJson));
    $("#rejectionsHiddenInput").val(JSON.stringify(emptyJson));
    //Disable tabs
    var tabsDivId = '#tabsM';
    //TODO: Comment (if needed)
    //$(tabsDivId).tabs( { disabled: [0, 1, 2, 3] } );
    $(tabsDivId).tabs( { disabled: [1, 3] } );

    $('#mappingform > input[type=file]').change(function(){
        if ($('#mappingform > input[type=file]').val() != null) {
            $('#mappingform > input[type=submit]').removeAttr('disabled');
        }
    });

    $("#selectMapFileRemImg").click(function() {
        $(this).parent().parent().parent().empty();
    });

    $("#mappingform").submit(function(event){
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: 'MappingProvideServlet',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                if (typeof(json.error) != "undefined")  {
                    alert(json.error);
                    return;
                }
                // Place mapping JSON data in hidden field
                $("#correspondencesHiddenInput").val(JSON.stringify(json));
                // Clear suggestions
				$("#suggestionList").empty();
                // Update UI
                updateCorrespondences();
            }
        });
        return false;
    });

    // Handle quick mappings
    $("#narrow, #equal, #broader").click(function() {
        var img = $(this).attr("src");
        var relationStr = (img == "images/narrower.png") ? "Narrower Meaning" : (img == "images/equal.png") ? "Equivalent" : (img == "images/broader.png") ? "Broader Meaning" : "";
        var clsStrA = $.cookie('SelectedClassOntA');
        var clsStrB = $.cookie('SelectedClassOntB');
        if (clsStrA != null && clsStrB != null) {
            var jsonA = { pid: "SimpleClassPattern", classuri: $.parseJSON(clsStrA).uri, classname: $.parseJSON(clsStrA).name };
            var jsonB = { pid: "SimpleClassPattern", classuri: $.parseJSON(clsStrB).uri, classname: $.parseJSON(clsStrB).name };
            var mappingJson = { entity1: jsonA, entity2: jsonB, directTransformation: null, inverseTransformation: null, relation: relationStr, direction: "", comments: "" };
            var jsonStr = $("#correspondencesHiddenInput").val();
            var json = $.parseJSON(jsonStr);
            json.jsonarray.push(mappingJson);
            $("#correspondencesHiddenInput").val(JSON.stringify(json));
            updateCorrespondences();
            $(this).fadeOut("slow").fadeIn("slow");
            return;
        }
        var objPropStrA = $.cookie('SelectedObjPropOntA');
        var objPropStrB = $.cookie('SelectedObjPropOntB');
        if (objPropStrA != null && objPropStrB != null) {
            var jsonA = { pid: "SimpleRelationPattern", relationuri: $.parseJSON(objPropStrA).uri, relationname: $.parseJSON(objPropStrA).name };
            var jsonB = { pid: "SimpleRelationPattern", relationuri: $.parseJSON(objPropStrB).uri, relationname: $.parseJSON(objPropStrB).name };
            var mappingJson = { entity1: jsonA, entity2: jsonB, directTransformation: null, inverseTransformation: null, relation: relationStr, direction: "", comments: "" };
            var jsonStr = $("#correspondencesHiddenInput").val();
            var json = $.parseJSON(jsonStr);
            json.jsonarray.push(mappingJson);
            $("#correspondencesHiddenInput").val(JSON.stringify(json));
            updateCorrespondences();
            $(this).fadeOut("slow").fadeIn("slow");
            return;
        }
        var dataPropStrA = $.cookie('SelectedDataPropOntA');
        var dataPropStrB = $.cookie('SelectedDataPropOntB');
        if (dataPropStrA != null && dataPropStrB != null) {
            var jsonA = { pid: "SimplePropertyPattern", propertyuri: $.parseJSON(dataPropStrA).uri , propertyname: $.parseJSON(dataPropStrA).name };
            var jsonB = { pid: "SimplePropertyPattern", propertyuri: $.parseJSON(dataPropStrB).uri , propertyname: $.parseJSON(dataPropStrB).name };
            var mappingJson = { entity1: jsonA, entity2: jsonB, directTransformation: null, inverseTransformation: null, relation: relationStr, direction: "", comments: "" };
            var jsonStr = $("#correspondencesHiddenInput").val();
            var json = $.parseJSON(jsonStr);
            json.jsonarray.push(mappingJson);
            $("#correspondencesHiddenInput").val(JSON.stringify(json));
            updateCorrespondences();
            $(this).fadeOut("slow").fadeIn("slow");
            return;
        }
        var indivStrA = $.cookie('SelectedIndivOntA');
        var indivStrB = $.cookie('SelectedIndivOntB');
        if (indivStrA != null && indivStrB != null) {
            var jsonA = { pid: "SimpleInstancePattern", instanceuri: $.parseJSON(indivStrA).uri, instancename: $.parseJSON(indivStrA).name };
            var jsonB = { pid: "SimpleInstancePattern", instanceuri: $.parseJSON(indivStrB).uri, instancename: $.parseJSON(indivStrB).name };
            var mappingJson = { entity1: jsonA, entity2: jsonB, directTransformation: null, inverseTransformation: null, relation: relationStr, direction: "", comments: "" };
            var jsonStr = $("#correspondencesHiddenInput").val();
            var json = $.parseJSON(jsonStr);
            json.jsonarray.push(mappingJson);
            $("#correspondencesHiddenInput").val(JSON.stringify(json));
            updateCorrespondences();
            $(this).fadeOut("slow").fadeIn("slow");
            return;
        }

        alert("Elements selected do not belong to the same type (e.g. classes).");
    });
}

// --------------------------------- SUGGESTIONS --------------------------------- 

/**
 * Retrieves the possible correspondences among the ontologies A and B from server, 
 * taking into consideration the correspondences already specified.
 * 
 * The suggestions retrieved are placed in a hidden field while this method 
 * updates the user's screen using the method updateSuggestions
 * 
 * @see updateSuggestions
 * 		This method is being used for updating the user's screen with the suggested correspondences
 */
function getSuggestionsWhenReady() {
	var jsonAstr = $("#jsonOntologyA").val();
	var jsonBstr = $("#jsonOntologyB").val();
	var jsonMstr = $("#correspondencesHiddenInput").val();
    var jsonRstr = $("#rejectionsHiddenInput").val();
	
	if (jsonAstr == "" || jsonBstr == "") return true;

    $("#tabsM").tabs("enable", 0);
    $("#tabsM").tabs("enable", 1);
    $("#tabsM").tabs("enable", 2);
    $("#tabsM").tabs("enable", 3);

    $('#tabsM > #tabs-1 > #divQuickM').show();

	$("#suggestionList").empty();
	$("#suggestionList").append("<img src=\"images/loading.gif\" style=\"width: 400px; height: 250px;\">");
	$("#suggestedAction").hide();
    $("#reloadSuggestionsAction").hide();
	$("#suggestionListExport").hide();
	$("#correspondenceListExport").hide();
	
	$.post(  
		"SuggestionsServlet",  
		{
			ontoAJsonStr: jsonAstr, 
			ontoBJsonStr: jsonBstr,
			mappingsJsonStr: jsonMstr,
			rejectJsonStr: jsonRstr
		},  
		function(json){
            if (typeof(json.error) != "undefined")  {
                $("#tabsM > ul > li > a[href=#tabs-2]").removeAttr('title');
                $("#suggestionList").empty();
                alert(json.error);
                return;
            }

			// Save JSON as String  ...
			$("#suggestionsHiddenInput").val(JSON.stringify(json));
			
			// Update user screen
			updateSuggestions();
			
			$("#reloadSuggestionsAction").show();
            $("#suggestionListExport").show();
		},
		"json"  
	);  // end post function
	
}

/**
 * Reload Suggestions when Reload Button clicked 
 */
function handleReloadAction() {
	$("#reloadSuggestionsButton")
		.button()
	    .click(function( event ) {
			getSuggestionsWhenReady();
            $("#correspondenceListExport").show();
			event.preventDefault();
	    });
}


/**
 * This methods loads the suggested correspondences from the hidden field and 
 * presents them to the end user in a human readable way.
 * 
 * In each one attaches an Accept and Reject button giving the ability to 
 * Accept or Reject them one by one.
 */
function updateSuggestions() {
	// Get suggestions from hidden input field
	var jsonStr = $("#suggestionsHiddenInput").val();
	var json = $.parseJSON(jsonStr);
	
	// Change pupup title of this tab 
	if ( typeof(json.jsonarray) != "undefined" && typeof(json.datetime) != "undefined" ) {
		if (json.jsonarray.length > 0) {
			var thereMessage= "Handle the " + json.jsonarray.length + " suggested Mapping Rule(s), automatically detected by OAT. Process executed at: " + json.datetime;
			$("#tabsM > ul > li > a[href=#tabs-2]").attr("title", thereMessage);
			$("#suggestedAction").show();
		} else {
            var thereMessage= "None Mapping Rule, detected by OAT. Process executed on Date: " + json.datetime;
            $("#tabsM > ul > li > a[href=#tabs-2]").attr("title", thereMessage);
            $("#suggestedAction").show();
		}
	} else {
		$("#suggestedAction").hide();
		$("#tabsM > ul > li > a[href=#tabs-2]").removeAttr('title');
	}
		
	// Update User screen
    //$("#suggestionList").fadeOut("slow");
    $("#suggestionList").empty();
    //$("#suggestionList").fadeIn("fast");

	// FIELDS
    $("#suggestionList").append("<h3>Fields Mapping</h3>");	
    jQuery.each(json.fieldsjsonarray, function(i, item) {
		
		var nextIndex = getNextIndex();
		var sugHiddenInputID = "sugHiddenInput" + 	nextIndex;
				
		var tableStr = 
			"<table class=\"viewMappingTable\">" +
				"<input type=\"hidden\" id=\"" + sugHiddenInputID + "\"/>" + 
				"<tr>" +
					"<td class=\"actionArea\">" +
						"<img src=\"images/delete.icon.png\" class=\"rejectSuggestedMappingImg\">" +
					"</td>" +
					"<td class=\"correspondenceArea\">" +
						"<table class=\"correspondenceTable\">" +
							"<tr>" +
								"<td class=\"viewEntity1\">" + jsonDataToHtml(item.entity1) + "</td>" +
								"<td class=\"viewRelation\">" +
									"<img id=\"FieldConfidenceID" + i + "\" src='images/blueinfo.png' class='blueinfoCls'>" +
									"<select name=\"relation\" id=\"relationSelection\">" +
										"<option>Equivalent</option>" +
										"<option>Broader Meaning</option>" +
										"<option>Narrower Meaning</option>" +
										"<option>Partially Overlapping</option>" +
										"<option selected=\"selected\">Linked With</option>" +
									"</select>" + 
								"</td>" +
								"<td class=\"viewEntity2\">" + jsonDataToHtml(item.entity2) + "</td>" +
							"</tr>" +
						"</table>" +
					"</td>" +
					"<td class=\"actionArea\">" +
						"<img src=\"images/option.icon.png\" class=\"optionsSuggestedMappingImg\">" +
					"</td>" +
				"</tr>" +
			"</table>";
		
		$("#suggestionList").append(tableStr);
		
		if (typeof(item.simcomments) != "undefined") {
			$('#FieldConfidenceID' + i).balloon({
				position: "left",
				offsetY: -10,
				contents: "<div class='balloonRootDivCls'>" + item.simcomments + "</div>",
				showDuration: "slow",
				showAnimation: function(d) { this.fadeIn(d); },
				hideDuration: 0
			});
		}
		
		// Store item json...
		$("#" + sugHiddenInputID).val(JSON.stringify(item));
			
	}); // end each function
	
	// TERMS
    $("#suggestionList").append("<h3 style=\"margin-top: 10px;\">Vocabularies Alignment</h3>");
    jQuery.each(json.vocabsjsonarray, function(i, vocab) {
      $("#suggestionList").append("<h4>About ... " + vocab.domain + "</h4>");
      jQuery.each(vocab.termsjsonarray, function(j, item) {
		
		var nextIndex = getNextIndex();
		var sugHiddenInputID = "sugHiddenInput" + 	nextIndex;
				
		var tableStr = 
			"<table class=\"viewMappingTable\">" +
				"<input type=\"hidden\" id=\"" + sugHiddenInputID + "\"/>" + 
				"<tr>" +
					"<td class=\"actionArea\">" +
						"<img src=\"images/delete.icon.png\" class=\"rejectSuggestedMappingImg\">" +
					"</td>" +
					"<td class=\"correspondenceArea\">" +
						"<table class=\"correspondenceTable\">" +
							"<tr>" +
								"<td class=\"viewEntity1\">" + jsonDataToHtml(item.entity1) + "</td>" +
								"<td class=\"viewRelation\">" +
									"<img id=\"TermConfidenceID" + nextIndex + "\" src='images/blueinfo.png' class='blueinfoCls'>" +
									"<select name=\"relation\" id=\"relationSelection\">" +
										"<option>Equivalent</option>" +
										"<option>Broader Meaning</option>" +
										"<option>Narrower Meaning</option>" +
										"<option>Partially Overlapping</option>" +
									"</select>" + 
								"</td>" +
								"<td class=\"viewEntity2\">" + jsonDataToHtml(item.entity2) + "</td>" +
							"</tr>" +
						"</table>" +
					"</td>" +
					"<td class=\"actionArea\">" +
						"<img src=\"images/accept.icon.png\" class=\"acceptSuggestedMappingImg\">" +
					"</td>" +
				"</tr>" +
			"</table>";
		
		$("#suggestionList").append(tableStr);
		
		if (typeof(item.simcomments) != "undefined") {
			$('#TermConfidenceID' + nextIndex).balloon({
				position: "left",
				offsetY: -10,
				contents: "<div class='balloonRootDivCls'>" + item.simcomments + "</div>",
				showDuration: "slow",
				showAnimation: function(d) { this.fadeIn(d); },
				hideDuration: 0
			});
		}
		
		// Store item json...
		$("#" + sugHiddenInputID).val(JSON.stringify(item));
			
	}) }); // end each function
    

	
	handleCorrespondencesLinks();
	
	// Accepting...		
	$(".acceptSuggestedMappingImg")
		.click(function(){
			var jsonStr = $("#suggestionsHiddenInput").val();
			var json = $.parseJSON(jsonStr);
            var selectRoot = $(this).parent().parent().parent().parent();
		    var selectedJsonStr = selectRoot.find("input[type=hidden]").val();
		    var selectedJson = $.parseJSON(selectedJsonStr);

            var relationStr = selectRoot.find("#relationSelection").val();
            selectedJson.relation = relationStr;

            $(this).parent().parent().attr("style", "background-color: #BCF5A9;");

		    var corJsonStr = $("#correspondencesHiddenInput").val();
		    var corJson = $.parseJSON(corJsonStr);

		    // add selected in correspondences
		    corJson.jsonarray.push(selectedJson);
		    $("#correspondencesHiddenInput").val(JSON.stringify(corJson));

		    updateCorrespondences();

		    // remove selected from suggestions
			jQuery.each(json.vocabsjsonarray, function(i, vocab) {
				vocab.termsjsonarray = jQuery.grep(vocab.termsjsonarray, function(item, i) {
	    			if ( JSON.stringify(item) == selectedJsonStr) {
	    				return false;
	    			}
	    			return true;
	    		});
			});
			json.vocabsjsonarray = jQuery.grep(json.vocabsjsonarray, function(vocab, i) {
				if ( vocab.termsjsonarray.length == 0) {
    				return false;
    			}
    			return true;
    		});
	   		$("#suggestionsHiddenInput").val(JSON.stringify(json));

			updateSuggestions();
		});
	$(".acceptSuggestedMappingImg")	
		.hover(
			function () {
                $(this).parent().attr("style", "background-color: #BCF5A9;");
				$(this).parent().parent().attr("style", "background-color: #BCF5A9;");
				$(this).css("cursor", "hand");
			}, 
			function () {
				$(this).parent().removeAttr("style");
                $(this).parent().parent().removeAttr("style");
				$(this).css("cursor", "pointer");
			}
		);

	// Further Processing
	$(".optionsSuggestedMappingImg")
		.click(function(){
			var jsonStr = $("#suggestionsHiddenInput").val();
			var json = $.parseJSON(jsonStr);
            var selectRoot = $(this).parent().parent().parent().parent();
		    var selectedJsonStr = selectRoot.find("input[type=hidden]").val();
		    var selectedJson = $.parseJSON(selectedJsonStr);
			
			// Prepare Dialog with possible Scenarios
			$("#mappingScenarioSuggestionSelection").empty();
			var divData = "";
			jQuery.each(selectedJson.scenariosja, function(j, mapscen) {
				divData += 
					"<li><img src=\"images/infoButton.jpg\" title=\"" + mapscen.desc + "\">" + 
					"<a href=\"" + mapscen.uri + "\">" +  mapscen.name + "</a></li>"							
			});
			$("#mappingScenarioSuggestionSelection").html("<ul>" + divData + "</ul>");
			
			// present available mapping scenarios
			$("#mappingScenarioSuggestionSelection").dialog("open");
			
			// depending on the mapping scenario selected
			$("#mappingScenarioSuggestionSelection a").unbind('click').click(function( event ) {
				var uri = $(this).attr("href");
				var desc = $(this).parent().find("img").attr("title");
				
				resetCreateMappingPanel();
				handleMappingScenarioSelected(uri, desc);
				
				// prepare mapping scenario instance
				alert("Will be moved to another tab");
				
				// Prepare Entity 1
				$("#propDiv0 > .entityAdatatypePropertyInput").val( selectedJson.entity1.propertyuri );
				$("#propDiv0 > .entityAdatatypePropertyInput").attr("title", selectedJson.entity1.propertyname);
				$("#propDiv0 .selectedParameterValue").html( selectedJson.entity1.propertyname );
				
				// Prepare Transformation Attributes
				$("#transfdiv1 .argValueInput0").val( selectedJson.entity2.classuri );
				$("#transfdiv1 .argValueInput0").attr("title", selectedJson.entity2.classname);
				
				// move to the other tab
				$( "#tabsM" ).tabs( "option", "active", 2 );
				
				// remove selected from suggestions
				json.fieldsjsonarray = jQuery.grep(json.fieldsjsonarray, function(item, i) {
					if ( JSON.stringify(item) == selectedJsonStr) {
						return false;
					}
					return true;
				});
				$("#suggestionsHiddenInput").val(JSON.stringify(json));

				updateSuggestions();
				
				$("#mappingScenarioSuggestionSelection").dialog("close");
				event.preventDefault();
			});	
			
		});
	$(".optionsSuggestedMappingImg")	
		.hover(
			function () {
                $(this).parent().attr("style", "background-color: #c2d6d6;");
				$(this).parent().parent().attr("style", "background-color: #c2d6d6;");
				$(this).css("cursor", "hand");
			}, 
			function () {
				$(this).parent().removeAttr("style");
                $(this).parent().parent().removeAttr("style");
				$(this).css("cursor", "pointer");
			}
		);
		
	// Rejecting ...			
	$(".rejectSuggestedMappingImg")
		.click(function(){
			var jsonStr = $("#suggestionsHiddenInput").val();
			var json = $.parseJSON(jsonStr);
            var selectRoot = $(this).parent().parent().parent().parent();
			var selectedJsonStr = selectRoot.find("input[type=hidden]").val();
            var selectedJson = $.parseJSON(selectedJsonStr);

            $(this).parent().parent().attr("style", "background-color: #FAAC58;");

            // Take down rejected
            var rejectJsonStr = $("#rejectionsHiddenInput").val();
            var rejectJson = $.parseJSON(rejectJsonStr);
            rejectJson.jsonarray.push(selectedJson);
            $("#rejectionsHiddenInput").val(JSON.stringify(rejectJson));

			// Remove selected
			jQuery.each(json.vocabsjsonarray, function(i, vocab) {
				vocab.termsjsonarray = jQuery.grep(vocab.termsjsonarray, function(item, i) {
	    			if ( JSON.stringify(item) == selectedJsonStr) {
	    				return false;
	    			}
	    			return true;
	    		});
			});
			json.vocabsjsonarray = jQuery.grep(json.vocabsjsonarray, function(vocab, i) {
				if ( vocab.termsjsonarray.length == 0) {
    				return false;
    			}
    			return true;
    		});
			json.fieldsjsonarray = jQuery.grep(json.fieldsjsonarray, function(item, i) {
				if ( JSON.stringify(item) == selectedJsonStr) {
	    			return false;
	    		}
	    		return true;
	    	});

	   		$("#suggestionsHiddenInput").val(JSON.stringify(json));

			updateSuggestions();
		});
	$(".rejectSuggestedMappingImg")	
		.hover(
			function () {
				$(this).parent().attr("style", "background-color: #FAAC58;");
                $(this).parent().parent().attr("style", "background-color: #FAAC58;");
				$(this).css("cursor", "hand");
			}, 
			function () {
				$(this).parent().removeAttr("style");
                $(this).parent().parent().removeAttr("style");
				$(this).css("cursor", "pointer");
			}
		);
	
}

/**
 * This method attaches action/click listeners to the buttons presented at the bottom of suggested
 * correspondences tab. Using these buttons the end user can massively accept or reject the 
 * suggested correspondences. 
 */
function handleSuggestionActions() {
	
	$('#suggestedActionFilter').fadeTo('fast', 0.5);
	$('#suggestedActionFilter input').attr('disabled', 'disabled');
    $("#suggestedActionFilter").hide();
    $("#rejectAllSuggestCorrespButton").hide();
    $("#acceptAllSuggestCorrespButton").hide();

	$("#suggestEnableFilter")
		.click(function(){
			if ($(this).attr("checked") == "checked"){
				$('#suggestedActionFilter').fadeTo('fast', 1.0);
				$('#suggestedActionFilter input').removeAttr('disabled');
				$("#rejectAllSuggestCorrespButton").attr("title", "Reject all with confidence value below the one specified");
				$("#acceptAllSuggestCorrespButton").attr("title", "Accept all with confidence value greater than or equals the one specified");
                $("#suggestedActionFilter").show();
                $("#rejectAllSuggestCorrespButton").show();
                $("#acceptAllSuggestCorrespButton").show();
			} else {
				$('#suggestedActionFilter').fadeTo('fast', 0.5);
				$('#suggestedActionFilter input').attr('disabled', 'disabled'); 
				$("#rejectAllSuggestCorrespButton").removeAttr('title');
				$("#acceptAllSuggestCorrespButton").removeAttr('title');
                $("#suggestedActionFilter").hide();
                $("#rejectAllSuggestCorrespButton").hide();
                $("#acceptAllSuggestCorrespButton").hide();
			}
		});
	
	$("#rejectAllSuggestCorrespButton")
		.button()
	    .click(function( event ) {
	    	var jsonStr = $("#suggestionsHiddenInput").val();
	    	var json = $.parseJSON(jsonStr);
            var rejectJsonStr = $("#rejectionsHiddenInput").val();
            var rejectJson = $.parseJSON(rejectJsonStr);
	    	// Update JSON
	    	if ($("#suggestEnableFilter").attr("checked") == "checked"){
	    		var limit = $("#downConfidenceLimit").val();
	    		var limitFloat = parseFloat(limit);
	    		if (typeof(json.jsonarray) != "undefined") {
	    			json.jsonarray = jQuery.grep(json.jsonarray, function(item, i) {
	    				var similarityFloat = parseFloat(item.similarity);
	    				var flag = (similarityFloat < limitFloat);
                        if (flag) {
                            rejectJson.jsonarray.push(item);
                        }
	    				return !flag;
	    			});
	    		}
	    	} else {
	    		json = {};
	    	}
	    	// Update hidden field
            $("#rejectionsHiddenInput").val(JSON.stringify(rejectJson));
	    	$("#suggestionsHiddenInput").val(JSON.stringify(json));
			updateSuggestions();
	    
			event.preventDefault();
	    });
	
	$("#acceptAllSuggestCorrespButton")
		.button()
	    .click(function( event ) {
	    	var jsonStr = $("#suggestionsHiddenInput").val();
	    	var json = $.parseJSON(jsonStr);
	    	var corJsonStr = $("#correspondencesHiddenInput").val();
		    var corJson = $.parseJSON(corJsonStr);
	    	// Update JSONs
	    	if ($("#suggestEnableFilter").attr("checked") == "checked"){
	    		var limit = $("#downConfidenceLimit").val();
	    		var limitFloat = parseFloat(limit);
	    		if (typeof(json.jsonarray) != "undefined") {
	    			json.jsonarray = jQuery.grep(json.jsonarray, function(item, i) {
	    				var similarityFloat = parseFloat(item.similarity);
	    				var flag = (similarityFloat >= limitFloat);
	    				if (flag) {
	    					corJson.jsonarray.push(item);
	    				}
	    				return !flag;
	    			});
	    		}
	    	} else {
	    		jQuery.each(json.jsonarray, function(i, item) {
	    			corJson.jsonarray.push(item);
	    		});
	    		json = {};
	    	}
			// Update hidden fields
			$("#correspondencesHiddenInput").val(JSON.stringify(corJson));
			updateCorrespondences();
	    	$("#suggestionsHiddenInput").val(JSON.stringify(json));
			updateSuggestions();
			
			event.preventDefault();
	    });
	    
}


// --------------------------------- MAPPING SCENARIOS --------------------------------- 

function loadMappingScenarios() {
	$.post("MappingScenariosServlet", { method: "GET-ALL-MAP-SCENARIOS" }, function(json){
		if (typeof(json.error) != "undefined")  {
			alert("Unable to LOAD mapping scenarios: " + json.error);
			return;
		}
		$("#scenarioaccordion").empty();
		jQuery.each(json.categJA, function(i, categ) {
			var divData = "";
			jQuery.each(categ.mapscenJA, function(j, mapscen) {
				divData += 
					"<li><img src=\"images/infoButton.jpg\" title=\"" + mapscen.desc + "\">" + 
					"<a href=\"" + mapscen.uri + "\">" +  mapscen.name + "</a></li>"							
			});
			$("#scenarioaccordion").append("<h3>" + categ.categ + "</h3><div><ul>" + divData + "</ul></div>");
		});
	}, "json" );  // end post function 
}

function handleMappingScenarioButton() {
	$( "#mapscenariobutton" )
	.button()
    .click(function( event ) {
      	$("#mappingScenarioSelection").dialog("open");

		$( "#scenarioaccordion" ).accordion({
			event: "click hoverintent",
			heightStyle: "fill"
		});
					
		$( "#accordion-resizer" ).resizable({
			minHeight: 140,
			minWidth: 200,
			resize: function() {
				$( "#scenarioaccordion" ).accordion( "refresh" );
			}
		});
		
		$("#scenarioaccordion a").unbind('click').click(function( event ) {
			var uri = $(this).attr("href");
			var desc = $(this).parent().find("img").attr("title");
			
			handleMappingScenarioSelected(uri, desc);
			$("#mappingScenarioSelection").dialog("close");
			event.preventDefault();
		});	
		
        event.preventDefault();
    });
	
	$("#mappingScenarioSelection").dialog({
		title: "Mapping Scenario Selection",
		autoOpen: false,
		width: 880,
		height: 610
	});	
}

function handleMappingScenarioSelected(scenuri, desc) {
	
	$("#mapscenariodiv")
		.empty()
		.html("<p>" + desc + "</p>");
	
	$.post("MappingScenariosServlet", { method: "GET-MAP-SCENARIO-DETAILS", uri: scenuri }, function(json){
		if (typeof(json.error) != "undefined")  {
			alert("Unable to LOAD mapping scenarios: " + json.error);
			return;
		}

		// Entity 1 Input Fields
		var entity1divData = "<input type=\"hidden\" name=\"PropertiesCollectionPattern\"/>";	
		jQuery.each(json.entity1.ja, function(i, item) {
			var nextIndex1 = getNextIndex();
			var dataPropInputId = "datatypePropertyInput" + nextIndex1;
			var copyButtonId = "dataPropCopyButton" + nextIndex1;
			entity1divData +=
				"<div id=\"propDiv" + i + "\">" + 
					"<input type=\"hidden\" name=\"SimplePropertyPattern\"/>" +
					"<img class='eleImage' src='images/DatatypeProperty.gif'>" +
					"<input class=\"entityAdatatypePropertyInput\" type=\"text\" id=\"" + dataPropInputId + "\">"  +
					"<img class='copyButton' src=\"images/copyButton.png\" id=\"" + copyButtonId + "\" title=\"Copy selected property\">" +
					"<span class=\"inputFieldLabel\"> ( " + item.name + " )</span> " +
					"<span class=\"selectedParameterValue\"></span>" +
				"</div>"
		});	
		$("#entity1div").html(entity1divData);
		
		var hiddenInputDiv = "#jsonOntologyFlatListsA";
		var ontjsonStr = $(hiddenInputDiv).val();
		var ontjson = $.parseJSON(ontjsonStr);
		if( typeof(ontjson.datapropja) != "undefined" ) {
			$(".entityAdatatypePropertyInput").autocomplete({
            	source: ontjson.datapropja,
            	select: function( event, ui ) {
            		var selectedUri = ui.item.uri;
            		// Input Text
                	$(this).val( selectedUri );
                	// Input Title
					$(this).attr("title", ui.item.value);
					// Text
					$(this).parent().find(".selectedParameterValue").html( ui.item.value );
                	return false;
            	}
        	}).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.value + "</a>")
                    .appendTo(ul);
            };
		}
			
		$(".copyButton").bind('click', function ( event ) {
			var entityCharIndex = "A";
			var selectedElementStr = $.cookie('SelectedDataPropOnt' + entityCharIndex);
			var propInputField = $(this).parent().find(".entityAdatatypePropertyInput");
			var existingUri = propInputField.val();
			if (selectedElementStr != null) {
                var selectedElement = $.parseJSON(selectedElementStr);
				// Input Title
				propInputField.attr("title", selectedElement.name);
				// Input Text
				if (existingUri == "" ) {
					propInputField
						.hide()
						.val( selectedElement.uri )
						.fadeIn(500);
				} else {
					if ( selectedElement.uri != existingUri) {
						propInputField
							.fadeOut(500)
							.val( selectedElement.uri )
							.fadeIn(500);
					}
				}
				// Text
				$(this).parent().find(".selectedParameterValue").html( selectedElement.name );
			} else {
				$("#infodialog").empty();
				$("#infodialog").html("<p>None owl DatatypeProperty from Ontology " + entityCharIndex + " selected...</p><p>Input text has not changed !</p>");
				$("#infodialog").dialog("open");
			}
		});

		// Entity 2
		var nextIndex2 = getNextIndex();
		var clsDivId = "clsDivId" + nextIndex2;
		var classInputId = "classInput" + nextIndex2;
		var addOtherClassDivId = "addPropDivId" + nextIndex2;	
		var entity2divData = 		
			"<input type=\"hidden\" name=\"ClassWithPropertiesRestrictionPattern\"/>" +
			"<p><div id=\"" + clsDivId + "\">" + 
				"<input type=\"hidden\" name=\"SimpleClassPattern\"/>" +
				"<img class='eleImage' src='images/Class.gif'>" +
				"<input  class=\"csshidden\" type=\"text\" id=\""+ classInputId + "\" title=\"" + json.entity2.label + "\" value=\"" + json.entity2.uri + "\">" +
				"<span class=\"inputFieldLabel\"><a href=\"" + json.entity2.uri + "\">" + json.entity2.label + "</a></span>" +
			"</div></p>" +
			"<p><span class=\"mappingElementType\">Parameters: </span></p>" +
			"<div id=\"" + addOtherClassDivId + "\">" + "<div>";
		jQuery.each(json.entity2.ja, function(i, item) {
			var nextIndex3 = getNextIndex();
			var propDivId = "propDivId" + nextIndex3 + "-" + nextIndex3;
			var propInputId = "objectPropertyInput" + nextIndex3;
			var img = "ObjectProperty.gif";
			var pat = "SimpleRelationPattern";
			if (item.proptype == "DP") {
				img = "DatatypeProperty.gif";
				pat = "SimplePropertyPattern";
				propInputId = "datatypePropertyInput" + nextIndex3;
			}
			entity2divData +=
				"<div id=\"" + propDivId + "\">" +
					"<input type=\"hidden\" name=\"" + pat + "\"/>" +
					"<img class='eleImage' src='images/" + img + "'>" +
					"<input class=\"csshidden\" type=\"text\" id=\"" + propInputId + "\" title=\"" + item.label + "\" value=\"" + item.uri + "\">" +
					"<span class=\"inputFieldLabel\"><a href=\"" + item.uri + "\">" + item.label + "</a>" + "</span>" +
				"</div>";
		});
		entity2divData += "</div></div><p></p>";
		$("#entity2div").html(entity2divData);	
		
		$("#entity2div a").click(function(evt) {
			var elementUri = $(this).attr('href');
			var jsonParamStr = $("#jsonOntologyB").val();
			presentDataInDialog(jsonParamStr, elementUri);
			return false;
		});
		
		// Data Transformation
		var buttonIndex = 1;
		var transDivId = "#transfdiv" + buttonIndex;
  		var transfInputId = "tranformationInputId" + buttonIndex;
  		var addArgDiv = "argsdiv" + buttonIndex;
  		$(transDivId).html(
  			"<input type=\"hidden\" name=\"ValuesTransformation\"/>" + 
  			"<p><span class=\"mappingElementUriTag\"> URI: </span><span id=\"transUri\"></span>" +
  			"<input class=\"csshidden\" type=\"text\" id=\"" + transfInputId + "\" style=\"width: 90%; margin-right: 5px;\"></p>" +
			"<div id=\"transDesc\" rows=\"3\" style=\"width: 90%; margin-left: 5px; font-size: 0.7em;\"></div>" +
  			"<div id=\"" + addArgDiv + "\"></div>" 
  		);
		
  		// Introduce the appropriate input fields taking into account the data type
		$("#" + transfInputId).val(json.datatrans.uri);
		$("#transUri").html(json.datatrans.uri);
		$("#transDesc").html(json.datatrans.desc);
		$("#" + addArgDiv).empty();
		jQuery.each(json.datatrans.ja, function(i, item) {
			var optional = "NO";
			var note = "";
			if (typeof(item.canBeEmpty) != "undefined") {
				optional = "YES";
				note = " (optional) ";
			}
			$("#" + addArgDiv).append(
				"<div style=\"margin-left:5px;\" optional=\"" + optional + "\">" + 
				"<span class=\"transArgName\">" + item.name + ":</span>" + 
				"<input type=\"text\" class=\"csshidden\" id=\"argname\" value=\"" + item.name + "\"style=\"width:150px;\" readonly> " + 
				"<span class=\"clsOptional\">" + note + "</span>" + 
				"<input type=\"text\" id=\"argvalue\" class=\"argValueInput" + i + "\" style=\"width:100px;\">" +
				"</div>"
			);
			// Number
			if (item.datatype == "REAL-NUMBER") {
				$(".argValueInput" + i).prop("type", "number");
			}
			// Auto complete
			if (item.datatype == "CLASS") {
				$(".argValueInput" + i).autocomplete({
					source: item.classja,
					select: function( event, ui ) {
						var selectedUri = ui.item.uri;
						// Input Text
						$(this).val( selectedUri );
						// Input Title
						$(this).attr("title", ui.item.value);
						// Text
						$(this).parent().find(".selectedParameterValue").html( ui.item.value );
						return false;
					}
				}).data("ui-autocomplete")._renderItem = function (ul, item) {
					return $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.value + "</a>")
						.appendTo(ul);
				};
			}
			// Dropdown List
			if (typeof(item.optionsja) != "undefined") {
				var divelem = $(".argValueInput" + i).parent();
				$(".argValueInput" + i).remove();
				divelem.append("<select id=\"argvalue\" type=\"text\" class=\"argValueInput" + i + "\" style=\"width:100px;\"></select>");
				$(".argValueInput" + i).append('<option></option>');
				jQuery.each(item.optionsja, function(j, opt) {
					$(".argValueInput" + i).append("<option>" + opt + "</option>");
				});
			}
		});
				
	}, "json" );  // end post function 		
	
}

// --------------------------------- ENTITIES --------------------------------- 

/**
 * This method prompts the user to select the desirable pattern and accordingly updates the main screen,
 * depending on the Entity called... provided as a parameter in this method.
 * 
 * @param entityIndex
 * 		A number (1 or 2) indicating the Entity.
 * 
 * @see handlePatternSelected
 * 		This method is being used to handle the pattern selected and properly update the screen.
 */
function handleEntityButton(entityIndex) {
	$( "#entity" + entityIndex + "button" )
		.button()
	    .click(function( event ) {
	      	$("#patternSelection").dialog("open");
	        	
	       	$( "#accordion" ).accordion({
	        	event: "click hoverintent",
	            heightStyle: "fill"
	        });
	            
	        $( "#accordion-resizer" ).resizable({
	            minHeight: 140,
	            minWidth: 200,
	            resize: function() {
	                $( "#accordion" ).accordion( "refresh" );
	            }
	        });
	        
	        $("#accordion a").unbind('click').click(function( event ) {
		   		var uri = $(this).attr("href");
		   		handlePatternSelected("#entity" + entityIndex + "div", "#patternSelection", uri, entityIndex);
		   		$("#patternSelection").dialog("close");
		   		event.preventDefault();
			});
	        	
	        event.preventDefault();
	    });
    	
	$("#patternSelection").dialog({
		title: "Pattern Selection",
		autoOpen: false,
		width: 450,
		height: 350
	});
}

/**
 * This method enables the user to select the desirable pattern and accordingly update the main screen.
 * 
 * @param pageDivId
 * 		The ID of an element in which the selected pattern will be placed
 * @param patternUri
 * 		The URI of the pattern selected
 * @param entityIndex
 * 		A number (1 or 2) indicating the Entity called this method.
 * 
 * @param dialogDivId ???
 * 
 */
function handlePatternSelected(pageDivId, dialogDivId, patternUri, entityIndex) {
	// Find Pattern ID
    var startIndex = patternUri.indexOf("#") + 1;
	var endIndex = patternUri.length;
	var patternId = patternUri.substring(startIndex, endIndex);

	$(pageDivId).empty();

	$(pageDivId).css("padding-left", "40px");

	// *** Update DIV based on Pattern ID ***

	// ------------------------------------------------------------------------------
	// -- Class Patterns
	// ------------------------------------------------------------------------------

	if (patternId == "SimpleClassPattern") {
		var nextIndex = getNextIndex();
		var classInputId = "classInput" + nextIndex;
		var copyButtonId = "classCopyButton" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"SimpleClassPattern\"/>" +
            "<img class='eleImage' src='images/Class.gif'>" +
            "<input type=\"text\" id=\""+ classInputId + "\" >" +
			"<img class='copyButton' src=\"images/copyButton.png\" id=\"" + copyButtonId + "\" title=\"Place the URI of the owl Class selected.\">"
		);

		var hiddenInputDiv = "#jsonOntologyFlatListsA";
		if (entityIndex == "2") {
			hiddenInputDiv = "#jsonOntologyFlatListsB";
		}
		var jsonStr = $(hiddenInputDiv).val();
		var json = $.parseJSON(jsonStr);
		if( typeof(json.classja) != "undefined" ) {
			$("#" + classInputId).autocomplete({
            	source: json.classja,
            	select: function( event, ui ) {
            		var selectedUri = ui.item.uri;
            		// Input Text
                	$("#" + classInputId).val( selectedUri );
                	// Input Title
					$("#" + classInputId).attr("title", ui.item.value);
                	return false;
            	}
        	}).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.value + " ( <span class='text06em'>" + item.uri + "</span> )" + "</a>")
                    .appendTo(ul);
            };
		}

		$("#" + copyButtonId)/*.unbind('click')*/.bind('click', function ( event ) {
			var entityCharIndex = "A";
			if (entityIndex == "2") {
				entityCharIndex = "B";
			}
			var selectedElementStr = $.cookie('SelectedClassOnt' + entityCharIndex);
			var existingUri = $("#" + classInputId).val();
			if (selectedElementStr != null) {
                var selectedElement = $.parseJSON(selectedElementStr);
                $("#" + classInputId).attr("title", selectedElement.name);
				// Input Text
				if (existingUri == "" ) {
					$("#" + classInputId)
						.hide()
						.val( selectedElement.uri )
						.fadeIn(500);
				} else {
					if ( selectedElement.uri != existingUri) {
						$("#" + classInputId)
							.fadeOut(500)
							.val( selectedElement.uri )
							.fadeIn(500);
					}
				}
			} else {
				$("#infodialog").empty();
				$("#infodialog").html("<p>None owl Class from Ontology " + entityCharIndex + " selected...</p><p>Input text has not changed !</p>");
				$("#infodialog").dialog("open");
			}
		});

	}
	else if (patternId == "ClassUnionPattern" || patternId == "ClassIntersectionPattern") {
		var patternInstanceLabel = "Class Union Pattern - Instance:";
		var hiddenInputElement = "<input type=\"hidden\" name=\"ClassUnionPattern\"/>";
		if (patternId == "ClassIntersectionPattern") {
			patternInstanceLabel = "Class Intersection Pattern - Instance:";
			hiddenInputElement = "<input type=\"hidden\" name=\"ClassIntersectionPattern\"/>";
		}

		var nextIndex = getNextIndex();
		var cls1DivId = "cls1DivId" + nextIndex;
		var cls1ButtonId = "cls1ButtonId" + nextIndex;
		var cls2DivId = "cls2DivId" + nextIndex;
		var cls2ButtonId = "cls2ButtonId" + nextIndex;
		var addOtherClassDivId = "addClassDivId" + nextIndex;
		var addClsButtonId = "addClsButtonId" + nextIndex;

		$(pageDivId).html(
			hiddenInputElement +
			"<p><span class=\"patternInstanceType\">" + patternInstanceLabel + "</span> <button id=\"" + addClsButtonId + "\">Add ...</button></p>" +
			"<p><span class=\"mappingElementType\">Class:</span><div id=\"" + cls1DivId + "\"><button id=\"" + cls1ButtonId + "\">Select Class Pattern</button></div></p>" +
			"<p><span class=\"mappingElementType\">Class:</span><div id=\"" + cls2DivId + "\"><button id=\"" + cls2ButtonId + "\">Select Class Pattern</button></div></p>" +
			"<div id=\"" + addOtherClassDivId + "\"></div>"
		);

	   	$("#" + cls1ButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#class-pattern-dialog").dialog("open");
	   			$("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + cls1DivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#class-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

		$("#" + cls2ButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#class-pattern-dialog").dialog("open");
	   			$("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + cls2DivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#class-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

		$( "#" + addClsButtonId)
			.button({
				icons: {
					primary: "ui-icon-plus"
				}, text: false
			})
		    .click(function( event ) {
		    	var nextIndex = getNextIndex();
		    	var clsDivId = "clsDivId" + nextIndex;
				var clsButtonId = "clsButtonId" + nextIndex;
		    	$("#" + addOtherClassDivId).append(
		    		"<div><span class=\"mappingElementType\">Class:</span> <button class=\"clsRemoveButton\" style=\"margin-left:5px;\"></button> <div id=\"" + clsDivId + "\"><button id=\"" + clsButtonId + "\">Select Class Pattern</button></div></div>"
		    	);

		    	$("#" + clsButtonId )
	   				.button()
	   				.click(function( event ) {
	   					$("#class-pattern-dialog").dialog("open");
	   					$("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   						handlePatternSelected( "#" + clsDivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
	   						$("#class-pattern-dialog").dialog("close");
	   						event.preventDefault();
                		});
                		event.preventDefault();
        			});

		    	$( ".clsRemoveButton" )
					.button({
				        icons: {
				            primary: "ui-icon-closethick"
				        }, text: false
				    })
				    .click(function( event ) {
		    			$(this).parent().remove();
				    	event.preventDefault();
				    });
		    	event.preventDefault();
		    });
	}
	else if (patternId == "ClassByAttributeValueRestrictionPattern" || patternId == "ClassByAttributeOccurencePattern") {
		var patternInstanceLabel = "Class By Attribute Value Restriction Pattern - Instance:";
		var hiddenInputElement = "<input type=\"hidden\" name=\"ClassByAttributeValueRestrictionPattern\"/>";
		if (patternId == "ClassByAttributeOccurencePattern") {
			patternInstanceLabel = "Class By Attribute Occurence Pattern - Instance:";
			hiddenInputElement = "<input type=\"hidden\" name=\"ClassByAttributeOccurencePattern\"/>";
		}

		var nextIndex = getNextIndex();
		var clsDivId = "clsDivId" + nextIndex;
		var clsButtonId = "clsButtonId" + nextIndex;
		var propDivId = "propDivId" + nextIndex;
		var probButtonId = "probButtonId" + nextIndex;
		var compInputId = "compInputId" + nextIndex;
		var compTextAreaId = "compTextAreaId" + nextIndex;
		var valueInputId = "valInputId" + nextIndex;

		$(pageDivId).html(
			hiddenInputElement +
			"<p><span class=\"patternInstanceType\">" + patternInstanceLabel + "</span></p>" +
			"<p><span class=\"mappingElementType\">Class:</span><div id=\"" + clsDivId + "\"><button id=\"" + clsButtonId + "\">Select Class Pattern</button></div></p>" +
			"<p><span class=\"mappingElementType\">Property:</span><div id=\"" + propDivId + "\"><button id=\"" + probButtonId + "\">Select Property Pattern</button></div></p>" +
			"<p><span class=\"mappingElementType\">Restriction:</span></p>" +
  			"<p style=\"padding-left:50px;\"><span class=\"mappingElementUriTag\">Comparator-URI: </span></p>" +
  			"<p style=\"padding-left:50px;\"><input type=\"text\" id=\"" + compInputId + "\" style=\"width: 80%; margin-right: 5px;\"></p>" +
  			"<p style=\"padding-left:50px;\"><span class=\"mappingElementUriTag\">Description: </span></p>" +
  			"<p style=\"padding-left:50px;\"><textarea id=\"" + compTextAreaId + "\" rows=\"2\" style=\"width: 80%; margin-right: 5px;\"></textarea></p>" +
			"<p><span class=\"mappingElementType\">Value:</p><p style=\"padding-left:50px;\"></span><input type=\"text\" id=\""+ valueInputId + "\" ></p>"
		);

		$("#" + clsButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#class-pattern-dialog").dialog("open");
	   			$("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + clsDivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#class-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

		$("#" + probButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#property-pattern-dialog").dialog("open");
	   			$("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + propDivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#property-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

	}
	else if (patternId == "ClassWithPropertiesRestrictionPattern") {
		var patternInstanceLabel = "Class with a few Parameters Pattern - Instance:";
		var hiddenInputElement = "<input type=\"hidden\" name=\"ClassWithPropertiesRestrictionPattern\"/>";

		var nextIndex = getNextIndex();
		var clsDivId = "clsDivId" + nextIndex;
		var clsButtonId = "clsButtonId" + nextIndex;
		var propDivId = "propDivId" + nextIndex;
		var probButtonId = "probButtonId" + nextIndex;
		
		var cls1DivId = "prop1DivId" + nextIndex;
        var cls1ButtonId = "prop1ButtonId" + nextIndex;
        var cls2DivId = "prop2DivId" + nextIndex;
        var cls2ButtonId = "prop2ButtonId" + nextIndex;
        var addOtherClassDivId = "addPropDivId" + nextIndex;
        var addClsButton1Id = "addPropButtonId1" + nextIndex;
		var addClsButton2Id = "addPropButtonId2" + nextIndex;

		$(pageDivId).html(
			hiddenInputElement +
			"<p><span class=\"patternInstanceType\">" + patternInstanceLabel + "</span></p>" +
			"<p><span class=\"mappingElementType\">Class:</span><div id=\"" + clsDivId + "\"><button id=\"" + clsButtonId + "\">Select Class Pattern</button></div></p>" +
			"<p><span class=\"mappingElementType\">Parameters: </span>" + 
				"<img class='eleImage' src='images/DatatypeProperty.gif'><button id=\"" + addClsButton1Id + "\">Add ...</button>" +
				"<img class='eleImage' src='images/ObjectProperty.gif'><button id=\"" + addClsButton2Id + "\">Add ...</button>" + 
			"</p>" +
            "<div id=\"" + addOtherClassDivId + "\"></div>" +
			"<p></p>"
		);
		
		$("#" + clsButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#class-pattern-dialog").dialog("open");
	   			$("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + clsDivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#class-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

        $("#" + cls1ButtonId )
            .button()
            .click(function( event ) {
                $("#property-pattern-dialog").dialog("open");
                $("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
                    handlePatternSelected( "#" + cls1DivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
                    $("#property-pattern-dialog").dialog("close");
                    event.preventDefault();
                });
                event.preventDefault();
            });

        $("#" + cls2ButtonId )
            .button()
            .click(function( event ) {
                $("#property-pattern-dialog").dialog("open");
                $("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
                    handlePatternSelected( "#" + cls2DivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
                    $("#property-pattern-dialog").dialog("close");
                    event.preventDefault();
                });
                event.preventDefault();
            });

        $( "#" + addClsButton1Id)
            .button({
                icons: {
                    primary: "ui-icon-plus"
                }, text: false
            })
            .click(function( event ) {
                var nextIndex = getNextIndex();
                var clsDivId = "propDivId" + nextIndex;
                var clsButtonId = "propButtonId" + nextIndex;
                $("#" + addOtherClassDivId).append(
                        "<div><button class=\"clsRemoveButton\" style=\"margin-left:5px;\"></button> <div id=\"" + clsDivId + "\"><button id=\"" + clsButtonId + "\">Select Property Pattern</button></div></div>"
                );

                $("#" + clsButtonId )
                    .button()
                    .click(function( event ) {
                        $("#property-pattern-dialog").dialog("open");
                        $("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
                            handlePatternSelected( "#" + clsDivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
                            $("#property-pattern-dialog").dialog("close");
                            event.preventDefault();
                        });
                        event.preventDefault();
                    });

                $( ".clsRemoveButton" )
                    .button({
                        icons: {
                            primary: "ui-icon-closethick"
                        }, text: false
                    })
                    .click(function( event ) {
                        $(this).parent().remove();
                        event.preventDefault();
                    });
                event.preventDefault();
            });	

        $( "#" + addClsButton2Id)
            .button({
                icons: {
                    primary: "ui-icon-plus"
                }, text: false
            })
            .click(function( event ) {
                var nextIndex = getNextIndex();
                var clsDivId = "propDivId" + nextIndex;
                var clsButtonId = "propButtonId" + nextIndex;
                $("#" + addOtherClassDivId).append(
                        "<div><button class=\"clsRemoveButton\" style=\"margin-left:5px;\"></button><div id=\"" + clsDivId + "\"><button id=\"" + clsButtonId + "\">Select Relation Pattern</button></div></div>"
                );
                $("#" + clsButtonId )
                    .button()
                    .click(function( event ) {
                        $("#relation-pattern-dialog").dialog("open");
                        $("#relation-pattern-dialog" + " a").unbind('click').click(function( event ) {
                            handlePatternSelected( "#" + clsDivId, "#relation-pattern-dialog", $(this).attr("href"), entityIndex);
                            $("#relation-pattern-dialog").dialog("close");
                            event.preventDefault();
                        });
                        event.preventDefault();
                    });

                $( ".clsRemoveButton" )
                    .button({
                        icons: {
                            primary: "ui-icon-closethick"
                        }, text: false
                    })
                    .click(function( event ) {
                        $(this).parent().remove();
                        event.preventDefault();
                    });
                event.preventDefault();
            });	
			
	}
	
	// +++

	// ------------------------------------------------------------------------------
	// -- Relation Patterns
	// ------------------------------------------------------------------------------

	else if (patternId == "SimpleRelationPattern") {
		var nextIndex = getNextIndex();
		var objPropInputId = "objectPropertyInput" + nextIndex;
		var copyButtonId = "objPropCopyButton" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"SimpleRelationPattern\"/>" +
			//"<span class=\"patternInstanceType\">SR-P: </span>" +
			//"<span class=\"mappingElementUriTag\">Relation-URI: </span> " +
            "<img class='eleImage' src='images/ObjectProperty.gif'>" +
            "<input type=\"text\" id=\"" + objPropInputId + "\" >" +
			"<img class='copyButton' src=\"images/copyButton.png\" id=\"" + copyButtonId + "\" title=\"Place the URI of the owl ObjectProperty selected.\">"
		);

		var hiddenInputDiv = "#jsonOntologyFlatListsA";
		if (entityIndex == "2") {
			hiddenInputDiv = "#jsonOntologyFlatListsB";
		}
		var jsonStr = $(hiddenInputDiv).val();
		var json = $.parseJSON(jsonStr);
		if( typeof(json.objpropja) != "undefined" ) {
			$("#" + objPropInputId).autocomplete({
            	source: json.objpropja,
            	select: function( event, ui ) {
            		var selectedUri = ui.item.uri;
            		// Input Text
                	$("#" + objPropInputId).val( selectedUri );
                	// Input Title
					$("#" + objPropInputId).attr("title", ui.item.value);
                	return false;
            	}
        	}).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.value + " ( <span class='text06em'>" + item.uri + "</span> )" + "</a>")
                    .appendTo(ul);
            };
		}

		$("#" + copyButtonId)/*.unbind('click')*/.bind('click', function ( event ) {
			var entityCharIndex = "A";
			if (entityIndex == "2") {
				entityCharIndex = "B";
			}
			var selectedElementStr = $.cookie('SelectedObjPropOnt' + entityCharIndex);
			var existingUri = $("#" + objPropInputId).val();
			if (selectedElementStr != null) {
                var selectedElement = $.parseJSON(selectedElementStr);
				$("#" + objPropInputId).attr("title", selectedElement.name);
				// Input Text
				if (existingUri == "" ) {
					$("#" + objPropInputId)
						.hide()
						.val( selectedElement.uri )
						.fadeIn(500);
				} else {
					if ( selectedElement.uri != existingUri) {
						$("#" + objPropInputId)
							.fadeOut(500)
							.val( selectedElement.uri )
							.fadeIn(500);
					}
				}
			} else {
				$("#infodialog").empty();
				$("#infodialog").html("<p>None owl ObjectProperty from Ontology " + entityCharIndex + " selected...</p><p>Input text has not changed !</p>");
				$("#infodialog").dialog("open");
			}

		});

	}
	else if (patternId == "InverseRelationPattern") {
		var nextIndex = getNextIndex();
		var relDivId = "relDivId" + nextIndex;
		var relButtonId = "relButtonId" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"InverseRelationPattern\"/>" +
			"<p><span class=\"patternInstanceType\">Inverse Relation Pattern - Instance:</span></p>" +
			"<span class=\"mappingElementType\">Relation:</span><div class='inLineBlockCls' id=\"" + relDivId + "\"><button id=\"" + relButtonId + "\">Select Relation Pattern</button></div>"
		);

		$("#" + relButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#relation-pattern-dialog").dialog("open");
	   			$("#relation-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + relDivId, "#relation-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#relation-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});
	}
	else if (patternId == "RelationDomainRestrictionPattern") {
		var nextIndex = getNextIndex();
		var relDivId = "relDivId" + nextIndex;
		var relButtonId = "relButtonId" + nextIndex;
		var domClsDivId = "domClsDivId" + nextIndex;
		var domClsButtonId = "domClsButtonId" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"RelationDomainRestrictionPattern\"/>" +
			"<p><span class=\"patternInstanceType\">Relation Domain Restriction Pattern - Instance:</span></p>" +
			"<span class=\"mappingElementType\">Relation:</span><div class='inLineBlockCls' id=\"" + relDivId + "\"><button id=\"" + relButtonId + "\">Select Relation Pattern</button></div>" +
			"<br>" +
            "<span class=\"mappingElementType\">Domain-Class:</span><div class='inLineBlockCls' id=\"" + domClsDivId + "\"><button id=\"" + domClsButtonId + "\">Select Class Pattern</button></div>"
		);

		$("#" + relButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#relation-pattern-dialog").dialog("open");
	   			$("#relation-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + relDivId, "#relation-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#relation-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

	   	$("#" + domClsButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#class-pattern-dialog").dialog("open");
	   			$("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + domClsDivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#class-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});
	}
	else if (patternId == "RelationRangeRestrictionPattern") {
		var nextIndex = getNextIndex();
		var relDivId = "relDivId" + nextIndex;
		var relButtonId = "relButtonId" + nextIndex;
		var rangeClsDivId = "rangeClsDivId" + nextIndex;
		var rangeClsButtonId = "rangeClsButtonId" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"RelationRangeRestrictionPattern\"/>" +
			"<p><span class=\"patternInstanceType\">Relation Range Restriction Pattern - Instance:</span></p>" +
			"<span class=\"mappingElementType\">Relation:</span><div class='inLineBlockCls' id=\"" + relDivId + "\"><button id=\"" + relButtonId + "\">Select Relation Pattern</button></div>" +
            "<br>" +
			"<span class=\"mappingElementType\">Range-Class:</span><div class='inLineBlockCls' id=\"" + rangeClsDivId + "\"><button id=\"" + rangeClsButtonId + "\">Select Class Pattern</button></div>"
		);

		$("#" + relButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#relation-pattern-dialog").dialog("open");
	   			$("#relation-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + relDivId, "#relation-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#relation-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

	   	$("#" + rangeClsButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#class-pattern-dialog").dialog("open");
	   			$("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + rangeClsDivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#class-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});
	}

	else if (patternId == "RelationPathPattern") {
		var nextIndex = getNextIndex();
		var rel1DivId = "rel1DivId" + nextIndex;
		var rel1ButtonId = "rel1ButtonId" + nextIndex;
		var rel2DivId = "rel2DivId" + nextIndex;
		var rel2ButtonId = "rel2ButtonId" + nextIndex;
		var addOtherRelationDivId = "addRelationDivId" + nextIndex;
		var addRelationButtonId = "addClsRelationId" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"RelationPathPattern\"/>" +
			"<p><span class=\"patternInstanceType\">Relation Path Pattern - Instance:</span> <button id=\"" + addRelationButtonId + "\">Add...</button></p>" +
			"<div class='inLineBlockCls' id=\"" + rel1DivId + "\"><button id=\"" + rel1ButtonId + "\">Select Relation Pattern</button></div>" +
			"<div class='inLineBlockCls' id=\"" + rel2DivId + "\"><button id=\"" + rel2ButtonId + "\">Select Relation Pattern</button></div>" +
			"<div id=\"" + addOtherRelationDivId + "\"></div>"
		);

	   	$("#" + rel1ButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#relation-pattern-dialog").dialog("open");
	   			$("#relation-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + rel1DivId, "#relation-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#relation-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

		$("#" + rel2ButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#relation-pattern-dialog").dialog("open");
	   			$("#relation-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + rel2DivId, "#relation-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#relation-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

		$( "#" + addRelationButtonId)
			.button({
				icons: {
					primary: "ui-icon-plus"
				}, text: false
			})
		    .click(function( event ) {
		    	var nextIndex = getNextIndex();
		    	var relDivId = "relDivId" + nextIndex;
				var relButtonId = "relButtonId" + nextIndex;
		    	$("#" + addOtherRelationDivId).append(
		    		"<div><span class=\"mappingElementType\">Relation:</span> <button class=\"relRemoveButton\" style=\"margin-left:5px;\"></button> <div id=\"" + relDivId + "\"><button id=\"" + relButtonId + "\">Select Relation Pattern</button></div></div>"
		    	);

		    	$("#" + relButtonId )
	   				.button()
	   				.click(function( event ) {
	   					$("#relation-pattern-dialog").dialog("open");
	   					$("#relation-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   						handlePatternSelected( "#" + relDivId, "#relation-pattern-dialog", $(this).attr("href"), entityIndex);
	   						$("#relation-pattern-dialog").dialog("close");
	   						event.preventDefault();
                		});
                		event.preventDefault();
        			});

		    	$( ".relRemoveButton" )
					.button({
				        icons: {
				            primary: "ui-icon-closethick"
				        }, text: false
				    })
				    .click(function( event ) {
		    			$(this).parent().remove();
				    	event.preventDefault();
				    });
		    	event.preventDefault();
		    });
	}

    else if (patternId == "RelationDomainRangeRestrictionPattern") {
        var nextIndex = getNextIndex();
        var relDivId = "relDivId" + nextIndex;
        var relButtonId = "relButtonId" + nextIndex;
        var domClsDivId = "domClsDivId" + nextIndex;
        var domClsButtonId = "domClsButtonId" + nextIndex;
        var rangeClsDivId = "rangeClsDivId" + nextIndex;
        var rangeClsButtonId = "rangeClsButtonId" + nextIndex;

        $(pageDivId).html(
                "<input type=\"hidden\" name=\"RelationDomainRangeRestrictionPattern\"/>" +
                "<p><span class=\"patternInstanceType\">Relation Domain-Range Restriction Pattern - Instance:</span></p>" +
                "<span class=\"mappingElementType\">Relation:</span><div class='inLineBlockCls' id=\"" + relDivId + "\"><button id=\"" + relButtonId + "\">Select Relation Pattern</button></div>" +
                "<br>" +
                "<span class=\"mappingElementType\">Domain-Class:</span><div class='inLineBlockCls' id=\"" + domClsDivId + "\"><button id=\"" + domClsButtonId + "\">Select Class Pattern</button></div>" +
                "<br>" +
                "<span class=\"mappingElementType\">Range-Class:</span><div class='inLineBlockCls' id=\"" + rangeClsDivId + "\"><button id=\"" + rangeClsButtonId + "\">Select Class Pattern</button></div>"
        );

        $("#" + relButtonId )
            .button()
            .click(function( event ) {
                $("#relation-pattern-dialog").dialog("open");
                $("#relation-pattern-dialog" + " a").unbind('click').click(function( event ) {
                    handlePatternSelected( "#" + relDivId, "#relation-pattern-dialog", $(this).attr("href"), entityIndex);
                    $("#relation-pattern-dialog").dialog("close");
                    event.preventDefault();
                });
                event.preventDefault();
            });

        $("#" + domClsButtonId )
            .button()
            .click(function( event ) {
                $("#class-pattern-dialog").dialog("open");
                $("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
                    handlePatternSelected( "#" + domClsDivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
                    $("#class-pattern-dialog").dialog("close");
                    event.preventDefault();
                });
                event.preventDefault();
            });

        $("#" + rangeClsButtonId )
            .button()
            .click(function( event ) {
                $("#class-pattern-dialog").dialog("open");
                $("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
                    handlePatternSelected( "#" + rangeClsDivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
                    $("#class-pattern-dialog").dialog("close");
                    event.preventDefault();
                });
                event.preventDefault();
            });
    }

	// ------------------------------------------------------------------------------
	// -- Property Patterns
	// ------------------------------------------------------------------------------

	else if (patternId == "SimplePropertyPattern") {
		var nextIndex = getNextIndex();
		var dataPropInputId = "datatypePropertyInput" + nextIndex;
		var copyButtonId = "dataPropCopyButton" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"SimplePropertyPattern\"/>" +
            "<img class='eleImage' src='images/DatatypeProperty.gif'>" +
            "<input type=\"text\" id=\"" + dataPropInputId + "\" >"  +
			"<img class='copyButton' src=\"images/copyButton.png\" id=\"" + copyButtonId + "\" title=\"Place the URI of the owl DatatypeProperty selected.\">"
		);

		var hiddenInputDiv = "#jsonOntologyFlatListsA";
		if (entityIndex == "2") {
			hiddenInputDiv = "#jsonOntologyFlatListsB";
		}
		var jsonStr = $(hiddenInputDiv).val();
		var json = $.parseJSON(jsonStr);
		if( typeof(json.datapropja) != "undefined" ) {
			$("#" + dataPropInputId).autocomplete({
            	source: json.datapropja,
            	select: function( event, ui ) {
            		var selectedUri = ui.item.uri;
            		// Input Text
                	$("#" + dataPropInputId).val( selectedUri );
                	// Input Title
					$("#" + dataPropInputId).attr("title", ui.item.value);
                	return false;
            	}
        	}).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.value + " ( <span class='text06em'>" + item.uri + "</span> )" + "</a>")
                    .appendTo(ul);
            };
		}

		$("#" + copyButtonId)/*.unbind('click')*/.bind('click', function ( event ) {
			var entityCharIndex = "A";
			if (entityIndex == "2") {
				entityCharIndex = "B";
			}
			var selectedElementStr = $.cookie('SelectedDataPropOnt' + entityCharIndex);
			var existingUri = $("#" + dataPropInputId).val();
			if (selectedElementStr != null) {
                var selectedElement = $.parseJSON(selectedElementStr);
				// Input Title

				$("#" + dataPropInputId).attr("title", selectedElement.name);
				// Input Text
				if (existingUri == "" ) {
					$("#" + dataPropInputId)
						.hide()
						.val( selectedElement.uri )
						.fadeIn(500);
				} else {
					if ( selectedElement.uri != existingUri) {
						$("#" + dataPropInputId)
							.fadeOut(500)
							.val( selectedElement.uri )
							.fadeIn(500);
					}
				}
			} else {
				$("#infodialog").empty();
				$("#infodialog").html("<p>None owl DatatypeProperty from Ontology " + entityCharIndex + " selected...</p><p>Input text has not changed !</p>");
				$("#infodialog").dialog("open");
			}

		});
	}
	else if (patternId == "PropertyDomainRestrictionPattern") {
		var nextIndex = getNextIndex();
		var propDivId = "propDivId" + nextIndex;
		var probButtonId = "probButtonId" + nextIndex;
		var domClsDivId = "domClsDivId" + nextIndex;
		var domClsButtonId = "domClsButtonId" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"PropertyDomainRestrictionPattern\"/>" +
			"<p><span class=\"patternInstanceType\">Property Domain Restriction Pattern - Instance:</span></p>" +
			"<span class=\"mappingElementType\">Property:</span><div class='inLineBlockCls' ' id=\"" + propDivId + "\"><button id=\"" + probButtonId + "\">Select Property Pattern</button></div>" +
			"<br>" +
            "<span class=\"mappingElementType\">Domain-Class:</span><div class='inLineBlockCls' id=\"" + domClsDivId + "\"><button id=\"" + domClsButtonId + "\">Select Class Pattern</button></div>"
		);

		$("#" + probButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#property-pattern-dialog").dialog("open");
	   			$("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + propDivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#property-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

	   	$("#" + domClsButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#class-pattern-dialog").dialog("open");
	   			$("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + domClsDivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#class-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});
	}
	else if (patternId == "PropertyRangeRestrictionPattern") {
		var nextIndex = getNextIndex();
		var propDivId = "propDivId" + nextIndex;
		var probButtonId = "probButtonId" + nextIndex;
		var datatypeId = "datatypeId" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"PropertyRangeRestrictionPattern\"/>" +
			"<p><span class=\"patternInstanceType\">Property Range Restriction Pattern - Instance:</span></p>" +
			"<span class=\"mappingElementType\">Property:</span><div class='inLineBlockCls' id=\"" + propDivId + "\"><button id=\"" + probButtonId + "\">Select Property Pattern</button></div>" +
			"<br>" +
            "<span class=\"mappingElementType\">Range:</span><span class=\"mappingElementUriTag\">Datatype: </span><input id=\"" + datatypeId + "\" />"
		);

		var availableDatatypes = [
			"xsd:boolean",
			"xsd:byte",
			"xsd:short",
			"xsd:int",
            "xsd:integer",
            "xsd:positiveInteger",
            "xsd:negativeInteger",
            "xsd:long",
            "xsd:float",
            "xsd:double",
            "xsd:dateTime",
            "xsd:dateTimeStamp",
            "xsd:string",
            "xsd:normalizedString",
            "xsd:token",
            "xsd:language",
            "xsd:Name",
            "xsd:hexBinary",
            "xsd:base64Binary"
        ];
        $("#" +datatypeId).autocomplete({
            source: availableDatatypes
        });

		$("#" + probButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#property-pattern-dialog").dialog("open");
	   			$("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + propDivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#property-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});
	}
	else if (patternId == "PropertyValueRestrictionPattern") {
		var nextIndex = getNextIndex();
		var propDivId = "propDivId" + nextIndex;
		var probButtonId = "probButtonId" + nextIndex;
		var compInputId = "compInputId" + nextIndex;
		var compTextAreaId = "compTextAreaId" + nextIndex;
		var valueInputId = "valueInputId" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"PropertyValueRestrictionPattern\"/>" +
			"<p><span class=\"patternInstanceType\">Property Value Restriction Pattern - Instance:</span></p>" +
			"<p><span class=\"mappingElementType\">Property:</span><div id=\"" + propDivId + "\"><button id=\"" + probButtonId + "\">Select Property Pattern</button></div></p>" +
			"<p><span class=\"mappingElementType\">Restriction:</span></p>" +
  			"<p style=\"padding-left:50px;\"><span class=\"mappingElementUriTag\">Comparator-URI: </span></p>" +
  			"<p style=\"padding-left:50px;\"><input type=\"text\" id=\"" + compInputId + "\" style=\"width: 80%; margin-right: 5px;\"></p>" +
  			"<p style=\"padding-left:50px;\"><span class=\"mappingElementUriTag\">Description: </span></p>" +
  			"<p style=\"padding-left:50px;\"><textarea id=\"" + compTextAreaId + "\" rows=\"2\" style=\"width: 80%; margin-right: 5px;\"></textarea></p>" +
			"<p><span class=\"mappingElementType\">Value:</p><p style=\"padding-left:50px;\"></span><input type=\"text\" id=\""+ valueInputId + "\" ></p>"
		);

		$("#" + probButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#property-pattern-dialog").dialog("open");
	   			$("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + propDivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#property-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});



	}
	else if (patternId == "RelationPropertyPathPattern") {

        $(pageDivId).css("background-color", "#F2F2F2");
        $(pageDivId).css("border", "1px solid #F7BE81")
        $(pageDivId).css("border-radius", "5px");

		var nextIndex = getNextIndex();
		var relDivId = "relDivId" + nextIndex;
		var relButtonId = "relButtonId" + nextIndex;
		var propDivId = "propDivId" + nextIndex;
		var probButtonId = "probButtonId" + nextIndex;

        $(pageDivId).html(
                "<input type=\"hidden\" name=\"RelationPropertyPathPattern\"/>" +
                "<p><span class=\"patternInstanceType\">Relation Property Pattern - Instance:</span></p>" +
                    "<div style='display: inline-block !important;' id=\"" + relDivId + "\">" +
                        "<button id=\"" + relButtonId + "\">Select Relation Pattern</button>" +
                    "</div>" +
                    "<div style='display: inline-block !important;' id=\"" + propDivId + "\">" +
                        "<button id=\"" + probButtonId + "\">Select Property Pattern</button>" +
                    "</div>"
        );

		$("#" + relButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#relation-pattern-dialog").dialog("open");
	   			$("#relation-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + relDivId, "#relation-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#relation-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});

		$("#" + probButtonId )
	   		.button()
	   		.click(function( event ) {
	   			$("#property-pattern-dialog").dialog("open");
	   			$("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
	   				handlePatternSelected( "#" + propDivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
	   				$("#property-pattern-dialog").dialog("close");
	   				event.preventDefault();
                });
                event.preventDefault();
        	});
	}
    else if (patternId == "PropertyDomainRangeRestrictionPattern") {
        var nextIndex = getNextIndex();
        var propDivId = "propDivId" + nextIndex;
        var probButtonId = "probButtonId" + nextIndex;
        var domClsDivId = "domClsDivId" + nextIndex;
        var domClsButtonId = "domClsButtonId" + nextIndex;
        var datatypeId = "datatypeId" + nextIndex;

        $(pageDivId).html(
                "<input type=\"hidden\" name=\"PropertyDomainRangeRestrictionPattern\"/>" +
                "<p><span class=\"patternInstanceType\">Property Domain-Range Restriction Pattern - Instance:</span></p>" +
                "<p><span class=\"mappingElementType\">Property:</span><div id=\"" + propDivId + "\"><button id=\"" + probButtonId + "\">Select Property Pattern</button></div></p>" +
                "<p><span class=\"mappingElementType\">Domain-Class:</span><div id=\"" + domClsDivId + "\"><button id=\"" + domClsButtonId + "\">Select Class Pattern</button></div></p>" +
                "<p><span class=\"mappingElementType\">Range:</span></p><p style=\"padding-left:50px;\"><span class=\"mappingElementUriTag\">Datatype:</span><span class=\"mappingElementUriTag\"></span><input id=\"" + datatypeId + "\" /></p>"
        );

        var availableDatatypes = [
            "xsd:boolean",
            "xsd:byte",
            "xsd:short",
            "xsd:int",
            "xsd:integer",
            "xsd:positiveInteger",
            "xsd:negativeInteger",
            "xsd:long",
            "xsd:float",
            "xsd:double",
            "xsd:dateTime",
            "xsd:dateTimeStamp",
            "xsd:string",
            "xsd:normalizedString",
            "xsd:token",
            "xsd:language",
            "xsd:Name",
            "xsd:hexBinary",
            "xsd:base64Binary"
        ];
        $("#" +datatypeId).autocomplete({
            source: availableDatatypes
        });

        $("#" + probButtonId )
            .button()
            .click(function( event ) {
                $("#property-pattern-dialog").dialog("open");
                $("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
                    handlePatternSelected( "#" + propDivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
                    $("#property-pattern-dialog").dialog("close");
                    event.preventDefault();
                });
                event.preventDefault();
            });

        $("#" + domClsButtonId )
            .button()
            .click(function( event ) {
                $("#class-pattern-dialog").dialog("open");
                $("#class-pattern-dialog" + " a").unbind('click').click(function( event ) {
                    handlePatternSelected( "#" + domClsDivId, "#class-pattern-dialog", $(this).attr("href"), entityIndex);
                    $("#class-pattern-dialog").dialog("close");
                    event.preventDefault();
                });
                event.preventDefault();
            });
    }

	// ------------------------------------------------------------------------------
	// -- Instance Patterns
	// ------------------------------------------------------------------------------

	else if (patternId == "SimpleInstancePattern") {
		var nextIndex = getNextIndex();
		var indivInputId = "individualInput" + nextIndex;
		var copyButtonId = "indivCopyButton" + nextIndex;

		$(pageDivId).html(
			"<input type=\"hidden\" name=\"SimpleInstancePattern\"/>" +
            "<img class='eleImage' src='images/NamedIndividual.gif'>" +
            "<input type=\"text\" id=\"" + indivInputId + "\" >" +
			"<img class='copyButton' src=\"images/copyButton.png\" id=\"" + copyButtonId + "\" title=\"Place the URI of the owl DatatypeProperty selected.\">"
		);

		var hiddenInputDiv = "#jsonOntologyFlatListsA";
		if (entityIndex == "2") {
			hiddenInputDiv = "#jsonOntologyFlatListsB";
		}
		var jsonStr = $(hiddenInputDiv).val();
		var json = $.parseJSON(jsonStr);
		if( typeof(json.indivja) != "undefined" ) {
			$("#" + indivInputId).autocomplete({
            	source: json.indivja,
            	select: function( event, ui ) {
            		var selectedUri = ui.item.uri;
            		// Input Text
                	$("#" + indivInputId).val( selectedUri );
                	// Input Title
					$("#" + indivInputId).attr("title", ui.item.value);
                	return false;
            	}
        	}).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.value + " ( <span class='text06em'>" + item.uri + "</span> )" + "</a>")
                    .appendTo(ul);
            };
		}

		$("#" + copyButtonId)/*.unbind('click')*/.bind('click', function ( event ) {
			var entityCharIndex = "A";
			if (entityIndex == "2") {
				entityCharIndex = "B";
			}
			var selectedElementStr = $.cookie('SelectedIndivOnt' + entityCharIndex);
			var existingUri = $("#" + indivInputId).val();
			if (selectedElementStr != null) {
                var selectedElement = $.parseJSON(selectedElementStr);
				$("#" + indivInputId).attr("title", selectedElement.name);
				// Input Text
				if (existingUri == "" ) {
					$("#" + indivInputId)
						.hide()
						.val( selectedElement.uri )
						.fadeIn(500);
				} else {
					if ( selectedElement.uri != existingUri) {
						$("#" + indivInputId)
							.fadeOut(500)
							.val( selectedElement.uri )
							.fadeIn(500);
					}
				}
			} else {
				$("#infodialog").empty();
				$("#infodialog").html("<p>None owl Individual from Ontology " + entityCharIndex + " selected...</p><p>Input text has not changed !</p>");
				$("#infodialog").dialog("open");
			}

		});
	}

    // Other

    else if (patternId == "PropertyCollectionPattern") {
        var patternInstanceLabel = "Two or More Parameters Pattern - Instance:";
        var hiddenInputElement = "<input type=\"hidden\" name=\"PropertiesCollectionPattern\"/>";

        var nextIndex = getNextIndex();
        var cls1DivId = "prop1DivId" + nextIndex;
        var cls1ButtonId = "prop1ButtonId" + nextIndex;
        var cls2DivId = "prop2DivId" + nextIndex;
        var cls2ButtonId = "prop2ButtonId" + nextIndex;
        var addOtherClassDivId = "addPropDivId" + nextIndex;
        var addClsButtonId = "addPropButtonId" + nextIndex;

        $(pageDivId).html(
                hiddenInputElement +
                "<p><span class=\"patternInstanceType\">" + patternInstanceLabel + "</span> <button id=\"" + addClsButtonId + "\">Add ...</button></p>" +
                "<p><div id=\"" + cls1DivId + "\"><button id=\"" + cls1ButtonId + "\">Select Property Pattern</button></div></p>" +
                "<p><div id=\"" + cls2DivId + "\"><button id=\"" + cls2ButtonId + "\">Select Property Pattern</button></div></p>" +
                "<div id=\"" + addOtherClassDivId + "\"></div>"
        );

        $("#" + cls1ButtonId )
            .button()
            .click(function( event ) {
                $("#property-pattern-dialog").dialog("open");
                $("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
                    handlePatternSelected( "#" + cls1DivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
                    $("#property-pattern-dialog").dialog("close");
                    event.preventDefault();
                });
                event.preventDefault();
            });

        $("#" + cls2ButtonId )
            .button()
            .click(function( event ) {
                $("#property-pattern-dialog").dialog("open");
                $("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
                    handlePatternSelected( "#" + cls2DivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
                    $("#property-pattern-dialog").dialog("close");
                    event.preventDefault();
                });
                event.preventDefault();
            });

        $( "#" + addClsButtonId)
            .button({
                icons: {
                    primary: "ui-icon-plus"
                }, text: false
            })
            .click(function( event ) {
                var nextIndex = getNextIndex();
                var clsDivId = "propDivId" + nextIndex;
                var clsButtonId = "propButtonId" + nextIndex;
                $("#" + addOtherClassDivId).append(
                        "<div><button class=\"clsRemoveButton\" style=\"margin-left:5px;\"></button> <div id=\"" + clsDivId + "\"><button id=\"" + clsButtonId + "\">Select Property Pattern</button></div></div>"
                );

                $("#" + clsButtonId )
                    .button()
                    .click(function( event ) {
                        $("#property-pattern-dialog").dialog("open");
                        $("#property-pattern-dialog" + " a").unbind('click').click(function( event ) {
                            handlePatternSelected( "#" + clsDivId, "#property-pattern-dialog", $(this).attr("href"), entityIndex);
                            $("#property-pattern-dialog").dialog("close");
                            event.preventDefault();
                        });
                        event.preventDefault();
                    });

                $( ".clsRemoveButton" )
                    .button({
                        icons: {
                            primary: "ui-icon-closethick"
                        }, text: false
                    })
                    .click(function( event ) {
                        $(this).parent().remove();
                        event.preventDefault();
                    });
                event.preventDefault();
            });
    }
	else {
		$(pageDivId).html("The User Interface for \"" + patternId + "\" is under Construction !");
	}
}


// --------------------------------- TRANSFORMATION --------------------------------- 


/**
 * This methods enables the user to select the desirable Transformation Pattern and accordingly updates the main screen.
 *  
 * @see handleTransformation
 * 		Being used for updating the main screen based on the Transformation Pattern selected.
 */
function handleTransformationButton(buttonIndex) {
	$( "#transfbutton" + buttonIndex )
		.button()
	    .click(function( event ) {
	      	$("#transfSelection").dialog("open");
	        	
	       	$( "#transfaccordion" ).accordion({
	        	event: "click hoverintent",
	            heightStyle: "fill"
	        });
	            
	        $( "#accordion-resizer" ).resizable({
	            minHeight: 140,
	            minWidth: 200,
	            resize: function() {
	                $( "#transfaccordion" ).accordion( "refresh" );
	            }
	        });
	        
	        $("#transfaccordion a").unbind('click').click(function( event ) {
		   		var uri = $(this).attr("href");
		   		handleTransformation(uri, buttonIndex);
		   		$("#transfSelection").dialog("close");
		   		event.preventDefault();
			});
	        	
	        event.preventDefault();
	    });
    	
	$("#transfSelection").dialog({
		autoOpen: false,
		width: 600,
		height: 500
	});	
	
}

/**
 * This method updates the main screen based on the Transformation Pattern Selected
 *  
 * @param {Object} uri
 * 		The Transformation Pattern URI
 */
function handleTransformation(uri, buttonIndex) {
	// Find Transformation ID
    var startIndex = uri.indexOf("#") + 1;
	var endIndex = uri.length;
	var transfId = uri.substring(startIndex, endIndex);
	var transDivId = "#transfdiv" + buttonIndex;
  
  	var parentPaddingLeft = parseInt( $(transDivId).parent().css("padding-left").replace("px", "") );
	$(transDivId).css("padding-left", ( parentPaddingLeft + 20 ) + "px");	
  
  	$(transDivId).empty();
  	
  	if (transfId == "ValuesTransformation" || transfId == "ExpressionTransformation") {
  		var transfInputId = "tranformationInputId" + buttonIndex;
  		var addArgButtonId = "addArgButton" + buttonIndex;
  		var addArgDiv = "argsdiv" + buttonIndex;
  		$(transDivId).html(
  			"<input type=\"hidden\" name=\"" + transfId + "\"/>" + 
  			"<p><span class=\"mappingElementUriTag\">Transformation-URI: </span></p>" +
  			"<p><input type=\"text\" id=\"" + transfInputId + "\" style=\"width: 90%; margin-right: 5px;\"></p>" +
  			"<p><span class=\"mappingElementUriTag\">Arguments: </span> <button id=\"" + addArgButtonId + "\">Add ...</button></p>" + 
  			"<div id=\"" + addArgDiv + "\"></div>" + 
  			"<p><span class=\"mappingElementUriTag\">Description: </span></p>" +
  			"<textarea rows=\"3\" style=\"width: 90%; margin-right: 5px; font-size: 0.8em;\"></textarea>"
  		);
  		
  		$( "#" + addArgButtonId )
			.button()
		    .click(function( event ) {
		    	$("#" + addArgDiv).append(
		    		"<div style=\"margin-left:20px;\">" + 
		    		"<span class=\"mappingElementUriTag\">Name:</span><input type=\"text\" id=\"argname\" style=\"width:100px;\"> : " + 
		    		"<span class=\"mappingElementUriTag\">Value:</span><input type=\"text\" id=\"argvalue\" style=\"width:100px;\">" + 
		    		"<button class=\"removeButton\" style=\"margin-left:5px;\"></button>" +
		    		"</div>"
		    	);
		    	
		    	$( ".removeButton" )
					.button({
				        icons: {
				            primary: "ui-icon-closethick"
				        }, text: false
				    })
				    .click(function( event ) {
		    			$(this).parent().remove();
				    	event.preventDefault();
				    });
		    	event.preventDefault();
		    });
			
		// Make Input Field auto-complete based on string and for each one define parameters
		/*
		var implementedFunctions = [
			"http://www.semanticweb.com/iccs/ntua/harmonciss/BloodTestValueToEntity",
			"http://www.semanticweb.com/iccs/ntua/harmonciss/BloodTestValueAndDateToEntity",
            "http://www.semanticweb.com/iccs/ntua/harmonciss/BloddTestValueAndAssessmentAndDateToEntity"
        ];
		
		var service1 = {
			args: ["Cohort Test Name", "Test Unit of Measurement", "Year Tests Performed", "Lab Test UNL", "Lab Test DNL"],
			desc: "Provides the Name of the Lab Test, the Test Outcome, Date Performed and Assessment Code (OWL entities) " +
				  "based on the value of the given property and the service parameters also specified."
		};

		var service2= {
			args: ["Cohort Test Name", "Test Unit of Measurement", "Date Format", "Lab Test UNL", "Lab Test DNL"],
			desc: "Provides the Name of the Lab Test, the Test Outcome, Date Performed and Assessment Code (OWL entities)" +
				  "based on the values of the two given properties and the service parameters also specified."
		};
		
		var service2= {
			args: ["Cohort Test Name", "Test Unit of Measurement", "Date Format"],
			desc: "Provides the Name of the Lab Test, the Test Outcome, Date Performed and Assessment Code (OWL entities)" +
				  "based on the three given properties."
		};

		$("#" + transfInputId).autocomplete({
           	source: implementedFunctions,
           	select: function( event, ui ) {
           		var serviceUri = ui.item.label;
           		// Depending on the Service URI selected, update arguments
				var serviceStartIndex = serviceUri.lastIndexOf("/") + 1;
				var serviceEndIndex = serviceUri.length;
				var serviceLabel = serviceUri.substring(serviceStartIndex, serviceEndIndex);
				
				$("#" + addArgDiv).empty();
				var service;
				if (serviceLabel == "BloodTestValueToEntity") service = service1;
				if (serviceLabel == "BloodTestValueAndDateToEntity") service = service2;
				if (serviceLabel == "BloddTestValueAndAssessmentAndDateToEntity") service = service3;
				
				jQuery.each(service.args, function(i, item) {
						$("#" + addArgDiv).append(
							"<div style=\"margin-left:20px;\">" + 
							"<span class=\"mappingElementUriTag\">Name:</span><input type=\"text\" id=\"argname\" value=\"" + item + "\"style=\"width:150px;\" readonly> " + 
							"<span class=\"mappingElementUriTag\">Value:</span><input type=\"text\" id=\"argvalue\" style=\"width:100px;\">" +
							"</div>"
						);
				});
				
				$(transDivId + " textarea").val(service.desc);
				
				return true;
           	}
        });	
		*/
  	} else {
  		//Update if necessary
  		$(transDivId).html("The user Interface for \"" + transfId + "\" is under Construction !");
  	}
  	
}


// --------------------------------- ACTIONS --------------------------------- 


/**
 * This method stores the Correspondence specified in "Create a New Correspondence" tab and 
 * updates the "View All Correspondences" tab. 
 *  
 * @see getData
 * 		This method is being used to get the data specified in each entity.
 * @see jsonDataToHtml
 * 		This method is being used to get an HTML version of JSON data retrieved from the entity.
 * @see updateCorrespondences
 * 		This method is being used for updating the user's screen with the correspondences specified.
 */
function handleStoreResetButtons() {
	$("#storeButton")
		.button({
	        icons: {
	        	primary: "ui-icon-gear"
	        }
		})
	    .click(function( event ) {
	    	// Get data from HTML elements
	      	var entity1json = getData("#entity1div");
	       	var entity2json = getData("#entity2div");
	       	var directTransformationJson = getTransformationData("#transfdiv1");
			// TODO: Check if needed
	       	var inverseTransformationJson = getTransformationData("#transfdiv2");
	       	var relationstr = $('#relationdiv').find("#relationSelection").val();
			var directionstr = $('#directionSelection').val();
	       	var commentsstr = $('#mappingdesc').val();
	       	
			// Check if Entity 1 and 2 and DataTrans internal elements specified
			if (entity1json != null && entity2json != null && directTransformationJson != "") {
				// Create a JSON object with all the correspondence parameters specified
				var mappingJson = {
					entity1: entity1json,
					entity2: entity2json,
					directTransformation: directTransformationJson,
					inverseTransformation: inverseTransformationJson,
					relation: relationstr,
					direction: directionstr,
					comments: commentsstr
				};
				
				// Load - Update - Save Hidden Field
				var jsonStr = $("#correspondencesHiddenInput").val();
				var json = $.parseJSON(jsonStr);
				json.jsonarray.push(mappingJson);
				$("#correspondencesHiddenInput").val(JSON.stringify(json));
				// Update user's screen
				updateCorrespondences();
				
				// Show correspondence created in a Dialog
				$("#dialog-message").empty();
				$("#dialog-message").append();
				$("#dialog-message").append("<p>Correspondence Instance successfully created !</p>");
				$("#dialog-message").append("<p><b>Entity-1:</b></p><p>" + jsonDataToHtml(entity1json) + "</p>");
				$("#dialog-message").append("<p><b>Entity-2:</b></p><p>" + jsonDataToHtml(entity2json) + "</p>");
				if (directTransformationJson != null) $("#dialog-message").append("<p><strong>Direct Transformation:</strong></p><p>" + jsonTransformationDataToHtml(directTransformationJson) + "</p>");
				if (inverseTransformationJson != null) $("#dialog-message").append("<p><strong>Inverse Transformation:</strong></p><p>" + jsonTransformationDataToHtml(inverseTransformationJson) + "</p>");
				$("#dialog-message").append("<p><strong>Relation:</strong></p><p>" + relationstr + "</p>");
				if (directionstr != "") 	$("#dialog-message").append("<p><strong>Direction:</strong></p><p>" + directionstr + "</p>");
				if (commentsstr != "") 	$("#dialog-message").append("<p><strong>Comments:</strong></p><p>" + commentsstr + "</p>");
				$("#dialog-message").dialog("open");
				
				// Reset ... for defining a new correspondence
				resetCreateMappingPanel();
			} else {
				alert("No Mapping Rule specified due to Missing Values !");
			}
			
	       	event.preventDefault();
	    });
	        
	$("#resetButton")
		.button()
	    .click(function( event ) {
	       	resetCreateMappingPanel();
	       	event.preventDefault();
	    });	
}


// ---------------------------------  VIEW --------------------------------- 

/**
 * This method updates user's screen with the correspondences specified, based on
 * the ones existing in the hidden field. 
 */
function updateCorrespondences() {
	// Get correspondences from hidden input field
	var jsonStr = $("#correspondencesHiddenInput").val();
	var json = $.parseJSON(jsonStr);
	
	// Change pupup title of this tab 
	if ( typeof(json.jsonarray) != "undefined" ) {
		if (json.jsonarray.length > 0) {
			var thereMessage= "Examine and Export the " + json.jsonarray.length + " Mapping Rule(s) specified.";
			$("#tabsM > ul > li > a[href=#tabs-4]").attr("title", thereMessage);
			$("#correspondenceListExport").show();
		} else {
            var thereMessage= "None Mapping Rule specified !" ;
            $("#tabsM > ul > li > a[href=#tabs-4]").attr("title", thereMessage);
            $("#correspondenceListExport").show();
		}
	} else {
		$("#tabsM > ul > li > a[href=#tabs-4]").removeAttr('title');
	}
	
	// Update User screen
    //$("#correspondenceList").fadeOut("slow");
    $("#correspondenceList").empty();
    //$("#correspondenceList").fadeIn("fast");

	// Reset elements color CHECK
	$("#tabsA a, #tabsB a").each(function(index) {
		$(this).removeAttr("harmcolor");
		var prevcolor = $(this).attr("prevcolor");
		if (typeof(prevcolor) != "undefined") {
			$(this).css("color", prevcolor);
			$(this).removeAttr("prevcolor");
		}
	});
	// Update color of harmonized terms
	jQuery.each(json.jsonarray, function(i, item) {
		// Terms
		if (item.entity1.pid == "SimpleClassPattern") {
			$("#tabsA a, #tabsB a").each(function(index) {
				var uri = $(this).attr("href");
				if (uri == item.entity1.classuri) {
					$(this).attr("harmcolor", "#008ae6");
					$(this).attr("prevcolor", $(this).css("color"));
					$(this).css("color", "#008ae6");
				}
			});
		}
		if (item.entity2.pid == "SimpleClassPattern") {
			$("#tabsA a, #tabsB a").each(function(index) {
				var uri = $(this).attr("href");
				if (uri == item.entity2.classuri) {
					$(this).attr("harmcolor", "#008ae6");
					$(this).attr("prevcolor", $(this).css("color"));
					$(this).css("color", "#008ae6");
				}
			});
		}
		// Fields
		if (item.entity1.pid == "PropertiesCollectionPattern") {
			$("#tabsA a, #tabsB a").each(function(index) {
				var elem = $(this);
				var uri = elem.attr("href");
				jQuery.each(item.entity1.proparray, function(i, prop) {
					if (uri == prop.propertyuri) {
						elem.attr("harmcolor", "#008ae6");
						elem.attr("prevcolor", elem.css("color"));
						elem.css("color", "#008ae6");
					}
				});
			});
		}
		
		// TODO: Update for Classes/Properties in the RIGHT side
		
	});
	
	//Order Mapping Rules already specified
	json.jsonarray.sort(function(a, b) {
		//alert("a: " + JSON.stringify(a));
		if (a.entity1.pid != b.entity1.pid) {
			return a.entity1.pid.localeCompare(b.entity1.pid);
		} else {
			if (a.entity1.pid == "SimpleClassPattern") {
				return a.entity1.classuri.localeCompare(b.entity1.classuri);
			}
			return 0;
		}
	});
	
	jQuery.each(json.jsonarray, function(i, item) {
		
		var transformationStr = "";
		if (item.directTransformation != null || item.inverseTransformation != null) {
			if (item.directTransformation != null) {
				transformationStr += 
					"<tr>" + 
	    				"<td class=\"transformation\" colspan=\"3\">" +
	    					"<strong>Direct:</strong>" +
	    				"</td>" +
	    			"</tr>" +
					"<tr>" + 
	    				"<td class=\"transformation\" colspan=\"3\">" +
	    					jsonTransformationDataToHtml(item.directTransformation) +
	    				"</td>" +
	    			"</tr>";
			}
			if (item.inverseTransformation != null) {
				transformationStr += 
					"<tr>" + 
						"<tr>" + 
	    					"<td class=\"transformation\" colspan=\"3\">" +
	    						"<strong>Inverse:</strong>" +
	    					"</td>" +
	    				"</tr>" +
	    				"<td class=\"transformation\" colspan=\"3\">" +
	    					jsonTransformationDataToHtml(item.inverseTransformation) +
	    				"</td>" +
	    			"</tr>";
			}
		}

	   	var nextIndex = getNextIndex();
		var corHiddenInputID = "corHiddenInput" + 	nextIndex;
	    
	    var directionAttrStr = "";
        if (typeof(item.direction) != "undefined" && item.direction != "")  {
	    	directionAttrStr = "title=\"Correspondence Direction: " + item.direction + "\"";
	    }

        var addStyle = ((typeof(item.similarity) != "undefined" && item.similarity != "")) ? " style='border: 1px dashed forestgreen !important;'" : "";

	    var tableStr = 
	    	"<table class=\"viewMappingTable\">" +
	    		"<input type=\"hidden\" id=\"" + corHiddenInputID + "\"/>" +
	    		"<tr>" +
		   			"<td class=\"correspondenceArea\">" +
		   				"<table class=\"correspondenceTable\" " + addStyle + ">" +
		     				"<tr>" +
			       				"<td class=\"viewEntity1\">" + jsonDataToHtml(item.entity1) + "</td>" +
			       				"<td class=\"viewRelation\" " + directionAttrStr + ">" + item.relation + "</td>" +
			       				"<td class=\"viewEntity2\">" + jsonDataToHtml(item.entity2) + "</td>" +
		       				"</tr>" +
		       				transformationStr +
		   				"</table>" +
		   			"</td>" +
		   			"<td class=\"actionArea\">" +
		   				"<img src=\"images/delete.icon.png\" class=\"viewMappingImg\">" +
		   			"</td>" +
	    		"</tr>" +
	    	"</table>";
	       	
	    $("#correspondenceList").append(tableStr);
				
		// Store item json...
		$("#" + corHiddenInputID).val(JSON.stringify(item));
			
	}); // end each function
	
	handleCorrespondencesLinks();
	
	// Deleting ... 
	$(".viewMappingImg")
		.click(function(){
			var jsonStr = $("#correspondencesHiddenInput").val();
			var json = $.parseJSON(jsonStr);
			var selectedJsonStr = $(this).parent().parent().parent().parent().find("input[type=hidden]").val();

            $(this).parent().parent().attr("style", "background-color: #FAAC58;");

			// remove selected
			json.jsonarray = jQuery.grep(json.jsonarray, function(item, i) {
	    			if ( JSON.stringify(item) == selectedJsonStr) {
	    				return false;
	    			}
	    			return true;
	    		});
	   		$("#correspondencesHiddenInput").val(JSON.stringify(json));

			updateCorrespondences();
		});
	$(".viewMappingImg")	
		.hover(
			function () {
				$(this).parent().attr("style", "background-color: #FAAC58;");
                $(this).parent().parent().attr("style", "background-color: #FAAC58;");
				$(this).css("cursor", "hand");
			}, 
			function () {
				$(this).parent().removeAttr("style");
                $(this).parent().parent().removeAttr("style");
				$(this).css("cursor", "pointer");
			}
		);	
}

function handleCorrespondencesLinks() {
	$(".viewEntity1 a").click(function(evt) {
		var elementUri = $(this).attr('href');
		var jsonParamStr = $("#jsonOntologyA").val();
		presentDataInDialog(jsonParamStr, elementUri);
		return false;
	});
	$(".viewEntity2 a").click(function(evt) {
		var elementUri = $(this).attr('href');
		var jsonParamStr = $("#jsonOntologyB").val();
		presentDataInDialog(jsonParamStr, elementUri);
		return false;
	});
}

/**
 * This method returns the Create Correspondence "Form" in the initial state. 
 */
function resetCreateMappingPanel() {
	$("#mapscenariodiv")
		.empty()
		.css("padding-left", "5px")
		.html("<button id=\"mapscenariobutton\">Select Mapping Scenario...</button>");	
	handleMappingScenarioButton();
	
	$("#entity1div")
		.empty()
		.css("padding-left", "5px")
		.html("<button id=\"entity1button\">Specify...</button>");
	handleEntityButton("1");
	
	$("#entity2div")
		.empty()
		.css("padding-left", "5px")
		.html("<button id=\"entity2button\">Specify...</button>");
	handleEntityButton("2");
	
	$("#transfdiv1").empty().css("padding-left", "5px");
	$("#transfdiv2").empty().css("padding-left", "5px");
	/*
	$("#transfdiv1")
		.empty()
		.css("padding-left", "5px")
		.html("<button id=\"transfbutton1\">Specify...</button>");
	handleTransformationButton(1);
	
	// TODO: check if needed
	$("#transfdiv2")
		.empty()
		.css("padding-left", "5px")
		.html("<button id=\"transfbutton2\">Specify...</button>");
	handleTransformationButton(2);
	*/
	
	$('#relationdiv').find("#relationSelection").val("Linked With");
	$('#directionSelection').val("From Ontology 1 to Ontology 2");
	$('#mappingdesc').val("");
}


function handleExportAction() {
    // Export User Defined Mappings
	$("#exportButton")
		.button()
	    .click(function( event ) {
	    	if ($("#exportFormat").val() == "0") {
				alert("Please select Mapping Rules Export Format.");
				event.preventDefault();
				return ;
			}
			var correspondencesStr = $("#correspondencesHiddenInput").val();
			// Put data in the hidden fields within the form
			$("#correspondenceListExport #theExportCorrespondences").val(correspondencesStr);
			$("#correspondenceListExport #theExportOntologies").val(
				JSON.stringify( 
					{ 
						ontA: $.parseJSON( $("#jsonOntologyPropertiesA").val() ) , 
						ontB: $.parseJSON( $("#jsonOntologyPropertiesB").val() )
					} 
				)
			);
			// Manually submit the form
			$("#exportForm").submit();
			event.preventDefault();
   		});
}


/**
 * Finds the data specified within an element of the HTML page and places them in a JSON object.
 * 
 * In the JSON object, the "pid" element indicates the Pattern being used/instanciated.
 * 
 * @param divIndex
 * 		The ID of the element in which the entity specified
 * @return
 * 		Returns a JSON object with the data
 */
function getData(divIndex) {
	var pattern = $(divIndex + " > input[type=hidden]").attr("name");

	// ------------------------------------------------------------------------------
	// -- Class Patterns
	// ------------------------------------------------------------------------------
	
	if (pattern == "SimpleClassPattern") {
        var ele = $(divIndex + " > input[type=text]");
		var eleUri = ele.val();
		if (eleUri.trim() == "") return null;
        var eleName = ele.attr("title");
        if ( typeof (eleName) == "undefined" ) {
            eleName = eleUri;
        }
		var json = { 
			pid: "SimpleClassPattern", 
			classuri: eleUri ,
            classname: eleName
        };
		return json;
	}
	
	// TODO: Handle Return NULL values
	
	if (pattern == "ClassUnionPattern" || pattern == "ClassIntersectionPattern") {
		var jsonarray = [];
		$(divIndex + " > div[id^=\"cls\"]").each(function(index) {
    		jsonarray.push( getData( "#" +  $(this).attr("id") ) );
		});
		$(divIndex + " > div[id^=\"addClassDiv\"] > div > div[id^=\"cls\"]").each(function(index) {
    		jsonarray.push( getData( "#" +  $(this).attr("id") ) );
		});
		var json = { 
			pid: pattern, 
			classarray: jsonarray//getData( "#" +  $(divIndex + " > div[id^=\"relDiv\"]").attr("id") )
		};
		return json;
	}

	if (pattern == "ClassByAttributeValueRestrictionPattern" || pattern == "ClassByAttributeOccurencePattern") {
		var json = { 
			pid: pattern, 
			cls: getData( "#" +  $(divIndex + " > div[id^=\"clsDiv\"]").attr("id") ),
			property: getData( "#" + $(divIndex + " > div[id^=\"propDiv\"]").attr("id") ),
			restrictionuri: $(divIndex + " > p > input[id^=\"compInput\"]").val(),
			restrictiondesc: $(divIndex + " > p > textarea[id^=\"compTextArea\"]").val(),
			restrictionvalue: $(divIndex + " > p > input[id^=\"valInput\"]").val()
		};
		return json;
	}
	
	if (pattern == "ClassWithPropertiesRestrictionPattern") {
		var i = 0;
		var jsonarray = [];
        $(divIndex + " > div[id^=\"addPropDiv\"] > div > div[id^=\"prop\"]").each(function(index) {
			var dataJson = getData( "#" +  $(this).attr("id") );
			dataJson.index = i++;
            jsonarray.push(dataJson);
        });
		var json = {
			pid: pattern, 
			cls: getData( "#" +  $(divIndex + " > div[id^=\"clsDiv\"]").attr("id") ),
			proparray: jsonarray
		};
		return json;	
	}
	
	// ------------------------------------------------------------------------------
	// -- Relation Patterns
	// ------------------------------------------------------------------------------
	
	if (pattern == "SimpleRelationPattern") {
        var ele = $(divIndex + " > input[type=text]");
        var eleUri = ele.val();
		if (eleUri.trim() == "") return null;
        var eleName = ele.attr("title");
        if ( typeof (eleName) == "undefined" ) {
            eleName = eleUri;
        }
		var json = { 
			pid: "SimpleRelationPattern", 
			relationuri: eleUri ,
            relationname: eleName
		};
		return json;
	}
	
	// TODO: Handle Return NULL values
	
	if (pattern == "InverseRelationPattern") {
		var json = { 
			pid: "InverseRelationPattern", 
			relation: getData( "#" +  $(divIndex + " > div[id^=\"relDiv\"]").attr("id") )
		};
		return json;
	}
	
	if (pattern == "RelationDomainRestrictionPattern") {
		var json = { 
			pid: "RelationDomainRestrictionPattern", 
			relation: getData( "#" +  $(divIndex + " > div[id^=\"relDiv\"]").attr("id") ),
			domainclass: getData( "#" + $(divIndex + " > div[id^=\"domClsDiv\"]").attr("id") )
		};
		return json;
	}
	
	if (pattern == "RelationRangeRestrictionPattern") {
		var json = { 
			pid: "RelationRangeRestrictionPattern", 
			relation: getData( "#" +  $(divIndex + " > div[id^=\"relDiv\"]").attr("id") ),
			rangeclass: getData( "#" + $(divIndex + " > div[id^=\"rangeClsDiv\"]").attr("id") )
		};
		return json;
	}
	
	if (pattern == "RelationPathPattern") {
		var jsonarray = [];
		$(divIndex + " > div[id^=\"rel\"]").each(function(index) {
    		jsonarray.push( getData( "#" +  $(this).attr("id") ) );
		});
		$(divIndex + " > div[id^=\"addRelationDiv\"] > div > div[id^=\"rel\"]").each(function(index) {
    		jsonarray.push( getData( "#" +  $(this).attr("id") ) );
		});
		var json = { 
			pid: pattern, 
			relationarray: jsonarray
		};
		return json;
	}

    if (pattern == "RelationDomainRangeRestrictionPattern") {
        var json = {
            pid: "RelationDomainRangeRestrictionPattern",
            relation: getData( "#" +  $(divIndex + " > div[id^=\"relDiv\"]").attr("id") ),
            domainclass: getData( "#" + $(divIndex + " > div[id^=\"domClsDiv\"]").attr("id") ),
            rangeclass: getData( "#" + $(divIndex + " > div[id^=\"rangeClsDiv\"]").attr("id") )
        };
        return json;
    }

	// ------------------------------------------------------------------------------
	// -- Property Patterns
	// ------------------------------------------------------------------------------
	
	if (pattern == "SimplePropertyPattern") {
        var ele = $(divIndex + " > input[type=text]");
        var eleUri = ele.val();
		if (eleUri.trim() == "") return null;
        var eleName = ele.attr("title");
        if ( typeof (eleName) == "undefined" ) {
            eleName = eleUri;
        }
        var json = {
            pid: "SimplePropertyPattern",
            propertyuri: eleUri ,
            propertyname: eleName
        };
		return json;
	}
	
	// TODO: Handle Return NULL values
	
	if (pattern == "PropertyDomainRestrictionPattern") {
		var json = { 
			pid: "PropertyDomainRestrictionPattern", 
			property: getData( "#" +  $(divIndex + " > div[id^=\"propDiv\"]").attr("id") ),
			domainclass: getData( "#" + $(divIndex + " > div[id^=\"domClsDiv\"]").attr("id") )
		};
		return json;
	}
	
	if (pattern == "PropertyRangeRestrictionPattern") {
		var json = { 
			pid: "PropertyRangeRestrictionPattern", 
			property: getData( "#" +  $(divIndex + " > div[id^=\"propDiv\"]").attr("id") ),
			rangedatatype: $(divIndex + " > p > input[id^=\"datatype\"]").val()
		};
		return json;
	}
	
	if (pattern == "PropertyValueRestrictionPattern") {
		var json = { 
			pid: "PropertyValueRestrictionPattern", 
			property: getData( "#" +  $(divIndex + " > div[id^=\"propDiv\"]").attr("id") ),
			restrictionuri: $(divIndex + " > p > input[id^=\"compInput\"]").val(),
			restrictiondesc: $(divIndex + " > p > textarea[id^=\"compTextArea\"]").val(),
			restrictionvalue: $(divIndex + " > p > input[id^=\"valueInput\"]").val()
		};
		return json;
	}
	
	if (pattern == "RelationPropertyPathPattern") {
		var json = { 
			pid: "RelationPropertyPathPattern", 
			relation: getData( "#" +  $(divIndex + " > div[id^=\"relDiv\"]").attr("id") ),
			property: getData( "#" + $(divIndex + " > div[id^=\"propDiv\"]").attr("id") )
		};
		return json;
	}

    if (pattern == "PropertyDomainRangeRestrictionPattern") {
        var json = {
            pid: "PropertyDomainRangeRestrictionPattern",
            property: getData( "#" +  $(divIndex + " > div[id^=\"propDiv\"]").attr("id") ),
            domainclass: getData( "#" + $(divIndex + " > div[id^=\"domClsDiv\"]").attr("id") ),
            rangedatatype: $(divIndex + " > p > input[id^=\"datatype\"]").val()
        };
        return json;
    }
	
	// ------------------------------------------------------------------------------
	// -- Instance Patterns
	// ------------------------------------------------------------------------------
	
	if (pattern == "SimpleInstancePattern") {
        var ele = $(divIndex + " > input[type=text]");
        var eleUri = ele.val();
		if (eleUri.trim() == "") return null;
        var eleName = ele.attr("title");
        if ( typeof (eleName) == "undefined" ) {
            eleName = eleUri;
        }
		var json = {
			pid: "SimpleInstancePattern", 
			instanceuri: eleUri ,
            instancename: eleName
		};
		return json;
	}

    // ------------------------------------------------------------------------------
    // -- Other Patterns
    // ------------------------------------------------------------------------------

    if (pattern == "PropertiesCollectionPattern") {
		var data = true;
		var i = 0;
        var jsonarray = [];
        $(divIndex + " > div[id^=\"prop\"]").each(function(index) {
			var dataJson = getData( "#" +  $(this).attr("id") );
			if (dataJson == null) { data = false; return false; }
			dataJson.index = i++;
			jsonarray.push(dataJson);
        });
        $(divIndex + " > div[id^=\"addPropDiv\"] > div > div[id^=\"prop\"]").each(function(index) {
			var dataJson = getData( "#" +  $(this).attr("id") );
			if (dataJson == null) { data = false; return false; }
			dataJson.index = i++;
            jsonarray.push(dataJson);
        });
		if (data == false) return null;
        var json = {
            pid: pattern,
            proparray: jsonarray
        };
        return json;
    }
	
	//+++
	
	return null; //return "Under construction !";
}

/**
 * Transforms the entity provided in a human readable way. 
 * 
 * @param {Object} mapAsJson
 * 		An Entity being part of a Correspondence Instance as a JSON
 * @return 
 * 		An HTML representation of the entity
 */
function jsonDataToHtml(mapAsJson) {
	if (mapAsJson == null) return "";
		
	var mapPid = mapAsJson.pid;
	
	// ------------------------------------------------------------------------------
	// -- Class Patterns
	// ------------------------------------------------------------------------------	
	
	if (mapPid == "SimpleClassPattern") {
        var className = "<a href=\"" + mapAsJson.classuri + "\">" + mapAsJson.classname + "</a>";
        var htmlResponse =
            "<table>" +
            "<tr><td'><img class='eleImage' src='images/Class.gif'>" + className + "</td></tr>" +
            "</table>";
		return htmlResponse;
	} 
	
	if (mapPid == "ClassUnionPattern" || mapPid == "ClassIntersectionPattern") {
		var htmlResponse = "<table><tr><td colspan=\"2\"><strong>" + mapPid + "</strong></td></tr>";
		jQuery.each(mapAsJson.classarray, function(i, item) {
			htmlResponse += "<tr><td>Class:</td><td>" + jsonDataToHtml(item) + "</td></tr>";
		});
		htmlResponse += "</table>";
		return htmlResponse;
	}

	if (mapPid == "ClassByAttributeValueRestrictionPattern" || mapPid == "ClassByAttributeOccurencePattern") {
		var restrictionStr = mapAsJson.restrictionuri;
		if (isUri(restrictionStr)) {
			restrictionStr = getShortUri(restrictionStr);
		}
		var htmlResponse = 
			"<table>" +
				"<tr><td colspan=\"2\"><strong>" + mapPid + "</strong></td></tr>" +
				"<tr><td>Class:</td><td>" + jsonDataToHtml(mapAsJson.cls) + "</td></tr>" +
				"<tr><td>Property:</td><td>" + jsonDataToHtml(mapAsJson.property) + "</td></tr>" +
				"<tr><td>Comparator:</td><td title=\"" + mapAsJson.restrictiondesc + "\">" + restrictionStr + "</td></tr>" +
				"<tr><td>Value:</td><td>" + mapAsJson.restrictionvalue + "</td></tr>" +
			"</table>";
		return htmlResponse;
	}
	
	if (mapPid == "ClassWithPropertiesRestrictionPattern") {
        var htmlResponse =
			"<table>" + 
				"<tr><td colspan=\"2\"><strong>" + mapPid + "</strong></td></tr>" +
				"<tr><td>Class:</td><td>" + jsonDataToHtml(mapAsJson.cls) + "</td></tr>";
        jQuery.each(mapAsJson.proparray, function(i, item) {
            htmlResponse += "<tr><td>Property:</td><td>" + jsonDataToHtml(item) + "</td></tr>";
        });
        htmlResponse += "</table>";
        return htmlResponse;
	}
	
	// ------------------------------------------------------------------------------
	// -- Relation Patterns
	// ------------------------------------------------------------------------------
	
	if (mapPid == "SimpleRelationPattern") {
        var relationName = "<a href=\"" + mapAsJson.relationuri + "\">" + mapAsJson.relationname + "</a>";
        var htmlResponse =
            "<table>" +
            "<tr><td><img class='eleImage' src='images/ObjectProperty.gif'>" + relationName + "</td></tr>" +
            "</table>";

        return htmlResponse;
	} 
	
	if (mapPid == "InverseRelationPattern") {
		var htmlResponse = 
			"<table>" +
				"<tr><td colspan=\"2\"><strong>InverseRelationPattern</strong></td></tr>" +
				"<tr><td>Relation:</td><td>" + jsonDataToHtml(mapAsJson.relation) + "</td></tr>" +
			"</table>";
		return htmlResponse;
	}
	
	if (mapPid == "RelationDomainRestrictionPattern") {
		var htmlResponse = 
			"<table>" +
				"<tr><td colspan=\"2\"><strong>RelationDomainRestrictionPattern</strong></td></tr>" +
				"<tr><td>Relation:</td><td>" + jsonDataToHtml(mapAsJson.relation) + "</td></tr>" +
				"<tr><td>DomainClass:</td><td>" + jsonDataToHtml(mapAsJson.domainclass) + "</td></tr>" +
			"</table>";
		return htmlResponse;
	} 
	
	if (mapPid == "RelationRangeRestrictionPattern") {
		var htmlResponse = 
			"<table>" +
				"<tr><td colspan=\"2\"><strong>RelationRangeRestrictionPattern</strong></td></tr>" +
				"<tr><td>Relation:</td><td>" + jsonDataToHtml(mapAsJson.relation) + "</td></tr>" +
				"<tr><td>RangeClass:</td><td>" + jsonDataToHtml(mapAsJson.rangeclass) + "</td></tr>" +
			"</table>";
		return htmlResponse;
	}
	
	if (mapPid == "RelationPathPattern") {
		var htmlResponse = "<table><tr><td colspan=\"2\"><strong>RelationPathPattern</strong></td></tr>";
		jQuery.each(mapAsJson.relationarray, function(i, item) {
			htmlResponse += "<tr><td>Relation:</td><td>" + jsonDataToHtml(item) + "</td></tr>";
		});
		htmlResponse += "</table>";
		return htmlResponse;
	}

    if (mapPid == "RelationDomainRangeRestrictionPattern") {
        var htmlResponse =
            "<table>" +
            "<tr><td colspan=\"2\"><strong>RelationDomainRangeRestrictionPattern</strong></td></tr>" +
            "<tr><td>Relation:</td><td>" + jsonDataToHtml(mapAsJson.relation) + "</td></tr>" +
            "<tr><td>DomainClass:</td><td>" + jsonDataToHtml(mapAsJson.domainclass) + "</td></tr>" +
            "<tr><td>RangeClass:</td><td>" + jsonDataToHtml(mapAsJson.rangeclass) + "</td></tr>" +
            "</table>";
        return htmlResponse;
    }

    // ------------------------------------------------------------------------------
	// -- Property Patterns
	// ------------------------------------------------------------------------------
	
	if (mapPid == "SimplePropertyPattern") {
        var propertyName = "<a href=\"" + mapAsJson.propertyuri + "\">" + mapAsJson.propertyname + "</a>";
        var htmlResponse =
            "<table>" +
            "<tr><td><img class='eleImage' src='images/DatatypeProperty.gif'>" + propertyName + "</td></tr>" +
            "</table>";
		return htmlResponse;
	} 
	
	if (mapPid == "PropertyDomainRestrictionPattern") {
		var htmlResponse = 
			"<table>" +
				"<tr><td colspan=\"2\"><strong>PropertyDomainRestrictionPattern</strong></td></tr>" +
				"<tr><td>Property:</td><td>" + jsonDataToHtml(mapAsJson.property) + "</td></tr>" +
				"<tr><td>DomainClass:</td><td>" + jsonDataToHtml(mapAsJson.domainclass) + "</td></tr>" +
			"</table>";
		return htmlResponse;
	} 

	if (mapPid == "PropertyRangeRestrictionPattern") {
		var htmlResponse = 
			"<table>" +
				"<tr><td colspan=\"2\"><strong>PropertyRangeRestrictionPattern</strong></td></tr>" +
				"<tr><td>Property:</td><td>" + jsonDataToHtml(mapAsJson.property) + "</td></tr>" +
				"<tr><td>RangeDatatype:</td><td>" + mapAsJson.rangedatatype + "</td></tr>" +
			"</table>";
		return htmlResponse;
	}

	if (mapPid == "PropertyValueRestrictionPattern") {		
		var htmlResponse = 
			"<table>" +
				"<tr><td colspan=\"2\"><strong>PropertyValueRestrictionPattern</strong></td></tr>" +
				"<tr><td>Property:</td><td>" + jsonDataToHtml(mapAsJson.property) + "</td></tr>" +
				"<tr><td>Restriction:</td><td title=\"" + mapAsJson.restrictiondesc + "\">" + mapAsJson.restrictionuri + "</td></tr>" +
				"<tr><td>Value:</td><td>" + mapAsJson.restrictionvalue + "</td></tr>" +
			"</table>";
		return htmlResponse;
	}
	
	if (mapPid == "RelationPropertyPathPattern") {
		var htmlResponse = 
			"<table>" +
				"<tr><td colspan=\"2\"><strong>RelationPropertyPathPattern</strong></td></tr>" +
				"<tr><td>Relation:</td><td>" + jsonDataToHtml(mapAsJson.relation) + "</td></tr>" +
				"<tr><td>Property:</td><td>" + jsonDataToHtml(mapAsJson.property) + "</td></tr>" +
			"</table>";
		return htmlResponse;
	}

    if (mapPid == "PropertyDomainRangeRestrictionPattern") {
        var htmlResponse =
            "<table>" +
            "<tr><td colspan=\"2\"><strong>PropertyDomainRangeRestrictionPattern</strong></td></tr>" +
            "<tr><td>Property:</td><td>" + jsonDataToHtml(mapAsJson.property) + "</td></tr>" +
            "<tr><td>DomainClass:</td><td>" + jsonDataToHtml(mapAsJson.domainclass) + "</td></tr>" +
            "<tr><td>RangeDatatype:</td><td>" + mapAsJson.rangedatatype + "</td></tr>" +
            "</table>";
        return htmlResponse;
    }

	// ------------------------------------------------------------------------------
	// -- Instance Patterns
	// ------------------------------------------------------------------------------
	
	if (mapPid == "SimpleInstancePattern") {
        var instanceName = "<a href=\"" + mapAsJson.instanceuri + "\">" + mapAsJson.instancename + "</a>";
        var htmlResponse =
            "<table>" +
            "<tr><td><img class='eleImage' src='images/NamedIndividual.gif'>" + instanceName + "</td></tr>" +
            "</table>";
		return htmlResponse;
	}

    // ------------------------------------------------------------------------------
    // -- Other Patterns
    // ------------------------------------------------------------------------------

    if (mapPid == "PropertiesCollectionPattern") {
        var htmlResponse = "<table><tr><td colspan=\"2\"><strong>" + mapPid + "</strong></td></tr>";
        jQuery.each(mapAsJson.proparray, function(i, item) {
            htmlResponse += "<tr><td>Property:</td><td>" + jsonDataToHtml(item) + "</td></tr>";
        });
        htmlResponse += "</table>";
        return htmlResponse;
    }

    // +++

	return "JSON: " + JSON.stringify(mapAsJson);
}

/**
 * Finds the transformation parameters specified based on the div provided.
 * 
 * @param
 * 		The div in which the transformation is specified
 * @return
 * 		A JSON string with the transformation parameters 
 */
function getTransformationData(divIndex) {
	if (typeof( $(divIndex + " input[id^=\"tranformationInput\"]").val() ) == "undefined") return null;
	var data = true;
	var jsonarray = [];
	$(divIndex + " > div[id^=\"argsdiv\"] > div").each(function(index) {
		var optional = $(this).attr("optional");
		var argnamestr = $(this).find("input[id=argname]").val();
		var argvaluestr = $(this).find("input[id=argvalue], select[id=argvalue]").val();
		if (optional == "NO" && argvaluestr.trim() == "") { data = false; return false; }
    	jsonarray.push( 
    		{ argname: argnamestr, argvalue: argvaluestr }
    	);
	});
	if (data == false) return "";
	var json = { 
		uri: $(divIndex + " input[id^=\"tranformationInput\"]").val(),
		arguments: jsonarray,
		description: $(divIndex + " #transDesc").html()// $(divIndex + " textarea").val()
	};
	return json;
}

/**
 * Produces a human readable representation of the JSON string provided.
 * 
 * @param
 * 		A JSON string with the transformation parameters specified
 * @return
 * 		A String with the data specified in a human readable way 
 */
function jsonTransformationDataToHtml(json) {
	if (json == null) return "";
	var argumentsstr = "";
	jQuery.each(json.arguments, function(i, item) {
		argumentsstr += item.argname + ": " + item.argvalue;
		if (i < json.arguments.length - 1)  argumentsstr += " , ";
	});
	var tableArgumentsRow = "";
	if (argumentsstr != "") {
		tableArgumentsRow = "<tr><td>Arguments:</td><td>" + argumentsstr + "</td></tr>";
	}
	var jsonUriStr = json.uri;
	if (isUri(json.uri)) {
		jsonUriStr = getShortUri(json.uri);
	}
	var titleAttrStr = "";
	if (json.description != "") {
		titleAttrStr = "title=\"" + json.description + "\"";
	}
	var htmlResponse = 
		"<table>" +
			"<tr><td>Transformation-URI:</td><td " + titleAttrStr + ">" + jsonUriStr + "</td></tr>" +
			tableArgumentsRow +
		"</table>";
	return htmlResponse;
}

/**
 * Checks if the given String is a URI (starts with http://) or not.
 * 
 * @param {Object} str
 * 		A String
 * @return
 * 		Returns true if the given String is a URI, otherwise false.
 */
function isUri(str) {
	if (str.match("^http://")) {
		return true;
	} else {
		return false;
	}
} 

/**
 * Provides an abbreviated form of the given URI.
 * 
 * The response retrieved has the following format: http://... + (/LastString OR #LastString)
 * 
 * @param {Object} uri
 * 		A URI - A String starting with http://
 * @return
 * 		Returns an abbreviated form of the given URI.
 */
function getShortUri(uri) {
	var uriStartIndex = 0;
	var index1 = uri.indexOf("#");
	var index2 = uri.indexOf("/");
	if (index1 > index2) {
		uriStartIndex = index1;
	} else {
		uriStartIndex = index2;
	}
	var uriEndIndex = uri.length;
	var uriLocalNameWithPrefix = uri.substring(uriStartIndex, uriEndIndex);
	var shortUri = "http://..." + uriLocalNameWithPrefix;
	return shortUri;
}

/**
 * This method undertakes to toggle (hide or show) the corresponding areas (divs) when the user 
 * presses the minimize or maximize buttons
 */
function handleMinimizeMaximize() {
	$(".divToggle").click(function( event ) {
		$(this).parent().parent().find(" .toggleable").slideToggle();
		$(this).parent().parent().find(" > div").slideToggle();
        var img = $(this).find("img");
		var imgsrc = img.attr('src');
		if (imgsrc == "images/minimize.png") {
            img.attr('src', "images/maximize.png");
		} else {
            img.attr('src', "images/minimize.png");
		}
	});
	$(".divToggle")	
		.hover(
			function () {
				$(this).css("cursor", "hand");
			}, 
			function () {
				$(this).css("cursor", "pointer");
			}
		);
}
