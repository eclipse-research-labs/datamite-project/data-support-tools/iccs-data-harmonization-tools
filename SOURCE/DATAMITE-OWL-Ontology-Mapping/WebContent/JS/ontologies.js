// This JS files contains all the necessary method for the Left and Right panels of the screen being used for the ontologies.
//
// @author: Efthymios Chondrogiannis


/**
 * This method prepares the panel in which the ontology will be loaded,
 * and accordingly based on the user input updates the panel with the ontological elements.
 * 
 * @param panelIndex
 * 		An index (A or B) indicating the panel in which the ontologies will be loaded.
 */
function preparePanel(panelIndex) {

    $.cookie('SelectedClassOnt' + panelIndex, null);
    $.cookie('SelectedIndivOnt' + panelIndex, null);
    $.cookie('SelectedObjPropOnt' + panelIndex, null);
    $.cookie('SelectedDataPropOnt' + panelIndex, null);
    $.cookie('correspondences', null);

    var ontoDivId = '#onto' + panelIndex;
    var tabsDivId = '#tabs' + panelIndex;
    var dialogDivId = '#ontoDialog' + panelIndex;

    // Disable Tabs - Tables will be enabled when corresponding elements loaded
    $(tabsDivId).tabs( { disabled: [0, 1, 2, 3] } );

    // Hide
    $('#tabsM > #tabs-1 > #divA').hide();
    $('#tabsM > #tabs-1 > #divQuickM').hide();
    $('#tabsM > #tabs-1 > #divB').hide();

    // Bind Button with Dialog Panel
    var ontoButtonId = ontoDivId + ' > #button' + panelIndex;
    $(ontoButtonId)
        .button()
        .click(function( event ) {
            $(dialogDivId).dialog("open");
            event.preventDefault();
        });

    // Transform Dialog Forms to Ajax Form and specify the Server Method Executed
    var dialogFormId = dialogDivId + ' > #ontoform';
    $(dialogFormId).submit(function(event){

        //disable the default form submission
        event.preventDefault();

        //grab all form data
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: 'OntoFileUploadServlet',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (json) {
                if (typeof(json.error) != "undefined")  {
                    alert(json.error);
                    return;
                }

                // Response retrieved from server
                var jsonObj = json.jsonarray[0];

                // Close Dialog Panel
                $(dialogDivId).dialog("close");

                // The Ontology
                $(ontoDivId).empty();
                $(ontoDivId).append("<h3><a href=\"" + jsonObj.uri + "\">" + jsonObj.label + "</a></h3>");

                // Save Ontology JSON in a hidden field
                var hiddenOntologyInputID = "#jsonOntology" + panelIndex;
                var hiddenOntologyPropertiesInputID = "#jsonOntologyProperties" + panelIndex;
                $(hiddenOntologyPropertiesInputID).val(JSON.stringify(jsonObj));

                // Place ontology parameters in middle screen
                loadOntologyProperties(panelIndex);

                $(ontoDivId + " > h3 > a").click(function() {
                    loadOntologyProperties(panelIndex);
                    return false;
                });

                var jsonParameters =
                {
                    uid: jsonObj.uid,
                    main: {
                        uri: jsonObj.uri,
                        path: jsonObj.path
                    },
                    imports: []
                };
                $.cookie( 'param' + panelIndex, JSON.stringify(jsonParameters) );

                if (jsonObj.imports.length == 0) {
                    // Ready - Load Elements
                    var jsonParamStr = $.cookie( 'param' + panelIndex );
                    //alert("Case-1-Parameters-" + panelIndex + ":: " + jsonParamStr);

                    $("#divO").empty();
                    loadOntClasses(panelIndex, jsonParamStr);
                    loadOntObjectProperties(panelIndex, jsonParamStr);
                    loadOntDatatypeProperties(panelIndex, jsonParamStr);
                    loadIndividuals(panelIndex, jsonParamStr);

                    $(hiddenOntologyInputID).val(jsonParamStr);

                    loadAndSaveOntElementFlatLists(panelIndex, jsonParamStr);

                    getSuggestionsWhenReady();

                    $.cookie( 'param' + panelIndex, null);
                } else {
                    // Resolve Dependencies (first)
                    $("#depenDialog").empty();

                    // Prepare a new Dialog for Dependencies
                    var formHtml = "<form id=\"ontoform\" action=\"OntoFileUploadServlet\" method=\"post\" enctype=\"multipart/form-data\">";
                    for (var i=0; i<jsonObj.imports.length ; i++) {
                        formHtml += "<p><strong>URI:</strong> " + jsonObj.imports[i] + "</p>";
                        //formHtml += "<input type=\"file\" id=\"ontofile\" name=\"ontofileD" + i + "\"/>";
                        formHtml += "<div id='ontoProvideAccordionD" + i + "'> <h3>Select File from your Disk:</h3> <div>";
                        formHtml += "<input type=\"file\" name=\"ontofileD" + i + "\"/>";
                        formHtml += "</div> <h3>Provide the URL:</h3> <div>";
                        formHtml += "<input type=\"text\" name=\"ontourlD" + i + "\"/>";
                        formHtml += "</div> </div>";
                    }
                    formHtml += "</br>";
                    formHtml += "<input type=\"submit\" value=\"Provide ALL\" onclick=\"this.disabled=true;\"/>";
                    formHtml += "</form>";
                    $("#depenDialog").html(formHtml);

                    for (var i=0; i<jsonObj.imports.length ; i++) {
                        $("#ontoProvideAccordionD" + i + "").accordion({
                            event: "click hoverintent",
                            heightStyle: "content"
                        });
                    }

                    // Show Dependencies Dialog
                    $("#depenDialog").dialog("open");

                    // Transform Dependencies Form to Ajax Form and define actions
                    var depenDialogFormId = '#depenDialog > #ontoform';
                    $(depenDialogFormId).submit(function(event){
                        //disable the default form submission
                        event.preventDefault();
                        //grab all form data
                        var formData = new FormData($(this)[0]);
                        $.ajax({
                            url: 'OntoFileUploadServlet',
                            type: 'POST',
                            data: formData,
                            async: false,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (json) {
                                if (typeof(json.error) != "undefined")  {
                                    alert(json.error);
                                    return;
                                }

                                // Load Cookie
                                var jsonParamStr = $.cookie( 'param' + panelIndex);
                                var jsonParam = $.parseJSON(jsonParamStr);

                                var imports = new Array();
                                var index = 0;
                                for (var i=0; i<json.jsonarray.length ; i++) {
                                    var jsonObj = json.jsonarray[i];
                                    jsonParam.imports.push(
                                        { uri: jsonObj.uri, path: jsonObj.path }
                                    );
                                    if (jsonObj.imports.length > 0) {
                                        for (var i=0; i<jsonObj.imports.length ; i++) {
                                            imports[index] = jsonObj.imports[i];
                                            index++;
                                        }
                                    }
                                }

                                // Save Cookie
                                $.cookie( 'param' + panelIndex, JSON.stringify(jsonParam) );

                                if (imports.length == 0) {
                                    $("#depenDialog").dialog( "close" );
                                    // Ready ... !!!

                                    var jsonParamStr = $.cookie( 'param' + panelIndex );
                                    //alert("Case-2-Parameters-" + panelIndex + ":: " + jsonParamStr);

                                    $("#divO").empty();
                                    loadOntClasses(panelIndex, jsonParamStr);
                                    loadOntObjectProperties(panelIndex, jsonParamStr);
                                    loadOntDatatypeProperties(panelIndex, jsonParamStr);
                                    loadIndividuals(panelIndex, jsonParamStr);

                                    $(hiddenOntologyInputID).val(jsonParamStr);

                                    loadAndSaveOntElementFlatLists(panelIndex, jsonParamStr);

                                    getSuggestionsWhenReady();

                                    $.cookie( 'param' + panelIndex, null);
                                } else {
                                    // Dependencies still exist ... !!!
                                    $("#depenDialog").dialog( "close" );

                                    $("#depenDialog > #ontoform").empty();
                                    formContentHtml = "";
                                    for (var i=0; i<imports.length ; i++) {
                                        formContentHtml += "<p><strong>URI:</strong> " + imports[i] + "</p>";
                                        formContentHtml += "<input type=\"file\" id=\"ontofile" + i + "\" name=\"Ontology File " + i + "\"/>";
                                    }
                                    formContentHtml += "</br>";
                                    formContentHtml += "<input type=\"submit\" value=\"Upload\" />";
                                    $("#depenDialog > #ontoform").html(formContentHtml);
                                    $("#depenDialog").dialog("open");
                                }
                                return false;
                            }
                        }); // End ajax function
                        return false;
                    }); // End ajaxForm function
                }
                return false;
            }
        }); // End ajax function
        return false;
    }); // End

	// Patch: Submit "Ajax" Form when one of the two submit buttons is pressed
	var dialogFormUpload = dialogDivId + ' > #ontoform' + " input[value=Upload]";
	var dialogFormLoad = dialogDivId + ' > #ontoform' + " input[value=Load]";
	var dialogFormUploadOrLoad = dialogFormUpload + " , " + dialogFormLoad;
	$(dialogFormUploadOrLoad).button().click(function( event ) {
		$(dialogDivId + ' > #ontoform').submit();
	});

	// TODO: Uncomment and Update for Loading a specific Source or Target Model
	//if (panelIndex == "B") {
	//	$(dialogFormId + " input[type=text]").val("HarmonicSS-Reference-Model");
	//	$(dialogFormId).submit();
	//}
}

function loadOntologyProperties(divIndex) {
	var hiddenOntologyPropertiesInputID = "#jsonOntologyProperties" + divIndex;
	var jsonStr = $(hiddenOntologyPropertiesInputID).val();
	var json = $.parseJSON(jsonStr);
	
	var interDiv = "#divA";
	if (divIndex == "B") {
		interDiv = "#divB";
	}
	var element = '#tabsM > #tabs-1 > ' + interDiv;
    $(element).show();

	$(element).empty();
	$(element).append("<img src=\"images/eraser.png\" class=\"eraser\"/>");
	// Label
	$(element).append("<h3>" + /* "Ontology" + divIndex + " : " + */ json.label + "</h3>");
	// URI
	$(element).append("<p class=\"uri\"><em><strong>URI:</strong> " + json.uri + "</em></p>");
	// Comments
	if( typeof(json.comment) != "undefined" ) {
		lines = json.comment.split(/\n/);
		for(var i in lines) {
			$(element).append("<p>" + lines[i] + "</p>");
		}
	}
	// Version Info
	if( typeof(json.versioninfo) != "undefined" ) {
		$(element).append("<h4>Version:</h4>");
		$(element).append("<p>" + json.versioninfo + "</p>");
	}
	// Date
	if( typeof(json.date) != "undefined" ) {
		$(element).append("<h4>Date:</h4>");
		$(element).append("<p>" + json.date + "</p>");
	}
	// Creator
	if( typeof(json.creator) != "undefined" ) {
		$(element).append("<h4>Creator:</h4>");
		$(element).append("<p>" + json.creator + "</p>");	
	}
	
	handleEraser();
}

/**
 * This method undertakes to load the OWL classes from the Ontology provided and update the appropriate tab of the screen
 * 
 * @param divIndex
 * 		An index (A or B) indicating the panel in which the ontologies will be loaded.
 * @param jsonParamStr
 * 		A JSON object specifying the main parameters of the Ontology including the paths in the server of the main ontology and the imported ones.
 */
function loadOntClasses(divIndex, jsonParamStr) {
	var pageDivId = "#tabs" + divIndex;
	$.post("OntoPanelServlet", { jsonStr: jsonParamStr, elements: "Classes" }, function(data){
        if (typeof(data.error) != "undefined")  {
            alert("Unable to get CLASSES TREE !" + data.error);
            return;
        }
		// Put the classes on a tree
		$(pageDivId + ' > #tabs-1').empty();
		$(pageDivId + ' > #tabs-1').html("<ul class=\"treeview\" id=\"ontotree\">"+ data + "</ul>");
		$(pageDivId + ' > #tabs-1 > #ontotree').treeview({
			//collapsed: true,
			animated: "fast",
			control:"#sidetreecontrol",
			prerendered: true,
			persist: "location"
		});
        $(pageDivId + " > #tabs-1 > ul > li[class='expandable']").children('.hitarea').trigger('click');

        // Enable classes tab
		$(pageDivId).tabs("enable", 0);
		
		// Add Click Listeners on the elements of tree
		$(pageDivId + ' > #tabs-1 a[href]').click(function(evt){
			// URI of selected element
			var elementUri = $(this).attr('href');
			
			var aColor = "#0B0B61";
			if (divIndex == "B") {
				aColor = "#0B3B0B";
			}
			
			$(pageDivId + ' a').each(function(index) {
				var harmcolor = $(this).attr('harmcolor');
				if (typeof(harmcolor) == "undefined") {
					$(this).css('color',aColor);
				} else {
					$(this).css('color',harmcolor);
				}
			});
			// Style of selected element
			$(this).css('color','red');
			$.cookie('SelectedClassOnt' + divIndex, JSON.stringify( { name: $(this).text() , uri: $(this).attr('href')} ) );
			$.cookie('SelectedIndivOnt' + divIndex, null);
			$.cookie('SelectedObjPropOnt' + divIndex, null);
			$.cookie('SelectedDataPropOnt' + divIndex, null);
			
			var interDiv = "#divA";
	    	if (divIndex == "B") {
	    		interDiv = "#divB";
	    	} 
	    	var element = '#tabsM > #tabs-1 > ' + interDiv;
			
			//$(element).empty();
			//$(element).append("<p>Please Wait...</p>");
			
			// Get Definition
	    	$.post(  
	            "OntoElemServlet",  
	            {
	            	div: divIndex,
	        		jsonStr: jsonParamStr, 
	        		uri: elementUri,
	        		method: "GetElementDefinition"
	            },  
	            function(json){
                    if (typeof(json.error) != "undefined")  {
                        alert("Unable to LOAD elements definition: " + json.error);
                        return;
                    }
	            	$(element).empty();
	            	$(element).append("<img src=\"images/eraser.png\" class=\"eraser\"/>");
	            	// Label
	            	$(element).append("<h3>" + json.label + "</h3>");
	            	// URI
	            	$(element).append("<p class=\"uri\"><em><strong>URI:</strong> " + json.uri + "</em></p>");
	            	// Comments
	            	if( typeof(json.comment) != "undefined" ) {
	            		lines = json.comment.split(/\n/);
	                	for(var i in lines) {
	                		$(element).append("<p>" + lines[i] + "</p>");
	                	}
	                }
					
					// Other Annoation Properties
	            	if( typeof(json.otherprop) != "undefined" ) {
	            		$.each(json.otherprop, function(i, other) {
	            			$(element).append("<p><div class=\"range\">" + other.name + ":</div> <span class=\"otherannopropvalue\">" + other.value + "</span></p>");
                    	});
	            	}
					
	            	// Super Classes
                    if( typeof(json.superclasses) != "undefined" ) {
                        $(element).append("<h4 class='elemWithNext'>Parameters (" + json.superclasses.length + ")</h4>");
                        $(element).append("<ul class='superclasses'></ul>");
                        var elementSCUL = element + " .superclasses";
                        $.each(json.superclasses, function(i, item) {
                            $(elementSCUL).append("<li>" + item.str + "</li>");
                        });
                    }
					
					// Inherited Super Classes
					if( typeof(json.inheritedsuperclasses) != "undefined" ) {
						$(element).append("<p class='elemWithNext'>Inherited Super Classes (" + json.inheritedsuperclasses.length + ")</p>");
                        $(element).append("<ul class='inheritedsuperclasses'></ul>");
                        var elementISCUL = element + " .inheritedsuperclasses";
                        $.each(json.inheritedsuperclasses, function(i, item) {
                            $(elementISCUL).append("<li>" + item.str + "</li>");
                        });
					}
					
					//Within the Range of
					if( typeof(json.withintherange) != "undefined" ) {
						$(element).append("<h4 class='elemWithNext'>Within the Range of ObjectProperties (" + json.withintherange.length + ")</h4>");
                        $(element).append("<ul class='withintherange'></ul>");
                        var elementWTRUL = element + " .withintherange";
                        $.each(json.withintherange, function(i, item) {
                            $(elementWTRUL).append("<li>" + item.str + "</li>");
                        });
	            	}
	            
	            	// Instances
	            	if( typeof(json.instances) != "undefined" ) {
	            		$(element).append("<h4 class='elemWithNext'>Instances (" + json.instances.length + ")</h4>");
                        $(element).append("<ul class='instances'></ul>");
                        var elementIUL = element + " .instances";
                        $.each(json.instances, function(i, item) {
                            $(elementIUL).append("<li>" + item.str + "</li>");
                        });
	            	}
	            	
	            	//Disjoint With
	            	if( typeof(json.disjointwith) != "undefined" ) {
	            		$(element).append("<h4 class='elemWithNext'>Disjoint With  (" + json.disjointwith.length + ")</h4>");
                        $(element).append("<p class='disjointwith'></p>");
                        var elementDWP = element + " .disjointwith";
                        $.each(json.disjointwith, function(i, item) {
                            $(elementDWP).append(item.str);
                            if (i < json.disjointwith.length - 1) {
                                $(element).append(", ");
                            }
                        });
	            	}
	            	
	            	// See Also
	            	if( typeof(json.seealso) != "undefined" ) {
	            		$(element).append("<h4>See Also</h4>");
	            		$.each(json.seealso, function(i, item) {
	            			$(element).append("<p>" + item.str + "</p>");
                    	});
	            	}
	            	
	            	handleLinks(jsonParamStr, json.div);
                    $(".elemWithNext").click(function() {
                        $(this).next().toggle();
                    });
	            	handleEraser();
	            },  
	          	"json"  
	    	);  // end post function
			return false;
		}); // End click function
		
	});	// End post function
	
}

/**
 * This method undertakes to load the OWL Object Properties from the Ontology provided and update the appropriate tab of the screen
 * 
 * @param divIndex
 * 		An index (A or B) indicating the panel in which the ontologies will be loaded.
 * @param jsonParamStr
 * 		A JSON object specifying the main parameters of the Ontology including the paths in the server of the main ontology and the imported ones.
 */
function loadOntObjectProperties(divIndex, jsonParamStr) {
	var pageDivId = "#tabs" + divIndex;
	$.post("OntoPanelServlet", { jsonStr: jsonParamStr, elements: "ObjectProperties" }, function(data){
        if (typeof(data.error) != "undefined")  {
            alert("Unable to get DATATYPE PROPERTIES TREE !" + data.error);
            return;
        }
		// Put the Datatype Properties on a tree
		$(pageDivId + ' > #tabs-2').empty();
		$(pageDivId + ' > #tabs-2').html("<ul class=\"treeview\" id=\"ontotree\">"+ data + "</ul>");
		$(pageDivId + ' > #tabs-2 > #ontotree').treeview({
			//collapsed: true,
			animated: "fast",
			control:"#sidetreecontrol",
			prerendered: true,
			persist: "location"
		});
        $(pageDivId + " > #tabs-2 > ul > li[class='expandable']").children('.hitarea').trigger('click');

        // Enable Datatype Properties Tab
		$(pageDivId).tabs("enable", 1);
		
		// Add Click Listeners on the elements of tree
		$(pageDivId + ' > #tabs-2 a[href]').click(function(evt){
			// URI of selected element
			var elementUri = $(this).attr('href');
			
			var aColor = "#0B0B61";
			if (divIndex == "B") {
				aColor = "#0B3B0B";
			} 
			
			// Style of selected element
			$(pageDivId + ' a').css('color',aColor);
			$(this).css('color','red');
			$.cookie('SelectedObjPropOnt' + divIndex, JSON.stringify( { name: $(this).text() , uri: $(this).attr('href')} ));
			$.cookie('SelectedClassOnt' + divIndex, null);
			$.cookie('SelectedIndivOnt' + divIndex, null);
			$.cookie('SelectedDataPropOnt' + divIndex, null);
			
			// Get Definition
	    	$.post(  
	            "OntoElemServlet",  
	            {
	            	div: divIndex,
	        		jsonStr: jsonParamStr, 
	        		uri: elementUri,
	        		method: "GetElementDefinition"
	            },  
	            function(json){
                    if (typeof(json.error) != "undefined")  {
                        alert("Unable to LOAD elements definition: " + json.error);
                        return;
                    }
	            	var interDiv = "#divA";
	    			if (divIndex == "B") {
	    				interDiv = "#divB";
	    			} 
	    			var element = '#tabsM > #tabs-1 > ' + interDiv;
	            	
	            	$(element).empty();
	            	$(element).append("<img src=\"images/eraser.png\" class=\"eraser\"/>");
	            	// Label
	            	$(element).append("<h3>" + /* "Ontology" + divIndex + " : " + */ json.label + "</h3>");
	            	// URI
	            	$(element).append("<p class=\"uri\"><em><strong>URI:</strong> " + json.uri + "</em></p>");
	            	// Comments
	            	if( typeof(json.comment) != "undefined" ) {
	            		lines = json.comment.split(/\n/);
	                	for(var i in lines) {
	                		$(element).append("<p>" + lines[i] + "</p>");
	                	}
	               }
	            	// Domain & Range
	            	if( typeof(json.domain) != "undefined" ) {
	            		$(element).append("<p><div class=\"domain\">Domain:</div> " + json.domain + "</p>");
	            	}
	            	if( typeof(json.range) != "undefined" ) {
	            		$(element).append("<p><div class=\"range\">Range:</div> " + json.range + "</p>");
	            	}
	            	
					// Other Annoation Properties
	            	if( typeof(json.otherprop) != "undefined" ) {
	            		$.each(json.otherprop, function(i, other) {
	            			$(element).append("<p><div class=\"range\">" + other.name + ":</div> <span class=\"otherannopropvalue\">" + other.value + "</span></p>");
                    	});
	            	}
					
	            	// See Also
	            	if( typeof(json.seealso) != "undefined" ) {
	            		$(element).append("<h4>See Also</h4>");
	            		$.each(json.seealso, function(i, item) {
	            			$(element).append("<p>" + item.str + "</p>");
                    	});
	            	}
	            	
	            	handleLinks(jsonParamStr, json.div);
	            	handleEraser();
	            },  
	          	"json"  
	    	);  // end post function
			return false;
		}); // End click function
		
	});	// End get function
	
}

/**
 * This method undertakes to load the OWL Datatype Properties from the Ontology provided and update the appropriate tab of the screen
 * 
 * @param divIndex
 * 		An index (A or B) indicating the panel in which the ontologies will be loaded.
 * @param jsonParamStr
 * 		A JSON object specifying the main parameters of the Ontology including the paths in the server of the main ontology and the imported ones.
 */
function loadOntDatatypeProperties(divIndex, jsonParamStr) {
	var pageDivId = "#tabs" + divIndex;
	$.post("OntoPanelServlet", { jsonStr: jsonParamStr, elements: "DatatypeProperties" }, function(data){
        if (typeof(data.error) != "undefined")  {
            alert("Unable to get DATATYPE PROPERTIES TREE !" + data.error);
            return;
        }
		// Put the Datatype Properties on a tree
		$(pageDivId + ' > #tabs-3').empty();
		$(pageDivId + ' > #tabs-3').html("<ul class=\"treeview\" id=\"ontotree\">"+ data + "</ul>");
		$(pageDivId + ' > #tabs-3 > #ontotree').treeview({
			//collapsed: true,
			animated: "fast",
			control:"#sidetreecontrol",
			prerendered: true,
			persist: "location"
		});
        $(pageDivId + " > #tabs-3 > ul > li[class='expandable']").children('.hitarea').trigger('click');

        // Enable Datatype Properties Tab
		$(pageDivId).tabs("enable", 2);
		
		// Add Click Listeners on the elements of tree
		$(pageDivId + ' > #tabs-3 a[href]').click(function(evt){
			// URI of selected element
			var elementUri = $(this).attr('href');
			
			var aColor = "#0B0B61";
			if (divIndex == "B") {
				aColor = "#0B3B0B";
			} 
			
			$(pageDivId + ' a').each(function(index) {
				var harmcolor = $(this).attr('harmcolor');
				if (typeof(harmcolor) == "undefined") {
					$(this).css('color',aColor);
				} else {
					$(this).css('color',harmcolor);
				}
			});
			
			// Style of selected element
			$(this).css('color','red');
			$.cookie('SelectedDataPropOnt' + divIndex, JSON.stringify( { name: $(this).text() , uri: $(this).attr('href')} ));
			$.cookie('SelectedClassOnt' + divIndex, null);
			$.cookie('SelectedIndivOnt' + divIndex, null);
			$.cookie('SelectedObjPropOnt' + divIndex, null);
			
			// Get Definition
	    	$.post(  
	            "OntoElemServlet",  
	            {
	            	div: divIndex,
	        		jsonStr: jsonParamStr, 
	        		uri: elementUri,
	        		method: "GetElementDefinition"
	            },  
	            function(json){
                    if (typeof(json.error) != "undefined")  {
                        alert("Unable to LOAD elements definition: " + json.error);
                        return;
                    }
	            	var interDiv = "#divA";
	    			if (divIndex == "B") {
	    				interDiv = "#divB";
	    			} 
	    			var element = '#tabsM > #tabs-1 > ' + interDiv;
	            	
	            	$(element).empty();
	            	$(element).append("<img src=\"images/eraser.png\" class=\"eraser\"/>");
	            	// Label
	            	$(element).append("<h3>" + /* "Ontology" + divIndex + " : " + */ json.label + "</h3>");
	            	// URI
	            	$(element).append("<p class=\"uri\"><em><strong>URI:</strong> " + json.uri + "</em></p>");
	            	// Comments
	            	if( typeof(json.comment) != "undefined" ) {
	            		lines = json.comment.split(/\n/);
	                	for(var i in lines) {
	                		$(element).append("<p>" + lines[i] + "</p>");
	                	}
	                }
	            	// Domain & Range
	            	if( typeof(json.domain) != "undefined" ) {
	            		$(element).append("<p><div class=\"domain\">Domain:</div> " + json.domain + "</p>");
	            	}
	            	if( typeof(json.range) != "undefined" ) {
	            		$(element).append("<p><div class=\"range\">Range:</div> " + json.range + "</p>");
	            	}
	            	
	            	// Other Annoation Properties
	            	if( typeof(json.otherprop) != "undefined" ) {
	            		$.each(json.otherprop, function(i, other) {
	            			$(element).append("<p><div class=\"range\">" + other.name + ":</div> <span class=\"otherannopropvalue\">" + other.value + "</span></p>");
                    	});
	            	}
	            	
	            	// See Also
	            	if( typeof(json.seealso) != "undefined" ) {
	            		$(element).append("<h4>See Also</h4>");
	            		$.each(json.seealso, function(i, item) {
	            			$(element).append("<p>" + item.str + "</p>");
                    	});
	            	}
	            	
	            	handleLinks(jsonParamStr, json.div);
	            	handleEraser();
	            },  
	          	"json"  
	    	);  // end post function
			return false;
		}); // End click function
		
	});	// End get function

	//+++
}

/**
 * This method undertakes to load the OWL Individuals from the Ontology provided and update the appropriate tab of the screen
 * 
 * @param divIndex
 * 		An index (A or B) indicating the panel in which the ontologies will be loaded.
 * @param jsonParamStr
 * 		A JSON object specifying the main parameters of the Ontology including the paths in the server of the main ontology and the imported ones.
 */
function loadIndividuals(divIndex, jsonParamStr) {
	var pageDivId = "#tabs" + divIndex;
	$.post("OntoPanelServlet", { jsonStr: jsonParamStr, elements: "Individuals" }, function(data){
        if (typeof(data.error) != "undefined")  {
            alert("Unable to get INDIVIDUALS List: " + data.error);
            return;
        }
		// Put the Datatype Properties on a tree
		$(pageDivId + ' > #tabs-4').empty();
		$(pageDivId + ' > #tabs-4').html("<ul class=\"treeview\" id=\"ontotree\">"+ data + "</ul>");
		$(pageDivId + ' > #tabs-4 > #ontotree').treeview({
			collapsed: true,
			animated: "fast",
			control:"#sidetreecontrol",
			prerendered: true,
			persist: "location"
		});
		
		// Enable Datatype Properties Tab
		$(pageDivId).tabs("enable", 3);
		
		// Add Click Listeners on the elements of tree
		$(pageDivId + ' > #tabs-4 a[href]').click(function(evt){
			// URI of selected element
			var elementUri = $(this).attr('href');
			
			var aColor = "#0B0B61";
			if (divIndex == "B") {
				aColor = "#0B3B0B";
			} 
			
			// Style of selected element
			$(pageDivId + ' a').css('color',aColor);
			$(this).css('color','red');
			$.cookie('SelectedIndivOnt' + JSON.stringify( { name: $(this).text() , uri: $(this).attr('href')} ));
			$.cookie('SelectedClassOnt' + divIndex, null);
			$.cookie('SelectedObjPropOnt' + divIndex, null);
			$.cookie('SelectedDataPropOnt' + divIndex, null);
			
			// Get Definition
	    	$.post(
	            "OntoElemServlet",  
	            {
	            	div: divIndex,
	        		jsonStr: jsonParamStr, 
	        		uri: elementUri,
	        		method: "GetElementDefinition"
	            },  
	            function(json){
                    if (typeof(json.error) != "undefined")  {
                        alert("Unable to LOAD elements definition: " + json.error);
                        return;
                    }
	            	var interDiv = "#divA";
	    			if (divIndex == "B") {
	    				interDiv = "#divB";
	    			} 
	    			var element = '#tabsM > #tabs-1 > ' + interDiv;
	            	
	            	$(element).empty();
	            	$(element).append("<img src=\"images/eraser.png\" class=\"eraser\"/>");
	            	// Label
	            	$(element).append("<h3>" + json.label + "</h3>");
	            	// URI
	            	$(element).append("<p class=\"uri\"><em><strong>URI:</strong> " + json.uri + "</em></p>");
	            	// Comments
	            	if( typeof(json.comment) != "undefined" ) {
	            		lines = json.comment.split(/\n/);
	                	for(var i in lines) {
	                		$(element).append("<p>" + lines[i] + "</p>");
	                	}
	               	}
	               	// Belongs To
                    if( typeof(json.belongs) != "undefined" ) {
                        $(element).append("<h4>Belongs To</h4>");
                        $(element).append("<ul class='belongs'></ul>");
                        var elementBL = element + " .belongs";
                        $.each(json.belongs, function(i, item) {
                            $(elementBL).append("<li>" + item.str + "</li>");
                        });
                    }
	               	
	            	// Statements
                    if( typeof(json.statements) != "undefined" ) {
                        $(element).append("<h4>Statements</h4>");
                        $(element).append("<ul class='statements'></ul>");
                        var elementSTMS = element + " .statements";
                        $.each(json.statements, function(i, item) {
                            $(elementSTMS).append("<li>" + item.str + "</li>");
                        });
                    }
	            	
	            	// See Also
	            	if( typeof(json.seealso) != "undefined" ) {
	            		$(element).append("<h4>See Also</h4>");
	            		$.each(json.seealso, function(i, item) {
	            			$(element).append("<p>" + item.str + "</p>");
                    	});
	            	}
	            	
	            	handleLinks(jsonParamStr, json.div);
	            	handleEraser();
	            },  
	          	"json"  
	    	);  // end post function
			return false;
		}); // End click function
		
	});	// End get function

	//+++
}

/**
 * This methods handle the eraser button presented at the top right corner of the Definition Tab (in the middle of the screen) 
 */
function handleEraser() {
	$(".eraser")
		.click(function() {
			$(this).parent().fadeTo('fast', 1.0);
			$(this).parent().empty();
		});
	$(".eraser")	
		.hover(
			function () {
				$(this).css("cursor", "hand");
				$(this).parent().fadeTo('fast', 0.5);
			}, 
			function () {
				$(this).css("cursor", "pointer");
				$(this).parent().fadeTo('fast', 1.0);
			}
		);	
}

/**
 * This method hanles the links presented at the middle of the user's screen 
 */
function handleLinks(jsonParamStr, divIndex) {
	// "#tabsM > #tabs-1 a, #clickedElementDialog a" "#divA or #divB"
	var aSelector;
	if (divIndex == null) {
		aSelector = "#clickedElementDialog a";
	} else {
		aSelector = "#tabsM > #tabs-1 > #div" + divIndex + " a";
	}
	$(aSelector).click(function(evt) {
		// URI of clicked element
		var elementUri = $(this).attr('href');
		
		if (elementUri.match("^http://www.w3.org/TR/xmlschema-2/")) return true;
		
		if ($(this).attr('class') == "extlink") return true;
		
		presentDataInDialog(jsonParamStr, elementUri);
		
		return false;
	});
}

function presentDataInDialog(jsonParamStr, elementUri) {
		$("#clickedElementDialog").empty();
		$("#clickedElementDialog").html("<p>Please wait...</p>");
		$("#clickedElementDialog").dialog("open");
		
		// Get Definition
	   	$.post(
	   		"OntoElemServlet",  
	        {
	   			div: "-",
	        	jsonStr: jsonParamStr, 
	        	uri: elementUri,
	        	method: "GetElementDefinition"
	        },  
	        function(json){
                if (typeof(json.error) != "undefined")  {
                    alert("Unable to load ontological definition of element: " + json.error);
                    return;
                }

	    		var element = '#clickedElementDialog';
	            	
	            $(element).empty();
	            // Label
	            $(element).append("<h3>" + json.label + "</h3>");
	            // URI
	            $(element).append("<p class=\"uri\"><em><strong>URI:</strong> " + json.uri + "</em></p>");
	            // Comments
	            if( typeof(json.comment) != "undefined" ) {
	            	lines = json.comment.split(/\n/);
	               	for(var i in lines) {
	               		$(element).append("<p>" + lines[i] + "</p>");
	               	}
	            }
	            
            	// Other Annoation Properties
            	if( typeof(json.otherprop) != "undefined" ) {
            		$.each(json.otherprop, function(i, other) {
            			$(element).append("<p><div class=\"range\">" + other.name + ":</div> " + other.value + "</p>");
                	});
            	}
	            
	            /* Especially for classes */
	            
	            // Super Classes
	            if( typeof(json.superclasses) != "undefined" ) {
	         	   $(element).append("<h4>Parameters</h4>");
	            	$(element).append("<ul>");
	            	$.each(json.superclasses, function(i, item) {
	            		$(element).append("<li>" + item.str + "</li>");
					});
	            	$(element).append("</ul>");
				}
					
				// Inherited Super Classes
				if( typeof(json.inheritedsuperclasses) != "undefined" ) {
					$(element).append("<p>Inherited Super Classes</p>");
	            	$(element).append("<ul>");
	            	$.each(json.inheritedsuperclasses, function(i, item) {
	            		$(element).append("<li>" + item.str + "</li>");
					});
	            	$(element).append("</ul>");
				}
				
				// Sub Classes subclasses
				if( typeof(json.subclasses) != "undefined" ) {
		            $(element).append("<h4>Sub Classes</h4>");
		           	$(element).append("<ul>");
		           	$.each(json.subclasses, function(i, item) {
		           		$(element).append("<li>" + item.str + "</li>");
					});
		           	$(element).append("</ul>");
				}
					
				//Within the Range of
				if( typeof(json.withintherange) != "undefined" ) {
					$(element).append("<h4>Within the Range of ObjectProperties:</h4>");
	            	$(element).append("<ul>");
	            	$.each(json.withintherange, function(i, item) {
	            		$(element).append("<li>" + item.str + "</li>");
                   	});
	            	$(element).append("</ul>");
	            }
	            
	            // Instances
	            if( typeof(json.instances) != "undefined" ) {
	            	$(element).append("<h4>Instances</h4>");
	            	$(element).append("<ul>");
	            	$.each(json.instances, function(i, item) {
	            		$(element).append("<li>" + item.str + "</li>");
                   	});
                   	(element).append("</ul>");
	            }
	            	
	            //Disjoint With
	            if( typeof(json.disjointwith) != "undefined" ) {
	            	$(element).append("<h4>Disjoint With</h4>");
	            	$(element).append("<p>");
	            	$.each(json.disjointwith, function(i, item) {
	            		$(element).append(item.str);
	            		if (i < json.disjointwith.length - 1) {
	            			$(element).append(", ");
	            		}
                   	});
                   	$(element).append("</p>");
	            }
	            
	            /* Especially for properties */
	            
	            // Domain & Range
	            if( typeof(json.domain) != "undefined" ) {
	            	$(element).append("<p><div class=\"domain\">Domain:</div> " + json.domain + "</p>");
	            }
	            if( typeof(json.range) != "undefined" ) {
	            	$(element).append("<p><div class=\"range\">Range:</div> " + json.range + "</p>");
	            }
	            
	            /* Especially for instances */
	            
	            // Belongs To
				if( typeof(json.belongs) != "undefined" ) {
	            	$(element).append("<h4>Belongs To</h4>");
	            	$(element).append("<ul>");
	            	$.each(json.belongs, function(i, item) {
	            		$(element).append("<li>" + item.str + "</li>");
                   	});
	            	$(element).append("</ul>");
	            }
	               	
	            // Statements
	            if( typeof(json.statements) != "undefined" ) {
	            	$(element).append("<h4>Statements</h4>");
	            	$(element).append("<ul>");
	            	$.each(json.statements, function(i, item) {
	            		$(element).append("<li>" + item.str + "</li>");
                   	});
	            	$(element).append("</ul>");
	            }
	            	
	            // See Also
	            if( typeof(json.seealso) != "undefined" ) {
	            	$(element).append("<h4>See Also</h4>");
	            	$.each(json.seealso, function(i, item) {
	            		$(element).append("<p>" + item.str + "</p>");
                   	});
	            }
	            
	            handleLinks(jsonParamStr, null);
	        },  
	      	"json"  
	    );  // end post function	
}


function loadAndSaveOntElementFlatLists(divIndex, jsonParamStr) {
	$.post(
		"OntoElemServlet",  
	    {
			jsonStr: jsonParamStr, 
			uri: "NONE",
			method: "AllElements"
		},  
		function(json){
            if (typeof(json.error) != "undefined")  {
                alert("Unable to LOAD ontological elements from ontology " + divIndex + " : " + json.error);
                return;
            }
			var hiddenInputDiv = "#jsonOntologyFlatListsA";
			if (divIndex == "B") {
				hiddenInputDiv = "#jsonOntologyFlatListsB";
			} 
	    	$(hiddenInputDiv).val(JSON.stringify(json));
	    },  
		"json"  
	);  // end post function
}

