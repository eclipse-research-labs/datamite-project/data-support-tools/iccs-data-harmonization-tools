
## Data Harmonisation Process and Software Tools 

**Data Harmonisation** is part of the DATAMITE architecture, as presented in the following figure. 

![architecture_datamite](/docs/figures/arch_extended_harmonisation.png)

Data Harmonisation is **a semi-automatic process** that is totally based on Mapping Rules specified among the elements of two different models, i.e. (a) the **source model** that expresses the existing data structure and vocabularies being used and (b) the **target model** that expresses how the data should be expressed including the particular data format, structure and vocabularies.  

For this purpose, **three software components** have been developed which are depicted in the following figure: 

![architecture_harmonisation](/docs/figures/data-harmonisation-tools-img.png)

In a nutshell, the software components developed enable users to perform the following activities: 

 - Extract Metadata from CSV Files (CSV-Metadata-Extraction) and Relational Databases (DbSchemaToOWL)
 
 - Bridge the gap among the elements of source and target OWL ontologies (OWL Ontology Mapping) in the form of Mapping Rules.
 
 - Express the data in the appropriate format and structure so that they comply with the terminology specified in the target ontology (Data Transformation), based on the Mapping Rules specified

The data harmonisation process and software tools have been analytically described in the DATAMITE deliverables, including their input and output along with the interactions among them. 

## Repository Structure and Software Components Files

The software components have been developed using JAVA programming language and relevant frameworks.

The JAVA **frameworks/libraries** being used (i.e., jar files) have been uploaded in the LIB folder.

The **source code** of the three software components developed have been uploaded in the SOURCE folder. 

More information about each one of them exist in the respective folder, including, but not limited to the source files of the applications, the procedure followed for creating the application archives and testing them, and the dockerisation of these software components as well. 

